//
//  YYRowSingleView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYRowSingleView.h"

#import "HomeListStructInfoModel.h"
#import "HomeListItemGoodInfoModel.h"

@interface YYRowSingleView()

@property (nonatomic, strong) UIView  *shadowView;
@property (nonatomic, strong) UIView  *groundView;

@property (nonatomic, strong) UILabel *goodNameLabel;
@property (nonatomic, strong) UILabel *goodPriceLabel;
@property (nonatomic, strong) UILabel *extraLabel;
@property (nonatomic, strong) UILabel *couponLabel;
@property (nonatomic, strong) UILabel *volumeLabel;
@property (nonatomic, strong) UIImageView *goodImageView;
@property (nonatomic, strong) UIImageView *markImageView;
@property (nonatomic, strong) UIButton    *shareBtn;

@end

@implementation YYRowSingleView

#pragma mark - Setter method
- (void)setItemModel:(HomeListStructInfoModel *)itemModel {
    _itemModel = itemModel;
    if (_itemModel) {
        [self setSubViewContent];
    }
}
#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        CGFloat width = ((kScreenW - kSizeScale(8) - kSizeScale(12)*2)/2.0);
        CGFloat height = width + kSizeScale(80) - kSizeScale(8);
        
        self.shadowView = [YYCreateTools createView:XHClearColor];
        self.shadowView.frame = CGRectMake(0, 0, width, height);
        [self addSubview:self.shadowView];
                          
        self.groundView = [YYCreateTools createView:XHWhiteColor];
        self.groundView.v_cornerRadius = kSizeScale(4);
        self.groundView.frame = CGRectMake(0, 0, width, height - 0.5);
        [self.shadowView addSubview:self.groundView];
        
        self.goodImageView = [YYCreateTools createImageView:nil  viewModel:-1];
        [self.groundView addSubview:self.goodImageView];
        
        self.markImageView = [YYCreateTools createImageView:nil viewModel:-1];
        [self.goodImageView addSubview:self.markImageView];
        
        self.couponLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(10)
                                            textColor:HexRGB(0xFED18A)];
        self.couponLabel.v_cornerRadius = 2;
        self.couponLabel.textAlignment = NSTextAlignmentCenter;
        self.couponLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage gradientColorImageFromColors:@[] gradientType:GradientTypeUpleftToLowright imgSize:CGSizeMake(kSizeScale(50), kSizeScale(16))]];
        [self.groundView addSubview:self.couponLabel];
        
        self.goodNameLabel = [YYCreateTools createLabel:nil
                                                   font:normalFont(13)
                                              textColor:XHBlackColor];
        self.goodNameLabel.numberOfLines = 2;
        [self.groundView addSubview:self.goodNameLabel];
        
        self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                    font:boldFont(14)
                                               textColor:XHBlackColor];
        [self.groundView addSubview:self.goodPriceLabel];
        
        self.extraLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(14)
                                           textColor:XHRedColor];
        [self.groundView addSubview:self.extraLabel];
        
        self.volumeLabel = [YYCreateTools createLabel:@""
                                                 font:normalFont(11)
                                            textColor:XHBlackLitColor];
        self.volumeLabel.textAlignment = NSTextAlignmentRight;
        [self.groundView addSubview:self.volumeLabel];
        
        self.shareBtn = [YYCreateTools createBtnImage:@"MainHomeShare_icon"];
        self.shareBtn.isIgnore = NO;
        [self.shareBtn addTarget:self action:@selector(shareClickOperator:) forControlEvents:UIControlEventTouchUpInside];
        [self.groundView addSubview:self.shareBtn];
        [self.groundView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewEventClickHandler:)]];
        
        [self setSubViewLayout];
        /*[self.shadowView addShadow:ShadowDirectionBottom
                       shadowColor:HexRGBALPHA(0x000000, 0.2)
                     shadowOpacity:0.8
                      shadowRadius:0.5
                   shadowPathWidth:0.3];*/
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self.goodImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.width);
    }];
}

#pragma mark - Private
- (void)setSubViewLayout {
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.groundView);
        make.height.mas_equalTo(self.width);
    }];
    
    [self.markImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(6));
        make.left.mas_equalTo(kSizeScale(9));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(40)));
    }];
    
    [self.goodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImageView.mas_bottom).offset(5);
        make.left.mas_equalTo(kSizeScale(13));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self.couponLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(13));
        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(2));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(0), kSizeScale(16)));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.bottom.equalTo(self).offset(-kSizeScale(6));
    }];
    
    [self.extraLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(6));
        make.centerY.equalTo(self.goodPriceLabel);
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(kSizeScale(-2));
        make.centerY.equalTo(self.goodPriceLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(30)));
    }];
    
    // 销量
    [self.volumeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(kSizeScale(-10)).priorityLow();
        make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(20)));
        make.centerY.equalTo(self.goodPriceLabel);
    }];
}

- (void)setSubViewContent {
    UIColor *priceColor = XHRedColor;
    CGFloat priceWidth = 0.0, commisionWidth = 0.0;
    HomeListItemGoodInfoModel *goodModel = _itemModel.item;
    self.goodNameLabel.text = goodModel.title;
    
    if ([_itemModel.type integerValue] == 4) {
        self.shareBtn.hidden = YES;
        self.volumeLabel.hidden = NO;
        self.volumeLabel.text = goodModel.salesVolume;
    }
    else {
        self.shareBtn.hidden = NO;
        self.volumeLabel.text = @"";
        self.volumeLabel.hidden = YES;
    }
    
    //isMealZone 暂时隐藏
    self.shareBtn.hidden = YES;
    // 券展示处理
    if (kValidDictionary(goodModel.coupon)) {
        if ([goodModel.coupon objectForKey:@"maxDenomination"]) {
            CGSize couponSize = CGSizeZero;
            NSString *maxDenomination = [goodModel.coupon objectForKey:@"maxDenomination"];
            if (kValidString(maxDenomination)) {
                NSString *showContent = [NSString stringWithFormat:@"领券减¥%@",maxDenomination];
                couponSize = [YYCommonTools sizeWithText:showContent
                                                           font:normalFont(10)
                                                        maxSize:CGSizeMake(MAXFLOAT, kSizeScale(16))];
                couponSize.width = couponSize.width + kSizeScale(8);
                couponSize.height = kSizeScale(16);
                self.goodNameLabel.numberOfLines = 1;
                self.couponLabel.text = showContent;
            }
            else {
                self.couponLabel.text = nil;
                self.goodNameLabel.numberOfLines = 2;
            }
            [self.couponLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(couponSize.width);
            }];
        }
    }
    [self.goodImageView yy_sdWebImage:goodModel.coverImage.imageUrl
                 placeholderImageType:YYPlaceholderImageListGoodType];
    self.markImageView.hidden = kValidString(goodModel.tag)?NO:YES;
    if (kValidString(goodModel.tag)) {
        [self.markImageView sd_setImageWithURL:[NSURL URLWithString:goodModel.tag]];
    }
    if (goodModel.saleStatus) {
        if ([goodModel.saleStatus integerValue] == 2) {
            priceColor = HexRGB(0x11BC56);
        }
        else {
            priceColor = XHRedColor;
        }
    }
    else {
        priceColor = XHRedColor;
    }
    if (kIsFan) {
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(14) symbolTextColor:XHRedColor wordSpace:2 price:goodModel.reservePrice priceFont:boldFont(14) priceTextColor: XHRedColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
        NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",goodModel.originPrice]];
        [origin yy_setAttributes:XHBlackLitColor
                            font:midFont(11)
                         content:origin
                       alignment:NSTextAlignmentCenter];
        [origin addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, origin.length)];
        [origin addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle)range:NSMakeRange(0,origin.length)];
        if (@available(iOS 10.3, *)) {
            [origin addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)}range:NSMakeRange(0,origin.length)];
        }
        self.extraLabel.attributedText = origin;
    }
    else {
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(14) symbolTextColor:XHBlackColor wordSpace:2 price:goodModel.reservePrice priceFont:boldFont(14) priceTextColor: XHBlackColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
        priceWidth = [attribut boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.width;
        
        NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(11) symbolTextColor:priceColor wordSpace:2 price:goodModel.commission priceFont:boldFont(14) priceTextColor:priceColor symbolOffsetY:1.2];
        self.extraLabel.attributedText = extra;
        
        commisionWidth = [extra boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size.width;
        
        if ((priceWidth+commisionWidth)>kSizeScale(85)) {
            self.volumeLabel.text = @"";
        }
    }
}

#pragma mark - Event
- (void)shareClickOperator:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(rowSingleDidSelect:isShare:)]) {
        [self.delegate rowSingleDidSelect:self isShare:(sender == self.shareBtn)?YES:NO];
    }
}

- (void)viewEventClickHandler:(UITapGestureRecognizer *)tapGesture {
    [self shareClickOperator:nil];
}

@end
