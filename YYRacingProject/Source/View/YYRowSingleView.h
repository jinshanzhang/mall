//
//  YYRowSingleView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YYRowSingleView;
@class HomeListStructInfoModel;
@protocol YYRowSingleViewDelegate <NSObject>

@optional
- (void)rowSingleDidSelect:(YYRowSingleView *)singleView  isShare:(BOOL)isShare;

@end

@interface YYRowSingleView : UIView

@property (nonatomic, weak) id <YYRowSingleViewDelegate> delegate;
@property (nonatomic, strong) HomeListStructInfoModel *itemModel;

@end

NS_ASSUME_NONNULL_END
