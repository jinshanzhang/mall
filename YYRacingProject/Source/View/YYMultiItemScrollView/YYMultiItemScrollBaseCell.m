//
//  YYMultiItemScrollBaseCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYMultiItemScrollBaseCell.h"

@interface YYMultiItemScrollBaseCell ()

@end

@implementation YYMultiItemScrollBaseCell

#pragma mark - Setter method

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

#pragma mark - Public method
- (void)createUI {
    
}


@end
