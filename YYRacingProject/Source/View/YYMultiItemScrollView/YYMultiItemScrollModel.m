//
//  YYMultiItemScrollBaseInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYMultiItemScrollModel.h"

#import "YYCircleModel.h"
#import "HomeResourceItemInfoModel.h"
#import "HomeListItemGoodInfoModel.h"
#import "FriendCircleTopicInfoModel.h"

@implementation YYMultiItemScrollModel

@synthesize type = _type;
@synthesize title = _title;
@synthesize subTitle = _subTitle;
@synthesize url = _url;
@synthesize imageUrl = _imageUrl;
@synthesize originPrice = _originPrice;
@synthesize nowPrice = _nowPrice;
@synthesize couponPrice = _couponPrice;
@synthesize commission = _commission;
@synthesize skipLink = _skipLink;
@synthesize saleStatus = _saleStatus;
@synthesize identifierName = _identifierName;

@synthesize startTime = _startTime;
@synthesize endTime = _endTime;
@synthesize currentTime = _currentTime;


- (instancetype)initWithPictureItem:(YYPictureModel *)pictureModel {
    self = [super init];
    if (self) {
        //0普通图片  1商品  2二维码海报图 4视频
        _type = pictureModel.type;
        _nowPrice = pictureModel.price;
        _saleStatus = @(pictureModel.salesType);
        
        _imageUrl = pictureModel.imageUrl;
        _identifierName = @"YYCirclePictureCell";
    }
    return self;
}

- (instancetype)initWithFavorUserItem:(YYFavorUserModel *)favorModel {
    self = [super init];
    if (self) {
        _type = favorModel.isAddLayer; // 0 不增加图层 1 增加图层
        _imageUrl = favorModel.avatarUrl;
        
        _identifierName = @"YYCicleLikeCell";
    }
    return self;
}

- (instancetype)initWithListGoodItem:(HomeListItemGoodInfoModel *)goodItem {
    self = [super init];
    if (self) {
        _imageUrl = goodItem.coverImage.imageUrl;
        _originPrice = goodItem.originPrice;
        _nowPrice = goodItem.reservePrice;
        _commission = goodItem.commission;
        _saleStatus = goodItem.saleStatus;
        
        NSMutableDictionary *link = [NSMutableDictionary dictionary];
        if (goodItem.linkType) {
            [link setObject:goodItem.linkType forKey:@"linkType"];
        }
        if (kValidString(goodItem.url)) {
            [link setObject:goodItem.url forKey:@"url"];
        }
        _skipLink = link;

        _identifierName = @"SuperBrandLumpInfoCell";
    }
    return self;
}

- (instancetype)initWithTopicCircleItem:(FriendCircleTopicInfoModel *)topicModel {
    self = [super init];
    if (self) {
        _imageUrl = topicModel.imageUrl;
        _type = topicModel.topicId;
        _title = topicModel.title;
        
        _identifierName = @"YYCircleTopicCell";
    }
    return self;
}

- (instancetype)initWithHomeResourceItem:(HomeResourceItemInfoModel *)itemModel {
    self = [super init];
    if (self) {
        _title = itemModel.title;
        _subTitle = itemModel.sellpoint;
        _couponPrice = itemModel.couponPrice;
        
        _url = itemModel.url;
        
        if (itemModel.coverImage) {
            _imageUrl = itemModel.coverImage.imageUrl;
        }
        
        _startTime = itemModel.startTime;
        _currentTime = itemModel.currentTime;
        _endTime = itemModel.endTime;
        
        if (itemModel) {
            _identifierName = @"HomeNewVipGoodInfoCell";
        }
        else {
            _identifierName = @"HomeNewVipEmptyInfoCell";
        }
    }
    return self;
}


@end
