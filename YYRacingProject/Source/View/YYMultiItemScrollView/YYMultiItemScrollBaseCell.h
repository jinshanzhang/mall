//
//  YYMultiItemScrollBaseCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol YYMultiItemScrollViewProtocol;

@interface YYMultiItemScrollBaseCell : UICollectionViewCell

- (void)setViewModel:(id <YYMultiItemScrollViewProtocol>)viewModel;

- (void)createUI;
@end

NS_ASSUME_NONNULL_END
