//
//  YYMultiItemScrollView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYMultiItemScrollView.h"

#import "YYMultiItemScrollModel.h"

#import "YYMultiItemScrollBaseCell.h"
@interface YYMultiItemScrollView()
<
UICollectionViewDelegate,
UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *multiScrollView;
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) NSMutableArray *itemDatas;
@property (nonatomic, strong) NSMutableArray *identifierDatas; //重用标识存储

@end

@implementation YYMultiItemScrollView

#pragma mark - Setter method
- (UICollectionView *)multiScrollView {
    if (!_multiScrollView) {
        self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
        self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _multiScrollView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
        _multiScrollView.showsVerticalScrollIndicator = NO;
        _multiScrollView.showsHorizontalScrollIndicator = NO;
        _multiScrollView.delegate = self;
        _multiScrollView.dataSource = self;
        _multiScrollView.backgroundColor = XHWhiteColor;
    }
    return _multiScrollView;
}

- (void)setItemSize:(CGSize)itemSize {
    _itemSize = itemSize;
    self.flowLayout.itemSize = itemSize;
}

- (void)setMiniItemSpace:(CGFloat)miniItemSpace {
    _miniItemSpace = miniItemSpace;
    self.flowLayout.minimumInteritemSpacing = miniItemSpace;
}

- (void)setMinimumLineSpacing:(CGFloat)minimumLineSpacing {
    _minimumLineSpacing = minimumLineSpacing;
    self.flowLayout.minimumLineSpacing = minimumLineSpacing;
}

- (void)setScrollDirection:(UICollectionViewScrollDirection)scrollDirection {
    _scrollDirection = scrollDirection;
    self.flowLayout.scrollDirection = scrollDirection;
}

- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets {
    _contentEdgeInsets = contentEdgeInsets;
    self.multiScrollView.contentInset = contentEdgeInsets;
}

- (void)setScrollEnabled:(BOOL)scrollEnabled {
    _scrollEnabled = scrollEnabled;
    self.multiScrollView.scrollEnabled = scrollEnabled;
}

- (void)setContentBackgroundColor:(UIColor *)contentBackgroundColor {
    _contentBackgroundColor = contentBackgroundColor;
    self.multiScrollView.backgroundColor = contentBackgroundColor;
}

- (void)setDataSource:(NSMutableArray *)dataSource {
    _dataSource = dataSource;
    if (kValidArray(_dataSource)) {
        [_dataSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YYMultiItemScrollModel *itemModel = (YYMultiItemScrollModel *)obj;
            if (![self.identifierDatas containsObject:itemModel.identifierName]) {
                [self.identifierDatas addObject:itemModel.identifierName];
            }
        }];
        for (int i = 0; i < self.identifierDatas.count; i ++) {
            NSString *identifier = [self.identifierDatas objectAtIndex:i];
            [self.multiScrollView registerClass:NSClassFromString(identifier)
                     forCellWithReuseIdentifier:identifier];
        }
        self.itemDatas = _dataSource;
        [self.multiScrollView reloadData];
    }
}

#pragma mark - Public
- (UICollectionViewCell *)multiView:(YYMultiItemScrollView *)multiView
             cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (!multiView || !indexPath) {
        return nil;
    }
   return [self.multiScrollView cellForItemAtIndexPath:indexPath];
}

- (NSMutableArray *)allItemOfMultiView {
    NSMutableArray *items = [NSMutableArray array];
    for (int i = 0; i < self.itemDatas.count; i ++) {
        NSIndexPath *currentPath = [NSIndexPath indexPathForRow:i
                                                      inSection:0];
        [items addObject:[self multiView:self
                  cellForItemAtIndexPath:currentPath]];
    }
    return items;
}


#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemDatas = [NSMutableArray array];
        self.identifierDatas = [NSMutableArray array];
        [self addSubview:self.multiScrollView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    self.multiScrollView.frame = self.bounds;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.itemDatas.count;//> 0 ? 1 : 0
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    YYMultiItemScrollBaseCell *baseCell = nil;
    if (row < self.itemDatas.count) {
        YYMultiItemScrollModel *itemModel = [self.itemDatas objectAtIndex:row];
        baseCell = [collectionView dequeueReusableCellWithReuseIdentifier:itemModel.identifierName
                                                             forIndexPath:indexPath];
        [baseCell setViewModel:itemModel];
    }
    return baseCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    if (self.multiScrollDelegate && [self.multiScrollDelegate respondsToSelector:@selector(multiScrollView:didSelectItemAtIndexPath:)]) {
        [self.multiScrollDelegate multiScrollView:self
                         didSelectItemAtIndexPath:indexPath];
    }
    
}
@end
