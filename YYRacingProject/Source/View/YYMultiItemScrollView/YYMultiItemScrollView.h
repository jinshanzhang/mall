//
//  YYMultiItemScrollView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YYMultiItemScrollViewDelegate;

@interface YYMultiItemScrollView : UIView

@property (nonatomic, assign) UIEdgeInsets contentEdgeInsets;
@property (nonatomic, assign) CGFloat miniItemSpace;
@property (nonatomic, assign) CGFloat minimumLineSpacing;
@property (nonatomic, assign) CGSize  itemSize;
@property (nonatomic, assign) BOOL    scrollEnabled;
@property (nonatomic, assign) UICollectionViewScrollDirection scrollDirection;
@property (nonatomic, strong) UIColor *contentBackgroundColor;

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) id <YYMultiItemScrollViewDelegate> multiScrollDelegate;

- (UICollectionViewCell *)multiView:(YYMultiItemScrollView *)multiView
             cellForItemAtIndexPath:(NSIndexPath *)indexPath;

- (NSMutableArray *)allItemOfMultiView;
@end


@protocol YYMultiItemScrollViewDelegate <NSObject>

@optional
- (void)multiScrollView:(YYMultiItemScrollView *)scrollView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end



NS_ASSUME_NONNULL_END
