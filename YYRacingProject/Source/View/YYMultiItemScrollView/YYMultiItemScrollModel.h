//
//  YYMultiItemScrollBaseInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@protocol YYMultiItemScrollViewProtocol <NSObject>

@property (nonatomic, assign) NSInteger type;  //item类型。 根据model中的类型定义

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subTitle;

@property (nonatomic, strong) NSString *url;

@property (nonatomic, strong) NSString *imageUrl;   // 图片
@property (nonatomic, strong) NSString *originPrice;// 原价
@property (nonatomic, strong) NSString *nowPrice;   // 现价
@property (nonatomic, strong) NSString *couponPrice; // 券后价
@property (nonatomic, strong) NSString *commission; // 佣金
@property (nonatomic, strong) NSString *identifierName; //重用标识
@property (nonatomic, strong) NSNumber *saleStatus;   //特卖状态 1 未开始    2 预热    3在售中

@property (nonatomic, strong) NSNumber  *startTime;
@property (nonatomic, strong) NSNumber  *endTime;
@property (nonatomic, strong) NSNumber  *currentTime;

@property (nonatomic, strong) NSDictionary *skipLink; //跳转链接(包括类型，内容)
@end

@class YYPictureModel;
@class YYFavorUserModel;
@class FriendCircleTopicInfoModel;
@class HomeListItemGoodInfoModel;
@class HomeResourceItemInfoModel;

@interface YYMultiItemScrollModel : NSObject <YYMultiItemScrollViewProtocol>

- (instancetype)initWithPictureItem:(YYPictureModel *)pictureModel;
- (instancetype)initWithFavorUserItem:(YYFavorUserModel *)favorModel;
- (instancetype)initWithListGoodItem:(HomeListItemGoodInfoModel *)goodItem;
- (instancetype)initWithTopicCircleItem:(FriendCircleTopicInfoModel *)topicModel;
- (instancetype)initWithHomeResourceItem:(nullable HomeResourceItemInfoModel *)itemModel;

@end



NS_ASSUME_NONNULL_END
