//
//  YYMultiScrollView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@class HomeListStructInfoModel, HomeResourcesInfoModel;
@protocol YYMultiScrollViewDelegate <NSObject>

@optional
- (void)mulitiScrollViewDidSelect:(NSIndexPath *)indexPath;
- (void)mulitiScrollViewDidScroll:(UICollectionView *)collectionView;
- (void)mulitiScrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)mulitiScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

@end

@interface YYMultiScrollView : UIView

@property (nonatomic, strong) UICollectionView *brandListView;
@property (nonatomic, weak) id <YYMultiScrollViewDelegate> delegate;

@property (nonatomic, assign) YYMultiScrollModelType  modelType;
@property (nonatomic, strong) HomeResourcesInfoModel  *resourceModel;
@property (nonatomic, strong) HomeListStructInfoModel *structModel;

- (void)scrollItemArriveAssignLocation:(NSInteger)currentIndex
                             robCounts:(NSInteger)robCounts;
@end

NS_ASSUME_NONNULL_END
