//
//  YYPopShareView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPopShareDefine.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYPopShareView : UIView

@property (nonatomic, copy) void (^callBlock)(NSIndexPath *indexPath);

- (instancetype)initWithPopShareView:(NSDictionary *)headerParams
                            listData:(NSMutableArray *)listData;

- (void)popShow;
- (void)popHide;

@end

NS_ASSUME_NONNULL_END
