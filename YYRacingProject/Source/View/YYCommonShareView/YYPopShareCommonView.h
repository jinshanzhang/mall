//
//  YYPopShareCommonView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YYPopShareHeaderModel.h"
#import "YYMultiItemScrollModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol YYPopShareCommonDelegate;
@interface YYPopShareCommonView : UIView

@property (nonatomic, weak) id <YYPopShareCommonDelegate> delegate;
@property (nonatomic, assign) UICollectionViewScrollDirection direction;
- (instancetype)initWithShareView:(NSDictionary *)params
                         contents:(NSMutableArray <YYMultiItemScrollModel *>*)contents;

- (CGFloat)heightForPopShareCommonView;

@end

@protocol YYPopShareCommonDelegate <NSObject>
@optional
- (void)popShareCommonDidSelectAtIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
