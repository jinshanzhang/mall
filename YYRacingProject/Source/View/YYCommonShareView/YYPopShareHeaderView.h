//
//  YYPopShareHeaderView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YYPopShareDefine.h"
NS_ASSUME_NONNULL_BEGIN
@protocol  YYPopShareHeaderProtocol;
@interface YYPopShareHeaderView : UIView

- (void)setViewModel:(id <YYPopShareHeaderProtocol>)viewModel;
- (CGFloat)heightForHeaderView;

@end

NS_ASSUME_NONNULL_END
