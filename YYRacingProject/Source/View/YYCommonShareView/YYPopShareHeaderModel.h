//
//  YYPopShareHeaderModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/6.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol YYPopShareHeaderProtocol <NSObject>

@property (nonatomic, assign) NSInteger type;  //item类型。 根据model中的类型定义

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detailTitle;
@property (nonatomic, strong) NSString *imageUrl;   //图片

@property (nonatomic, assign) CGFloat  height;
@property (nonatomic, assign) CGFloat  width;

@end

@interface YYPopShareHeaderModel : NSObject <YYPopShareHeaderProtocol>


@end

NS_ASSUME_NONNULL_END
