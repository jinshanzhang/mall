//
//  YYPopShareDefine.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#ifndef YYPopShareDefine_h
#define YYPopShareDefine_h


#define kAnimateDuration  0.3
#define kLightAlpha 0.3

#define kShareWidth  [UIScreen mainScreen].bounds.size.width
#define kShareHeight [UIScreen mainScreen].bounds.size.height

#define kShareScale             [[NSString stringWithFormat:@"%.2f",kScreenW/375.0] floatValue]
#define kShareSizeScale(x)      kShareScale * x

#define kDefaultItemHeight      kShareSizeScale(80)
#define kFooterHeight           kShareSizeScale(55)
#define kDefaultHeaderHeight    kShareSizeScale(65)

#define kCloseShareViewNotification  @"kCloseShareViewNotification"

#endif /* YYPopShareDefine_h */
