//
//  YYPopShareItemView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/6.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPopShareItemView.h"
#import "YYMultiItemScrollModel.h"

@interface YYPopShareItemView ()

@property (nonatomic, strong) UIImageView *pictureView;
@property (nonatomic, strong) UILabel     *titleLabel;

@end


@implementation YYPopShareItemView

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    self.titleLabel.text = viewModel.title;
    if ([viewModel.imageUrl isKindOfClass:[NSString class]]) {
        if ([viewModel.imageUrl isValidUrl]) {
            [self.pictureView sd_setImageWithURL:[NSURL URLWithString:viewModel.imageUrl]];
        }
        else {
            self.pictureView.image = [UIImage imageNamed:viewModel.imageUrl];
        }
    }
}

- (void)createUI {
    self.pictureView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.pictureView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = HexRGB(0x222222);
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:self.titleLabel];
    
    [self.pictureView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.centerX.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kShareSizeScale(40), kShareSizeScale(40)));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.pictureView.mas_bottom).offset(kShareSizeScale(8));
    }];
}

@end
