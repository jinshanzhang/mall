//
//  YYPopShareItemView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/6.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYMultiItemScrollBaseCell.h"

#import "YYPopShareDefine.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYPopShareItemView : YYMultiItemScrollBaseCell

@end

NS_ASSUME_NONNULL_END
