//
//  YYPopShareFooterView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPopShareDefine.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYPopShareFooterView : UIView

@property (nonatomic, copy) void (^cancelBlock)(void);
- (CGFloat)heightForFooterView;

@end

NS_ASSUME_NONNULL_END
