//
//  YYPopShareView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPopShareView.h"

#import "YYPopShareCommonView.h"
@interface YYPopShareView ()
<YYPopShareCommonDelegate>
@property (nonatomic, strong) UIView    *lightBackgroundView;
@property (nonatomic, strong) YYPopShareCommonView *shareView;
@property (nonatomic, strong) UIWindow  *currentWindow;

@end

@implementation YYPopShareView

#pragma mark - Life cycle
- (instancetype)initWithPopShareView:(NSDictionary *)headerParams
                            listData:(NSMutableArray *)listData {
    self = [self initWithFrame:CGRectZero];
    if (self) {
        self.shareView = [[YYPopShareCommonView alloc] initWithShareView:headerParams
                                                                contents:listData];
        self.shareView.delegate = self;
        self.shareView.frame = CGRectMake(0, kShareHeight, kShareWidth, [self.shareView heightForPopShareCommonView]);
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setter method
- (UIView *)lightBackgroundView {
    if (!_lightBackgroundView) {
        _lightBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kShareWidth, kShareHeight)];
        _lightBackgroundView.backgroundColor = [UIColor blackColor];
        _lightBackgroundView.alpha = 0;
        
        // 添加手势监听
        UITapGestureRecognizer *closeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideShareView:)];
        [_lightBackgroundView addGestureRecognizer:closeGesture];
    }
    return _lightBackgroundView;
}

#pragma mark - Public method
- (void)popShow {
    [self addToCurrentWindow];
    [self showViewAnimationWithCompletion:nil];
}

- (void)popHide {
    [self hideViewAnimationWithCompletion:^(BOOL finish) {
        [self removeFromCurrentWindow];
    }];
}


#pragma mark - Private method
/**
 初始化视图
 **/
- (void)createUI {
    self.frame = CGRectMake(0, 0, kShareWidth, kShareHeight);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideShareView:) name:kCloseShareViewNotification object:nil];
}

/**
 添加到window上
 **/
- (void)addToCurrentWindow {
    if (!self.superview) {
        UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
        [keyWindow addSubview:self];
        
        [self addSubview:self.lightBackgroundView];
        [self addSubview:self.shareView];
    }
}

/**
 从父视图上移除
 **/
- (void)removeFromCurrentWindow {
    if (self.superview) {
        [self.lightBackgroundView removeFromSuperview];
        self.lightBackgroundView = nil;
        
        [self.shareView removeFromSuperview];
        self.shareView = nil;
        [self removeFromSuperview];
    }
}

/**
 显示动画
 **/
- (void)showViewAnimationWithCompletion:(void (^)(BOOL finish))completionBlock {
    [UIView animateWithDuration:kAnimateDuration
                     animations:^{
                         self.lightBackgroundView.alpha = kLightAlpha;
                         
                         CGRect frame = self.shareView.frame;
                         frame.origin.y = kShareHeight - self.shareView.height;
                         self.shareView.frame = frame;
                         
                     } completion:completionBlock];
}

/**
 隐藏动画
 **/
- (void)hideViewAnimationWithCompletion:(void (^)(BOOL finish))completionBlock {
    [UIView animateWithDuration:kAnimateDuration
                     animations:^{
                         self.lightBackgroundView.alpha = 0;
                         
                         CGRect frame = self.shareView.frame;
                         frame.origin.y = kShareHeight;
                         self.shareView.frame = frame;
                         
                     } completion:completionBlock];
}

#pragma mark - Event
- (void)hideShareView:(UITapGestureRecognizer *)gesture {
    if (self.callBlock) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        self.callBlock(indexPath);
    }
}


#pragma mark - YYPopShareCommonDelegate
- (void)popShareCommonDidSelectAtIndexPath:(NSIndexPath *)indexPath {
    if (self.callBlock) {
        self.callBlock(indexPath);
    }
}

@end
