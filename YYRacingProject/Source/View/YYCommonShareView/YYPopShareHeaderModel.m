//
//  YYPopShareHeaderModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/6.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPopShareHeaderModel.h"

@implementation YYPopShareHeaderModel

@synthesize type = _type;
@synthesize title = _title;
@synthesize detailTitle = _detailTitle;
@synthesize imageUrl = _imageUrl;
@synthesize height = _height;
@synthesize width = _width;


@end
