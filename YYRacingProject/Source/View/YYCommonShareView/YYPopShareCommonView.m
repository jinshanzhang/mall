//
//  YYPopShareCommonView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPopShareCommonView.h"

#import "YYMultiItemScrollView.h"
#import "YYPopShareHeaderView.h"
#import "YYPopShareFooterView.h"

@interface YYPopShareCommonView ()
<YYMultiItemScrollViewDelegate>
@property (nonatomic, strong) YYMultiItemScrollView *scrollView;
@property (nonatomic, strong) YYPopShareHeaderView *headerView;
@property (nonatomic, strong) YYPopShareFooterView *footerView;

@property (nonatomic, assign) CGFloat totalHeight;
@property (nonatomic, strong) NSMutableDictionary *headerParams;
@property (nonatomic, strong) NSMutableArray *itemListData;
@end

@implementation YYPopShareCommonView

#pragma mark - Setter method
- (YYMultiItemScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[YYMultiItemScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.miniItemSpace = 0.0;
        _scrollView.minimumLineSpacing = 0.0;
        _scrollView.multiScrollDelegate = self;
    }
    return _scrollView;
}

- (YYPopShareHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[YYPopShareHeaderView alloc] initWithFrame:CGRectZero];
    }
    return _headerView;
}

- (YYPopShareFooterView *)footerView {
    @weakify(self);
    if (!_footerView) {
        _footerView = [[YYPopShareFooterView alloc] initWithFrame:CGRectZero];
        _footerView.cancelBlock = ^{
            @strongify(self);
            if (self.delegate && [self.delegate respondsToSelector:@selector(popShareCommonDidSelectAtIndexPath:)]) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
                [self.delegate popShareCommonDidSelectAtIndexPath:indexPath];
            }
        };
    }
    return _footerView;
}

#pragma mark - Life cycle
- (instancetype)initWithShareView:(NSDictionary *)params contents:(NSMutableArray<YYMultiItemScrollModel *> *)contents {
    self = [self initWithFrame:CGRectZero];
    if (self) {
        self.totalHeight = 0.0;
        self.headerParams = [NSMutableDictionary dictionary];
        self.itemListData = [NSMutableArray array];
        
        if (params.count > 0) {
            self.headerParams = [params mutableCopy];
        }
        if (contents.count > 0) {
            self.itemListData = contents;
        }
        [self evaluationSubView];
        [self setSubViewsLayout];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

#pragma mark - Public
- (CGFloat)heightForPopShareCommonView {
    return self.totalHeight;
}

#pragma mark - Private
- (void)createUI {
    [self addSubview:self.headerView];
    [self addSubview:self.scrollView];
    [self addSubview:self.footerView];
}

- (void)evaluationSubView {
    if ([self.headerParams objectForKey:@"headerModel"]) {
        YYPopShareHeaderModel *model = [self.headerParams objectForKey:@"headerModel"];
        [self.headerView setViewModel:model];
    }
    self.scrollView.dataSource = self.itemListData;
    [self setSubViewsLayout];
}

- (void)setSubViewsLayout {
    self.totalHeight = 0.0;
    NSInteger count = self.itemListData.count;
    CGFloat height = 0.0, itemHeight = 0.0, itemWidth = 0.0;
    if (self.direction == UICollectionViewScrollDirectionHorizontal) {
        itemHeight = kDefaultItemHeight;
        height = itemHeight;
    }
    else {
        if (count <= 4) {
            itemHeight = kDefaultItemHeight;
        }
        else {
            itemHeight = kShareSizeScale(70);
        }
        height = itemHeight * ceilf(count/4.0);
    }
    if (count >= 4) {
        itemWidth = kScreenW/4;
    }
    else {
        itemWidth = kScreenW/count;
    }
    CGRect scrollFrame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame)-kSizeScale(0.5), kScreenW, height);
    self.scrollView.itemSize = CGSizeMake(itemWidth, itemHeight);
    self.scrollView.frame = scrollFrame;
    
    CGRect footerFrame = self.footerView.frame;
    footerFrame.origin.y = self.scrollView.bottom;
    self.footerView.frame = footerFrame;
    
    self.totalHeight = self.footerView.bottom;
}


#pragma mark - YYMultiItemScrollViewDelegate
- (void)multiScrollView:(YYMultiItemScrollView *)scrollView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(popShareCommonDidSelectAtIndexPath:)]) {
        NSIndexPath *tempPath = [NSIndexPath indexPathForItem:indexPath.item+1 inSection:0];
        [self.delegate popShareCommonDidSelectAtIndexPath:tempPath];
    }
}
@end
