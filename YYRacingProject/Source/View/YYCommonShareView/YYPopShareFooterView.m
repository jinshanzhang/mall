//
//  YYPopShareFooterView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPopShareFooterView.h"

@interface YYPopShareFooterView ()

@property (nonatomic, strong) UIView   *topLineView;
@property (nonatomic, strong) UIButton *cancelButton;

@end

@implementation YYPopShareFooterView

#pragma mark - Setter method
- (UIView *)topLineView {
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = HexRGB(0xfafafa);
    }
    return _topLineView;
}

- (UIButton *)cancelButton {
    if (!_cancelButton) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:17];
        _cancelButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_cancelButton setTitleColor:HexRGB(0x222222) forState:UIControlStateNormal];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(cancelClickEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, kShareWidth, kFooterHeight);
        [self createUI];
        [self setSubViewLayout];
    }
    return self;
}

#pragma mark - Private method
- (void)createUI {
    self.backgroundColor = XHWhiteColor;
    [self addSubview:self.topLineView];
    [self addSubview:self.cancelButton];
}

- (void)setSubViewLayout {
    CGFloat lineHeight = 1;
    CGRect lineFrame = CGRectMake(self.left, self.top, self.width, lineHeight);
    self.topLineView.frame = lineFrame;
    
    CGRect cancelFrame = CGRectMake(self.topLineView.left, (self.topLineView.height +self.topLineView.top), self.topLineView.width, self.height-lineHeight);
    self.cancelButton.frame = cancelFrame;
}

#pragma mark - Public method
- (CGFloat)heightForFooterView {
    return kFooterHeight;
}

#pragma mark - Event
- (void)cancelClickEvent {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
@end
