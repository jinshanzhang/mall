//
//  YYPopShareHeaderView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/5.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPopShareHeaderView.h"
#import "YYPopShareHeaderModel.h"

#define kTopPadding kShareSizeScale(26)
@interface YYPopShareHeaderView()

@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, assign) CGFloat  totalHeight;

@end

@implementation YYPopShareHeaderView
#pragma mark - Setter method
- (void)setViewModel:(id<YYPopShareHeaderProtocol>)viewModel {
    if (viewModel.imageUrl.length > 0) {
        if ([viewModel.imageUrl isKindOfClass:[NSString class]]) {
            if ([viewModel.imageUrl isValidUrl]) {
                [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:viewModel.imageUrl]];
            }
            else {
                self.contentImageView.image = [UIImage imageNamed:viewModel.imageUrl];
            }
            self.totalHeight = viewModel.height;
            CGRect tempFrame = self.frame;
            tempFrame.size.height = viewModel.height;
            self.frame = tempFrame;
            self.contentImageView.hidden = NO;
        }
    }
    else {
        self.titleLabel.text = viewModel.title;
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.totalHeight = kDefaultHeaderHeight;
        self.frame = CGRectMake(0, 0, kShareWidth, kDefaultHeaderHeight);
        [self createUI];
    }
    return self;
}

#pragma mark - Private
- (void)createUI {
    self.backgroundColor = XHWhiteColor;
    self.contentImageView = [[UIImageView alloc] init];
    self.contentImageView.hidden = YES;
    [self addSubview:self.contentImageView];

    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = HexRGB(0x999999);
    self.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:self.titleLabel];
    
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(kTopPadding);
    }];
}

#pragma marl - Public
- (CGFloat)heightForHeaderView {
    return self.totalHeight;
}

@end
