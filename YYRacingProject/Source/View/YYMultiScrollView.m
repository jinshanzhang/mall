//
//  YYMultiScrollView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYMultiScrollView.h"

#import "MultiScrollSuperInfoCell.h"
#import "MultiScrollShowInfoCell.h"
#import "MultiScrollSuperEmptyInfoCell.h"

#import "HomeResourcesInfoModel.h"
#import "HomeListStructInfoModel.h"

@interface YYMultiScrollView ()
<UICollectionViewDelegate,
UICollectionViewDataSource>

@property (nonatomic, assign) BOOL             isItemSwitch;
@property (nonatomic, assign) CGFloat          itemSizeWith;
@property (nonatomic, assign) YYSingleRowScrollType scrollType;

@end

@implementation YYMultiScrollView

#pragma mark - Setter
- (UICollectionView *)brandListView {
    if (!_brandListView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumInteritemSpacing = 0.0f;
        flowLayout.sectionInset = UIEdgeInsetsMake(0, kSizeScale(12), 0, kSizeScale(12));
        
        _brandListView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                            collectionViewLayout:flowLayout];
        _brandListView.delegate = self;
        _brandListView.dataSource = self;
        _brandListView.scrollsToTop = NO;
        _brandListView.showsHorizontalScrollIndicator = NO;
        _brandListView.backgroundColor = XHClearColor;
        Class cls = [MultiScrollShowInfoCell class];
        [_brandListView registerClass:cls forCellWithReuseIdentifier:strFromCls(cls)];
        cls = [MultiScrollSuperInfoCell class];
        [_brandListView registerClass:cls forCellWithReuseIdentifier:strFromCls(cls)];
        cls = [MultiScrollSuperEmptyInfoCell class];
        [_brandListView registerClass:cls forCellWithReuseIdentifier:strFromCls(cls)];
    }
    return _brandListView;
}

- (void)setStructModel:(HomeListStructInfoModel *)structModel {
    _structModel = structModel;
    if (_structModel) {
        self.scrollType = self.structModel.scrollType;
        [_brandListView reloadData];
    }
}

- (void)setResourceModel:(HomeResourcesInfoModel *)resourceModel {
    _resourceModel = resourceModel;
    if (_resourceModel) {
        self.scrollType = self.resourceModel.scrollType;
        [_brandListView reloadData];
    }
}

- (void)scrollItemArriveAssignLocation:(NSInteger)currentIndex
                             robCounts:(NSInteger)robCounts {
    NSInteger count = 0;
    if (self.scrollType == YYSingleRowScrollSuperHot) {
        count = self.resourceModel.items.count;
    }
    else {
        count = self.structModel.items.count;
    }
    if (currentIndex >= 0) {
        if (currentIndex == 0) {
            [self.brandListView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else {
            if (robCounts <= 2) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:count-1 inSection:0];
                [self.brandListView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
            }
            else
            {
                float offsetX = self.itemSizeWith * currentIndex + kSizeScale(8) * (currentIndex -1) + kSizeScale(12);
                [self.brandListView setContentOffset:CGPointMake(offsetX, 0) animated:YES];
            }
        }
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isItemSwitch = NO;
        self.itemSizeWith = 0.0;
        
        [self addSubview:self.brandListView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.brandListView.frame = self.bounds;
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.modelType == YYMultiScrollModelGoodType) {
        return self.structModel.items.count;
    }
    else {
        return self.resourceModel.items.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class current = [MultiScrollShowInfoCell class];
    if (self.scrollType == YYSingleRowScrollSuperHot) {
        HomeResourceItemInfoModel *itemModel = nil;
        if (row < self.resourceModel.items.count) {
            itemModel = [self.resourceModel.items objectAtIndex:row];
        }
        if (itemModel.scrollType == YYSingleRowScrollEmpty) {
            current = [MultiScrollSuperEmptyInfoCell class];
            MultiScrollSuperEmptyInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
            return cell;
        }
        else {
            current = [MultiScrollSuperInfoCell class];
            MultiScrollSuperInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
            cell.itemModel = itemModel;
            return cell;
        }
    }
    else {
        MultiScrollShowInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
        if (row < self.structModel.items.count && kValidArray(self.structModel.items) && self.modelType == YYMultiScrollModelGoodType) {
            cell.itemModel = [self.structModel.items objectAtIndex:row];
        }
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize sizeValue = CGSizeZero;
    if (self.scrollType == YYSingleRowScrollGoodRecommend) {
        sizeValue = CGSizeMake(kSizeScale(100), self.height);
    }
    else if (self.scrollType == YYSingleRowScrollHomeBrand) {
        sizeValue = CGSizeMake(kSizeScale(90), self.height);
    }
    else if (self.scrollType == YYSingleRowScrollSuperHot) {
        sizeValue = CGSizeMake(kSizeScale(120), self.height);
    }
    self.itemSizeWith = sizeValue.width;
    return sizeValue;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kSizeScale(8);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(mulitiScrollViewDidSelect:)]) {
        [self.delegate mulitiScrollViewDidSelect:indexPath];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!self.isItemSwitch) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(mulitiScrollViewDidScroll:)]) {
            [self.delegate mulitiScrollViewDidScroll:self.brandListView];
        }
    }
    self.isItemSwitch = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (self.delegate && [self.delegate respondsToSelector:@selector(mulitiScrollViewDidEndDragging:willDecelerate:)]) {
        [self.delegate mulitiScrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(mulitiScrollViewDidEndDecelerating:)]) {
        [self.delegate mulitiScrollViewDidEndDecelerating:scrollView];
    }
}
@end
