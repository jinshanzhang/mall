//
//  BrandShowInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "MultiScrollShowInfoCell.h"

#import "HomeListItemGoodInfoModel.h"

@interface MultiScrollShowInfoCell ()
@property (nonatomic, strong) UIView      *brandBackgroundView;
@property (nonatomic, strong) UIImageView *brandImageView;

@property (nonatomic, strong) UIImageView *goodImageView;

@property (nonatomic, strong) YYLabel     *goodTitleLabel;
@property (nonatomic, strong) UILabel     *goodPriceLabel;
@property (nonatomic, strong) UILabel     *extraLabel;
@end

@implementation MultiScrollShowInfoCell

#pragma mark - Setter
- (void)setItemModel:(HomeListItemGoodInfoModel *)itemModel {
    _itemModel = itemModel;
    if (_itemModel.scrollType == YYSingleRowScrollGoodRecommend) {
        [self goodDetailLayout];
        [self.goodImageView yy_sdWebImage:_itemModel.coverImage.imageUrl placeholderImageType:YYPlaceholderImageListGoodType];
        self.goodTitleLabel.text = _itemModel.title;
        [self goodDetailPriceShow];
    }
    else if (_itemModel.scrollType == YYSingleRowScrollHomeBrand) {
        [self brandGoodLayout];
        [self.brandImageView yy_sdWebImage:_itemModel.coverImage.imageUrl placeholderImageType:YYPlaceholderImageListGoodType];
        [self goodDetailPriceShow];
    }
    else {
        [self emptyStyleLayout:NO];
        self.brandImageView.image = [UIImage imageNamed:@"look_more_icon"];
        self.goodPriceLabel.text = self.extraLabel.text = nil;
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.brandBackgroundView = [YYCreateTools createView:XHWhiteColor];
    self.brandBackgroundView.layer.borderWidth = kSizeScale(1);
    self.brandBackgroundView.layer.borderColor = XHLightColor.CGColor;
    [self addSubview:self.brandBackgroundView];
    
    self.brandImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.brandBackgroundView addSubview:self.brandImageView];
    
    self.goodImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self addSubview:self.goodImageView];
    
    self.goodTitleLabel = [YYCreateTools createLabel:nil
                                               font:normalFont(12)
                                          textColor:XHBlackColor
                                           maxWidth:self.width
                                      fixLineHeight:kSizeScale(17)];
    self.goodTitleLabel.numberOfLines = 2;
    [self addSubview:self.goodTitleLabel];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(12)
                                           textColor:XHBlackColor];
    [self addSubview:self.goodPriceLabel];

    self.extraLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(12)
                                       textColor:XHRedColor];
    [self addSubview:self.extraLabel];
}

#pragma mark - Private
/**品牌布局**/
- (void)brandGoodLayout {
    [self.brandBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(8));
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(90), kSizeScale(90)));
    }];
    
    [self.brandImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.brandBackgroundView);
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.brandBackgroundView);
        make.bottom.equalTo(self.mas_bottom).offset(-kSizeScale(8));
    }];
    
    /*[self.extraLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(6));
        make.centerY.equalTo(self.goodPriceLabel);
    }];*/
}

/**商品详情布局**/
- (void)goodDetailLayout {
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.mas_equalTo(kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(100), kSizeScale(100)));
    }];
    
    [self.goodTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.goodImageView);
        make.top.equalTo(self.goodImageView.mas_bottom).offset(kSizeScale(5));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.goodTitleLabel);
        make.bottom.equalTo(self);
    }];
    /*[self.extraLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(6));
        make.centerY.equalTo(self.goodPriceLabel);
    }];*/
}


/**空白布局**/
- (void)emptyStyleLayout:(BOOL)isSuperHot {
    [self.brandBackgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(isSuperHot?kSizeScale(0):kSizeScale(8));
        make.centerX.equalTo(self);
        make.size.mas_equalTo(isSuperHot?CGSizeMake(self.width, self.width):CGSizeMake(kSizeScale(90), kSizeScale(90)));
    }];
    
    [self.brandImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.brandBackgroundView);
    }];
}

/**商品价格佣金展示**/
- (void)goodDetailPriceShow {
    
    BOOL isNormalLogic = YES;
    BOOL isHideExtra = NO;
    CGFloat pictureWidth = 0.0;
    UIColor *reservePriceColor = XHBlackColor;
    UIColor *originPriceColor = XHRedColor;
    NSMutableAttributedString *reservePrice = nil;
    NSMutableAttributedString *originPrice = nil;
    CGRect reserveRect = CGRectZero, originRect = CGRectZero;

    if (_itemModel.scrollType == YYSingleRowScrollGoodRecommend) {
        pictureWidth = kSizeScale(100);
    }
    else if (_itemModel.scrollType == YYSingleRowScrollHomeBrand) {
        pictureWidth = kSizeScale(90);
    }
    if (kIsFan) {
        reservePriceColor = ([_itemModel.saleStatus integerValue] == 2 ? HexRGB(0x11BC56) : XHRedColor);
    }
    originPriceColor = ([_itemModel.saleStatus integerValue] == 2 ? HexRGB(0x11BC56) : XHRedColor);
    
    reservePrice = [[NSMutableAttributedString alloc] init];
    if (_itemModel.couponLogic) {
        // 是否开启券的逻辑
        NSInteger couponFlag = [_itemModel.couponFlag integerValue];
        NSInteger couponLogic = [_itemModel.couponLogic integerValue];
        if (couponLogic == 1 && couponFlag == 1) {
            reservePrice = [YYCommonTools containSpecialSymbolHandler:@"券后¥" symbolFont:boldFont(11) symbolTextColor:reservePriceColor wordSpace:1 price:_itemModel.couponPrice priceFont:boldFont(12) priceTextColor: reservePriceColor symbolOffsetY:0];
            isNormalLogic = NO;
        }
    }
    if (isNormalLogic) {
        reservePrice = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(12) symbolTextColor:reservePriceColor wordSpace:2 price:_itemModel.reservePrice priceFont:boldFont(12) priceTextColor: reservePriceColor symbolOffsetY:0];
    }
    
    self.goodPriceLabel.attributedText = reservePrice;
    
    if (kIsFan) {
        originPrice = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",_itemModel.originPrice]];
        [originPrice yy_setAttributes:XHBlackLitColor
                            font:midFont(12)
                         content:originPrice
                       alignment:NSTextAlignmentCenter];
        [originPrice yy_setAttributeOfDeleteLine:NSMakeRange(0, originPrice.length)];
    }
    else {
        originPrice = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(10) symbolTextColor:originPriceColor wordSpace:2 price:_itemModel.commission priceFont:boldFont(12) priceTextColor:originPriceColor symbolOffsetY:1.2];
    }
    if (reservePrice) {
        reserveRect = [reservePrice boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    }
    if (originPrice) {
        originRect = [originPrice boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    }
    if (originRect.size.width + reserveRect.size.width + kSizeScale(12) > pictureWidth) {
        isHideExtra = YES;
    }
    if (!isHideExtra) {
        [reservePrice appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "]];
        [reservePrice appendAttributedString:originPrice];
        NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
        [reservePrice yy_setAttributes:attributes
                               content:reservePrice
                             alignment:NSTextAlignmentCenter];
    }
    self.goodPriceLabel.attributedText = reservePrice;
}

@end
