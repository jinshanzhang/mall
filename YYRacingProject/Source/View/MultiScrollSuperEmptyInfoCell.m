//
//  MultiScrollSuperEmptyInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/19.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "MultiScrollSuperEmptyInfoCell.h"

@interface MultiScrollSuperEmptyInfoCell()

@property (nonatomic, strong) UIImageView *brandImageView;

@end

@implementation MultiScrollSuperEmptyInfoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    CGFloat cellWidth = kSizeScale(120);
    self.brandImageView = [YYCreateTools createImageView:@"home_look_more_icon"
                                               viewModel:-1];
    [self.contentView addSubview:self.brandImageView];
    [self.brandImageView zy_cornerRadiusAdvance:2 rectCornerType:UIRectCornerTopLeft|UIRectCornerTopRight];
    [self.brandImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(cellWidth);
    }];
}

@end
