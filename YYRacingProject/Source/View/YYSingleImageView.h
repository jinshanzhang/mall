//
//  YYSingleView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@protocol YYSingleImageViewDelegate <NSObject>

@optional
- (void)singleImageViewDidSelect;

@end

@interface YYSingleImageView : UIView

@property (nonatomic, weak) id <YYSingleImageViewDelegate> delegate;
@property (nonatomic, strong) id <PhotoViewProtocol> coverImage;

@end

NS_ASSUME_NONNULL_END
