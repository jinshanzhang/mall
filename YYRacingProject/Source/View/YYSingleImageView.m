//
//  YYSingleView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYSingleImageView.h"

@interface YYSingleImageView()

@property (nonatomic, strong) UIImageView *hotImageView;

@end

@implementation YYSingleImageView

#pragma mark - Setter
- (void)setCoverImage:(id<PhotoViewProtocol>)coverImage {
    _coverImage = coverImage;
    if (_coverImage) {
        [self.hotImageView yy_sdWebImage:_coverImage.imageUrl
                    placeholderImageType:YYPlaceholderImageHomeBannerType];
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewClickEvent:)]];
    }
    return self;
}

- (void)createUI {
    self.hotImageView = [YYCreateTools createImageView:nil
                                             viewModel:-1];
    self.hotImageView.clipsToBounds = YES;
    self.hotImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.hotImageView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.hotImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark - Event
- (void)viewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(singleImageViewDidSelect)]) {
        [self.delegate singleImageViewDidSelect];
    }
}
@end
