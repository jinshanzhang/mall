//
//  HomeSuperHotInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/11.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeResourceItemInfoModel;
NS_ASSUME_NONNULL_BEGIN

@interface MultiScrollSuperInfoCell : UICollectionViewCell

@property (nonatomic, strong) HomeResourceItemInfoModel *itemModel;

@end

NS_ASSUME_NONNULL_END
