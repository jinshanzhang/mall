//
//  HomeSuperHotInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/11.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "MultiScrollSuperInfoCell.h"
#import "yyCountDownManager.h"
#import "HomeResourceItemInfoModel.h"

@interface MultiScrollSuperInfoCell()

@property (nonatomic, strong) UIView      *brandBackgroundView;
@property (nonatomic, strong) UIImageView *brandImageView;
@property (nonatomic, strong) UIImageView *leftMarkImageView;
@property (nonatomic, strong) UIImageView *centerMarkImageView;
@property (nonatomic, strong) YYLabel     *superHotTitleLabel;
@property (nonatomic, strong) UILabel     *superHotPriceLabel;
@property (nonatomic, strong) UILabel     *couponPriceLabel;
@property (nonatomic, strong) UILabel     *sellingPointLabel; //卖点
@property (nonatomic, strong) UIImageView *countDownBgView;   //倒计时背景
@property (nonatomic, strong) YYLabel     *countDownTimeLabel;

@end


@implementation MultiScrollSuperInfoCell

#pragma mark - Setter
- (void)setItemModel:(HomeResourceItemInfoModel *)itemModel {
    _itemModel = itemModel;
    if (_itemModel) {
        if (_itemModel.scrollType == YYSingleRowScrollSuperHot) {
            [self setSuperContent];
        }
    }
}
#pragma mark - Life cycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:YYCountDownNotification object:nil];
        [self createUI];
    }
    return self;
}


#pragma mark - Private

- (void)createUI {
    CGFloat cellWidth = kSizeScale(120);
    self.brandBackgroundView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.brandBackgroundView];
    
    self.brandImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.brandBackgroundView addSubview:self.brandImageView];
    [self.brandImageView zy_cornerRadiusAdvance:5 rectCornerType:UIRectCornerAllCorners];
    
    self.countDownBgView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    //self.countDownBgView = [YYCreateTools createView:HexRGB(0xFFEAED)];
    //self.countDownBgView.hidden = YES;
    [self.brandBackgroundView addSubview:self.countDownBgView];
    
    self.countDownTimeLabel = [YYCreateTools createLabel:nil
                                                    font:normalFont(9)
                                               textColor:XHRedColor
                                                maxWidth:self.width
                                           fixLineHeight:kSizeScale(11)];
    self.countDownTimeLabel.hidden = YES;
    self.countDownTimeLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    self.countDownTimeLabel.textAlignment = NSTextAlignmentLeft;
    [self.brandBackgroundView addSubview:self.countDownTimeLabel];
    
    self.sellingPointLabel = [YYCreateTools createLabel:nil
                                                   font:normalFont(9)
                                              textColor:XHWhiteColor];
    self.sellingPointLabel.numberOfLines = 1;
    self.sellingPointLabel.v_cornerRadius = 7;
    self.sellingPointLabel.hidden = YES;
    self.sellingPointLabel.textAlignment = NSTextAlignmentCenter;
    [self.brandBackgroundView addSubview:self.sellingPointLabel];
    
    self.superHotTitleLabel = [YYCreateTools createLabel:nil
                                                    font:normalFont(12)
                                               textColor:XHBlackColor
                                                maxWidth:self.width
                                           fixLineHeight:kSizeScale(17)];
    self.superHotTitleLabel.numberOfLines = 1;
    [self.brandBackgroundView addSubview:self.superHotTitleLabel];
    
    self.superHotPriceLabel = [YYCreateTools createLabel:nil
                                                    font:boldFont(12)
                                               textColor:XHRedColor];
    [self.brandBackgroundView addSubview:self.superHotPriceLabel];
    
    self.leftMarkImageView = [YYCreateTools createImageView:nil
                                                  viewModel:-1];
    [self.leftMarkImageView zy_cornerRadiusAdvance:5 rectCornerType:UIRectCornerTopLeft];
    self.leftMarkImageView.hidden = YES;
    [self.brandBackgroundView addSubview:self.leftMarkImageView];
    
    self.centerMarkImageView = [YYCreateTools createImageView:@"count_down_end"
                                                    viewModel:-1];
    self.centerMarkImageView.hidden = YES;
    [self.brandBackgroundView addSubview:self.centerMarkImageView];
    
    self.couponPriceLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(12)
                                             textColor:XHRedColor];
    [self.brandBackgroundView addSubview:self.couponPriceLabel];
    
#pragma mark - layout
    [self.brandBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(188));
    }];
    
    [self.brandImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(cellWidth);
    }];
    
    [self.superHotTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.brandImageView.mas_left);
        make.right.equalTo(self.brandImageView.mas_right);
        make.top.equalTo(self.brandImageView.mas_bottom).offset(kSizeScale(4));
    }];
    
    [self.sellingPointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.superHotTitleLabel.mas_left);
        make.top.equalTo(self.superHotTitleLabel.mas_bottom).offset(kSizeScale(5));
        make.size.mas_equalTo(CGSizeZero);
    }];
    
    [self.couponPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sellingPointLabel.mas_left);
        make.bottom.equalTo(self.brandBackgroundView.mas_bottom);
    }];
    
    [self.superHotPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponPriceLabel.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.couponPriceLabel);
        //make.bottom.equalTo(self.couponPriceLabel.mas_bottom).offset(-kSizeScale(2.8));
    }];
    
    [self.leftMarkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.brandBackgroundView);
        make.size.mas_equalTo(CGSizeZero);
    }];
    
    [self.centerMarkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.brandBackgroundView);
        make.top.mas_equalTo(kSizeScale(28));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(65), kSizeScale(65)));
    }];
    
    [self.countDownBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.brandImageView);
        make.top.mas_equalTo(self.width-15);
        make.height.mas_equalTo(15.5);
    }];
    
    [self.countDownTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(22));
        make.top.bottom.equalTo(self.countDownBgView);
    }];
}

#pragma mark - Private
/**赋值**/
- (void)setSuperContent {
    BOOL  isSingleBrand = NO; //是否是单品
    BOOL  isWillRot = NO;
    BOOL  isLeftNotNull = NO;
    UIColor *reservePriceColor = XHBlackColor;
    UIColor *originPriceColor = XHRedColor;
    NSMutableAttributedString *reservePriceAttri = nil;
    NSMutableAttributedString *originPriceAttri = nil;
    CGRect reserveRect = CGRectZero, originRect = CGRectZero;
    
    // 布局调整
    isWillRot = ([_itemModel.saleStatus integerValue] == 2 ? YES : NO);
    isSingleBrand = ([_itemModel.superTrendyType integerValue] == 0? YES : NO);
    self.countDownBgView.hidden = self.countDownTimeLabel.hidden = NO;
    self.superHotTitleLabel.hidden =  self.sellingPointLabel.hidden = self.superHotPriceLabel.hidden = self.couponPriceLabel.hidden = !isSingleBrand;
    if (kValidString(_itemModel.rightIcon) && isSingleBrand) {
        isLeftNotNull = YES;
        self.leftMarkImageView.hidden = NO;
        CGSize leftSize = CGSizeMake(kSizeScale(24), kSizeScale(22));
        [self.leftMarkImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(leftSize);
        }];
    }
    else {
        self.leftMarkImageView.hidden = YES;
    }
    
    double leftTime = [_itemModel.endTime longValue] - [_itemModel.currentTime longValue];
    if (leftTime > 0) {
        self.centerMarkImageView.hidden = YES;
    }

    CGSize sellingPointSize = [YYCommonTools sizeWithText:_itemModel.sellpoint
                                                     font:normalFont(9)
                                                  maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    sellingPointSize.width = sellingPointSize.width + 12;
    sellingPointSize.height = kSizeScale(14);
    if (sellingPointSize.width > kSizeScale(110)) {
        sellingPointSize.width = kSizeScale(110);
    }
    self.sellingPointLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage gradientColorImageFromColors:@[HexRGB(0xCFA368), HexRGB(0xECB976)] gradientType:GradientTypeLeftToRight imgSize:sellingPointSize]];
    [self.sellingPointLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(sellingPointSize);
    }];

    [self.brandImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(isSingleBrand?kSizeScale(120):kSizeScale(188));
    }];
    
    // 赋值
    [self.brandImageView yy_sdWebImage:_itemModel.coverImage.imageUrl placeholderImageType:YYPlaceholderImageHotGoodType];
    
    if (isLeftNotNull) {
        [self.leftMarkImageView sd_setImageWithURL:[NSURL URLWithString:_itemModel.rightIcon]];
    }
    if (kIsFan) {
        reservePriceColor = (isWillRot ? HexRGB(0x11BC56) :XHRedColor);
    }
    originPriceColor = (isWillRot ? HexRGB(0x11BC56) :XHRedColor);
    
    if (isWillRot) {
        NSString *timeFormat = [NSString getPreHotShowTime:[_itemModel.startTime longValue]
                                               currentTime:[_itemModel.currentTime longValue]];
        UIImage *image = [UIImage imageNamed:@"home_rob_time_icon"];
        NSMutableAttributedString *postAttribute = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeScaleAspectFit attachmentSize:CGSizeMake(11.0, 11.0) alignToFont:normalFont(10) alignment:YYTextVerticalAlignmentCenter];
        [postAttribute appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@开抢",timeFormat]]];
        [postAttribute yy_setAttributes:XHBlackColor
                                   font:normalFont(9)
                              wordSpace:@(0)
                                content:postAttribute
                              alignment:NSTextAlignmentLeft];
        self.countDownTimeLabel.attributedText = postAttribute;
    }
    // 2 为即将开抢
    self.countDownTimeLabel.textColor = originPriceColor;
    [self.countDownBgView zy_cornerRadiusAdvance:(isSingleBrand?kSizeScale(5):kSizeScale(0)) rectCornerType:UIRectCornerBottomLeft|UIRectCornerBottomRight];
    self.countDownBgView.image = [UIImage imageWithColor:(isWillRot ?HexRGB(0xE9FFF2) : HexRGB(0xFFEAED))];
    
    if (isSingleBrand) {
        // 单品
        NSString *reservePrice = @"0";
        self.superHotTitleLabel.text = _itemModel.title;
        self.sellingPointLabel.text = _itemModel.sellpoint;
        if (kValidString(_itemModel.couponPrice) && [_itemModel.couponFlag integerValue] == 1) {
            reservePrice = [NSString stringWithFormat:@"%@",_itemModel.couponPrice];
        }
        else {
            reservePrice = [NSString stringWithFormat:@"%@",_itemModel.reservePrice];
        }
        if ([_itemModel.couponFlag integerValue] == 1) {
            reservePriceAttri = [YYCommonTools containSpecialSymbolHandler:@"券后¥" symbolFont:boldFont(10) symbolTextColor:reservePriceColor wordSpace:0 price:reservePrice priceFont:boldFont(14) priceTextColor:reservePriceColor symbolOffsetY:1.2];
        }
        else {
            reservePriceAttri = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(10) symbolTextColor:reservePriceColor wordSpace:1 price:reservePrice priceFont:boldFont(14) priceTextColor:reservePriceColor symbolOffsetY:1.2];
        }
        self.couponPriceLabel.attributedText = reservePriceAttri;
        
        if (kIsFan) {
            NSString *originPrice = @"¥0";
            if (kValidString(originPrice)) {
                originPrice = [NSString stringWithFormat:@"¥%@",_itemModel.originPrice];
            }
            originPriceAttri = [[NSMutableAttributedString alloc] initWithString:originPrice];
            [originPriceAttri yy_setAttributes:XHBlackLitColor
                                          font:boldFont(11)
                                       content:originPriceAttri
                                     alignment:NSTextAlignmentLeft];
            [originPriceAttri addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, originPriceAttri.length)];
            [originPriceAttri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle)range:NSMakeRange(0,originPriceAttri.length)];
            if (@available(iOS 10.3, *)) {
                [originPriceAttri addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)}range:NSMakeRange(0,originPriceAttri.length)];
            }
            self.superHotPriceLabel.attributedText = originPriceAttri;
        }
        else {
            NSString *commissionPrice = @"0";
            if (kValidString(_itemModel.commission)) {
                commissionPrice = [NSString stringWithFormat:@"%@",_itemModel.commission];
            }
            originPriceAttri = [YYCommonTools containSpecialSymbolHandler:@"赚¥" symbolFont:midFont(11) symbolTextColor:originPriceColor wordSpace:1 price:commissionPrice priceFont:boldFont(12) priceTextColor:originPriceColor symbolOffsetY:0];
            self.superHotPriceLabel.attributedText = originPriceAttri;
        }
        if (reservePriceAttri) {
            reserveRect = [reservePriceAttri boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        }
        if (originPriceAttri) {
            originRect = [originPriceAttri boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        }
        if (originRect.size.width + reserveRect.size.width + kSizeScale(12) > kSizeScale(120)) {
            self.superHotPriceLabel.hidden = YES;
        }
    }
}

#pragma mark - Event
/**倒计时**/
- (void)countDownNotification {
    if (_itemModel.scrollType == YYSingleRowScrollSuperHot) {
        BOOL  isWillRot = NO;
        UIColor *originPriceColor = XHRedColor;
        isWillRot = ([_itemModel.saleStatus integerValue] == 2 ? YES : NO);
        originPriceColor = (isWillRot ? HexRGB(0x11BC56) :XHRedColor);
        if (!isWillRot) {
            NSString *showContent = nil;
            CGFloat timeInterval = [kConfigCountDown timeIntervalWithIdentifier:@"superHot"];
            
            double leftTime = [_itemModel.endTime longValue] - [_itemModel.currentTime longValue];
            double differTime = leftTime - timeInterval;
            //正在开抢
            if (differTime <= 0) {
                showContent = @"已结束00:00:00.0";
                self.centerMarkImageView.hidden = NO;
            }
            else {
                NSInteger currentHour = (NSInteger)((differTime)/1000/60/60);
                NSInteger currentMinute = (NSInteger)((differTime)/1000/60)%60;
                NSInteger currentSeconds = ((NSInteger)(differTime))/1000%60;
                CGFloat   currentMsec = ((NSInteger)((differTime)))%1000/100;
                //小时数
                NSString *hours = [NSString stringWithFormat:@"%@", (currentHour < 10 ?[NSString stringWithFormat:@"0%ld",currentHour]:@(currentHour))];
                //分钟数
                NSString *minute = [NSString stringWithFormat:@"%@", (currentMinute < 10 ?[NSString stringWithFormat:@"0%ld",currentMinute]:@(currentMinute))];
                //秒数
                NSString *second = [NSString stringWithFormat:@"%@", (currentSeconds < 10 ?[NSString stringWithFormat:@"0%ld",currentSeconds]:@(currentSeconds))];
                //毫秒数
                NSString *msec = [NSString stringWithFormat:@"%.0f", currentMsec];
                showContent = [NSString stringWithFormat:@"距结束%@:%@:%@.%@",hours,minute,second,msec];
            }
            NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:showContent];
            [attribute yy_setAttributes:XHBlackColor
                                   font:normalFont(9)
                              wordSpace:@(0)
                                content:attribute
                              alignment:NSTextAlignmentLeft];
            self.countDownTimeLabel.attributedText = attribute;
            self.countDownTimeLabel.textColor = originPriceColor;
        }
    }
}


@end
