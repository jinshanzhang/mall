//
//  YYCompositeShareInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
// 合成图分享数据
@interface YYCompositeShareInfoModel : NSObject<ShareViewProtocol>

@property (nonatomic, copy) NSString *shopName;
@property (nonatomic, copy) NSString *shopAvatorUrl;

@property (nonatomic, copy) NSString *goodsTitle;
@property (nonatomic, copy) NSString *goodsShortTitle;
@property (nonatomic, copy) NSString *goodsShareImage;
@property (nonatomic, copy) NSString *qrcodePictureUrl;  //二维码图片URL

@property (nonatomic, copy) NSString *goodsShareBigImages;
@property (nonatomic, copy) NSString *shareUrl;
@property (nonatomic, copy) NSString *reservePrice;
@property (nonatomic, copy) NSString *originPrice;
@property (nonatomic, copy) NSString *commission;

@property (nonatomic, copy) NSString *discountPrice; //券后价，折扣价
@property (nonatomic, copy) NSString *maxDenomination; //最大券的面额

@property (nonatomic, strong) NSNumber *isNeedComposite;     //是否需要合成
@property (nonatomic, assign) NSInteger specialType; //0:普通特卖 1:新人特卖

@property (nonatomic, strong) NSNumber *itemSaleType;
@property (nonatomic, strong) NSNumber *specialStartSellDate;//特卖开始
@property (nonatomic, strong) NSNumber *specialEndSellDate;  //特卖结束

@property (nonatomic, copy) NSString * payment; //支付平台 0全部 1 支付宝微信 2 饭卡


@property (nonatomic, assign) BOOL isApplet; //0-h5；1-小程序
@property (nonatomic, copy) NSString *path; //小程序路径

@end

NS_ASSUME_NONNULL_END
