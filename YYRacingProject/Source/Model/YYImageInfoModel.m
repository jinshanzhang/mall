//
//  YYImageInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYImageInfoModel.h"

@implementation YYImageInfoModel

- (NSNumber *)pWidth {
    return [NSNumber numberWithFloat:[self.width floatValue]];
}

- (NSNumber *)pHeight {
    return [NSNumber numberWithFloat:[self.height floatValue]];
}

@end
