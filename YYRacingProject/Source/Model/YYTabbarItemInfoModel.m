//
//  YYTabbarItemInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYTabbarItemInfoModel.h"

@implementation YYTabbarItemInfoModel

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.index
                   forKey:@"index"];
    [aCoder encodeObject:self.title
                  forKey:@"title"];
    [aCoder encodeObject:self.normalColor forKey:@"normalColor"];
    [aCoder encodeObject:self.selectColor forKey:@"selectColor"];
    
    [aCoder encodeObject:self.normalIcon forKey:@"normalIcon"];
    [aCoder encodeObject:self.selectIcon forKey:@"selectIcon"];
    
    [aCoder encodeObject:self.normalIconPath forKey:@"normalIconPath"];
    [aCoder encodeObject:self.selectIconPath forKey:@"selectIconPath"];
    
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.index = [aDecoder decodeObjectForKey:@"index"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        
        self.normalColor = [aDecoder decodeObjectForKey:@"normalColor"];
        self.selectColor = [aDecoder decodeObjectForKey:@"selectColor"];
        
        self.normalIcon = [aDecoder decodeObjectForKey:@"normalIcon"];
        self.selectIcon = [aDecoder decodeObjectForKey:@"selectIcon"];
        
        self.normalIconPath = [aDecoder decodeObjectForKey:@"normalIconPath"];
        self.selectIconPath = [aDecoder decodeObjectForKey:@"selectIconPath"];
    }
    return self;
}

@end
