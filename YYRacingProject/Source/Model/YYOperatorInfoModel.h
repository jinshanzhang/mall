//
//  YYOperatorInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYOperatorInfoModel : NSObject

@property (nonatomic, strong) NSString *showTitle;
@property (nonatomic, assign) BOOL     isSelect;
@property (nonatomic, assign) BOOL     isWaitPay;

@property (nonatomic, assign) YYOrderOperatorType operatorType; // 订单操作类型
@property (nonatomic, assign) YYGoodAfterSaleOperatorType goodOperatorType; //商品售后操作类型
@property (nonatomic, assign) YYAfterSaleOperatorType afterSaleOperatorType; //售后列表操作类型

@end
