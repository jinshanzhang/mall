//
//  YYShareInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYShareInfoModel : NSObject

@property (nonatomic, assign) NSInteger  channel;
@property (nonatomic, strong) NSString   *shareDesc;
@property (nonatomic, strong) NSString   *shareImgUrl;
@property (nonatomic, strong) NSString   *shareImage;
@property (nonatomic, strong) NSString   *shareLinkUrl;
@property (nonatomic, strong) NSString   *shareLink;
@property (nonatomic, strong) NSString   *shareTitle;
/**
 * 分享类型
 * 0:纯文本
 * 1:纯图片
 * 2:文本+图片
 * 3:分享连接
 * 4:小程序
 * qq和qq空间不支持纯文本
 * qq不能纯图片
 * qq不支持文本+图片
 */
@property (nonatomic, assign) NSNumber   *shareType;   //分享类型
@property (nonatomic, assign) int        type;

@end
