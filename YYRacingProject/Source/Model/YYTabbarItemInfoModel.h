//
//  YYTabbarItemInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYTabbarItemInfoModel : NSObject <NSCoding>

@property (nonatomic, strong) NSNumber *index;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *normalColor;
@property (nonatomic, copy) NSString *selectColor;

@property (nonatomic, copy) NSString *normalIcon;
@property (nonatomic, copy) NSString *selectIcon;

@property (nonatomic, copy) NSString *normalIconPath;
@property (nonatomic, copy) NSString *selectIconPath;

@end

NS_ASSUME_NONNULL_END
