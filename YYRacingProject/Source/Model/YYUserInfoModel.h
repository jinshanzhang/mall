//
//  YYUserInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYUserInfoModel : NSObject

@property (nonatomic, assign) int level;  // （暂时不加0会员） 1粉丝  2店主
@property (nonatomic, assign) int wxBindingFlag;   //是否绑定微信， 0 未绑定， 1绑定
@property (nonatomic, assign) int certifyFlag;     //用户认证状态 1: 未认证 2： 已认证
@property (nonatomic, assign) int contractFlag;    //签约情况，1：未认证；2：已认证;

@property (nonatomic, assign) int orderWaitToPayCnt;     //待支付数
@property (nonatomic, assign) int orderWaitToReceiptCnt; //待收货数
@property (nonatomic, assign) int orderFinishCnt;        //已完成

@property (nonatomic, assign) int useDefaultNick;   //是否默认昵称  0否 1是 
@property (nonatomic, assign) int useDefaultAvatar; //是否默认头像

@property (nonatomic, assign) int    newFansFlag; //是否新粉丝 0 否 1 是
@property (nonatomic, assign) BOOL   isTestAccount; //是否测试账号
@property (nonatomic, assign) BOOL   isTouristAccount; //是否游客账号

@property (nonatomic, assign) BOOL   isAllowHomeExpireTip;   //是否允许首页过期提醒
@property (nonatomic, assign) BOOL   isAllowMineExpireTip;   //是否允许个人过期提醒

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *useToken;  //用户token
@property (nonatomic, copy) NSString *userName;  //用户名
@property (nonatomic, copy) NSString *avatarUrl; //头像
@property (nonatomic, copy) NSString *wxName;    //微信名称
@property (nonatomic, copy) NSString *phoneTail; //手机尾号
@property (nonatomic, copy) NSString *promotionCode; //推荐码
@property (nonatomic, copy) NSString *balanceIncome; //余额

@property (nonatomic, copy) NSString *easemobId;     //环信ID
@property (nonatomic, copy) NSString *easemobSecret; //环信密码

@property (nonatomic, copy) NSString *couponCountText;         //个人中心券张数文案
@property (nonatomic, copy) NSString *couponExpireSoonText;    //优惠券列表过期提醒样式
@property (nonatomic, copy) NSString *couponExpireSoonUserImg; //个人中心券过期提醒图片
@property (nonatomic, copy) NSString *couponExpireSoonIconImg; //首页券过期提醒图片

@property (nonatomic, copy) NSString *userImgOvertime;  //个人中心过期提醒时间
@property (nonatomic, copy) NSString *homeImgOvertime;  //首页过期提醒日期

@property (nonatomic, assign) BOOL payAccountPasswordFlag;//是否设置过支付密码

@property (nonatomic, copy) NSString *deviceToken;    //设备token

@property (nonatomic, assign) int cityPartnerFlag;//1:展示我的城市店主入口；2：展示我的城市合伙人入口；3:展示我的专属二维码入口;4：粉丝-隐藏所有城市合伙人相关入口

@property (nonatomic, strong) NSDictionary *cityPartner;//城市合伙人昵称


@end
