//
//  YYImageInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYImageInfoModel : NSObject<PhotoViewProtocol>

@property (nonatomic, assign) NSInteger type;   //图片类型: //0普通图片  1商品  2大促活动海报图  3粉丝邀约海报  4视频  5店铺海报图
@property (nonatomic, copy) NSString  *imageUrl; //缩略图
@property (nonatomic, strong) NSString  *bigImageUrl; //大图
@property (nonatomic, strong) NSString  *videoUrl;//视频
@property (nonatomic, strong) NSString  *price; //价格
@property (nonatomic, strong) NSString  *shellUri; //商品ID

@property (nonatomic, assign) NSInteger imageId; //图片ID, 邀约模版大图ID，type为1，2传入。

@property (nonatomic, strong) NSString *height;
@property (nonatomic, strong) NSString *width;

@property (nonatomic, strong) NSString *maxUrl;
@property (nonatomic, strong) NSString *minUrl;

@end
