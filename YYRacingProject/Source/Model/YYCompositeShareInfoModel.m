//
//  YYCompositeShareInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYCompositeShareInfoModel.h"

@implementation YYCompositeShareInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"isApplet" : @"shareChannel", @"path" : @"appletsShareUrl"};
}

@end
