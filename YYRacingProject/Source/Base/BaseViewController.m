//
//  BaseViewController.m
//  YYRacingProject
//
//  Created by cujia_1 on 2020/8/26.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self js_propertyInit];
    [self js_createSubViews];
    [self js_layoutSubViews];
}

- (void)js_propertyInit {
    self.view.backgroundColor = XHLightColor;
}

- (void)js_createSubViews {
    
}

- (void)js_layoutSubViews {
    
}

@end
