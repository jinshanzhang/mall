//
//  BaseViewController.h
//  YYRacingProject
//
//  Created by cujia_1 on 2020/8/26.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

- (void)js_propertyInit;
- (void)js_createSubViews;
- (void)js_layoutSubViews;

@end

NS_ASSUME_NONNULL_END
