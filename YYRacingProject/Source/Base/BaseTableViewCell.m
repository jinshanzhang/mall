//
//  BaseTableViewCell.m
//  ysscw_ios
//
//  Created by 张金山 on 2019/12/29.
//  Copyright © 2019 ysscw. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        if([self respondsToSelector:@selector(js_createSubViews)]) {
            [self js_createSubViews];
        }
        if([self respondsToSelector:@selector(js_layoutSubViews)]) {
            [self js_layoutSubViews];
        }
    }
    return self;
}

- (void)js_createSubViews {
    
}

- (void)js_layoutSubViews {
    
}

@end
