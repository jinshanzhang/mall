//
//  BaseTableViewController.h
//  ysscw_ios
//
//  Created by 张金山 on 2019/12/29.
//  Copyright © 2019 ysscw. All rights reserved.
//

#import "YYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewController : YYBaseViewController

//UITableViewStyle
@property (nonatomic, assign) UITableViewStyle tableViewStyle;
//UItableView
@property (nonatomic, strong) UITableView * tableView;
//是否需要下拉刷新
@property (nonatomic, assign) BOOL needPullDownRefresh;
//是否需要上拉加载
@property (nonatomic, assign) BOOL needPullUpRefresh;

@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, assign) NSInteger pageNum;

@property (nonatomic, assign) NSInteger pageSize;

/**
 下拉加载数据请求
 */
- (void)loadNewData;

/**
上拉加载数据请求
*/
- (void)loadMoreData;

@end

NS_ASSUME_NONNULL_END
