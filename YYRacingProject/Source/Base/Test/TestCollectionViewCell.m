//
//  TestCollectionViewCell.m
//  YYRacingProject
//
//  Created by cujia_1 on 2020/8/26.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "TestCollectionViewCell.h"

@implementation TestCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = UIColor.redColor;
    }
    return self;
}

@end
