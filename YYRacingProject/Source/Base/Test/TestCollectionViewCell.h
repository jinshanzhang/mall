//
//  TestCollectionViewCell.h
//  YYRacingProject
//
//  Created by cujia_1 on 2020/8/26.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestCollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
