//
//  TestCollectionViewController.m
//  YYRacingProject
//
//  Created by cujia_1 on 2020/8/26.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "TestCollectionViewController.h"
#import "UICollectionViewWaterFallLayout.h"
#import "TestCollectionViewCell.h"

@interface TestCollectionViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewWaterFallLayoutDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation TestCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"购物车"];
    [self xh_popTopRootViewController:NO];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.bottom.equalTo(self.view);
    }];
}

#pragma mark -UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TestCollectionViewCell" forIndexPath:indexPath];
    return cell;
}

#pragma mark -UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}


#pragma mark -UICollectionViewWaterFallLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout heightForWidth:(CGFloat)width atIndexPath:(NSIndexPath*)indexPath {
    return width + 54.5;
}


//区列数
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout colCountForSectionAtIndex:(NSInteger)section {
    return 2;
}

//区内边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 12, 5, 12);
}

//垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout lineSpacingForSectionAtIndex:(NSInteger)section {
    return 8.0;
}

//水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout interitemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

#pragma mark - getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewWaterFallLayout *waterFallLayout = [[UICollectionViewWaterFallLayout alloc]init];
        waterFallLayout.delegate = self;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:waterFallLayout];
        _collectionView.backgroundColor = XHLineColor;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[TestCollectionViewCell class] forCellWithReuseIdentifier:@"TestCollectionViewCell"];
//        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
//        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
//        [_collectionView.mj_header beginRefreshing];
        _collectionView.mj_footer.hidden = YES;
    }
    return _collectionView;
}



@end
