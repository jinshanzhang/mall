//
//  BaseTableViewController.m
//  ysscw_ios
//
//  Created by 张金山 on 2019/12/29.
//  Copyright © 2019 ysscw. All rights reserved.
//

#import "BaseTableViewController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)js_propertyInit {
    [super js_propertyInit];
    self.pageNum = 1;
    self.pageSize = 10;
}

- (void)js_createSubViews {
   [super js_createSubViews];
   [self.view addSubview:self.tableView];
}

- (void)js_layoutSubViews {
    [super js_layoutSubViews];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        make.top.mas_equalTo(kNavigationH);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view);
    }];
}

- (UITableView *)tableView {
    if(!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) style:self.tableViewStyle ? self.tableViewStyle : UITableViewStyleGrouped];
        _tableView.backgroundColor = XHLightColor;
        [_tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11, *)) {
            //目前如此
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
        }else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        if (@available(iOS 11.0, *)) {
           _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

- (void)setNeedPullDownRefresh:(BOOL)needPullDownRefresh {
    if(needPullDownRefresh) {
        self.tableView.mj_header = [YYRacingBeckRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    }
}

- (void)setNeedPullUpRefresh:(BOOL)needPullUpRefresh {
    if(needPullUpRefresh) {
        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
        [footer setTitle:@"点击或者上拉去刷新" forState:MJRefreshStateIdle];
        [footer setTitle:@"加载更多 ..." forState:MJRefreshStateRefreshing];
        [footer setTitle:@"我是有底线的" forState:MJRefreshStateNoMoreData];
        footer.stateLabel.font = normalFont(12);
        footer.stateLabel.textColor = HexRGB(0xcccccc);
        self.tableView.mj_footer = footer;
        self.tableView.mj_footer.hidden = YES;
    }
}

- (void)loadNewData {}

- (void)loadMoreData {}

- (NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
