//
//  BaseView.m
//  ysscw_ios
//
//  Created by 张金山 on 2019/12/29.
//  Copyright © 2019 ysscw. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView


-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self js_propertyInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self js_propertyInit];
        [self cj_config];
    }
    return self;
}

#pragma mark ==== LiftCircle ====
- (void)cj_config {
    [self js_createSubViews];
    [self js_layoutSubViews];
}

- (void)js_propertyInit {
    self.backgroundColor = XHLightColor;
}

- (void)js_createSubViews {
    
}

- (void)js_layoutSubViews {
    
}

@end
