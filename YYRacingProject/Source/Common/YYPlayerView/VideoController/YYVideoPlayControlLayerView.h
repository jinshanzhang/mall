//
//  YYVideoPlayControlLayerView.h
//  TestSideslip
//
//  Created by 蔡金明 on 2019/1/17.
//  Copyright © 2019 蔡金明. All rights reserved.
//

#import "SJEdgeControlLayerAdapters.h"
#import "SJControlLayerCarrier.h"
NS_ASSUME_NONNULL_BEGIN

@protocol YYVideoPlayControlLayerDelegate <NSObject>

@optional
- (void)backButtonItemClickOperator;
- (void)downloadButtonItemClickOperator;
- (void)lookGoodButtonItemClickOperator;

@end


@interface YYVideoPlayControlLayerView : SJEdgeControlLayerAdapters<SJControlLayer>

@property (nonatomic, assign) BOOL isHideSkipDetail;  //是否隐藏跳转到商品详情
@property (nonatomic, weak) id <YYVideoPlayControlLayerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
