//
//  YYVideoPlayControlLayerView.m
//  TestSideslip
//
//  Created by 蔡金明 on 2019/1/17.
//  Copyright © 2019 蔡金明. All rights reserved.
//

#import "YYVideoPlayControlLayerView.h"

#import "SJLoadingView.h"
#import "SJProgressSlider.h"
#import "UIView+SJAnimationAdded.h"
#import <SJBaseVideoPlayer/SJBaseVideoPlayer.h>


#define   kBackItemTag       100
#define   kPauseItemTag      101
#define   kDownloadItemTag   102

@interface YYVideoPlayControlLayerView()
<SJProgressSliderDelegate>
@property (nonatomic, weak, nullable) SJBaseVideoPlayer *player; // need weak ref

@property (nonatomic, strong) SJLoadingView    *loadingView;
@property (nonatomic, strong) SJProgressSlider *bottomProgressSlider;

@property (nonatomic, strong) UIButton         *backButton;
@property (nonatomic, strong) UIButton         *pauseButton;
@property (nonatomic, strong) UIButton         *downloadButton;
@property (nonatomic, strong) UIButton         *lookGoodButton;

@end

@implementation YYVideoPlayControlLayerView
@synthesize restarted = _restarted;
@synthesize bottomProgressSlider = _bottomProgressSlider;

#pragma mark - Setter

- (UIButton *)backButton {
    if ( _backButton ) return _backButton;
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backButton setImage:[UIImage imageNamed:@"find_back_icon"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(backButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    return _backButton;
}

- (UIButton *)pauseButton {
    if ( _pauseButton ) return _pauseButton;
    _pauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _pauseButton.hidden = YES;
    [_pauseButton setImage:[UIImage imageNamed:@"video_pause_icon"] forState:UIControlStateNormal];
    [_pauseButton addTarget:self action:@selector(pauseButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    return _pauseButton;
}

- (UIButton *)downloadButton {
    if ( _downloadButton ) return _downloadButton;
    _downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_downloadButton setImage:[UIImage imageNamed:@"video_download_icon"]   forState:UIControlStateNormal];
    [_downloadButton addTarget:self action:@selector(downloadButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    return _downloadButton;
}

- (UIButton *)lookGoodButton {
    if (_lookGoodButton) return _lookGoodButton;
    _lookGoodButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_lookGoodButton setImage:[UIImage imageNamed:@"video_look_good_icon"]
                     forState:UIControlStateNormal];
    [_lookGoodButton addTarget:self action:@selector(lookGoodButtonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    return _lookGoodButton;
}

- (SJLoadingView *)loadingView {
    if ( _loadingView ) return _loadingView;
    _loadingView = [SJLoadingView new];
    _loadingView.lineColor = [UIColor whiteColor];
    return _loadingView;
}

- (SJProgressSlider *)bottomProgressSlider {
    if ( _bottomProgressSlider ) return _bottomProgressSlider;
    _bottomProgressSlider = [SJProgressSlider new];
    _bottomProgressSlider.trackHeight = 2;
    _bottomProgressSlider.round = NO;
    _bottomProgressSlider.delegate = self;
    _bottomProgressSlider.tap.enabled = NO;
    _bottomProgressSlider.enableBufferProgress = YES;
    _bottomProgressSlider.trackImageView.backgroundColor = [UIColor whiteColor];
    _bottomProgressSlider.traceImageView.backgroundColor = [UIColor redColor];
    __weak typeof(self) _self = self;
    _bottomProgressSlider.tappedExeBlock = ^(SJProgressSlider * _Nonnull slider, CGFloat location) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        [self.player seekToTime:location completionHandler:^(BOOL finished) {
            __strong typeof(_self) self = _self;
            if ( !self ) return;
            if ( finished ) [self.player play];
        }];
    };
    return _bottomProgressSlider;
}

- (void)setIsHideSkipDetail:(BOOL)isHideSkipDetail {
    _isHideSkipDetail = isHideSkipDetail;
    self.lookGoodButton.hidden = isHideSkipDetail;
}

- (void)restartControlLayer {
    [self _show:self.controlView animated:YES];
}

- (void)exitControlLayer {
    _player.controlLayerDataSource = nil;
    _player.controlLayerDelegate = nil;
    _player = nil;
    
    [self _hidden:self.controlView animated:YES completionHandler:^{
        if ( !self -> _restarted ) [self.controlView removeFromSuperview];
    }];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( !self ) return nil;
    [self _setupViews];
    return self;
}

- (void)_setupViews {
    [self.controlView addSubview:self.backButton];
    [self.controlView addSubview:self.pauseButton];
    [self.controlView addSubview:self.loadingView];
    [self.controlView addSubview:self.downloadButton];
    [self.controlView addSubview:self.lookGoodButton];
    [self.controlView addSubview:self.bottomProgressSlider];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12);
        make.top.offset(27);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [self.pauseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.offset(0);
        make.size.mas_equalTo(CGSizeMake(81, 81));
    }];
    [self.downloadButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12);
        make.bottom.offset(-27);
        make.size.mas_equalTo(CGSizeMake(31, 31));
    }];
    [self.lookGoodButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-13);
        make.bottom.offset(-27);
        make.size.mas_equalTo(CGSizeMake(72, 23));
    }];
    [self.bottomProgressSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.controlView);
        make.height.mas_equalTo(2);
    }];
    [self.loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.offset(0);
    }];
    
    [self.loadingView start];
}

#pragma mark -

- (UIView *)controlView {
    return self;
}

- (BOOL)controlLayerOfVideoPlayerCanAutomaticallyDisappear:(__kindof SJBaseVideoPlayer *)videoPlayer {
    return YES;
}

/// 是否可以触发播放器的手势
- (BOOL)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer gestureRecognizerShouldTrigger:(SJPlayerGestureType)type location:(CGPoint)location {
    if ( CGRectContainsPoint( _topContainerView.frame, location) ||
        CGRectContainsPoint( _bottomContainerView.frame, location) ) return NO;
    return YES;
}


- (void)installedControlViewToVideoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer {
    self.player = videoPlayer;
    
    [self.player.view layoutIfNeeded];
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer bufferStatusDidChange:(SJPlayerBufferStatus)bufferStatus {
    switch ( bufferStatus ) {
        case SJPlayerBufferStatusUnknown:
        case SJPlayerBufferStatusPlayable: {
            [self.loadingView stop];
        }
            break;
        case SJPlayerBufferStatusUnplayable: {
            [self.loadingView start];
        }
            break;
    }
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer prepareToPlay:(SJVideoPlayerURLAsset *)asset {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
    NSLog(@"current time = %f, total time = %f", videoPlayer.currentTime, videoPlayer.totalTime);
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer statusDidChanged:(SJVideoPlayerPlayStatus)status {
    if (status == SJVideoPlayerPlayStatusPaused) {
        self.pauseButton.hidden = NO;
    }
    else {
        self.pauseButton.hidden = YES;
        if (status == SJVideoPlayerPlayStatusInactivity) {
            [videoPlayer replay];
        }
    }
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer
        currentTime:(NSTimeInterval)currentTime currentTimeStr:(NSString *)currentTimeStr
          totalTime:(NSTimeInterval)totalTime totalTimeStr:(NSString *)totalTimeStr {
    self.bottomProgressSlider.value = currentTime;
    self.bottomProgressSlider.maxValue = videoPlayer.totalTime?:1;
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer presentationSize:(CGSize)size {
    
}

- (void)_show:(UIView *)view animated:(BOOL)animated {
    if ( !view.sjv_disappeared ) return;
    [UIView animateWithDuration:animated?0.6:0 animations:^{
        [view sjv_appear];
    }];
}

- (void)_hidden:(UIView *)view animated:(BOOL)animated {
    [self _hidden:view animated:animated completionHandler:nil];
}

- (void)_hidden:(UIView *)view animated:(BOOL)animated completionHandler:(void(^_Nullable)(void))competionHandler {
    if ( view.sjv_disappeared ) return;
    [UIView animateWithDuration:animated?0.6:0 animations:^{
        [view sjv_disapear];
    }];
}

// SJPlayStatusControlDelegate
// SJVolumeBrightnessRateControlDelegate
// SJLoadingControlDelegate
// .....
// ....

#pragma mark - Event
- (void)backButtonClickEvent:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(backButtonItemClickOperator)]) {
        [self.delegate backButtonItemClickOperator];
    }
}

- (void)pauseButtonClickEvent:(UIButton *)sender {
    if (self.player.playStatus == SJVideoPlayerPlayStatusPaused) {
        [self.player play];
    }
}

- (void)downloadButtonClickEvent:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(downloadButtonItemClickOperator)]) {
        [self.delegate downloadButtonItemClickOperator];
    }
}

- (void)lookGoodButtonClickEvent:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(lookGoodButtonItemClickOperator)]) {
        [self.delegate lookGoodButtonItemClickOperator];
    }
}

@end
