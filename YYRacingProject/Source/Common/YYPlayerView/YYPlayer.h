//
//  YYPlayer.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "SJVideoPlayer.h"

NS_ASSUME_NONNULL_BEGIN
@interface YYPlayer : SJVideoPlayer
@property (nonatomic, copy, nullable) void(^willAppearInScrollViewExeBlock)(YYPlayer *videoPlayer);
@property (nonatomic, copy, nullable) void(^willDisappearInScrollView)(YYPlayer *videoPlayer);
@end
NS_ASSUME_NONNULL_END
