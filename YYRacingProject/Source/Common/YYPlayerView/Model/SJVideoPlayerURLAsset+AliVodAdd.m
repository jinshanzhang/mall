//
//  SJVideoPlayerURLAsset+AliVodAdd.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/27.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "SJVideoPlayerURLAsset+AliVodAdd.h"
#import <objc/message.h>

@implementation AliVodModel
- (instancetype)initWithVid:(NSString *)vid accessKeyId:(NSString *)accessKeyId accessKeySecret:(NSString *)accessKeySecret securityToken:(NSString *)securityToken {
    self = [super init];
    if ( !self ) return nil;
    _vid = vid;
    _accessKeyId = accessKeyId;
    _accessKeySecret = accessKeySecret;
    _securityToken = securityToken;
    return self;
}
@end

@implementation SJVideoPlayerURLAsset (AliVodAdd)
- (instancetype)initWithAliVod:(AliVodModel *)model playModel:(SJPlayModel *)playModel {
    self = [super init];
    if ( !self ) return nil;
    self.aliVod = model;
    self.playModel = playModel;
    return self;
}

- (void)setAliVod:(AliVodModel * _Nullable)aliVod {
    objc_setAssociatedObject(self, @selector(aliVod), aliVod, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (nullable AliVodModel *)aliVod {
    return objc_getAssociatedObject(self, _cmd);
}

- (BOOL)isM3u8 {
    return YES;
}
@end
