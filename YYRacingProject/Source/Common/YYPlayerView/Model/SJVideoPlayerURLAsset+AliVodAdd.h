//
//  SJVideoPlayerURLAsset+AliVodAdd.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/27.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "SJVideoPlayerURLAsset.h"

NS_ASSUME_NONNULL_BEGIN
@interface AliVodModel : NSObject
- (instancetype)initWithVid:(NSString *)vid accessKeyId:(NSString *)accessKeyId accessKeySecret:(NSString *)accessKeySecret securityToken:(NSString *)securityToken;
@property (nonatomic, strong) NSString *vid;
@property (nonatomic, strong) NSString *accessKeyId;
@property (nonatomic, strong) NSString *accessKeySecret;
@property (nonatomic, strong) NSString *securityToken;
@end


@interface SJVideoPlayerURLAsset (AliVodAdd)
- (instancetype)initWithAliVod:(AliVodModel *)model playModel:(SJPlayModel *)playModel;

@property (nonatomic, strong, readonly, nullable) AliVodModel *aliVod;
@end
NS_ASSUME_NONNULL_END
