//
//  AliVodPlaybackTimeObserver.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/27.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AliyunVodPlayer;

NS_ASSUME_NONNULL_BEGIN
@interface AliVodPlaybackTimeObserver : NSObject
@property (nonatomic, weak, readonly, nullable) AliyunVodPlayer *player;
@property (nonatomic, readonly) NSTimeInterval currentTime;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval bufferTime;

- (instancetype)initWithPlayer:(__weak AliyunVodPlayer *)player
  currentTimeDidChangeExeBlock:(void(^)(AliVodPlaybackTimeObserver *helper))currentTimeDidChangeExeBlock
   bufferTimeDidChangeExeBlock:(void(^)(AliVodPlaybackTimeObserver *helper))bufferTimeDidChangeExeBlock
     durationDidChangeExeBlock:(void(^)(AliVodPlaybackTimeObserver *helper))durationDidChangeExeBlock;

- (void)start;
- (void)pause;
- (void)stop;
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new  NS_UNAVAILABLE;
@end
NS_ASSUME_NONNULL_END
