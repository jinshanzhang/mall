//
//  AliVodPlaybackTimeObserver.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/27.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "AliVodPlaybackTimeObserver.h"
#import <AliyunVodPlayerSDK/AliyunVodPlayer.h>
#import <SJBaseVideoPlayer/NSTimer+SJAssetAdd.h>

NS_ASSUME_NONNULL_BEGIN
@interface AliVodPlaybackTimeObserver ()
@property (nonatomic, strong, nullable) NSTimer *refreshTimer;
@end
@implementation AliVodPlaybackTimeObserver {
    void(^_currentTimeDidChangeExeBlock)(AliVodPlaybackTimeObserver *helper);
    void(^_bufferTimeDidChangeExeBlock)(AliVodPlaybackTimeObserver *helper);
    void(^_durationDidChangeExeBlock)(AliVodPlaybackTimeObserver *helper);
}
- (instancetype)initWithPlayer:(__weak AliyunVodPlayer *)player
  currentTimeDidChangeExeBlock:(void(^)(AliVodPlaybackTimeObserver *helper))currentTimeDidChangeExeBlock
   bufferTimeDidChangeExeBlock:(void(^)(AliVodPlaybackTimeObserver *helper))bufferTimeDidChangeExeBlock
     durationDidChangeExeBlock:(void(^)(AliVodPlaybackTimeObserver *helper))durationDidChangeExeBlock {
    self = [super init];
    if ( !self ) return nil;
    _player = player;
    _currentTimeDidChangeExeBlock = currentTimeDidChangeExeBlock;
    _durationDidChangeExeBlock = durationDidChangeExeBlock;
    _bufferTimeDidChangeExeBlock = bufferTimeDidChangeExeBlock;
    return self;
}
- (void)start {
    [_refreshTimer invalidate];
    __weak typeof(self) _self = self;
    _refreshTimer = [NSTimer assetAdd_timerWithTimeInterval:0.5 block:^(NSTimer *timer) {
        __strong typeof(_self) self = _self;
        if ( !self ) {
            [timer invalidate];
            return ;
        }
        [self _updateTimes];
    } repeats:YES];
    [NSRunLoop.mainRunLoop addTimer:_refreshTimer forMode:NSRunLoopCommonModes];
}
- (void)_updateTimes {
    if ( _player.duration != _duration ) {
        _duration = _player.duration;
        if ( _durationDidChangeExeBlock ) _durationDidChangeExeBlock(self);
    }
    
    if ( _player.currentTime != _currentTime ) {
        _currentTime = _player.currentTime;
        if ( _currentTimeDidChangeExeBlock ) _currentTimeDidChangeExeBlock(self);
    }
    
    if ( _bufferTime != _player.loadedTime ) {
        _bufferTime = _player.loadedTime;
        if ( _bufferTimeDidChangeExeBlock ) _bufferTimeDidChangeExeBlock(self);
    }
}
- (void)pause {
    [_refreshTimer invalidate];
    _refreshTimer = nil;
}
- (void)stop {
    [_refreshTimer invalidate];
    _refreshTimer = nil;
    _currentTime = 0;
    _duration = 0;
    _bufferTime = 0;
}
@end
NS_ASSUME_NONNULL_END
