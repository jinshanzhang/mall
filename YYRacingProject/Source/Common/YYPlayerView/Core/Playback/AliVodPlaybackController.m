//
//  AliVodPlaybackController.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/27.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "AliVodPlaybackController.h"
#import "AliVodPlaybackTimeObserver.h"
#import "SJVideoPlayerURLAsset+AliVodAdd.h"

NS_ASSUME_NONNULL_BEGIN
@interface AliVodPlaybackController ()<AliyunVodPlayerDelegate>
@property (nonatomic, strong, readonly) AliyunVodPlayer *player;
@property (nonatomic, strong, readonly) AliVodPlaybackTimeObserver *timeObserver;
@property (nonatomic, strong, nullable) NSError *error;
@property (nonatomic) SJMediaPlaybackPrepareStatus prepareStatus;
@property (nonatomic) BOOL isSeekingToSpecifyStartTime;
@property (nonatomic) AliyunVodPlayerState playerState_1_loading;
@property (nonatomic) AliyunVodPlayerState playerState_2_prepare;
@property (nonatomic) BOOL isFinished_needReset;
@property (nonatomic) SJPlayerBufferStatus bufferStatus;
@property (nonatomic) BOOL isReadyForDisplay;
@end

@implementation AliVodPlaybackController {
    void(^_Nullable _seek_completionHandler)(BOOL result);
}
@synthesize delegate = _delegate;
@synthesize media = _media;
@synthesize rate = _rate;
@synthesize mute = _mute;
@synthesize volume = _volume;
@synthesize videoGravity = _videoGravity;
@synthesize bufferStatus = _bufferStatus;
@synthesize presentationSize = _presentationSize;
@synthesize isReadyForDisplay = _isReadyForDisplay;
@synthesize pauseWhenAppDidEnterBackground = _pauseWhenAppDidEnterBackground;

- (instancetype)init {
    self = [super init];
    if ( !self ) return nil;
    _player = [AliyunVodPlayer new];
    _player.autoPlay = NO;
    _player.delegate = self;
    
    __weak typeof(self) _self = self;
    _timeObserver = [[AliVodPlaybackTimeObserver alloc] initWithPlayer:_player currentTimeDidChangeExeBlock:^(AliVodPlaybackTimeObserver * _Nonnull helper) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( [self.delegate respondsToSelector:@selector(playbackController:currentTimeDidChange:)] ) {
            [self.delegate playbackController:self currentTimeDidChange:helper.currentTime];
        }
    } bufferTimeDidChangeExeBlock:^(AliVodPlaybackTimeObserver * _Nonnull helper) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( [self.delegate respondsToSelector:@selector(playbackController:bufferLoadedTimeDidChange:)] ) {
            [self.delegate playbackController:self bufferLoadedTimeDidChange:helper.bufferTime];
        }
    } durationDidChangeExeBlock:^(AliVodPlaybackTimeObserver * _Nonnull helper) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( [self.delegate respondsToSelector:@selector(playbackController:durationDidChange:)] ) {
            [self.delegate playbackController:self durationDidChange:helper.duration];
        }
    }];
    return self;
}

- (void)dealloc {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
    [self.playerView removeFromSuperview];
    [_player releasePlayer];
}

#pragma mark -
- (void)setMute:(BOOL)mute {
    _mute = mute;
    _player.muteMode = mute;
}

- (void)setVideoGravity:(SJVideoGravity)videoGravity {
    _videoGravity = videoGravity;
    if ( videoGravity == AVLayerVideoGravityResizeAspect ) {
        _player.displayMode = AliyunVodPlayerDisplayModeFit;
    }
    else if ( videoGravity == AVLayerVideoGravityResizeAspectFill ) {
        _player.displayMode = AliyunVodPlayerDisplayModeFitWithCropping;
    }
}

- (void)setBufferStatus:(SJPlayerBufferStatus)bufferStatus {
    if ( bufferStatus == _bufferStatus )
        return;
    _bufferStatus = bufferStatus;
    if ( [self.delegate respondsToSelector:@selector(playbackController:bufferStatusDidChange:)] ) {
        [self.delegate playbackController:self bufferStatusDidChange:bufferStatus];
    }
}

- (void)setIsReadyForDisplay:(BOOL)isReadyForDisplay {
    if ( isReadyForDisplay == _isReadyForDisplay )
        return;
    _isReadyForDisplay = isReadyForDisplay;
    if ( isReadyForDisplay && [self.delegate respondsToSelector:@selector(playbackControllerIsReadyForDisplay:)] ) {
        [self.delegate playbackControllerIsReadyForDisplay:self];
    }
}

#pragma mark -

- (AliyunVodPlayerVideoQuality)quality {
    return [_player getAliyunMediaInfo].videoQuality;
}

- (nullable NSArray<NSNumber *> *)currentVideoSupportQualitys {
    return [_player getAliyunMediaInfo].allSupportQualitys;
}

- (nullable NSArray<NSString *> *)currentVideoSupportDefinitions {
    NSMutableArray *m = [NSMutableArray new];
    [[self currentVideoSupportQualitys] enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [m addObject:[self definitionStrByQuality:[obj integerValue]]];
    }];
    if ( 0 != m.count ) return m;
    return nil;
}

- (void)yy_swithToDefinitionAtIndex:(NSInteger)idx {
    NSArray<NSNumber *> *s = [self currentVideoSupportQualitys];
    if ( idx >= s.count ) return;
    if ( idx < 0 ) return;
    AliyunVodPlayerVideoQuality q = [[[self currentVideoSupportQualitys] objectAtIndex:idx] integerValue];
    _player.quality = q;
}

- (NSString *)definitionStrByQuality:(AliyunVodPlayerVideoQuality)quality {
    switch ( quality ) {
        case AliyunVodPlayerVideoFD:
            return @"流畅";
        case AliyunVodPlayerVideoLD:
            return @"标清";
        case AliyunVodPlayerVideoSD:
            return @"高清";
        case AliyunVodPlayerVideoHD:
            return @"超清";
        case AliyunVodPlayerVideo2K:
            return @"2K";
        case AliyunVodPlayerVideo4K:
            return @"4K";
        case AliyunVodPlayerVideoOD:
            return @"原画";
    }
}

- (NSUInteger)indexOfCurrentQuality {
    __block NSUInteger index = NSNotFound;
    [[self currentVideoSupportQualitys] enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( [obj integerValue] != self.quality ) return;
        *stop = YES;
        index = idx;
    }];
    return index;
}

- (NSTimeInterval)currentTime {
    return _timeObserver.currentTime;
}

- (NSTimeInterval)duration {
    return _timeObserver.duration;
}

- (NSTimeInterval)bufferLoadedTime {
    return _timeObserver.bufferTime;
}

- (UIView *)playerView {
    return _player.playerView;
}

- (void)setMedia:(nullable id<SJMediaModelProtocol>)media {
    _media = media;
    [_timeObserver stop];
    [_player reset];
    _error = nil;
    _bufferStatus = 0;
    _presentationSize = CGSizeZero;
    _prepareStatus = 0;
    self.isReadyForDisplay = NO;
    _isSeekingToSpecifyStartTime = NO;
}

- (void)prepareToPlay {
    if ( !_media ) return;
    SJVideoPlayerURLAsset *asset = (id)self.media;
    if ( asset.aliVod ) {
        [_player prepareWithVid:asset.aliVod.vid accessKeyId:asset.aliVod.accessKeyId accessKeySecret:asset.aliVod.accessKeySecret securityToken:asset.aliVod.securityToken];
    }
    else {
        [_player prepareWithURL:_media.mediaURL];
    }
}

- (void)play {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
    if ( _prepareStatus == SJMediaPlaybackPrepareStatusFailed ) return;
    if ( _player.playerState == AliyunVodPlayerStatePrepared ) [_player start];
    else if ( _player.playerState == AliyunVodPlayerStateFinish ) [_player replay];
    else [_player resume];
    [_timeObserver start];
}

- (void)pause {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
    [_player pause];
    [_timeObserver pause];
}

- (void)stop {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
    [_player stop];
    [_timeObserver stop];
    self.isReadyForDisplay = NO;
}

- (void)seekToTime:(NSTimeInterval)secs completionHandler:(nullable void (^)(BOOL))completionHandler {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
    [_player resume];
    [_player seekToTime:secs];
    _seek_completionHandler = completionHandler;
}

- (nullable UIImage *)screenshot {
    return [_player snapshot];
}

- (void)switchVideoDefinitionByURL:(nonnull NSURL *)URL { /* nothing */}


#pragma mark -
/**
 * 功能：播放事件协议方法,主要内容 AliyunVodPlayerEventPrepareDone状态下，此时获取到播放视频数据（时长、当前播放数据、视频宽高等）
 * 参数：event 视频事件
 */
- (void)vodPlayer:(AliyunVodPlayer *)vodPlayer onEventCallback:(AliyunVodPlayerEvent)event {
    switch ( event ) {
        case AliyunVodPlayerEventFirstFrame: {
            self.isReadyForDisplay = YES;
            _presentationSize = CGSizeMake(vodPlayer.videoWidth, vodPlayer.videoHeight);
            if ( [self.delegate respondsToSelector:@selector(playbackController:presentationSizeDidChange:)] ) {
                [self.delegate playbackController:self presentationSizeDidChange:_presentationSize];
            }
            
            if ( !_isSeekingToSpecifyStartTime && 0 != self.media.specifyStartTime ) {
                _isSeekingToSpecifyStartTime = YES;
                [self seekToTime:self.media.specifyStartTime completionHandler:nil];
            }
        }
            break;
        case AliyunVodPlayerEventSeekDone: {
            if ( _seek_completionHandler ) {
                _seek_completionHandler(YES);
                _seek_completionHandler = nil;
            }
        }
            break;
        case AliyunVodPlayerEventBeginLoading: {
//            self.bufferStatus = SJPlayerBufferStatusUnplayable;
            printf("\n: -- AliyunVodPlayerEventBeginLoading\n");
        }
            break;
        case AliyunVodPlayerEventEndLoading: {
            self.bufferStatus = SJPlayerBufferStatusPlayable;
            printf("\n: -- AliyunVodPlayerEventEndLoading\n");
        }
            break;
        default: break;
    }
}

- (void)vodPlayer:(AliyunVodPlayer*)vodPlayer newPlayerState:(AliyunVodPlayerState)state {
#if 1
    switch ( state ) {
        case AliyunVodPlayerStateIdle:
            NSLog(@":--  AliyunVodPlayerStateIdle");
            break;
        case AliyunVodPlayerStateError:
            NSLog(@":--  AliyunVodPlayerStateError");
            break;
        case AliyunVodPlayerStatePrepared:
            NSLog(@":--  AliyunVodPlayerStatePrepared");
            break;
        case AliyunVodPlayerStatePlay:
            NSLog(@":--  AliyunVodPlayerStatePlay");
            break;
        case AliyunVodPlayerStatePause:
            NSLog(@":--  AliyunVodPlayerStatePause");
            break;
        case AliyunVodPlayerStateStop:
            NSLog(@":--  AliyunVodPlayerStateStop");
            break;
        case AliyunVodPlayerStateFinish:
            NSLog(@":--  AliyunVodPlayerStateFinish");
            break;
        case AliyunVodPlayerStateLoading:
            NSLog(@":--  AliyunVodPlayerStateLoading");
            break;
    }
#endif

//    AliyunVodPlayerStatePrepared,           //已准备好
//    AliyunVodPlayerStateLoading             //加载中
    switch ( state ) {
        default: break;
        case AliyunVodPlayerStateError: {
            self.error = [NSError errorWithDomain:NSCocoaErrorDomain code:-1 userInfo:@{@"msg":@"播放错误"}];
            self.prepareStatus = SJMediaPlaybackPrepareStatusFailed;
            if ( [self.delegate respondsToSelector:@selector(playbackController:prepareToPlayStatusDidChange:)] ) {
                [self.delegate playbackController:self prepareToPlayStatusDidChange:self.prepareStatus];
            }
        }
            break;
        case AliyunVodPlayerStatePrepared: {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ( self.error ) return;
                if ( vodPlayer.playerState == AliyunVodPlayerStatePrepared ) {
                    self.prepareStatus = SJMediaPlaybackPrepareStatusReadyToPlay;
                    if ( [self.delegate respondsToSelector:@selector(playbackController:prepareToPlayStatusDidChange:)] ) {
                        [self.delegate playbackController:self prepareToPlayStatusDidChange:SJMediaPlaybackPrepareStatusReadyToPlay];
                    }
                }
            });
        }
            break;
        case AliyunVodPlayerStateLoading: {
            self.bufferStatus = SJPlayerBufferStatusUnplayable;
        }
            break;
        case AliyunVodPlayerStatePlay: {
            if ( _isFinished_needReset &&
                _playerState_1_loading == AliyunVodPlayerStateLoading &&
                _playerState_2_prepare == AliyunVodPlayerStatePrepared ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self->_player pause];
                });
                _isFinished_needReset = NO;
            }
        }
            break;
        case AliyunVodPlayerStateFinish: {
            _isFinished_needReset = YES;
            [_player replay];
            if ( [self.delegate respondsToSelector:@selector(mediaDidPlayToEndForPlaybackController:)] ) {
                [self.delegate mediaDidPlayToEndForPlaybackController:self];
            }
        }
            break;
    }
    
    _playerState_1_loading = _playerState_2_prepare;
    _playerState_2_prepare = state;
}

/**
 * 功能：播放器播放时发生错误时，回调信息
 * 参数：errorModel 播放器报错时提供的错误信息对象
 */
- (void)vodPlayer:(AliyunVodPlayer *)vodPlayer playBackErrorModel:(AliyunPlayerVideoErrorModel *)errorModel {
}

@end
NS_ASSUME_NONNULL_END
