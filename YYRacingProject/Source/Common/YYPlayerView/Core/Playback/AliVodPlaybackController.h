//
//  AliVodPlaybackController.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/27.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SJBaseVideoPlayer/SJMediaPlaybackProtocol.h>
#import <AliyunVodPlayerSDK/AliyunVodPlayer.h>

NS_ASSUME_NONNULL_BEGIN
@interface AliVodPlaybackController : NSObject<SJMediaPlaybackController>
@property (nonatomic, readonly) AliyunVodPlayerVideoQuality quality;

- (void)yy_swithToDefinitionAtIndex:(NSInteger)idx;
- (nullable NSArray<NSString *> *)currentVideoSupportDefinitions;
- (NSString *)definitionStrByQuality:(AliyunVodPlayerVideoQuality)quality;
- (NSUInteger)indexOfCurrentQuality;
@end
NS_ASSUME_NONNULL_END
