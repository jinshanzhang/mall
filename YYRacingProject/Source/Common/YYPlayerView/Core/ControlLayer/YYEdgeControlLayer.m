//
//  YYEdgeControlLayer.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "YYEdgeControlLayer.h"
#import <SJBaseVideoPlayer/SJBaseVideoPlayer.h>
#import <AliyunVodPlayerSDK/AliyunVodPlayer.h>
#import "AliVodPlaybackController.h"
#import "LWZLabelContainerView.h"
#import "YYVideoDefinitionsView.h"
#import <Masonry/Masonry.h>

NS_ASSUME_NONNULL_BEGIN
static SJEdgeControlButtonItemTag const SJEdgeControlButtonItem_Definition = 101;

@interface YYEdgeControlLayer()
@property (nonatomic, weak, nullable) SJBaseVideoPlayer *yy_player;
@property (nonatomic, weak, nullable) AliVodPlaybackController *playbackController;
@property (nonatomic, strong, readonly) LWZLabelContainerView *definitionLabel;
@property (nonatomic, strong, readonly) YYVideoDefinitionsView *definitonsView;
@end

@implementation YYEdgeControlLayer
@synthesize definitionLabel = _definitionLabel;
@synthesize definitonsView = _definitonsView;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( !self ) return nil;
    [self yy_setupView];
    return self;
}

- (void)dealloc {
#ifdef DEBUG
    NSLog(@"%d - %s", (int)__LINE__, __func__);
#endif
}

- (void)yy_setupView {
    [self yy_setupDefinitionsView];
}

#pragma mark -
- (void)yy_setupDefinitionsView {
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 49, 49)];
    _definitionLabel = [[LWZLabelContainerView alloc] initWithEdges:UIEdgeInsetsMake(4, 8, 4, 8) borderColor:[UIColor whiteColor] borderWidth:1 cornerRadius:5];
    _definitionLabel.label.text = @"画质";
    _definitionLabel.label.textColor = [UIColor whiteColor];
    _definitionLabel.label.font = [UIFont systemFontOfSize:12];
    [contentView addSubview:_definitionLabel];
    [_definitionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.offset(0);
    }];
    SJEdgeControlButtonItem *definitionItem = [[SJEdgeControlButtonItem alloc] initWithCustomView:contentView tag:SJEdgeControlButtonItem_Definition];
    [definitionItem addTarget:self action:@selector(yy_clickedDefinitionItem:)];
    [self.bottomAdapter insertItem:definitionItem rearItem:SJEdgeControlLayerBottomItem_FullBtn];
    
    _definitonsView = [YYVideoDefinitionsView new];
    _definitonsView.hidden = YES;
    [self addSubview:_definitonsView];
    /// 选择画质
    __weak typeof(self) _self = self;
    _definitonsView.selectedItemExeBlock = ^(YYVideoDefinitionsView * _Nonnull view, NSInteger atIndex) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        [self yy_swithToDefinitionAtIndex:atIndex];
    };
}

/// 点击 底部控制层上的画质item
- (void)yy_clickedDefinitionItem:(SJEdgeControlButtonItem *)item {
    if ( [self yy_definitionsViewIsHidden] ) {
        [self yy_needShowDefinitionsView];
    }
    else {
        [self yy_needHiddenDefinitionsView];
    }
}

/// 点击 画质s视图上的cell
- (void)yy_swithToDefinitionAtIndex:(NSInteger)idx {
    NSString *deifinition = [[_playbackController currentVideoSupportDefinitions] objectAtIndex:idx];
    _definitonsView.selectedIdx = idx;
    _definitionLabel.label.text = deifinition;
    [_playbackController yy_swithToDefinitionAtIndex:idx];
    [self yy_needHiddenDefinitionsView];
}

/// 需要更新 底部控制层上的画质item
- (void)yy_updateDefinitionLabel {
    SJEdgeControlButtonItem *definitionItem = [self.bottomAdapter itemForTag:SJEdgeControlButtonItem_Definition];
    definitionItem.hidden = !_yy_player.isFullScreen;
    
    if ( !definitionItem.hidden ) {
        if ( [self yy_definitionsViewIsHidden] ) { // 如果画质s视图显示, 则高亮显示, 否则正常状态
            _definitionLabel.layer.borderColor = [UIColor whiteColor].CGColor;
            _definitionLabel.label.textColor = [UIColor whiteColor];
        }
        else {
            _definitionLabel.layer.borderColor = [UIColor orangeColor].CGColor;
            _definitionLabel.label.textColor = [UIColor redColor];
        }
    }
}

/// 画质s 视图, 是否隐藏
- (BOOL)yy_definitionsViewIsHidden {
    return _definitonsView.isHidden;
}

/// 需要显示 画质s 视图
- (void)yy_needHiddenDefinitionsView {
    _definitonsView.hidden = YES;
    [self yy_updateDefinitionLabel];
}

/// 需要隐藏 画质s 视图
- (void)yy_needShowDefinitionsView {
    _definitonsView.hidden = NO;
    [_yy_player controlLayerNeedAppear];
    [self yy_updateDefinitionLabel];
    
    SJEdgeControlButtonItem *definitionItem = [self.bottomAdapter itemForTag:SJEdgeControlButtonItem_Definition];
    if ( definitionItem.customView.superview ) {
        // 更新一下位置
        [_definitonsView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.definitionLabel);
            make.bottom.equalTo(self.definitionLabel.mas_top).offset(-8);
        }];
    }
}

#pragma mark - Delegate Methods
- (void)installedControlViewToVideoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer {
    [super installedControlViewToVideoPlayer:videoPlayer];
    _yy_player = videoPlayer;
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer statusDidChanged:(SJVideoPlayerPlayStatus)status {
    [super videoPlayer:videoPlayer statusDidChanged:status];
    switch ( status ) {
        case SJVideoPlayerPlayStatusReadyToPlay: {          // 准备好播放后, 获取 当前画质
            _definitionLabel.label.text = [_playbackController definitionStrByQuality:_playbackController.quality];
            _definitonsView.selectedIdx = [_playbackController indexOfCurrentQuality];
            _definitonsView.definitions = [_playbackController currentVideoSupportDefinitions];
        }
            break;
        default:
            break;
    }
}
/// 控制层自动隐藏的条件, 返回YES, 表示可以隐藏
/// 画质显示时, 返回NO, 表示控制层不可自动隐藏
- (BOOL)controlLayerOfVideoPlayerCanAutomaticallyDisappear:(__kindof SJBaseVideoPlayer *)videoPlayer {
     if ( ![self yy_definitionsViewIsHidden] ) return NO;
    return [super controlLayerOfVideoPlayerCanAutomaticallyDisappear:videoPlayer];
}
/*- (BOOL)controlLayerDisappearCondition {
    if ( ![self yy_definitionsViewIsHidden] ) return NO;
    return [super controlLayerDisappearCondition];
}*/

/// 控制层需要隐藏
/// 一起隐藏画质
- (void)controlLayerNeedDisappear:(__kindof SJBaseVideoPlayer *)videoPlayer {
    [super controlLayerNeedDisappear:videoPlayer];
    [self yy_needHiddenDefinitionsView];
}

/// 手势触发的条件, 返回YES, 表示可以触发播放器自带手势
/// 点击位置在画质视图上时, 不可触发
- (BOOL)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer gestureRecognizerShouldTrigger:(SJPlayerGestureType)type location:(CGPoint)location {
    if ( CGRectContainsPoint(_definitonsView.frame, location) )
        return NO;
    return [super videoPlayer:videoPlayer gestureRecognizerShouldTrigger:type location:location];
}
/*
- (BOOL)triggerGesturesCondition:(CGPoint)location {
    if ( CGRectContainsPoint(_definitonsView.frame, location) )
        return NO;
    return [super triggerGesturesCondition:location];
}*/

/// 播放器滚动出现
- (void)videoPlayerWillAppearInScrollView:(__kindof SJBaseVideoPlayer *)videoPlayer {
    if ( _videoPlayerWillAppearInScrollViewExeBlock ) _videoPlayerWillAppearInScrollViewExeBlock();
}

/// 播放器滚动消失
- (void)videoPlayerWillDisappearInScrollView:(__kindof SJBaseVideoPlayer *)videoPlayer {
    if ( _videoPlayerWillDisappearInScrollView ) _videoPlayerWillDisappearInScrollView();
}

- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer willRotateView:(BOOL)isFull {
    [super videoPlayer:videoPlayer willRotateView:isFull];
    [self yy_needHiddenDefinitionsView];        // 播放器将要旋转的时候, 隐藏画质视图
}

/// 每次准备播放一个资源, 该方法都会调用
- (void)videoPlayer:(__kindof SJBaseVideoPlayer *)videoPlayer prepareToPlay:(SJVideoPlayerURLAsset *)asset {
    [super videoPlayer:videoPlayer prepareToPlay:asset];
    _playbackController = nil;
    if ( [videoPlayer.playbackController isKindOfClass:[AliVodPlaybackController class]] ) {
        _playbackController = videoPlayer.playbackController;
    }
}
@end
NS_ASSUME_NONNULL_END
