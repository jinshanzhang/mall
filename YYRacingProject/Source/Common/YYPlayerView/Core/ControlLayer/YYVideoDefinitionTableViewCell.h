//
//  YYVideoDefinitionTableViewCell.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface YYVideoDefinitionTableViewCell : UITableViewCell
@property (nonatomic, strong, readonly) UILabel *label;
@property (nonatomic, strong, readonly) UIButton *backgroundButton;
@property (nonatomic) BOOL isCurrentDefinitionCell;
@property (nonatomic, copy, nullable) void(^selectedExeBlock)(YYVideoDefinitionTableViewCell *cell);
@end
NS_ASSUME_NONNULL_END
