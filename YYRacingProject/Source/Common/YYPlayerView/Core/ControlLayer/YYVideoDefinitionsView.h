//
//  YYVideoDefinitionsView.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface YYVideoDefinitionsView : UIView
@property (nonatomic) NSInteger selectedIdx;
@property (nonatomic, strong, nullable) NSArray<NSString *> *definitions;
@property (nonatomic, copy, nullable) void(^selectedItemExeBlock)(YYVideoDefinitionsView *view, NSInteger atIndex);
@end
NS_ASSUME_NONNULL_END
