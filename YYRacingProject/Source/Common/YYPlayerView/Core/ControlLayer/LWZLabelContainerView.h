//
//  LWZLabelContainerView.h
//  LWZBaseViews_Example
//
//  Created by BlueDancer on 2018/11/22.
//  Copyright © 2018 changsanjiang@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface LWZLabelContainerView : UIView
- (instancetype)initWithEdges:(UIEdgeInsets)edges corners:(UIRectCorner)corners cornerRadius:(CGFloat)cornerRadius;

- (instancetype)initWithEdges:(UIEdgeInsets)edges borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius;

@property (nonatomic, strong, readonly) UILabel *label;
@property (nonatomic, readonly) UIRectCorner corners;
@property (nonatomic, readonly) CGFloat cornerRadius;

- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new  NS_UNAVAILABLE;
@end
NS_ASSUME_NONNULL_END
