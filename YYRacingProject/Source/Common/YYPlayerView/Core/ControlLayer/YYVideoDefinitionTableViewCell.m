//
//  YYVideoDefinitionTableViewCell.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "YYVideoDefinitionTableViewCell.h"
#import <Masonry/Masonry.h>

@implementation YYVideoDefinitionTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( !self ) return nil;
    [self _setupView];
    return self;
}

- (void)_setupView {
    self.backgroundColor = [UIColor clearColor];

    _label = [UILabel new];
    [self.contentView addSubview:_label];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.textColor = [UIColor whiteColor];
    [_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    
    _backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contentView addSubview:_backgroundButton];
    [_backgroundButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    [_backgroundButton addTarget:self action:@selector(clicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)clicked {
    if ( _selectedExeBlock ) _selectedExeBlock(self);
}

- (void)setIsCurrentDefinitionCell:(BOOL)isCurrentDefinitionCell {
    _isCurrentDefinitionCell = isCurrentDefinitionCell;
    if ( isCurrentDefinitionCell )
        _label.textColor = [UIColor redColor];
    else
        _label.textColor = [UIColor whiteColor];
}
@end
