//
//  YYEdgeControlLayer.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "SJEdgeControlLayer.h"

NS_ASSUME_NONNULL_BEGIN
@interface YYEdgeControlLayer : SJEdgeControlLayer
@property (nonatomic, copy, nullable) void(^videoPlayerWillAppearInScrollViewExeBlock)(void);
@property (nonatomic, copy, nullable) void(^videoPlayerWillDisappearInScrollView)(void);
@end
NS_ASSUME_NONNULL_END
