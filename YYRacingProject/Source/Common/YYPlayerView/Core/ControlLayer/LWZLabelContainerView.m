//
//  LWZLabelContainerView.m
//  LWZBaseViews_Example
//
//  Created by BlueDancer on 2018/11/22.
//  Copyright © 2018 changsanjiang@gmail.com. All rights reserved.
//

#import "LWZLabelContainerView.h"
#import <Masonry/Masonry.h>

NS_ASSUME_NONNULL_BEGIN
@implementation LWZLabelContainerView {
    CAShapeLayer *_Nullable _maskLayer;
}

- (instancetype)initWithEdges:(UIEdgeInsets)edges corners:(UIRectCorner)corners cornerRadius:(CGFloat)cornerRadius {
    self = [super initWithFrame:CGRectZero];
    if ( !self ) return nil;
    [self _addLabel:edges];
    self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    
    _maskLayer = [CAShapeLayer layer];
    _maskLayer.fillColor = UIColor.blackColor.CGColor;
    self.layer.mask = _maskLayer;
    _corners = corners;
    _cornerRadius = cornerRadius;
    return self;
}

- (instancetype)initWithEdges:(UIEdgeInsets)edges borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius {
    self = [super initWithFrame:CGRectZero];
    if ( !self ) return nil;
    [self _addLabel:edges];
    
    self.layer.borderColor = borderColor.CGColor;
    self.layer.borderWidth  = borderWidth;
    self.layer.cornerRadius = _cornerRadius = cornerRadius;
    return self;
}

- (void)_addLabel:(UIEdgeInsets)edges {
    _label = [[UILabel alloc] init];
    _label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_label];
    [_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).mas_offset(edges);
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if ( _maskLayer ) {
        CGRect bounds = self.bounds;
        _maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:bounds byRoundingCorners:_corners cornerRadii:CGSizeMake(_cornerRadius, _cornerRadius)].CGPath;
        _maskLayer.bounds = bounds;
        _maskLayer.position = CGPointMake(bounds.size.width * 0.5, bounds.size.height * 0.5);
    }
}
@end
NS_ASSUME_NONNULL_END
