//
//  YYVideoDefinitionsView.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "YYVideoDefinitionsView.h"
#import <Masonry/Masonry.h>
#import "YYVideoDefinitionTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
static NSString *YYVideoDefinitionTableViewCellID = @"YYVideoDefinitionTableViewCell";

@interface YYVideoDefinitionsView ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong, readonly) UITableView *tableView;
@end

@implementation YYVideoDefinitionsView
- (instancetype)init {
    self = [super init];
    if ( !self ) return nil;
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [_tableView registerClass:NSClassFromString(YYVideoDefinitionTableViewCellID) forCellReuseIdentifier:YYVideoDefinitionTableViewCellID];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = 30;
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    return self;
}

- (void)setSelectedIdx:(NSInteger)selectedIdx {
    _selectedIdx = selectedIdx;
    [_tableView reloadData];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(60, _tableView.rowHeight * _definitions.count);
}

- (void)setDefinitions:(nullable NSArray<NSString *> *)definitions {
    if ( definitions == _definitions ) return;
    _definitions = definitions;
    [self invalidateIntrinsicContentSize];
    [_tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _definitions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YYVideoDefinitionTableViewCell *cell = (id)[tableView dequeueReusableCellWithIdentifier:YYVideoDefinitionTableViewCellID forIndexPath:indexPath];
    cell.isCurrentDefinitionCell = (_selectedIdx == indexPath.row);
    cell.label.text = _definitions[indexPath.row];
    __weak typeof(self) _self = self;
    cell.selectedExeBlock = ^(YYVideoDefinitionTableViewCell * _Nonnull cell) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( self.selectedItemExeBlock ) self.selectedItemExeBlock(self, indexPath.row);
    };
    return cell;
}
@end
NS_ASSUME_NONNULL_END
