//
//  YYPlayer.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/28.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "YYPlayer.h"
#import "YYEdgeControlLayer.h"
#import "AliVodPlaybackController.h"

@implementation YYPlayer
- (instancetype)init {
    self = [super init];
    if ( !self ) return nil;
    self.pausedToKeepAppearState = YES;
    self.playbackController = [AliVodPlaybackController new];
    YYEdgeControlLayer *edgeControlLayer = [YYEdgeControlLayer new];
    __weak typeof(self) _self = self;
    edgeControlLayer.videoPlayerWillAppearInScrollViewExeBlock = ^{
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( self.willAppearInScrollViewExeBlock ) self.willAppearInScrollViewExeBlock(self);
    };
    edgeControlLayer.videoPlayerWillDisappearInScrollView = ^{
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( self.willDisappearInScrollView ) self.willDisappearInScrollView(self);
    };
    [self.switcher addControlLayer:[[SJControlLayerCarrier alloc] initWithIdentifier:SJControlLayer_Edge controlLayer:edgeControlLayer]];
    return self;
}
@end
