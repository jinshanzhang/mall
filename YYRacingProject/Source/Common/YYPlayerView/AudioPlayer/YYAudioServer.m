//
//  YYAudioServer.m
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/29.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import "YYAudioServer.h"
#import <SJObserverHelper/NSObject+SJObserverHelper.h>

NS_ASSUME_NONNULL_BEGIN
@implementation YYAudioServer
+ (instancetype)shared {
    static id _instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [self new];
    });
    return _instance;
}

@synthesize player = _player;
- (void)setPlayer:(nullable YYPlayer *)player {
    _player = player;
    [self _configPlayer:player];
}

- (YYPlayer *)player {
    if ( _player ) return _player;
    _player = [YYPlayer player];
    [self _configPlayer:_player];
    return _player;
}

- (NSTimeInterval)currentTime {
    return self.player.currentTime;
}
- (NSTimeInterval)duration {
    return self.player.totalTime;
}

- (SJVideoPlayerPlayStatus)playStatus {
    return self.player.playStatus;
}

- (void)play {
    [self.player play];
}
- (void)pause {
    [self.player pause];
}
- (void)stop {
    [self.player stop];
}

static NSString *kPlayStatus = @"kPlayStatus";
- (void)_configPlayer:(YYPlayer *)player {
    if ( !player ) return;
    __weak typeof(self) _self = self;
    player.playTimeDidChangeExeBlok = ^(__kindof SJBaseVideoPlayer * _Nonnull videoPlayer) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( self.currentTimeDidChangeExeBlock ) self.currentTimeDidChangeExeBlock(self);
    };
    
    player.playDidToEndExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
        __strong typeof(_self) self = _self;
        if ( !self ) return;
        if ( self.didPlayToEndExeBlock ) self.didPlayToEndExeBlock(self);
    };
    
    [player sj_addObserver:self forKeyPath:@"playStatus" context:&kPlayStatus];
}

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<NSKeyValueChangeKey,id> *)change context:(nullable void *)context {
    if ( context == &kPlayStatus ) {
        if ( _playStatusDidChangeExeBlock ) _playStatusDidChangeExeBlock(self);
    }
}

- (void)playWithAsset:(SJVideoPlayerURLAsset *)asset {
    self.player.URLAsset = asset;
}
@end
NS_ASSUME_NONNULL_END
