//
//  YYAudioManagerClass.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/29.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYAudioManagerClass.h"

@interface YYAudioManagerClass()

@property (nonatomic, assign) BOOL  isManualClose;  //是否手动关闭
@property (nonatomic, strong) EAMiniAudioPlayerView *playerView;

@end

@implementation YYAudioManagerClass

+ (instancetype)shared {
    static id _instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [self new];
    });
    return _instance;
}

- (void)createAudioView:(YYPlayer *)player
              showModel:(YYAudioInfoModel *)showModel {
    if (player) {
        @weakify(self);
        dispatch_async(dispatch_get_main_queue(), ^{
            @strongify(self);
            CGFloat x = (kScreenW - kSizeScale(350))/2;
            CGFloat y = kScreenH;
            [self setReleaseAudio];
            YYAudioServer.shared.player = player;
            [self removeRepeatAddView];
            if (!self.playerView) {
                EAMiniAudioPlayerView *miniView = [[EAMiniAudioPlayerView alloc] initWithFrame:CGRectMake(x, y, kSizeScale(350), kSizeScale(45))];
                [[YYCommonTools getCurrentVC].view addSubview:miniView];
                self.playerView = miniView;
                self.lastPlayPoint = 0;
            }
            self.playerView.alpha = 0.0;
            self.isExist = YES;
            self.showStatus = self.playerView.alpha > 0.0 ? YES : NO;
            if (showModel) {
                self.playerView.videoCoverUrl = showModel.audioHeaderUrl;
                self.playerView.videoTitle = showModel.titleStr;
                self.playerView.videoSubTitle = showModel.detailTitleStr;
                self.playerView.videoDurationStr = showModel.timeStr;
            }
            
            YYAudioServer.shared.currentTimeDidChangeExeBlock = ^(YYAudioServer * _Nonnull server) {
                @strongify(self);
                self.playerView.playProgress = server.currentTime/server.duration;
                self.lastPlayPoint = server.currentTime;
            };
            
            YYAudioServer.shared.playStatusDidChangeExeBlock = ^(YYAudioServer * _Nonnull server) {
                @strongify(self);
                if (server.playStatus == SJVideoPlayerPlayStatusPaused) {
                    self.playerView.playStatus = MiniAudioPlayStatusPause;
                }
                else if (server.playStatus == SJVideoPlayerPlayStatusPlaying) {
                    self.playerView.playStatus = MiniAudioPlayStatusPlay;
                    if (self.isManualClose) {
                        self.isManualClose = NO;
                    }
                }
            };
            
            self.playerView.contentClickBlock = ^{
                // 如果是当前页面，就不做跳转
                if (![[[YYCommonTools getCurrentVC] className] isEqualToString:@"YY_AudioDetailViewController"]) {
                    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                    [dic setObject:showModel.lessonID forKey:@"lessonID"];
                    [dic setObject:@"miniVideo" forKey:@"skipPageType"];
                    [YYCommonTools skipAudioWith:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:dic];
                }
            };
            
            self.playerView.playAndPuseAudioPlayView = ^(MiniAudioPlayStatus playStatus) {
                if (playStatus == MiniAudioPlayStatusPause || playStatus == MiniAudioPlayStatusPrepare) {
                    [YYAudioServer.shared.player pause];
                }
                else if (playStatus == MiniAudioPlayStatusPlay ) {
                    [YYAudioServer.shared.player play];
                }
            };
            
            self.playerView.closeAudioPlayView = ^(EAMiniAudioPlayerView *view){
                @strongify(self);
                [self handlerAudioHideAndShow:NO];
                self.isManualClose = YES;
            };
        });
    }
}

- (BOOL)getAudioHideStatu {
    return self.playerView.alpha > 0.0 ? YES : NO;
}

- (void)handlerAudioHideAndShow:(BOOL)show {
    __block BOOL isExit = NO;
    __block UIView *tempView = nil;
    // 判断是否已经添加， 如果有移除
    NSArray *subViews = [[YYCommonTools getCurrentVC].view subviews];
    [subViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *view = (UIView *)obj;
        if (view.tag == 222) {
            tempView = view;
            isExit = YES;
            *stop = YES;
        }
    }];
    if (tempView) {
        [tempView removeFromSuperview];
    }
    if (!self.isExist || self.isManualClose) {
        return;
    }
    // 判断当前页面是否在可添加页面上。
    if ([self.needFilterShowClassNames containsObject:[[YYCommonTools getCurrentVC] className]]) {
        if ([[[YYCommonTools getCurrentVC] className] isEqualToString:@"YY_GoodDetailViewController"]) {
            __block UIView *bottomView = nil;
            NSArray *subViews = [[YYCommonTools getCurrentVC].view subviews];
            [subViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                UIView *view = (UIView *)obj;
                if (view.tag == 225) {
                    bottomView = view;
                    *stop = YES;
                }
            }];
            if (bottomView) {
                [[YYCommonTools getCurrentVC].view insertSubview:self.playerView belowSubview:bottomView];
            }
        }
        else {
            [[YYCommonTools getCurrentVC].view addSubview:self.playerView];
        }
        
        __block BOOL isFilterUpdateFrame = NO;
        [_needFilterUpdateFrameClassNames enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *clsName = (NSString *)obj;
            if ([clsName isEqualToString:[[YYCommonTools getCurrentVC] className]]) {
                isFilterUpdateFrame = YES;
                *stop = YES;
            }
        }];
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             if (show) {
                                 self.playerView.transform = CGAffineTransformMakeTranslation(0, -(self.playerView.height+(isFilterUpdateFrame?kBottom(58):12)));
                                 self.playerView.alpha = 1.0;
                             }
                             else {
                                 self.playerView.transform = CGAffineTransformMakeTranslation(0, (self.playerView.height+(isFilterUpdateFrame?kBottom(58):12)));
                                 self.playerView.alpha = 0.0;
                             }
                         }];
        
        self.showStatus = self.playerView.alpha > 0.0 ? YES : NO;
    }
}

- (void)setNeedFilterShowClassNames:(NSMutableArray *)needFilterShowClassNames {
    _needFilterShowClassNames = needFilterShowClassNames;
    if (self.isExist) {
        __block BOOL isFilterShow = NO;
        [_needFilterShowClassNames enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *clsName = (NSString *)obj;
            if ([clsName isEqualToString:[[YYCommonTools getCurrentVC] className]]) {
                isFilterShow = YES;
                *stop = YES;
            }
        }];
        [self handlerAudioHideAndShow:isFilterShow];
    }
}

- (void)setReleaseAudio {
    self.isExist = NO;
    [YYAudioServer.shared setPlayer:nil];
    self.playerView = nil;
    [self removeRepeatAddView];
}

- (void)removeRepeatAddView {
    // 移除已添加页面的音频组件。
    __block BOOL isExit = NO;
    __block UIView *tempView = nil;
    NSArray *subViews = [[YYCommonTools getCurrentVC].view subviews];
    [subViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *view = (UIView *)obj;
        if (view.tag == 222) {
            tempView = view;
            isExit = YES;
            *stop = YES;
        }
    }];
    if (tempView) {
        [tempView removeFromSuperview];
    }
}
@end
