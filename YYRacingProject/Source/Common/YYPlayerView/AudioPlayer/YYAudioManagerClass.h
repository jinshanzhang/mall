//
//  YYAudioManagerClass.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/29.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYAudioServer.h"
#import "EAMiniAudioPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYAudioManagerClass : NSObject

@property (nonatomic, assign) BOOL isExist; //是否存在
@property (nonatomic, assign) BOOL showStatus;
@property (nonatomic, assign) NSTimeInterval lastPlayPoint;//上次播放到的位置
@property (nonatomic, strong) NSMutableArray *needFilterShowClassNames; //筛选需要显示的页面
@property (nonatomic, strong) NSMutableArray *needFilterUpdateFrameClassNames; //筛选需要特殊处理高度的页面

+ (instancetype)shared;

// 创建音频组件
- (void)createAudioView:(YYPlayer *)player
              showModel:(YYAudioInfoModel *)showModel;
// 音频组件是否显示
- (void)handlerAudioHideAndShow:(BOOL)show;
// 释放音频组件
- (void)setReleaseAudio;
@end

NS_ASSUME_NONNULL_END
