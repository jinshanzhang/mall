//
//  YYAudioServer.h
//  AliVODPlayer
//
//  Created by BlueDancer on 2018/11/29.
//  Copyright © 2018 SanJiang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYPlayer.h"
#import "SJVideoPlayerURLAsset+AliVodAdd.h"

NS_ASSUME_NONNULL_BEGIN
@interface YYAudioServer : NSObject
+ (instancetype)shared;
@property (nonatomic, strong, null_resettable) YYPlayer *player;

@property (nonatomic, readonly) NSTimeInterval currentTime;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, copy, nullable) void(^currentTimeDidChangeExeBlock)(YYAudioServer *server);
@property (nonatomic, copy, nullable) void(^didPlayToEndExeBlock)(YYAudioServer *server);

@property (nonatomic, readonly) SJVideoPlayerPlayStatus playStatus;
@property (nonatomic, copy, nullable) void(^playStatusDidChangeExeBlock)(YYAudioServer *server);

- (void)playWithAsset:(SJVideoPlayerURLAsset *)asset;
- (void)play;
- (void)pause;
- (void)stop;

@end
NS_ASSUME_NONNULL_END
