//
//  YYLoadingView.h
//  YIYanProject
//
//  Created by cjm on 2018/4/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYLoadingView : UIView

/** 一次动画所持续时长 默认2秒*/
@property(nonatomic,assign)NSTimeInterval duration;
/** 线条颜色*/
@property (nonatomic, strong) UIColor *strokeColor;
/** 线条粗细**/
@property (nonatomic, assign) CGFloat  lineWidth;
/** 是否能用*/
@property(nonatomic,assign,getter=isEnable)BOOL enable;

/**
 开始动画
 */
- (void)starAnimation;
/**
 停止动画
 */
- (void)stopAnimation;

@end
