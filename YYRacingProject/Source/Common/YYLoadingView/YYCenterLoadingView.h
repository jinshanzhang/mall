//
//  YYInnerLoadingView.h
//  YIYanProject
//
//  Created by cjm on 2018/4/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYCenterLoadingView : UIView

@property (nonatomic, strong) YYLoadingView *loadingView;
@property (nonatomic, strong) UIImageView *centerImageView;  //中心图片
@property (nonatomic, strong) UILabel     *loadingTipLabel;  //加载提示

@property (nonatomic, assign) BOOL        isShowTip;
@end
