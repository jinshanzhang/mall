//
//  YYInnerLoadingView.m
//  YIYanProject
//
//  Created by cjm on 2018/4/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCenterLoadingView.h"

@interface YYCenterLoadingView()

@end

@implementation YYCenterLoadingView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isShowTip = NO;
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 5;
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.isShowTip) {
        [self.loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.mas_equalTo(kSizeScale(15));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(40)));
        }];
        
        [self.centerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.loadingView);
            make.size.mas_equalTo(CGSizeMake(15, 18));
        }];
        
        [self.loadingTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.loadingView);
            make.height.mas_equalTo(15).priorityHigh();
            make.bottom.equalTo(self.mas_bottom).offset(-10);
        }];
    }
    else {
        [self.loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(40)));
        }];
        
        [self.centerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(15, 18));
        }];
    }
}

- (void)createUI {
    self.loadingView = [[YYLoadingView alloc] initWithFrame:CGRectZero];
    self.loadingView.strokeColor = XHRedColor;
    [self addSubview:self.loadingView];

    self.centerImageView = [YYCreateTools createImageView:@"loading_icon"
                                                viewModel:-1];
    [self addSubview:self.centerImageView];
    
    self.loadingTipLabel = [YYCreateTools createLabel:@"二维码生成中..."
                                                 font:normalFont(11)
                                            textColor:XHBlackMidColor];
    [self addSubview:self.loadingTipLabel];
}

@end
