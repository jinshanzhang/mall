//
//  YYRacingBeckRefreshHeader.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/15.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MJRefreshHeader.h"

@interface YYRacingBeckRefreshHeader : MJRefreshHeader

@property (nonatomic, assign) BOOL show;

@end
