//
//  YYQRCodeCenterLoading.h
//  YIYanProject
//
//  Created by cjm on 2018/6/28.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYQRCodeCenterLoading : UIView
WMSingletonH(YYQRCodeCenterLoading);

// 在window 上显示隐藏
+ (void)showCenterLoading:(NSString *)loadingTip
                superView:(UIView *)superView;
+ (void)hideCenterLoading:(NSString *)loadingTip
                superView:(UIView *)superView;

@end
