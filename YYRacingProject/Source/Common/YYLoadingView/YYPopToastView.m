//
//  YYPopToastView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/6.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYPopToastView.h"

@interface  ToastView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *toastImageView;
@property (nonatomic, copy) void (^toastClickBlock)(void);

@end

@implementation ToastView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
        self.backgroundColor = XHClearColor;
        self.tag = 888;
        /*self.layer.shadowOffset = CGSizeZero;
        self.layer.shadowColor = XHBlackColor.CGColor;//shadowColor阴影颜色
        self.layer.shadowOpacity = 0.3;//阴影透明度，默认0
        //self.layer.shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(-4, -4, self.width+8, self.height+8)].CGPath;
        self.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(-4, -4, self.width+8, self.height+8) cornerRadius:self.height/2.0].CGPath;
        //self.layer.masksToBounds = YES;
        self.layer.cornerRadius = self.height/2.0;
        */
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toastClickEvent:)]];
    }
    return self;
}

- (void)createUI {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = XHBlackColor;
    self.titleLabel.font = normalFont(14);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    //[self addSubview:self.titleLabel];
    //self.titleLabel.frame = CGRectMake(0, 0, self.bounds.size.width-10, self.bounds.size.height-10);
    //self.titleLabel.center = self.center;
    
    self.toastImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"share_screening_icon"]];
    self.toastImageView.userInteractionEnabled = YES;
    self.toastImageView.center = self.center;
    self.toastImageView.bounds = CGRectMake(0, 0, kSizeScale(97), kSizeScale(33));
    [self addSubview:self.toastImageView];
}

#pragma mark -Event
- (void)toastClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.toastClickBlock) {
        self.toastClickBlock();
    }
}

@end


@interface YYPopToastView ()

@property (nonatomic, strong) ToastView *toastView;
@property (nonatomic, strong) UIView    *referView; //参考视图
@property (nonatomic, strong) UIView    *superView; //父视图
@property (nonatomic, assign) BOOL      isToastHide;

@end


@implementation YYPopToastView

+ (instancetype)shared {
    static id _instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [self new];
    });
    return _instance;
}

- (void)showToastView:(UIView *)superView
            referView:(UIView *)referView
        showCondition:(BOOL)showCondition
           clickBlock:(void(^)(void))clickBlock {
    self.referView = referView;
    [self showToastView:self.superView
          showCondition:showCondition
             clickBlock:clickBlock];
}

- (void)showToastView:(UIView *)superView
        showCondition:(BOOL)showCondition
           clickBlock:(void(^)(void))clickBlock {
    CGFloat width = kSizeScale(102), height = kSizeScale(33);
    self.superView = superView;
    if (!self.toastView) {
        self.toastView = [[ToastView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    }
    self.toastView.toastClickBlock = ^{
        clickBlock();
    };
    if (self.toastView) {
        if (self.toastView.superview) {
            [self.toastView removeFromSuperview];
        }
        self.toastView.centerX = superView.centerX;
        [self.superView addSubview:self.toastView];
        [self.superView sendSubviewToBack:self.toastView];
    }
    if (showCondition) {
        [self showToastAnimation];
    }
}

- (void)showToastAnimation {
    [UIView animateWithDuration:0.5
                     animations:^{
                         CGRect frame = self.toastView.frame;
                         frame.origin.y = self.toastView.superview.frame.size.height + kSizeScale(15);
                         self.toastView.frame = frame;
                         self.isToastHide = NO;
                     }];
}

- (void)hideToastAnimation {
    if (!self.toastView || self.isToastHide) {
        return;
    }
    [UIView animateWithDuration:0.2
                     animations:^{
                         CGRect frame = self.toastView.frame;
                         frame.origin.y = 0;
                         self.toastView.frame = frame;
                         self.isToastHide = YES;
                     }];
}

@end
