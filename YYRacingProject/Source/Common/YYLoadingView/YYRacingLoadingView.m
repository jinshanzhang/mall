//
//  YYRacingLoadingView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/15.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYRacingLoadingView.h"

@interface YYRacingLoadingView()

@property (nonatomic, strong) UIView      *backgroundView;
@property (nonatomic, strong) UIImageView *loadingView;
@property (nonatomic, strong) UILabel     *loadingTipLabel; //加载提示

@end

@implementation YYRacingLoadingView

#pragma mark - Setter method

- (void)setTopPadding:(CGFloat)topPadding {
    _topPadding = topPadding;
}

- (void)setItemPadding:(CGFloat)itemPadding {
    _itemPadding = itemPadding;
}

- (void)setDuration:(NSTimeInterval)duration {
    _duration = duration;
    self.loadingView.animationDuration = _duration;
}

- (void)setLoadingTip:(NSString *)loadingTip {
    _loadindTip = loadingTip;
    self.loadingTipLabel.text = _loadindTip;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _duration = 2.;
        [self creaeteLoadingUI];
    }
    return self;
}

- (void)creaeteLoadingUI {

    self.backgroundView = [YYCreateTools createView:XHWhiteColor];
    self.backgroundView.layer.cornerRadius = 4;
    self.backgroundView.clipsToBounds = YES;
    [self addSubview:self.backgroundView];
    
    self.loadingView = [[UIImageView alloc] init];
    [self.backgroundView addSubview:self.loadingView];
    
    self.loadingView.animationImages = [self initialImageArray];
    self.loadingView.animationDuration = self.duration;
    self.loadingView.animationRepeatCount = MAXFLOAT;
    
    self.loadingTipLabel = [YYCreateTools createLabel:nil
                                                 font:midFont(12)
                                            textColor:XHBlackMidColor];
    self.loadingTipLabel.numberOfLines = 1;
    self.loadingTipLabel.preferredMaxLayoutWidth = kSizeScale(100);
    self.loadingTipLabel.textAlignment = NSTextAlignmentCenter;
    [self.backgroundView addSubview:self.loadingTipLabel];
}

- (void)layoutSubviews {
    [self.backgroundView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.mas_equalTo(self.frame.size);
    }];
    [self.loadingView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backgroundView).offset(kSizeScale(self.topPadding));
        make.centerX.equalTo(self.backgroundView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(28), kSizeScale(28)));
    }];
    if (kValidString(self.loadindTip)) {
        self.loadingTipLabel.text = self.loadindTip;
        [self.loadingTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.backgroundView);
        make.top.equalTo(self.loadingView.mas_bottom).offset(kSizeScale(self.itemPadding));
        }];
    }
}

#pragma mark - Private method
/**组合动画所有图片**/
- (NSMutableArray *)initialImageArray {
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 8; i++) {
        NSString *imageName = [NSString stringWithFormat:@"loading-%d", i];
        UIImage *image = [UIImage imageNamed:imageName];
        [imageArray addObject:image];
    }
    return imageArray;
}

#pragma mark - Public method
- (void)starAnimation {
    [self.loadingView startAnimating];
}

- (void)stopAnimation {
    [self.loadingView stopAnimating];
}
@end
