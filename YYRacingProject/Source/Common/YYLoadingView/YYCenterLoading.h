//
//  YYCenterLoading.h
//  YIYanProject
//
//  Created by cjm on 2018/5/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYCenterLoading : UIView
WMSingletonH(YYCenterLoading);

+ (void)showCenterLoading;
+ (void)hideCenterLoading;
+ (void)showCenterLoading:(BOOL)isGoodDetail;

+ (void)showCenterLoadingInWindow;
+ (void)hideCenterLoadingInWindow;
@end
