//
//  YYCenterLoading.m
//  YIYanProject
//
//  Created by cjm on 2018/5/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCenterLoading.h"
//#import "YYCenterLoadingView.h"
#import "YYRacingLoadingView.h"

@interface YYCenterLoading()
//@property (nonatomic, strong) YYCenterLoadingView *centerView;
@property (nonatomic, strong) YYRacingLoadingView *centerView;
@property (nonatomic, assign) BOOL    isGoodDetail;
@property (nonatomic, assign) CGSize  loadingViewSize;
@end

@implementation YYCenterLoading
WMSingletonM(YYCenterLoading);

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isGoodDetail = NO;
        self.loadingViewSize = CGSizeMake(kSizeScale(63), kSizeScale(63));
        self.centerView = [[YYRacingLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.loadingViewSize.width, self.loadingViewSize.height)];

        self.centerView.topPadding = (kSizeScale(63)-kSizeScale(28))/2.0;
        self.centerView.duration = 1.0;
        [self addSubview:self.centerView];
    }
    return self;
}

- (void)setIsGoodDetail:(BOOL)isGoodDetail {
    _isGoodDetail = isGoodDetail;
}

- (void)setLoadingViewSize:(CGSize)loadingViewSize {
    _loadingViewSize = loadingViewSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat height = self.frame.size.height;
    if (height == kScreenH) {
        if (self.isGoodDetail) {//单独处理商品详情
            height = (height)/2.0 - kStatuH;
        }
        else {
            height = (height)/2.0 - kStatuH;
        }
    }
    else if (height == kScreenH-kNavigationH) {
        height = (height)/2.0;
    }
    else {
        height = (height)/2.0;  //237.5
    }
    [self.centerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(height);
        make.size.mas_equalTo(self.loadingViewSize);
    }];
}
/*
 老的加载处理
 - (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isGoodDetail = NO;
        self.loadingViewSize = CGSizeMake(kSizeScale(63), kSizeScale(63));
        self.backgroundColor = XHClearColor;
        [self createUI];
    }
    return self;
}

- (void)setIsGoodDetail:(BOOL)isGoodDetail {
    _isGoodDetail = isGoodDetail;
}

- (void)setLoadingViewSize:(CGSize)loadingViewSize {
    _loadingViewSize = loadingViewSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat height = self.frame.size.height;
    if (height == kScreenH) {
        if (self.isGoodDetail) {//单独处理商品详情
            height = (height)/2.0 - kStatuH;
        }
        else {
            height = (height)/2.0 - kStatuH;
        }
    }
    else if (height == kScreenH-kNavigationH) {
        height = (height)/2.0;
    }
    else {
        height = (height)/2.0;  //237.5
    }
    [self.centerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(height);
        make.size.mas_equalTo(self.loadingViewSize);
    }];
}
 
 - (void)createUI {
 self.centerView = [[YYCenterLoadingView alloc] initWithFrame:CGRectZero];
 [self addSubview:self.centerView];
 }
*/

+ (void)showCenterLoading {
    UIViewController *ctrl = [YYCommonTools getCurrentVC];
    YYCenterLoading *hud   = [YYCenterLoading sharedYYCenterLoading];
    [hud.centerView starAnimation];
    //[hud.centerView.loadingView starAnimation];
    
    CGRect frame = ctrl.view.frame;
    frame.origin.y = 0;
    hud.frame = frame;
    [ctrl.view addSubview:hud];
}

+ (void)showCenterLoading:(BOOL)isGoodDetail {
    UIViewController *ctrl = [YYCommonTools getCurrentVC];
    YYCenterLoading *hud   = [YYCenterLoading sharedYYCenterLoading];
    hud.isGoodDetail = YES;
    [hud.centerView starAnimation];
    //[hud.centerView.loadingView starAnimation];
    
    CGRect frame = ctrl.view.frame;
    frame.origin.y = 0;
    hud.frame = frame;
    [ctrl.view addSubview:hud];
}

+ (void)showCenterLoadingInWindow {
    YYCenterLoading *hud   = [YYCenterLoading sharedYYCenterLoading];
    hud.isGoodDetail = YES;
    [hud.centerView starAnimation];
    //[hud.centerView.loadingView starAnimation];
    
    hud.frame = kAppWindow.frame;
    [kAppWindow addSubview:hud];
}

+ (void)hideCenterLoading {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIViewController *ctrl = [YYCommonTools getCurrentVC];
        YYCenterLoading *hud   = [YYCenterLoading sharedYYCenterLoading];
        hud.isGoodDetail = NO;
        [hud.centerView stopAnimation];
        //[hud.centerView.loadingView stopAnimation];
        
        CGRect frame = ctrl.view.frame;
        frame.origin.y = 0;
        hud.frame = frame;
        [hud removeFromSuperview];
    });
}

+ (void)hideCenterLoadingInWindow {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        YYCenterLoading *hud   = [YYCenterLoading sharedYYCenterLoading];
        hud.isGoodDetail = NO;
        [hud.centerView stopAnimation];
        //[hud.centerView.loadingView stopAnimation];
        
        hud.frame = kAppWindow.frame;
        [hud removeFromSuperview];
    });
}


@end
