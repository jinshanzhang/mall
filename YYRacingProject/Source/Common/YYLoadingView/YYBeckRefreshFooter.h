//
//  YYBeckRefreshFooter.h
//  YIYanProject
//
//  Created by cjm on 2018/5/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MJRefreshFooter.h"

@interface YYBeckRefreshFooter : MJRefreshAutoFooter

@property (nonatomic, strong) UILabel       *contentLbl;

@end
