//
//  YYPopToastView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/6.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYPopToastView : NSObject

+ (instancetype)shared;

- (void)showToastAnimation;
- (void)hideToastAnimation;

- (void)showToastView:(UIView *)superView
        showCondition:(BOOL)showCondition
           clickBlock:(void(^)(void))clickBlock;

- (void)showToastView:(UIView *)superView
            referView:(UIView *)referView
        showCondition:(BOOL)showCondition
           clickBlock:(void(^)(void))clickBlock;

@end

NS_ASSUME_NONNULL_END
