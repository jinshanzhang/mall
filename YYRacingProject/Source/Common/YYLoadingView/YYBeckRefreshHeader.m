//
//  YYBeckRefreshHeader.m
//  YIYanProject
//
//  Created by cjm on 2018/4/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBeckRefreshHeader.h"
@interface YYBeckRefreshHeader()

@property (nonatomic, strong) UIImageView   *refreshLogoView;
@property (nonatomic, strong) UIImageView   *downArrowView;  //向下图标
@property (nonatomic, strong) YYLoadingView *loadView;

@property (nonatomic, assign) BOOL           isAlreadyShow; //是否已经显示
@property (nonatomic, assign) BOOL           hasRefreshed;  //是否已经刷新了
@end

@implementation YYBeckRefreshHeader

#pragma mark - Life cycle
/**初始化配置**/
- (void)prepare {
    [super prepare];
    
    self.hasRefreshed = NO;
    self.isAlreadyShow = NO;
    [self addSubview:self.refreshLogoView];
    [self addSubview:self.downArrowView];
    [self addSubview:self.loadView];
    
    self.loadView.lineWidth = 1;
    self.loadView.duration = 3;
    self.loadView.strokeColor = XHRedColor;
    [self.loadView starAnimation];
}

-(void)setShow:(BOOL)show
{
    self.refreshLogoView.hidden = YES;
    self.downArrowView.hidden = YES;
    self.loadView.hidden = YES;
}


/**设置控件的布局**/
- (void)placeSubviews {
    [super placeSubviews];
    self.refreshLogoView.center = CGPointMake(self.mj_w/2.0, (self.mj_h/2.0) - 44);
    self.refreshLogoView.bounds = CGRectMake(0, 0, 106, 92);
    
    self.downArrowView.center = CGPointMake(self.refreshLogoView.center.x, self.refreshLogoView.center.y + 55);
    self.downArrowView.bounds = CGRectMake(0, 0, 18, 18);
}

#pragma mark  - Setter method
- (UIImageView *)refreshLogoView {
    if (!_refreshLogoView) {
        _refreshLogoView = [[UIImageView alloc] init];
        _refreshLogoView.image = [UIImage imageNamed:@"loading_refresh_logo_icon"];
    }
    return _refreshLogoView;
}

- (UIImageView *)downArrowView {
    if (!_downArrowView) {
        _downArrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading_refresh_down_icon"]];
    }
    return _downArrowView;
}

- (YYLoadingView *)loadView {
    if (!_loadView) {
        _loadView = [[YYLoadingView alloc] initWithFrame:CGRectMake((kScreenW-18)/2.0, (self.mj_h/2.0)+2, 18, 18)];
    }
    return _loadView;
}

- (void)setPullingPercent:(CGFloat)pullingPercent {
    NSString *tempPercent = [NSString stringWithFormat:@"%.1f",pullingPercent];
    pullingPercent = [tempPercent floatValue];
    if (!self.hasRefreshed) {
        if ([tempPercent rangeOfString:@"-"].location != NSNotFound) {
            //处理返回-0.0000000  的问题
            return;
        }
        if (pullingPercent == 0.0) {
            self.downArrowView.hidden = NO;
            self.isAlreadyShow = YES;
        }
        else {
            if (pullingPercent <= 0.8) {
                self.downArrowView.image = [UIImage imageNamed:@"loading_refresh_down_icon"];
                [UIView animateWithDuration:0.2 animations:^{
                    self.downArrowView.transform = CGAffineTransformIdentity;
                }];
            }
            else {
                [UIView animateWithDuration:0.2 animations:^{
                    self.downArrowView.transform = CGAffineTransformMakeRotation(M_PI);
                }];
            }
        }
    }
    else {
        self.hasRefreshed = NO;
        self.isAlreadyShow = NO;
    }
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
        {
            self.loadView.hidden = YES;
            if (self.loadView.enable) {
                [self.loadView stopAnimation];
            }
        }
            break;
        case MJRefreshStatePulling:
            break;
        case MJRefreshStateRefreshing: {
            //开启动画
            self.loadView.hidden = NO;
            if (!self.loadView.enable) {
                [self.loadView starAnimation];
            }
            self.downArrowView.hidden = YES;
        
            self.hasRefreshed = YES;
        }
            break;
        default:
            break;
    }
}
@end
