//
//  YYRacingLoadingView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/15.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYRacingLoadingView : UIView

/** 一次动画所持续时长 默认2秒*/
@property (nonatomic, assign) NSTimeInterval duration;

@property (nonatomic, assign) CGFloat       topPadding; // 距离顶部间隔
@property (nonatomic, assign) CGFloat       itemPadding;// item 间隔

// 加载提示
@property (nonatomic, strong) NSString      *loadindTip;
/**
 开始动画
 */
- (void)starAnimation;
/**
 停止动画
 */
- (void)stopAnimation;

@end
