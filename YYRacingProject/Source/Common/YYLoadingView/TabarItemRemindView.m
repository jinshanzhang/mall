//
//  TabarItemRemindView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "TabarItemRemindView.h"

@interface TabarItemRemindView()

@property (nonatomic, strong) YYAnimatedImageView *remindImageView;

@end

@implementation TabarItemRemindView

#pragma mark - Setter method
- (void)setRemindURL:(NSString *)remindURL {
    _remindURL = remindURL;
    if (kValidString(_remindURL)) {
        [self.remindImageView setImageURL:[NSURL URLWithString:_remindURL]];
    }
}

#pragma mark - Life cycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.remindImageView = [[YYAnimatedImageView alloc] init];
        self.remindImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.remindImageView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.remindImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

@end
