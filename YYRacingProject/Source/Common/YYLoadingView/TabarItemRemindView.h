//
//  TabarItemRemindView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabarItemRemindView : UIView

@property (nonatomic, strong) NSString *remindURL;

@end
