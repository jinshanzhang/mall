//
//  YYRacingBeckRefreshHeader.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/15.
//  Copyright © 2018年 cjm. All rights reserved.
//
#import "YYRacingBeckRefreshHeader.h"
@interface YYRacingBeckRefreshHeader()

@property (nonatomic, strong) UIImageView *refreshImageView;
@property (nonatomic, strong) UILabel     *refreshTipLabel;

@end

@implementation YYRacingBeckRefreshHeader
#pragma mark - Setter method
- (UIImageView *)refreshImageView {
    if (!_refreshImageView) {
        _refreshImageView = [[UIImageView alloc] init];
        _refreshImageView.animationImages = [self initialImageArray];
        _refreshImageView.animationDuration = 2;
        _refreshImageView.animationRepeatCount = MAXFLOAT;
    }
    return _refreshImageView;
}

- (UILabel *)refreshTipLabel {
    if (!_refreshTipLabel) {
        _refreshTipLabel = [YYCreateTools createLabel:@"爱好物 享生活"
                                                 font:midFont(10)
                                            textColor:XHWhiteColor];
        _refreshTipLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _refreshTipLabel;
}

#pragma mark - Life cycle
/**初始化配置**/
- (void)prepare {
    [super prepare];
    
    self.refreshImageView.image  = [UIImage imageNamed:@"down_pull-1"];
    [self addSubview:self.refreshImageView];
    [self addSubview:self.refreshTipLabel];
}

/**设置控件的布局**/
- (void)placeSubviews {
    [super placeSubviews];

    self.refreshImageView.center = CGPointMake(self.mj_w/2.0, (self.mj_h/2.0)-kSizeScale(10));
    self.refreshImageView.bounds = CGRectMake(0, 0, kSizeScale(28), kSizeScale(28));
    
    self.refreshTipLabel.center = CGPointMake(self.mj_w/2.0, (self.mj_h/2.0)+kSizeScale(18));
    self.refreshTipLabel.bounds = CGRectMake(0, 0, kSizeScale(150), kSizeScale(20));
}


- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    switch (state) {
        case MJRefreshStateIdle: {
            //停止
            [self.refreshImageView stopAnimating];
        }
            break;
        case MJRefreshStatePulling: {
            [self.refreshImageView startAnimating];
        }
            break;
        case MJRefreshStateRefreshing: {
            //开启动画
            [self.refreshImageView startAnimating];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Private method
/**组合动画所有图片**/
- (NSMutableArray *)initialImageArray {
    NSMutableArray *imageArray = [[NSMutableArray alloc] init];
    for (int i = 1; i <= 4; i++) {
        NSString *imageName = [NSString stringWithFormat:@"down_pull-%d", i];
        UIImage *image = [UIImage imageNamed:imageName];
        [imageArray addObject:image];
    }
    return imageArray;
}


@end
