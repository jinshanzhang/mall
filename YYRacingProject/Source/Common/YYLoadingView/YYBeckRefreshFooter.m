//
//  YYBeckRefreshFooter.m
//  YIYanProject
//
//  Created by cjm on 2018/5/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBeckRefreshFooter.h"

@interface YYBeckRefreshFooter()

@property (nonatomic, strong) YYLoadingView *loadView;

@end


@implementation YYBeckRefreshFooter

#pragma mark - Life cycle
/**初始化配置**/
- (void)prepare {
    [super prepare];
    
    [self addSubview:self.loadView];
    [self addSubview:self.contentLbl];
    
    self.loadView.lineWidth = 0.5;
    self.loadView.duration = 3;
    self.loadView.strokeColor = XHRedColor;
    [self.loadView starAnimation];
}

/**设置控件的布局**/
- (void)placeSubviews {
    [super placeSubviews];
    self.contentLbl.frame = CGRectMake((kScreenW-18)/2.0, (self.mj_h - 20 ) / 2.0,kScreenW, 20);
    self.contentLbl.centerX = self.centerX;
}

#pragma mark - Setter method

- (YYLoadingView *)loadView {
    if (!_loadView) {
        _loadView = [[YYLoadingView alloc] initWithFrame:CGRectMake((kScreenW-18)/2.0 - 30, (self.mj_h/2.0), 18, 18)];
    }
    return _loadView;
}

- (UILabel *)contentLbl {
    if (!_contentLbl) {
//        @"我是有底线的"
        _contentLbl = [YYCreateTools createLabel:@"上拉加载更多"
                                            font:normalFont(10)
                                       textColor:XHBlackLitColor];
        _contentLbl.textAlignment = NSTextAlignmentCenter;
    }
    return _contentLbl;
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
        {
            self.loadView.hidden = YES;
            if (self.loadView.enable) {
                [self.loadView stopAnimation];
            }
        }
            break;
        case MJRefreshStatePulling:
            
            break;
        case MJRefreshStateRefreshing: {
            //开启动画
            self.loadView.hidden = NO;
            if (!self.loadView.enable) {
                [self.loadView starAnimation];
            }
        }
            break;
        default:
            break;
    }
}

@end
