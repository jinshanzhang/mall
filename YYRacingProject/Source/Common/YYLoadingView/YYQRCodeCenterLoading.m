//
//  YYQRCodeCenterLoading.m
//  YIYanProject
//
//  Created by cjm on 2018/6/28.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYQRCodeCenterLoading.h"

//#import "YYCenterLoadingView.h"
#import "YYRacingLoadingView.h"
@interface YYQRCodeCenterLoading()

@property (nonatomic, assign) CGSize  loadingViewSize;
@property (nonatomic, strong) YYRacingLoadingView *centerView;

@end

@implementation YYQRCodeCenterLoading
WMSingletonM(YYQRCodeCenterLoading);

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHClearColor;
        
        self.loadingViewSize = CGSizeMake(kSizeScale(90), kSizeScale(90));
        self.centerView = [[YYRacingLoadingView alloc] initWithFrame:CGRectMake(0, 0, self.loadingViewSize.width, self.loadingViewSize.height)];
        self.centerView.topPadding = 18;
        self.centerView.itemPadding = 15;
        self.centerView.duration = 1.0;
        [self addSubview:self.centerView];
    }
    return self;
}

- (void)setLoadingViewSize:(CGSize)loadingViewSize {
    _loadingViewSize = loadingViewSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat height = self.frame.size.height;
    if (height == kScreenH) {
        height = (height)/2.0 - kStatuH;
    }
    else if (height == kScreenH-kNavigationH) {
        height = (height)/2.0;
    }
    else {
        height = (height)/2.0;  //237.5
    }
    [self.centerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(height);
        make.size.mas_equalTo(self.loadingViewSize);
    }];
}


+ (void)showCenterLoading:(NSString *)loadingTip
                superView:(UIView *)superView {
    UIViewController *ctrl = [YYCommonTools getCurrentVC];

    YYQRCodeCenterLoading *hud = [YYQRCodeCenterLoading sharedYYQRCodeCenterLoading];
    hud.centerView.loadindTip = kValidString(loadingTip)?loadingTip:@"店铺升级中...";
    [hud.centerView starAnimation];
    if (superView) {
        hud.frame = superView.frame;
        [superView addSubview:hud];
    }
    else {
        CGRect frame = ctrl.view.frame;
        frame.origin.y = 0;
        hud.frame = frame;
        [ctrl.view addSubview:hud];
    }
}

+ (void)hideCenterLoading:(NSString *)loadingTip
                superView:(UIView *)superView {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIViewController *ctrl = [YYCommonTools getCurrentVC];
        
        YYQRCodeCenterLoading *hud   = [YYQRCodeCenterLoading sharedYYQRCodeCenterLoading];
        hud.centerView.loadindTip = kValidString(loadingTip)?loadingTip:@"店铺升级中...";
        [hud.centerView stopAnimation];
    
        if (superView) {
            hud.frame = superView.frame;
            [hud removeFromSuperview];
        }
        else {
            CGRect frame = ctrl.view.frame;
            frame.origin.y = 0;
            hud.frame = frame;
            [hud removeFromSuperview];
        }
    });
}

@end
