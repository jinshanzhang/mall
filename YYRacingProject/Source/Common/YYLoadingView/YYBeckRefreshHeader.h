//
//  YYBeckRefreshHeader.h
//  YIYanProject
//
//  Created by cjm on 2018/4/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MJRefreshHeader.h"

@interface YYBeckRefreshHeader : MJRefreshHeader

@property (nonatomic, assign) BOOL show;

@end
