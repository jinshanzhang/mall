//
//  YYScrollViewToolClass.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYScrollViewToolClass : NSObject

/**
 * 解决当前页面多个scrollView 点击状态栏不能返回顶部的问题。
 */

+ (void)scrollViewScrollToTop;

@end

NS_ASSUME_NONNULL_END
