//
//  YYScrollViewToolClass.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYScrollViewToolClass.h"

@implementation YYScrollViewToolClass

/**
 * 解决当前页面多个scrollView 点击状态栏不能返回顶部的问题。
 */
+ (void)scrollViewScrollToTop {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self searchScrollViewInView:window];
}

/**
 *  状态栏点击
 */
+ (void)statusBarWindowClick {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [self searchScrollViewInView:window];
}

/**
 *  view是否显示在window上
 */
+ (BOOL)isShowingOnKeyWindow:(UIView *)view {
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    CGRect newFrame = [keyWindow convertRect:view.frame fromView:view.superview];
    CGRect winBounds = keyWindow.bounds;
    BOOL intersects = CGRectIntersectsRect(newFrame, winBounds);
    return !view.isHidden && view.alpha > 0.01 && view.window == keyWindow && intersects;
}

/**
 *  搜索在view 上的scrollView
 */
+ (void)searchScrollViewInView:(UIView *)supView {
    for (UIScrollView *subView in supView.subviews) {
        if ([subView isKindOfClass:[UIScrollView class]] && [self isShowingOnKeyWindow:supView]) {
            CGPoint offset = subView.contentOffset;
            // 将offset 的y轴还原成最开始的值
            offset.y = -subView.contentInset.top;
            [subView setContentOffset:offset animated:YES];
        }
        [self searchScrollViewInView:subView];
    }
}

@end
