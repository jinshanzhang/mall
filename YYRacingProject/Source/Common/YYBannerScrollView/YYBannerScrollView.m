//
//  YYBannerScrollView.m
//  YIYanProject
//
//  Created by cjm on 2018/6/1.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBannerScrollView.h"
#import <SDCycleScrollView/TAPageControl.h>
@interface YYBannerScrollView()
<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView      *m_scrollView;
@property (nonatomic, strong) TAPageControl     *m_pageControl;

@property (nonatomic, strong) UIImageView       *m_firstImageView;
@property (nonatomic, strong) UIImageView       *m_secondImageView;
@property (nonatomic, strong) UIImageView       *m_thirdImageView;

@property (nonatomic, assign) NSInteger         maxCount;
@property (nonatomic, assign) NSInteger         currentPage;
@property (nonatomic, strong) NSMutableArray    *bannerItems;
@end

@implementation YYBannerScrollView

#pragma mark - getter method
- (UIScrollView *)m_scrollView {
    if (!_m_scrollView) {
        _m_scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _m_scrollView.bounces = NO;
        _m_scrollView.delegate = self;
        _m_scrollView.pagingEnabled = YES;
        _m_scrollView.scrollsToTop = NO;
        _m_scrollView.showsVerticalScrollIndicator = NO;
        _m_scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _m_scrollView;
}

- (TAPageControl *)m_pageControl {
    if (!_m_pageControl) {
        _m_pageControl = [[TAPageControl alloc] initWithFrame:CGRectZero];
        [_m_pageControl addTarget:self action:@selector(pageChangeEvent) forControlEvents:UIControlEventValueChanged];
    }
    return _m_pageControl;
}

- (UIImageView *)m_firstImageView {
    if (!_m_firstImageView) {
        _m_firstImageView = [[UIImageView alloc] init];
        _m_firstImageView.contentMode = self.imageViewContentMode;
    }
    return _m_firstImageView;
}

- (UIImageView *)m_secondImageView {
    if (!_m_secondImageView) {
        _m_secondImageView = [[UIImageView alloc] init];
        _m_secondImageView.contentMode = self.imageViewContentMode;
    }
    return _m_secondImageView;
}

- (UIImageView *)m_thirdImageView {
    if (!_m_thirdImageView) {
        _m_thirdImageView = [[UIImageView alloc] init];
        _m_thirdImageView.contentMode = self.imageViewContentMode;
    }
    return _m_thirdImageView;
}

#pragma mark - Setter method

- (void)setCurrentPageDotImage:(UIImage *)currentPageDotImage {
    _currentPageDotImage = currentPageDotImage;
    self.m_pageControl.currentDotImage = _currentPageDotImage;
}

- (void)setPageDotImage:(UIImage *)pageDotImage {
    _pageDotImage = pageDotImage;
    self.m_pageControl.dotImage = _pageDotImage;
}

- (void)setPageControlDotSize:(CGSize)pageControlDotSize {
    _pageControlDotSize = pageControlDotSize;
    self.m_pageControl.dotSize = _pageControlDotSize;
}

- (void)setCurrentPage:(NSInteger)currentPage {
    _currentPage = currentPage;
    [self resetScroll];
}

- (void)setImageURLs:(NSMutableArray *)imageURLs {
    _imageURLs = imageURLs;
    NSInteger count = _imageURLs.count;
    self.maxCount = count;
    if (count > 1) {
        self.bannerItems = [@[self.m_firstImageView, self.m_secondImageView, self.m_thirdImageView] mutableCopy];
    }
    else {
        self.bannerItems = [@[self.m_firstImageView] mutableCopy];
        self.maxCount = 0;
    }
    self.m_pageControl.numberOfPages = self.maxCount;
    [self.m_scrollView setContentSize:CGSizeMake(self.width * self.bannerItems.count, self.height)];
    for (NSInteger i = 0; i < self.bannerItems.count ;i ++) {
        UIImageView *item = [self.bannerItems objectAtIndex:i];
        item.frame = CGRectMake(self.width * i, 0, self.width, self.height);
        item.userInteractionEnabled = YES;
        [item addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(itemClickEvent:)]];
        [self.m_scrollView addSubview:item];
    }
    //
    [self resetScroll];
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.bannerItems = [NSMutableArray array];
        self.imageViewContentMode = UIViewContentModeScaleToFill;
        [self createUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.m_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.m_pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-20);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
}

- (void)createUI {
    [self addSubview:self.m_scrollView];
    [self addSubview:self.m_pageControl];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.x ;
    if (offset >= 2*self.width) {
        _currentPage ++;
        if (_currentPage == _maxCount) {
            _currentPage = 0;
        }
        [self resetScroll];
    }
    
    if (offset <= 0) {
        _currentPage --;
        if (_currentPage == -1) {
            _currentPage = _maxCount-1;
        }
        [self resetScroll];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //NSLog(@"====== scrollViewDidEndDecelerating === ");
}

#pragma mark - Publick method
- (void)makeScrollViewScrollToIndex:(NSInteger)index {
    self.currentPage = index;
    [self resetScroll];
}

#pragma mark - Private method
- (void)resetScroll {
    if (self.bannerItems.count > 0) {
        self.m_pageControl.currentPage = _currentPage;
        if (self.bannerItems.count == 1) {
            if (self.imageURLs.count > 0) {
                [_m_firstImageView yy_sdWebImage:[self.imageURLs firstObject] placeholderImageType:YYPlaceholderImageGoodDetailType];
            }
            else if (self.localImageGroup.count > 0) {
                
            }
            [_m_scrollView setContentOffset:CGPointMake(0, 0)];
        }else {
            NSArray *imageUrls;
            if (_currentPage == 0) {  //当前位置为0, 加载最后一个， 当前位置，下一个
                imageUrls = @[[self.imageURLs lastObject],self.imageURLs[_currentPage],self.imageURLs[_currentPage +1]];
            }
            else if (_currentPage == _maxCount - 1) { //当前位置为最后一个
                imageUrls = @[self.imageURLs[_currentPage-1],self.imageURLs[_currentPage],self.imageURLs[0]];
            }
            else {
                imageUrls = @[self.imageURLs[_currentPage-1],self.imageURLs[_currentPage],self.imageURLs[_currentPage+1]];
            }
            
            for (NSInteger i = 0; i< self.bannerItems.count; i++) {
                UIImageView *imageview = self.bannerItems[i];
                [imageview yy_sdWebImage:[imageUrls objectAtIndex:i] placeholderImageType:YYPlaceholderImageGoodDetailType];
            }
            [_m_scrollView setContentOffset:CGPointMake(self.width, 0)];
        }
    }
}
#pragma mark - Event method
//滚动视图翻页
- (void)pageChangeEvent {
    NSInteger index =  _m_scrollView.contentOffset.x/self.width;
    //滑动时点击切换不做任何操作
    if (index * self.width != _m_scrollView.contentOffset.x) {
        return;
    }
    _currentPage = _m_pageControl.currentPage;
    //重置ScrollView
    [self resetScroll];
}

- (void)itemClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (_delegate && [_delegate respondsToSelector:@selector(yyBannerScrollView:didSelectItemAtIndex:)]) {
        [_delegate yyBannerScrollView:self didSelectItemAtIndex:self.currentPage];
    }
}
@end
