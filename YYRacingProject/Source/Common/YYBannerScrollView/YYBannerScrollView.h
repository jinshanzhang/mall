//
//  YYBannerScrollView.h
//  YIYanProject
//
//  Created by cjm on 2018/6/1.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  YYBannerScrollView;

@protocol YYBannerScrollViewDelegate <NSObject>

@optional
- (void)yyBannerScrollView:(YYBannerScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index;
@end

@interface YYBannerScrollView : UIView

/**图片模式**/
@property (nonatomic, assign) UIViewContentMode     imageViewContentMode;
/**网络图片**/
@property (nonatomic, strong) NSMutableArray        *imageURLs;
/**本地图片(可图片名称，可图片对象)**/
@property (nonatomic, strong) NSMutableArray        *localImageGroup;
/**占位图片**/
@property (nonatomic, strong) UIImage               *placeholderImage;
/**选中指示器样式**/
@property (nonatomic, strong) UIImage               *currentPageDotImage;
/**正常指示器样式**/
@property (nonatomic, strong) UIImage               *pageDotImage;
/**page size**/
@property (nonatomic, assign) CGSize                pageControlDotSize;

@property (nonatomic, weak) id <YYBannerScrollViewDelegate> delegate;

- (void)makeScrollViewScrollToIndex:(NSInteger)index;
@end
