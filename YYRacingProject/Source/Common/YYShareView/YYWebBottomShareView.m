//
//  YYWebBottomShareView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYWebBottomShareView.h"

#define ItemTag  550

@interface YYWebBottomShareView()

@property (nonatomic, assign) CGFloat   itemWidth;
@property (nonatomic, assign) CGFloat   rowHeight;        //128
@property (nonatomic, assign) NSInteger rowItemCount;     //每行item显示的数量
@property (nonatomic, assign) NSInteger rowCount;

@property (nonatomic, strong) NSMutableArray *allTitles;
@property (nonatomic, strong) NSMutableArray *allImageNames;

@property (nonatomic, strong) NSMutableArray *allItems;
@property (nonatomic, strong) NSMutableDictionary *extendDict;

@property (nonatomic, strong) UILabel        *line;
@property (nonatomic, strong) UIButton       *cancelBtn;


@end

@implementation YYWebBottomShareView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tag = 112;
        self.rowCount = 1;
        self.rowItemCount = 4;
        self.rowHeight = 128.0;
        self.backgroundColor = HexRGB(0xfafafa);
        self.allItems = [NSMutableArray array];
        self.itemWidth = kScreenW / self.rowItemCount;
    }
    return self;
}

- (instancetype)initShowBottomView:(NSArray *)showImageNames
                        showTitles:(NSArray *)showTitles
                            extend:(NSMutableDictionary *)extend{
    self = [self init];
    if (self) {
        if (showTitles == nil || showImageNames == nil) {
            return self;
        }
        self.allImageNames = [showImageNames mutableCopy];
        self.allTitles = [showTitles mutableCopy];
        self.extendDict = extend;
        NSInteger itemCount = self.allImageNames.count;
        if (itemCount <= self.rowItemCount) {
            self.itemWidth = kScreenW / itemCount;
        }
        self.rowCount = (int)ceilf(itemCount/4.0);
        self.frame = CGRectMake(0, kScreenH, kScreenW, self.rowCount * self.rowHeight + 54 + kBottom(50));
        
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    YYButton *lastBtn = nil;
    
    self.shareTipLable = [YYCreateTools createLabel:@"分享到"
                                               font:normalFont(14)
                                          textColor:XHBlackMidColor];
    self.shareTipLable.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.shareTipLable];
    [self.shareTipLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(25);
    }];
    
    NSInteger itemCount = self.allImageNames.count;
    for (int i = 0; i < itemCount; i ++) {
        
        YYButton *btn = [[YYButton alloc] initWithFrame:CGRectZero];
        btn.normalTitle = btn.selectTitle = self.allTitles[i];
        btn.normalImageName = btn.selectImageName = self.allImageNames[i];
    
        btn.select = NO;
        btn.tag = ItemTag + i;
        [self addSubview:btn];
        
        btn.btnClickEventBlock = ^(BOOL status, NSInteger btnTag) {
            NSInteger tag = btnTag - ItemTag;
            if (self.shareBlock) {
                self.shareBlock(tag);
            }
        };
        [self.allItems addObject:btn];
        
    }
    
    if (itemCount <= self.rowItemCount) {
        for (YYButton *tempBtn in self.allItems) {
            if (lastBtn) {
                [tempBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(lastBtn.mas_right);
                    make.top.equalTo(lastBtn);
                    make.size.equalTo(lastBtn);
                }];
                if ([self.extendDict objectForKey:@"showCouponTip"]) {
                    UILabel *tip = [YYCreateTools createLabel:@"(仅可在您的店铺使用)" font:normalFont(10) textColor:XHBlackLitColor];
                    [self addSubview:tip];
                    
                    [tip mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.centerX.equalTo(tempBtn);
                        make.bottom.mas_equalTo(tempBtn.mas_bottom).mas_offset(-12);
                    }];
                }
            }
            else {
                [tempBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(tempBtn.superview);
                    make.top.equalTo(tempBtn.superview).offset(50);
                    make.size.mas_equalTo(CGSizeMake(self.itemWidth, self.rowHeight));
                }];
            }
            lastBtn = tempBtn;
        }
    }
    // 底部
    self.cancelBtn = [YYCreateTools createBtn:@"取消"
                                         font:normalFont(16)
                                    textColor:XHBlackColor];
    [self addSubview:self.cancelBtn];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(40);
        make.bottom.mas_equalTo(-kBottom(0));
    }];
    
    self.line = [YYCreateTools createLabel:@""
                                      font:midFont(14)
                                 textColor:XHBlackColor];
    self.line.backgroundColor = HexRGB(0xF5F5F5);
    [self addSubview:self.line];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.cancelBtn);
        make.bottom.equalTo(self.cancelBtn.mas_top);
        make.height.mas_equalTo(1);
    }];
    
    [self.cancelBtn addTarget:self action:@selector(cancelClickEvent:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Event
- (void)cancelClickEvent:(UIButton *)sender {
    [self showViewOfAnimateType:YYPopAnimateDownUp];
}

@end
