//
//  YYCompositePosterView.m
//  YIYanProject
//
//  Created by cjm on 2018/5/15.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCompositePosterView.h"

@interface YYCompositePosterView()

@property (nonatomic, strong) UIImageView *posterHeadImageView;  // 海报头
@property (nonatomic, strong) UIImageView *posterTitleImageView; // 海报标题
@property (nonatomic, strong) UIImageView *headImageView;  //店铺头像
@property (nonatomic, strong) UIImageView *goodImageView;  //商品图片
@property (nonatomic, strong) UIImageView *qrcodeImageView;//二维码

@property (nonatomic, strong) UILabel     *shopNickLabel;  //店铺昵称
@property (nonatomic, strong) UILabel     *goodTitleLabel; //商品标题
@property (nonatomic, strong) UILabel     *goodPriceLabel; //商品价格

@property (nonatomic, strong) UILabel     *specialDateLabel;    //限时特卖
@property (nonatomic, strong) UILabel     *specialDateTipLabel; //限时特卖日期

@property (nonatomic, strong) UIImageView *couponMarkBgView; //券的标记背景
@property (nonatomic, strong) UILabel     *couponMarkContentLabel; //券的标记内容

@property (nonatomic, strong) UIView *discountBgView; //券后背景图
@property (nonatomic, strong) UILabel     *discountPriceLabel; //券后价

@property (nonatomic, strong) UIImageView *specialMarkBgView; //新人专享

@property (nonatomic, strong) UILabel     *qrcodeTipLabel;      //二维码提示

@property (nonatomic, assign) NSInteger   salingType;           //特卖商品的状态  1 正常 2 预热 3 热卖

@end
@implementation YYCompositePosterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.posterHeadImageView = [YYCreateTools createImageView:@"share_header_icon"
                                              viewModel:-1];
    [self addSubview:self.posterHeadImageView];
    
    self.headImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.headImageView zy_cornerRadiusAdvance:kSizeScale(18)
                                rectCornerType:UIRectCornerAllCorners];
    [self addSubview:self.headImageView];
    
    self.posterTitleImageView = [YYCreateTools createImageView:@"share_content_icon"
                                                    viewModel:-1];
    [self addSubview:self.posterTitleImageView];
    
    self.goodImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.goodImageView zy_cornerRadiusAdvance:kSizeScale(10)
                                rectCornerType:UIRectCornerAllCorners];
    [self addSubview:self.goodImageView];
    
    self.qrcodeImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self addSubview:self.qrcodeImageView];
    
    self.couponMarkBgView = [YYCreateTools createImageView:nil
                                                 viewModel:-1];
    [self addSubview:self.couponMarkBgView];
    
    self.specialMarkBgView = [YYCreateTools createImageView:@"share_new_vip_icon"
                                                  viewModel:-1];
    self.specialMarkBgView.alpha = 0.0;
    [self addSubview:self.specialMarkBgView];
    
    /*self.discountBgView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.discountBgView zy_cornerRadiusAdvance:kSizeScale(10) rectCornerType:UIRectCornerAllCorners];*/
    self.discountBgView = [YYCreateTools createView:XHClearColor];
    self.discountBgView.v_cornerRadius = kSizeScale(10);
    [self addSubview:self.discountBgView];
    
    self.discountPriceLabel = [YYCreateTools createLabel:nil
                                                    font:boldFont(38)
                                               textColor:XHWhiteColor];
    //self.discountPriceLabel.backgroundColor =
    self.discountPriceLabel.textAlignment = NSTextAlignmentCenter;
    //self.discountPriceLabel.v_cornerRadius = kSizeScale(10);
    [self addSubview:self.discountPriceLabel];
    
    // 店铺
    self.shopNickLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(12)
                                             textColor:XHBlackMidColor];
    [self addSubview:self.shopNickLabel];
    
    self.goodTitleLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(14)
                                             textColor:XHBlackColor];
    self.goodTitleLabel.numberOfLines = 2;
    [self addSubview:self.goodTitleLabel];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(18)
                                             textColor:XHRedColor];
    [self addSubview:self.goodPriceLabel];
    
    self.couponMarkContentLabel = [YYCreateTools createLabel:nil
                                                        font:normalFont(10)
                                                   textColor:XHWhiteColor];
    self.couponMarkContentLabel.backgroundColor = XHClearColor;
    self.couponMarkContentLabel.textAlignment = NSTextAlignmentCenter;
    [self.couponMarkBgView addSubview:self.couponMarkContentLabel];
    
    self.specialDateTipLabel = [YYCreateTools createLabel:@"限时特卖"
                                                font:midFont(12)
                                           textColor:XHRedColor];
    self.specialDateTipLabel.textAlignment = NSTextAlignmentCenter;
    self.specialDateTipLabel.layer.borderColor = XHRedColor.CGColor;
    self.specialDateTipLabel.layer.borderWidth = 1;
    self.specialDateTipLabel.layer.cornerRadius = 2;
    self.specialDateTipLabel.clipsToBounds = YES;
    [self addSubview:self.specialDateTipLabel];
    
    self.specialDateLabel = [YYCreateTools createLabel:nil
                                                     font:midFont(12)
                                                textColor:XHWhiteColor];
    self.specialDateLabel.textAlignment = NSTextAlignmentCenter;
    self.specialDateLabel.backgroundColor = XHRedColor;
    self.specialDateLabel.layer.cornerRadius = 2;
    self.specialDateLabel.clipsToBounds = YES;
    [self addSubview:self.specialDateLabel];
    
    self.qrcodeTipLabel = [YYCreateTools createLabel:@"长按识别领优惠"
                                                  font:normalFont(11)
                                             textColor:XHBlackLitColor];
    [self addSubview:self.qrcodeTipLabel];
    
    self.qrcodeImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self addSubview:self.qrcodeImageView];
    
    [self _setSubViewLayouts];
}


- (void)_setSubViewLayouts {
    [self mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenW);
    }];
    
    [self.posterHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(53));
    }];
    
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.posterHeadImageView.mas_bottom).offset(kSizeScale(15));
        make.left.mas_equalTo(kSizeScale(16));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(36), kSizeScale(36)));
    }];
    
    [self.shopNickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headImageView);
        make.left.equalTo(self.headImageView.mas_right).offset(kSizeScale(10));
    }];

    [self.posterTitleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopNickLabel);
        make.bottom.equalTo(self.headImageView.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(217), kSizeScale(22)));
    }];
    
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.height.mas_equalTo(kScreenW - kSizeScale(16)*2);
        make.top.equalTo(self.headImageView.mas_bottom).offset(kSizeScale(12));
    }];
    
    [self.discountBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        //make.top.equalTo(self.goodImageView.mas_top).offset(kSizeScale(30));
        //make.right.equalTo(self.goodImageView);
        make.bottom.equalTo(self.goodImageView.mas_bottom).offset(-kSizeScale(30));
        make.centerX.equalTo(self.goodImageView);
        make.width.mas_equalTo(kSizeScale(84));
        make.height.mas_equalTo(kSizeScale(58));
    }];
    
    [self.discountPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        //make.right.equalTo(self.discountBgView.mas_right).offset(-kSizeScale(14));
        //make.centerY.equalTo(self.discountBgView);
        make.center.equalTo(self.discountBgView);
        make.width.mas_equalTo(kSizeScale(0));
        make.height.mas_equalTo(kSizeScale(58));
    }];
    
    [self.qrcodeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-kSizeScale(16));
        make.top.equalTo(self.goodImageView.mas_bottom).offset(kSizeScale(19));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(95), kSizeScale(95)));
    }];
    
    // 二维码提示
    [self.qrcodeTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.qrcodeImageView);
        make.top.equalTo(self.qrcodeImageView.mas_bottom).offset(kSizeScale(6));
        make.bottom.equalTo(self.mas_bottom).offset(-kSizeScale(28)).priorityHigh();
    }];
    
    [self.goodTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.top.equalTo(self.qrcodeImageView.mas_top).offset(kSizeScale(40));
        make.right.equalTo(self.qrcodeImageView.mas_left).offset(-kSizeScale(19));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodTitleLabel);
        make.top.equalTo(self.qrcodeImageView.mas_top).offset(-kSizeScale(4));
    }];
    
    [self.specialMarkBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(4));
        make.centerY.equalTo(self.goodPriceLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(63), kSizeScale(19)));
    }];
    
    [self.couponMarkBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(4));
        make.centerY.equalTo(self.goodPriceLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(65), kSizeScale(20)));
    }];
    
    [self.couponMarkContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.couponMarkBgView);
    }];
    
    // 限时特卖提示
    [self.specialDateTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodTitleLabel);
        make.top.equalTo(self.qrcodeImageView.mas_bottom).offset(kSizeScale(2));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(64), kSizeScale(22)));
    }];
    
    //限时特卖时间
    [self.specialDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.specialDateTipLabel.mas_right).offset(-kSizeScale(2));
        make.centerY.equalTo(self.specialDateTipLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(120), kSizeScale(22)));
    }];
}

- (void)setSalingType:(NSInteger)salingType {
    _salingType = salingType;
    // 1 正常 2 预热 3 热卖
    self.specialDateLabel.hidden = (_salingType == 1?YES:NO);
    self.specialDateTipLabel.hidden = (_salingType == 1?YES:NO);
}

+ (void)generatePosterView:(id<ShareViewProtocol>)shareModel cutImageBlock:(void (^)(UIImage *))cutImageBlock {
    @autoreleasepool {
        NSString *dateText = nil;
        BOOL isHaveCoupon = NO;
        UIColor *contentColor = nil;
        NSInteger specialType = -1;
        CGFloat endDateTime = -1, startDateTime = -1;
        CGFloat couponMarkWidth = -1.0, discountWidth = -1.0;
        
        __block NSString *goodUrl = nil, *inviteUrl = nil;
        __block NSMutableAttributedString *goodPrice = nil;
        NSString *shopName = nil, *shopAvaUrl = nil, *goodTitle = nil;
        YYGoodSalingStatusType salingType = YYGoodSalingStatusNoraml;
        specialType = shareModel.specialType;
        YYCompositePosterView *posterView = [[YYCompositePosterView alloc] initWithFrame:CGRectZero];
        YYBaseViewController *ctrl = (YYBaseViewController *)[YYCommonTools getCurrentVC];
        [ctrl.view addSubview:posterView];
        [ctrl.view sendSubviewToBack:posterView];
        [posterView layoutIfNeeded];
        
        if (shareModel) {
            endDateTime = [shareModel.specialEndSellDate longLongValue];
            startDateTime = [shareModel.specialStartSellDate longLongValue];
            salingType = [shareModel.itemSaleType integerValue];
            
            shopName = shareModel.shopName;
            shopAvaUrl = shareModel.shopAvatorUrl;
            goodTitle = shareModel.goodsTitle;
            
            goodUrl = shareModel.goodsShareImage;
            inviteUrl = shareModel.shareUrl;
            
            posterView.shopNickLabel.text = (kValidString(shopName)?shopName:@"");
            posterView.goodTitleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            posterView.goodTitleLabel.text = goodTitle;
            
            if (kValidString(shareModel.discountPrice) && kValidString(shareModel.maxDenomination)) {
                isHaveCoupon = YES;
            }
            
            if (isHaveCoupon) {
                NSString *content = [NSString stringWithFormat:@"券后¥%@",shareModel.discountPrice];
                CGSize discountSize = [YYCommonTools sizeWithText:content
                                                             font:boldFont(38)
                                                          maxSize:CGSizeMake(MAXFLOAT, kSizeScale(58))];
                discountWidth = discountSize.width + kSizeScale(60);
                posterView.discountPriceLabel.text = content;
                
                [posterView.discountBgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(discountWidth);
                }];
                [posterView.discountPriceLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(discountWidth);
                }];
            }
            
            if (isHaveCoupon) {
                NSString *content = [NSString stringWithFormat:@"领券立减¥%@",shareModel.maxDenomination];
                CGSize maxDenominationSize = [YYCommonTools sizeWithText:content
                                                             font:normalFont(10)
                                                          maxSize:CGSizeMake(MAXFLOAT, kSizeScale(20))];
                couponMarkWidth = maxDenominationSize.width + kSizeScale(10);
                posterView.couponMarkContentLabel.text = content;
                [posterView.couponMarkBgView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(couponMarkWidth);
                }];
            }
            // 现价
            goodPrice = [YYCommonTools containSpecialSymbolHandler:@"¥"
                                                        symbolFont:boldFont(24)
                                                   symbolTextColor:salingType == YYGoodSalingStatusPreHot?HexRGB(0x11BC56):XHRedColor
                                                         wordSpace:3
                                                             price:shareModel.reservePrice
                                                         priceFont:boldFont(24)
                                                    priceTextColor:salingType == YYGoodSalingStatusPreHot?HexRGB(0x11BC56):XHRedColor
                                                     symbolOffsetY:0];
            // 原价
            NSMutableAttributedString *originPrice = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",shareModel.originPrice]];
            [originPrice yy_setAttributes:XHBlackLitColor
                                     font:normalFont(15)
                                wordSpace:@(0) content:originPrice alignment:NSTextAlignmentCenter];
            [originPrice yy_setAttributeOfDeleteLine:NSMakeRange(0, originPrice.length)];
            if (isHaveCoupon) {
                // 有券
                posterView.couponMarkBgView.alpha = posterView.couponMarkContentLabel.alpha = posterView.discountPriceLabel.alpha = 1.0;
                posterView.discountBgView.hidden = NO;
            }
            else {
                // 无券
                posterView.couponMarkBgView.alpha = posterView.couponMarkContentLabel.alpha = posterView.discountPriceLabel.alpha = 0.0;
                [goodPrice appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "]];
                [goodPrice appendAttributedString:originPrice];
                posterView.discountBgView.hidden = YES;
            }
            
            if (specialType == 1) {
                posterView.couponMarkBgView.alpha = posterView.couponMarkContentLabel.alpha = 0.0;
                posterView.specialMarkBgView.alpha = 1.0;
            }
            
            if (salingType != YYGoodSalingStatusNoraml) {
                if (salingType == YYGoodSalingStatusPreHot) {
                    contentColor = HexRGB(0x11BC56);
                    NSDate *date = [NSDate dateWithTimeIntervalSince1970:startDateTime/1000];
                    // 预热
                    dateText = [NSString stringWithFormat:@"%ld月%ld号%ld点开抢",(long)date.month,(long)date.day,(long)date.hour];
                    posterView.couponMarkBgView.image = [UIImage imageNamed:@"coupon_new_bg_icon"];
                    
                    //posterView.discountBgView.image = [[UIImage imageNamed:@"share_prehot_bg_icon"] stretchableImageWithLeftCapWidth:45 topCapHeight:0];
                }
                else if (salingType == YYGoodSalingStatusSpecial) {
                    // 热卖
                    contentColor = HexRGB(0x191946);
                    NSDate *date = [NSDate dateWithTimeIntervalSince1970:endDateTime/1000];
                    dateText = [NSString stringWithFormat:@"%ld月%ld号%ld点结束",(long)date.month,(long)date.day,(long)date.hour];
                    posterView.couponMarkBgView.image = [UIImage imageNamed:@"coupon_new_bg_icon"];
                    //posterView.discountBgView.image = [[UIImage imageNamed:@"share_hot_bg_icon"] stretchableImageWithLeftCapWidth:45 topCapHeight:0];
                }
                posterView.specialDateTipLabel.textColor = contentColor;
                posterView.specialDateTipLabel.layer.borderColor = contentColor.CGColor;
                posterView.specialDateLabel.backgroundColor = contentColor;
                posterView.specialDateLabel.text = dateText;
                posterView.discountBgView.backgroundColor = (salingType == YYGoodSalingStatusSpecial? HexRGB(0xFC264A) : HexRGB(0x17BC5B));
            }
            else {
                posterView.couponMarkBgView.image = [UIImage imageNamed:@"coupon_new_bg_icon"];
            }
            posterView.salingType = salingType;
            
            posterView.goodPriceLabel.attributedText = goodPrice;
            
            dispatch_semaphore_t sem = dispatch_semaphore_create(0);
            NSMutableDictionary *allURL = [NSMutableDictionary dictionary];
            if (kValidString(shopAvaUrl)) {
                [allURL setObject:shopAvaUrl forKey:@"headerImage"];
            }
            if (kValidString(shareModel.qrcodePictureUrl)) {
                [allURL setObject:shareModel.qrcodePictureUrl forKey:@"qrcodeImage"];
            }
            if (kValidString(goodUrl)) {
                [allURL setObject:goodUrl forKey:@"goodImage"];
            }
            NSInteger allCount = allURL.count;
            //这里模仿的http请求block块都是在同一线程（主线程）执行返回的，所以对这个变量的访问不存在资源竞争问题，故不需要枷锁处理
            //如果网络请求在不同线程返回，要对这个变量进行枷锁处理，不然很会有资源竞争危险
            __block NSInteger finishCount = 0;
            __block NSMutableDictionary *downloadImage = [NSMutableDictionary dictionary];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                //demo testUsingSemaphore方法是在主线程调用的，不直接调用遍历执行，而是嵌套了一个异步，是为了避免主线程阻塞
                [allURL enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:obj] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                        
                    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                        if (image) {
                            [downloadImage setObject:image forKey:key];
                        }
                        if (++finishCount == allCount) {
                            [downloadImage enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                                if ([key isEqualToString:@"headerImage"]) {
                                    posterView.headImageView.image = obj;
                                }
                                if ([key isEqualToString:@"qrcodeImage"]) {
                                    posterView.qrcodeImageView.image = obj;
                                }
                                if ([key isEqualToString:@"goodImage"]) {
                                    posterView.goodImageView.image = obj;
                                }
                            }];
                            dispatch_semaphore_signal(sem);
                        }
                    }];
                }];
                //如果全部请求没有返回则该线程会一直阻塞
                dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIGraphicsBeginImageContextWithOptions(posterView.bounds.size, NO, 0);
                    // 方式1绘制方式
                    //[posterView.layer renderInContext:UIGraphicsGetCurrentContext()];
                    //posterView.layer.contents = nil;
                    // 方式2截屏操作
                    [posterView drawViewHierarchyInRect:posterView.bounds afterScreenUpdates:YES];
                    
                    UIImage *returnImage = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    cutImageBlock(returnImage);
                    [posterView removeFromSuperview];
                });
            });
        }
    }
}
@end
