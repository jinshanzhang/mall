//
//  YYAddCartTipView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYAddCartTipView : UIView

+ (void)showAddCartTipBelowSubview:(UIView *)belowView
                     tipClickBlock:(void (^)(void))tipClickBlock;

@end
