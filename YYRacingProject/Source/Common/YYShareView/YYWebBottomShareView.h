//
//  YYWebBottomShareView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseShareView.h"

@interface YYWebBottomShareView : YYBaseShareView

@property (nonatomic, copy) void(^shareBlock)(NSInteger atIndex);

- (instancetype)initShowBottomView:(NSArray *)showImageNames
                        showTitles:(NSArray *)showTitles
                            extend:(NSMutableDictionary *)extend;
@property (nonatomic, strong) UILabel        *shareTipLable;

@end
