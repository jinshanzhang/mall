//
//  YYBaseShareView.m
//  YIYanProject
//
//  Created by cjm on 2018/4/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseShareView.h"

@interface YYBaseShareView()

@property (nonatomic, assign) YYPopAnimateType animateType;
@property (nonatomic, strong) UIView *maskContainerView;

@end

@implementation YYBaseShareView

#pragma mark - Life cycle
- (instancetype)init {
    self = [super init];
    if (self) {
        self.showStatu = NO;
        self.maskContainerView = [[UIView alloc]init];
        self.maskContainerView.tag = 600;
        self.maskContainerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
        self.maskContainerView.frame = [UIScreen mainScreen].bounds;
        [self addTapGesture];
        UIView *oldView = [[UIApplication sharedApplication].delegate.window viewWithTag:600];
        if (oldView) {
            [oldView removeFromSuperview];
        }
        [[UIApplication sharedApplication].delegate.window addSubview:self.maskContainerView];
    }
    return self;
}

#pragma mark - Private method
- (void)addTapGesture {
    [self.maskContainerView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickHideViewEvent:)]];
}

- (void)addViewAnimationTranslationDownToUp {
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.transform = CGAffineTransformMakeTranslation(0, -self.frame.size.height);
                         self.alpha = 1;
                         self.maskContainerView.alpha = 1;
                         self.showStatu = YES;
                     }
                     completion:nil];
}

- (void)removeAnimationTranslationDownToUp {
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.transform = CGAffineTransformMakeTranslation(0, self.frame.size.height);
                         self.alpha = 0;
                         self.maskContainerView.alpha = 0;
                         self.showStatu = NO;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [self.maskContainerView removeFromSuperview];
                     }];
}

- (void)addViewScaleAnimation {
    self.transform = CGAffineTransformMakeScale(.6, .6);
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.transform = CGAffineTransformMakeScale(1.0, 1.0);
                         self.alpha = 1;
                         self.showStatu = YES;
                     }
                     completion:nil];
}

- (void)removeViewScaleAnimation {
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.transform = CGAffineTransformMakeScale(0.6, 0.6);
                         self.alpha = 0;
                         self.maskContainerView.alpha = 0;
                         self.showStatu = NO;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                         [self.maskContainerView removeFromSuperview];
                     }];
}

#pragma mark - Public method
- (void)showViewOfAnimateType:(YYPopAnimateType)animateType {
    self.animateType = animateType;
    if (self.showStatu) {
        if (animateType == YYPopAnimateDownUp) {
            [self removeAnimationTranslationDownToUp];
        }
        else if (animateType == YYPopAnimateScale) {
            [self removeViewScaleAnimation];
        }
    }
    else {
        if (animateType == YYPopAnimateDownUp) {
            [self addViewAnimationTranslationDownToUp];
        }
        else if (animateType == YYPopAnimateScale) {
            [self addViewScaleAnimation];
        }
    }
}

#pragma mark - Event method
- (void)clickHideViewEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.dissClickBlock) {
        self.dissClickBlock();
    }
    else {
        if (self.animateType == YYPopAnimateDownUp) {
            [self removeAnimationTranslationDownToUp];
        }
        else if (self.animateType == YYPopAnimateScale) {
            [self removeViewScaleAnimation];
        }
    }
}


+ (instancetype)initYYPopView:(id)viewObj {
    YYBaseShareView *baseMask = viewObj;
    baseMask.maskContainerView.backgroundColor =  [[UIColor blackColor] colorWithAlphaComponent:.7];
    __block BOOL isExit = NO;
    __block UIView *tempView = nil;
    NSArray *subViews = [[UIApplication sharedApplication].delegate.window subviews];
    [subViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView *view = (UIView *)obj;
        if (view.tag == baseMask.tag && [view isKindOfClass:[YYBaseShareView class]]) {
            tempView = view;
            isExit = YES;
            *stop = YES;
        }
    }];
    if (isExit && tempView) {
        [tempView removeFromSuperview];
    }
    [[UIApplication sharedApplication].delegate.window addSubview:baseMask];
    return baseMask;
}

@end
