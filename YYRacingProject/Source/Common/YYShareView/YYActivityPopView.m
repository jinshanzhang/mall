//
//  YYActivityPopView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYActivityPopView.h"
#import "HomeBannerInfoModel.h"
@interface YYActivityPopView()<UIGestureRecognizerDelegate>

@property (nonatomic, strong) YYAnimatedImageView *loadingImageView;
@property (nonatomic, strong) UIImageView *closeImageView;
@property (nonatomic, strong) UIButton *closeBtn;


@end

@implementation YYActivityPopView

#pragma mark - Setter method
- (void)setParams:(NSMutableDictionary *)params {
    _params = params;
    if (kValidDictionary(_params)) {
        HomeBannerInfoModel *bannerModel = [_params objectForKey:@"activityModel"];
        CGFloat loadingHeight = 0.0;
        CGFloat loadingWidth = 0.0;
        
        if (kValidString(bannerModel.imageUrl)) {
            CGSize size = [UIImage getImageSizeWithURL:bannerModel.imageUrl];
            if (size.width > 375) {
                loadingHeight = size.height/2.0;
                loadingWidth = size.width/2.0;
            }
            else {
                loadingHeight = size.height;
                loadingWidth = size.width;
            }
        }
     
        CGFloat height = loadingHeight + kSizeScale(60);
        height = bannerModel.HideBottom?loadingHeight:height;
        self.closeImageView.hidden = bannerModel.HideBottom;
        
        CGFloat top = (kScreenH - height)/2.0;
        [self.loadingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(@(top));
            make.centerX.equalTo(self);
            make.width.mas_equalTo(@(loadingWidth));
            make.height.mas_equalTo(@(loadingHeight));
        }];

        [self.loadingImageView setImageURL:[NSURL URLWithString:bannerModel.imageUrl]];
        [self.closeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.loadingImageView.mas_bottom);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(60)));
        }];
        
        
        self.closeBtn.hidden = !bannerModel.HideBottom;
        [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(100), kSizeScale(100)));
        }];
        @weakify(self);
        self.closeBtn.actionBlock = ^(UIButton *sender) {
            @strongify(self);
                [self showViewOfAnimateType:YYPopAnimateScale];
        };
    }
}

#pragma mark - Life cycle





- (instancetype)init {
    if (self = [super init]) {
        [self createUI];
        self.tag = 115;
    }
    return self;
}

- (void)createUI {
    self.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    self.loadingImageView = [[YYAnimatedImageView alloc] init];
    self.loadingImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(activityClickEvent:)];
    [self.loadingImageView addGestureRecognizer:tap];
    tap.delegate = self;
    [self addSubview:self.loadingImageView];

    
    self.closeImageView = [YYCreateTools createImageView:@"coupon_close_icon"
                                               viewModel:-1];
    self.closeImageView.userInteractionEnabled = YES;
    
    [self.closeImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopViewEvent:)]];
    
    [self addSubview:self.closeImageView];
    
    self.closeBtn = [YYCreateTools createBtn:@""];
    [self.loadingImageView addSubview:self.closeBtn];
    
}

#pragma mark - Event
- (void)activityClickEvent:(UITapGestureRecognizer *)gestrue {
    if (self.operatorBlock) {
        self.operatorBlock(1);
    }
}
- (void)closePopViewEvent:(UIGestureRecognizer *)gesture {
    if (self.operatorBlock) {
        self.operatorBlock(0);
        [self showViewOfAnimateType:YYPopAnimateScale];
    }
}



@end
