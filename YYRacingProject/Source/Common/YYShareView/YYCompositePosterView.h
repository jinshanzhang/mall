//
//  YYCompositePosterView.h
//  YIYanProject
//
//  Created by cjm on 2018/5/15.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYCompositePosterView : UIView

+ (void)generatePosterView:(id<ShareViewProtocol>)shareModel
                  cutImageBlock:(void(^)(UIImage *))cutImageBlock;

@end
