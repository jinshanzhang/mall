//
//  YYBaseShareView.h
//  YIYanProject
//
//  Created by cjm on 2018/4/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYBaseShareView : UIView
/**
 *  是否处于显示状态
 */
@property (nonatomic, assign) BOOL  showStatu;
@property (nonatomic, copy) void (^dissClickBlock)(void);

+ (instancetype)initYYPopView:(id)viewObj;
/**
 * 指定动画显示或者隐藏视图
 */
- (void)showViewOfAnimateType:(YYPopAnimateType)animateType;

@end
