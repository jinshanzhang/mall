//
//  YYBottomShareView.h
//  YIYanProject
//
//  Created by cjm on 2018/4/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseShareView.h"

@interface YYBottomShareView : YYBaseShareView

@property (nonatomic, copy) void(^shareBlock)(NSInteger atIndex);

- (instancetype)initShowBottomView:(NSArray *)showImageNames
                        showTitles:(NSArray *)showTitles;


- (instancetype)initShowBottomView:(NSArray *)showImageNames
                        showTitles:(NSArray *)showTitles
                            others:(NSArray *)others;
@end
