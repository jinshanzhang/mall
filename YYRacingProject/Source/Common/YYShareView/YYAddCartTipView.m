//
//  YYAddCartTipView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYAddCartTipView.h"
typedef  void (^tipClickBlock)(void);
@interface YYAddCartTipView()

@property (nonatomic, strong) UILabel  *tipLabel;
@property (nonatomic, strong) UILabel  *balanceLabel;
@property (nonatomic, strong) UIImageView *rightImageView;

@property (nonatomic, copy)   tipClickBlock clickBlock;

@end

@implementation YYAddCartTipView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.tipLabel = [YYCreateTools createLabel:@"成功加入购物车"
                                          font:midFont(15)
                                     textColor:XHWhiteColor];
    [self addSubview:self.tipLabel];
    
    self.balanceLabel = [YYCreateTools createLabel:@"立即结算"
                                              font:midFont(15)
                                         textColor:XHRedColor];
    [self addSubview:self.balanceLabel];
    
    self.rightImageView = [YYCreateTools createImageView:@"add_cart_tip_icon"
                                               viewModel:-1];
    [self addSubview:self.rightImageView];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goBalanceClickEvent:)]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(kSizeScale(13));
        make.centerY.equalTo(self);
    }];
    
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-kSizeScale(10));
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(10), kSizeScale(10)));
    }];
    
    [self.balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.rightImageView.mas_left).offset(kSizeScale(-10));
    }];
}

- (void)addViewAnimationTranslationDownToUp {
    self.alpha = 0;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.transform = CGAffineTransformMakeTranslation(0, -kBottom(50)-self.height);
                         self.alpha = 1;
                     }
                     completion:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self removeAnimationTranslationDownToUp];
    });
}

- (void)removeAnimationTranslationDownToUp {
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.transform = CGAffineTransformMakeTranslation(0, self.frame.size.height);
                         self.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

- (void)goBalanceClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.clickBlock) {
        self.clickBlock();
    }
}

+ (void)showAddCartTipBelowSubview:(UIView *)belowView
                     tipClickBlock:(void (^)(void))tipClickBlock {
    YYAddCartTipView *tipView = [[YYAddCartTipView alloc] initWithFrame:CGRectMake(0, kScreenH, kScreenW, kSizeScale(35))];
    [[YYCommonTools getCurrentVC].view insertSubview:tipView belowSubview:belowView];
    [tipView addViewAnimationTranslationDownToUp];
    tipView.clickBlock = ^{
        tipClickBlock();
    };
}

@end
