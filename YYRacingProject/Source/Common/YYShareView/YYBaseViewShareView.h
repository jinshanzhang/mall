//
//  YYBaseViewShareView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYBaseViewShareView : UIView
/**
 *  是否处于显示状态
 */
@property (nonatomic, assign) BOOL  showStatu;
@property (nonatomic, copy) void (^dissClickBlock)(void);

+(instancetype)initYYPopView:(id)viewObj base:(YYBaseViewController *)ctrl;

/**
 * 指定动画显示或者隐藏视图
 */
- (void)showViewOfAnimateType:(YYPopAnimateType)animateType;

@end

NS_ASSUME_NONNULL_END
