//
//  YYBottomShareView.m
//  YIYanProject
//
//  Created by cjm on 2018/4/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBottomShareView.h"

#define ItemTag  400
@interface YYBottomShareView()

@property (nonatomic, assign) CGFloat   itemWidth;
@property (nonatomic, assign) CGFloat   rowHeight;        //128
@property (nonatomic, assign) NSInteger rowItemCount;     //每行item显示的数量
@property (nonatomic, assign) NSInteger rowCount;

@property (nonatomic, strong) NSMutableArray *allTitles;
@property (nonatomic, strong) NSMutableArray *allImageNames;
@property (nonatomic, strong) NSMutableArray *allOthers;

@property (nonatomic, strong) NSMutableArray *allItems;

@property (nonatomic, strong) UILabel        *line;
@property (nonatomic, strong) UILabel        *shareTipLabel;
@property (nonatomic, strong) UILabel        *shareSubTitleLabel;
@property (nonatomic, strong) UIButton       *cancelBtn;

@property (nonatomic, assign) BOOL           isShowSubTitles;
@property (nonatomic, strong) MASConstraint  *subTitleHeight;
@end

@implementation YYBottomShareView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.tag = 111;
        self.rowCount = 1;
        self.rowItemCount = 4;
        self.isShowSubTitles = NO;
        self.rowHeight = kSizeScale(100);
        self.backgroundColor = HexRGB(0xfafafa);
        self.allItems = [NSMutableArray array];
        self.itemWidth = kScreenW / self.rowItemCount;
    }
    return self;
}

- (instancetype)initShowBottomView:(NSArray *)showImageNames
                        showTitles:(NSArray *)showTitles {
    self = [self init];
    if (self) {
        if (showTitles == nil || showImageNames == nil) {
            return self;
        }
        self.allImageNames = [showImageNames mutableCopy];
        self.allTitles = [showTitles mutableCopy];
        NSInteger itemCount = self.allImageNames.count;
        if (itemCount < self.rowItemCount) {
            self.itemWidth = kScreenW / itemCount;
        }
        self.rowCount = (int)ceilf(itemCount/4.0);
        self.frame = CGRectMake(0, kScreenH, kScreenW, self.rowCount * self.rowHeight + 54 + 50);
        
        [self createUI];
    }
    return self;
}

- (instancetype)initShowBottomView:(NSArray *)showImageNames
                        showTitles:(NSArray *)showTitles
                            others:(NSArray *)others {
    self = [self init];
    if (self) {
        if (showTitles == nil || showImageNames == nil) {
            return self;
        }
        if (!kValidArray(others)) {
            // other 为空， 表示不需要显示副标题
            self.isShowSubTitles = NO;
        }
        else {
            self.isShowSubTitles = YES;
            self.allOthers = [others mutableCopy];
        }
        self.allImageNames = [showImageNames mutableCopy];
        self.allTitles = [showTitles mutableCopy];
        NSInteger itemCount = self.allImageNames.count;
        if (itemCount < self.rowItemCount) {
            self.itemWidth = kScreenW / itemCount;
        }
        self.rowCount = (int)ceilf(itemCount/4.0);
        self.frame = CGRectMake(0, kScreenH, kScreenW, self.rowCount * self.rowHeight + 54 + kBottom(50) + (self.isShowSubTitles? kSizeScale(45): 0.0));
        if (self.isShowSubTitles) {
            self.rowHeight = self.rowHeight - kSizeScale(34);
        }
        else {
            self.rowHeight = self.rowHeight - kSizeScale(20);
        }
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    YYButton *lastBtn = nil;
    NSInteger itemCount = self.allImageNames.count;
    
    self.shareTipLabel = [YYCreateTools createLabel:@"分享到"
                                               font:normalFont(15)
                                          textColor:XHBlackMidColor];
    self.shareTipLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.shareTipLabel];
    [self.shareTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.mas_equalTo(25);
        make.height.mas_equalTo(25);
    }];
    
    self.shareSubTitleLabel = [YYCreateTools createLabel:@""
                                                    font:normalFont(14)
                                               textColor:XHBlackColor];
    self.shareSubTitleLabel.numberOfLines = 2;
    self.shareSubTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.shareSubTitleLabel];
    [self.shareSubTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(60));
        make.right.mas_equalTo(-kSizeScale(60));
        make.top.equalTo(self.shareTipLabel.mas_bottom).offset(kSizeScale(10));
        self.subTitleHeight = make.height.mas_equalTo(0).priorityHigh();
    }];
    
    [self.subTitleHeight uninstall];
    if (!self.isShowSubTitles) {
        // 不显示副标题
        [self.subTitleHeight install];
        [self.shareSubTitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.shareTipLabel.mas_bottom);
        }];
    }
    else {
        // 显示副标题
        self.shareTipLabel.text = @"";
        self.shareSubTitleLabel.text = [NSString stringWithFormat:@"只要你的好友通过你的链接购买此商品\n你就能赚到%@元的佣金哦~",[self.allOthers firstObject]];
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(15) symbolTextColor:XHRedColor wordSpace:2 price:[self.allOthers firstObject] priceFont:boldFont(18) priceTextColor: XHRedColor symbolOffsetY:1.2];
       
        self.shareTipLabel.attributedText = attribut;
        
    }
    for (int i = 0; i < itemCount; i ++) {
        
        YYButton *btn = [[YYButton alloc] initWithFrame:CGRectZero];
        btn.normalTitle = btn.selectTitle = self.allTitles[i];
        btn.normalImageName = btn.selectImageName = self.allImageNames[i];
    
        btn.titleFont = midFont(12);
        btn.titleTextColor = XHBlackColor;
        btn.select = NO;
        btn.tag = ItemTag + i;
        [self addSubview:btn];
        
        btn.btnClickEventBlock = ^(BOOL status, NSInteger btnTag) {
            NSInteger tag = btnTag - ItemTag;
            if (self.shareBlock) {
                self.shareBlock(tag);
            }
        };
        [self.allItems addObject:btn];
    }
    
    for (YYButton *tempBtn in self.allItems) {
        if (lastBtn) {
            [tempBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastBtn.mas_right);
                make.top.equalTo(lastBtn);
                make.size.equalTo(lastBtn);
            }];
        }
        else {
            [tempBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(tempBtn.superview.mas_left);
                make.top.equalTo(self.shareSubTitleLabel.mas_bottom).offset(kSizeScale(10));
                make.size.mas_equalTo(CGSizeMake(self.itemWidth, self.rowHeight));
            }];
        }
        lastBtn = tempBtn;
    }
    // 底部
    self.cancelBtn = [YYCreateTools createBtn:@"取消"
                                         font:normalFont(16)
                                    textColor:XHBlackColor];
    [self addSubview:self.cancelBtn];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(50);
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
    
    self.line = [YYCreateTools createLabel:@""
                                      font:normalFont(14)
                                 textColor:XHBlackColor];
    self.line.backgroundColor = HexRGB(0xF5F5F5);
    [self addSubview:self.line];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.cancelBtn);
        make.bottom.equalTo(self.cancelBtn.mas_top);
        make.height.mas_equalTo(1);
    }];

    [self.cancelBtn addTarget:self action:@selector(cancelClickEvent:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Event
- (void)cancelClickEvent:(UIButton *)sender {
    [self showViewOfAnimateType:YYPopAnimateDownUp];
}
@end
