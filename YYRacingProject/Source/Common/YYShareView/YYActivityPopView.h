//
//  YYActivityPopView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseShareView.h"

@interface YYActivityPopView : YYBaseShareView

@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, copy) void (^operatorBlock)(NSInteger operatorType);

@end
