//
//  YYAlertView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYAlertView.h"

#define TagValue  1000
#define AlertTime 0.3 //弹出动画时间
#define DropTime 0.5 //落下动画时间

@interface YYAlertView ()

//@property (nonatomic, strong) UIImageView *titleImageView;
@property (nonatomic, strong) UILabel * navTitleLabel;

@property (nonatomic, strong) YYLabel     *titleLabel;
@property (nonatomic, strong) YYLabel     *subTitleLabel;

@property (nonatomic, strong) UIButton    *confirmBtn;
@property (nonatomic, strong) UIButton    *cancelBtn;

@property (nonatomic, copy) CancelButtonClick  cancelBlock;
@property (nonatomic, copy) ConfirmButtonClick confirmBlock;
@property (nonatomic, copy) OtherButtonClick   otherBlock;

@end

@implementation YYAlertView

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                     subTitle:(NSString *)subTitle
                    alertType:(YYAlertType)alertType
                   otherBlock:(void (^)(NSInteger))otherBlock
                 confirmBlock:(void (^)(void))confirmBlock
                  cancelBlock:(void (^)(void))cancelBlock {
    if (self = [super initWithFrame:frame]) {
        self.layer.masksToBounds = YES;
        self.backgroundColor = XHWhiteColor;
        self.layer.cornerRadius = kSizeScale(5);
        self.otherBlock = otherBlock;
        self.confirmBlock = confirmBlock;
        self.cancelBlock = cancelBlock;
        if (alertType == YYAlertPrivacyPolicyType) {
            [self createPrivacyPolicy:title
                             subTitle:subTitle];
        }
    }
    return self;
}

- (void)createPrivacyPolicy:(NSString *)title
                   subTitle:(NSString *)subTitle {
    @weakify(self);
    CGRect frame = self.frame;
    CGFloat height = 0.0;
    
    self.navTitleLabel = [[UILabel alloc] init];
//    self.titleImageView.image = [UIImage imageNamed:@"privacy_title_icon"];
    self.navTitleLabel.text = @"蜀黍之家隐私政策";
    self.navTitleLabel.font = [UIFont systemFontOfSize:18];
    self.navTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.navTitleLabel];
    
    [self.navTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(kSizeScale(24));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(207), kSizeScale(16)));
    }];
    
    height = height + kSizeScale(40);
    
    self.titleLabel = [[YYLabel alloc] init];
    self.titleLabel.preferredMaxLayoutWidth = self.frame.size.width - kSizeScale(20);
    YYTextLinePositionSimpleModifier *modifier = [YYTextLinePositionSimpleModifier new];
    modifier.fixedLineHeight = 21;
    self.titleLabel.linePositionModifier = modifier;
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.top.equalTo(self.navTitleLabel.mas_bottom).offset(kSizeScale(12));
    }];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:title];
    text.font = midFont(13);
    text.color = XHBlackColor;
    NSRange range = NSMakeRange(0, 0);
    if ([title rangeOfString:@"《"].location != NSNotFound) {
        NSRange checkRange = [title rangeOfString:@"《"];
        range = NSMakeRange(checkRange.location, (title.length-checkRange.location-1));
    }
    [text setTextHighlightRange:range color:XHBlueColor backgroundColor:XHWhiteColor tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        @strongify(self);
        self.otherBlock(0);
        [self hideAlertView];
    }];
    
    self.titleLabel.attributedText = text;
    
    [self.titleLabel layoutIfNeeded];
    height = height + kSizeScale(12) + self.titleLabel.height;
    
    self.subTitleLabel = [[YYLabel alloc] init];
    self.subTitleLabel.text = subTitle;
    self.subTitleLabel.numberOfLines = 0;
    self.subTitleLabel.font = midFont(13);
    self.subTitleLabel.textColor = XHBlackLitColor;
    self.subTitleLabel.preferredMaxLayoutWidth = self.frame.size.width;
    YYTextLinePositionSimpleModifier *modifiers = [YYTextLinePositionSimpleModifier new];
    modifiers.fixedLineHeight = 21;
    self.subTitleLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.subTitleLabel.linePositionModifier = modifiers;
    self.subTitleLabel.preferredMaxLayoutWidth = self.titleLabel.preferredMaxLayoutWidth;
    [self addSubview:self.subTitleLabel];
    
    [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.titleLabel);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(kSizeScale(12));
    }];
    
    [self.subTitleLabel layoutIfNeeded];
    height = height + kSizeScale(12) + self.subTitleLabel.height;
    
    self.cancelBtn = [YYCreateTools createButton:@"不同意"
                                         bgColor:XHWhiteColor
                                       textColor:XHBlackColor];
    self.cancelBtn.layer.borderWidth = 1.0;
    self.cancelBtn.layer.masksToBounds = YES;
    self.cancelBtn.layer.cornerRadius = kSizeScale(2);
    self.cancelBtn.layer.borderColor = HexRGB(0xf1f1f1).CGColor;
    [self addSubview:self.cancelBtn];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.top.equalTo(self.subTitleLabel.mas_bottom).offset(kSizeScale(20));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(130), kSizeScale(40)));
    }];
    
    self.confirmBtn = [YYCreateTools createButton:@"同意"
                                         bgColor:XHLoginColor
                                       textColor:XHWhiteColor];
    self.confirmBtn.layer.cornerRadius = kSizeScale(2);
    self.confirmBtn.layer.masksToBounds = YES;
    [self addSubview:self.confirmBtn];
    
    [self.confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cancelBtn);
        make.right.equalTo(self.titleLabel);
        make.size.equalTo(self.cancelBtn);
    }];
    
    self.cancelBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        self.cancelBlock();
    };
    self.confirmBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        self.confirmBlock();
        [self hideAlertView];
    };
    
    [self.cancelBtn layoutIfNeeded];
    
    height = height + kSizeScale(80);
    frame.size.height = height;
    
    self.frame = frame;
}


- (void)showAlertView {
    if (self.superview) {
        [self removeFromSuperview];
    }
    UIView *oldView = [[UIApplication sharedApplication].keyWindow viewWithTag:TagValue];
    if (oldView) {
        [oldView removeFromSuperview];
    }
    UIView *iview = [[UIView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    iview.tag = TagValue;
    iview.userInteractionEnabled = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideAlertView)];
    [iview addGestureRecognizer:tap];
    iview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [[UIApplication sharedApplication].keyWindow addSubview:iview];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.center = [UIApplication sharedApplication].keyWindow.center;
    
    self.alpha = 0;
    self.transform = CGAffineTransformScale(self.transform,0.1,0.1);
    [UIView animateWithDuration:AlertTime animations:^{
        self.transform = CGAffineTransformIdentity;
        self.alpha = 1;
    }];
}

//弹出隐藏
- (void)hideAlertView {
    if (self.superview) {
        [UIView animateWithDuration:AlertTime animations:^{
            self.transform = CGAffineTransformScale(self.transform,0.1,0.1);
            self.alpha = 0;
        } completion:^(BOOL finished) {
            UIView *bgview = [[UIApplication sharedApplication].keyWindow viewWithTag:TagValue];
            if (bgview) {
                [bgview removeFromSuperview];
            }
            [self removeFromSuperview];
        }];
    }
}


@end
