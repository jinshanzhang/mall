//
//  YYAlertView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YYAlertType) {
    YYAlertPrivacyPolicyType = 0
};

typedef void (^ConfirmButtonClick) (void);
typedef void (^CancelButtonClick) (void);
typedef void (^OtherButtonClick) (NSInteger index);

@interface YYAlertView : UIView

- (instancetype)initWithFrame:(CGRect)frame
                        title:(NSString *)title
                     subTitle:(NSString *)subTitle
                    alertType:(YYAlertType)alertType
                   otherBlock:(void (^)(NSInteger))otherBlock
                 confirmBlock:(void (^)(void))confirmBlock
                  cancelBlock:(void (^)(void))cancelBlock;

- (void)showAlertView;
- (void)hideAlertView;
@end

NS_ASSUME_NONNULL_END
