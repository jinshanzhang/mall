//
//  YYGoodSkuCollectionFlowLayout.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYGoodSkuCollectionFlowLayout : UICollectionViewFlowLayout

@end

@protocol UICollectionViewDelegateLeftAlignedLayout <UICollectionViewDelegateFlowLayout>

@end
