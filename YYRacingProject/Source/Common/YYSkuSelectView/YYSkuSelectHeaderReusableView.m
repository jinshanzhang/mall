//
//  YYSkuSelectHeaderReusableView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYSkuSelectHeaderReusableView.h"

@interface YYSkuSelectHeaderReusableView()

@property (nonatomic, strong) UILabel  *titleLabel;

@end

@implementation YYSkuSelectHeaderReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel = [YYCreateTools createLabel:nil
                                                font:midFont(13)
                                           textColor:XHBlackColor];
        [self addSubview:self.titleLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(16));
            make.top.mas_equalTo(kSizeScale(8));
        }];
    }
    return self;
}

- (void)setShowTitle:(NSString *)showTitle {
    _showTitle = showTitle;
    self.titleLabel.text = _showTitle;
}

- (void)setTopSpace:(CGFloat)topSpace {
    @weakify(self);
    _topSpace = topSpace;
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(kSizeScale(16));
        make.top.mas_equalTo(kSizeScale(self.topSpace));
    }];
}

@end
