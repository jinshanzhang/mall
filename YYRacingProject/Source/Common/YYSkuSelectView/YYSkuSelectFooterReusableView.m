//
//  YYSkuSelectFooterReusableView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYSkuSelectFooterReusableView.h"

@interface YYSkuSelectFooterReusableView()

@property (nonatomic, strong) UIView  *topLineView;
@property (nonatomic, strong) UILabel *skuPropLabel;
@property (nonatomic, strong) UILabel *skuTipLabel;

@property (nonatomic, strong) YYNumberView *numberView;

@end

@implementation YYSkuSelectFooterReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        @weakify(self);
        self.topLineView = [YYCreateTools createView:XHLightColor];
        [self addSubview:self.topLineView];
        
        self.skuPropLabel = [YYCreateTools createLabel:@"购买数量"
                                                  font:midFont(14)
                                             textColor:XHBlackColor];
        [self addSubview:self.skuPropLabel];
        
        self.skuTipLabel = [YYCreateTools createLabel:nil
                                                  font:midFont(11)
                                             textColor:XHRedColor];
        [self addSubview:self.skuTipLabel];
        
        self.numberView = [[YYNumberView alloc] initWithFrame:CGRectZero];
        self.numberView.isOpenTipMsg = YES;
        self.numberView.isOpenTenLimit = YES;
        [self addSubview:self.numberView];
        
        [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(16));
            make.right.mas_equalTo(-kSizeScale(16));
            make.top.mas_equalTo(kSizeScale(15));
            make.height.mas_equalTo(kSizeScale(1));
        }];
        
        [self.numberView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-kSizeScale(12));
            make.top.equalTo(self.topLineView.mas_bottom).offset(kSizeScale(25));
            make.width.mas_equalTo(kSizeScale(105));
            make.height.mas_equalTo(kSizeScale(30));
        }];
        
        [self.skuPropLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.topLineView);
            make.centerY.equalTo(self.numberView);
        }];
        
        [self.skuTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.topLineView);
            make.top.equalTo(self.skuPropLabel.mas_bottom).offset(kSizeScale(5));
        }];
        
        self.numberView.addClick = ^(NSInteger number) {
            @strongify(self);
            if (self.numberOperatorBlock) {
                self.numberOperatorBlock(number);
            }
        };
        
        self.numberView.reduceClick = ^(NSInteger number) {
            @strongify(self);
            if (self.numberOperatorBlock) {
                self.numberOperatorBlock(number);
            }
        };
    }
    return self;
}

- (void)setSalingType:(YYGoodSalingStatusType)salingType {
    _salingType = salingType;
    if (_salingType != YYGoodSalingStatusNoraml) {
        self.numberView.afterSaleTip = [NSString stringWithFormat:@"每单限购%ld件",self.maxValue];
    }
    self.skuTipLabel.hidden = (_salingType == YYGoodSalingStatusNoraml?YES:NO);
}

- (void)setCurrentValue:(NSInteger)currentValue {
    _currentValue = currentValue;
    self.numberView.currentValue = _currentValue;
}

- (void)setMaxValue:(NSInteger)maxValue {
    _maxValue = maxValue;
    self.numberView.maxValue = _maxValue;
}

- (void)setSpecialValue:(NSInteger)specialValue {
    _specialValue = specialValue;
    self.skuTipLabel.text = [NSString stringWithFormat:@"本次特卖限购%ld件",_specialValue];
}

@end
