//
//  YYSkuSelectFooterReusableView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYSkuSelectFooterReusableView : UICollectionReusableView

@property (nonatomic, assign) NSInteger maxValue;
@property (nonatomic, assign) NSInteger currentValue;
@property (nonatomic, assign) NSInteger specialValue; //特卖展示的价格

@property (nonatomic, assign) YYGoodSalingStatusType salingType;
@property (nonatomic, copy) void (^numberOperatorBlock)(NSInteger value);

@end
