//
//  YYSkuSelectHeaderReusableView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYSkuSelectHeaderReusableView : UICollectionReusableView

@property (nonatomic, strong) NSString *showTitle;
@property (nonatomic, assign) CGFloat  topSpace;

@end
