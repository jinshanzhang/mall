//
//  YYSkuSelectCollectionViewCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYSkuSelectCollectionViewCell.h"

#import "UIColor+Extension.h"
#import "HomeGoodInfoModel.h"
#import "HomeGoodPropSkuInfoModel.h"

@interface YYSkuSelectCollectionViewCell()

@property (nonatomic, strong) UIButton  *skuBtn;

@end

@implementation YYSkuSelectCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.skuBtn = [YYCreateTools createButton:nil
                                          bgColor:HexRGB(0xf7f7f7)
                                        textColor:XHBlackColor];
        self.skuBtn.isIgnore = YES;
        self.skuBtn.layer.cornerRadius = 2;
        self.skuBtn.layer.masksToBounds = YES;
        self.skuBtn.titleLabel.font = midFont(13);
        self.skuBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        self.skuBtn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.skuBtn setTitleColor:XHWhiteColor forState:UIControlStateSelected];
        [self.skuBtn setTitleEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
        
//        [self.skuBtn setBackgroundImage:[UIImage gradientColorImageFromColors:@[HexRGB(0xE6B96E), HexRGB(0xE2A848)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(kSizeScale(50), kSizeScale(30))] forState:UIControlStateSelected];
        [self.skuBtn setBackgroundImage:[UIImage imageWithColor:XHMainColor] forState:UIControlStateSelected];
        [self.contentView addSubview:self.skuBtn];
        
        [self.skuBtn addTarget:self action:@selector(btnSelectClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.skuBtn.frame = CGRectMake(0, 0, self.contentView.width, self.contentView.height);
}
#pragma mark - Setter

- (void)setSkuModel:(HomeGoodValueSkuInfoModel *)skuModel {
    _skuModel = skuModel;
    [self.skuBtn setTitle:_skuModel.name forState:UIControlStateNormal];
    [self handlerSkuButtonSelectStatu:_skuModel.isSelect isNull:_skuModel.isNull];
}

#pragma mark - Private
- (void)handlerSkuButtonSelectStatu:(BOOL)select isNull:(BOOL)isNull {
    if (select) {
        [self.skuBtn setSelected:YES];
    }
    else {
        [self.skuBtn setSelected:NO];
        if (isNull) {
            // 空
            [self.skuBtn setTitleColor:HexRGB(0xCCCCCC) forState:UIControlStateNormal];
            [self.skuBtn setBackgroundImage:[UIImage imageWithColor:HexRGB(0xf4f4f4)] forState:UIControlStateNormal];
        }
        else {
            // 非空
            [self.skuBtn setTitleColor:XHBlackColor forState:UIControlStateNormal];
            [self.skuBtn setBackgroundImage:[UIImage imageWithColor:HexRGB(0xf7f7f7)] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - Event
- (void)btnSelectClickEvent:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(yySkuSelectCellDidSelect:)]) {
        [self.delegate yySkuSelectCellDidSelect:self];
    }
}

@end
