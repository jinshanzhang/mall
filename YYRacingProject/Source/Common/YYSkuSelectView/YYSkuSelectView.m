//
//  YYSkuSelectView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYSkuSelectView.h"

#import "UIColor+Extension.h"
#import "HomeGoodPropSkuInfoModel.h"

#import "YYSkuSelectCollectionViewCell.h"
#import "YYGoodSkuCollectionFlowLayout.h"
#import "YYSkuSelectFooterReusableView.h"
#import "YYSkuSelectHeaderReusableView.h"

#define  kCellBtnCenterToBorderMargin   kSizeScale(10)
#define  kCellBtnHeight                 kSizeScale(30)
#define  kCellsHorizonMargin            kSizeScale(16)
#define  kCellsToTopBottomMargin        kSizeScale(8)
#define  kCellsToLeftRightMargin        kSizeScale(16)
@interface YYSkuSelectView()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
YYSkuSelectCollectionViewCellDelegate
>
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UIButton *addCartBtn;
@property (nonatomic, strong) UIButton *buyBtn;
@property (nonatomic, strong) UIButton *mealZoneSureBtn;
@property (nonatomic, strong) UILabel  *tipLabel;
@property (nonatomic, strong) UILabel  *extraLabel;
@property (nonatomic, strong) UILabel  *skuLabel;
@property (nonatomic, strong) UILabel  *goodPriceLabel;

@property (nonatomic, strong) UIImageView *skuImageView;
@property (nonatomic, strong) UICollectionView *m_collectionView;
@property (nonatomic, strong) YYSkuSelectFooterReusableView *footerView;

@property (nonatomic, copy)   NSString  *showTip;
@property (nonatomic, copy)   NSString  *showPrice;
@property (nonatomic, copy)   NSString  *showCommissionPrice;
@property (nonatomic, strong) NSString  *skuLargeUrl;
@property (nonatomic, strong) NSMutableArray *propLists;


@property (nonatomic, assign) NSInteger  curRow;
@property (nonatomic, assign) NSInteger  currentValue;
@property (nonatomic, assign) NSInteger  maxValue;
@property (nonatomic, assign) NSInteger  curSelectSection;
@property (nonatomic, assign) BOOL       isClickBuyBtn;

@property (nonatomic, assign) YYSkuSelectViewDisType pageDisType;
@property (nonatomic, strong) HomeGoodValueSkuInfoModel *selectSkuModel;
@end

@implementation YYSkuSelectView

#pragma mark - Setter
- (UICollectionView *)m_collectionView {
    if (!_m_collectionView) {
        YYGoodSkuCollectionFlowLayout *skuFlowLayout = [[YYGoodSkuCollectionFlowLayout alloc] init];
        _m_collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:skuFlowLayout];
        _m_collectionView.delegate = self;
        _m_collectionView.dataSource = self;
        _m_collectionView.backgroundColor = XHWhiteColor;
        _m_collectionView.showsHorizontalScrollIndicator = NO;
        _m_collectionView.showsVerticalScrollIndicator = NO;
        _m_collectionView.allowsSelection = NO;
    }
    return _m_collectionView;
}

- (void)setGoodInfo:(HomeGoodInfoModel *)goodInfo {
    _goodInfo = goodInfo;
    self.propLists = _goodInfo.props;
    self.currentValue = _goodInfo.currentValue;
    self.isClickBuyBtn = YES;
    if (kValidArray(self.propLists)) {
        __block NSInteger selectSkuProCount = 0;
        [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
            [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
                if (model.isSelect) {
                    selectSkuProCount ++;
                    *stop = YES;
                }
            }];
        }];
        if (selectSkuProCount <= 1 && selectSkuProCount > 0) {
            [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
                [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    BOOL isNull = NO;
                    HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
                    if ([model.storage integerValue]<=0) {
                        isNull = YES;
                    }
                    model.isNull = isNull;
                }];
            }];
        }
    }

    [self checkSelectSku];
    [self refreshSkuSelectView];
    if (_goodInfo.specialType == 1) {
        [self specialRemarkSubViewLayout];
    }
    if (_goodInfo.salingType == YYGoodSalingStatusPreHot) {
        [self remarkSubViewLayout];
    }
    
    //是否显示确定按钮
    self.mealZoneSureBtn.hidden = !goodInfo.isMealZonePay;
}

#pragma mark - Life cycle

- (instancetype)initSkuSelectView {
    self = [super init];
    if (self) {
        @weakify(self);
        self.backgroundColor = XHWhiteColor;
        self.frame = CGRectMake(0, kScreenH, kScreenW, kSizeScale(480)+kBottom(0));
        
        self.curRow = -1;
        self.maxValue = 5;
        self.currentValue = 1;
        self.isClickBuyBtn = YES;
        self.curSelectSection = -1;
        self.pageDisType = YYSkuSelectViewDisDefault;
        self.propLists = [NSMutableArray array];
        
        [self createUI];
        self.dissClickBlock = ^{
            @strongify(self);
            [self closeClickEvent:nil];
        };
    }
    return self;
}

- (void)createUI {
    self.closeBtn = [YYCreateTools createBtnImage:@"goodCoupon_close_icon"];
    [self addSubview:self.closeBtn];
    
    self.skuImageView = [YYCreateTools createImageView:nil
                                             viewModel:-1];
    [self.skuImageView zy_cornerRadiusAdvance:4 rectCornerType:UIRectCornerAllCorners];
    self.skuImageView.image = [UIImage imageWithColor:XHRedColor];
    [self addSubview:self.skuImageView];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(17)
                                           textColor:XHBlackColor];
    [self addSubview:self.goodPriceLabel];
    
    self.tipLabel = [YYCreateTools createLabel:nil
                                          font:midFont(14)
                                     textColor:XHRedColor];
    [self addSubview:self.tipLabel];
    
    self.extraLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(17)
                                       textColor:XHRedColor];
    [self addSubview:self.extraLabel];
    
    self.skuLabel = [YYCreateTools createLabel:@"请选择规格"
                                          font:midFont(13)
                                     textColor:XHBlackLitColor];
    self.skuLabel.numberOfLines = 0;
    [self addSubview:self.skuLabel];
    
    self.addCartBtn = [YYCreateTools createButton:@"加入购物车"
                                          bgColor:XHMainColor
                                        textColor:XHWhiteColor];
    self.addCartBtn.titleLabel.font = midFont(17);
    [self addSubview:self.addCartBtn];
    self.buyBtn = [YYCreateTools createButton:@"立即购买"
                                          bgColor:XHGaldMidColor
                                        textColor:XHWhiteColor];
    self.buyBtn.titleLabel.font = midFont(17);
    [self addSubview:self.buyBtn];
    
    //蜀黍专区确定按钮
    self.mealZoneSureBtn = [YYCreateTools createButton:@"确定"
                                          bgColor:XHMainColor
                                        textColor:XHWhiteColor];
    self.mealZoneSureBtn.titleLabel.font = MediumFont(18);
    self.mealZoneSureBtn.hidden = YES;
    [self.mealZoneSureBtn addTarget:self action:@selector(closeClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.mealZoneSureBtn];
    
    Class currentCls = [YYSkuSelectCollectionViewCell class];
    [self.m_collectionView registerClass:currentCls forCellWithReuseIdentifier:strFromCls(currentCls)];
    currentCls = [YYSkuSelectHeaderReusableView class];
    [self.m_collectionView registerClass:currentCls forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:strFromCls(currentCls)];
    currentCls = [YYSkuSelectFooterReusableView class];
    [self.m_collectionView registerClass:currentCls forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:strFromCls(currentCls)];
    [self addSubview:self.m_collectionView];
    
    [self setSubLayout];
    
    [self.skuImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickPreviewEvent:)]];
    [self.buyBtn addTarget:self action:@selector(closeClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.closeBtn addTarget:self action:@selector(closeClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.addCartBtn addTarget:self action:@selector(closeClickEvent:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.propLists.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (kValidArray(self.propLists)) {
        HomeGoodPropSkuInfoModel *propSkuModel = [self.propLists objectAtIndex:section];
        return propSkuModel.values.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    Class currentCls = [YYSkuSelectCollectionViewCell class];
    YYSkuSelectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(currentCls) forIndexPath:indexPath];
    cell.delegate = self;
    if (kValidArray(self.propLists) && section < self.propLists.count) {
        HomeGoodPropSkuInfoModel *propSkuModel = [self.propLists objectAtIndex:section];
        cell.skuModel = [propSkuModel.values objectAtIndex:row];
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    NSInteger section = indexPath.section;
    Class current = [YYSkuSelectHeaderReusableView class];
    if ([kind isEqualToString:@"UICollectionElementKindSectionHeader"]) {
        YYSkuSelectHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
        if (kValidArray(self.propLists)) {
            HomeGoodPropSkuInfoModel *propSkuModel = [self.propLists objectAtIndex:section];
            headerView.showTitle = propSkuModel.name;
            //headerView.topSpace = (section == 0? kSizeScale(10): kSizeScale(8));
        }
        return headerView;
    }
    else if ([kind isEqualToString:@"UICollectionElementKindSectionFooter"]) {
        current = [YYSkuSelectFooterReusableView class];
        YYSkuSelectFooterReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
        footerView.maxValue  = self.maxValue;
        footerView.specialValue = self.goodInfo.limitTotal;
        footerView.currentValue = self.currentValue;
        footerView.salingType = self.goodInfo.salingType;
        footerView.numberOperatorBlock = ^(NSInteger value) {
            @strongify(self);
            self.currentValue = value;
        };
        if (_goodInfo.salingType == YYGoodSalingStatusPreHot) {
            footerView.hidden = YES;
        }
        return footerView;
    }
    return [UICollectionReusableView new];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    if (kValidArray(self.propLists)) {
        HomeGoodPropSkuInfoModel *propSkuModel = [self.propLists objectAtIndex:section];
        HomeGoodValueSkuInfoModel *skuValues = [propSkuModel.values objectAtIndex:row];
        return CGSizeMake([self collectionCellWidthText:skuValues.name], kCellBtnHeight);
    }
    return CGSizeZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return kCellsHorizonMargin;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(kCellsToTopBottomMargin, kCellsToLeftRightMargin, kCellsToTopBottomMargin, kCellsToLeftRightMargin);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    if (kValidArray(self.propLists)) {
        if (section == (self.propLists.count - 1)) {
            return CGSizeMake(kScreenW, kSizeScale(100));
        }
        return CGSizeZero;
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (kValidArray(self.propLists)) {
        return CGSizeMake(kScreenW, kSizeScale(30));
    }
    return CGSizeZero;
}

#pragma mark - YYSkuSelectCollectionViewCellDelegate
/**SKU 属性选择**/
- (void)yySkuSelectCellDidSelect:(YYSkuSelectCollectionViewCell *)selectCell {
    NSIndexPath *selectIndex = [self.m_collectionView indexPathForCell:selectCell];

    self.curRow = selectIndex.row;;
    self.curSelectSection = selectIndex.section;
    
    if (kValidArray(self.propLists)) {
        [self changeAtIndexPathSelectStatus:self.curSelectSection row:self.curRow];
    }
    [self.m_collectionView reloadData];
}

#pragma mark - Private
/**预热重置按钮的布局**/
- (void)remarkSubViewLayout {
    self.buyBtn.hidden = YES;
    [self.addCartBtn setTitle:[NSString stringWithFormat:@"%@开抢",_goodInfo.preTimeFormat] forState:UIControlStateNormal];
    [self.addCartBtn setBackgroundImage:[UIImage imageWithColor:HexRGB(0x11BC56)] forState:UIControlStateNormal];
    [self.addCartBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(50));
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
    [self.m_collectionView reloadData];
}

/**新人专享重置按钮的布局**/
- (void)specialRemarkSubViewLayout {
    [self.addCartBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(0, kSizeScale(50)));
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
    
    [self.buyBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.left.equalTo(self.addCartBtn.mas_right).offset(0);
        make.height.equalTo(self.addCartBtn);
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
}

/**初始化子视图布局**/
- (void)setSubLayout {
    CGFloat btnWidth = self.width/2.0;
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(22), kSizeScale(22)));
    }];
    
    [self.skuImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(-kSizeScale(15));
        make.left.mas_equalTo(kSizeScale(16));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(100), kSizeScale(100)));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top).offset(kSizeScale(10));
        make.left.mas_equalTo(self.skuImageView.mas_right).offset(kSizeScale(12));
    }];
    
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(10));
        make.centerY.equalTo(self.goodPriceLabel).offset(-0.3);
    }];
    
    [self.extraLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tipLabel.mas_right);
        make.centerY.equalTo(self.goodPriceLabel);
    }];
    
    [self.skuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_left);
        make.top.equalTo(self.goodPriceLabel.mas_bottom).offset(kSizeScale(5));
        make.right.mas_equalTo(-kSizeScale(30));
    }];
    
    [self.addCartBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(btnWidth, kSizeScale(50)));
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
    
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.left.equalTo(self.addCartBtn.mas_right).offset(-0.5);
        make.height.equalTo(self.addCartBtn);
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
    
    [self.mealZoneSureBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(50));
        make.bottom.equalTo(self.mas_bottom).offset(-kBottom(0));
    }];
    
    [self.m_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.skuImageView.mas_bottom).offset(kSizeScale(24));
        make.bottom.equalTo(self.addCartBtn.mas_top);
    }];
    
//    [self.addCartBtn setBackgroundImage:[UIImage gradientColorImageFromColors:@[HexRGB(0x323264), HexRGB(0x191946)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(btnWidth, kSizeScale(50))] forState:UIControlStateNormal];
    
    [self.buyBtn setBackgroundImage:[UIImage gradientColorImageFromColors:@[HexRGB(0xE6B96E), HexRGB(0xE2A848)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(btnWidth, kSizeScale(50))] forState:UIControlStateNormal];
    [self.addCartBtn setBackgroundImage:[UIImage imageWithColor:XHMainColor] forState:UIControlStateNormal];


}


/**
 *  刷新数据
 **/
- (void)refreshSkuSelectView {
    [self.m_collectionView reloadData];
}

/**操作数据展示**/
- (void)operatorShowData {
    if (self.selectSkuModel) {
        self.skuLargeUrl = kValidString(self.selectSkuModel.imageUrl)?self.selectSkuModel.imageUrl:self.skuLargeUrl;
        if (!kValidString(self.skuLargeUrl)) {
            self.skuLargeUrl = [self.goodInfo.auctionImages firstObject];
        }
    }
    else {
        self.skuLargeUrl = [self.goodInfo.auctionImages firstObject];
    }
    self.skuLabel.text = self.showTip;
    if (kIsFan) {
        //粉丝
        self.tipLabel.text = nil;
        self.extraLabel.text = nil;
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(17) symbolTextColor:XHRedColor wordSpace:2 price:self.showPrice priceFont:boldFont(17) priceTextColor: XHRedColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
    }
    else {
        //会员
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(17) symbolTextColor:XHBlackColor wordSpace:2 price:self.showPrice priceFont:boldFont(17) priceTextColor: XHBlackColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
        if (kValidString(self.showCommissionPrice)) {
            self.tipLabel.text = @"赚";
            NSMutableAttributedString *extra = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",self.showCommissionPrice]];
            [attribut yy_setAttributeWordSpace:@(1) content:extra alignment:NSTextAlignmentCenter];
            self.extraLabel.attributedText = extra;
        }
        else {
            self.tipLabel.text = @"";
            self.extraLabel.attributedText = [NSMutableAttributedString new];
        }
    }
    
    [self.skuImageView sd_setImageWithURL:[NSURL URLWithString:self.skuLargeUrl] placeholderImage:[UIImage imageNamed:@"sku_photo_icon"]];
    
    [UIView performWithoutAnimation:^{
        [self.m_collectionView reloadData];
    }];
}

/**
 *  根据文本获取size的大小
 **/
- (float)collectionCellWidthText:(NSString *)text{
    float cellWidth;
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:14]}];
    if (ceilf(size.width) > (kScreenW - kSizeScale(24))) {
        cellWidth = kScreenW - kSizeScale(24);
    }
    else {
        cellWidth = ceilf(size.width) + kCellBtnCenterToBorderMargin;
    }
    if (cellWidth < kSizeScale(39)) {
        cellWidth = kSizeScale(39);
    }
    return cellWidth;
}

/**判断是否已经选择一个类型**/
- (BOOL)checkIsNull:(NSInteger)section row:(NSInteger)row {
    __block NSInteger tempRow = row;
    __block BOOL      tempIsNull = NO;
    __block NSInteger tempSection = section;
    [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
        if (tempSection == idx) {
            [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
                if (tempRow == idx) {
                    tempIsNull = model.isNull;
                    *stop = YES;
                }
            }];
            *stop = YES;
        }
    }];
    return tempIsNull;
}

/**
 *  查询选中的SKU
 **/
- (void)checkSelectSku {
    
    self.showCommissionPrice = nil;
    self.showTip = self.goodInfo.propTipInfo;
    self.showPrice = self.goodInfo.skuDefaultPrice;
    
    NSInteger count = self.propLists.count; //查询有几种类型
    __block NSMutableArray *selectSkuList = [NSMutableArray array];
    [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
        [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
            if (model.isSelect) {
                [selectSkuList addObject:model];
            }
        }];
    }];
    
    if (kValidArray(selectSkuList)) {
        if (count == selectSkuList.count && count > 1) {
            __block NSString *splicStr = [NSString new];
            __block NSString *selectTipStr = [NSString new];
            [selectSkuList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodValueSkuInfoModel *selectModel = (HomeGoodValueSkuInfoModel *)obj;
                splicStr =  [splicStr stringByAppendingString:[NSString stringWithFormat:@"%@:%@",selectModel.pid,selectModel.vid]];
                selectTipStr = [selectTipStr stringByAppendingString:selectModel.name];
                if (idx == 0) {
                    splicStr = [splicStr stringByAppendingString:@";"];
                    selectTipStr = [selectTipStr stringByAppendingString:@"、"];
                }
            }];
            [self.goodInfo.skus enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodValueSkuInfoModel *infoModel = (HomeGoodValueSkuInfoModel *)obj;
                if ([infoModel.propPath isEqualToString:splicStr]) {
                    self.selectSkuModel = infoModel;
                    *stop = YES;
                }
            }];
            [self.selectSkuModel.priceInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodPriceInfoModel *priceModel = (HomeGoodPriceInfoModel *)obj;
                if (self.goodInfo.salingType == YYGoodSalingStatusPreHot) {
                    if (priceModel.priceType == 3) {
                        self.showCommissionPrice = priceModel.commissionPriceTag;
                        self.showPrice = priceModel.priceTag;
                    }
                }
                else {
                    if (priceModel.priceType == 2) {
                        self.showCommissionPrice = priceModel.commissionPriceTag;
                        self.showPrice = priceModel.priceTag;
                    }
                }
            }];
            self.maxValue = [self.selectSkuModel.storage integerValue] >=5 ? 5: [self.selectSkuModel.storage integerValue];
            self.currentValue = [self.selectSkuModel.storage integerValue] >=self.currentValue ? self.currentValue: [self.selectSkuModel.storage integerValue];
            self.showTip = selectTipStr;
        }
        else {
            if (count <= 1) {
                self.selectSkuModel = [selectSkuList firstObject];
                HomeGoodValueSkuInfoModel *childSku = [self.selectSkuModel.childArrs firstObject];
                self.selectSkuModel.skuId = childSku.skuId;
                
                [childSku.priceInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    HomeGoodPriceInfoModel *priceModel = (HomeGoodPriceInfoModel *)obj;
                    if (self.goodInfo.salingType == YYGoodSalingStatusPreHot) {
                        if (priceModel.priceType == 3) {
                            self.showCommissionPrice = priceModel.commissionPriceTag;
                            self.showPrice = priceModel.priceTag;
                        }
                    }
                    else {
                        if (priceModel.priceType == 2) {
                            self.showCommissionPrice = priceModel.commissionPriceTag;
                            self.showPrice = priceModel.priceTag;
                        }
                    }
                }];
                
                self.showTip = self.selectSkuModel.name;
                self.maxValue = [self.selectSkuModel.storage integerValue] >=5 ? 5: [self.selectSkuModel.storage integerValue];
                self.currentValue = [self.selectSkuModel.storage integerValue] >=self.currentValue ? self.currentValue: [self.selectSkuModel.storage integerValue];
            }
            else {
                self.selectSkuModel = [selectSkuList firstObject];
                self.showTip = [self gainSelectPropTipInfo:self.selectSkuModel.pid];
                //self.maxValue = [self.selectSkuModel.storage integerValue];
                self.maxValue = 5;
            }
        }
    }
    else {
        //  没选中sku
        self.selectSkuModel = nil;
    }
    self.maxValue = ((self.maxValue <=0)?1:self.maxValue);
    if (_goodInfo.salingType != YYGoodSalingStatusNoraml) {
        self.maxValue = _goodInfo.limitTotal >= 5 ? 5 : _goodInfo.limitTotal;
    }
    [self operatorShowData];
}
/**
 *  修改sku 属性选中的状态
 **/
- (void)changeAtIndexPathSelectStatus:(NSInteger)section
                                  row:(NSInteger)row {
    __block NSInteger tempRow = row;
    __block NSInteger tempSection = section;
    __block BOOL      isSelect = NO;
    __block NSString  *spliceStr = nil;
    if ([self checkIsNull:section row:row]) {
        //判断当前选中的item 库存量是否为空，如果为空则返回。
        return;
    }
    [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
        if (tempSection == idx) {
            [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
                // 选中或者取消已选中的item
                if (tempRow == idx && !model.isNull) {
                    model.isSelect = !model.isSelect;
                    isSelect = model.isSelect;
                    spliceStr = [NSString stringWithFormat:@"%@:%@",skuModel.pid,model.vid];
                }
                else {
                    model.isSelect = NO;
                }
            }];
            *stop = YES;
        }
    }];
    // 判断是否已选中一项属性，如果是遍历查询其他属性是否存在库存为空，如果有将状态置为Null
    if (isSelect) {
        [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
            if (tempSection != idx) {
                [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    __block BOOL isNull = NO;
                    HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
                    [model.childArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        HomeGoodValueSkuInfoModel *infoModel = (HomeGoodValueSkuInfoModel *)obj;
                        if ([infoModel.propPath rangeOfString:spliceStr].location != NSNotFound) {
                            if ([infoModel.storage integerValue]<=0) {
                                isNull = YES;
                                *stop = YES;
                            }
                        }
                    }];
                    model.isNull = isNull;
                }];
                *stop = YES;
            }
        }];
    }
    else {
        // 如果不存在选中属性，则将所有属性是否库存为空的字段重置。
        [self.propLists enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPropSkuInfoModel *skuModel = (HomeGoodPropSkuInfoModel *)obj;
            if (tempSection != idx) {
                [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    __block BOOL isNull = NO;
                    HomeGoodValueSkuInfoModel *model = (HomeGoodValueSkuInfoModel *)obj;
                    model.isNull = isNull;
                }];
                *stop = YES;
            }
        }];
    }
    [self checkSelectSku];
}
/**获取提示信息**/
- (NSString *)gainSelectPropTipInfo:(NSString *)pid {
    __block NSString *spliceStr = [NSString new];
    NSInteger count = self.goodInfo.originProps.count;
    if (count > 1) {
        [self.goodInfo.originProps enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPropSkuInfoModel *model = (HomeGoodPropSkuInfoModel *)obj;
            if ([pid integerValue] != [model.pid integerValue] && count ) {
                spliceStr = [NSString stringWithFormat:@"请选择%@", model.name];
                *stop = YES;
            }
        }];
    }
    return spliceStr;
}

/**弹出视图关闭参数回调**/
- (void)dissSkuSkipView {
    if (self.selectBlock) {
        [self showViewOfAnimateType:YYPopAnimateDownUp];
        self.selectBlock(self.showTip, self.selectSkuModel, self.currentValue, self.pageDisType);
    }
}

#pragma mark - Request
/**添加购物车**/
- (void)addSkuToShippingCart {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(0) forKey:@"action"];
    if ([self.selectSkuModel.skuId integerValue] <= 0) {
        [YYCommonTools showTipMessage:self.showTip];
        return;
    }
    NSDictionary *dict = @{@"skuId":self.selectSkuModel.skuId,@"skuCnt":@(self.currentValue)};
    [params setObject:@[dict] forKey:@"skuList"];
    [kWholeConfig addGoodSkuToShippingCart:params
                                   success:^(BOOL isSuccess) {
                                       if (isSuccess) {
                                           [self dissSkuSkipView];
                                       }
                                   }];
}


/**确认订单**/
- (void)confirmOrderPageWithPreviewSwitch:(int)previewSwitch {
    if ([self.selectSkuModel.skuId integerValue] <= 0) {
        [YYCommonTools showTipMessage:self.showTip];
        return;
    }
    self.isClickBuyBtn = NO;
    
    NSDictionary *dict = @{@"skuId":self.selectSkuModel.skuId,@"skuCnt":@(self.currentValue)};
    [kWholeConfig submitOrderWithSkulist:[@[dict] mutableCopy]
                                   souce:1
                                   addID:0
                           previewSwitch:previewSwitch
     couponId:@[@(-1)]
                                 payment:self.goodInfo.shareInfo.payment
                           deliveryType : [NSString stringWithFormat:@"%@",self.goodInfo.goodsType]
                                   block:^(PreOrderModel *model) {
                                       self.isClickBuyBtn = YES;
                                       [self dissSkuSkipView];
        model.isMealZonePay = (previewSwitch == 3);
        [YYCommonTools skipPreOrder:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:model skuArr:[@[dict] mutableCopy] source:1];
                                   } failBlock:^(PreOrderModel *model) {
                                       self.isClickBuyBtn = YES;
                                   }];
}

#pragma mark - Event
- (void)closeClickEvent:(UIButton *)sender {
    if (!sender) {
        self.pageDisType = YYSkuSelectViewDisDefault;
        [self dissSkuSkipView];
    }
    else if (self.addCartBtn == sender) {
        self.pageDisType = YYSkuSelectViewDisAddCart;
        if (_goodInfo.salingType == YYGoodSalingStatusPreHot) {
            [YYCommonTools showTipMessage:[NSString stringWithFormat:@"%@开抢",_goodInfo.preTimeFormat]];
            return;
        }
        [self addSkuToShippingCart];
    }
    else if (self.closeBtn == sender) {
        self.pageDisType = YYSkuSelectViewDisClose;
        [self dissSkuSkipView];
    }
    else if (self.buyBtn == sender) {
        if (!self.isClickBuyBtn) {
            // 判断是否在发送请求期间
            return;
        }
        self.pageDisType = YYSkuSelectViewDisBuy;
        [self confirmOrderPageWithPreviewSwitch:1];
    }
    else if (self.mealZoneSureBtn == sender) {
        //饭卡支付 isMealZonePay
        if (!self.isClickBuyBtn) {
            // 判断是否在发送请求期间
            return;
        }
        self.pageDisType = YYSkuSelectViewDisBuy;
        [self confirmOrderPageWithPreviewSwitch:3];
    }
}

- (void)clickPreviewEvent:(UITapGestureRecognizer *)tapGesture {
    UIView *fromView;
    NSMutableArray *items = [NSMutableArray new];
    
    for (NSUInteger i = 0, max = 1; i < max; i++) {
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = self.skuImageView;
        item.largeImageURL = [NSURL URLWithString:self.skuLargeUrl];
        
        [items addObject:item];
        fromView = self.skuImageView;
    }
    YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:0];
    [v presentFromImageView:fromView toContainer:kAppWindow animated:YES completion:nil];
    v.hideViewBlock = ^(NSInteger currentPage) {
        
    };
}
@end
