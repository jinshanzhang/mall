//
//  YYSkuSelectCollectionViewCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeGoodPropSkuInfoModel.h"

@protocol YYSkuSelectCollectionViewCellDelegate;
@interface YYSkuSelectCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id <YYSkuSelectCollectionViewCellDelegate> delegate;
@property (nonatomic, strong) HomeGoodValueSkuInfoModel *skuModel;

@end

@protocol YYSkuSelectCollectionViewCellDelegate <NSObject>

@optional
- (void)yySkuSelectCellDidSelect:(YYSkuSelectCollectionViewCell *)selectCell;

@end


