//
//  YYSkuSelectView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseShareView.h"
#import "HomeGoodInfoModel.h"
#import "HomeGoodPropSkuInfoModel.h"

typedef NS_ENUM(NSInteger, YYSkuSelectViewDisType) {
    YYSkuSelectViewDisDefault = 0,
    YYSkuSelectViewDisClose,
    YYSkuSelectViewDisAddCart,
    YYSkuSelectViewDisBuy
};

typedef void (^skuSelectBlock)(NSString *showTip, HomeGoodValueSkuInfoModel *selectModel, NSInteger currentValue, YYSkuSelectViewDisType disType);
@interface YYSkuSelectView : YYBaseShareView

@property (nonatomic, strong) HomeGoodInfoModel *goodInfo;
@property (nonatomic, copy)   skuSelectBlock   selectBlock;

- (instancetype)initSkuSelectView;

@end
