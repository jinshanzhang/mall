//
//  ShoppingCarNumberView.h
//  XinHuoApp
//
//  Created by born on 2018/1/8.
//  Copyright © 2018年 蔡金明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYNumberView : UIView

@property (nonatomic , assign) NSInteger currentValue;     //当前值
@property (nonatomic , assign) NSInteger minValue;
@property (nonatomic , assign) NSInteger maxValue;

@property (nonatomic , assign) BOOL      isOpenTipMsg;    //是否开启提示信息
@property (nonatomic , assign) BOOL      isOpenTenLimit;  //是否开启商品限制10件的功能
@property (nonatomic , assign) BOOL      noEvent;         //是否不需要点击事件
@property (nonatomic,  copy)   NSString  *afterSaleTip;
//这里购物车需要
@property (nonatomic , copy) void (^addClick)(NSInteger number);
@property (nonatomic , copy) void (^reduceClick)(NSInteger number);
@property (nonatomic , copy) void (^inputNumberBlock)(NSInteger number);

@end



