//
//  ShoppingCarNumberView.m
//  XinHuoApp
//
//  Created by born on 2018/1/8.
//  Copyright © 2018年 蔡金明. All rights reserved.
//

#import "YYNumberView.h"

@interface YYNumberView()
<UITextFieldDelegate>

@property (nonatomic , strong) UITextField *number_field;
@property (nonatomic , strong) UIButton    *reduce_btn;
@property (nonatomic , strong) UIButton    *add_btn;
@property (nonatomic , strong) UILabel     *reduce_line;
@property (nonatomic , strong) UILabel     *add_line;

@end

@implementation YYNumberView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.isOpenTipMsg = YES;
        self.isOpenTenLimit = NO;
        self.noEvent = NO;
        
        self.userInteractionEnabled = YES;
        self.layer.borderColor = HexRGB(0xcccccc).CGColor;
        self.layer.borderWidth = .5;
        
        _maxValue = INT_MAX;
        _minValue = 1;
        
        self.reduce_line = [[UILabel alloc] init];
        self.reduce_line.backgroundColor = HexRGB(0xcccccc);
        
        self.reduce_btn = [YYCreateTools createButton:@"-" bgColor:XHWhiteColor textColor:XHBlackColor];
        self.reduce_btn.isIgnore = YES;
        [self.reduce_btn setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [self.reduce_btn setTitleColor:HexRGB(0xdddddd) forState:UIControlStateSelected];
        [self.reduce_btn enlargeTouchAreaWithTop:20 right:0 bottom:20 left:20];
        
        self.number_field = [YYCreateTools createTextField:@""
                                                      font:normalFont(14)
                                                  texColor:XHBlackColor];
        self.number_field.textAlignment = NSTextAlignmentCenter;
        self.number_field.delegate = self;
        self.number_field.keyboardType = UIKeyboardTypeNumberPad;
        self.number_field.enabled = NO;
        
        self.add_btn = [YYCreateTools createButton:@"+" bgColor:XHWhiteColor textColor:XHBlackColor];
        self.add_btn.isIgnore = YES;
        [self.add_btn setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [self.add_btn setTitleColor:HexRGB(0xdddddd) forState:UIControlStateSelected];
        [self.add_btn enlargeTouchAreaWithTop:0 right:30 bottom:30 left:0];
        
        self.add_line = [[UILabel alloc] init];
        self.add_line.backgroundColor = HexRGB(0xcccccc);
        
        [self.reduce_btn addTarget:self action:@selector(reduceClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.add_btn addTarget:self action:@selector(addClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.reduce_btn];
        [self addSubview:self.reduce_line];
        [self addSubview:self.number_field];
        [self addSubview:self.add_btn];
        [self addSubview:self.add_line];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    if (self.height<=0) {
        return;
    }
    //[self layoutIfNeeded];
    CGFloat height = self.height;
    [self.reduce_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(@0);
        make.size.mas_equalTo(CGSizeMake(height, height));
    }];
    [self.reduce_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(@.5);
        make.left.mas_equalTo(self.reduce_btn.mas_right).offset(0);
        make.size.mas_equalTo(CGSizeMake(.5, height-1));
    }];
    [self.add_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.mas_equalTo(@0);
        make.size.mas_equalTo(CGSizeMake(height, height));
    }];
    [self.add_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(@.5);
        make.right.mas_equalTo(self.add_btn.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(.5, height-1));
    }];
    [self.number_field mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(@0);
        make.left.mas_equalTo(self.reduce_btn.mas_right).offset(0.5);
        make.right.mas_equalTo(self.add_btn.mas_left).offset(0);
        make.height.mas_equalTo(@(height));
        make.bottom.mas_equalTo(self.mas_bottom).offset(0);
    }];
}

- (void)setCurrentValue:(NSInteger)currentValue {
    _currentValue = currentValue;
    [self setAppointValueForTextField:_currentValue notAddOrReduce:YES zeroShowTip:NO];
}

- (void)reduceClickEvent:(UIButton *)sender {
    if (_currentValue != 0) {
        _currentValue -- ;
    }
    
    if (_currentValue == 0) {
        self.noEvent =YES;
    }else{
        self.noEvent =NO;
    }

    [self setAppointValueForTextField:_currentValue notAddOrReduce:NO zeroShowTip:YES];
    //如果值小于等于最小值，则为最小值。切不能传递
    if (!self.noEvent) {
        self.reduceClick(_currentValue);
    }
}

- (void)addClickEvent:(UIButton *)sender {
    _currentValue ++ ;
    self.noEvent = NO;
    
    if (!self.noEvent) {
        if (_currentValue > self.maxValue) {
            _currentValue = self.maxValue;
            if (self.isOpenTenLimit) {
                if (self.maxValue < 5) {
                    [YYCommonTools showTipMessage:self.afterSaleTip?:[NSString stringWithFormat:@"商品库存仅剩%ld",_currentValue]];
                }
                else {
                    if (_currentValue >= 5) {
                        [YYCommonTools showTipMessage:self.afterSaleTip?:@"每单限购5件"];
                    }
                }
            }
            else{
                [YYCommonTools showTipMessage:[NSString stringWithFormat:@"商品库存仅剩%ld",_currentValue]];
            }
        }
        else{
                //如果小于最大值，但是开启了5的限制的话。
            if (self.isOpenTenLimit) {
                if (_currentValue >= 5) {
                    _currentValue = 5;
                    [YYCommonTools showTipMessage:self.afterSaleTip?:@"每单限购5件"];
                }
            }
        }
        self.addClick(_currentValue);
    }
    [self setAppointValueForTextField:_currentValue notAddOrReduce:NO zeroShowTip:YES];
}

- (void)setAppointValueForTextField:(NSInteger)value
                     notAddOrReduce:(BOOL)notAddOrReduce
                        zeroShowTip:(BOOL)zeroShowTip {
    value = [self operatorBtnSelectStatus:value
                           notAddOrReduce:notAddOrReduce
                              zeroShowTip:zeroShowTip];
    _currentValue = value;
    self.number_field.text = [NSString stringWithFormat:@"%ld",(long)value];
}

- (NSInteger)operatorBtnSelectStatus:(NSInteger)value
                      notAddOrReduce:(BOOL)notAddOrReduce
                         zeroShowTip:(BOOL)zeroShowTip {
    if (value <= self.minValue) {
        if (value == 0 && zeroShowTip) {
            [YYCommonTools showTipMessage:@"不能再少啦~"];
        }
        value = self.minValue;
        self.reduce_btn.selected = YES;
        self.add_btn.selected = NO;
    }
    if (value > self.minValue && value < self.maxValue) {
        self.reduce_btn.selected = NO;
        self.add_btn.selected = NO;
    }
    
    if (value >= self.maxValue||(value >= 5 && self.isOpenTenLimit)) {
        if (value >= self.maxValue) {
            if (!notAddOrReduce) {
                //是加或减
                value = self.maxValue;
            }
        }
        else {
            if (self.isOpenTenLimit && value >= 5) {
                value = 5;
            }
        }
        self.reduce_btn.selected = NO;
        self.add_btn.selected = YES;
    }
    if (value == self.maxValue && value == self.minValue) {
        self.reduce_btn.selected = YES;
        self.add_btn.selected = YES;
    }
    return value;
}

    //隐藏键盘
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    id view = [super hitTest:point withEvent:event];
    [self.number_field endEditing:YES];
    return view;
}

#pragma mark - UITextFieldDelegate
/*- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSInteger value;
    NSString *content = textField.text;
    value = [content integerValue];
    value = [self operatorBtnSelectStatus:value notAddOrReduce:NO];
    textField.text = [NSString stringWithFormat:@"%ld",(long)value];
    self.inputNumberBlock(value);
}*/
@end
