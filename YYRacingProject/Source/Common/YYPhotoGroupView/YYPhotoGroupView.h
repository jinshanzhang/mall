//
//  YYPhotoGroupView.h
//
//  Created by ibireme on 14/3/9.
//  Copyright (C) 2014 ibireme. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>

#import "YYImageInfoModel.h"
/// Single picture's info.
@interface YYPhotoGroupItem : NSObject
@property (nonatomic, strong) UIView  *thumbView; ///< thumb image, used for animation position calculation
@property (nonatomic, strong) UIImage *shellImage;
@property (nonatomic, assign) CGSize  largeImageSize;
@property (nonatomic, strong) NSURL   *largeImageURL;
@property (nonatomic, assign) BOOL    isAlreadyReplace;   //生成二维码图片是否已经替换。

@property (nonatomic, strong) YYImageInfoModel *imageModel;
@end

@interface YYPhotoManager : NSObject

//获取添加到相机胶卷的图片
- (PHObjectPlaceholder *)gainAssetsAddImageData:(id)newImageData;
//获取自定义相册
- (PHAssetCollection *)createCollection;

@end

/// Used to show a group of images.
/// One-shot.
@interface YYPhotoGroupView : UIView
//@property (nonatomic, strong) NSArray *groupItems; ///< Array<YYPhotoGroupItem>
// 修改为可变的
@property (nonatomic, strong) NSMutableArray *groupItems;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL blurEffectBackground; ///< Default is YES

@property (nonatomic, copy) void (^hideViewBlock)(NSInteger currentPage);
@property (nonatomic, copy) void (^skipGoodBlock)(YYImageInfoModel *infoModel);

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
- (instancetype)initWithFrame:(CGRect)frame UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;
- (instancetype)initWithGroupItems:(NSArray *)groupItems;
//新增
- (instancetype)initWithGroupItems:(NSArray *)groupItems currentPage:(NSInteger)currentPage;


- (void)presentFromImageView:(UIView *)fromView
                 toContainer:(UIView *)container
                    animated:(BOOL)animated
                  completion:(void (^)(void))completion;

- (void)dismissAnimated:(BOOL)animated completion:(void (^)(void))completion;
- (void)dismiss;
@end
