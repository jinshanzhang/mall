//
//  YYPhotoGroupView.m
//
//  Created by ibireme on 14/3/9.
//  Copyright (C) 2014 ibireme. All rights reserved.
//

#import "YYPhotoGroupView.h"
#import "YYKit.h"

#import "FriendCircleCommodityQRPhotoInfoRequestAPI.h"
#import "FriendCircleCommodityPosterQrInfoRequestAPI.h"

#define kPadding 20
#define kHiColor [UIColor colorWithRGBHex:0x2dd6b8]

@interface YYPhotoGroupItem()<NSCopying>
@property (nonatomic, readonly) UIImage *thumbImage;
@property (nonatomic, readonly) BOOL thumbClippedToTop;
- (BOOL)shouldClipToTop:(CGSize)imageSize forView:(UIView *)view;
@end
@implementation YYPhotoGroupItem

- (UIImage *)thumbImage {
    if ([_thumbView respondsToSelector:@selector(image)]) {
        return ((UIImageView *)_thumbView).image;
    }
    return nil;
}

- (BOOL)thumbClippedToTop {
    if (_thumbView) {
        if (_thumbView.layer.contentsRect.size.height < 1) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)shouldClipToTop:(CGSize)imageSize forView:(UIView *)view {
    if (imageSize.width < 1 || imageSize.height < 1) return NO;
    if (view.width < 1 || view.height < 1) return NO;
    return imageSize.height / imageSize.width > view.width / view.height;
}

- (id)copyWithZone:(NSZone *)zone {
    YYPhotoGroupItem *item = [self.class new];
    return item;
}
@end




@interface YYPhotoManager()

@end


@implementation YYPhotoManager

- (PHObjectPlaceholder *)gainAssetsAddImageData:(id)newImageData {
    NSError *error = nil;
    __block PHObjectPlaceholder *createdAsset = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
        
        if ([newImageData isKindOfClass:[NSURL class]]) {
            NSData *data = [NSData dataWithContentsOfURL:newImageData];
            UIImage *image = [UIImage imageWithData:data];
            createdAsset = [PHAssetChangeRequest creationRequestForAssetFromImage:image].placeholderForCreatedAsset;
        }
        else if ([newImageData isKindOfClass:[UIImage class]]) {
            UIImage *image = newImageData;
            createdAsset = [PHAssetChangeRequest creationRequestForAssetFromImage:image].placeholderForCreatedAsset;
        }
        // url 图片不能使用下面方法， 需要自己做一下转化。
        //createAssetId = [PHAssetCreationRequest creationRequestForAssetFromImageAtFileURL:newImageUrl].placeholderForCreatedAsset.localIdentifier;
    } error:&error];
    if (error) {
        YYLog(@"保存失败：%@", error);
        return nil;
    }
    return createdAsset;
}

- (PHAssetCollection *)createCollection {
    __block NSString *appName = @"蜀黍之家";
    // 先从已存在相册中找到自定义相册对象
    PHFetchResult<PHAssetCollection *> *collectionResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    for (PHAssetCollection *collection in collectionResult) {
        if ([collection.localizedTitle isEqualToString:appName]) {
            return collection;
        }
    }
    // 新建自定义相册
    __block NSString *collectionId = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
        collectionId = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:appName].placeholderForCreatedAssetCollection.localIdentifier;
    } error:nil];
    
    if (!kValidString(collectionId)) {
        [YYCommonTools showTipMessage:@"相册创建失败"];
        return nil;
    }
    //注意返回的是lastObject
    return [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[collectionId] options:nil].lastObject;
}

@end

@interface YYPhotoGroupCell : UIScrollView <UIScrollViewDelegate>
@property (nonatomic, strong) UIView *imageContainerView;
@property (nonatomic, strong) YYAnimatedImageView *imageView;
@property (nonatomic, assign) NSInteger page;

@property (nonatomic, assign) BOOL showProgress;
@property (nonatomic, assign) CGFloat progress;
@property (nonatomic, strong) CAShapeLayer *progressLayer;

@property (nonatomic, strong) YYPhotoGroupItem *item;
@property (nonatomic, readonly) BOOL itemDidLoad;

- (void)resizeSubviewSize;
@end

@implementation YYPhotoGroupCell

- (instancetype)init {
    self = super.init;
    if (!self) return nil;
    self.delegate = self;
    self.bouncesZoom = YES;
    self.maximumZoomScale = 3;
    self.multipleTouchEnabled = YES;
    self.alwaysBounceVertical = NO;
    self.showsVerticalScrollIndicator = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.frame = [UIScreen mainScreen].bounds;
    
    _imageContainerView = [UIView new];
    _imageContainerView.clipsToBounds = YES;
    [self addSubview:_imageContainerView];
    
    _imageView = [YYAnimatedImageView new];
    _imageView.clipsToBounds = YES;
    _imageView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:0.500];
    [_imageContainerView addSubview:_imageView];
    
    _progressLayer = [CAShapeLayer layer];
    _progressLayer.size = CGSizeMake(40, 40);
    _progressLayer.cornerRadius = 20;
    _progressLayer.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.500].CGColor;
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(_progressLayer.bounds, 7, 7) cornerRadius:(40 / 2 - 7)];
    _progressLayer.path = path.CGPath;
    _progressLayer.fillColor = [UIColor clearColor].CGColor;
    _progressLayer.strokeColor = [UIColor whiteColor].CGColor;
    _progressLayer.lineWidth = 4;
    _progressLayer.lineCap = kCALineCapRound;
    _progressLayer.strokeStart = 0;
    _progressLayer.strokeEnd = 0;
    _progressLayer.hidden = YES;
    //[self.layer addSublayer:_progressLayer];
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _progressLayer.center = CGPointMake(self.width / 2, self.height / 2);
}

- (void)setItem:(YYPhotoGroupItem *)item {
    //if (_item == item) return;
    _item = item;
    _itemDidLoad = NO;
    
    [self setZoomScale:1.0 animated:NO];
    self.maximumZoomScale = 1;
    
    [_imageView cancelCurrentImageRequest];
    [_imageView.layer removePreviousFadeAnimation];
    
    _progressLayer.hidden = NO;
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    _progressLayer.strokeEnd = 0;
    _progressLayer.hidden = YES;
    [CATransaction commit];
    
    if (!_item) {
        _imageView.image = nil;
        return;
    }
    
    @weakify(self);
    NSLog(@"==============================");
    [_imageView setImageWithURL:item.largeImageURL placeholder:(item.thumbImage==nil)?item.shellImage:item.thumbImage options:YYWebImageOptionProgressiveBlur|YYWebImageOptionSetImageWithFadeAnimation progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        @strongify(self);
        if (!self) return;
        [YYCenterLoading showCenterLoadingInWindow];
        CGFloat progress = receivedSize / (float)expectedSize;
        progress = progress < 0.01 ? 0.01 : progress > 1 ? 1 : progress;
        if (isnan(progress)) progress = 0;
        if (progress >= 1) {
            [YYCenterLoading hideCenterLoadingInWindow];
        }
        self.progressLayer.hidden = NO;
        self.progressLayer.strokeEnd = progress;
    } transform:nil completion:^(UIImage *image, NSURL *url, YYWebImageFromType from, YYWebImageStage stage, NSError *error) {
        @strongify(self);
        if (!self) return;
        [YYCenterLoading hideCenterLoadingInWindow];
        self.progressLayer.hidden = YES;
        if (stage == YYWebImageStageFinished) {
            self.maximumZoomScale = 3;
            if (image) {
                self->_itemDidLoad = YES;
                [self resizeSubviewSize];
                [self.imageView.layer addFadeAnimationWithDuration:0.1 curve:UIViewAnimationCurveLinear];
            }
        }
    }];
    [self resizeSubviewSize];
}

- (void)resizeSubviewSize {
    _imageContainerView.origin = CGPointZero;
    _imageContainerView.width = self.width;
    
    UIImage *image = _imageView.image;
    if (image.size.height / image.size.width > self.height / self.width) {
        _imageContainerView.height = floor(image.size.height / (image.size.width / self.width));
    } else {
        CGFloat height = image.size.height / image.size.width * self.width;
        if (height < 1 || isnan(height)) height = self.height;
        height = floor(height);
        _imageContainerView.height = height;
        _imageContainerView.centerY = self.height / 2;
    }
    if (_imageContainerView.height > self.height && _imageContainerView.height - self.height <= 1) {
        _imageContainerView.height = self.height;
    }
    self.contentSize = CGSizeMake(self.width, MAX(_imageContainerView.height, self.height));
    [self scrollRectToVisible:self.bounds animated:NO];
    
    if (_imageContainerView.height <= self.height) {
        self.alwaysBounceVertical = NO;
    } else {
        self.alwaysBounceVertical = YES;
    }
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    _imageView.frame = _imageContainerView.bounds;
    [CATransaction commit];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return _imageContainerView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    UIView *subView = _imageContainerView;
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

@end


@interface YYPhotoGroupView() <UIScrollViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, weak) UIView *fromView;
@property (nonatomic, weak) UIView *toContainerView;

@property (nonatomic, strong) UIImage *snapshotImage;
@property (nonatomic, strong) UIImage *snapshorImageHideFromView;

@property (nonatomic, strong) UIImageView *background;
@property (nonatomic, strong) UIImageView *blurBackground;

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *cells;
@property (nonatomic, strong) UIPageControl *pager;
@property (nonatomic, strong) UILabel       *pagerLabel;

@property (nonatomic, strong) UIButton     *savePhotos;
@property (nonatomic, strong) YYButton     *lookGood;
@property (nonatomic, strong) UIButton     *saveQRPhoto;

@property (nonatomic, assign) CGFloat pagerCurrentPage;
@property (nonatomic, assign) NSInteger maxPage;
@property (nonatomic, assign) BOOL fromNavigationBarHidden;

@property (nonatomic, assign) NSInteger fromItemIndex;
@property (nonatomic, assign) BOOL isPresented;

@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@property (nonatomic, assign) CGPoint panGestureBeginPoint;
@end

@implementation YYPhotoGroupView

- (instancetype)initWithGroupItems:(NSArray *)groupItems {
    self = [super init];
    if (groupItems.count == 0) return nil;
    _groupItems = [groupItems mutableCopy];
    _blurEffectBackground = YES;
    
    @weakify(self);
    NSString *model = [UIDevice currentDevice].machineModel;
    static NSMutableSet *oldDevices;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        oldDevices = [NSMutableSet new];
        [oldDevices addObject:@"iPod1,1"];
        [oldDevices addObject:@"iPod2,1"];
        [oldDevices addObject:@"iPod3,1"];
        [oldDevices addObject:@"iPod4,1"];
        [oldDevices addObject:@"iPod5,1"];
        
        [oldDevices addObject:@"iPhone1,1"];
        [oldDevices addObject:@"iPhone1,1"];
        [oldDevices addObject:@"iPhone1,2"];
        [oldDevices addObject:@"iPhone2,1"];
        [oldDevices addObject:@"iPhone3,1"];
        [oldDevices addObject:@"iPhone3,2"];
        [oldDevices addObject:@"iPhone3,3"];
        [oldDevices addObject:@"iPhone4,1"];
        
        [oldDevices addObject:@"iPad1,1"];
        [oldDevices addObject:@"iPad2,1"];
        [oldDevices addObject:@"iPad2,2"];
        [oldDevices addObject:@"iPad2,3"];
        [oldDevices addObject:@"iPad2,4"];
        [oldDevices addObject:@"iPad2,5"];
        [oldDevices addObject:@"iPad2,6"];
        [oldDevices addObject:@"iPad2,7"];
        [oldDevices addObject:@"iPad3,1"];
        [oldDevices addObject:@"iPad3,2"];
        [oldDevices addObject:@"iPad3,3"];
    });
    if ([oldDevices containsObject:model]) {
        _blurEffectBackground = NO;
    }
    
    self.backgroundColor = [UIColor clearColor];
    self.frame = [UIScreen mainScreen].bounds;
    self.clipsToBounds = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    tap2.delegate = self;
    tap2.numberOfTapsRequired = 2;
    [tap requireGestureRecognizerToFail: tap2];
    [self addGestureRecognizer:tap2];
    
    UILongPressGestureRecognizer *press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress)];
    press.delegate = self;
    [self addGestureRecognizer:press];
    
    if (kSystemVersion >= 7) {
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
        [self addGestureRecognizer:pan];
        _panGesture = pan;
    }
    
    
    _cells = @[].mutableCopy;
    
    _background = UIImageView.new;
    _background.frame = self.bounds;
    _background.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    _blurBackground = UIImageView.new;
    _blurBackground.frame = self.bounds;
    _blurBackground.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    _contentView = UIView.new;
    _contentView.frame = self.bounds;
    _contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    _scrollView = UIScrollView.new;
    _scrollView.frame = CGRectMake(-kPadding / 2, 0, self.width + kPadding, self.height);
    _scrollView.delegate = self;
    _scrollView.scrollsToTop = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.alwaysBounceHorizontal = groupItems.count > 1;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _scrollView.delaysContentTouches = NO;
    _scrollView.canCancelContentTouches = YES;
    
    _pager = [[UIPageControl alloc] init];
    _pager.hidesForSinglePage = YES;
    _pager.userInteractionEnabled = NO;
    _pager.width = self.width - 36;
    _pager.height = 10;
    _pager.center = CGPointMake(self.width / 2, self.height - 40);
    _pager.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    self.pagerLabel = [[UILabel alloc] init];
    self.pagerLabel.width = 62;
    self.pagerLabel.height = 20;
    self.pagerLabel.layer.cornerRadius = 10;
    self.pagerLabel.clipsToBounds = YES;
    self.pagerLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    self.pagerLabel.textAlignment = NSTextAlignmentCenter;
    self.pagerLabel.font = [UIFont systemFontOfSize:12];
    self.pagerLabel.textColor = [UIColor whiteColor];
    self.pagerLabel.center = CGPointMake(self.width / 2, kBottom(50));
    
    
    self.savePhotos = [UIButton buttonWithType:UIButtonTypeCustom];
    self.savePhotos.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.savePhotos setTitle:@"保存图片" forState:UIControlStateNormal];
    self.savePhotos.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    self.savePhotos.layer.cornerRadius = 4;
    [self.savePhotos addTarget:self action:@selector(savePhotosClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.savePhotos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.savePhotos.width = kSizeScale(80);
    self.savePhotos.height = kSizeScale(30);
    self.savePhotos.center = CGPointMake(self.width - kSizeScale(50), self.height - kSizeScale(40));
    self.savePhotos.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    
    self.saveQRPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    self.saveQRPhoto.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.saveQRPhoto setTitle:@"保存二维码图片" forState:UIControlStateNormal];
    self.saveQRPhoto.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    self.saveQRPhoto.layer.cornerRadius = 3;
    [self.saveQRPhoto addTarget:self action:@selector(savePhotosClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.saveQRPhoto setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.saveQRPhoto.width = kSizeScale(100);
    self.saveQRPhoto.height = kSizeScale(30);
    self.saveQRPhoto.hidden = YES;
    self.saveQRPhoto.center = CGPointMake(self.width - kSizeScale(105), self.height - kBottom(kSizeScale(70)));
    self.saveQRPhoto.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    self.lookGood = [[YYButton alloc] initWithFrame:CGRectZero];
    self.lookGood.space = 0;
    self.lookGood.layer.cornerRadius = 3;
    self.lookGood.titleTextColor = XHWhiteColor;
    self.lookGood.titleFont = [UIFont systemFontOfSize:12];
    self.lookGood.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
    self.lookGood.btnClickBlock = ^(BOOL status) {
        @strongify(self);
        [self performSelector:@selector(lookGoodClickEvent:) withObject:nil];
    };

    self.lookGood.width = kSizeScale(100);
    self.lookGood.height = kSizeScale(30);
    self.lookGood.hidden = YES;
    self.lookGood.center = CGPointMake(kSizeScale(105), self.height - kBottom(kSizeScale(70)));
    self.lookGood.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    [self addSubview:_background];
    [self addSubview:_blurBackground];
    [self addSubview:_contentView];
    [_contentView addSubview:_scrollView];
    //[_contentView addSubview:_pager];
    [_contentView addSubview:self.pagerLabel];
    [_contentView addSubview:self.savePhotos];
    [_contentView addSubview:self.saveQRPhoto];
    [_contentView addSubview:self.lookGood];
    
    return self;
}

- (instancetype)initWithGroupItems:(NSArray *)groupItems currentPage:(NSInteger)currentPage
{
    if (self = [self initWithGroupItems:groupItems]) {
        _currentPage = currentPage;
        _maxPage = groupItems.count;
        if (_maxPage <= 1) {
            self.pagerLabel.hidden = YES;
        }
        self.pagerLabel.text = [NSString stringWithFormat:@"%ld/%ld",_currentPage+1,_maxPage];
        
        _pager.numberOfPages = groupItems.count;
        _pager.currentPage = _currentPage;
    }
    return self;
}


- (void)presentFromImageView:(UIView *)fromView
                 toContainer:(UIView *)toContainer
                    animated:(BOOL)animated
                  completion:(void (^)(void))completion {
    if (!toContainer) return;
    
    @weakify(self);
    _fromView = fromView;
    _toContainerView = toContainer;
    
    /*  由于cell为nil，导致下面的写法被废弃
    NSInteger page = -1;
    for (NSUInteger i = 0; i < self.groupItems.count; i++) {
        if (fromView == ((YYPhotoGroupItem *)self.groupItems[i]).thumbView) {
            page = (int)i;
            break;
        }
    }
    if (page == -1) page = 0;
    _fromItemIndex = page;*/
    
    _snapshotImage = [_toContainerView snapshotImageAfterScreenUpdates:NO];
    BOOL fromViewHidden = fromView.hidden;
    fromView.hidden = YES;
    _snapshorImageHideFromView = [_toContainerView snapshotImage];
    fromView.hidden = fromViewHidden;
    
    _background.image = _snapshorImageHideFromView;
    if (_blurEffectBackground) {
        _blurBackground.image = [_snapshorImageHideFromView imageByBlurDark]; //Same to UIBlurEffectStyleDark
    } else {
        _blurBackground.image = [UIImage imageWithColor:[UIColor blackColor]];
    }
    
    self.size = _toContainerView.size;
    self.blurBackground.alpha = 0;
    self.pager.alpha = 0;
    //self.pager.numberOfPages = self.groupItems.count;
    //self.pager.currentPage = page;
    [_toContainerView addSubview:self];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.width * self.groupItems.count, _scrollView.height);
    [_scrollView scrollRectToVisible:CGRectMake(_scrollView.width * _pager.currentPage, 0, _scrollView.width, _scrollView.height) animated:NO];
    //[self scrollViewDidScroll:_scrollView];
    
    [UIView setAnimationsEnabled:YES];
    _fromNavigationBarHidden = [UIApplication sharedApplication].statusBarHidden;
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:animated ? UIStatusBarAnimationFade : UIStatusBarAnimationNone];
    
    YYPhotoGroupCell *cell = [self cellForPage:self.currentPage];
    YYPhotoGroupItem *item = _groupItems[self.currentPage];
    
    if (!item.thumbClippedToTop) {
        NSString *imageKey = [[YYWebImageManager sharedManager] cacheKeyForURL:item.largeImageURL];
        if ([[YYWebImageManager sharedManager].cache getImageForKey:imageKey withType:YYImageCacheTypeMemory]) {
            cell.item = item;
        }
    }

    [self controlOperatorBtn:item];
    
    if (!cell.item) {
        cell.imageView.image = (item.thumbImage == nil)?item.shellImage:item.thumbImage;
        [cell resizeSubviewSize];
        if (item.imageModel.type > 1) {
            //邀约海报图
            [self commodifyPosterQrPhoto:item qrBlock:^(NSURL *url) {
                @strongify(self);
                YYPhotoGroupItem *tempItem = item;
                tempItem.isAlreadyReplace = YES;
                tempItem.largeImageURL = url;
                [self.groupItems replaceObjectAtIndex:self.currentPage withObject:tempItem];
                cell.item = tempItem;
                [cell resizeSubviewSize];
            }];
        }
    }
    
    if (item.thumbClippedToTop) {
        CGRect fromFrame = [_fromView convertRect:_fromView.bounds toView:cell];
        CGRect originFrame = cell.imageContainerView.frame;
        CGFloat scale = fromFrame.size.width / cell.imageContainerView.width;
        
        cell.imageContainerView.centerX = CGRectGetMidX(fromFrame);
        cell.imageContainerView.height = fromFrame.size.height / scale;
        cell.imageContainerView.layer.transformScale = scale;
        cell.imageContainerView.centerY = CGRectGetMidY(fromFrame);
        
        float oneTime = animated ? 0.25 : 0;
        [UIView animateWithDuration:oneTime delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut animations:^{
            self.blurBackground.alpha = 1;
        }completion:NULL];
        
        _scrollView.userInteractionEnabled = NO;
        [UIView animateWithDuration:oneTime delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            cell.imageContainerView.layer.transformScale = 1;
            cell.imageContainerView.frame = originFrame;
            self.pager.alpha = 1;
        }completion:^(BOOL finished) {
            self.isPresented = YES;
            [self scrollViewDidScroll:self.scrollView];
            self.scrollView.userInteractionEnabled = YES;
            [self hidePager];
            if (completion) completion();
        }];
        
    } else {
        CGRect fromFrame = [_fromView convertRect:_fromView.bounds toView:cell.imageContainerView];
        
        cell.imageContainerView.clipsToBounds = NO;
        cell.imageView.frame = fromFrame;
        cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        float oneTime = animated ? 0.18 : 0;
        [UIView animateWithDuration:oneTime*2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut animations:^{
            self.blurBackground.alpha = 1;
        }completion:NULL];
        
        _scrollView.userInteractionEnabled = NO;
        [UIView animateWithDuration:oneTime delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut animations:^{
            cell.imageView.frame = cell.imageContainerView.bounds;
            cell.imageView.layer.transformScale = 1.01;
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:oneTime delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut animations:^{
                cell.imageView.layer.transformScale = 1.0;
                self.pager.alpha = 1;
            }completion:^(BOOL finished) {
                cell.imageContainerView.clipsToBounds = YES;
                self.isPresented = YES;
                [self scrollViewDidScroll:self.scrollView];
                self.scrollView.userInteractionEnabled = YES;
                [self hidePager];
                if (completion) completion();
            }];
        }];
    }
}

- (void)controlOperatorBtn:(YYPhotoGroupItem *)item {
    //新增按钮操作
    if (!item.imageModel) {
        return;
    }
    if (item.imageModel) {
        NSInteger type = item.imageModel.type;
        if (type == 0) {
            self.saveQRPhoto.hidden = YES;
            self.lookGood.hidden = YES;
            self.savePhotos.hidden = NO;
            [self.savePhotos setTitle:@"保存图片" forState:UIControlStateNormal];
        }
        else if (type == 1) {
            self.saveQRPhoto.hidden = NO;
            self.lookGood.hidden = NO;
            self.savePhotos.hidden = YES;
        
            NSMutableAttributedString *lookGoodTitle = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" ¥%@ ",item.imageModel.price]];
            
            NSMutableAttributedString *lineTitle = [[NSMutableAttributedString alloc] initWithString:@"| "];
            [lineTitle yy_setAttributes:HexRGB(0x424242)
                                   font:normalFont(12)
                                content:lineTitle
                              alignment:NSTextAlignmentCenter];
            [lookGoodTitle appendAttributedString:lineTitle];
            
            NSMutableAttributedString *fixedTitle = [[NSMutableAttributedString alloc] initWithString:@"查看商品"];
            [fixedTitle yy_setAttributes:XHWhiteColor
                                   font:normalFont(12)
                                content:fixedTitle
                               alignment:NSTextAlignmentCenter];
            [lookGoodTitle appendAttributedString:fixedTitle];
            
            CGFloat textWidth = [YYCommonTools sizeWithText:lookGoodTitle
                                                                              font:normalFont(14)
                                                                           maxSize:CGSizeMake(MAXFLOAT, kSizeScale(30))].width;
            CGRect frame = self.lookGood.frame;
            frame.size.width = textWidth + 15;
            self.lookGood.frame = frame;
            
            self.lookGood.normalAttibuteTitle = lookGoodTitle;
        }
        else {
            self.saveQRPhoto.hidden = YES;
            self.lookGood.hidden = YES;
            self.savePhotos.hidden = NO;
            
            self.savePhotos.frame = CGRectMake(self.width - kSizeScale(150), self.height - kBottom(kSizeScale(70)), kSizeScale(130), kSizeScale(30));
            [self.savePhotos setTitle:@"保存二维码海报图" forState:UIControlStateNormal];
        }
    }
}

- (void)dismissAnimated:(BOOL)animated completion:(void (^)(void))completion {
    [UIView setAnimationsEnabled:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:_fromNavigationBarHidden withAnimation:animated ? UIStatusBarAnimationFade : UIStatusBarAnimationNone];
    NSInteger currentPage = self.currentPage;
    self.hideViewBlock(self.currentPage);
    
    YYPhotoGroupCell *cell = [self cellForPage:currentPage];
    YYPhotoGroupItem *item = _groupItems[currentPage];
    
    UIView *fromView = nil;
    // 蜀黍之家圈修改这里
    /*if (_fromItemIndex == currentPage) {
        fromView = _fromView;
    } else {
        fromView = item.thumbView;
    }*/
    fromView = item.thumbView;
    
    [self cancelAllImageLoad];
    _isPresented = NO;
    BOOL isFromImageClipped = fromView.layer.contentsRect.size.height < 1;
    
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    if (isFromImageClipped) {
        CGRect frame = cell.imageContainerView.frame;
        cell.imageContainerView.layer.anchorPoint = CGPointMake(0.5, 0);
        cell.imageContainerView.frame = frame;
    }
    cell.progressLayer.hidden = YES;
    [CATransaction commit];
    

    if (fromView == nil) {
        self.background.image = _snapshotImage;
        [UIView animateWithDuration:animated ? 0.25 : 0 delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseOut animations:^{
            self.alpha = 0.0;
            self.scrollView.layer.transformScale = 0.95;
            self.scrollView.alpha = 0;
            self.pager.alpha = 0;
            self.blurBackground.alpha = 0;
        }completion:^(BOOL finished) {
            self.scrollView.layer.transformScale = 1;
            [self removeFromSuperview];
            [self cancelAllImageLoad];
            if (completion) completion();
        }];
        return;
    }
    
    if (_fromItemIndex != currentPage) {
        _background.image = _snapshotImage;
        [_background.layer addFadeAnimationWithDuration:0.25 curve:UIViewAnimationCurveEaseOut];
    } else {
        _background.image = _snapshorImageHideFromView;
    }

    if (isFromImageClipped) {
        [cell scrollToTopAnimated:NO];
    }
    
    [UIView animateWithDuration:animated ? 0.2 : 0 delay:0 options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseOut animations:^{
        self.pager.alpha = 0.0;
        self.blurBackground.alpha = 0.0;
        //新增, 页面消失前隐藏按钮。
        self.savePhotos.hidden = YES;
        self.saveQRPhoto.hidden = YES;
        self.lookGood.hidden = YES;
        
        if (isFromImageClipped) {
            
            CGRect fromFrame = [fromView convertRect:fromView.bounds toView:cell];
            CGFloat scale = fromFrame.size.width / cell.imageContainerView.width * cell.zoomScale;
            CGFloat height = fromFrame.size.height / fromFrame.size.width * cell.imageContainerView.width;
            if (isnan(height)) height = cell.imageContainerView.height;
            
            cell.imageContainerView.height = height;
            cell.imageContainerView.center = CGPointMake(CGRectGetMidX(fromFrame), CGRectGetMinY(fromFrame));
            cell.imageContainerView.layer.transformScale = scale;
            
        } else {
            CGRect fromFrame = [fromView convertRect:fromView.bounds toView:cell.imageContainerView];
            cell.imageContainerView.clipsToBounds = NO;
            cell.imageView.contentMode = fromView.contentMode;
            cell.imageView.frame = fromFrame;
        }
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:animated ? 0.15 : 0 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            cell.imageContainerView.layer.anchorPoint = CGPointMake(0.5, 0.5);
            [self removeFromSuperview];
            if (completion) completion();
        }];
    }];
}

- (void)dismiss {
    [self dismissAnimated:YES completion:nil];
}

#pragma mark - Request
/**实时生成二维码**/
- (void)commodifyQRCodePhoto:(YYPhotoGroupItem *)item
                     qrBlock:(void(^)(NSURL*))qrBlock {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSMutableArray *content = [NSMutableArray array];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(item.imageModel.imageId) forKey:@"imageId"];
    [dict setObject:item.imageModel.shellUri forKey:@"shellUri"];
    [content addObject:dict];
    [params setObject:content forKey:@"items"];
    [YYQRCodeCenterLoading showCenterLoading:@"二维码生成中..."
                                   superView:kAppWindow];
    FriendCircleCommodityQRPhotoInfoRequestAPI *qrAPI = [[FriendCircleCommodityQRPhotoInfoRequestAPI alloc] initFriendCircleCommodityQRPhotosRequest:params];
    [qrAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYQRCodeCenterLoading hideCenterLoading:nil
                                       superView:nil];
        NSDictionary *responObject = request.responseJSONObject;
        if (kValidDictionary(responObject)) {
            NSArray *qrArrs = [responObject objectForKey:@"qr"];
            qrBlock([NSURL URLWithString:[qrArrs firstObject]]);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**邀约海报图生成**/
- (void)commodifyPosterQrPhoto:(YYPhotoGroupItem *)item
                       qrBlock:(void(^)(NSURL*))qrBlock {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSMutableArray *content = [NSMutableArray array];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(item.imageModel.imageId) forKey:@"imageId"];
    [content addObject:dict];
    [params setObject:content forKey:@"items"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [YYCenterLoading hideCenterLoadingInWindow];
        [YYCenterLoading showCenterLoadingInWindow];
        FriendCircleCommodityPosterQrInfoRequestAPI *qrAPI = [[FriendCircleCommodityPosterQrInfoRequestAPI alloc] initFriendCircleCommodityPosterQrRequest:params];
        [qrAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [YYCenterLoading hideCenterLoadingInWindow];
            NSDictionary *responObject = request.responseJSONObject;
            if (kValidDictionary(responObject)) {
                NSArray *qrArrs = [responObject objectForKey:@"qr"];
                qrBlock([NSURL URLWithString:[qrArrs firstObject]]);
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    });
}

#pragma mark - 相册处理
- (void)savePhotosClickEvent:(UIButton *)sender {
    PHAuthorizationStatus oldStatus = [PHPhotoLibrary authorizationStatus];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (status) {
                case PHAuthorizationStatusAuthorized: {
                    [self saveImageIntoAlbum:self.savePhotos!=sender];
                    break;
                }
                case PHAuthorizationStatusDenied: {
                    if (oldStatus == PHAuthorizationStatusNotDetermined) return;
                    [YYCommonTools showTipMessage:@"请打开访问相册权限"];
                    break;
                }
                case PHAuthorizationStatusRestricted: {
                    [YYCommonTools showTipMessage:@"系统原因，不能访问相册"];
                    break;
                }
                default:
                    break;
            }
        });
    }];
}

- (void)lookGoodClickEvent:(UIButton *)sender {
    YYPhotoGroupItem *item = self.groupItems[self.currentPage];
    if (self.skipGoodBlock) {
        self.skipGoodBlock(item.imageModel);
    }
    [self dismiss];
}

- (BOOL)judgePhotosPermission {
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied) {
        return NO;
    }
    return YES;
}

- (void)saveImageIntoAlbum:(BOOL)isCommodifyQRPhoto {
    if (![self judgePhotosPermission]) {
        UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"由于系统原因, 无法访问相册" preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertC addAction:alertA];
        [[YYCommonTools getCurrentVC] presentViewController:alertC animated:YES completion:nil];
        return;
    }
    YYPhotoGroupItem *item = self.groupItems[self.currentPage];
    YYPhotoManager *manager = [[YYPhotoManager alloc] init];

    __block PHObjectPlaceholder *createdAsset = nil;
    if (isCommodifyQRPhoto) {
        [self commodifyQRCodePhoto:item
                           qrBlock:^(NSURL *url) {
                               createdAsset = [manager gainAssetsAddImageData:url];
                               PHAssetCollection *createdCollection = [manager createCollection];
                               
                               NSError *error = nil;
                               if (createdCollection == nil || createdAsset == nil) {
                                   [YYCommonTools showTipMessage:@"保存失败"];
                                   return;
                               }
                               [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
                                   [[PHAssetCollectionChangeRequest changeRequestForAssetCollection:createdCollection] insertAssets:@[createdAsset] atIndexes:[NSIndexSet indexSetWithIndex:0]];
                               } error:&error];
                               
                               if (error) {
                                   [YYCommonTools showTipMessage:@"保存失败"];
                               } else {
                                   [YYCommonTools showTipMessage:@"保存成功"];
                               }
                           }];
    }
    else {
        if (item.largeImageURL) {
            createdAsset = [manager gainAssetsAddImageData:item.largeImageURL];
        }
        else if (item.shellImage) {
            createdAsset = [manager gainAssetsAddImageData:item.shellImage];
        }
        PHAssetCollection *createdCollection = [manager createCollection];
        
        NSError *error = nil;
        if (createdCollection == nil || createdAsset == nil) {
            [YYCommonTools showTipMessage:@"保存失败"];
            return;
        }
        [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
            [[PHAssetCollectionChangeRequest changeRequestForAssetCollection:createdCollection] insertAssets:@[createdAsset] atIndexes:[NSIndexSet indexSetWithIndex:0]];
        } error:&error];
        
        if (error) {
            [YYCommonTools showTipMessage:@"保存失败"];
        } else {
            [YYCommonTools showTipMessage:@"保存成功"];
        }
    }
}

- (void)cancelAllImageLoad {
    [_cells enumerateObjectsUsingBlock:^(YYPhotoGroupCell *cell, NSUInteger idx, BOOL *stop) {
        [cell.imageView cancelCurrentImageRequest];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self updateCellsForReuse];
    
    //@weakify(self);
    CGFloat floatPage = _scrollView.contentOffset.x / _scrollView.width;
    NSInteger page = _scrollView.contentOffset.x / _scrollView.width + 0.5;
    
    for (NSInteger i = page - 1; i <= page + 1; i++) { // preload left and right cell
        if (i >= 0 && i < self.groupItems.count) {
            YYPhotoGroupCell *cell = [self cellForPage:i];
            if (!cell) {
                YYPhotoGroupCell *cell = [self dequeueReusableCell];
                cell.page = i;
                cell.left = (self.width + kPadding) * i + kPadding / 2;
                
                if (_isPresented) {
                    cell.item = self.groupItems[i];
                }
                [self.scrollView addSubview:cell];
            } else {
                if (_isPresented && !cell.item) {
                    cell.item = self.groupItems[i];
                }
            }
        }
    }
    
    NSInteger intPage = floatPage + 0.5;
    intPage = intPage < 0 ? 0 : intPage >= _groupItems.count ? (int)_groupItems.count - 1 : intPage;
    _pager.currentPage = intPage;
    
    YYPhotoGroupCell *cell = [self cellForPage:intPage];
    YYPhotoGroupItem *item = _groupItems[intPage];
    cell.item = item;
    /*if (item.imageModel.type > 1) {
        if (item.isAlreadyReplace) {
            cell.item = item;
            [cell resizeSubviewSize];
        }
        else {
            [self commodifyPosterQrPhoto:item qrBlock:^(NSURL *url) {
                @strongify(self);
                NSLog(@"======%ld ==== %ld", item.imageModel.type, item.imageModel.imageId);
                YYPhotoGroupItem *tempItem = item;
                tempItem.isAlreadyReplace = YES;
                tempItem.largeImageURL = url;
                [self.groupItems replaceObjectAtIndex:self.currentPage withObject:tempItem];
                cell.item = tempItem;
                [cell resizeSubviewSize];
            }];
        }
    }*/
    [self controlOperatorBtn:item];
    self.pagerLabel.text = [NSString stringWithFormat:@"%ld/%ld",intPage+1,_maxPage];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseInOut animations:^{
        self.pager.alpha = 1;
    }completion:^(BOOL finish) {
        
    }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self hidePager];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self hidePager];
}


- (void)hidePager {
    @weakify(self);
    YYPhotoGroupCell *cell = [self cellForPage:self.currentPage];
    YYPhotoGroupItem *item = _groupItems[self.currentPage];
    if (item.imageModel.type > 1) {
        if (item.isAlreadyReplace) {
            cell.item = item;
            [cell resizeSubviewSize];
        }
        else {
            [self commodifyPosterQrPhoto:item qrBlock:^(NSURL *url) {
                @strongify(self);
                YYPhotoGroupItem *tempItem = item;
                tempItem.isAlreadyReplace = YES;
                tempItem.largeImageURL = url;
                [self.groupItems replaceObjectAtIndex:self.currentPage withObject:tempItem];
                cell.item = tempItem;
                [cell resizeSubviewSize];
            }];
        }
    }
    [UIView animateWithDuration:0.3 delay:0.8 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut animations:^{
        self.pager.alpha = 0;
    }completion:^(BOOL finish) {
       
    }];
}

/// enqueue invisible cells for reuse
- (void)updateCellsForReuse {
    for (YYPhotoGroupCell *cell in _cells) {
        if (cell.superview) {
            if (cell.left > _scrollView.contentOffset.x + _scrollView.width * 2||
                cell.right < _scrollView.contentOffset.x - _scrollView.width) {
                [cell removeFromSuperview];
                cell.page = -1;
                cell.item = nil;
            }
        }
    }
}

/// dequeue a reusable cell
- (YYPhotoGroupCell *)dequeueReusableCell {
    YYPhotoGroupCell *cell = nil;
    for (cell in _cells) {
        if (!cell.superview) {
            return cell;
        }
    }
    
    cell = [YYPhotoGroupCell new];
    cell.frame = self.bounds;
    cell.imageContainerView.frame = self.bounds;
    cell.imageView.frame = cell.bounds;
    cell.page = -1;
    cell.item = nil;
    [_cells addObject:cell];
    return cell;
}

/// get the cell for specified page, nil if the cell is invisible
- (YYPhotoGroupCell *)cellForPage:(NSInteger)page {
    for (YYPhotoGroupCell *cell in _cells) {
        if (cell.page == page) {
            return cell;
        }
    }
    return nil;
}

- (NSInteger)currentPage {
    NSInteger page = _scrollView.contentOffset.x / _scrollView.width + 0.5;
    if (page >= _groupItems.count) page = (NSInteger)_groupItems.count - 1;
    if (page < 0) page = 0;
    return page;
}

- (void)showHUD:(NSString *)msg {
    if (!msg.length) return;
    UIFont *font = [UIFont systemFontOfSize:17];
    CGSize size = [msg sizeForFont:font size:CGSizeMake(200, 200) mode:NSLineBreakByCharWrapping];
    UILabel *label = [UILabel new];
    label.size = CGSizePixelCeil(size);
    label.font = font;
    label.text = msg;
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    
    UIView *hud = [UIView new];
    hud.size = CGSizeMake(label.width + 20, label.height + 20);
    hud.backgroundColor = [UIColor colorWithWhite:0.000 alpha:0.650];
    hud.clipsToBounds = YES;
    hud.layer.cornerRadius = 8;
    
    label.center = CGPointMake(hud.width / 2, hud.height / 2);
    [hud addSubview:label];
    
    hud.center = CGPointMake(self.width / 2, self.height / 2);
    hud.alpha = 0;
    [self addSubview:hud];
    
    [UIView animateWithDuration:0.4 animations:^{
        hud.alpha = 1;
    }];
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView animateWithDuration:0.4 animations:^{
            hud.alpha = 0;
        } completion:^(BOOL finished) {
            [hud removeFromSuperview];
        }];
    });
}

- (void)doubleTap:(UITapGestureRecognizer *)g {
    if (!_isPresented) return;
    YYPhotoGroupCell *tile = [self cellForPage:self.currentPage];
    if (tile) {
        if (tile.zoomScale > 1) {
            [tile setZoomScale:1 animated:YES];
        } else {
            CGPoint touchPoint = [g locationInView:tile.imageView];
            CGFloat newZoomScale = tile.maximumZoomScale;
            CGFloat xsize = self.width / newZoomScale;
            CGFloat ysize = self.height / newZoomScale;
            [tile zoomToRect:CGRectMake(touchPoint.x - xsize/2, touchPoint.y - ysize/2, xsize, ysize) animated:YES];
        }
    }
}

- (void)longPress {
    if (!_isPresented) return;
    
    YYPhotoGroupCell *tile = [self cellForPage:self.currentPage];
    if (!tile.imageView.image) return;
    
    // try to save original image data if the image contains multi-frame (such as GIF/APNG)
    id imageItem = [tile.imageView.image imageDataRepresentation];
    YYImageType type = YYImageDetectType((__bridge CFDataRef)(imageItem));
    if (type != YYImageTypePNG &&
        type != YYImageTypeJPEG &&
        type != YYImageTypeGIF) {
        imageItem = tile.imageView.image;
    }
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[imageItem] applicationActivities:nil];
    if ([activityViewController respondsToSelector:@selector(popoverPresentationController)]) {
        activityViewController.popoverPresentationController.sourceView = self;
    }

    UIViewController *toVC = self.toContainerView.viewController;
    if (!toVC) toVC = self.viewController;
    [toVC presentViewController:activityViewController animated:YES completion:nil];
}

- (void)pan:(UIPanGestureRecognizer *)g {
    switch (g.state) {
        case UIGestureRecognizerStateBegan: {
            if (_isPresented) {
                _panGestureBeginPoint = [g locationInView:self];
            } else {
                _panGestureBeginPoint = CGPointZero;
            }
        } break;
        case UIGestureRecognizerStateChanged: {
            if (_panGestureBeginPoint.x == 0 && _panGestureBeginPoint.y == 0) return;
            CGPoint p = [g locationInView:self];
            CGFloat deltaY = p.y - _panGestureBeginPoint.y;
            _scrollView.top = deltaY;
            
            CGFloat alphaDelta = 160;
            CGFloat alpha = (alphaDelta - fabs(deltaY) + 50) / alphaDelta;
            alpha = YY_CLAMP(alpha, 0, 1);
            [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveLinear animations:^{
                self.blurBackground.alpha = alpha;
                self.pager.alpha = alpha;
            } completion:nil];
            
        } break;
        case UIGestureRecognizerStateEnded: {
            if (_panGestureBeginPoint.x == 0 && _panGestureBeginPoint.y == 0) return;
            CGPoint v = [g velocityInView:self];
            CGPoint p = [g locationInView:self];
            CGFloat deltaY = p.y - _panGestureBeginPoint.y;
            
            if (fabs(v.y) > 1000 || fabs(deltaY) > 120) {
                [self cancelAllImageLoad];
                _isPresented = NO;
                [[UIApplication sharedApplication] setStatusBarHidden:_fromNavigationBarHidden withAnimation:UIStatusBarAnimationFade];
                
                BOOL moveToTop = (v.y < - 50 || (v.y < 50 && deltaY < 0));
                CGFloat vy = fabs(v.y);
                if (vy < 1) vy = 1;
                CGFloat duration = (moveToTop ? _scrollView.bottom : self.height - _scrollView.top) / vy;
                duration *= 0.8;
                duration = YY_CLAMP(duration, 0.05, 0.3);
                
                [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState animations:^{
                    self.blurBackground.alpha = 0;
                    self.pager.alpha = 0;
                    if (moveToTop) {
                        self.scrollView.bottom = 0;
                    } else {
                        self.scrollView.top = self.height;
                    }
                } completion:^(BOOL finished) {
                    [self removeFromSuperview];
                }];
                
                _background.image = _snapshotImage;
                [_background.layer addFadeAnimationWithDuration:0.3 curve:UIViewAnimationCurveEaseInOut];
                
            } else {
                [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.9 initialSpringVelocity:v.y / 1000 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
                    self.scrollView.top = 0;
                    self.blurBackground.alpha = 1;
                    self.pager.alpha = 1;
                } completion:^(BOOL finished) {
                    
                }];
            }
            
        } break;
        case UIGestureRecognizerStateCancelled : {
            _scrollView.top = 0;
            _blurBackground.alpha = 1;
        }
        default:break;
    }
}

@end
