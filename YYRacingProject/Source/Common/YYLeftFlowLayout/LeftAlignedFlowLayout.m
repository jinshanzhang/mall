//
//  LeftAlignedFlowLayout.m
//  LeftAlignedCollectionView
//
//  Created by zhoufei on 16/5/12.
//  Copyright © 2016年 zhoufei. All rights reserved.
//

#import "LeftAlignedFlowLayout.h"
#import "Attribute.h"
#import "LayoutAttributeTools.h"


@interface LeftAlignedFlowLayout()
@property (assign,nonatomic)CGFloat leftMargin;//一行   距离左侧的距离
@property (assign,nonatomic)CGFloat itemMargin;//一行中 选项之间的距离
@end

@implementation LeftAlignedFlowLayout

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSInteger itemCount = [[self collectionView] numberOfSections];
    NSMutableArray* attributes = [[super layoutAttributesForElementsInRect:rect] mutableCopy];
    NSMutableArray * subArray = [LayoutAttributeTools groupTheSameLineItems:attributes];
    [self leftAlign_updateItemAttributeInSigleLine:subArray totalSectionCount:itemCount];
    return attributes;
}

/*!
 *  @author zhoufei
 *
 *  @brief 更新每个元素的位置
 *  @param groupArray 归并后的结果数组
 */
- (void)leftAlign_updateItemAttributeInSigleLine:(NSMutableArray * )groupArray
                               totalSectionCount:(NSInteger)sectionCount {

    NSMutableArray * modelArray = [NSMutableArray array];

    for (NSArray * array  in groupArray) {
        NSInteger count = array.count;
        if (!count) {
            continue;
        }
        for (int i = 0; i<count; i++) {
            UICollectionViewLayoutAttributes *attrOne = array[i];
            [modelArray addObject:[Attribute AttributeWithIndex:i width:attrOne.size.width]];
        }
        for (int i = 0; i<count; i++) {
            [self handlerCollectionCellLayout:i
                                     sections:sectionCount
                                   attributes:array
                              attributeModels:modelArray];
        }
        [modelArray removeAllObjects];
    }
}

- (void)handlerCollectionCellLayout:(int)index
                           sections:(NSInteger)sections
                         attributes:(NSArray *)attributes
                    attributeModels:(NSArray *)attributeModels {
    CGFloat leftWith = 0;
    BOOL    isHandlerFrame = NO;  //是否处理frame布局
    NSInteger currentSection = -1;  //当前section
    self.leftMargin = self.itemMargin = self.minimumLineSpacing;
    UICollectionViewLayoutAttributes *attr = [attributes objectAtIndex:index];
    currentSection = attr.indexPath.section;
    if (sections < 2) {
        isHandlerFrame = YES;
    }
    else {
        isHandlerFrame = ((currentSection != 0) ? YES : NO);
    }
    if (isHandlerFrame) {
        NSPredicate *predice =[NSPredicate predicateWithFormat:@"index < %d",index];
        NSArray * resultArray = [attributeModels filteredArrayUsingPredicate:predice];
        NSNumber * number = [resultArray valueForKeyPath:@"@sum.width"];
        
        leftWith = self.leftMargin + self.itemMargin * index + number.doubleValue;
        
        CGRect frame = attr.frame;
        frame.origin.x = leftWith;
        attr.frame = frame;
    }
}
@end
