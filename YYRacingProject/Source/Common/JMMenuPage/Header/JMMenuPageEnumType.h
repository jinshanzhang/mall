//
//  JMMenuPageEnumType.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#ifndef JMMenuPageEnumType_h
#define JMMenuPageEnumType_h
/**
 * menuBarItem layout style
 **/
typedef NS_ENUM(NSUInteger, JMMenuBarLayoutStyle) {
    JMMenuBarLayoutStyleDefault = 0,  //默认自适应
    JMMenuBarLayoutStyleDivide //等分
};

typedef NS_ENUM(NSUInteger, JMMenuBarStayLocation) {
    JMMenuBarStayLocationDefault = 0, //默认第一个
    JMMenuBarStayLocationCenter, //居中
};

typedef NS_ENUM(NSUInteger, JMMenuPageSwitchEvent) {
    JMMenuPageSwitchEventLoad = 0, //加载
    JMMenuPageSwitchEventScroll, //滚动
    JMMenuPageSwitchEventClick, //点击
    JMMenuPageSwitchEventUnkown //未知
};

typedef NS_ENUM(NSUInteger, JMMenuPageAppearanceState) {
    JMMenuPageAppearanceStateDidDisappear, //已经消失
    JMMenuPageAppearanceStateWillDisappear, //将要消失
    JMMenuPageAppearanceStateWillAppear, //即将显示
    JMMenuPageAppearanceStateAppear //已经显示
};

#endif /* JMMenuPageEnumType_h */
