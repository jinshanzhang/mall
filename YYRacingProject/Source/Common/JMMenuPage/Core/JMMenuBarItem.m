//
//  JMMenuBarItem.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuBarItem.h"

/**
 *  set menuBarItem default style
 */
@interface JMMenuBarItemStyle : NSObject <JMMenuBarItemStyleProtocol>

@end

@implementation JMMenuBarItemStyle

- (UIFont *)jm_NormalFont {
    return [UIFont systemFontOfSize:13];
}

- (UIFont *)jm_SelectFont {
    return [UIFont systemFontOfSize:17];
}

- (UIColor *)jm_NormalTitleColor {
    return [UIColor blackColor];
}

- (UIColor *)jm_SelectTitleColor {
    return [UIColor redColor];
}

@end


@interface JMMenuBarItem()


@end

@implementation JMMenuBarItem

- (id<JMMenuBarItemStyleProtocol>)itemStyleConfigure {
    return [[JMMenuBarItemStyle alloc] init];
}

- (void)isItitemScaleTransform:(BOOL)isScale {
    
}
@end


@implementation JMMenuBarTitleItem
#pragma mark - Setter
- (void)setItemSelect:(BOOL)itemSelect {
    [super setItemSelect:itemSelect];
    if (itemSelect) {
        self.titleLabel.font = self.itemStyleConfigure.jm_SelectFont;
        self.titleLabel.textColor = self.itemStyleConfigure.jm_SelectTitleColor;
    }
    else {
        self.titleLabel.font = self.itemStyleConfigure.jm_NormalFont;
        self.titleLabel.textColor = self.itemStyleConfigure.jm_NormalTitleColor;
    }
}

- (void)setItemStyleConfigure:(id<JMMenuBarItemStyleProtocol>)itemStyleConfigure {
    if (itemStyleConfigure) {
        self.titleLabel.font = itemStyleConfigure.jm_NormalFont;
        self.titleLabel.textColor = itemStyleConfigure.jm_NormalTitleColor;
    }
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [self initWithFrame:CGRectZero];
    if (self) { }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemSelect = NO;
        [self _setSubViews];
        [self _setSubLayouts];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self _setSubLayouts];
}
#pragma mark - Private
- (void)_setSubViews {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
}

- (void)_setSubLayouts {
    self.titleLabel.bounds = self.bounds;
    self.titleLabel.center = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0);
}


@end
