//
//  JMMenuPageView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JMMenuContentView;
NS_ASSUME_NONNULL_BEGIN

@interface JMMenuPageView : UIView

@property (nonatomic, weak) id <JMMenuPageViewDelegate> menuPageDelegate;

@property (nonatomic, weak) id <JMMenuPageViewDataSource> menuPageDataSource;

@property (nonatomic, weak) UIViewController <JMMenuPagePresentProtocol> *parentViewController;
/**
 * The source of data for the menu page.
 **/
@property (nonatomic, strong) NSMutableArray <id <JMMenuBarItemContentProtocol>> *menuContents;

/**
 * set menu page layout style.
 **/
@property (nonatomic, assign) JMMenuBarLayoutStyle menuPageLayoutStyle;

/**
 * set menu page stay style.
 **/
@property (nonatomic, assign) JMMenuBarStayLocation menuPageStayStyle;

/**
 * menu page item containers.
 **/
@property (nonatomic, strong) JMMenuBar *menuBar;

/**
 * menu page containers.
 **/
@property (nonatomic, strong) JMMenuContentView *contentView;

/**
 * navigation view.
 **/
@property (nonatomic, strong, readonly) UIView *navigationView;


/**
 * add left item view at navigation view.
 **/
@property (nonatomic, strong, nullable) UIView *leftNavigationItem;

/**
 * add right item view at navigation view.
 **/
@property (nonatomic, strong, nullable) UIView *rightNavigationItem;

/**
 * add additional scroll area around content. default UIEdgeInsetsZero
 **/
@property (nonatomic, assign) UIEdgeInsets navigationInset;

/**
 * Set the navigationview background color. default [UIColor whiteColor]
 **/
@property (nonatomic, strong, nullable) UIColor *navigationColor;

/**
 * Increase the spacing between the item. default 0.0
 **/
@property (nonatomic, assign) CGFloat itemSpace;

/**
 * Set the navigationview height. default 44.0
 **/
@property (nonatomic, assign) CGFloat navigationHeight;

/**
 * Set up navigation ambiguity.
 **/
@property (nonatomic, assign) CGFloat navigationBlurValue;

/*
 * add left navigation space.
 **/
@property (nonatomic, assign) CGFloat leftNavigationSpace;

/**
 * Whether to allow sliding around the page, the default YES
 */
@property (nonatomic, assign, getter=isScrollEnabled) BOOL scrollEnabled;

/**
 *  Whether the navigation menu to the left or right sliding, YES by default
 */
@property (nonatomic, assign, getter=isMenuScrollEnabled) BOOL menuScrollEnabled;

/**
 * Whether to allow switching, including sliding around and click on the switch, the default is YESIf ban, all the switching events no response, not special circumstances shall not modify the attribute
 */
@property (nonatomic, assign, getter=isSwitchEnabled) BOOL switchEnabled;

/**
 * Whether you need click on the navigation menu to switch when the page is animated, the default is YES.
 */
@property (nonatomic, assign, getter=isSwitchAnimated) BOOL switchAnimated;

/**
 * Whether hidden the navigationView.
 **/
@property (nonatomic, assign, getter=isHeaderHidden) BOOL navigationHidden;

/**
 * Is switching, animation only when switching to YES.
 **/
@property (nonatomic, assign, getter=isSwitching) BOOL switching;

/**
 * Whether the preload.
 **/
@property (nonatomic, assign) BOOL isPreloading;

/**
 * Content is increased flexibility.
 **/
@property (nonatomic, assign) BOOL bounces;

/**
 * Switch event state.
 **/
@property (nonatomic, assign, readonly) JMMenuPageSwitchEvent switchEvent;

/**
 *  Show or hide the navigation view.
 */
- (void)setNavigationHidden:(BOOL)navigationHidden duration:(CGFloat)duration;

/**
 * Reload the data.
 **/
- (void)reloadData;

/**
 * Reload all of the data and positioning to the specified page at the same time, if the page of crossing the line, automatic correction of 0.
 **/
- (void)reloadDataToPage:(NSUInteger)pageIndex;

/**
 * Switch to the specified page.
 **/
- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated;

/**
 * clear memory cache.
 **/
- (void)clearMemoryCache;

/**
 * According to reuse identifier query reusable menubar item.
 */
- (nullable __kindof JMMenuBarItem *)dequeueReusableItemWithIdentifier:(NSString *)identifier;

/**
 * According to reuse identifier query reusable UIViewController.
 **/
- (nullable __kindof UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier;
@end

NS_ASSUME_NONNULL_END
