//
//  JMMenuPageViewController.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMMenuPageViewController : YYBaseViewController<JMMenuPagePresentProtocol, JMMenuPageViewDelegate, JMMenuPageViewDataSource>

@property (nonatomic, strong) JMMenuPageView *menuPageView;

@property (nonatomic, assign) NSUInteger currentPage;

@property (nonatomic, assign) VTAppearanceState appearanceState;

@property (nonatomic, strong, nullable) __kindof UIViewController *currentViewController;

@end

NS_ASSUME_NONNULL_END
