//
//  JMMenuContentView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMMenuContentView : UIScrollView

/**
 * Whether need to preload.
 */
@property (nonatomic, assign) BOOL isNeedPreload;

/**
 * Number of page controller.
 **/
@property (nonatomic, assign) NSInteger pageCount;

/**
 * Current page of visible controller.
 **/
@property (nonatomic, assign) NSInteger currentPage;

/**
 * DataSource of JMMenuContentView.
 **/
@property (nonatomic, weak) id <JMMenuContentViewDataSource> menuContentViewDataSource;

/**
 * reload data source.
 **/
- (void)reloadData;

/**
 * reset frames.
 **/
- (void)resetPageFrames;

/**
 *  clear memory cache.
 */
- (void)clearMemoryCache;

/**
 * According to the pageIndex for the view frame.
 **/
- (CGRect)frameOfViewControllerAtPage:(NSUInteger)pageIndex;

/**
 * According to the pageIndex for the UIViewController.
 **/
- (nullable __kindof UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex;

/**
 * According to the pageIndex for the UIViewController.
 **/
- (nullable __kindof UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex autoCreate:(BOOL)autoCreate;

/**
 * To create the UIViewController with pageIndex.
 **/
- (nullable __kindof UIViewController *)creatViewControllerAtPage:(NSUInteger)pageIndex;

/**
 * According to reuse identifier query reusable UIViewController.
 **/
- (nullable __kindof UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier;
@end

NS_ASSUME_NONNULL_END
