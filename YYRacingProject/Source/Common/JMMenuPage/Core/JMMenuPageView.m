//
//  JMMenuPageView.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuPageView.h"

typedef struct {
    unsigned int dataSourceMenuBarItem : 1;
    unsigned int dataSourceViewController : 1;
    unsigned int viewControllerDidAppear : 1;
    unsigned int viewControllerDidDisappear : 1;
    unsigned int shouldManualForwardAppearanceMethods : 1;
} JMMenuPageFlags;

@interface JMMenuPageView ()
<
JMMenuBarDelegate,
JMMenuBarDataSource,
UIScrollViewDelegate,
JMMenuContentViewDataSource>

@property (nonatomic, strong) UIVisualEffectView *navigationViewEffectView; //导航模糊效果

@property (nonatomic, assign) JMMenuPageFlags  menuPageFlags;
@property (nonatomic, assign) NSInteger nextPageIndex; // 下一个页面的索引
@property (nonatomic, assign) NSInteger currentPage; //当前页面的索引
@property (nonatomic, assign) NSInteger previousIndex; // 上一个页面的索引

@property (nonatomic, assign) BOOL isViewWillApear;
@property (nonatomic, assign) BOOL needSkipUpdate; //是否需要跳页刷新

@end

@implementation JMMenuPageView
@synthesize navigationView = _navigationView;
@synthesize switching = _switching;

#pragma mark - Setter and Getter
- (UIView *)navigationView {
    if (!_navigationView) {
        _navigationView = [[UIView alloc] init];
        _navigationView.backgroundColor = [UIColor clearColor];
        _navigationView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _navigationView.clipsToBounds = YES;
    }
    return _navigationView;
}

- (JMMenuBar *)menuBar {
    if (!_menuBar) {
        _menuBar = [[JMMenuBar alloc] init];
        _menuBar.backgroundColor = [UIColor clearColor];
        _menuBar.showsHorizontalScrollIndicator = NO;
        _menuBar.showsVerticalScrollIndicator = NO;
        _menuBar.clipsToBounds = YES;
        _menuBar.scrollsToTop = NO;
        _menuBar.isUseCache = NO;
        _menuBar.rightAddSite = YES;
        _menuBar.menuBarDelegate = self;
        _menuBar.menuBarDataSource = self;
    }
    return _menuBar;
}

- (JMMenuContentView *)contentView {
    if (!_contentView) {
        _contentView = [[JMMenuContentView alloc] init];
        _contentView.showsVerticalScrollIndicator = NO;
        _contentView.showsHorizontalScrollIndicator = NO;
        _contentView.pagingEnabled = YES;
        _contentView.scrollsToTop = NO;
        _contentView.delegate = self;
        _contentView.menuContentViewDataSource = self;
        _contentView.bounces = NO;
        _contentView.backgroundColor = XHClearColor;
    }
    return _contentView;
}

- (UIVisualEffectView *)navigationViewEffectView {
    if (!_navigationViewEffectView) {
        UIBlurEffect *blureStyle = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        _navigationViewEffectView = [[UIVisualEffectView alloc] initWithEffect:blureStyle];
        _navigationViewEffectView.alpha = 0.0;
    }
    return _navigationViewEffectView;
}

- (void)setMenuPageLayoutStyle:(JMMenuBarLayoutStyle)menuPageLayoutStyle {
    _menuPageLayoutStyle = menuPageLayoutStyle;
    _menuBar.menuBarLayoutStyle = menuPageLayoutStyle;
}

- (void)setMenuPageStayStyle:(JMMenuBarStayLocation)menuPageStayStyle {
    _menuPageStayStyle = menuPageStayStyle;
    _menuBar.selectItemStayType = menuPageStayStyle;
}

- (void)setLeftNavigationItem:(UIView *)leftNavigationItem {
    _leftNavigationItem = leftNavigationItem;
    [_navigationView addSubview:leftNavigationItem];
    [_navigationView bringSubviewToFront:_menuBar];
    
    [self _updateFrameForLeftNavigationItem];
}

- (void)setLeftNavigationSpace:(CGFloat)leftNavigationSpace {
    _leftNavigationSpace = leftNavigationSpace;
}

- (void)setRightNavigationItem:(UIView *)rightNavigationItem {
    _rightNavigationItem = rightNavigationItem;
    [_navigationView addSubview:rightNavigationItem];
    [_navigationView bringSubviewToFront:_menuBar];
    
    [self _updateFrameForRightNavigationItem];
}

- (void)setMenuPageDataSource:(id<JMMenuPageViewDataSource>)menuPageDataSource {
    _menuPageDataSource = menuPageDataSource;
    _menuPageFlags.dataSourceMenuBarItem = [_menuPageDataSource respondsToSelector:@selector(menuPageView:menuBarItemAtIndex:)];
    _menuPageFlags.dataSourceViewController = [_menuPageDataSource respondsToSelector:@selector(menuPageView:viewControllerAtPage:)];
}

- (void)setMenuPageDelegate:(id<JMMenuPageViewDelegate>)menuPageDelegate {
    _menuPageDelegate = menuPageDelegate;
    _menuPageFlags.viewControllerDidAppear = [_menuPageDelegate respondsToSelector:@selector(menuPageView:viewDidAppear:atPage:)];
    _menuPageFlags.viewControllerDidDisappear = [_menuPageDelegate respondsToSelector:@selector(menuPageView:viewDidDisappear:atPage:)];
    // 赋值父控制器
    if (!_parentViewController && [_menuPageDelegate isKindOfClass:[UIViewController class]] && [_menuPageDelegate conformsToProtocol:@protocol(JMMenuPagePresentProtocol)]) {
        _parentViewController = (UIViewController <JMMenuPagePresentProtocol>*)_menuPageDelegate;
    }
}

- (void)setScrollEnabled:(BOOL)scrollEnabled {
    _scrollEnabled = scrollEnabled;
    _contentView.scrollEnabled = scrollEnabled;
}

- (void)setMenuScrollEnabled:(BOOL)menuScrollEnabled {
    _menuScrollEnabled = menuScrollEnabled;
    _menuBar.scrollEnabled = menuScrollEnabled;
}

- (void)setSwitchEnabled:(BOOL)switchEnabled {
    _switchEnabled = switchEnabled;
    _menuBar.scrollEnabled = switchEnabled;
    self.scrollEnabled = switchEnabled;
}

- (void)setIsPreloading:(BOOL)isPreloading {
    _isPreloading = isPreloading;
    _contentView.isNeedPreload = isPreloading;
}

- (void)setBounces:(BOOL)bounces {
    _bounces = bounces;
    _contentView.bounces = bounces;
}

- (void)setNavigationBlurValue:(CGFloat)navigationBlurValue {
    _navigationBlurValue = navigationBlurValue;
    _navigationViewEffectView.alpha = navigationBlurValue;
}

- (void)setNavigationInset:(UIEdgeInsets)navigationInset {
    _navigationInset = navigationInset;
    _menuBar.menuBarEdgeInsets = navigationInset;
}

- (void)setNavigationColor:(UIColor *)navigationColor {
    _navigationColor = navigationColor;
    _navigationView.backgroundColor = navigationColor;
}

- (void)setItemSpace:(CGFloat)itemSpace {
    _itemSpace = itemSpace;
    _menuBar.menuBarItemSpace = itemSpace;
}

- (void)setNavigationHidden:(BOOL)navigationHidden {
    _navigationHidden = navigationHidden;
    _navigationView.hidden = navigationHidden;
    [self _updataFrameForSubViews];
}

- (void)setNavigationHidden:(BOOL)navigationHidden duration:(CGFloat)duration {
    _navigationView.hidden = NO;
    _navigationHidden = navigationHidden;
    __weak typeof(self) _self = self;
    [UIView animateWithDuration:duration animations:^{
        [self _updataFrameForSubViews];
    } completion:^(BOOL finished) {
        __strong typeof(_self) self = _self;
        self.navigationView.hidden = navigationHidden;
    }];
}


- (void)setParentViewController:(UIViewController<JMMenuPagePresentProtocol> *)parentViewController {
    _parentViewController = parentViewController;
    if ([parentViewController respondsToSelector:@selector(shouldAutomaticallyForwardAppearanceMethods)]) {
        _menuPageFlags.shouldManualForwardAppearanceMethods = [parentViewController shouldAutomaticallyForwardAppearanceMethods];
    }
}

- (void)setCurrentPage:(NSInteger)currentPage {
    if (currentPage < 0) return;
    
    NSInteger preIndex = _currentPage;
    _currentPage = currentPage;
    _previousIndex = preIndex;
    _contentView.currentPage = _currentPage;
    _menuBar.currentItemIndex = _currentPage;
    
    if (_switchEvent != JMMenuPageSwitchEventScroll) return;
    
    [self displayPageHasChanged:_currentPage preIndex:preIndex];
    [self viewControllerDidDisAppear:preIndex];
    [self viewControllerDidAppear:_currentPage];
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _setBasicProperty];
        [self _setSubViews];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self _updataFrameForSubViews];
}

#pragma mark - Public
- (void)reloadData {
    [self reloadDataWithIndex:_currentPage];
}

- (void)reloadDataToPage:(NSUInteger)pageIndex {
    _previousIndex = _currentPage;
    _currentPage = pageIndex;
    _nextPageIndex = pageIndex;
    _menuBar.currentItemIndex = pageIndex;
    _contentView.currentPage = pageIndex;
    [self reloadDataWithIndex:_previousIndex];
}

- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated {
    if (pageIndex == _currentPage || self.menuContents.count <= pageIndex) {
        return;
    }
    
    _switchEvent = JMMenuPageSwitchEventScroll;
    _contentView.currentPage = pageIndex;
    if (animated && _isPreloading) {
        [self switchAnimation:pageIndex];
    } else {
        [self switchWithOutAnimation:pageIndex];
    }

}

- (void)clearMemoryCache {
    [self.contentView clearMemoryCache];
}

- (nullable __kindof JMMenuBarItem *)dequeueReusableItemWithIdentifier:(NSString *)identifier {
    JMMenuBarItem *menuItem = [self.menuBar dequeueReusableItemWithIdentifier:identifier];
    return menuItem;
}

- (UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier {
    UIViewController *viewController = [self.contentView dequeueReusablePageWithIdentifier:identifier];
    return viewController;
}

#pragma mark - Private
- (void)_setBasicProperty {
    _isPreloading = YES;
    _scrollEnabled = YES;
    _menuScrollEnabled = YES;
    _navigationHidden = NO;
    _switchAnimated = YES;
    _switchEnabled = YES;
    
    _navigationHeight = 44.0;
    _leftNavigationSpace = 0.0;
    _navigationInset = UIEdgeInsetsZero;
    _navigationColor = [UIColor whiteColor];
}

- (void)_setSubViews {
    [self addSubview:self.navigationView];
    [self addSubview:self.contentView];
    
    [self.navigationView addSubview:self.menuBar];
    [self.navigationView addSubview:self.navigationViewEffectView];
}

- (void)_updataFrameForSubViews {
    
    CGSize size = self.frame.size;
    CGFloat menuOriginY = _navigationHidden ?-_navigationHeight:0.0;
    CGRect originMenuFrame = _menuBar.frame;
    CGFloat leftItemWidth = CGRectGetMaxX(_leftNavigationItem.frame) + self.leftNavigationSpace + (_leftNavigationItem.size.width > 0 ? kSizeScale(7) : 0);
    CGFloat rightItemWidth = CGRectGetWidth(_rightNavigationItem.frame);
    CGFloat menuWidth = size.width - leftItemWidth - rightItemWidth;
    
    _menuBar.frame = CGRectMake(leftItemWidth, menuOriginY , menuWidth, _navigationHeight);
    
    CGFloat tempNavigationHeight = _navigationHidden ? 0.0 : _navigationHeight;
    _navigationView.frame = CGRectMake(0, 0, size.width, tempNavigationHeight);
    
    [self _updateFrameForLeftNavigationItem];
    [self _updateFrameForRightNavigationItem];
    
    if (!CGRectEqualToRect(originMenuFrame, _menuBar.frame)) {
        [_menuBar resetItemFrames];
    }
    self.needSkipUpdate = YES;
    CGRect originContentFrame = _contentView.frame;
    CGFloat contentY = CGRectGetMaxY(_navigationView.frame);
    CGFloat contentH = size.height - contentY;
    _contentView.frame = CGRectMake(0, contentY, size.width, contentH);
    if (!CGRectEqualToRect(originContentFrame, _contentView.frame)) {
        [_contentView resetPageFrames];
    }
    self.needSkipUpdate = NO;
}

- (void)_updateFrameForLeftNavigationItem {
    CGRect leftFrame = _leftNavigationItem.frame;
    CGFloat leftHeight = CGRectGetHeight(leftFrame);
    leftFrame.origin.y = (CGRectGetHeight(_navigationView.bounds) - leftHeight)/2.0;
    _leftNavigationItem.frame = leftFrame;
    
    CGRect originMenuFrame = _menuBar.frame;
    originMenuFrame.origin.x = _leftNavigationItem.origin.x + _leftNavigationItem.size.width + (_leftNavigationItem.size.width > 0 ? kSizeScale(7) : 0);
    
    _menuBar.frame = originMenuFrame;
}

- (void)_updateFrameForRightNavigationItem {
    CGRect rightFrame = _rightNavigationItem.frame;
    CGFloat rightHeight = CGRectGetHeight(rightFrame);
    rightFrame.origin.x = _navigationView.frame.size.width - rightFrame.size.width;
    rightFrame.origin.y = (CGRectGetHeight(_navigationView.bounds) - rightHeight)/2.0;
    
    _rightNavigationItem.frame = rightFrame;
    
    CGRect originMenuFrame = _menuBar.frame;
    originMenuFrame.size.width = originMenuFrame.size.width - (_rightNavigationItem.size.width+(self.width - rightFrame.origin.x));
    _menuBar.frame = originMenuFrame;
}

- (void)reloadDataWithIndex:(NSInteger)index {
    
    UIViewController *viewController = [self.contentView viewControllerAtPage:index];
    if (viewController && _menuPageFlags.viewControllerDidDisappear) {
        [_menuPageDelegate menuPageView:self viewDidDisappear:viewController atPage:index];
    }
    [self viewControllerWillDidAppear:index];
    [self viewControllerDidDisAppear:index];
    
    if (self.menuContents && self.menuContents.count > 0) {
        self.menuBar.menuBarContents = self.menuContents;
    }
    
    if (self.menuContents.count <= _currentPage) {
        _currentPage = 0;
        _nextPageIndex = _currentPage;
        _previousIndex = _currentPage;
        _menuBar.currentItemIndex = _currentPage;
        _contentView.currentPage = _currentPage;
        [_parentViewController setCurrentViewController:nil];
        [_parentViewController setCurrentPage:_currentPage];
    }
    
    _switchEvent = JMMenuPageSwitchEventLoad;
    _contentView.pageCount = self.menuContents.count;
    [_contentView reloadData];
    [_menuBar reloadData];
    [self updateMenuBarWhenSwitchEnd];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)switchWithOutAnimation:(NSInteger)pageIndex {
    if ( self.menuContents.count < pageIndex) return;
    
    [_contentView creatViewControllerAtPage:_currentPage];
    [_contentView creatViewControllerAtPage:pageIndex];
    [self subviewWillAppearAtPage:pageIndex];
    
    self.needSkipUpdate = YES;
    CGFloat offset = _contentView.frame.size.width * pageIndex;
    _contentView.contentOffset = CGPointMake(offset, 0);
    self.needSkipUpdate = NO;
    
    _previousIndex = _currentPage;
    _currentPage = pageIndex;
    _contentView.currentPage = _currentPage;
    _menuBar.currentItemIndex = _currentPage;
    
    [self displayPageHasChanged:pageIndex preIndex:_previousIndex];
    [self viewControllerDidDisAppear:_previousIndex];
    if (JMMenuPageAppearanceStateWillAppear != _parentViewController.appearanceState) {
        [self viewControllerDidAppear:pageIndex];
    }
    [self updateMenuBarWhenSwitchEnd];
}

- (void)switchAnimation:(NSInteger)pageIndex {
    if ( self.menuContents.count < pageIndex) return;
    
    _switching = YES;
    __weak typeof(self) _self = self;
    NSInteger preIndex = _currentPage;
    CGFloat contentWidth = CGRectGetWidth(self.contentView.frame);
    BOOL isNotAdjacent = abs((int)(_currentPage - pageIndex)) > 1;
    if (isNotAdjacent) {// 当前按钮与选中按钮不相邻时
        self.needSkipUpdate = YES;
        _isViewWillApear = YES;
        [self displayPageHasChanged:pageIndex preIndex:_currentPage];
        [self subviewWillAppearAtPage:pageIndex];
        [self viewControllerDidDisAppear:preIndex];
        [_parentViewController setCurrentViewController:nil];
        NSInteger tempIndex = pageIndex + (_currentPage < pageIndex ? -1 : 1);
        _contentView.contentOffset = CGPointMake(contentWidth * tempIndex, 0);
        _isViewWillApear = NO;
    }
    else {
        [self viewControllerWillDidAppear:preIndex];
        [self viewControllerWillAppear:pageIndex];
    }
    
    _currentPage = pageIndex;
    _previousIndex = preIndex;
    _contentView.currentPage = pageIndex;
    _menuBar.currentItemIndex = pageIndex;
    
    [UIView animateWithDuration:JMMenuPageSwitchDuration animations:^{
        __strong typeof(_self) self = _self;
        [self.menuBar updateItemSelectStateAtIndex:pageIndex];
        self.contentView.contentOffset = CGPointMake(contentWidth * pageIndex, 0);
    } completion:^(BOOL finished) {
        __strong typeof(_self) self = _self;
        [self displayPageHasChanged:self.currentPage preIndex:preIndex];
        if (!isNotAdjacent && self.currentPage != preIndex) {
            [self viewControllerDidDisAppear:preIndex];
        }
        if (pageIndex == self.currentPage) {
            [self viewControllerDidAppear:pageIndex];
        }
        self.needSkipUpdate = NO;
        self.switching = NO;
    }];
}

- (void)viewControllerWillAppear:(NSInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    UIViewController *viewController = [_contentView creatViewControllerAtPage:pageIndex];
    if (viewController) {
        [viewController beginAppearanceTransition:YES animated:YES];
    }
}

- (void)viewControllerDidAppear:(NSInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    UIViewController *viewController = [_contentView viewControllerAtPage:pageIndex];
    if (viewController) {
        [viewController endAppearanceTransition];
    }
}

- (void)viewControllerWillDidAppear:(NSInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    UIViewController *viewController = [_contentView viewControllerAtPage:pageIndex];
    if (viewController) {
        [viewController beginAppearanceTransition:NO animated:YES];
    }
}

- (void)viewControllerDidDisAppear:(NSInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    UIViewController *viewController = [_contentView viewControllerAtPage:pageIndex];
    if (viewController) {
        [viewController endAppearanceTransition];
    }
}

- (BOOL)shouldForwardAppearanceMethods {
    return _menuPageFlags.shouldManualForwardAppearanceMethods && (JMMenuPageAppearanceStateDidDisappear == _parentViewController.appearanceState || JMMenuPageAppearanceStateWillAppear == _parentViewController.appearanceState);
}


- (UIViewController *)autoCreateViewControllAtPage:(NSInteger)pageIndex {
   return [_contentView viewControllerAtPage:pageIndex autoCreate:!_needSkipUpdate];
}

- (void)displayPageHasChanged:(NSInteger)pageIndex preIndex:(NSInteger)preIndex {
    UIViewController *appearViewController = [self autoCreateViewControllAtPage:pageIndex];
    UIViewController *disAppearViewController = [self autoCreateViewControllAtPage:preIndex];

    if (appearViewController) {
        [_parentViewController setCurrentPage:pageIndex];
        [_parentViewController setCurrentViewController:appearViewController];
    }
    if (appearViewController && _menuPageFlags.viewControllerDidAppear) {
        [_menuPageDelegate menuPageView:self viewDidAppear:appearViewController atPage:pageIndex];
    }
    if (disAppearViewController && _menuPageFlags.viewControllerDidDisappear) {
        [_menuPageDelegate menuPageView:self viewDidDisappear:disAppearViewController atPage:preIndex];
    }
}

- (void)subviewWillAppearAtPage:(NSInteger)pageIndex {
    if ( _nextPageIndex == pageIndex ) return;
    
    if (_contentView.isDragging && 1 < ABS(_nextPageIndex - pageIndex)) {
        [self viewControllerWillDidAppear:_nextPageIndex];
        [self viewControllerDidDisAppear:_nextPageIndex];
    }
    
    [self viewControllerWillDidAppear:_currentPage];
    [self viewControllerWillAppear:pageIndex];
    _nextPageIndex = pageIndex;
}

- (void)updateMenuBarWhenSwitchEnd {
    _menuBar.needSkipLayout = NO;
    [UIView animateWithDuration:JMMenuPageSwitchDuration animations:^{
        [self.menuBar updateItemSelectStateAtIndex:self.currentPage];
    }];
}

#pragma mark - JMMenuBarDelegate
- (void)menuBar:(JMMenuBar *)menuBar didSelectItemAtIndex:(NSInteger)itemIndex {
    if ( !_switchEnabled ) return;
    
    if (self.menuPageDelegate && [self.menuPageDelegate respondsToSelector:@selector(menuPageView:didSelectItemAtIndex:)]) {
        [self.menuPageDelegate menuPageView:self didSelectItemAtIndex:itemIndex];
    }
    
    if (itemIndex == _menuBar.currentItemIndex) return;
    
    [self _updataFrameForSubViews];
    
    _switchEvent = JMMenuPageSwitchEventClick;
    if (_switchEnabled && _isPreloading) {
        [self switchAnimation:itemIndex];
    }
    else {
        [self switchWithOutAnimation:itemIndex];
    }
}

- (CGFloat)menuBar:(JMMenuBar *)menuBar menuBarItemWidthAtIndex:(NSInteger)itemIndex {
    if (self.menuPageDelegate && [self.menuPageDelegate respondsToSelector:@selector(menuPageView:menuBarItemWidthAtIndex:)]) {
     return [self.menuPageDelegate menuPageView:self menuBarItemWidthAtIndex:itemIndex];
    }
    return 0.0;
}

#pragma mark - JMMenuBarDataSource
- (JMMenuBarItem *)menuBar:(JMMenuBar *)menuBar menuBarItemAtIndex:(NSInteger)itemIndex {
    if (self.menuPageDataSource && [self.menuPageDataSource respondsToSelector:@selector(menuPageView:menuBarItemAtIndex:)]) {
        return [self.menuPageDataSource menuPageView:self menuBarItemAtIndex:itemIndex];
    }
    return nil;
}

#pragma mark - JMMenuContentViewDataSource
- (UIViewController *)menuContentView:(JMMenuContentView *)menuContentView viewControllerAtPage:(NSInteger)pageIndex {
    
    if (self.menuPageDataSource && [self.menuPageDataSource respondsToSelector:@selector(menuPageView:viewControllerAtPage:)]) {
        UIViewController *viewController = [self.menuPageDataSource menuPageView:self viewControllerAtPage:pageIndex];
        if (viewController && ![viewController.parentViewController isEqual:_parentViewController]) {
            [_parentViewController addChildViewController:viewController];
            [menuContentView addSubview:viewController.view];
            [viewController didMoveToParentViewController:_parentViewController];
            if (pageIndex == _currentPage && JMMenuPageSwitchEventLoad == _switchEvent) {
                [self resetCurrentViewController:viewController];
            }
        }
        return viewController;
    }
    return nil;
}

- (void)resetCurrentViewController:(UIViewController *)viewController {
    [_parentViewController setCurrentPage:_currentPage];
    [_parentViewController setCurrentViewController:viewController];
    viewController.view.frame = [_contentView frameOfViewControllerAtPage:_currentPage];
    if (_menuPageFlags.viewControllerDidAppear) {
        [_menuPageDelegate menuPageView:self viewDidAppear:viewController atPage:_currentPage];
    }
    
    if ([self shouldForwardAppearanceMethods]) {
        [viewController beginAppearanceTransition:YES animated:NO];
        if (JMMenuPageAppearanceStateWillAppear != _parentViewController.appearanceState) {
            [viewController endAppearanceTransition];
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (_menuBar.isTracking) {
        _contentView.scrollEnabled = NO;
    } else if (_contentView.isTracking) {
        _menuBar.needSkipLayout = NO;
        _switchEvent = JMMenuPageSwitchEventScroll;
        _menuBar.scrollEnabled = NO;
        _isViewWillApear = NO;
    }
    if (self.menuPageDelegate && [self.menuPageDelegate respondsToSelector:@selector(menuPageView:scrollViewWillBeginDragging:)]) {
        [self.menuPageDelegate menuPageView:self scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![scrollView isEqual:_contentView] || _needSkipUpdate || CGRectIsEmpty(self.frame)) {
        return;
    }
    if (self.menuPageDelegate && [self.menuPageDelegate respondsToSelector:@selector(menuPageView:scrollViewDidScroll:)]) {
        [self.menuPageDelegate menuPageView:self scrollViewDidScroll:scrollView];
    }
    NSInteger newIndex;
    NSInteger tempIndex;
    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat scrollWidth = scrollView.frame.size.width;
    BOOL isSwipeToLeft = scrollWidth * _currentPage < offsetX;
    if (isSwipeToLeft) { // 向左滑动
        newIndex = floorf(offsetX/scrollWidth);
        tempIndex = (int)((offsetX + scrollWidth - 0.1)/scrollWidth);
    } else {
        newIndex = ceilf(offsetX/scrollWidth);
        tempIndex = (int)(offsetX/scrollWidth);
    }
    if (!_needSkipUpdate && newIndex != _currentPage) {
        self.currentPage = newIndex;
        [self updateMenuBarWhenSwitchEnd];
    }
    
    if (_nextPageIndex != tempIndex) _isViewWillApear = NO;
    if (!_isViewWillApear && newIndex != tempIndex) {
        _isViewWillApear = YES;
        NSInteger nextPageIndex = newIndex + (isSwipeToLeft ? 1 : -1);
        [self subviewWillAppearAtPage:nextPageIndex];
    }
    
    if (tempIndex == _currentPage) { // 重置_nextPageIndex
        if (_nextPageIndex != _currentPage &&
            JMMenuPageSwitchEventScroll == _switchEvent) {
            [self viewControllerWillDidAppear:_nextPageIndex];
            [self viewControllerWillAppear:_currentPage];
            [self viewControllerDidDisAppear:_nextPageIndex];
            [self viewControllerDidAppear:_currentPage];
        }
        _nextPageIndex = _currentPage;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    _menuBar.scrollEnabled = _menuScrollEnabled; // 这里放开会导致最后一个滚动偏移
    _contentView.scrollEnabled = _scrollEnabled;
    if (self.menuPageDelegate && [self.menuPageDelegate respondsToSelector:@selector(menuPageView:scrollViewDidEndDragging:willDecelerate:)]) {
        [self.menuPageDelegate menuPageView:self scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
    
    if (!decelerate) {
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.menuPageDelegate && [self.menuPageDelegate respondsToSelector:@selector(menuPageView:scrollViewDidEndDecelerating:)]) {
        [self.menuPageDelegate menuPageView:self scrollViewDidEndDecelerating:scrollView];
    }
    
    if (![scrollView isEqual:_contentView]) {
        return;
    }
    
    if (JMMenuPageSwitchEventClick == _switchEvent) {
        CGFloat contentWidth = CGRectGetWidth(self.contentView.frame);
        CGPoint offset = CGPointMake(contentWidth * self.currentPage, 0);
        [self.contentView setContentOffset:offset animated:YES];
    }
    //[self updateMenuBarWhenSwitchEnd];
}

@end

