//
//  JMMenuBar.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuBar.h"

@interface JMMenuBar ()

@property (nonatomic, strong) NSMutableSet   *bufferPool; //缓存池
@property (nonatomic, strong) NSMutableArray *indexs; //索引集合
@property (nonatomic, strong) NSMutableArray *frames; //frame集合
@property (nonatomic, strong) NSMutableDictionary *visibles; //屏幕上可见的items
@property (nonatomic, strong) NSMutableDictionary *itemObjs; //所以的items

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) UIFont *itemDefaultFont;
@property (nonatomic, strong) JMMenuBarItem *selectItem;

@end

@implementation JMMenuBar


#pragma mark - Setter
- (void)setMenuBarContents:(NSMutableArray<id<JMMenuBarItemContentProtocol>> *)menuBarContents {
    _menuBarContents = menuBarContents;
    //[self reloadData];
}

- (void)setMenuBarItemSpace:(CGFloat)menuBarItemSpace {
    _menuBarItemSpace = menuBarItemSpace;
}

- (void)setMenuBarEdgeInsets:(UIEdgeInsets)menuBarEdgeInsets {
    _menuBarEdgeInsets = menuBarEdgeInsets;
}

- (void)setMenuBarLayoutStyle:(JMMenuBarLayoutStyle)menuBarLayoutStyle {
    _menuBarLayoutStyle = menuBarLayoutStyle;
}

- (void)setCurrentItemIndex:(NSInteger)currentItemIndex {
    _currentItemIndex = currentItemIndex;
}

- (void)setTitleColorTurnBlack:(BOOL)titleColorTurnBlack {
    _titleColorTurnBlack = titleColorTurnBlack;
    [self refreshItemsStyle];
}

- (void)setIsUseCache:(BOOL)isUseCache {
    _isUseCache = isUseCache;
}

- (void)setIsStartAddSpace:(BOOL)isStartAddSpace {
    _isStartAddSpace = isStartAddSpace;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.rightAddSite = NO;
        self.isUseCache = YES;
        self.isStartAddSpace = NO;
        _currentItemIndex = 0;
        _menuBarItemSpace = 0.0;
        _menuBarEdgeInsets = UIEdgeInsetsZero;
        _bufferPool = [[NSMutableSet alloc] init];
        _indexs = [[NSMutableArray alloc] init];
        _frames = [[NSMutableArray alloc] init];
        _visibles = [[NSMutableDictionary alloc] init];
        _itemObjs = [[NSMutableDictionary alloc] init];
        
        self.scrollsToTop = NO;
        self.contentSize = CGSizeZero;
        // 是否只允许同时滑动一个方向,默认为NO,如果设置为YES,用户在水平/竖直方向开始进行滑动,便禁止同时在竖直/水平方向滑动(注: 当用户在对角线方向开始进行滑动,则本次滑动可以同时在任何方向滑动)
        //self.directionalLockEnabled = YES;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (!self.isUseCache) {
        return;
    }
    BOOL mandatory = _menuBarContents.count && !_visibles.count;
    if (!mandatory && !self.decelerating && _needSkipLayout) {
        return;
    }
    JMMenuBarItem *menuItem = nil;
    CGRect menuFrame = CGRectZero;
    NSArray *tempIndexs = [_visibles allKeys];
    
    for (NSNumber *index in tempIndexs) {
        menuItem = [_visibles objectForKey:index];
        menuFrame = [[_frames objectAtIndex:[index intValue]] CGRectValue];
        if (![self jm_isItemNeedDisplayWithFrame:menuFrame]) {
            // 不需要显示
            [menuItem setSelected:NO];
            [menuItem removeFromSuperview];
            [_visibles removeObjectForKey:index];
            [_bufferPool addObject:menuItem];
        }
        else {
            menuItem.selected = ([index integerValue]==_currentItemIndex?YES:NO);
            menuItem.frame = menuFrame;
        }
    }
    
    NSMutableArray *leftIndexList = [_indexs mutableCopy];
    [leftIndexList removeObjectsInArray:tempIndexs]; // 对没有显示的item设置frame
    for (NSNumber *index in leftIndexList) {
        menuFrame = [_frames[[index integerValue]] CGRectValue];
        if ([self jm_isItemNeedDisplayWithFrame:menuFrame]) {
            [self loadItemWithIndex:[index integerValue]];
        }
    }
}

#pragma mark - Private

- (JMMenuBarItem *)loadItemWithIndex:(NSInteger)itemIndex {
    JMMenuBarItem *item = nil;
    if (self.menuBarDataSource && [self.menuBarDataSource respondsToSelector:@selector(menuBar:menuBarItemAtIndex:)]) {
        item = [self.menuBarDataSource menuBar:self menuBarItemAtIndex:itemIndex];
    }
    NSAssert([item isKindOfClass:[JMMenuBarItem class]], @"The class of menu item:%@ must be JMMenuBarItem", item);
    if (item) {
        item.itemSelect = NO;
        item.itemTag = JMMenuPageMenuBarItemBasicTag + itemIndex;
        if (itemIndex < _frames.count) {
            item.frame = [_frames[itemIndex] CGRectValue];
        }
        [item addTarget:self
                 action:@selector(menuBarItemClickEvent:)
       forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:item];
        if (self.isUseCache) {
            [_visibles setObject:item forKey:@(itemIndex)];
        }
        else {
            [_itemObjs setObject:item forKey:@(itemIndex)];
        }
    }
    return item;
}

- (JMMenuBarItem *)itemAtIndex:(NSUInteger)index autoCreate:(BOOL)autoCreate {
    if (_menuBarContents.count <= index) {
        return nil;
    }
    
    JMMenuBarItem *menuItem = nil;
    if (self.isUseCache) {
        menuItem = _visibles[@(index)];
    }
    else {
        menuItem = _itemObjs[@(index)];
    }
    if (autoCreate && !menuItem) {
        menuItem = [self loadItemWithIndex:index];
    }
    return menuItem;
}

- (void)scrollSelectItem:(JMMenuBarItem *)selectItem
                animated:(BOOL)animated {
    CGRect centerRect = CGRectZero;
    //CGFloat windowCenterWidth = [UIScreen mainScreen].bounds.size.width/2.0;
    if (self.selectItemStayType != JMMenuBarStayLocationCenter) {
        centerRect = CGRectMake(selectItem.frame.origin.x - selectItem.frame.size.width, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        [self scrollRectToVisible:centerRect animated:YES];
    }
    else {
        /*此次如果再细考虑，可以分window层居中，还是当前视图层居中。 这里暂时用window层居中。
        centerRect = CGRectMake(selectItem.center.x - kSizeScale(windowCenterWidth) + self.frame.origin.x, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));*/
        // 计算偏移量
        CGFloat offsetX = selectItem.center.x - self.frame.size.width * 0.5;
        if (offsetX < 0) offsetX = 0;
        // 获取最大滚动范围
        CGFloat maxOffsetX = self.contentSize.width - self.frame.size.width;
        if (offsetX > maxOffsetX) offsetX = maxOffsetX;
        // 滚动标题滚动条
        if (self.frame.origin.x > 0 && (offsetX != 0 && offsetX < maxOffsetX)) {
            offsetX = offsetX + self.frame.origin.x/2.0;
        }
        NSLog(@"[contentX = %f, x = %f]", offsetX, self.frame.origin.x);
        [self setContentOffset:CGPointMake(offsetX, 0) animated:YES];
    }
}

- (void)updateSelectItem:(JMMenuBarItem *)tempItem {
    if ( !tempItem ) { return; };
    
    _selectItem.itemSelect = NO;
    JMMenuBarItem *originItem = _selectItem;
    if (self.isUseCache) {
        _selectItem = _visibles[@(_currentItemIndex)];
    }
    else {
        _selectItem = _itemObjs[@(_currentItemIndex)];
    }
    _selectItem.itemSelect = YES;
    
    if (![originItem isEqual:_selectItem]) {
        [_selectItem isItitemScaleTransform:YES];
        [originItem isItitemScaleTransform:NO];
        [self scrollSelectItem:_selectItem
                      animated:YES];
    }
}

- (void)_resetCacheData {
    // reset index set.
    [_indexs removeAllObjects];
    NSInteger itemCount = _menuBarContents.count;
    for (NSInteger index = 0; index < itemCount; index ++ ){
        [_indexs addObject:@(index)];
    }
    // A visual item is added to the buffer pool.
    if (self.isUseCache) {
        NSArray *visualItems = [_visibles allValues];
        for (JMMenuBarItem *item in visualItems) {
            item.itemSelect = NO;
            [item removeFromSuperview];
            [_bufferPool addObject:item];
        }
        // empty visibles
        [_visibles removeAllObjects];
    }
    else {
        // empty visibles
        [_itemObjs removeAllObjects];
    }
}

- (void)_resetFramesForDefault {
    CGFloat itemWidth = 0.0;
    CGRect itemFrame = CGRectZero;
    CGFloat itemHeight = self.frame.size.height;
    CGFloat itemX = _menuBarEdgeInsets.left + (_isStartAddSpace ? _menuBarItemSpace : 0);
    itemHeight -= _menuBarEdgeInsets.bottom + _menuBarEdgeInsets.top;
    
    JMMenuBarItem *menuItem = nil;
    if (!self.itemDefaultFont) {
        menuItem = [self createItemAtIndex:_currentItemIndex];
        self.itemDefaultFont = menuItem.itemStyleConfigure.jm_SelectFont;
    }
    
    for (NSInteger index = 0; index < self.menuBarContents.count; index ++) {
        /*id <JMMenuBarItemContentProtocol> itemContent = [self.menuBarContents objectAtIndex:index];
        itemWidth = [NSString sizeWithTitle:itemContent.itemContent
                                       font:self.itemDefaultFont].width;*/
        if (self.menuBarDelegate && [self.menuBarDelegate respondsToSelector:@selector(menuBar:menuBarItemWidthAtIndex:)]) {
            itemWidth = [self.menuBarDelegate menuBar:self menuBarItemWidthAtIndex:index];
        }
        itemFrame = CGRectMake(itemX, _menuBarEdgeInsets.top, itemWidth, itemHeight);
        [_frames addObject:[NSValue valueWithCGRect:itemFrame]];
        itemX = itemX + itemWidth + _menuBarItemSpace;
    }
}

- (void)_resetFramesForDivide {
    CGFloat itemWidth = 0.0;
    CGRect itemFrame = CGRectZero;
    CGFloat itemHeight = self.frame.size.height;
    CGFloat itemX = _menuBarEdgeInsets.left + (_isStartAddSpace ? _menuBarItemSpace : 0);
    itemHeight -= _menuBarEdgeInsets.bottom + _menuBarEdgeInsets.top;
    
    for (NSInteger index = 0; index < self.menuBarContents.count; index ++) {
        if (self.menuBarDelegate && [self.menuBarDelegate respondsToSelector:@selector(menuBar:menuBarItemWidthAtIndex:)]) {
            itemWidth = [self.menuBarDelegate menuBar:self menuBarItemWidthAtIndex:index];
        }
        itemFrame = CGRectMake(itemX, _menuBarEdgeInsets.top, itemWidth, itemHeight);
        [_frames addObject:[NSValue valueWithCGRect:itemFrame]];
        itemX = itemX + itemWidth + _menuBarItemSpace;
    }
}


#pragma mark - Public
- (void)reloadData {
    [self _resetCacheData];
    [self resetItemFrames];
    if (!self.isUseCache) {
        // 不使用缓存
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        for(NSNumber *index in _indexs) {
            [self loadItemWithIndex:[index integerValue]];
        }
    }
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)resetItemFrames {
    [_frames removeAllObjects];
    if (_menuBarContents.count <= 0) { return;}
    
    switch (_menuBarLayoutStyle) {
        case JMMenuBarLayoutStyleDivide:
            // item 宽度平分
            [self _resetFramesForDivide];
            break;
        default:
            // 默认内容自适应
            [self _resetFramesForDefault];
            break;
    }
    CGRect lastFrame = [[_frames lastObject] CGRectValue];
    CGFloat contentWidth = CGRectGetMaxX(lastFrame);
    contentWidth += _menuBarEdgeInsets.right;
    self.contentSize = CGSizeMake(contentWidth, self.frame.size.height);
}

- (void)refreshItemsStyle {
    if (self.itemObjs.count > 0) {
        [self.itemObjs enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            JMMenuBarItem *item = (JMMenuBarItem *)obj;
            NSLog(@"====%d",item.itemSelect);
            item.titleColorTurnBlack = self.titleColorTurnBlack;
        }];
    }
}

- (void)updateItemSelectStateAtIndex:(NSInteger)index {
    if (index < 0 || index >= self.menuBarContents.count) {
        return;
    }
    JMMenuBarItem *menuItem = [self itemAtIndex:index];
    [self updateSelectItem:menuItem];
}

- (JMMenuBarItem *)itemAtIndex:(NSUInteger)index {
    return [self itemAtIndex:index autoCreate:NO];
}

- (JMMenuBarItem *)createItemAtIndex:(NSUInteger)index {
    return [self itemAtIndex:index autoCreate:YES];
}

- (JMMenuBarItem *)dequeueReusableItemWithIdentifier:(NSString *)identifier {
    _identifier = identifier;
    JMMenuBarItem *menuItem = [_bufferPool anyObject];
    if (menuItem) {
        [_bufferPool removeObject:menuItem];
    }
    return menuItem;
}

- (JMMenuBarItem *)gainItemSelectState {
    return self.selectItem;
}

#pragma mark - Event
- (void)menuBarItemClickEvent:(JMMenuBarItem *)menuItem {
    if ( !menuItem ) return;
    NSInteger selectTag = menuItem.itemTag - JMMenuPageMenuBarItemBasicTag;
    if (self.menuBarDelegate && [self.menuBarDelegate respondsToSelector:@selector(menuBar:didSelectItemAtIndex:)]) {
        [self.menuBarDelegate menuBar:self didSelectItemAtIndex:selectTag];
    }
}

@end
