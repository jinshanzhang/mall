//
//  JMMenuBar.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMMenuBarItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface JMMenuBar : UIScrollView

/**
 * Add additional scroll area around content. default UIEdgeInsetsZero
 **/
@property (nonatomic) UIEdgeInsets menuBarEdgeInsets;

/**
 * Increase the spacing between the item. default 0.0
 **/
@property (nonatomic, assign) CGFloat menuBarItemSpace;

/**
 * The currently selected item index.
 **/
@property (nonatomic, assign) NSInteger currentItemIndex;

/**
 * Whether you need to skip the layout.
 **/
@property (nonatomic, assign) BOOL needSkipLayout;

/**
 * Whether modify style.
 **/
@property (nonatomic, assign) BOOL titleColorTurnBlack;

/**
 * Whether to use the cache.
 */
@property (nonatomic, assign) BOOL isUseCache;


@property (nonatomic, assign) BOOL rightAddSite;


@property (nonatomic, assign) BOOL isStartAddSpace;

/**
 * Menubar display of data.
 **/
@property (nonatomic, strong) NSMutableArray <id <JMMenuBarItemContentProtocol>> *menuBarContents;

/**
 * Menubar item layout of style.
 **/
@property (nonatomic, assign) JMMenuBarLayoutStyle menuBarLayoutStyle;

/**
 * Select the item Stay type.
 **/
@property (nonatomic, assign) JMMenuBarStayLocation selectItemStayType;


@property (nonatomic, weak) id <JMMenuBarDelegate> menuBarDelegate;

@property (nonatomic, weak) id <JMMenuBarDataSource> menuBarDataSource;

/**
 * The refresh data.
 **/
- (void)reloadData;

/**
 * reset frame for subviews.
 **/
- (void)resetItemFrames;

/**
 * update the item select.
 **/
- (void)updateItemSelectStateAtIndex:(NSInteger)index;

/**
 * To create the item with index.
 */
- (nullable __kindof JMMenuBarItem *)createItemAtIndex:(NSUInteger)index;

/**
 * According to the index for the item.
 **/
- (nullable __kindof JMMenuBarItem *)itemAtIndex:(NSUInteger)index;

/**
 * According to reuse identifier query reusable menubar item.
 */
- (nullable __kindof JMMenuBarItem *)dequeueReusableItemWithIdentifier:(NSString *)identifier;

- (JMMenuBarItem *)gainItemSelectState;
@end

NS_ASSUME_NONNULL_END
