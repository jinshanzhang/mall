//
//  JMMenuContentView.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuContentView.h"

@interface JMMenuContentView()

@property (nonatomic, strong) NSMutableDictionary *visibleControllers; //屏幕可视控制器
@property (nonatomic, strong) NSMutableArray *indexs; //索引集合
@property (nonatomic, strong) NSMutableArray *frames; //控制器frame集合
@property (nonatomic, strong) NSCache *controllerBufferPool; //控制器缓存池

@property (nonatomic, strong) NSString *identifier; //重用标识符
@end

@implementation JMMenuContentView

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _isNeedPreload = YES;
        _visibleControllers = [[NSMutableDictionary alloc] init];
        _indexs = [[NSMutableArray alloc] init];
        _frames = [[NSMutableArray alloc] init];
        
        _controllerBufferPool = [[NSCache alloc] init];
        _controllerBufferPool.name = @"com.jmmenupage.content";
        _controllerBufferPool.countLimit = 15;
    }
#if TARGET_OS_IPHONE
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearMemoryCache) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
#endif
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    CGRect tempFrame = CGRectZero;
    CGFloat offsetX = self.contentOffset.x;
    CGFloat sizeWidth = self.frame.size.width;
    NSInteger currentPage = offsetX/sizeWidth;
    BOOL isNotBorder = 0 != (int)offsetX % (int)sizeWidth;
    if (_currentPage == currentPage && isNotBorder) {
        return;
    }
    UIViewController *viewController = nil;
    NSArray *pathList = [_visibleControllers allKeys];
    for (NSNumber *key in pathList) {
        viewController = [_visibleControllers objectForKey:key];
        tempFrame = [[_frames objectAtIndex:[key integerValue]] CGRectValue];
        if (![self jm_isNeedDisplayWithFrame:tempFrame preloading:_isNeedPreload]) {
            [self moveViewControllerToBufferPool:viewController];
            [_visibleControllers removeObjectForKey:key];
        }
        else {
            viewController.view.frame = tempFrame;
        }
    }
    NSMutableArray *leftIndexList = [_indexs mutableCopy];
    [leftIndexList removeObjectsInArray:pathList]; // 对没有显示的item设置frame
    for (NSNumber *index in leftIndexList) {
        tempFrame = [_frames[[index integerValue]] CGRectValue];
        if ([self jm_isItemNeedDisplayWithFrame:tempFrame]) {
            [self loadViewControllerAtPageIndex:[index integerValue]];
        }
    }
}
#pragma mark - Public
- (void)reloadData {
    [self _resetCacheData];
    [self resetPageFrames];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)resetPageFrames {
    [_frames removeAllObjects];
    CGRect tempFrame = self.bounds;
    for (NSInteger pageIndex = 0; pageIndex < _pageCount; pageIndex ++) {
        tempFrame.origin.x = pageIndex * tempFrame.size.width;
        [_frames addObject:[NSValue valueWithCGRect:tempFrame]];
    }
    self.contentSize = CGSizeMake(CGRectGetMaxX(tempFrame), 0);
    [self setContentOffset:CGPointMake(tempFrame.size.width * _currentPage, 0) animated:NO];
}

- (CGRect)frameOfViewControllerAtPage:(NSUInteger)pageIndex
{
    if (_frames.count <= pageIndex) {
        return CGRectZero;
    }
    return [_frames[pageIndex] CGRectValue];
}

- (UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex {
    return [self viewControllerAtPage:pageIndex autoCreate:NO];
}

- (UIViewController *)creatViewControllerAtPage:(NSUInteger)pageIndex {
    return [self viewControllerAtPage:pageIndex autoCreate:YES];
}

- (UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier {
    _identifier = identifier;
    if ( !_identifier ) { return nil; }
    NSMutableSet *cacheSet = [_controllerBufferPool objectForKey:_identifier];
    UIViewController *viewController = [cacheSet anyObject];
    if (viewController) {
        [cacheSet removeObject:viewController];
        [_controllerBufferPool setObject:cacheSet forKey:_identifier];
    }
    return viewController;
}

#pragma mark - Private
- (UIViewController *)loadViewControllerAtPageIndex:(NSInteger)pageIndex {
    UIViewController *viewController = nil;
    if (self.menuContentViewDataSource && [self.menuContentViewDataSource respondsToSelector:@selector(menuContentView:viewControllerAtPage:)]) {
        viewController = [self.menuContentViewDataSource menuContentView:self viewControllerAtPage:pageIndex];
    }
    if (viewController) {
        viewController.reuseIdentifier = _identifier;
        CGRect ctrlFrame = [[_frames objectAtIndex:pageIndex] CGRectValue];
        viewController.view.frame = ctrlFrame;
        [self addSubview:viewController.view];
        [_visibleControllers setObject:viewController forKey:@(pageIndex)];
    }
    _identifier = nil;
    return viewController;
}

- (UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex autoCreate:(BOOL)autoCreate {
    if (_pageCount <= pageIndex) {
        return nil;
    }
    
    UIViewController *viewController = [_visibleControllers objectForKey:@(pageIndex)];
    if (!viewController && autoCreate) {
        viewController = [self loadViewControllerAtPageIndex:pageIndex];
    }
    return viewController;
}

- (void)moveViewControllerToBufferPool:(UIViewController *)viewController {
    //移除一个 childViewController
    [viewController willMoveToParentViewController:nil];//子控制器被通知即将解除父子关系
    [viewController.view removeFromSuperview];//把子控制器的 view 从到父控制器的 view 上面移除
    [viewController removeFromParentViewController];//真正的解除关系,会自己调用 [vc didMoveToParentViewController:nil]
    
    NSMutableSet *tempBufferPool = [_controllerBufferPool objectForKey:viewController.reuseIdentifier];
    if (!tempBufferPool) {
        tempBufferPool = [[NSMutableSet alloc] init];
    }
    [tempBufferPool addObject:viewController];
    [_controllerBufferPool setObject:tempBufferPool forKey:viewController.reuseIdentifier];
}

- (void)_resetCacheData {
    [_indexs removeAllObjects];
    for (NSInteger index = 0; index < _pageCount; index ++ ) {
        [_indexs addObject:@(index)];
    }
    NSArray *visibleValues = [_visibleControllers allValues];
    for (UIViewController *viewController in visibleValues) {
        [self moveViewControllerToBufferPool:viewController];
    }
    [_visibleControllers removeAllObjects];
}


#pragma mark - Event
- (void)clearMemoryCache {
    [_controllerBufferPool removeAllObjects];
}

@end
