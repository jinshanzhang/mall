//
//  JMMenuBarItem.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface JMMenuBarItem : UIControl

/**
 * item tag value.
 **/
@property (nonatomic, assign) NSUInteger itemTag;

/**
 * item select status.
 */
@property (nonatomic, assign) BOOL itemSelect;


@property (nonatomic, assign) BOOL titleColorTurnBlack;

/**
 * item style configure.
 **/
@property (nonatomic, strong) id <JMMenuBarItemStyleProtocol> itemStyleConfigure;

- (void)isItitemScaleTransform:(BOOL)isScale;

@end


@interface JMMenuBarTitleItem : JMMenuBarItem

/**
 * item title.
 **/
@property (nonatomic, strong) UILabel *titleLabel;

@end


NS_ASSUME_NONNULL_END
