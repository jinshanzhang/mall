//
//  JMMenuBarProtocol.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#ifndef JMMenuBarProtocol_h
#define JMMenuBarProtocol_h
/**
 *  Agreement on the JMMenuBar
 **/

@class JMMenuBarItem, JMMenuBar;
@class JMMenuContentView;
@class JMMenuPageView;

/* (1) The style of the JMMenuBarItem agreement
 * JMMenuBarItem 样式协议
 **/
@protocol JMMenuBarItemStyleProtocol <NSObject>

@optional
- (UIFont *_Nullable)jm_NormalFont;
- (UIFont *_Nullable)jm_SelectFont;
- (UIColor *_Nullable)jm_NormalTitleColor;
- (UIColor *_Nullable)jm_SelectTitleColor;

@end

/**
 * JMMenuBarItem 内容协议
 **/
@protocol JMMenuBarItemContentProtocol <NSObject>

@optional
- (NSString *_Nullable)itemContent;

@end


/**
 * JMMenuBar 数据内容协议
 **/
@protocol JMMenuBarDataSource <NSObject>

@optional
- (JMMenuBarItem *)menuBar:(JMMenuBar *)menuBar menuBarItemAtIndex:(NSInteger)itemIndex;

@end


/**
 * JMMenuBar 用户交互协议
 **/
@protocol JMMenuBarDelegate <NSObject>

@optional
- (CGFloat)menuBar:(JMMenuBar *)menuBar menuBarItemWidthAtIndex:(NSInteger)itemIndex;

- (void)menuBar:(JMMenuBar *)menuBar didSelectItemAtIndex:(NSInteger)itemIndex;

@end

/**
 * JMMenuContentView 内容协议
 **/
@protocol JMMenuContentViewDataSource <NSObject>

@optional
- (nullable UIViewController *)menuContentView:(JMMenuContentView *_Nullable)menuContentView viewControllerAtPage:(NSInteger)pageIndex;

@end

/**
 * JMMenuPageView 内容协议
 **/
@protocol JMMenuPageViewDataSource <NSObject>

@optional
- (nullable JMMenuBarItem *)menuPageView:(JMMenuPageView *_Nullable)menuPageView menuBarItemAtIndex:(NSInteger)itemIndex;

- (nullable UIViewController *)menuPageView:(JMMenuPageView *_Nullable)menuPageView viewControllerAtPage:(NSInteger)pageIndex;

@end

/**
 * JMMenuPageView 交互协议
 **/
@protocol JMMenuPageViewDelegate <NSObject>

@optional
- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView viewDidAppear:(__kindof UIViewController *_Nullable)viewController atPage:(NSUInteger)pageIndex;

- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView viewDidDisappear:(__kindof UIViewController *_Nullable)viewController atPage:(NSUInteger)pageIndex;

- (CGFloat)menuPageView:(JMMenuPageView *_Nullable)menuPageView menuBarItemWidthAtIndex:(NSInteger)itemIndex;

- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView didSelectItemAtIndex:(NSInteger)itemIndex;

- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView scrollViewWillBeginDragging:(UIScrollView *)scrollView;

- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView scrollViewDidScroll:(UIScrollView *)scrollView;

- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

- (void)menuPageView:(JMMenuPageView *_Nullable)menuPageView scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

@end

/**
 *  JMMenuPage 父控制器的协议
 **/
@protocol JMMenuPagePresentProtocol <NSObject>

@optional
- (NSInteger)currentPage;
- (void)setCurrentPage:(NSInteger)currentPage;

- (__kindof UIViewController *_Nullable)currentViewController;
- (void)setCurrentViewController:(UIViewController *_Nullable)viewController;

// 生命周期
- (JMMenuPageAppearanceState)appearanceState;

@end



#endif /* JMMenuBarProtocol_h */
