//
//  JMMenuPageViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuPageViewController.h"

@interface JMMenuPageViewController ()

@end

@implementation JMMenuPageViewController

#pragma mark - Setter

- (JMMenuPageView *)menuPageView {
    if (!_menuPageView) {
        _menuPageView = [[JMMenuPageView alloc] init];
        _menuPageView.menuPageDelegate = self;
        _menuPageView.menuPageDataSource = self;
        _menuPageView.autoresizesSubviews = YES;
        _menuPageView.backgroundColor = XHClearColor;
        [self.view setNeedsLayout];
    }
    return _menuPageView;
}

#pragma mark - Life cycle
- (void)loadView {
    [super loadView];
    self.view = self.menuPageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_hideNavigation:YES];
    self.view.backgroundColor = XHClearColor;
}

@end
