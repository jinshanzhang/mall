//
//  NSString+JMExtension.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "NSString+JMExtension.h"

@implementation NSString (JMExtension)

+ (CGSize)sizeWithTitle:(NSString *)title
                   font:(UIFont *)font {
    CGSize size = CGSizeZero;
    if ([title respondsToSelector:@selector(sizeWithAttributes:)]) {
        size = [title sizeWithAttributes:@{ NSFontAttributeName : font}];
    }
    else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        size = [title sizeWithFont:font];
#pragma clang diagnostic pop
    }
    return size;
}

@end
