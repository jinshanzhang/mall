//
//  UIViewController+JMExtension.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (JMExtension)

/**
 * The reuse identifier of the controller.
 **/
@property (nonatomic, copy) NSString *reuseIdentifier;

@end

NS_ASSUME_NONNULL_END
