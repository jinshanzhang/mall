//
//  UIViewController+JMExtension.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "UIViewController+JMExtension.h"
#import <objc/runtime.h>
static const void *kJMMenuControllerReuseIdentifier = &kJMMenuControllerReuseIdentifier;

@implementation UIViewController (JMExtension)

- (void)setReuseIdentifier:(NSString *)reuseIdentifier {
    objc_setAssociatedObject(self, kJMMenuControllerReuseIdentifier, reuseIdentifier, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)reuseIdentifier {
    return objc_getAssociatedObject(self, kJMMenuControllerReuseIdentifier);
}

@end
