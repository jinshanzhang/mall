//
//  UIScrollView+JMExtension.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (JMExtension)

// 判断menuItem的frame是否需要显示在菜单栏上
- (BOOL)jm_isItemNeedDisplayWithFrame:(CGRect)frame;

// 判断指定的frame是否在滚动视图的可视范围内
- (BOOL)jm_isNeedDisplayWithFrame:(CGRect)frame preloading:(BOOL)preloading;

@end

NS_ASSUME_NONNULL_END
