//
//  NSString+JMExtension.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (JMExtension)

/**
 * 得到某段文本的size
 **/
+ (CGSize)sizeWithTitle:(NSString *)title
                   font:(UIFont *)font;

@end

NS_ASSUME_NONNULL_END
