//
//  UIScrollView+JMExtension.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2019/1/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "UIScrollView+JMExtension.h"

@implementation UIScrollView (JMExtension)

- (BOOL)jm_isNeedDisplayWithFrame:(CGRect)frame preloading:(BOOL)preloading {
    CGRect visibleRect = (CGRect){CGPointMake(self.contentOffset.x, 0), self.frame.size}; //可视范围定义为整个滚动视图的frame
    CGRect intersectRegion = CGRectIntersection(frame, visibleRect); // 是否存在交集
    BOOL isOnScreen =  !CGRectIsNull(intersectRegion) || !CGRectIsEmpty(intersectRegion); //是否在可视范围上。
    if (!preloading) { // 是否预加载
        BOOL isNotBorder = 0 != (int)self.contentOffset.x%(int)self.frame.size.width;
        return isOnScreen && (isNotBorder ?: 0 != intersectRegion.size.width);
    }
    return isOnScreen;
}

- (BOOL)jm_isItemNeedDisplayWithFrame:(CGRect)frame {
    frame.size.width *= 2;
    BOOL isOnScreen = [self jm_isNeedDisplayWithFrame:frame preloading:YES];
    if (isOnScreen) {
        return YES;
    }
    
    frame.size.width *= 0.5;
    frame.origin.x -= frame.size.width;
    isOnScreen = [self jm_isNeedDisplayWithFrame:frame preloading:YES];
    return isOnScreen;
}

@end
