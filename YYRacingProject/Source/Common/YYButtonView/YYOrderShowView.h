//
//  YYOrderShowView.h
//  YIYanProject
//
//  Created by cjm on 2018/5/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYOrderShowView : UIView

@property (nonatomic, strong) NSString *topTitle;
@property (nonatomic, strong) NSString *bottomTitle;

@property (nonatomic, strong) NSMutableAttributedString *topAttributed;
@property (nonatomic, strong) NSMutableAttributedString *bottomAttributed;

@property (nonatomic, assign) CGFloat  topSpace;
@property (nonatomic, assign) CGFloat  itemSpace;


@end
