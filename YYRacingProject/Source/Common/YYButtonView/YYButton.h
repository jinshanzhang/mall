//
//  YYButton.h
//  YIYanProject
//
//  Created by cjm on 2018/4/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYButton : UIView

@property (nonatomic, copy) NSString *normalImageName;
@property (nonatomic, copy) NSString *selectImageName;

@property (nonatomic, copy) NSString *normalTitle;
@property (nonatomic, copy) NSString *selectTitle;

@property (nonatomic, copy) NSMutableAttributedString *normalAttibuteTitle;
@property (nonatomic, copy) NSMutableAttributedString *selectAttibuteTitle;

@property (nonatomic, copy) UIFont *titleFont;
@property (nonatomic, strong) UIColor *titleTextColor;

@property (nonatomic, assign) BOOL   select;
@property (nonatomic, assign) BOOL   isHorizontalLayout;

@property (nonatomic, assign) CGFloat space;  //默认是10
@property (nonatomic, assign) CGFloat IMGspace;  //默认是10


@property (nonatomic, copy) void (^btnClickBlock)(BOOL status);
@property (nonatomic, copy) void (^btnClickEventBlock)(BOOL status, NSInteger btnTag);

@end
