//
//  YYButton.m
//  YIYanProject
//
//  Created by cjm on 2018/4/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYButton.h"

@interface YYButton()

@property (nonatomic, strong) UILabel *titleLable;
@property (nonatomic, strong) UIImageView *buttonImage;

@end

@implementation YYButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.space = 10.0;
        self.IMGspace = -8;
        self.isHorizontalLayout = NO;
        self.titleLable = [YYCreateTools createLabel:nil
                                                font:midFont(11)
                                           textColor:XHBlackColor];
        self.titleLable.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLable];
        
        self.buttonImage = [YYCreateTools createImageView:nil
                                                viewModel:-1];
        [self addSubview:self.buttonImage];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
          action:@selector(buttonClickEvent:)]];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //是否水平布局
    if (self.isHorizontalLayout) {
        [self.buttonImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self).offset(self.space);
        }];
        [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.buttonImage.mas_right).offset(self.IMGspace);
        }];
    }
    else {
        [self.buttonImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self).offset(self.IMGspace);
        }];
        
        [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.buttonImage.mas_bottom).offset(self.space);
        }];
    }
}

- (void)setSpace:(CGFloat)space {
    _space = space;
    if (self.isHorizontalLayout) {
        [self.buttonImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self).offset(self.space);
        }];
        [self.titleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.buttonImage.mas_right).offset(self.IMGspace);
        }];
    }
    else {
        [self.titleLable mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.equalTo(self.buttonImage.mas_bottom).offset(self.space);
        }];
        
        [self.buttonImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self).offset(self.IMGspace);
        }];
    }
}

- (void)setSelect:(BOOL)select {
    _select = select;
    if (_select) {
        self.titleLable.text = self.selectTitle;
        if (!kValidString(self.selectImageName)) {
            return;
        }
        if (self.selectAttibuteTitle) {
            self.titleLable.attributedText = self.selectAttibuteTitle;
        }
        [self.buttonImage setImage:[UIImage imageNamed:self.selectImageName]];
    }
    else {
        self.titleLable.text = self.normalTitle;
        if (!kValidString(self.normalImageName)) {
            return;
        }
        if (self.normalAttibuteTitle) {
            self.titleLable.attributedText = self.normalAttibuteTitle;
        }
        [self.buttonImage setImage:[UIImage imageNamed:self.normalImageName]];
    }
}

- (void)buttonClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.btnClickBlock) {
        self.btnClickBlock(self.select);
    }
    if (self.btnClickEventBlock) {
        self.btnClickEventBlock(self.select, self.tag);
    }
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFont = titleFont;
    self.titleLable.font = _titleFont;
}

- (void)setTitleTextColor:(UIColor *)titleTextColor {
    _titleTextColor = titleTextColor;
    self.titleLable.textColor = _titleTextColor;
}

- (void)setNormalAttibuteTitle:(NSMutableAttributedString *)normalAttibuteTitle {
    _normalAttibuteTitle = normalAttibuteTitle;
    self.titleLable.attributedText = _normalAttibuteTitle;
}

- (void)setSelectAttibuteTitle:(NSMutableAttributedString *)selectAttibuteTitle {
    _selectAttibuteTitle = selectAttibuteTitle;
    self.titleLable.attributedText = _selectAttibuteTitle;
}

@end
