//
//  YYLineHeaderView.h
//  YIYanProject
//
//  Created by cjm on 2018/5/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYLineHeaderView : UIView

@property (nonatomic, strong) NSString *title;

@end
