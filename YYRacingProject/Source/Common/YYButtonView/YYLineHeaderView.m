//
//  YYLineHeaderView.m
//  YIYanProject
//
//  Created by cjm on 2018/5/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYLineHeaderView.h"

@interface YYLineHeaderView()

@property (nonatomic, strong) UIView *redView;
@property (nonatomic, strong) UILabel *headerLabel;

@end

@implementation YYLineHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.redView = [YYCreateTools createView:XHRedColor];
    self.redView.layer.cornerRadius = 1.5;
    [self addSubview:self.redView];
    
    self.headerLabel = [YYCreateTools createLabel:nil
                                             font:normalFont(15)
                                        textColor:XHBlackColor];
    [self addSubview:self.headerLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.redView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(2, self.height-2));
    }];
    
    [self.headerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.redView);
        make.left.equalTo(self.redView.mas_right).offset(7);
    }];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.headerLabel.text = _title;
}

@end
