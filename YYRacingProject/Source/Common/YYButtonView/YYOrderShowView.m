//
//  YYOrderShowView.m
//  YIYanProject
//
//  Created by cjm on 2018/5/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYOrderShowView.h"

@interface YYOrderShowView()

@property (nonatomic, strong) YYLabel *topLabel;  //上面label
@property (nonatomic, strong) YYLabel *bottomLabel;

@end

@implementation YYOrderShowView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.topSpace = 5;
        self.itemSpace = self.topSpace;
        
        self.topLabel = [[YYLabel alloc] init];
        self.topLabel.font = midFont(15);
        self.topLabel.textColor = XHBlackColor;
        self.topLabel.numberOfLines = 1;
        self.topLabel.preferredMaxLayoutWidth = kSizeScale(150);
        self.topLabel.textAlignment = NSTextAlignmentCenter;
    
        [self addSubview:self.topLabel];
        
        self.bottomLabel = [[YYLabel alloc] init];
        self.bottomLabel.font = midFont(13);
        self.bottomLabel.textColor = XHBlackLitColor;
        self.bottomLabel.numberOfLines = 1;
        self.bottomLabel.preferredMaxLayoutWidth = kSizeScale(150);
        self.bottomLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.bottomLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.left.right.equalTo(self);
        make.top.mas_equalTo(self.topSpace);
    }];
    
    [self.bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.left.right.equalTo(self);
        make.top.equalTo(self.topLabel.mas_bottom).offset(self.itemSpace);
    }];
}

- (void)setTopTitle:(NSString *)topTitle {
    _topTitle = topTitle;
    self.topLabel.text = _topTitle;
}

- (void)setBottomTitle:(NSString *)bottomTitle {
    _bottomTitle = bottomTitle;
    self.bottomLabel.text = _bottomTitle;
}

- (void)setTopAttributed:(NSMutableAttributedString *)topAttributed {
    _topAttributed = topAttributed;
    self.topLabel.attributedText = _topAttributed;
}

- (void)setBottomAttributed:(NSMutableAttributedString *)bottomAttributed {
    _bottomAttributed = bottomAttributed;
    self.bottomLabel.attributedText = _bottomAttributed;
}

@end
