//
//  YYPopView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static  CGFloat   const animationDuration       = 0.25;
static  CGFloat   const popViewInsert           = 5;

@interface YYPopView : UIView

@property (nonatomic, strong) UIView *popContainerView; //包含要显示的View的父控件

//popView的移除回调
@property (nonatomic ,copy) void(^willRemovedFromeSuperView)(void);
@property (nonatomic ,copy) void(^didRemovedFromeSuperView)(void);
/*
 右上角微型菜单弹窗
 @param contentView 要显示的控件内容
 @param onView 一般是响应事件的按钮
 **/
+ (instancetype)popUpContentView:(UIView *)contentView
                          onView:(UIView *)onView;

//隐藏当前的popView
+ (void)hidenPopView;

@end

NS_ASSUME_NONNULL_END
