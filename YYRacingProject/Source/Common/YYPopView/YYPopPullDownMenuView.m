//
//  YYPopPullDownMenuView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPopPullDownMenuView.h"

@interface PopPullDownMenvTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel  *showContentLbl;
@property (nonatomic, strong) UIView   *lineView;
@property (nonatomic, assign) BOOL     isHideLine;
@property (nonatomic, copy)  void (^menuItemBlock)(PopPullDownMenvTableViewCell *currentCell);

@end

@implementation PopPullDownMenvTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.lineView = [YYCreateTools createView:HexRGB(0xf1f1f1)];
        self.lineView.hidden = YES;
        [self.contentView addSubview:self.lineView];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
            make.centerX.equalTo(self.contentView);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(55), kSizeScale(1)));
        }];
        
        self.showContentLbl = [YYCreateTools createLabel:nil
                                                    font:midFont(14)
                                               textColor:XHBlackMidColor];
        self.showContentLbl.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.showContentLbl];
        
        [self.showContentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.centerX.equalTo(self.contentView);
        }];
        
        [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellClickEvent:)]];
    }
    return self;
}

- (void)setIsHideLine:(BOOL)isHideLine {
    _isHideLine = isHideLine;
    self.lineView.hidden = _isHideLine;
}

- (void)cellClickEvent:(UITapGestureRecognizer *)gesture {
    if (self.menuItemBlock) {
        self.menuItemBlock(self);
    }
}

@end

@interface YYPopPullDownMenuView()
<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *menuTitles;
@property (nonatomic, strong) UIImageView  *bgImageView;
@property (nonatomic, strong) UITableView  *m_tableView;

@end

@implementation YYPopPullDownMenuView

#pragma mark - Setter method

- (UITableView *)m_tableView {
    if (!_m_tableView) {
        _m_tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                    style:UITableViewStylePlain];
        _m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _m_tableView.allowsSelection = NO;
        _m_tableView.delegate = self;
        _m_tableView.dataSource = self;
        _m_tableView.scrollEnabled = NO;
    }
    return _m_tableView;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, kSizeScale(94), kSizeScale(99));
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.menuTitles = [@[@"社群订单",@"只看本店"] mutableCopy];
    
    self.bgImageView = [YYCreateTools createImageView:@"down_arrow_icon"
                                            viewModel:-1];
    [self addSubview:self.bgImageView];
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.bgImageView addSubview:self.m_tableView];
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(1);
        make.top.mas_equalTo(10);
        make.bottom.right.mas_equalTo(-1);
    }];
    Class cls = [PopPullDownMenvTableViewCell class];
    registerClass(self.m_tableView, cls);
    self.m_tableView.rowHeight = 44.0;
    [self.m_tableView reloadData];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @weakify(self);
    NSInteger row = indexPath.row;
    Class cls = [PopPullDownMenvTableViewCell class];
    PopPullDownMenvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
    if (row < self.menuTitles.count && kValidArray(self.menuTitles)) {
        cell.showContentLbl.text = self.menuTitles[row];
        cell.isHideLine = (row != self.menuTitles.count-1)?YES:NO;
    }
    cell.menuItemBlock = ^(PopPullDownMenvTableViewCell *currentCell) {
        @strongify(self);
        NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:currentCell];
        if (self.menuClickBlock) {
            self.menuClickBlock(currentIndex.row);
        }
    };
    return cell;
}
@end
