//
//  YYPopPullDownMenuView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYPopPullDownMenuView : UIView

@property (nonatomic, copy) void (^menuClickBlock)(NSInteger menuItemIndex);

@end

NS_ASSUME_NONNULL_END
