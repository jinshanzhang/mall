//
//  YYPopView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPopView.h"
@interface YYPopView ()
<CAAnimationDelegate,
UIGestureRecognizerDelegate>

@property (nonatomic, weak) UIView  *contentView;//内容视图
@property (nonatomic, weak) UIView  *onView;     //参考视图（点击的某个视图）

@property (nonatomic, strong) UIView  *triangleView; //指示器视图
@property (nonatomic, assign) CGFloat  triangleOffSet;

@property (nonatomic, strong) UIResponder *control;

@property (nonatomic, assign) BOOL animation;
@property (nonatomic, strong) CABasicAnimation *showAnimation;
@property (nonatomic, strong) CABasicAnimation *hideAnimation;

@end

@implementation YYPopView

static  NSInteger const popViewTag              = 364;
+ (instancetype)getCurrentPopView {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    YYPopView *oldPopView = (YYPopView *)[window viewWithTag:popViewTag];
    return oldPopView;
}

#pragma mark - Life cycle

- (UIView *)popContainerView{
    if (_popContainerView == nil) {
        _popContainerView = [[UIView alloc] init];
        [self addSubview:_popContainerView];
    }
    return _popContainerView;
}

- (instancetype)initWithFrame:(CGRect)frame
                       onView:(UIView *)onView
                  contentView:(UIView *)contentView
               triangleOffSet:(CGFloat)triangleOffSet
                 triangleView:(UIView *)triangleView
                    animation:(BOOL)animation {
    self = [super initWithFrame:frame];
    if (self) {
        self.onView = onView;
        self.tag = popViewTag;
        self.contentView = contentView;
        self.triangleOffSet = triangleOffSet;
        self.triangleView = triangleView ;
        self.animation = animation;
        
        UIControl *control = [[UIControl alloc] initWithFrame:self.bounds];
        [self addSubview:control];
        self.control = control;
        [control addTarget:self action:@selector(popClickEvent) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

#pragma mark - Public method

+ (instancetype)popUpContentView:(UIView *)contentView
                          onView:(UIView *)onView {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    YYPopView *oldPopView = [self getCurrentPopView];
    YYPopView *newPopView = [[YYPopView alloc] initWithFrame:window.bounds
                                                      onView:onView
                                                 contentView:contentView
                                              triangleOffSet:5
                                                triangleView:nil
                                                   animation:YES];
    [window addSubview:newPopView];
    newPopView.showAnimation = [newPopView showPopView:contentView];
    newPopView.hideAnimation = [newPopView hidePopView:contentView];
    
    [newPopView setPopMenuSubViewFrame];
    [newPopView animationPopContainerViewWithOldPopView:oldPopView];
    [newPopView bringSubviewToFront:newPopView.popContainerView];
    
    return newPopView;
}

+ (void)hidenPopView{
    UIView *superView = [UIApplication sharedApplication].keyWindow;
    YYPopView *popView = (YYPopView *)[superView viewWithTag:popViewTag];
    if (popView && ![popView.popContainerView.layer animationForKey:@"hiddenAnimation"]) {
        if (popView.willRemovedFromeSuperView) {
            popView.willRemovedFromeSuperView();
        }
        if (popView.animation && popView.hideAnimation) {
            popView.hideAnimation.removedOnCompletion = NO;
            [popView.popContainerView.layer addAnimation:popView.hideAnimation forKey:@"hiddenAnimation"];
            [popView animationBackGroundColor:popView.backgroundColor toColor:[UIColor clearColor]];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(popView.hideAnimation.duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [popView removeFromSuperview];
            });
        }else{
            [popView removeFromSuperview];
        }
    }
}

#pragma mark - Private method
/**
 *  pop 显示
 */
- (CABasicAnimation *)showPopView:(UIView *)popView {
    
    CABasicAnimation *showAnima = [CABasicAnimation animation];
    showAnima.duration = animationDuration;
    showAnima.repeatCount = 1;
    showAnima.fillMode = kCAFillModeForwards;
    showAnima.removedOnCompletion = YES;
    
    showAnima.keyPath = @"transform";
    CATransform3D tofrom = CATransform3DMakeScale(1, 1, 1);
    CATransform3D from = CATransform3DMakeScale(0, 0, 1);
    showAnima.fromValue = [NSValue valueWithCATransform3D:from];
    showAnima.toValue =  [NSValue valueWithCATransform3D:tofrom];
    
    return showAnima;
}

/**
 *  pop 隐藏
 */
- (CABasicAnimation *)hidePopView:(UIView *)popView {
    
    CABasicAnimation *hidenAnima = [CABasicAnimation animation];
    hidenAnima.duration = animationDuration;
    hidenAnima.fillMode = kCAFillModeForwards;
    hidenAnima.removedOnCompletion = YES;
    hidenAnima.repeatCount = 1;
    
    hidenAnima.keyPath = @"transform";
    CATransform3D tofrom = CATransform3DMakeScale(0, 0, 1);
    CATransform3D from = CATransform3DMakeScale(1, 1, 1);
    hidenAnima.fromValue = [NSValue valueWithCATransform3D:from];
    hidenAnima.toValue =  [NSValue valueWithCATransform3D:tofrom];
    
    return hidenAnima;
}

- (void)animationPopContainerViewWithOldPopView:(YYPopView *)oldPopView{
    if (oldPopView) {
        if (oldPopView.willRemovedFromeSuperView) {
            oldPopView.willRemovedFromeSuperView();
        }
        [oldPopView removeFromSuperview];
    }
    UIColor *color = self.backgroundColor;
    [self.popContainerView.layer addAnimation:self.showAnimation forKey:nil];
    [self animationBackGroundColor:[UIColor clearColor] toColor:color];
}

- (void)animationBackGroundColor:(UIColor*)fromeColor toColor:(UIColor *)toColor{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    animation.fromValue = (id)fromeColor.CGColor;
    animation.toValue = (id)toColor.CGColor;
    animation.duration = animationDuration;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.layer addAnimation:animation forKey:@"backgroundColor"];
}

- (void)setPopMenuSubViewFrame {
    CGRect contentFrame = self.contentView.bounds;
    CGRect popContentFrame = CGRectZero;
    CGRect onViewFrame = [self.onView convertRect:self.onView.bounds toView:nil];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    //1.2、计算内容在window的位置
    contentFrame.origin.y = CGRectGetMaxY(onViewFrame) + self.triangleOffSet;
    contentFrame.origin.x = onViewFrame.origin.x + onViewFrame.size.width/2 - contentFrame.size.width/2 + self.triangleOffSet;
    if (contentFrame.origin.x < popViewInsert) {
        contentFrame.origin.x = popViewInsert;
    }
    if (CGRectGetMaxX(contentFrame) > window.bounds.size.width) {
        contentFrame.origin.x = window.bounds.size.width - popViewInsert - contentFrame.size.width;
    }
    popContentFrame = contentFrame;
    
    self.popContainerView.frame = popContentFrame;

    //2、计算内容实际的位置
    self.contentView.frame = contentFrame;
    [window addSubview:self.contentView];
    self.contentView.frame = [self.contentView convertRect:self.contentView.bounds toView:self.popContainerView];
    [self.popContainerView addSubview:self.contentView];
}

- (void)removeFromSuperview{
    if (self.didRemovedFromeSuperView) {
        self.didRemovedFromeSuperView();
    }
    [super removeFromSuperview];
}

#pragma mark - Event
- (void)popClickEvent {
    [YYPopView hidenPopView];
}


@end
