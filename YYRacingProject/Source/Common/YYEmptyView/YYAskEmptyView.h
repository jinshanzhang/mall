//
//  YYAskEmptyView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/28.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <LYEmptyView/LYEmptyView.h>
NS_ASSUME_NONNULL_BEGIN

@interface YYAskEmptyView : LYEmptyView

+ (instancetype)yyEmptyView:(YYEmptyViewType)emptyType
                      title:(NSString *)title
                     target:(id)target
                     action:(SEL)action;

@end

NS_ASSUME_NONNULL_END
