//
//  YYEmptyView.h
//  YIYanProject
//
//  Created by cjm on 2018/4/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <LYEmptyView/LYEmptyView.h>

@interface YYEmptyView : LYEmptyView

+ (instancetype)yyEmptyView:(YYEmptyViewType)emptyType
                      title:(NSString *)title
                     target:(id)target
                     action:(SEL)action;

+ (instancetype)yyEmptyView:(YYEmptyViewType)emptyType
                      title:(NSString *)title
                     target:(id)target
                     action:(SEL)action
                   contentY:(CGFloat)contentY;
@end
