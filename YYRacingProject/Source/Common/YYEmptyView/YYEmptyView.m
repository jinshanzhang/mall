//
//  YYEmptyView.m
//  YIYanProject
//
//  Created by cjm on 2018/4/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYEmptyView.h"

@interface YYEmptyView()


@end

@implementation YYEmptyView

+ (instancetype)yyEmptyView:(YYEmptyViewType)emptyType
                      title:(NSString *)title
                     target:(id)target
                     action:(SEL)action {

    NSString *btnTitle = nil;
    NSString *imageName = @"empty_shopping_cart_icon";
    NSString *detailTitle = @"啊噢，暂时没有数据哦~";
    if (emptyType == YYEmptyViewNoDataType){
        //无数据
        detailTitle = kValidString(title)? title : detailTitle;;
        btnTitle = @"刷新试试";
    }
    else if (emptyType == YYEmptyViewAlxeNoDataType) {
        btnTitle = nil;
    }
    else if (emptyType == YYEmptyViwStrollNoDataType) {
        //无数据，且逛逛
        imageName = @"noCollect_icon";
        detailTitle = kValidString(title)? title : detailTitle;;
        btnTitle = @"继续逛逛";
    }
    else if (emptyType == YYEmptyViewNoOder){
        imageName = @"empty_orderlist_icon";
        detailTitle = @"您还没有订单哦";
        btnTitle = @"继续逛逛";
    }
    else if (emptyType == YYEmptyViewOverTimeType) {
        imageName = @"empty_network_error_icon";
        detailTitle = @"网络连接超时，请刷新试试~";
        btnTitle = @"点击刷新";
    }
    else if (emptyType == YYEmptyViewNoNetworkType) {
        imageName = @"empty_nonetwork_icon";
        detailTitle = @"网络未连接，请检查网络设置";
        btnTitle = @"前往设置";
    }
    else if (emptyType == YYEmptyViewOrderNoDataType) {
        imageName = @"empty_orderlist_icon";
        detailTitle = kValidString(title)? title : @"您目前没有相关订单";
        btnTitle = @"随便逛逛";
    }else if (emptyType == YYEmptyViewCourseNoDataType){
        imageName = @"empty_orderlist_icon";
        detailTitle = kValidString(title)? title : @"您目前没有相关订单";
    }
    else if (emptyType == YYEmptyViewNoDraw) {
        imageName = @"empty_orderlist_icon";
        detailTitle = @"您目前没有提现记录";
        btnTitle = @"";
    }else if (emptyType == YYEmptyViewNoBalance) {
        imageName = @"empty_orderlist_icon";
        detailTitle = @"您目前没有余额记录";
        btnTitle = @"";
    }
    else if (emptyType == YYEmptyViewCouponNoDataType) {
        imageName = @"empty_coupon_icon";
        detailTitle = @"您暂时还没有优惠券哦～";
    }
    else if (emptyType == YYEmptyViewUnderCarringType) {
        imageName = @"good_under_carriage_icon";
        detailTitle = @"该商品暂不可购买或已下架...";
        btnTitle = @"随便逛逛";
    }
    else if (emptyType == YYEmptyViewNetworkErrorType) {
        imageName = @"empty_network_error_icon";
        detailTitle = @"网络异常，请刷新重试";
        btnTitle = @"刷新";
    } else if (emptyType == YYEmptyViewCarts) {
        imageName = @"empty_shopping_cart_icon";
        detailTitle = @"购物车空空的哦,去看看心仪的商品吧~";
        btnTitle = @"去购物";
    }else if(emptyType == YYEmptyViewAddress){
        imageName = @"empty_address_icon";
        detailTitle = @"您还没有收货地址哦~";
        btnTitle = @"";
    }else if (emptyType == YYEmptyViewNoOrderCoupon){
        imageName = @"";
        detailTitle = @"暂无优惠券哦~";
        btnTitle = @"";
    }
    else {
        detailTitle = kValidString(title)? title : detailTitle;
        btnTitle = @"刷新";
    }
    YYEmptyView *emptyView = [YYEmptyView emptyActionViewWithImageStr:imageName
                                                             titleStr:nil
                                                            detailStr:detailTitle
                                                          btnTitleStr:btnTitle
                                                               target:target
                                                               action:action];
    //UIImage *bgImg = [UIImage gradientColorImageFromColors:@[HexRGB(0xfe5c37), XHRedColor] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(100, 30)];

    emptyView.contentViewY = kSizeScale(100);
    
    emptyView.detailLabFont = midFont(12);
    emptyView.detailLabTextColor = XHBlackLitColor;
    
    emptyView.actionBtnFont = boldFont(14);
    emptyView.actionBtnTitleColor = XHWhiteColor;
    emptyView.actionBtnHeight = 30.f;
    emptyView.actionBtnHorizontalMargin = 20.f;
    emptyView.actionBtnCornerRadius = 2.f;
    emptyView.actionBtnBackGroundColor = XHLoginColor;
    
    return emptyView;
}

+ (instancetype)yyEmptyView:(YYEmptyViewType)emptyType
                      title:(NSString *)title
                     target:(id)target
                     action:(SEL)action
                   contentY:(CGFloat)contentY {
    YYEmptyView *empty = [YYEmptyView yyEmptyView:emptyType
                                             title:title
                                            target:target
                                            action:action];
    empty.contentViewY = contentY;
    return empty;
}


- (void)prepare{
    [super prepare];
    //self.autoShowEmptyView = NO;
}

@end
