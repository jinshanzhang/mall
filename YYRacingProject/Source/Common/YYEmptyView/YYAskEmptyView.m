//
//  YYAskEmptyView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/28.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYAskEmptyView.h"

@implementation YYAskEmptyView

+ (instancetype)yyEmptyView:(YYEmptyViewType)emptyType
                      title:(NSString *)title
                     target:(id)target
                     action:(SEL)action {
    NSString *imageName = nil, *detailTitle = nil, *btnTitle = nil;
    if (emptyType == YYEmptyViewGoodDetailCsdNoDataType) {
        imageName = @"csd_nodata_icon";
        detailTitle = kValidString(title)?title:@"暂时还没有素材哦~";
        btnTitle = @"求素材";
    }
    YYEmptyView *emptyView = [YYEmptyView emptyActionViewWithImageStr:imageName
                                                             titleStr:nil
                                                            detailStr:detailTitle
                                                          btnTitleStr:btnTitle
                                                               target:target
                                                               action:action];
    emptyView.contentViewY = kSizeScale(160);
    
    emptyView.detailLabFont = midFont(12);
    emptyView.detailLabTextColor = XHBlackLitColor;
    
    emptyView.actionBtnFont = boldFont(14);
    emptyView.actionBtnTitleColor = XHWhiteColor;
    emptyView.actionBtnHeight = 30.f;
    emptyView.actionBtnHorizontalMargin = 20.f;
    emptyView.actionBtnCornerRadius = 2.f;
    emptyView.actionBtnBackGroundColor = XHRedColor;
    
    return (YYAskEmptyView *)emptyView;
}

- (void)prepare {
    [super prepare];
}

@end
