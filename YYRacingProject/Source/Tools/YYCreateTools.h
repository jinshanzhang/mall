//
//  YYCreateTools.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYCreateTools : NSObject

#pragma mark -UIButton
+ (UIButton *)createBtn:(NSString *)title;
+ (UIButton *)createBtnImage:(NSString *)imageName;

+ (UIButton *)createBtn:(NSString *)title
                   font:(UIFont *)font
              textColor:(UIColor *)textColor;

+ (UIButton *)createButton:(NSString *)title
                   bgColor:(UIColor *)bgColor
                 textColor:(UIColor *)textColor;

#pragma mark -UILabel
+ (UILabel *)createLabel:(NSString *)text
                  font:(UIFont *)font
             textColor:(UIColor *)textColor;

+ (YYLabel *)createLabel:(NSString *)text
                    font:(UIFont *)font
               textColor:(UIColor *)textColor
                maxWidth:(CGFloat)maxWidth
           fixLineHeight:(CGFloat)fixLineHeight;

#pragma mark -UIImageView
+ (UIImageView *)createImageView:(NSString *)imageName
                       viewModel:(NSInteger)viewModel;


#pragma mark -UIView
+ (UIView *)createView:(UIColor *)color;

#pragma mark -UITextField
+ (UITextField *)createTextField:(NSString *)text
                            font:(UIFont *)font
                        texColor:(UIColor *)textColor;

@end
