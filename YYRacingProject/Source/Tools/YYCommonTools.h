//
//  YYCommonTools.h
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YYBaseViewController.h"
#import "PreOrderModel.h"
#import "VersionModel.h"
#import <CoreLocation/CoreLocation.h>

@interface YYCommonTools : NSObject 
WMSingletonH(YYCommonTools);

typedef void(^payBlock)(NSString *result);

//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC;

+(void)pushToLogin;

/**tabbar 切换**/
+ (void)tabbarSelectIndex:(NSInteger)index;
/**获取当前的毫秒**/
+ (NSString *)currentTimeMsec;
/**获取内容大小**/
+ (CGSize)sizeWithText:(id)text
                  font:(UIFont *)font
               maxSize:(CGSize)maxSize;
/**发送验证码**/
+ (void)sendCodeMethod:(NSString *)phone
               codeBtn:(UIButton *)codeBtn
            phoneBlock:(void (^)(NSString *))phoneBlock;
/**特殊字符处理**/
+ (NSMutableAttributedString *)containSpecialSymbolHandler:(NSString *)symbolContent
                                                symbolFont:(UIFont *)symbolFont
                                           symbolTextColor:(UIColor *)symbolTextColor
                                                 wordSpace:(NSInteger)wordSpace
                                                     price:(NSString *)price
                                                 priceFont:(UIFont *)priceFont
                                            priceTextColor:(UIColor *)textColor
                                             symbolOffsetY:(CGFloat)symbolOffsetY;

/**是否将关键字添加到左边**/
+ (NSMutableAttributedString *)containSpecialSymbolHandler:(NSString *)symbolContent
                                                symbolFont:(UIFont *)symbolFont
                                           symbolTextColor:(UIColor *)symbolTextColor
                                                 wordSpace:(NSInteger)wordSpace
                                           symbolIsAddLeft:(BOOL)symbolIsAddLeft
                                                     price:(NSString *)price
                                                 priceFont:(UIFont *)priceFont
                                            priceTextColor:(UIColor *)textColor
                                             symbolOffsetY:(CGFloat)symbolOffsetY;
/**复制文案信息操作**/
+ (void)pasteboardCopy:(NSString *)text
          appendParams:(NSDictionary *)appendParams;

/**手机号码验证**/
+ (BOOL)valiMobile:(NSString *)mobile;

+(BOOL)isValidWithIdentityNum:(NSString *)IdentityNum;

+ (void)savePhotoToThumb:(id)photoObject
               isSkipTip:(BOOL)isSkipTip;
/**保存图片**/
+ (void)saveImageToAlbum:(id)photoObject
               isSkipTip:(BOOL)isSkipTip;

/**商品分享操作**/
+ (void)goodShareOperator:(id)goodModel
             expandParams:(NSMutableDictionary *)params;

/**弹出分享组件**/
+ (void)skipShareComponent:(YYShareCombineInfoModel *)shareModel
           isNeedComposite:(BOOL)isNeedComposite
             expandContent:(NSMutableArray *)expandArray;

/**图片下载操作**/
+ (BOOL)downLoadImages:(NSMutableArray *)imageUrls
          photoPreName:(NSString *)name;

/**临时处理一下ios 12 wkwebview 首次加载userAgent 获取不到**/
+ (void)startSetWKWebViewUserAgent;

/**错误码提示**/
+ (void)errorMessageTipHandler:(NSInteger)code;

/**错误页展示**/
+ (void)emptyShowOperator:(YTKBaseRequest *)request
               scrollView:(UIScrollView *)scrollView
                   method:(SEL)method;
/**错误页处理**/
+ (void)emptyPageHandler:(YYEmptyViewType)type
                operator:(void (^)(NSInteger))operatorBlock;
/**弹提示**/
+ (void)showTipMessage:(NSString *)tip;
/**处理优惠券过期提醒操作**/
+ (BOOL)handlerCouponOvertimeTipOperator:(NSString *)timeString
                           lastTipStatus:(BOOL)lastTipStatus;
/**查询订单状态**/
+ (NSString *)showQueryOrderStatus:(NSNumber *)orderStatus;

/**根据订单里的操作字符串返回展示btn 的内容**/
+ (NSMutableArray *)getOrderShowOperatorBtns:(NSNumber *)operatorStr;

/**根据订单详情里的sku的售后操作字符串返回展示btn 的内容**/
+ (NSMutableArray *)getGoodAfterSaleShowOperatorBtns:(NSNumber *)operatorStr;

/**根据售后详情里的操作字符串返回展示btn 的内容**/
+ (NSMutableArray *)getAfterSaleShowOperatorBtns:(NSNumber *)operatorStr;

/**将单个数据组合为成对**/
+ (NSMutableArray *)transDataToTwoColwn:(NSArray *)array;

/**将数组拆解为指定长度的子数组**/
+ (NSMutableArray *)splitArray:(NSMutableArray *)array
                   withSubSize:(int)subSize;

/**将所有数据根据每行count个分割**/
+ (NSMutableArray *)transDataToSingleRowCont:(NSInteger)count
                                  transDatas:(NSMutableArray *)data;

/**过滤单个数据**/
+ (NSMutableDictionary *)removeListSingular:(NSMutableArray *)itemIds
                                      items:(NSMutableArray *)items
                                   isDelete:(BOOL)isDelete;

/**当前对象转字典**/
+ (NSMutableDictionary *)objectTurnToDictionary:(BOOL)isStore;

/**获取特定的资源位**/
+ (NSMutableDictionary *)getAssignResourcesUseOfType:(NSInteger)type;

/**渐变颜色**/
+ (UIColor *)colorWithHex:(NSString *)hexColor;

/*+ (CAGradientLayer *)setGradualChangingColor:(UIView *)view
                                   fromColor:(NSString *)fromHexColorStr
                                     toColor:(NSString *)toHexColorStr;*/
/**同盾初始化**/
- (void)makeFMDevice;
//16进制颜色(html颜色值)字符串转为UIColor
+(UIColor *)hexStringToColor:(NSString *)stringToConvert;

/**判断版本**/
+ (void)checkVersionUpdate:(BOOL)isBackgroundSkip;

/**同盾初始化**/
+ (void)umengEvent:(NSString *)eventId attributes:(NSDictionary *)attributes number:(NSNumber *)number;

#pragma mark - File
/**获取Document目录**/
+ (NSString *)documentPath;
/**拼接Document下自定义目录**/
+ (NSString *)documentCustomPath:(NSString *)path;

/**获取Caches目录**/
+ (NSString *)cachesPath;
/**拼接Caches下自定义目录**/
+ (NSString *)cachesCustomPath:(NSString *)path;

/**获取tmp目录**/
+ (NSString *)tmpPath;

/**判断检查某个目录是否存在，以及是否不存在时创建**/
+ (BOOL)fileIsExist:(NSString *)path
       isCreatePath:(BOOL)isCreate;

/**判断检查某个目录下面是否存在内容**/
+ (BOOL)fileBelowIsExistFiles:(NSString *)path;

#pragma mark - Skip
/**app登录**/
+ (void)loginWithToken:(NSMutableDictionary *)params;

/**app跳转**/
+ (void)openScheme:(NSString *)scheme;

/**跳到购物车**/
+ (void)skipCart:(YYBaseViewController *)ctrl;

/**跳转到播放详情**/
+ (void)skipAudioWith:(YYBaseViewController *)ctrl params:(NSDictionary *)params;

/**跳到客服页面**/
+ (void)skipKefu:(YYBaseViewController *)ctrl
          params:(NSDictionary *)params;

/**构造新的客服会话页面**/
+ (void)constructionNewChatController:(YYBaseViewController *)ctrl
                               params:(NSDictionary *)params;

/**跳到预结算页面**/
+ (void)skipPreOrder:(YYBaseViewController *)ctrl
              params:(PreOrderModel *)params
              skuArr:(NSMutableArray *)arr
              source:(int)source;
/**调起支付**/
+ (void)pingPay:(YYBaseViewController *)ctrl
         chargeJason:(NSString *)charge
         block:(payBlock)Block;

//支付宝支付
+ (void)alipay:(NSString *)orderStr;

/**调起wx支付**/
+(void)WXPay:(YYBaseViewController *)ctrl
 chargeJason:(NSString *)charge
       block:(payBlock)Block;

/**跳到可用余额**/
+ (void)skipBalance:(YYBaseViewController *)ctrl
             params:(NSDictionary *)params;

/**跳到订单列表**/
+ (void)skipOrderList:(YYBaseViewController *)ctrl
               params:(NSDictionary *)params;

/**跳到订单详情**/
+ (void)skipOrderDetial:(YYBaseViewController *)ctrl
                 params:(NSDictionary *)params
              ctrlBlock:(void(^)(NSNumber *orderStatus, NSNumber *operators, BOOL isReceive))ctrlBlock;

/**跳转到订单支付**/
+ (void)skipOrderPay:(YYBaseViewController *)ctrl orderID:(long)orderID canBackRoot:(BOOL)canBack;
/**跳转到支付成功**/
+(void)skipOrderComplete:(YYBaseViewController *)ctrl params:(NSMutableDictionary *)params type:(int)type isSelfGetOrder:(BOOL)isSelfGetOrder;
/**跳转到地址列表**/
+ (void)skipAddressList:(YYBaseViewController *)ctrl;

/**跳转到个人设置**/
+ (void)skipMineSet:(YYBaseViewController *)ctrl
       isHideTabbar:(BOOL)isHideTabbar;

/**跳到个人店铺**/
+ (void)skipStoreSet:(YYBaseViewController *)ctrl;

/**跳到导师和客服页面**/
+ (void)skipQRCode:(YYBaseViewController *)ctrl
            params:(NSDictionary *)params;
/**跳转到昵称修改页面**/
+ (void)skipNickNameSet:(YYBaseViewController *)ctrl
                 params:(NSDictionary *)params;
/**跳转到我的课程列表**/
+ (void)skipMyCourceList:(YYBaseViewController *)ctrl;
/**跳转到查看物流页面**/
+ (void)skipLogisticsPage:(YYBaseViewController *)ctrl
                   params:(NSDictionary *)params;
/**跳转到优惠券页面**/
+ (void)skipCouponPage:(YYBaseViewController *)ctrl;
/**跳转到搜索详情**/
+(void)skipSearchResult:(YYBaseViewController *)ctrl
                 params:(NSDictionary *)params;
/**跳转到我的售后**/
+ (void)skipMyAfterSalePage:(YYBaseViewController *)ctrl
                     params:(NSDictionary *)params;

/**跳转到售后详情**/
+ (void)skipAfterSaleDetailPage:(YYBaseViewController *)ctrl
                         params:(NSDictionary *)params;
/**跳转到w素材**/
+ (void)skipMaterial :(YYBaseViewController *)ctrl
               params:(NSDictionary *)params;
/**跳转到上传物流信息**/
+ (void)skipUploadLogisticPage:(YYBaseViewController *)ctrl
                        params:(NSDictionary *)params;
/**跳到设置支付密码**/
+ (void)skipPaymentPassword:(YYBaseViewController *)ctrl;

/**跳搜索页面**/
+ (void)skipSearchPage:(YYBaseViewController *)ctrl title:(NSString *)title type:(NSInteger)type;
/**跳订单搜索结果页面**/
+ (void)skipOrderSearchResult:(YYBaseViewController *)ctrl
                       params:(NSDictionary *)params;
/**多组合页面跳转**/
+ (void)skipMultiCombinePage:(YYBaseViewController *)ctrl
                      params:(NSDictionary *)params;
+ (void)skipOldMultiCombinePage:(YYBaseViewController *)ctrl
                         params:(NSDictionary *)params;

/**跳转到视频播放**/
+ (void)skipVideoPlayPage:(YYBaseViewController *)ctrl
                   params:(NSDictionary *)params;

/**暂不开店**/
+ (void)skipWaitStartStore:(YYBaseViewController *)ctrl
                    params:(NSDictionary *)params;

/**跳转到选择售后类型**/
+ (void)skipAfterSaleChoose:(YYBaseViewController *)ctrl
                    orderID:(long)orderID
                      skuID:(int)skuID;
/**跳转到提交售后**/
+ (void)skipAfterSaleConfig:(YYBaseViewController *)ctrl
                    orderID:(long)orderID
                      skuID:(int)skuID
                     change:(BOOL)change
                Arbitration:(BOOL)Arbitration
                       block:(payBlock)Block;;
/**跳转到超级爆承接页**/
+ (void)skipSuperHotPage:(YYBaseViewController *)ctrl;

/**获得订单operation**/
+ (NSMutableArray *)getPreorderShowOperatorCells:(NSNumber *)operatorStr;

/**推送多组合跳转**/
+ (void)skipPushCtrl:(YYBaseViewController *)ctrl
              params:(NSDictionary *)params;

/**截取第一帧**/
+(UIImage*)getVideoPreViewImage:(NSURL *)path;

@end
