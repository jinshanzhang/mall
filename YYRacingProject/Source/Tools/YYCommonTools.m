//
//  YYCommonTools.m
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCommonTools.h"
#import "AppDelegate.h"
#import <WebKit/WebKit.h>
#import <Photos/Photos.h>
#import "YYShareItem.h"
#import "UIImage+MultiFormat.h"
#import "HomeGoodInfoModel.h"
#import "HomeHotGoodInfoModel.h"

#import <AssetsLibrary/AssetsLibrary.h>

#import "YYGetVertsionRequestAPI.h"

#import "YY_CartViewCtrl.h"
#import "YY_OrderViewCtrl.h"
#import "CYLTabBarController.h"

#import "YY_SearchController.h"
#import "YY_VideoPlayViewCtrl.h"

#import "YY_OrderUnpaidViewCtrl.h"
#import "YY_AddressListViewCtrl.h"
#import "YY_WKWebViewController.h"
#import "YY_OrderUnpaidViewCtrl.h"
#import "YY_QRCodeViewController.h"
#import "YY_StoreSetViewController.h"
#import "YY_MineSetViewController.h"
#import "YY_OrderCompleteViewCtrl.h"
#import "YY_SuperHotViewController.h"
#import "YY_CategoryViewController.h"
#import "YY_OrderSearchResultController.h"
#import "GoodDetailMaterialController.h"

#import "YY_MineOrderViewController.h"
#import "YY_GoodDetailViewController.h"
#import "YY_MineCouponViewController.h"
#import "YY_StoreBalanceViewController.h"
#import "YY_SearchResultViewController.h"

#import "YY_SearchResultViewController.h"
#import "YY_MineNickNameViewController.h"
#import "YY_MineLogisticsViewController.h"
#import "YY_WaitOpenStoreViewController.h"
#import "YY_MineAfterSaleViewController.h"
#import "YY_AfterSaleDetailViewController.h"
#import "YY_MineOrderDetailViewController.h"
#import "YY_AfterSaleConfigViewController.h"
#import "YY_PaymentPasswordViewController.h"
#import "YY_AfterSaleConfigViewController.h"

#import "YY_AfterSaleChooseServiceController.h"
#import "YY_AfterSaleChooseServiceController.h"
#import "YY_AfterSaleUploadLogisticViewController.h"


#import "YY_MineCourceViewController.h"
#import "YY_AudioDetailViewController.h"

#import "YYHxChatViewController.h"
#import "YYHxMessageViewController.h"
#import "YY_PhoneLoginViewCtrl.h"
#import "MchListViewController.h"

@implementation YYCommonTools

WMSingletonM(YYCommonTools);

//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC {
    UIViewController * currVC = nil;
    UIViewController * Rootvc = [UIApplication sharedApplication].keyWindow.rootViewController ;
    do {
        if ([Rootvc isKindOfClass:[UINavigationController class]]) {
            UINavigationController * nav = (UINavigationController *)Rootvc;
            UIViewController * v = [nav.viewControllers lastObject];
            currVC = v;
            Rootvc = v.presentedViewController;
            continue;
        }else if([Rootvc isKindOfClass:[CYLTabBarController class]]){
            CYLTabBarController * tabVC = (CYLTabBarController *)Rootvc;
            UINavigationController * nav = (UINavigationController *)[tabVC.viewControllers objectAtIndex:tabVC.selectedIndex];
            Rootvc = nav;
            currVC = [nav.viewControllers lastObject];
            continue;
        } else {
            currVC = Rootvc;
            Rootvc = nil;
        }
    } while (Rootvc!=nil);
    return currVC;
}

+(void)pushToLogin {
    YY_PhoneLoginViewCtrl *login = [[YY_PhoneLoginViewCtrl alloc] init];
    UINavigationController *loginNav = [[UINavigationController alloc] initWithRootViewController:login];
    loginNav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[YYCommonTools getCurrentVC].navigationController presentViewController:loginNav animated:YES completion:nil];
}

/**tabbar 切换**/
+ (void)tabbarSelectIndex:(NSInteger)index {
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] tabbar].selectedIndex = index;
}

/**获取当前的毫秒**/
+ (NSString *)currentTimeMsec {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time = [date timeIntervalSince1970]*1000;// *1000 是精确到毫秒，不乘就是精确到秒
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

+ (BOOL)isValidWithIdentityNum:(NSString *)IdentityNum{
    if (IdentityNum.length!=18) {
        return NO;
    }
    //校验格式
    NSString *regex2 = @"^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    BOOL flag = [identityCardPredicate evaluateWithObject:IdentityNum];
    
    if (!flag) {
        return flag;  //格式错误
    }else {
        return YES;
    }
    
}

+(void)pingPay:(YYBaseViewController *)ctrl
   chargeJason:(NSString *)charge
         block:(payBlock)Block{
//    [Pingpp createPayment:charge
//           viewController:ctrl
//             appURLScheme:@"wxb25c10fb11fcea9b"
//           withCompletion:^(NSString *result, PingppError *error) {
//               if (Block) {
//                   Block(result);
//               }
//           }];
    NSDictionary *chargeDict = [YYCommonTools dictionaryWithJsonString:charge];
    NSDictionary *credential = chargeDict[@"credential"];
    NSString *orderString = credential[@"responseBody"];
    [[AlipaySDK defaultService] payOrder:orderString fromScheme:@"alipayshellmall" callback:^(NSDictionary *resultDic) {
        NSInteger orderState = [resultDic[@"resultStatus"] integerValue];
        if (orderState == 9000) {
            if (Block) {
               Block(@"success");
            }
        } else {
            if (Block) {
              Block(@"failed");
            }
        }
    }];
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
{
    if (jsonString == nil) {
        return nil;
    }

    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err)
    {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

//支付宝支付
+ (void)alipay:(NSString *)orderStr {
    //应用注册scheme,在AliSDKDemo-Info.plist定义URL types
    NSString *appScheme = @"alipayshellmall";
    NSString * orderString = orderStr;
    // NOTE: 调用支付结果开始支付
    [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
        NSString * memo = resultDic[@"memo"];
        NSLog(@"===memo:%@", memo);
        if ([resultDic[@"ResultStatus"] isEqualToString:@"9000"]) {
            //支付成功回调
//            ShowMessage(@"支付成功");
            [YYCommonTools showTipMessage:@"支付成功"];
        } else {
//            ShowMessage(memo);
            [YYCommonTools showTipMessage:@"支付失败"];
        }
    }];
}

+(void)WXPay:(YYBaseViewController *)ctrl
   chargeJason:(NSString *)charge
         block:(payBlock)Block{
    NSDictionary *dic = [[NSString dictionaryWithJsonString:charge] objectForKey:@"credential"];
    
    
    NSLog(@"chargeJason = %@",dic);
    
    PayReq *request = [[PayReq alloc] init] ;
    request.partnerId =  [dic objectForKey:@"partnerId"];
    request.prepayId= [dic objectForKey:@"prepayId"];
    request.package = [dic objectForKey:@"packageValue"];
    request.nonceStr= [dic objectForKey:@"nonceStr"];
    request.timeStamp= [[dic objectForKey:@"timeStamp"] intValue];
    request.sign= [dic objectForKey:@"sign"];
    [WXApi sendReq:request completion:nil];
}


/**获取内容大小**/
+ (CGSize)sizeWithText:(id)text
                  font:(UIFont *)font
               maxSize:(CGSize)maxSize {
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.lineBreakMode = NSLineBreakByCharWrapping;
    style.alignment = NSTextAlignmentLeft;
    
    if ([text isKindOfClass:[NSString class]]) {
        NSAttributedString *string = [[NSAttributedString alloc]initWithString:text attributes:@{NSFontAttributeName:font, NSParagraphStyleAttributeName:style}];
        return [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    }
    else {
        NSMutableAttributedString *string = text;
        return [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    }
}

/**特殊字符处理**/
+ (NSMutableAttributedString *)containSpecialSymbolHandler:(NSString *)symbolContent
                                                symbolFont:(UIFont *)symbolFont
                                           symbolTextColor:(UIColor *)symbolTextColor
                                                 wordSpace:(NSInteger)wordSpace
                                                     price:(NSString *)price
                                                 priceFont:(UIFont *)priceFont
                                            priceTextColor:(UIColor *)textColor
                                             symbolOffsetY:(CGFloat)symbolOffsetY {
    NSMutableAttributedString *tempPrice = [NSMutableAttributedString new];
    NSMutableAttributedString *money = [[NSMutableAttributedString alloc] initWithString:symbolContent];
    [money yy_setAttributes:symbolTextColor
                       font:symbolFont
                  wordSpace:@(wordSpace)
                    content:money
                  alignment:NSTextAlignmentCenter];
    [money addAttribute:NSBaselineOffsetAttributeName
                  value:@(symbolOffsetY)
                  range:NSMakeRange(0, money.length)];
    [tempPrice appendAttributedString:money];
    
    //金额
    NSMutableAttributedString *moneyPrice = [[NSMutableAttributedString alloc] initWithString:price];
    [moneyPrice yy_setAttributes:textColor
                            font:priceFont
                         content:moneyPrice
                       alignment:NSTextAlignmentCenter];
    
    [tempPrice appendAttributedString:moneyPrice];
    return tempPrice;
}

/**是否将关键字添加到左边**/
+ (NSMutableAttributedString *)containSpecialSymbolHandler:(NSString *)symbolContent
                                                symbolFont:(UIFont *)symbolFont
                                           symbolTextColor:(UIColor *)symbolTextColor
                                                 wordSpace:(NSInteger)wordSpace
                                           symbolIsAddLeft:(BOOL)symbolIsAddLeft
                                                     price:(NSString *)price
                                                 priceFont:(UIFont *)priceFont
                                            priceTextColor:(UIColor *)textColor
                                             symbolOffsetY:(CGFloat)symbolOffsetY {
    NSMutableAttributedString *tempPrice = [NSMutableAttributedString new];
    NSMutableAttributedString *money = [[NSMutableAttributedString alloc] initWithString:symbolContent];
    [money yy_setAttributes:symbolTextColor
                       font:symbolFont
                  wordSpace:@(wordSpace)
                    content:money
                  alignment:NSTextAlignmentCenter];
    [money addAttribute:NSBaselineOffsetAttributeName
                  value:@(symbolOffsetY)
                  range:NSMakeRange(0, money.length)];
    
    
    //金额
    NSMutableAttributedString *moneyPrice = [[NSMutableAttributedString alloc] initWithString:price];
    [moneyPrice yy_setAttributes:textColor
                            font:priceFont
                         content:moneyPrice
                       alignment:NSTextAlignmentCenter];
    if (symbolIsAddLeft) {
        [tempPrice appendAttributedString:money];
        [tempPrice appendAttributedString:moneyPrice];
    }
    else {
        [tempPrice appendAttributedString:moneyPrice];
        [tempPrice appendAttributedString:money];
    }
    return tempPrice;
}


/**手机号码验证**/
+ (BOOL)valiMobile:(NSString *)mobile {
    mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (mobile.length != 11) {
        return NO;
    }else{
        NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))|(198)\\d{8}|(1705)\\d{7}$";
        NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))|(166)\\d{8}|(1709)\\d{7}$";
        
        NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))|(199)\\d{8}$";
        
        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
        BOOL isMatch1 = [pred1 evaluateWithObject:mobile];
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
        BOOL isMatch2 = [pred2 evaluateWithObject:mobile];
        NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
        BOOL isMatch3 = [pred3 evaluateWithObject:mobile];
        
        if (isMatch1 || isMatch2 || isMatch3) {
            return YES;
        }else {
            return NO;
        }
    }
}
/**发送验证码**/
+ (void)sendCodeMethod:(NSString *)phone
               codeBtn:(UIButton *)codeBtn
            phoneBlock:(void (^)(NSString *))phoneBlock {
    __block NSUInteger timeout = 59;
    //发送验证码
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        
        if (timeout <= 0)
        {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                [codeBtn setTitleColor:XHBlackColor forState:UIControlStateNormal];
                codeBtn.userInteractionEnabled = YES;
                //[codeBtn setBackgroundColor:HexRGB(0xffffff)];
                //codeBtn.layer.borderColor = HexRGB(0xff0000).CGColor;
            });
        }else{
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            if (seconds==59)
            {
                // 发送验证码
                phoneBlock(phone);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [codeBtn setTitle:[NSString stringWithFormat:@"(%@)重新获取",strTime]  forState:UIControlStateNormal];
                codeBtn.userInteractionEnabled = NO;
                //[codeBtn setBackgroundColor:HexRGB(0xf8f8f8)];
                //codeBtn.layer.borderColor = HexRGB(0xe3e3e3).CGColor;
                [codeBtn setTitleColor:XHLoginColor forState:UIControlStateNormal];
            });
        }
        timeout -- ;
    });
    dispatch_resume(_timer);
}

+ (void)startSetWKWebViewUserAgent {
        WKWebView *webView = [[WKWebView alloc] init];
        if (@available(iOS 12.0, *)){
            //由于iOS12的UA改为异步，所以不管在js还是客户端第一次加载都获取不到，所以此时需要先设置好再去获取（1、如下设置；2、先在AppDelegate中设置到本地）
            NSString *userAgent = [webView valueForKey:@"applicationNameForUserAgent"];
            NSString *newUserAgent = [userAgent stringByAppendingString:@"shellselect"];
            [webView setValue:newUserAgent forKey:@"applicationNameForUserAgent"];
        }
//        不存在
        [webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
            NSString *userAgent = result;
            NSString *newUserAgent = [userAgent stringByAppendingString:@"shellselect"];
            NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent, @"UserAgent", nil];
            [kUserDefault registerDefaults:dictionary];
            [kUserDefault synchronize];
            //setCustomUserAgent iOS9 新增方法
            if (@available(iOS 9.0, *)) {
                [webView setCustomUserAgent:newUserAgent];
            }
            else {
                [webView setValue:newUserAgent forKey:@"applicationNameForUserAgent"];
            }
        }];
}

/**错误码提示**/
+ (void)errorMessageTipHandler:(NSInteger)code {
    
}

+ (void)pasteboardCopy:(NSString *)text
          appendParams:(NSDictionary *)appendParams {
    if (!kValidString(text)) {
        [YYCommonTools showTipMessage:@"文案信息缺失"];
        return;
    }
    UIPasteboard *paste = [UIPasteboard generalPasteboard];
    paste.string = text;
    if (!kValidDictionary(appendParams)) {
        [YYCommonTools showTipMessage:@"复制成功"];
    }
    else {
        if (kValidDictionary(appendParams)) {
            if ([appendParams objectForKey:@"title"]) {
                NSString *title = [appendParams objectForKey:@"title"];
                [YYCommonTools showTipMessage:title];
            }
        }
    }
}

/**错误页展示**/
+ (void)emptyShowOperator:(YTKBaseRequest *)request
               scrollView:(UIScrollView *)scrollView
                   method:(SEL)method {
    YYEmptyViewType emptyType;
    if (request.error.code == -1001) {
        emptyType = YYEmptyViewOverTimeType;
    }
    else {
        emptyType = YYEmptyViewNoNetworkType;
    }
    scrollView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                 title:nil
                                                target:self
                                                action:method];
}
/**错误页处理**/
+ (void)emptyPageHandler:(YYEmptyViewType)type
                operator:(void (^)(NSInteger))operatorBlock {
    if (type == YYEmptyViewNoNetworkType) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if (@available(iOS 10.0, *)) {
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        } else {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    else if (type == YYEmptyViwStrollNoDataType || type == YYEmptyViewOrderNoDataType) {
        [self tabbarSelectIndex:0];
    }
    operatorBlock(type);
}

/**弹提示**/
+ (void)showTipMessage:(NSString *)tip {
    dispatch_async(dispatch_get_main_queue(), ^{
        JGProgressHUD *HUD = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleDark];
        HUD.indicatorView = nil;
        
        HUD.textLabel.text = tip;
        HUD.position = JGProgressHUDPositionCenter;
        HUD.cornerRadius = 5.0;
        HUD.contentInsets = UIEdgeInsetsMake(10, 10, 10, 10);
        [HUD showInView:kAppWindow];
        
        [HUD dismissAfterDelay:0.8];
    });
}

/**处理优惠券过期提醒操作**/
+ (BOOL)handlerCouponOvertimeTipOperator:(NSString *)timeString
                           lastTipStatus:(BOOL)lastTipStatus {
    // 下面操作前提是，过期内容返回有数据
    // 用户信息为空时，将过期提醒变成可以弹。
    if ([timeString integerValue] <= 0) {
        return YES;
    }
    else {
        if (lastTipStatus) {
            return YES;
        }
        else {
            NSInteger lastTime = [timeString integerValue];
            NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:lastTime];
            NSDate *currentDate = [[NSString alloc] getCurrentDate];
            if (lastDate.day != currentDate.day) {
                return YES;
            }
            else {
                return NO;
            }
        }
    }
}

/**查询订单状态**/
+ (NSString *)showQueryOrderStatus:(NSNumber *)orderStatus {
    NSString *showStatus = @"";
    NSInteger orderStatu = [orderStatus integerValue];
    if (orderStatu == 2) {
        showStatus = @"待付款";
    }
    else if (orderStatu == 6) {
        showStatus = @"待发货";
    }
    else if (orderStatu == 7) {
        showStatus = @"待收货";
    }
    else if (orderStatu == 8) {
        showStatus = @"已完成";
    }
    else if (orderStatu == 20) {
        showStatus = @"交易关闭";
    }
    return showStatus;
}

/**根据订单里的操作字符串返回展示btn 的内容**/
+ (NSMutableArray *)getOrderShowOperatorBtns:(NSNumber *)operatorStr {
    __block BOOL  isContaintConfirm = NO;
    __block NSMutableArray *btnModels = [NSMutableArray array];
    NSString *operatorString = [NSString getBinaryByDecimal:[operatorStr integerValue]];
    NSMutableArray *operatorArrs = [operatorString intergerValueSeparated:NO];
    
    [operatorArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BOOL   isSelect = NO;
        BOOL   isPay = NO;
        NSString *showTitle = @"";
        YYOperatorInfoModel *btnModel = [YYOperatorInfoModel new];
        NSNumber *value = (NSNumber *)obj;
        if ([value integerValue] == 1) {
            if (idx == 0) {
                showTitle = @"取消订单";
                btnModel.operatorType = YYOrderOperatorCancel;
            }
            
            if (idx == 1) {
                showTitle = @"付款";
                isSelect = YES;
                isPay = YES;
                btnModel.operatorType = YYOrderOperatorPay;
            }
            
            if (idx == 2) {
                showTitle = @"确认收货";
                isSelect = YES;
                isContaintConfirm = YES;
                btnModel.operatorType = YYOrderOperatorConfirm;
            }
            
            if (idx == 3) {
                showTitle = @"查看物流";
                btnModel.operatorType = YYOrderOperatorLookLogistic;
            }
            
            if (idx == 4) {
                showTitle = @"提交售后";
                btnModel.operatorType = YYOrderOperatorCommit;
            }
            
            if (idx == 5) {
                showTitle = @"删除订单";
                btnModel.operatorType = YYOrderOperatorDelete;
            }
            btnModel.isWaitPay = isPay;
            btnModel.isSelect = isSelect;
            btnModel.showTitle = showTitle;
            [btnModels addObject:btnModel];
        }
    }];
    if (isContaintConfirm) {
        return btnModels;
    }
    else
    return [[[btnModels reverseObjectEnumerator] allObjects] mutableCopy];
}

/**根据预订单里的operation确定cell的开关**/

+ (NSMutableArray *)getPreorderShowOperatorCells:(NSNumber *)operatorStr {
    
    //__block NSMutableArray *btnModels = [NSMutableArray array];
    NSString *operatorString = [NSString getBinaryByDecimal:[operatorStr integerValue]];
    NSMutableArray *operatorArrs = [operatorString intergerValueSeparated:NO];
    return operatorArrs;
}



/**根据订单详情里的sku的售后操作字符串返回展示btn 的内容**/
+ (NSMutableArray *)getGoodAfterSaleShowOperatorBtns:(NSNumber *)operatorStr {
    __block NSMutableArray *btnModels = [NSMutableArray array];
    NSString *operatorString = [NSString getBinaryByDecimal:[operatorStr integerValue]];
    NSMutableArray *operatorArrs = [operatorString intergerValueSeparated:NO];
    
    [operatorArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString *showTitle = @"";
        NSNumber *value = (NSNumber *)obj;
        YYOperatorInfoModel *btnModel = [YYOperatorInfoModel new];
        btnModel.isSelect = NO;
        if ([value integerValue] == 1) {
            if (idx == 0) {
                showTitle = @"申请退款";
                btnModel.goodOperatorType = YYGoodAfterSaleOperatorApply;
            }
            
            if (idx == 1) {
                showTitle = @"售后中";
                btnModel.goodOperatorType = YYGoodAfterSaleOperatorApplying;
            }
            
            if (idx == 2) {
                showTitle = @"售后失败";
                btnModel.goodOperatorType = YYGoodAfterSaleOperatorFinish;
            }
            
            if (idx == 3) {
                showTitle = @"取消售后";
                btnModel.goodOperatorType = YYGoodAfterSaleOperatorCancel;
            }
            
            if (idx == 4) {
                showTitle = @"售后已取消";
                btnModel.goodOperatorType = YYGoodAfterSaleOperatorAlreadyCancel;;
            }
            
            if (idx == 5) {
                showTitle = @"售后成功";
                btnModel.goodOperatorType = YYGoodAfterSaleOperatorSuccess;
            }
            
            if (kValidString(showTitle)) {
                btnModel.showTitle = showTitle;
            }
            [btnModels addObject:btnModel];
        }
    }];
    return btnModels;
}

/**根据售后里的操作字符串返回展示btn 的内容**/
+ (NSMutableArray *)getAfterSaleShowOperatorBtns:(NSNumber *)operatorStr {
    __block NSMutableArray *btnModels = [NSMutableArray array];
    NSString *operatorString = [NSString getBinaryByDecimal:[operatorStr integerValue]];
    NSMutableArray *operatorArrs = [operatorString intergerValueSeparated:NO];
    
    [operatorArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        BOOL   isSelect = NO;
        NSString *showTitle = @"";
        YYOperatorInfoModel *btnModel = [YYOperatorInfoModel new];
        NSNumber *value = (NSNumber *)obj;
        // 0 修改申请  1 取消售后  2 申请仲裁  3 重新申请  4 上传物流  5 查看物流  6 确认收货
        if ([value integerValue] == 1) {
            if (idx == 0) {
                showTitle = @"修改申请";
                isSelect = YES;
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorModifyApply;
            }
            
            if (idx == 1) {
                showTitle = @"取消售后";
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorCancel;
            }
            
            if (idx == 2) {
                showTitle = @"申请仲裁";
                isSelect = YES;
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorApplyArbitration;
            }
            if (idx == 3) {
                showTitle = @"重新申请";
                isSelect = YES;
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorReApply;
            }
            
            if (idx == 4) {
                showTitle = @"上传物流";
                isSelect = YES;
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorUploadLogistic;
            }
            if (idx == 5) {
                showTitle = @"查看物流";
                isSelect = YES;
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorCheckLogistic;
            }
            if (idx == 6) {
                showTitle = @"确认收货";
                isSelect = YES;
                btnModel.afterSaleOperatorType = YYAfterSaleOperatorConfirm;
            }
            
            btnModel.isSelect = isSelect;
            btnModel.showTitle = showTitle;
            [btnModels addObject:btnModel];
        }
    }];
    return btnModels;
    //return [[[btnModels reverseObjectEnumerator] allObjects] mutableCopy];
}


+ (NSMutableArray *)transDataToSingleRowCont:(NSInteger)count
                                  transDatas:(NSMutableArray *)data {
    // count 一行数量
    NSMutableArray *tempArray = [NSMutableArray array];
    if (kValidArray(data)) {
        if (data.count <= count) {
            // 总数量小于单行数量时
            [tempArray addObject:data];
        }
        else {
            NSInteger row = data.count/count;
            for (int i = 0; i < row; i ++) {
                NSArray *array = [data subarrayWithRange:NSMakeRange(i*count, count)];
                [tempArray addObject:[array mutableCopy]];
            }
            if (row * count < data.count) {
                NSInteger length = data.count - row * count;
                NSArray *array = [data subarrayWithRange:NSMakeRange(row*count, length)];
                [tempArray addObject:[array mutableCopy]];
            }
        }
    }
    return tempArray;
}

+ (NSMutableArray *)transDataToTwoColwn:(NSArray *)array {
    NSMutableArray *tranArr = [[NSMutableArray alloc]init];
    int j = 0;
    NSRange range;
    for (int i = 0; i < array.count; i++) {
        if (array.count==1) {
            //当加载
            range = NSMakeRange(j, 1);
            NSArray *temp = [array subarrayWithRange:range];
            [tranArr addObject:temp];
            continue;
        }
        else{
            range = NSMakeRange(j, 2);
            j+=2;
            if (array.count>=j) {
                NSArray *temp = [array subarrayWithRange:range];
                [tranArr addObject:temp];
                continue;
            }
            //当加载数据为奇数
            else if (j-array.count==1){
                range = NSMakeRange(array.count-1, 1);
                NSArray *temp = [array subarrayWithRange:range];
                [tranArr addObject:temp];
                continue;
            }
        }
    }
    return tranArr;
}

+ (NSMutableArray *)splitArray:(NSMutableArray *)array
                   withSubSize:(int)subSize{
    //  数组将被拆分成指定长度数组的个数
    unsigned long count = array.count % subSize == 0 ? (array.count / subSize) : (array.count / subSize + 1);
    //  用来保存指定长度数组的可变数组对象
    NSMutableArray *tempArr = [[NSMutableArray alloc] init];
    
    //利用总个数进行循环，将指定长度的元素加入数组
    for (int i = 0; i < count; i ++) {
        //数组下标
        int index = i * subSize;
        //保存拆分的固定长度的数组元素的可变数组
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        //移除子数组的所有元素
        [arr removeAllObjects];
        
        int j = index;
        //将数组下标乘以1、2、3，得到拆分时数组的最大下标值，但最大不能超过数组的总大小
        while (j < subSize*(i + 1) && j < array.count) {
            [arr addObject:[array objectAtIndex:j]];
            j += 1;
        }
        //将子数组添加到保存子数组的数组中
        [tempArr addObject:[arr copy]];
    }
    return tempArr;
}


/**过滤单个数据**/
+ (NSMutableDictionary *)removeListSingular:(NSMutableArray *)itemIds
                                      items:(NSMutableArray *)items
                                   isDelete:(BOOL)isDelete {
    NSMutableDictionary *tempDicts = [NSMutableDictionary dictionary];
    NSMutableArray *tempItems = items;
    NSMutableArray *tempItemIds = itemIds;
    
    if (kValidArray(tempItemIds) && tempItemIds.count %2 == 1 && isDelete) {
        [tempItemIds removeLastObject];
    }
    if (kValidArray(tempItems) && tempItems.count %2 == 1 && isDelete) {
        [tempItems removeLastObject];
    }
    if (kValidArray(tempItemIds)) {
        [tempDicts setObject:tempItemIds forKey:@"ids"];
    }
    if (kValidArray(tempItems)) {
        [tempDicts setObject:tempItems forKey:@"item"];
    }
    return tempDicts;
}

/**获取特定的资源位**/
+ (NSMutableDictionary *)getAssignResourcesUseOfType:(NSInteger)type {
    __block NSInteger tempType = type;
    __block NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSMutableArray *commonSources = [kUserManager loadCommonSources];
    [commonSources enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *dict = (NSDictionary *)obj;
        if ([[dict objectForKey:@"type"] integerValue] == tempType) {
            if ([dict objectForKey:@"resources"]) {
                NSMutableArray *resources = [[dict objectForKey:@"resources"] mutableCopy];
                if (kValidArray(resources)) {
                    params = [NSMutableDictionary dictionaryWithDictionary:[resources firstObject]];
                    [params setObject:resources forKey:@"resources"];
                    if ([dict objectForKey:@"switchFlag"]) {
                        [params setObject:[dict objectForKey:@"switchFlag"] forKey:@"switchFlag"];
                    }
                }
            }
            [params setObject:[dict objectForKey:@"type"] forKey:@"type"];
            *stop = YES;
        }
    }];
    return params;
}

/**当前对象转字典**/
+ (NSMutableDictionary *)objectTurnToDictionary:(BOOL)isStore {
    NSString *userJons = (isStore ? kStoreInfo.modelToJSONString : kUserInfo.modelToJSONString);
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:[NSString dictionaryWithJsonString:userJons]];
    return params;
}

+ (void)savePhotoToThumb:(id)photoObject
               isSkipTip:(BOOL)isSkipTip {
    PHAuthorizationStatus oldStatus = [PHPhotoLibrary authorizationStatus];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (status) {
                case PHAuthorizationStatusAuthorized: {
                    [YYCommonTools saveImageToAlbum:photoObject
                                          isSkipTip:isSkipTip];
                    break;
                }
                case PHAuthorizationStatusDenied: {
                    if (oldStatus == PHAuthorizationStatusNotDetermined) return;
                    [YYCommonTools showTipMessage:@"请打开访问相册权限"];
                    break;
                }
                case PHAuthorizationStatusRestricted: {
                    [YYCommonTools showTipMessage:@"系统原因，不能访问相册"];
                    break;
                }
                default:
                    break;
            }
        });
    }];
}

+ (void)saveImageToAlbum:(id)photoObject
               isSkipTip:(BOOL)isSkipTip {
    YYPhotoGroupItem *item = [[YYPhotoGroupItem alloc] init];
    if ([photoObject isKindOfClass:[UIImage class]]) {
        item.shellImage = photoObject;
    }
    else if ([photoObject isKindOfClass:[NSString class]]) {
        item.largeImageURL = [NSURL URLWithString:photoObject];
    }
    YYPhotoManager *manager = [[YYPhotoManager alloc] init];
    
    PHObjectPlaceholder *createdAsset = nil;
    if (item.largeImageURL) {
        createdAsset = [manager gainAssetsAddImageData:item.largeImageURL];
    }
    else if (item.shellImage) {
        createdAsset = [manager gainAssetsAddImageData:item.shellImage];
    }
    PHAssetCollection *createdCollection = [manager createCollection];
    NSError *error = nil;
    if (createdCollection == nil || createdAsset == nil) {
        if (isSkipTip) {
            [YYCommonTools showTipMessage:@"保存失败"];
        }
        return;
    }
    [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
        [[PHAssetCollectionChangeRequest changeRequestForAssetCollection:createdCollection] insertAssets:@[createdAsset] atIndexes:[NSIndexSet indexSetWithIndex:0]];
    } error:&error];
    
    if (isSkipTip) {
        if (error) {
            [YYCommonTools showTipMessage:@"保存失败"];
        } else {
            [YYCommonTools showTipMessage:@"保存成功"];
        }
    }
}

/**商品分享操作**/
+ (void)goodShareOperator:(id)goodModel
             expandParams:(NSMutableDictionary *)params {
    NSString *commission = nil;  //佣金
    id <ShareViewProtocol> tempModel = nil;    // 分享的中间层数据
    __block YYShareCombineInfoModel *shareModel = [YYShareCombineInfoModel new]; // 封装的友盟需要的数据
    shareModel.isApply = YES;
    shareModel.isFriendShareLink = YES;
    NSMutableArray *expandArray = [NSMutableArray array]; //分享组件扩展字段
    if (kValidDictionary(params)) {
        if ([params objectForKey:@"commission"]) {
            commission = [params objectForKey:@"commission"];
            [expandArray addObject:commission];
        }
    }

    if ([goodModel conformsToProtocol:@protocol(ShareViewProtocol)]) {
        tempModel = goodModel;
        shareModel.isApplet = tempModel.isApplet;
        shareModel.path = tempModel.path;
        shareModel.webpageUrl = tempModel.shareUrl;
        shareModel.title = tempModel.goodsTitle;
        shareModel.detailTitle = tempModel.goodsShortTitle;
        shareModel.thumbImage = tempModel.goodsShareImage;
        if (tempModel.goodsShareBigImages) {
            shareModel.shareImage = tempModel.goodsShareBigImages;
            shareModel.shareImageArray = [@[tempModel.goodsShareBigImages] mutableCopy];
        }
    }
    
    if (kValidArray(shareModel.shareImageArray) || (![tempModel.isNeedComposite boolValue] && tempModel.isNeedComposite)) {
        // 类似场次
        [YYCommonTools skipShareComponent:shareModel
                          isNeedComposite:NO expandContent:expandArray];
    }
    else {
        // 类似首页商品， 需要对分享的图片进行合成
        [YYCenterLoading showCenterLoading];
        [YYCompositePosterView generatePosterView:goodModel
                                    cutImageBlock:^(UIImage *image) {
                                        [YYCenterLoading hideCenterLoading];
                                        if (image) {
                                            shareModel.shareImage = image;
                                            shareModel.shareImageArray = [@[image] mutableCopy];
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [YYCommonTools skipShareComponent:shareModel
                                                                  isNeedComposite:YES expandContent:expandArray];
                                            });
                                        }
                                        else {
                                            [YYCommonTools showTipMessage:@"图片合成失败"];
                                        }
                                    }];
    }
}

/**弹出分享组件**/
+ (void)skipShareComponent:(YYShareCombineInfoModel *)shareModel
           isNeedComposite:(BOOL)isNeedComposite
             expandContent:(NSMutableArray *)expandArray {
    if (!isNeedComposite) {
        if (!kValidArray(shareModel.shareImageArray)) {
            [YYCommonTools showTipMessage:@"网络问题，请稍后重试"];
            return;
        }
    }
//    YYBottomShareView *shareView = [YYBottomShareView initYYPopView:[[YYBottomShareView alloc] initShowBottomView:@[@"wechat_icon",@"friend_circle_icon",@"copy_link_icon",@"save_icon"] showTitles:@[@"微信好友",@"朋友圈",@"复制链接",@"保存图片"] others:kIsFan?@[]:expandArray]];
    YYBottomShareView *shareView = [YYBottomShareView initYYPopView:[[YYBottomShareView alloc] initShowBottomView:@[@"copy_link_icon",@"save_icon"] showTitles:@[@"复制链接",@"保存图片"] others:kIsFan?@[]:expandArray]];
    @weakify(shareView);
    [shareView showViewOfAnimateType:YYPopAnimateDownUp];
    //0 微信好友 1 朋友圈 2 QQ好友  3 QQ空间  4 保存图片 5 复制链接
    shareView.shareBlock = ^(NSInteger atIndex) {
        @strongify(shareView);
//        if (atIndex > 1) {
//            if (atIndex == 2) {
//                // 复制链接
//                atIndex = 5;
//            }
//            else {
//                // 保存图片
//                atIndex = 4;
//            }
//        }
        if (atIndex == 0) {
            // 复制链接
            atIndex = 5;
        } else {
            // 保存图片
            atIndex = 4;
        }
        shareModel.operatorType = atIndex;
        [kShareManager umengShareModel:shareModel
                              callBack:^(BOOL status) {
                                  
                              }];
        [shareView showViewOfAnimateType:YYPopAnimateDownUp];
    };
}

/**图片下载操作**/
+ (BOOL)downLoadImages:(NSMutableArray *)imageUrls
          photoPreName:(NSString *)name {
    BOOL isAlDownLoad = NO; //是否已下载完毕
    if (!kValidArray(imageUrls)) {
        return isAlDownLoad;
    }
    
    int scale = 2;
    
    //(int)[UIScreen mainScreen].scale;
    NSString *tabbarPath = [YYCommonTools documentCustomPath:@"/tabbar"];
    [YYCommonTools fileIsExist:tabbarPath
                              isCreatePath:YES];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    //queue.maxConcurrentOperationCount = 3;
    queue.maxConcurrentOperationCount = imageUrls.count;
    for (int i = 0; i < imageUrls.count; i ++) {
        //获得文件名称
        NSString *imageUrl = [imageUrls objectAtIndex:i];
        imageUrl = [NSString usingEncoding:imageUrl];
        NSString *fullPath = [tabbarPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%d@%dx.png",name, i, scale]];
        NSOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:imageUrl] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                NSLog(@"图片下载完成===%@",fullPath);
                [UIImagePNGRepresentation(image) writeToFile:fullPath
                                                  atomically:YES];
            }];
        }];
        /*
        NSString *imageUrl = [imageUrls objectAtIndex:i];
        imageUrl = [NSString usingEncoding:imageUrl];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
            if (!data) {
                return ;
            }
            [data writeToFile:fullPath atomically:YES];
        }];*/
        [queue addOperation:operation];
        if (i == imageUrls.count - 1) {
            isAlDownLoad = YES;
        }
    }
    return isAlDownLoad;
}


+ (void)loginWithToken:(NSMutableDictionary *)params {
    NSNumber  *sourceType;
    [YYCommonTools  showTipMessage:@"登录成功"];
    if (kValidDictionary(params)) {
        if ([params objectForKey:@"source"]) {
            sourceType = [params objectForKey:@"source"];
        }
    }
    [kUserManager modifyUserInfo:params];
    
    if ([sourceType integerValue] == 1) {
        [kWholeConfig newUserCouponRequest];
    }
    [kWholeConfig commonResourceRequest];
    
    [kWholeConfig gainUserCenterInfo:^(BOOL isSucess) {
        kPostNotification(LoginAndQuitSuccessNotification, sourceType);
    } isLoading:YES];
    
    [kWholeConfig storeDetailInfoRequest:^(BOOL isSuccess) {
    } isLoading:YES];
}

- (void)sendCodeMethod:(NSString *)phone
               codeBtn:(UIButton *)codeBtn
            phoneBlock:(void (^)(NSString *))phoneBlock {
    __block NSUInteger timeout = 59;
    //发送验证码
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        
        if (timeout <= 0)
        {
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
                [codeBtn setTitleColor:XHBlackColor forState:UIControlStateNormal];
                codeBtn.userInteractionEnabled = YES;
                //[codeBtn setBackgroundColor:HexRGB(0xffffff)];
                //codeBtn.layer.borderColor = HexRGB(0xff0000).CGColor;
            });
        }else{
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            if (seconds==59)
            {
                // 发送验证码
                phoneBlock(phone);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [codeBtn setTitle:[NSString stringWithFormat:@"(%@)重新获取",strTime]  forState:UIControlStateNormal];
                codeBtn.userInteractionEnabled = NO;
                //[codeBtn setBackgroundColor:HexRGB(0xf8f8f8)];
                //codeBtn.layer.borderColor = HexRGB(0xe3e3e3).CGColor;
                [codeBtn setTitleColor:XHLoginColor forState:UIControlStateNormal];
            });
        }
        timeout -- ;
    });
    dispatch_resume(_timer);
}

+(void)umengEvent:(NSString *)eventId attributes:(NSDictionary *)attributes number:(NSNumber *)number{
    NSString *numberKey = @"__ct__";
    NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionaryWithDictionary:attributes];
    [mutableDictionary setObject:[number stringValue] forKey:numberKey];
    [MobClick event:eventId attributes:mutableDictionary];
}

+ (void)checkVersionUpdate:(BOOL)isBackgroundSkip {
    BOOL passwordCheck = [[kUserDefault objectForKey:@"Command"] boolValue];
    BOOL newUser = [[kUserDefault objectForKey:@"NewUser"] boolValue];
    BOOL HomePop = [[kUserDefault objectForKey:@"HomePop"] boolValue];
    if (passwordCheck || newUser || HomePop) {
        return;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    YYGetVertsionRequestAPI *api = [[YYGetVertsionRequestAPI alloc] initRequest:dic];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            BOOL isValid = NO;
            int versionType;
            VersionModel *vmodel = [VersionModel modelWithJSON:responDict];
            NSString *latestVersion = [vmodel.latestVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
            NSString *nowVersion = [kAPPVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
            // 获取当前版本号和最新版本号
            float now = [nowVersion floatValue];
            float latest = [latestVersion floatValue];
            isValid = (now < latest) ? YES : NO;
            if (isValid) {
                versionType = vmodel.type;
                NSString *updateText = [vmodel.text stringByReplacingOccurrencesOfString:@"#" withString:@"\n"];
                if (versionType == -1) {
                    [JCAlertView showTwoButtonsWithMessage:updateText headerIMG:[UIImage imageNamed:@"version_alert"] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHRedColor ButtonTitle:@"立即更新" Click:^{
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:vmodel.downloadUrl]];
                    } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"" Click:^{
                        
                    } type:JCAlertViewTypeDefault];
                }
                else if (versionType == 1 && !isBackgroundSkip) {
                    [JCAlertView showTwoButtonsWithMessage:updateText headerIMG:[UIImage imageNamed:@"version_alert"] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackLitColor ButtonTitle:@"取消" Click:^{
                    } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"立即更新" Click:^{
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:vmodel.downloadUrl]];
                    } type:JCAlertViewTypeDefault];
                }
            }
            /*
            NSNumber *versionNumber = [kUserDefault objectForKey:@"latestVersion"];
            
            if (!versionNumber || ([versionNumber floatValue] < latest)) {
                // 如果是空的，或 存储的版本小于最新版本
                isValid = YES;
            }
            
            if ((latest > now && vmodel.type == 1) ||(isValid && vmodel.type == 0 && latest > now)) {
                NSString *updateText = [vmodel.text stringByReplacingOccurrencesOfString:@"#" withString:@"\n"];
                
                [JCAlertView showTwoButtonsWithMessage:updateText headerIMG:[UIImage imageNamed:@"version_alert"] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackLitColor ButtonTitle:@"取消" Click:^{
                } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"立即更新" Click:^{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:vmodel.downloadUrl]];
                } type:JCAlertViewTypeDefault];
                
                [kUserDefault setObject:latestVersion forKey:@"latestVersion"];
            }
            */
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}


-(void)makeFMDevice
{
//    // 获取设备管理器实例
//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//    // 准备SDK初始化参数
//    NSMutableDictionary *options = [NSMutableDictionary dictionary];
//    /*
//     * SDK具有防调试功能，当使用xcode运行时(开发测试阶段),请取消下面代码注释，
//     * 开启调试模式,否则使用xcode运行会闪退。上架打包的时候需要删除或者注释掉这
//     * 行代码,如果检测到调试行为就会触发crash,起到对APP的保护作用
//     */
//    // 上线Appstore的版本，请记得删除此行，否则将失去防调试防护功能！
//    [options setValue:@"allowd" forKey:@"allowd"];  // TODO
//    // 指定线上环境的url
//    [options setValue:@"https://apitest.tongdun.cn/riskService/v1.1" forKey:@"env"]; // TODO
//    // 此处替换为您的合作方标识
//    [options setValue:@"beike" forKey:@"partner"];
//    
//    [options setValue:@"6" forKey:@"timeLimit"];
//    
//    [options setObject:^(NSString *blackBox){
//        //添加你的回调逻辑
//        printf("同盾设备指纹,回调函数获取到的blackBox:%s\n",[blackBox UTF8String]);
//    } forKey:@"callback"];
//    // 使用上述参数进行SDK初始化
//    manager->initWithOptions(options);
}

/**渐变颜色**/
+ (UIColor *)colorWithHex:(NSString *)hexColor {
    hexColor = [hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([hexColor length] < 6) {
        return nil;
    }
    if ([hexColor hasPrefix:@"#"]) {
        hexColor = [hexColor substringFromIndex:1];
    }
    NSRange range;
    range.length = 2;
    range.location = 0;
    NSString *rs = [hexColor substringWithRange:range];
    range.location = 2;
    NSString *gs = [hexColor substringWithRange:range];
    range.location = 4;
    NSString *bs = [hexColor substringWithRange:range];
    unsigned int r, g, b, a;
    [[NSScanner scannerWithString:rs] scanHexInt:&r];
    [[NSScanner scannerWithString:gs] scanHexInt:&g];
    [[NSScanner scannerWithString:bs] scanHexInt:&b];
    if ([hexColor length] == 8) {
        range.location = 4;
        NSString *as = [hexColor substringWithRange:range];
        [[NSScanner scannerWithString:as] scanHexInt:&a];
    } else {
        a = 255;
    }
    return [UIColor colorWithRed:((float)r / 255.0f) green:((float)g / 255.0f) blue:((float)b / 255.0f) alpha:((float)a / 255.0f)];
}

#pragma mark - File
/**获取Document目录**/
+ (NSString *)documentPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

/**拼接Document下自定义目录**/
+ (NSString *)documentCustomPath:(NSString *)path {
    return [[YYCommonTools documentPath] stringByAppendingPathComponent:path];
}

/**获取Caches目录**/
+ (NSString *)cachesPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

/**拼接Caches下自定义目录**/
+ (NSString *)cachesCustomPath:(NSString *)path {
    return [[YYCommonTools cachesPath] stringByAppendingPathComponent:path];
}

/**获取tmp目录**/
+ (NSString *)tmpPath {
    return NSTemporaryDirectory();
}

+ (BOOL)fileIsExist:(NSString *)path
       isCreatePath:(BOOL)isCreate {
    BOOL isExist = YES;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        isExist = NO;
        if (isCreate) {
            [fileManager createDirectoryAtPath:path
                   withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    return isExist;
}

/**判断检查某个目录下面是否存在内容**/
+ (BOOL)fileBelowIsExistFiles:(NSString *)path {
    BOOL isExist = NO;
    if ([YYCommonTools fileIsExist:path
                      isCreatePath:YES]) {
        NSFileManager * fileManger = [NSFileManager defaultManager];
        NSArray *contents = [fileManger contentsOfDirectoryAtPath:path error:nil];
        if (contents && contents.count > 0) {
            isExist = YES;
        }
    }
    return isExist;
}


#pragma mark - Skip
/**app跳转**/
+ (void)openScheme:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    if([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        if (@available(iOS 10.0, *)) {
            [application openURL:URL options:@{}
               completionHandler:^(BOOL success) {
                   YYLog(@"Open %@: %d",scheme,success);
               }];
        } else {
            // Fallback on earlier versions
            BOOL success = [application openURL:URL];
            YYLog(@"Open %@: %d",scheme,success);
        }
    }else{
        BOOL success = [application openURL:URL];
        YYLog(@"Open %@: %d",scheme,success);
    }
}

/**跳到客服页面**/
+ (void)skipKefu:(YYBaseViewController *)ctrl
          params:(NSDictionary *)params {
    HDClient *client = [HDClient sharedClient];
    if (client.isLoggedInBefore) {
        [YYCommonTools constructionNewChatController:ctrl params:params];
    }
    else {
        HDError *error = [[HDClient sharedClient] loginWithUsername:kUserInfo.easemobId password:kUserInfo.easemobSecret];
        if (!error) { //IM登录成功
            [YYCommonTools constructionNewChatController:ctrl params:params];
        } else { //登录失败
            [YYCommonTools showTipMessage:@"环信登录失败"];
        }
    }
}

/**构造新的客服会话页面**/
+ (void)constructionNewChatController:(YYBaseViewController *)ctrl
                               params:(NSDictionary *)params {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        YYHxChatViewController *messCtrl = [[YYHxChatViewController alloc] initWithConversationChatter:kHYIMID];
        messCtrl.title = @"售后";
        messCtrl.parameter = params;
        dispatch_async(dispatch_get_main_queue(), ^{
            messCtrl.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:messCtrl animated:YES];
        });
    });
}


/**跳到设置支付密码**/
+ (void)skipPaymentPassword:(YYBaseViewController *)ctrl
{
    YY_PaymentPasswordViewController *vc = [[YY_PaymentPasswordViewController alloc] init];
    [ctrl.navigationController pushViewController:vc animated:YES];
}

+ (void)skipBalance:(YYBaseViewController *)ctrl params:(NSDictionary *)params {
    YY_StoreBalanceViewController *vc = [YY_StoreBalanceViewController new];
    vc.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:vc animated:YES];
}

/**跳到订单列表**/
+ (void)skipOrderList:(YYBaseViewController *)ctrl
               params:(NSDictionary *)params {
    YY_MineOrderViewController *orderList = [[YY_MineOrderViewController alloc] init];
    orderList.parameter = params;
    orderList.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:orderList animated:YES];
}
/**跳到订单详情**/
+ (void)skipOrderDetial:(YYBaseViewController *)ctrl
                 params:(NSDictionary *)params
              ctrlBlock:(void(^)(NSNumber *orderStatus, NSNumber *operators, BOOL isReceive))ctrlBlock {
    YY_MineOrderDetailViewController *orderDetail = [[YY_MineOrderDetailViewController alloc] init];
    orderDetail.parameter = params;
    orderDetail.confirmReceivingBlock = ^(NSNumber *orderStatus,NSNumber *operators, BOOL isReceive) {
        ctrlBlock(orderStatus, operators, isReceive);
    };
    orderDetail.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:orderDetail animated:YES];
}

/**跳到购物车**/
+(void)skipCart:(YYBaseViewController *)ctrl {
    YY_CartViewCtrl *cartCtrl = [[YY_CartViewCtrl alloc] init];
    cartCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:cartCtrl animated:YES];
}





/**跳到预览订单**/
+ (void)skipPreOrder:(YYBaseViewController *)ctrl
              params:(PreOrderModel *)params
              skuArr:(NSMutableArray *)arr
              source:(int)source
{
    YY_OrderViewCtrl *orderCtrl = [[YY_OrderViewCtrl alloc] init];
    orderCtrl.preOrderModel = params;
    orderCtrl.source = source;
    orderCtrl.preOrderSkuArr = arr;
    orderCtrl.finishIdArr = [@[@(-1)] mutableCopy];
    orderCtrl.hidesBottomBarWhenPushed = YES;
    orderCtrl.operationArr = [self getPreorderShowOperatorCells:@(params.operation)];

    [ctrl.navigationController pushViewController:orderCtrl animated:YES];
}


/**跳转到选择售后类型**/
+ (void)skipAfterSaleChoose:(YYBaseViewController *)ctrl
             orderID:(long)orderID
               skuID:(int)skuID;
{
    YY_AfterSaleChooseServiceController *orderCtrl = [[YY_AfterSaleChooseServiceController alloc] init];
    orderCtrl.orderID = orderID;
    orderCtrl.skuID = skuID;
    orderCtrl.hidesBottomBarWhenPushed = YES;

    [ctrl.navigationController pushViewController:orderCtrl animated:YES];
}

/**跳转到提交售后**/
+ (void)skipAfterSaleConfig:(YYBaseViewController *)ctrl
                    orderID:(long)orderID
                      skuID:(int)skuID
                     change:(BOOL)change
                Arbitration:(BOOL)Arbitration
                      block:(payBlock)Block;
{
    YY_AfterSaleConfigViewController *orderCtrl = [[YY_AfterSaleConfigViewController alloc] init];
    orderCtrl.orderID = orderID;
    orderCtrl.skuID = skuID;
    orderCtrl.ChangeConfig = change;
    orderCtrl.hidesBottomBarWhenPushed = YES;
    orderCtrl.Arbitration = Arbitration;
    orderCtrl.receiveBlock = ^(NSString *result) {
        if (Block) {
            Block(result);
        }
    };

    [ctrl.navigationController pushViewController:orderCtrl animated:YES];
}

/**跳转到搜索详情**/
+ (void)skipSearchResult:(YYBaseViewController *)ctrl
                  params:(NSDictionary *)params {
    YY_SearchResultViewController *vc = [[YY_SearchResultViewController alloc] init];
    vc.parameter = params;
    [ctrl.navigationController pushViewController:vc animated:YES];
}


/**跳转到素材**/
+ (void)skipMaterial :(YYBaseViewController *)ctrl
                  params:(NSDictionary *)params {
    GoodDetailMaterialController *vc = [[GoodDetailMaterialController alloc] init];
    vc.parameter = params;
    [ctrl.navigationController pushViewController:vc animated:YES];
}


/**跳转到订单搜索详情**/
+ (void)skipOrderSearchResult:(YYBaseViewController *)ctrl
                  params:(NSDictionary *)params {
    YY_OrderSearchResultController *vc = [[YY_OrderSearchResultController alloc] init];
    vc.parameter = params;
    [ctrl.navigationController pushViewController:vc animated:YES];
}

/**跳转到订单支付**/
+ (void)skipOrderPay:(YYBaseViewController *)ctrl
             orderID:(long)orderID
             canBackRoot:(BOOL)canBack
{
    YY_OrderUnpaidViewCtrl *orderCtrl = [[YY_OrderUnpaidViewCtrl alloc] init];
    orderCtrl.orderID = orderID;
    orderCtrl.canback = canBack;
    orderCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:orderCtrl animated:YES];
    
    if (canBack == YES) {
        NSMutableArray *array = ctrl.navigationController.viewControllers.mutableCopy;
        if ([array[array.count-2] isKindOfClass:[YYBaseViewController class]]) {
            //移除操作
            [array removeObjectAtIndex:array.count-2];
            //重新设定栈中的控制器
            [ctrl.navigationController setViewControllers:array animated:NO];
        }
    }
 
}

/**跳转到支付成功**/
+(void)skipOrderComplete:(YYBaseViewController *)ctrl params:(NSMutableDictionary *)params type:(int)type isSelfGetOrder:(BOOL)isSelfGetOrder{
    YY_OrderCompleteViewCtrl *order = [[YY_OrderCompleteViewCtrl alloc] init];
    order.parameter = params;
    order.orderType = type;
    order.isSelfGetOrder = isSelfGetOrder;
    [ctrl.navigationController pushViewController:order animated:YES];

    NSMutableArray *array = ctrl.navigationController.viewControllers.mutableCopy;
        if ([array[array.count-2] isKindOfClass:[YYBaseViewController class]]) {
            //移除操作
            [array removeObjectAtIndex:array.count-2];
            //重新设定栈中的控制器
            [ctrl.navigationController setViewControllers:array animated:NO];
    }
}

/**跳转到我的课程列表**/
+ (void)skipMyCourceList:(YYBaseViewController *)ctrl{
    
    YY_MineCourceViewController *CourceCtrl = [[YY_MineCourceViewController alloc] init];
    CourceCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:CourceCtrl animated:YES];
}
/**跳转到播放详情**/
+ (void)skipAudioWith:(YYBaseViewController *)ctrl
               params:(NSMutableDictionary *)params{
    YY_AudioDetailViewController *audioCtrl = [[YY_AudioDetailViewController alloc] init];
    audioCtrl.parameter = params;
    audioCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:audioCtrl animated:YES];
}

/**跳转到地址列表**/
+ (void)skipAddressList:(YYBaseViewController *)ctrl{
    
    YY_AddressListViewCtrl *orderCtrl = [[YY_AddressListViewCtrl alloc] init];
    orderCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:orderCtrl animated:YES];
}

/**跳转到个人设置**/
+ (void)skipMineSet:(YYBaseViewController *)ctrl
       isHideTabbar:(BOOL)isHideTabbar {
    YY_MineSetViewController *setCtrl = [[YY_MineSetViewController alloc] init];
    setCtrl.hidesBottomBarWhenPushed = isHideTabbar;
    [ctrl.navigationController pushViewController:setCtrl animated:YES];
}

/**跳到个人店铺**/
+ (void)skipStoreSet:(YYBaseViewController *)ctrl {
    YY_StoreSetViewController *setCtrl = [[YY_StoreSetViewController alloc] init];
    setCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:setCtrl animated:YES];
}

/**跳到导师和客服页面**/
+ (void)skipQRCode:(YYBaseViewController *)ctrl
            params:(NSDictionary *)params {
    YY_QRCodeViewController *qrCtrl = [[YY_QRCodeViewController alloc] init];
    qrCtrl.hidesBottomBarWhenPushed = YES;
    qrCtrl.parameter = params;
    [ctrl.navigationController pushViewController:qrCtrl animated:YES];
}

/**跳转到昵称修改页面**/
+ (void)skipNickNameSet:(YYBaseViewController *)ctrl
                 params:(NSDictionary *)params {
    YY_MineNickNameViewController *nickCtrl = [[YY_MineNickNameViewController alloc] init];
    nickCtrl.parameter = params;
    nickCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:nickCtrl animated:YES];
}

/**跳转到查看物流页面**/
+ (void)skipLogisticsPage:(YYBaseViewController *)ctrl
                   params:(NSDictionary *)params {
    YY_MineLogisticsViewController *logisticsCtrl = [[YY_MineLogisticsViewController alloc] init];
    logisticsCtrl.parameter = params;
    logisticsCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:logisticsCtrl animated:YES];
}

/**跳转到优惠券页面**/
+ (void)skipCouponPage:(YYBaseViewController *)ctrl {
    YY_MineCouponViewController *couponCtrl = [[YY_MineCouponViewController alloc] init];
    couponCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:couponCtrl animated:YES];
}

/**跳转到我的售后**/
+ (void)skipMyAfterSalePage:(YYBaseViewController *)ctrl
                     params:(NSDictionary *)params {
    YY_MineAfterSaleViewController *saleAfterCtrl = [[YY_MineAfterSaleViewController alloc] init];
    saleAfterCtrl.parameter = params;
    saleAfterCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:saleAfterCtrl animated:YES];
}

/**跳转到售后详情**/
+ (void)skipAfterSaleDetailPage:(YYBaseViewController *)ctrl
                         params:(NSDictionary *)params {
    YY_AfterSaleDetailViewController *detailCtrl = [[YY_AfterSaleDetailViewController alloc] init];
    detailCtrl.parameter = params;
    detailCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:detailCtrl animated:YES];
}

/**跳转到上传物流信息**/
+ (void)skipUploadLogisticPage:(YYBaseViewController *)ctrl
                        params:(NSDictionary *)params {
    YY_AfterSaleUploadLogisticViewController *uploadCtrl = [[YY_AfterSaleUploadLogisticViewController alloc] init];
    uploadCtrl.parameter = params;
    uploadCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:uploadCtrl animated:YES];
}

/**跳转到视频播放**/
+ (void)skipVideoPlayPage:(YYBaseViewController *)ctrl
                   params:(NSDictionary *)params {
    YY_VideoPlayViewCtrl *videoCtrl = [[YY_VideoPlayViewCtrl alloc] init];
    videoCtrl.parameter = params;
    videoCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:videoCtrl animated:YES];
}

/**暂不开店**/
+ (void)skipWaitStartStore:(YYBaseViewController *)ctrl
                    params:(NSDictionary *)params {
    YY_WaitOpenStoreViewController *waitCtrl = [[YY_WaitOpenStoreViewController alloc] init];
    waitCtrl.parameter = params;
    waitCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:waitCtrl animated:YES];
}

/**跳搜索页面**/
+ (void)skipSearchPage:(YYBaseViewController *)ctrl title:(NSString *)title type:(NSInteger)type{
    YY_SearchController *searShopVC = [YY_SearchController new];
    searShopVC.type = type;
    searShopVC.maintitle = title;
    [searShopVC setHidesBottomBarWhenPushed:YES];
    [ctrl pushAndPopViewController:YES viewController:searShopVC];
}

/**跳转到超级爆款承接页**/
+ (void)skipSuperHotPage:(YYBaseViewController *)ctrl {
    YY_SuperHotViewController *hotCtrl = [YY_SuperHotViewController new];
    hotCtrl.hidesBottomBarWhenPushed = YES;
    [ctrl.navigationController pushViewController:hotCtrl animated:YES];
}


+ (void)skipOldMultiCombinePage:(YYBaseViewController *)ctrl
                      params:(NSDictionary *)params {
    if (kValidDictionary(params)) {
        NSNumber *linkType = [params objectForKey:@"linkType"];
        if ([linkType integerValue] == 1) {
            // 商品详情
            YY_GoodDetailViewController *goodDetail = [[YY_GoodDetailViewController alloc] init];
            goodDetail.parameter = params;
            goodDetail.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:goodDetail animated:YES];
        }
        else if ([linkType integerValue] == 2) {
            // 切换首页
            [YYCommonTools tabbarSelectIndex:0];
            [ctrl.navigationController popToRootViewControllerAnimated:YES];
        }
        else if ([linkType integerValue] == 3) {
            // web
            YY_WKWebViewController *webView = [[YY_WKWebViewController alloc] init];
            webView.parameter = params;
            webView.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:webView animated:YES];
        }
        else if ([linkType integerValue] == 5 || [linkType integerValue] == 13) {
            // 5 搜索结果页  13 类目组合页
            YY_SearchResultViewController *vc = [[YY_SearchResultViewController alloc] init];
            vc.parameter = params;
            vc.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:vc animated:YES];
        }
    }
}






/**多组合页面跳转**/
+ (void)skipMultiCombinePage:(YYBaseViewController *)ctrl
                      params:(NSDictionary *)params {
    if (kValidDictionary(params)) {
        NSNumber *linkType = [params objectForKey:@"linkType"];
        if ([linkType integerValue] == 1||[linkType integerValue] == 4){
         
            // 1. 商品详情  4.特卖商详页
            YY_GoodDetailViewController *goodDetail = [[YY_GoodDetailViewController alloc] init];
            goodDetail.parameter = params;
            goodDetail.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:goodDetail animated:YES];
        }
        else if ([linkType integerValue] == 9) {
            // 切换首页
            [YYCommonTools tabbarSelectIndex:0];
            [ctrl.navigationController popToRootViewControllerAnimated:YES];
        }
        else if ([linkType integerValue] == 2||[linkType integerValue] == 3) {
            // web
            YY_WKWebViewController *webView = [[YY_WKWebViewController alloc] init];
            webView.parameter = params;
            webView.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:webView animated:YES];
        }
        else if ([linkType integerValue] == 5 || [linkType integerValue] == 13) {
            // 5 搜索结果页  13 类目组合页
            YY_SearchResultViewController *vc = [[YY_SearchResultViewController alloc] init];
            vc.parameter = params;
            vc.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:vc animated:YES];
        }else if ([linkType integerValue] == 6) {
            //我的订单
            [YYCommonTools skipOrderList:ctrl params:@{@"source": @(1)}];
        }else if ([linkType integerValue] == 7){
            //店铺订单
            [YYCommonTools skipOrderList:ctrl params:@{@"source": @(2)}];
        }
        else if ([linkType integerValue] == 8){
            //店铺主页
            [YYCommonTools tabbarSelectIndex:0];
        }
        else if ([linkType integerValue] == 18) {
            // 18 跳转分类
            YY_CategoryViewController *cateCtrl = [[YY_CategoryViewController alloc] init];
            cateCtrl.parameter = params;
            cateCtrl.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:cateCtrl animated:YES];
        }
        else if ([linkType integerValue] == 20) {
            // 20 跳转附近商家
            MchListViewController *cateCtrl = [[MchListViewController alloc] init];
            cateCtrl.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:cateCtrl animated:YES];
        }
    }
}

+(UIColor *)hexStringToColor:(NSString *)stringToConvert
{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
        if ([cString length] < 6) return [UIColor blackColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    if ([cString length] != 6) return [UIColor blackColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;

    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];

    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+ (void)skipPushCtrl:(YYBaseViewController *)ctrl
              params:(NSDictionary *)params {
    if (kValidDictionary(params)) {
        NSString *linkType = [params objectForKey:@"linkType"];
        if ([linkType integerValue] == 1||[linkType integerValue] == 4) {
            // 商品详情
            YY_GoodDetailViewController *goodDetail = [[YY_GoodDetailViewController alloc] init];
            goodDetail.parameter = params;
            goodDetail.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:goodDetail animated:YES];
        }
        else if ([linkType integerValue] == 2) {
            // web
            YY_WKWebViewController *webView = [[YY_WKWebViewController alloc] init];
            webView.parameter = params;
            webView.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:webView animated:YES];
        }
        
         else if ([linkType intValue] == 9){
            // 切换首页
            [YYCommonTools tabbarSelectIndex:0];
            [ctrl.navigationController popToRootViewControllerAnimated:YES];
        }
        else if ([linkType integerValue] == 3 || [linkType integerValue] == 13) {
            // 类目组合页
            YY_SearchResultViewController *vc = [[YY_SearchResultViewController alloc] init];
            vc.parameter = params;
            vc.hidesBottomBarWhenPushed = YES;
            [ctrl.navigationController pushViewController:vc animated:YES];
        }
    }
}

// 获取视频第一帧
+(UIImage*)getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}

@end

