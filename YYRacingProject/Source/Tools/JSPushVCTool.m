//
//  CJPushVCTool.m
//  CJ Dropshipping
//
//  Created by cujia_1 on 2020/7/28.
//  Copyright © 2020 CuJia. All rights reserved.
//

#import "JSPushVCTool.h"
#import "UIViewController+CJParams.h"

@implementation JSPushVCTool

/**
 页面跳转不带参数 默认隐藏tabbar 带动画
 
 @param  vcName  控制器名称
 */
+ (void)pushViewController:(NSString *)vcName {
    [self pushViewController:vcName params:@{} hiddenTabbar:YES animated:YES];
}

/**
 页面跳转带参数 默认隐藏tabbar 带动画
 
 @param  vcName  控制器名称
 */
+ (void)pushViewController:(NSString *)vcName params:(NSDictionary *)params {
    [self pushViewController:vcName params:params hiddenTabbar:YES animated:YES];
}

/**
页面跳转不带参数

@param  vcName  控制器名称
@param  hiddenTabbar  是否隐藏tbabar
@param  animated  是否动画
*/
+ (void)pushViewController:(NSString *)vcName hiddenTabbar:(BOOL)hiddenTabbar animated:(BOOL)animated {
    [self pushViewController:vcName params:@{} hiddenTabbar:hiddenTabbar animated:animated];
}

/**
页面跳转带参数

@param  vcName  控制器名称
@param  params  参数
@param  hiddenTabbar  是否隐藏tbabar
@param  animated  是否动画
*/
+ (void)pushViewController:(NSString *)vcName params:(NSDictionary *)params hiddenTabbar:(BOOL)hiddenTabbar animated:(BOOL)animated {
    if(vcName == nil) {
        NSLog(@"控制器名称不能为空");
        return;
    }
    Class class = NSClassFromString(vcName);
    UIViewController * vc = (UIViewController *)[[class alloc] init];
    if(vc == nil) {
        NSLog(@"控制器不存在，请检查控制器名称是否正确");
        return;
    }
    vc.hidesBottomBarWhenPushed = hiddenTabbar;
    if(params != nil) {
       vc.params = params;
    }
    [YYCommonTools.getCurrentVC.navigationController pushViewController:vc animated:animated];
}

/**
跳回指定的页面

@param  vcName  控制器名称
*/
+ (void)popToViewController:(NSString *)vcName {
    [self popToViewController:vcName completeHandle:nil];
}

/**
跳回指定的页面 带回调处理

@param  vcName  控制器名称
*/
+ (void)popToViewController:(NSString *)vcName completeHandle:(void(^)(void))completeHandle {
    if(vcName == nil) {
        NSLog(@"控制器名称不能为空");
        return;
    }
    for (UIViewController * vc in YYCommonTools.getCurrentVC.navigationController.viewControllers) {
        NSString * className = NSStringFromClass([vc class]);
        if([className isEqualToString:vcName]) {
            [YYCommonTools.getCurrentVC.navigationController popToViewController:vc animated:YES];
            if(completeHandle) {
                completeHandle();
            }
            return;
        }
    }
    NSLog(@"没找到指定的控制器");
}

/**
跳回指定的页面

@param  times  跳回几次。如果跳回两次，times = 2
*/
+ (void)popToViewControllerWithTimes:(NSInteger)times {
    [self popToViewControllerWithTimes:times completeHandle:nil];
}

/**
跳回指定的页面

@param  times  跳回几次。如果跳回两次，times = 2
@param  completeHandle  回调
*/
+ (void)popToViewControllerWithTimes:(NSInteger)times completeHandle:(void(^)(void))completeHandle {
    if (times > YYCommonTools.getCurrentVC.navigationController.viewControllers.count) {
        NSLog(@"跳回的次数错误，超过了导航控制器里的控制器的数量");
        return;
    }
    NSInteger vcCount = YYCommonTools.getCurrentVC.navigationController.viewControllers.count;
    UIViewController * targetVC = YYCommonTools.getCurrentVC.navigationController.viewControllers[vcCount - times - 1];
    if(targetVC == nil) {
        NSLog(@"控制器为空");
        return;
    }
    [YYCommonTools.getCurrentVC.navigationController popToViewController:targetVC animated:YES];
    if(completeHandle) {
        completeHandle();
    }
}

@end
