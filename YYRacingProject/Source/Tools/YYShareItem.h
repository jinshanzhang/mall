//
//  YYShareItem.h
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YYShareItem : NSObject<UIActivityItemSource>

- (instancetype)initWithData:(UIImage *)image
                 addFilePath:(NSURL *)filePath;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL   *filePath;

@end
