//
//  AFHttpManager.m
//  ysscw_ios
//
//  Created by next on 2018/6/9.
//  Copyright © 2018年 ysscw. All rights reserved.
//

#import "AFHttpManager.h"
//#import "UserInfoManager.h"
@implementation AFHttpManager
+ (instancetype)sharedManager{
    static AFHttpManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        manager = [[AFHttpManager alloc] initWithBaseURL:[NSURL URLWithString:kEnvConfig.baserUrl] sessionConfiguration:configuration];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/xml", @"text/javascript", @"text/html", @"text/plain", @"text/gif", nil];
        manager.requestSerializer.timeoutInterval = 20;
        manager.securityPolicy = [AFSecurityPolicy defaultPolicy];
    });
    return manager;
}

@end
