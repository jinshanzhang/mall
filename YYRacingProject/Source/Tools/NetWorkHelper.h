//
//  NetWorkHelper.h
//  ysscw_ios
//
//  Created by next on 2018/6/9.
//  Copyright © 2018年 ysscw. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHttpManager.h"

typedef void(^RequestBlock)(NSString *errorNO,NSDictionary *result);
typedef void(^ProgessBlock)(NSProgress *);

typedef NS_ENUM(NSUInteger,HttpMethod){
    GET = 0,
    POST,
};

@interface NetWorkHelper : NSObject

+(NSURLSessionDataTask *)requestWithMethod:(HttpMethod)method path:(NSString *)path parameters:(NSDictionary *)parameters block:(RequestBlock)block;

//推荐body方式
//+ (NSURLSessionDataTask *)requestWithPath:(NSString *)path
//                                   method:(NSString *)method
//                               parameters:(NSDictionary *)parameters
//                                    block:(RequestBlock)block;

+ (void)cancelTasks;
@end
