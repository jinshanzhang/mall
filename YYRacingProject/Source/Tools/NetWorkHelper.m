//
//  NetWorkHelper.m
//  ysscw_ios
//
//  Created by next on 2018/6/9.
//  Copyright © 2018年 ysscw. All rights reserved.
//

#import "NetWorkHelper.h"

@implementation NetWorkHelper

/**
 * header
 **/
+ (NSDictionary<NSString *,NSString *> *)requestHeaderDictionary {
    NSString  *x_platform = @"shellvip";
    NSString  *x_source = @"ios";
    NSString  *x_appversion = kAPPVersion;
    NSString  *x_imei = kDeviceUUID;
    NSString  *x_model = kSystemName;
    NSString  *x_osversion = kSystemVersions;
    NSString  *x_channel = @"ios";
    
    NSMutableDictionary *diction = [NSMutableDictionary dictionary];
    if(kValidString(kUserInfo.useToken)) {
        [diction setObject:kUserInfo.useToken forKey:@"x-token"];
    }
    NSString *timeMsec = [YYCommonTools currentTimeMsec];
    [diction setObject:x_platform forKey:@"x-platform"];
    [diction setObject:x_source forKey:@"x-source"];
    [diction setObject:x_appversion forKey:@"x-app-version"];
    [diction setObject:x_imei forKey:@"x-imei"];
    [diction setObject:x_model forKey:@"x-model"];
    [diction setObject:x_osversion forKey:@"x-os-version"];
    [diction setObject:x_channel forKey:@"x-channel"];
    [diction setObject:@"application/json" forKey:@"Content-Type"];
    [diction setObject:@"application/json" forKey:@"Accept"];
    [diction setObject:timeMsec forKey:@"x-timestamp"];
    //2.1 加入x-sign
    if(kValidString(kUserInfo.useToken)) {
        NSString *tempSign = [NSString stringWithFormat:@"%@&%@&%@&%@&%@",kUserInfo.useToken, x_platform, x_source, timeMsec, kEnvConfig.signKey];
        [diction setObject:[[CocoaSecurity md5:tempSign] hexLower] forKey:@"x-sign"];
    }
    return diction;
}

+ (NSURLSessionDataTask *)requestWithMethod:(HttpMethod)method path:(NSString *)path parameters:(NSDictionary *)parameters block:(RequestBlock)block{
    NSURLSessionDataTask *task = nil;
    NSString *serverHost = kEnvConfig.baserUrl;
    switch (method) {
        case GET:{
            NSString *url = [serverHost stringByAppendingPathComponent:path];
            NSMutableDictionary *paraDict = [NSMutableDictionary dictionaryWithDictionary:parameters];
            NSDictionary *dict = [NetWorkHelper requestHeaderDictionary];
            for (NSString *key in dict) {
                NSString *value = [dict objectForKey:key];
                [[AFHttpManager sharedManager].requestSerializer setValue:value forHTTPHeaderField:key];
            }
            if (![parameters isKindOfClass:[NSDictionary class]]) {
                parameters = [NSDictionary dictionary];
            }
            //添加打印Get请求操作
            if(paraDict) {
                NSUInteger paraCount = paraDict.allKeys.count;
                if(paraCount > 0) {
                    if( paraCount == 1) {
                        for (NSString * key in paraDict.allKeys){
                            url = [NSString stringWithFormat:@"%@?%@=%@",url,key,[paraDict objectForKey:key]];
                        }
                    } else {
                        for ( int i = 0;i < paraCount; i++){
                            NSString *key = paraDict.allKeys[i];
                            if(i == 0) {
                                url = [NSString stringWithFormat:@"%@?%@=%@",url,key,[paraDict objectForKey:key]];
                            } else {
                                url = [NSString stringWithFormat:@"%@&%@=%@",url,key,[paraDict objectForKey:key]];
                            }
                        }
                    }
                }
            }
            NSLog(@"-------------获取到的请求的url是----------:%@",url);
            task = [[AFHttpManager sharedManager] GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (!task.error) {
                    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingMutableLeaves error:nil];
                    if (jsonDict) {
                        NSString *errorNO = [NSString stringWithFormat:@"%@",jsonDict[@"code"]];
                        SLog(@"----获取到的接口请求的数据是:%@----",[NSString convertToJsonData:jsonDict]);
                        block(errorNO, jsonDict);
                    } else{
                        NSDictionary *info = @{@"code":@"9998",@"desc":@"数据解析错误！"};
                        block(@"9998", info);
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSDictionary *errorInfo = @{@"code":@"-1011",@"desc":@"数据请求错误，请稍后重试！"};
                block(@(error.code).stringValue, errorInfo);
            }];
        }
            break;
        case POST:{
            NSString *url = [serverHost stringByAppendingPathComponent:path];
            NSDictionary *dict = [NetWorkHelper requestHeaderDictionary];
            for (NSString *key in dict) {
                NSString *value = [dict objectForKey:key];
                [[AFHttpManager sharedManager].requestSerializer setValue:value forHTTPHeaderField:key];
            }
            if (![parameters isKindOfClass:[NSDictionary class]]) {
                parameters = [NSDictionary dictionary];
            }
            NSLog(@"----------获取到的参数是---------:%@ %@",url,parameters);
            for (NSString * key in parameters.allKeys) {
                NSLog(@"获取到的数据键值对是：%@:%@",key,[parameters objectForKey:key]);
            }
            task = [[AFHttpManager sharedManager] POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if (!task.error) {
                    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:(NSData *)responseObject options:NSJSONReadingMutableLeaves error:nil];
                    if (jsonDict) {
                        NSString *errorNO = [NSString stringWithFormat:@"%@",jsonDict[@"code"]];
                        SLog(@"----获取到的接口请求的数据是:%@----",[NSString convertToJsonData:jsonDict]);
                        block(errorNO, jsonDict);
                    } else {
                        NSDictionary *info = @{@"code":@"9998",@"desc":@"数据解析错误！"};
                        block(@"9998", info);
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               NSDictionary *errorInfo = @{@"code":@"-1011",@"desc":@"数据请求错误，请稍后重试！"};
                block(@(error.code).stringValue, errorInfo);
            }];

        }
            break;
        default:
            break;
    }
    
    return task;
}

+ (void)cancelTasks {
    if ([AFHttpManager.sharedManager.tasks count] > 0) {
        [AFHttpManager.sharedManager.tasks makeObjectsPerformSelector:@selector(cancel)];
    }
}

@end
