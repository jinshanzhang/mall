//
//  YYShareItem.m
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYShareItem.h"

@implementation YYShareItem

- (instancetype)initWithData:(UIImage *)image addFilePath:(NSURL *)filePath {
    self = [super init];
    if (self) {
        self.image = image;
        self.filePath = filePath;
    }
    return self;
}

#pragma mark - UIActivityItemSource
- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return self.image;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    return self.filePath;
}

- (NSString*)activityViewController:(UIActivityViewController *)activityViewController subjectForActivityType:(NSString *)activityType
{
    // 无用
    return @"";
}

@end
