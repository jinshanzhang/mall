//
//  CJPushVCTool.h
//  CJ Dropshipping
//
//  Created by cujia_1 on 2020/7/28.
//  Copyright © 2020 CuJia. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JSPushVCTool : NSObject

/**
 页面跳转不带参数 默认隐藏tabbar 带动画
 
 @param  vcName  控制器名称
 */
+ (void)pushViewController:(NSString *)vcName;

/**
 页面跳转带参数 默认隐藏tabbar 带动画
 
 @param  vcName  控制器名称
 */
+ (void)pushViewController:(NSString *)vcName params:(NSDictionary *)params;

/**
 页面跳转不带参数
 
 @param  vcName  控制器名称
 @param  hiddenTabbar  是否隐藏tbabar
 @param  animated  是否动画
 */
+ (void)pushViewController:(NSString *)vcName hiddenTabbar:(BOOL)hiddenTabbar animated:(BOOL)animated;

/**
页面跳转带参数

@param  vcName  控制器名称
@param  params  参数
@param  hiddenTabbar  是否隐藏tbabar
@param  animated  是否动画
*/
+ (void)pushViewController:(NSString *)vcName params:(NSDictionary *)params hiddenTabbar:(BOOL)hiddenTabbar animated:(BOOL)animated;

/**
跳回指定的页面

@param  vcName  控制器名称
*/
+ (void)popToViewController:(NSString *)vcName;

/**
跳回指定的页面 带回调处理

@param  vcName  控制器名称
@param  completeHandle  回调
*/
+ (void)popToViewController:(NSString *)vcName completeHandle:(void(^)(void))completeHandle;

/**
跳回指定的页面

@param  times  跳回几次。如果跳回两次，times = 2
*/
+ (void)popToViewControllerWithTimes:(NSInteger)times;

/**
跳回指定的页面

@param  times  跳回几次。如果跳回两次，times = 2
@param  completeHandle  回调
*/
+ (void)popToViewControllerWithTimes:(NSInteger)times completeHandle:(void(^)(void))completeHandle;


@end

NS_ASSUME_NONNULL_END
