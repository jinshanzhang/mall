//
//  AFHttpManager.h
//  ysscw_ios
//
//  Created by next on 2018/6/9.
//  Copyright © 2018年 ysscw. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
@interface AFHttpManager : AFHTTPSessionManager
+ (instancetype)sharedManager;
@end
