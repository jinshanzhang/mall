//
//  YYCreateTools.m
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCreateTools.h"

@implementation YYCreateTools

+ (UIButton *)createBtn:(NSString *)title {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (kValidString(title)) {
        [btn setTitle:title forState:UIControlStateNormal];
    }
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    return btn;
}

+ (UIButton *)createBtn:(NSString *)title
                   font:(UIFont *)font
              textColor:(UIColor *)textColor {
    UIButton *btn = [self createBtn:title];
    btn.titleLabel.font = font;
    btn.adjustsImageWhenHighlighted = NO;
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btn setTitleColor:textColor
              forState:UIControlStateNormal];
    return btn;
}

+ (UIButton *)createBtnImage:(NSString *)imageName {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    if (kValidString(imageName)) {
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    return btn;
}

+ (UIButton *)createButton:(NSString *)title
                   bgColor:(UIColor *)bgColor
                 textColor:(UIColor *)textColor{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectZero;
    btn.titleLabel.font = midFont(14);
    btn.selected = NO;
    btn.adjustsImageWhenHighlighted = NO;
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:textColor forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:bgColor] forState:UIControlStateNormal];

    return btn;
}

+ (UILabel *)createLabel:(NSString *)text
                    font:(UIFont *)font
               textColor:(UIColor *)textColor {
    UILabel *lbl = [UILabel new];
    if (kValidString(text)) {
        lbl.text = text;
    }
    lbl.textColor = textColor;
    lbl.font = font;
    return lbl;
}

+ (YYLabel *)createLabel:(NSString *)text
                    font:(UIFont *)font
               textColor:(UIColor *)textColor
                maxWidth:(CGFloat)maxWidth
           fixLineHeight:(CGFloat)fixLineHeight {
    YYLabel *lbl = [YYLabel new];
    if (kValidString(text)) {
        lbl.text = text;
    }
    lbl.textColor = textColor;
    lbl.font = font;
    lbl.numberOfLines = 0;
    YYTextLinePositionSimpleModifier *modifier = [YYTextLinePositionSimpleModifier new];
    modifier.fixedLineHeight = fixLineHeight;
    lbl.linePositionModifier = modifier;
    lbl.preferredMaxLayoutWidth = maxWidth;
    return lbl;
}


+ (UIImageView *)createImageView:(NSString *)imageName
                       viewModel:(NSInteger)viewModel {
    UIImageView *img = [[UIImageView alloc] init];
    if (kValidString(imageName)) {
        img.image = [UIImage imageNamed:imageName];
    }
    img.userInteractionEnabled = YES;
    if (viewModel == 1) {
        img.contentMode = UIViewContentModeScaleAspectFit;
    }
    return img;
}

+ (UIView *)createView:(UIColor *)color {
    UIView *bgView = [UIView new];
    if (!color) {
        bgView.backgroundColor = XHWhiteColor;
    }
    else
        bgView.backgroundColor = color;
    return bgView;
}

+ (UITextField *)createTextField:(NSString *)text
                            font:(UIFont *)font
                        texColor:(UIColor *)textColor {
    UITextField *textField = [UITextField new];
    if (kValidString(text)) {
        textField.text = text;
    }
    textField.textColor = textColor;
    textField.font = font;
    return textField;
}

@end
