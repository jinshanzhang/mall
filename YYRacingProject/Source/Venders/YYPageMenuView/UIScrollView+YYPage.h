//
//  UIScrollView+YYPageMenuContentView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (YYPage)

/** 判断指定的frame 是否在当前屏幕的可视范围内 **/
- (BOOL)yyPageMenu_isNeedDisplayWithFrame:(CGRect)frame preloading:(BOOL)preloading;

/** 判断item 的frame是否需要显示在菜单栏上 **/
- (BOOL)yyPageMenu_isItemNeedDisplayWithFrame:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
