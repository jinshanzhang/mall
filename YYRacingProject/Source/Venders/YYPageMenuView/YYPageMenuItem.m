//
//  YYPageMenuItem.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPageMenuItem.h"
#import "YYPageMenuView.h"

@interface YYPageMenuItem()

@property (nonatomic, strong) UILabel  *titleLbl;

@end

@implementation YYPageMenuItem

#pragma mark - Init

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialization];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialization];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self initialization];
    }
    return self;
}

- (void)initialization {
    
}

#pragma mark - Public method

+ (instancetype)menuView:(YYPageMenuView *)menuView itemForIndex:(NSInteger)index {
    [self registerItemCollectionView:menuView.menuCollectionView];
    Class currentCls = [self class];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    YYPageMenuItem *itemView = (YYPageMenuItem *)[menuView.menuCollectionView dequeueReusableCellWithReuseIdentifier:strFromCls(currentCls) forIndexPath:indexPath];
    return itemView;
}

- (void)setSelected:(BOOL)selected withAnimation:(BOOL)animation {
    _itemSelected = selected;
    if (selected) {
        [self menuItemSelectedStyle];
    }
    else {
        [self menuItemNormalStyle];
    }
}

#pragma mark - Other method

- (void)menuItemNormalStyle {
   
}

- (void)menuItemSelectedStyle {
    
}

+ (void)registerItemCollectionView:(UICollectionView *)collectionView {
    // 如果存在对应的xib，就注册nib。如果捕获到异常，就注册class
    /*@try {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([self class])];
    } @catch (NSException *exception) {
        [collectionView registerClass:[self class] forCellWithReuseIdentifier:NSStringFromClass([self class])];
    }*/
    [collectionView registerClass:[self class] forCellWithReuseIdentifier:NSStringFromClass([self class])];
}

@end
