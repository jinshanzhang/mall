//
//  YYPageMenuContentView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class YYPageContentView;
/**
 *  数据源协议
 */
@protocol YYPageContentViewDataSource <NSObject>
/**
 *  根据索引获取对应的控制器
 *
 *  @param contentView self
 *  @param pageIndex   索引
 *
 *  @return 当前索引对应的控制器
 */
- (nullable UIViewController *)contentView:(YYPageContentView *)contentView viewControllerAtPage:(NSUInteger)pageIndex;

@end

@interface YYPageContentView : UIScrollView
/** 数据源 **/
@property (nonatomic, weak, nullable) id <YYPageContentViewDataSource> dataSource;

/** 页面数量 **/
@property (nonatomic, assign) NSUInteger pageCount;

/** 当前页面索引 **/
@property (nonatomic, assign) NSUInteger currentPage;

/** 是否需要预加载下一页 **/
@property (nonatomic, assign) BOOL needPreloading;

@property (nonatomic, assign, readonly, getter=isSwitching) BOOL switching;

/** 当前屏幕上已加载的控制器 **/
@property (nonatomic, strong, readonly) NSArray *visibleList;

/** 当前屏幕上已加载的控制器 **/
@property (nonatomic, strong, readonly) NSArray<__kindof UIViewController *> *viewControllers;


/** 刷新数据 **/
- (void)reloadData;

/** 重置所以内容页frame **/
- (void)resetPageFrames;

/** 清楚所以缓存的页面 **/
- (void)clearMemoryCache;

/** 根据控制器获取对应的页面索引，仅当前现实的和预加载的控制有索引 **/
- (NSInteger)pageIndexForViewController:(nullable UIViewController *)viewController;

/** 根据页面索引获取对应页面的frame **/
- (CGRect)frameOfViewControllerAtPage:(NSUInteger)pageIndex;

/** 获取索引对应的ViewController **/
- (nullable UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex;

/** 根据索引生成对应的ViewController，若对应ViewController已经存在，则直接返回 **/
- (nullable UIViewController *)creatViewControllerAtPage:(NSUInteger)pageIndex;

/** 获取索引对应的ViewController，当ViewController为nil时，根据autoCreate的值决定是否创建 **/
- (nullable UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex autoCreate:(BOOL)autoCreate;

/** 根据缓存标识查询可重用的UIViewController **/
- (nullable __kindof UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier;

@end

NS_ASSUME_NONNULL_END
