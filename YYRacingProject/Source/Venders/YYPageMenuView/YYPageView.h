//
//  YYPageView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YYPageMenuView.h"
#import "YYPageMenuItem.h"
#import "YYPageContentView.h"
#import "YYPageViewController.h"
#import "YYPageMenuProtocol.h"

NS_ASSUME_NONNULL_BEGIN
@class YYPageView, YYPageMenuItem, YYPageMenuView;
@protocol YYPageViewDataSource <NSObject>

@required
/** 获取所以menu 需要展示的内容 **/
- (NSArray *)menuTitlesForPageView:(YYPageView *)pageView;

/** 获取某个位置的item**/
- (YYPageMenuItem *)menuCellForItemAtIndex:(NSInteger)itemIndex;

/** 根据pageIndex加载对应的页面控制权 **/
- (UIViewController *)pageView:(YYPageView *)pageView viewControllerAtPage:(NSUInteger)pageIndex;

@end


@protocol YYPageViewDelegate <NSObject>

@optional

/** 视图控制器显示到当前屏幕上时触发 **/
- (void)pageView:(YYPageView *)pageView viewDidAppear:(__kindof UIViewController *)viewController atPage:(NSUInteger)pageIndex;

/** 视图控制器从屏幕上消失时触发 **/
- (void)pageView:(YYPageView *)pageView viewDidDisappear:(__kindof UIViewController *)viewController atPage:(NSUInteger)pageIndex;

@end

@interface YYPageView : UIView

@property (nonatomic, weak, nullable) id<YYPageViewDelegate> delegate;
@property (nonatomic, weak, nullable) id<YYPageViewDataSource> dataSource;

/** 主控制器，若delegate遵循协议VTMagicProtocol，则默认与其相同 **/
@property (nonatomic, weak, nullable) UIViewController<YYPageMenuProtocol> *pageController;

/** 当前屏幕上已加载的控制器 **/
@property (nonatomic, strong, readonly) NSArray<__kindof UIViewController *> *viewControllers;

/** 是否允许页面左右滑动，默认YES **/
@property (nonatomic, assign, getter=isScrollEnabled) BOOL scrollEnabled;

/** 是否允许切换，包括左右滑动和点击切换，默认YES **/
@property (nonatomic, assign, getter=isSwitchEnabled) BOOL switchEnabled;

/** 点击导航菜单切换页面时是否需要动画，默认YES **/
@property (nonatomic, assign, getter=isSwitchAnimated) BOOL switchAnimated;

/** 是否允许导航菜单左右滑动，默认YES **/
@property (nonatomic, assign, getter=isMenuScrollEnabled) BOOL menuScrollEnabled;

/** menu 高度 **/
@property (nonatomic, assign) CGFloat  menuHeight;

/** menu size**/
@property (nonatomic, assign) CGSize   menuSize;

/** 是否需要预加载下一页，默认YES **/
@property (nonatomic, assign) BOOL needPreloading;

/** 是否正在切换中，仅动画切换时为YES **/
@property (nonatomic, assign, getter=isSwitching) BOOL switching;

@property (nonatomic, strong) YYPageMenuView *menuView;  //顶部菜单视图
@property (nonatomic, strong) YYPageContentView *contentView; //容器视图

/** 重新加载所有数据 **/
- (void)reloadData;

/** 重新加载所有数据，同时定位到指定页面，若page越界，则自动修正为0 **/
- (void)reloadDataToPage:(NSUInteger)pageIndex;

/** 更新菜单标题，但不重新加载页面 **/
- (void)reloadMenuTitles;

/** 根据缓存标识获取可重用的UIViewController **/
- (nullable __kindof UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier;

/** 根据控制器获取对应的页面索引，仅当前显示的和预加载的控制器有相应索引 **/
- (NSInteger)pageIndexForViewController:(UIViewController *)viewController;

/** 获取索引对应的ViewController **/
- (nullable __kindof UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex;

/** 切换到指定页面 **/
- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated;

/** 清除所有缓存的页面 **/
- (void)clearMemoryCache;

@end

NS_ASSUME_NONNULL_END
