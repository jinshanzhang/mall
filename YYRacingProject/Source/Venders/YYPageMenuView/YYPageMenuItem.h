//
//  YYPageMenuItem.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YYPageMenuView;
@interface YYPageMenuItem : UICollectionViewCell
/** item的选中状态 */
@property (nonatomic, assign, getter=isItemSelected) BOOL itemSelected;

/**
 设置item的选中状态
 
 @param selected 是否选中
 @param animation 是否动画执行
 */
- (void)setSelected:(BOOL)selected withAnimation:(BOOL)animation;

/**
 初始化，推荐使用该初始化方法，内部已经帮你做了注册cell等操作
 
 @param menuView 菜单视图
 @param index index
 @return JHPageMenuItem
 */
+ (instancetype)menuView:(YYPageMenuView *)menuView itemForIndex:(NSInteger)index;

@end

NS_ASSUME_NONNULL_END
