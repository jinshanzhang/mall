//
//  UIScrollView+YYPageMenuContentView.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UIScrollView+YYPage.h"

@implementation UIScrollView (YYPage)

- (BOOL)yyPageMenu_isNeedDisplayWithFrame:(CGRect)frame preloading:(BOOL)preloading {
    CGRect visibleRect = (CGRect){ CGPointMake(self.contentOffset.x, 0), self.frame.size };
    // 获取二个rect 的交集
    CGRect intersectRegion = CGRectIntersection(frame, visibleRect);
    BOOL isOnScreen = !CGRectIsNull(intersectRegion) || !CGRectIsEmpty(intersectRegion);
    if (!preloading) {
        BOOL isNotBorder = 0 != (int)self.contentOffset.x%(int)self.frame.size.width;
        return isOnScreen && (isNotBorder ?: 0 != intersectRegion.size.width);
    }
    return isOnScreen;
}

- (BOOL)yyPageMenu_isItemNeedDisplayWithFrame:(CGRect)frame {
    frame.size.width *= 2;
    BOOL isOnScreen = [self yyPageMenu_isNeedDisplayWithFrame:frame preloading:YES];
    if (isOnScreen) {
        return YES;
    }
    frame.size.width *= 0.5;
    frame.origin.x -= frame.size.width;
    isOnScreen = [self yyPageMenu_isNeedDisplayWithFrame:frame preloading:YES];
    return isOnScreen;
}
@end
