//
//  YYPageMenuView.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPageMenuView.h"
#import "YYPageMenuItem.h"
@interface YYPageMenuView()
<UICollectionViewDelegate, UICollectionViewDataSource>

/** collectionView布局对象 */
@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
/** 菜单个数 */
@property (nonatomic, assign) NSInteger menuCount;
/** 前一次选中的菜单下标 */
@property (nonatomic, assign) NSInteger beforeSelectIndex;
/** 当前选中的菜单下标 */
@property (nonatomic, assign) NSInteger selectIndex;

@end


@implementation YYPageMenuView

#pragma mark - Getter and Setter

- (UICollectionView *)menuCollectionView {
    if (!_menuCollectionView) {
        _menuCollectionView                                = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
        _menuCollectionView.backgroundColor                = [UIColor clearColor];
        _menuCollectionView.showsHorizontalScrollIndicator = NO;
        _menuCollectionView.showsVerticalScrollIndicator   = NO;
        _menuCollectionView.delegate                       = self;
        _menuCollectionView.dataSource                     = self;
    }
    return _menuCollectionView;
}

- (UICollectionViewFlowLayout *)flowLayout {
    if (!_flowLayout) {
        _flowLayout                     = [[UICollectionViewFlowLayout alloc] init];
        _flowLayout.minimumLineSpacing  = 0;
        _flowLayout.minimumLineSpacing = 0.f;
        _flowLayout.sectionInset = UIEdgeInsetsZero;
        _flowLayout.scrollDirection    = self.scrollDirection == YYPageMenuScrollDirectionHorizontal ? UICollectionViewScrollDirectionHorizontal : UICollectionViewScrollDirectionVertical;
    }
    return _flowLayout;
}

- (NSInteger)menuCount {
    NSInteger menuCount = [self.dataSource numbersOfItemsInMenuView:self];
    return menuCount;
}

- (void)setScrollDirection:(YYPageMenuScrollDirection)scrollDirection {
    _scrollDirection = scrollDirection;
    self.flowLayout.scrollDirection = self.scrollDirection == YYPageMenuScrollDirectionHorizontal ? UICollectionViewScrollDirectionHorizontal : UICollectionViewScrollDirectionVertical;;
}

- (void)setCurrentIndex:(NSUInteger)currentIndex {
    _currentIndex = currentIndex;
    if (_currentIndex >= 0 && _currentIndex < self.menuCount) {
        NSIndexPath *selectIndexPath = [NSIndexPath indexPathForItem:_currentIndex inSection:0];
        [self.menuCollectionView layoutIfNeeded];
        YYPageMenuItem *item = (YYPageMenuItem *)[self.menuCollectionView cellForItemAtIndexPath:selectIndexPath];

        self.beforeSelectIndex = self.selectIndex == -1 ? 0 : self.selectIndex;
        self.selectIndex = _currentIndex;
        [self.menuCollectionView reloadData];
        if (!item) {
            item = (YYPageMenuItem *)[self.menuCollectionView cellForItemAtIndexPath:selectIndexPath];
            if (!item) {
                [self.menuCollectionView scrollToItemAtIndexPath:selectIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
                [self.menuCollectionView layoutIfNeeded];
                item = (YYPageMenuItem *)[self.menuCollectionView cellForItemAtIndexPath:selectIndexPath];
            }
        }
        [self refreshContentOffsetItemFrame:item.frame];
    }
}

#pragma mark - Init

// 代码初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialization];
    }
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        [self initialization];
    }
    return self;
}

// xib初始化
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self initialization];
    }
    return self;
}

- (void)initialization {
    self.selectIndex = -1;
    self.beforeSelectIndex = -1;
    self.menuSize = CGSizeZero;

    [self addSubview:self.menuCollectionView];
    [self.menuCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(self);
    }];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.menuSize;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.menuCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YYPageMenuItem *item = [self.dataSource menuView:self menuCellForItemAtIndex:indexPath.row];
    if (indexPath.row == self.selectIndex) {
        [item setSelected:YES withAnimation:YES];
    } else {
        if (indexPath.row == self.beforeSelectIndex) {
            [item setSelected:NO withAnimation:YES];
        } else {
            [item setSelected:NO withAnimation:NO];
        }
    }
    return item;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self selectItemAtIndex:indexPath.row withAnimation:NO];
    });
}

#pragma mark - Private method

- (void)selectItemAtIndex:(NSInteger)index withAnimation:(BOOL)animation {
    if (index == -1) {
        index = 0;
    }
    [self.menuCollectionView layoutIfNeeded];
    NSIndexPath *selectIndexPath = [NSIndexPath indexPathForItem:index inSection:0];
    YYPageMenuItem *item = (YYPageMenuItem *)[self.menuCollectionView cellForItemAtIndexPath:selectIndexPath];
    if (!item) {
        return;
    }
    if (index != self.selectIndex && [self.delegate respondsToSelector:@selector(menuView:didSelectIndex:)]) {
        [self.delegate menuView:self didSelectIndex:selectIndexPath.row];
        self.beforeSelectIndex = self.selectIndex == -1 ? 0 : self.selectIndex;
        self.selectIndex = index;
        [self.menuCollectionView reloadData];
    }
    [self refreshContentOffsetItemFrame:item.frame];
}

// 让选中的item位于中间
- (void)refreshContentOffsetItemFrame:(CGRect)frame {
    CGSize contentSize = self.menuCollectionView.contentSize;
    if (self.scrollDirection == YYPageMenuScrollDirectionHorizontal) {
        
        if (contentSize.width <= self.menuCollectionView.frame.size.width) {
            return;
        }
        CGFloat itemX = frame.origin.x;
        CGFloat itemWidth = self.menuCollectionView.frame.size.width;
        if (itemX > itemWidth/2) {
            CGFloat targetX;
            if ((contentSize.width - itemX) <= itemWidth/2) {
                targetX = contentSize.width - itemWidth;
            }
            else {
                targetX = itemX - itemWidth/2 + frame.size.width/2;
            }
            if (targetX + itemWidth > contentSize.width) {
                targetX = contentSize.width - itemWidth;
            }
            [UIView animateWithDuration:0.25 animations:^{
                [self.menuCollectionView setContentOffset:CGPointMake(targetX, 0) animated:YES];
            }];
        }
        else {
            [UIView animateWithDuration:0.25 animations:^{
                [self.menuCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
            }];
        }
    }
    else {
        if (contentSize.height <= self.menuCollectionView.frame.size.height) { return; }
        
        CGFloat itemY = frame.origin.y;
        CGFloat itemHeight = self.menuCollectionView.frame.size.height;
        if (itemY > itemHeight/2) {
            CGFloat targetY;
            if ((contentSize.height-itemY) <= itemHeight/2) {
                targetY = contentSize.height - itemHeight;
            } else {
                targetY = itemY - itemHeight/2 + frame.size.height/2;
            }
            if (targetY + itemHeight > contentSize.height) {
                targetY = contentSize.height - itemHeight;
            }
            [UIView animateWithDuration:0.25 animations:^{
                [self.menuCollectionView setContentOffset:CGPointMake(targetY, 0) animated:YES];
            }];
        } else {
            [UIView animateWithDuration:0.25 animations:^{
                [self.menuCollectionView setContentOffset:CGPointMake(0, 0) animated:YES];
            }];
        }
    }
}

#pragma mark - Public method
- (void)reloadData {
    if (self.selectIndex > (self.menuCount - 1)) {
        self.selectIndex = 0;
    }
    [self.menuCollectionView reloadData];
}

@end
