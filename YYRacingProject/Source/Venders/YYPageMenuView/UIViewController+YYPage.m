//
//  UIViewController+YYPageMenuContentView.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UIViewController+YYPage.h"
#import <objc/runtime.h>
static const void *kVTReuseIdentifier = &kVTReuseIdentifier;

@implementation UIViewController (YYPage)

- (void)setReuseIdentifier:(NSString *)reuseIdentifier {
    objc_setAssociatedObject(self, kVTReuseIdentifier, reuseIdentifier, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)reuseIdentifier {
    return objc_getAssociatedObject(self, kVTReuseIdentifier);
}

- (UIViewController<YYPageMenuProtocol> *)magicController {
    UIViewController *viewController = self.parentViewController;
    while (viewController) {
        if ([viewController conformsToProtocol:@protocol(YYPageMenuProtocol)]) break;
        viewController = viewController.parentViewController;
    }
    return (UIViewController<YYPageMenuProtocol> *)viewController;
}

- (NSInteger)currentPageIndex {
    return -1;
}


@end
