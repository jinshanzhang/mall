//
//  YYPageViewController.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPageView.h"
#import "YYPageMenuProtocol.h"
#import "UIViewController+YYPage.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYPageViewController : UIViewController
<YYPageMenuProtocol>

@property (nonatomic, strong) YYPageView *contentView;
@property (nonatomic, assign) NSUInteger currentPage;

@property (nonatomic, assign) YYPageAppearanceState appearanceState;

/**
 *  当前显示的控制器
 */
@property (nonatomic, strong, nullable) __kindof UIViewController *currentViewController;

/**
 *  屏幕上可见的控制器
 */
@property (nonatomic, strong, readonly) NSArray<__kindof UIViewController *> *viewControllers;

/**
 *  获取索引对应的ViewController
 *  若index超出范围或对应控制器不可见，则返回nil
 *
 *  @param pageIndex 索引
 *
 *  @return UIViewController对象
 */
- (nullable __kindof UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex;

/**
 *  切换到指定页面
 *
 *  @param pageIndex 页面索引
 *  @param animated  是否需要动画执行
 */
- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated;
@end

NS_ASSUME_NONNULL_END
