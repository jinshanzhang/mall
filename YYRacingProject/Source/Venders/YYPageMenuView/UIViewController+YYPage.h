//
//  UIViewController+YYPageMenuContentView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPageMenuProtocol.h"
NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (YYPage)<YYPageMenuCtrlReuseProtocol>

/** 缓存重用标识 **/
@property (nonatomic, copy) NSString *reuseIdentifier;

/** 主控制器 **/
@property (nonatomic, weak, readonly) UIViewController<YYPageMenuProtocol> *magicController;

/** 当前控制器的页面索引，仅当前显示的和预加载的控制器有相应索引， **/
- (NSInteger)currentPageIndex;
@end

NS_ASSUME_NONNULL_END
