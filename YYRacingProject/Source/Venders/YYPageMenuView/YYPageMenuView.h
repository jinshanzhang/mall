//
//  YYPageMenuView.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class YYPageMenuView, YYPageMenuItem;

@protocol YYPageMenuViewDataSource <NSObject>
@required
/**
 告知 "YYPageMenuView" 需要多少个菜单
 
 @param menuView YYPageMenuView
 @return 菜单个数
 */
- (NSInteger)numbersOfItemsInMenuView:(YYPageMenuView *)menuView;

/**
 告知 "YYPageMenuView" 每个菜单的内容视图
 
 @param menuView YYPageMenuView
 @param index 当前需要展示内容视图的位置
 @return 菜单内容视图
 */
- (YYPageMenuItem *)menuView:(YYPageMenuView *)menuView menuCellForItemAtIndex:(NSInteger)index;
@optional

@end


@protocol YYPageMenuViewDelegate <NSObject>
@optional

/**
 当点击菜单子视图的时候，你可能需要做些事情，可以在这里面实现
 
 @param menuView YYPageMenuView
 @param index 当前点击的菜单视图的位置
 */
- (void)menuView:(YYPageMenuView *)menuView didSelectIndex:(NSInteger)index;

@end

@interface YYPageMenuView : UIView

/** menu代理 */
@property (nonatomic, weak) id<YYPageMenuViewDelegate> delegate;
@property (nonatomic, weak) id<YYPageMenuViewDataSource> dataSource;

/** 菜单视图 **/
@property (nonatomic, strong) UICollectionView *menuCollectionView;

/** 菜单单元格大小 **/
@property (nonatomic, assign) CGSize menuSize;

/** 当前选中item对应的索引 **/
@property (nonatomic, assign) NSUInteger currentIndex;

/** 菜单视图滚动方向 **/
@property (nonatomic, assign) YYPageMenuScrollDirection scrollDirection;

/** 刷新数据 **/
- (void)reloadData;
@end

NS_ASSUME_NONNULL_END
