//
//  YYPageMenuProtocol.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class YYPageView;
@protocol YYPageMenuProtocol <NSObject>

@required

/** 设置获取当前页索引 **/
- (NSInteger)currentPage;
- (void)setCurrentPage:(NSInteger)currentPage;

/** 设置获取当前页控制器 **/
- (__kindof UIViewController *)currentViewController;
- (void)setCurrentViewController:(UIViewController *)currentViewController;

- (YYPageAppearanceState)appearanceState;

- (YYPageView *)contentView;
@end

@protocol YYPageMenuCtrlReuseProtocol <NSObject>

@optional
- (void)prepareForReuse;

@end


NS_ASSUME_NONNULL_END
