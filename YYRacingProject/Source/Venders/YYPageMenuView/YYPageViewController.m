//
//  YYPageViewController.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPageViewController.h"

@interface YYPageViewController ()

@end

@implementation YYPageViewController

#pragma mark - Setter method
- (YYPageView *)contentView {
    if (!_contentView) {
        _contentView = [[YYPageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _contentView.autoresizesSubviews = YES;
        _contentView.pageController = self;
        [self.view setNeedsLayout];
    }
    return _contentView;
}

- (NSArray<UIViewController *> *)viewControllers {
    return self.contentView.viewControllers;
}

#pragma mark - Life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        } else if ([self respondsToSelector:@selector(setWantsFullScreenLayout:)]) {
            [self setValue:@YES forKey:@"wantsFullScreenLayout"];
        }
    }
    return self;
}

- (void)loadView {
    [super loadView];
    
    self.view = self.contentView;
}

/*- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.appearanceState = YYPageAppearanceStateWillAppear;
    if (!_contentView.isSwitching) {
        [_currentViewController beginAppearanceTransition:NO animated:animated];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.appearanceState = YYPageAppearanceStateDidAppear;
    if (!_contentView.isSwitching) {
        [_currentViewController endAppearanceTransition];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.appearanceState = YYPageAppearanceStateWillDisappear;
    if (!_contentView.isSwitching) {
        [_currentViewController beginAppearanceTransition:NO animated:animated];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.appearanceState = YYPageAppearanceStateDidDisappear;
    if (!_contentView.isSwitching) {
        [_currentViewController endAppearanceTransition];
    }
}*/

#pragma mark - Public method
- (UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex {
    return [self.contentView viewControllerAtPage:pageIndex];
}

- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated {
    [self.contentView switchToPage:pageIndex animated:animated];
}

#pragma mark - forward appearance methods
//  为了接管处理这些外观变化的回调， 需要在你的容器类视图中重写。（这些外观变化指）多个子视图控制器同时更新它们view的状态， 你可能会想合并这些变化。
- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

@end
