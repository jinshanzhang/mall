//
//  YYPageMenuConstant.h
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#ifndef YYPageMenuConstant_h
#define YYPageMenuConstant_h


/**
 菜单滚动方向
 */
typedef NS_ENUM(NSInteger, YYPageMenuScrollDirection) {
    YYPageMenuScrollDirectionHorizontal, // 水平
    YYPageMenuScrollDirectionVertical    // 垂直
};

/** 页面生命周期状态状态 */
typedef NS_ENUM(NSUInteger, YYPageAppearanceState) {
    /** 默认状态，已经消失 */
    YYPageAppearanceStateDidDisappear,
    /** 即将消失 */
    YYPageAppearanceStateWillDisappear,
    /** 即将显示 */
    YYPageAppearanceStateWillAppear,
    /** 已经显示 */
    YYPageAppearanceStateDidAppear,
};

#endif /* YYPageMenuConstant_h */
