//
//  YYPageView.m
//  OCBaseProject
//
//  Created by 蔡金明 on 2018/9/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPageView.h"

#import <objc/runtime.h>

typedef struct {
    unsigned int dataSourceMenuItemTitle : 1;
    unsigned int dataSourceViewController : 1;
    unsigned int viewControllerDidAppear : 1;
    unsigned int viewControllerDidDisappear : 1;
    unsigned int shouldManualForwardAppearanceMethods : 1;
} PageFlags;

static const void *kPageView = &kPageView;
@implementation UIViewController (PageViewPrivate)

- (void)setPageView:(YYPageView *)pageView {
    objc_setAssociatedObject(self, kPageView, pageView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YYPageView *)pageView {
    return objc_getAssociatedObject(self, kPageView);
}

@end

@interface YYPageView()
<
UIScrollViewDelegate,
YYPageMenuViewDelegate,
YYPageMenuViewDataSource,
YYPageContentViewDataSource>

@property (nonatomic, assign) BOOL isViewWillAppear;
@property (nonatomic, assign) BOOL needSkipUpdate; // 是否是跳页切换
@property (nonatomic, assign) PageFlags pageFlags;
@property (nonatomic, strong) NSArray *menuTitles; // 顶部分类名数组


@property (nonatomic, assign) NSInteger scrollEndPage; // 滚动结束的页码
@property (nonatomic, assign) NSInteger nextPageIndex; // 下一个页面的索引
@property (nonatomic, assign) NSInteger currentPage;   // 当前页面的索引
@property (nonatomic, assign) NSInteger previousIndex; // 上一个页面的索引

@end

@implementation YYPageView
#pragma mark - Setter and Getter
- (YYPageMenuView *)menuView {
    if (!_menuView) {
        _menuView = [[YYPageMenuView alloc] init];
        _menuView.delegate = self;
        _menuView.dataSource = self;
        _menuView.menuSize = self.menuSize;
        _menuView.scrollDirection = YYPageMenuScrollDirectionHorizontal;
    }
    return _menuView;
}

- (YYPageContentView *)contentView {
    if (!_contentView) {
        _contentView = [[YYPageContentView alloc] init];
        _contentView.dataSource = self;
        _contentView.delegate = self;
        _contentView.bounces = NO;
        _contentView.showsVerticalScrollIndicator = NO;
        _contentView.showsHorizontalScrollIndicator = NO;
        _contentView.scrollsToTop = NO;
        _contentView.pagingEnabled = YES;
    }
    return _contentView;
}

- (NSArray<UIViewController *> *)viewControllers {
    return [_contentView visibleList];
}

#pragma mark basic configurations
- (void)setDataSource:(id<YYPageViewDataSource>)dataSource {
    _dataSource = dataSource;
    _pageFlags.dataSourceMenuItemTitle = [dataSource respondsToSelector:@selector(menuTitlesForPageView:)];
    _pageFlags.dataSourceViewController = [dataSource respondsToSelector:@selector(pageView:viewControllerAtPage:)];
}

- (void)setDelegate:(id<YYPageViewDelegate>)delegate {
    _delegate = delegate;
    _pageFlags.viewControllerDidAppear = [delegate respondsToSelector:@selector(pageView:viewDidAppear:atPage:)];
    _pageFlags.viewControllerDidDisappear = [delegate respondsToSelector:@selector(pageView:viewDidDisappear:atPage:)];
    if (!_pageController && [_delegate isKindOfClass:[UIViewController class]] && [delegate conformsToProtocol:@protocol(YYPageMenuProtocol)]) {
        self.pageController = (UIViewController<YYPageMenuProtocol> *)delegate;
    }
}

- (void)setPageController:(UIViewController<YYPageMenuProtocol> *)pageController {
    _pageController = pageController;
    if (!_pageController.contentView) {
        [_pageController setPageView:self];
    }
    if ([pageController respondsToSelector:@selector(shouldAutomaticallyForwardAppearanceMethods)]) {
        _pageFlags.shouldManualForwardAppearanceMethods = ![pageController shouldAutomaticallyForwardAppearanceMethods];
    }
}

- (void)setMenuSize:(CGSize)menuSize {
    _menuSize = menuSize;
    self.menuView.menuSize = _menuSize;
}

- (void)setCurrentPage:(NSInteger)currentPage {
    if (currentPage < 0) {
        return;
    }
    NSInteger disIndex = _currentPage;
    _currentPage = currentPage;
    _previousIndex = disIndex;
    _menuView.currentIndex = currentPage;
    [self displayPageHasChanged:currentPage disIndex:disIndex];
    [self viewControllerDidDisappear:disIndex];
    [self viewControllerDidAppear:currentPage];
}

#pragma mark - 视图即将显示
- (void)subviewWillAppearAtPage:(NSInteger)pageIndex {
    if (_nextPageIndex == pageIndex) {
        return;
    }
    
    if (_contentView.isDragging && 1 < ABS(_nextPageIndex - pageIndex)) {
        [self viewControllerWillDisappear:_nextPageIndex];
        [self viewControllerDidDisappear:_nextPageIndex];
    }
    [self viewControllerWillDisappear:_currentPage];
    [self viewControllerWillAppear:pageIndex];
    _nextPageIndex = pageIndex;
}

#pragma mark - the life cycle of view controller
- (void)viewControllerWillAppear:(NSUInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    
    UIViewController *viewController = [_contentView viewControllerAtPage:pageIndex autoCreate:YES];
    [viewController beginAppearanceTransition:YES animated:NO];
}

- (void)viewControllerDidAppear:(NSUInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    
    UIViewController *viewController = [self viewControllerAtPage:pageIndex];
    [viewController endAppearanceTransition];
}

- (void)viewControllerWillDisappear:(NSUInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    
    UIViewController *viewController = [self viewControllerAtPage:pageIndex];
    [viewController beginAppearanceTransition:NO animated:NO];
}

- (void)viewControllerDidDisappear:(NSUInteger)pageIndex {
    if (![self shouldForwardAppearanceMethods]) {
        return;
    }
    
    UIViewController *viewController = [self viewControllerAtPage:pageIndex];
    [viewController endAppearanceTransition];
}

- (BOOL)shouldForwardAppearanceMethods {
    return _pageFlags.shouldManualForwardAppearanceMethods &&
    (YYPageAppearanceStateDidAppear == _pageController.appearanceState ||
     YYPageAppearanceStateWillAppear == _pageController.appearanceState);
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self configValues];
        [self addPageSubViews];
        [self updateFrameForSubViews];
    }
    return self;
}

- (void)configValues {
    self.menuHeight = 44.0;
    self.menuSize = CGSizeZero;
    _scrollEnabled = YES;
    _switchEnabled = YES;
    _needPreloading = YES;
    _switchAnimated = YES;
    _menuScrollEnabled = YES;
    self.scrollEndPage = -1;
}

- (void)addPageSubViews {
    [self addSubview:self.menuView];
    [self addSubview:self.contentView];
}


- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateFrameForSubViews];
}

- (void)updateFrameForSubViews {
    CGFloat y = 0;
    CGSize size = self.frame.size;
    self.menuView.frame = CGRectMake(0, y, size.width, self.menuHeight);
    
    CGFloat menuY = y + self.menuHeight;
    self.needSkipUpdate = YES;
    CGRect originalContentFrame = _contentView.frame;
    CGFloat contentH = size.height - menuY;
    _contentView.frame = CGRectMake(0, menuY, size.width, contentH);
    if (!CGRectEqualToRect(_contentView.frame, originalContentFrame)) {
        [_contentView resetPageFrames];
    }
    self.needSkipUpdate = NO;
    self.contentView.frame = CGRectMake(0, menuY, size.width, size.height - menuY);
}

- (void)switchWithoutAnimation:(NSUInteger)pageIndex {
    if (_menuTitles.count <= pageIndex) {
        return;
    }
    [_contentView creatViewControllerAtPage:_currentPage];
    [_contentView creatViewControllerAtPage:pageIndex];
    
    [self subviewWillAppearAtPage:pageIndex];
    self.needSkipUpdate = YES;
    CGFloat offset = _contentView.frame.size.width * pageIndex;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.contentOffset = CGPointMake(offset, 0);
    }];
    self.needSkipUpdate = NO;
    
    _previousIndex = _currentPage;
    _currentPage = pageIndex;
    [self displayPageHasChanged:pageIndex disIndex:_previousIndex];
    [self viewControllerDidDisappear:_previousIndex];
    if (YYPageAppearanceStateWillAppear != _pageController.appearanceState) {
        [self viewControllerDidAppear:pageIndex];
    }
}

- (void)switchAnimation:(NSUInteger)pageIndex {
    if (_menuTitles.count <= pageIndex) {
        return;
    }
    
    _switching = YES;
    NSInteger disIndex = _currentPage;
    CGFloat contentWidth = CGRectGetWidth(_contentView.frame);
    BOOL isNotAdjacent = abs((int)(_currentPage - pageIndex)) > 1;
    if (isNotAdjacent) {// 当前按钮与选中按钮不相邻时
        self.needSkipUpdate = YES;
        _isViewWillAppear = YES;
        [self displayPageHasChanged:pageIndex disIndex:_currentPage];
        [self subviewWillAppearAtPage:pageIndex];
        [self viewControllerDidDisappear:disIndex];
        [_pageController setCurrentViewController:nil];
        NSInteger tempIndex = pageIndex + (_currentPage < pageIndex ? -1 : 1);
        [UIView animateWithDuration:0.25 animations:^{
            [self.contentView setContentOffset:CGPointMake(contentWidth * tempIndex, 0)];
        }];
        
        _isViewWillAppear = NO;
    } else {
        [self viewControllerWillDisappear:disIndex];
        [self viewControllerWillAppear:pageIndex];
    }
    
    _currentPage = pageIndex;
    _previousIndex = disIndex;
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.contentView setContentOffset:CGPointMake(contentWidth * pageIndex, 0)];
    }];
    
    [self displayPageHasChanged:self.currentPage disIndex:disIndex];
    if (!isNotAdjacent && self.currentPage != disIndex) {
        [self viewControllerDidDisappear:disIndex];
    }
    if (pageIndex == self.currentPage) {
        [self viewControllerDidAppear:pageIndex];
    }
    self.needSkipUpdate = NO;
    self.switching = NO;
}

#pragma mark - display page has changed
- (void)displayPageHasChanged:(NSInteger)pageIndex disIndex:(NSInteger)disIndex {
    UIViewController *appearViewController = [self autoCreateViewControllAtPage:pageIndex];
    UIViewController *disappearViewController = [self autoCreateViewControllAtPage:disIndex];
    
    if (appearViewController) {
        [_pageController setCurrentPage:pageIndex];
        [_pageController setCurrentViewController:appearViewController];
    }
    
    if (disappearViewController && _pageFlags.viewControllerDidDisappear) {
        [_delegate pageView:self viewDidDisappear:disappearViewController atPage:disIndex];
    }
    
    if (appearViewController && _pageFlags.viewControllerDidAppear) {
        [_delegate pageView:self viewDidAppear:appearViewController atPage:pageIndex];
    }
}

- (UIViewController *)autoCreateViewControllAtPage:(NSInteger)pageIndex {
    return [_contentView viewControllerAtPage:pageIndex autoCreate:!_needSkipUpdate];
}

#pragma mark - YYPageMenuViewDataSource
- (NSInteger)numbersOfItemsInMenuView:(YYPageMenuView *)menuView {
    if (_dataSource  && [_dataSource respondsToSelector:@selector(menuTitlesForPageView:)]) {
        NSArray *menuTitles = [_dataSource menuTitlesForPageView:self];
        return menuTitles.count;
    }
    return -1;
}

- (YYPageMenuItem *)menuView:(YYPageMenuView *)menuView menuCellForItemAtIndex:(NSInteger)index {
    if (_dataSource && [_dataSource respondsToSelector:@selector(menuCellForItemAtIndex:)]) {
        return [_dataSource menuCellForItemAtIndex:index];
    }
    return [YYPageMenuItem menuView:menuView itemForIndex:-1];
}

#pragma mark - YYPageMenuViewDelegate
- (void)menuView:(YYPageMenuView *)menuView didSelectIndex:(NSInteger)index {
    if (!_switchEnabled) {
        return;
    }
    if (_switchAnimated && _needPreloading) {
        [self switchAnimation:index];
    } else {
        [self switchWithoutAnimation:index];
    }
}

#pragma mark - functional methods
- (void)reloadData {
    [self reloadDataWithDisIndex:_currentPage];
}

- (void)reloadDataToPage:(NSUInteger)pageIndex {
    _previousIndex = _currentPage;
    _currentPage = pageIndex;
    _nextPageIndex = pageIndex;
    _menuView.currentIndex = _currentPage;
    _contentView.currentPage = pageIndex;
    
    [self reloadDataWithDisIndex:_previousIndex];
}

- (void)reloadDataWithDisIndex:(NSInteger)disIndex {
    UIViewController *viewController = [self viewControllerAtPage:disIndex];
    if (viewController && _pageFlags.viewControllerDidDisappear) {
        [_delegate pageView:self viewDidDisappear:viewController atPage:disIndex];
    }
    [self viewControllerWillDisappear:disIndex];
    [self viewControllerDidDisappear:disIndex];
    
    if (_pageFlags.dataSourceMenuItemTitle) {
        _menuTitles = [_dataSource menuTitlesForPageView:self];
    }
    if (_menuTitles.count <= _currentPage) {
        _currentPage = 0;
        _nextPageIndex = _currentPage;
        _previousIndex = _currentPage;
        _contentView.currentPage = _currentPage;
        [_pageController setCurrentViewController:nil];
        [_pageController setCurrentPage:_currentPage];
    }
    _contentView.pageCount = _menuTitles.count;
    [_contentView reloadData];
    [_menuView reloadData];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}


- (void)reloadMenuTitles {
    if (_pageFlags.dataSourceMenuItemTitle) {
        _menuTitles = [_dataSource menuTitlesForPageView:self];
    }
    [_menuView reloadData];
}

- (UIViewController *)dequeueReusablePageWithIdentifier:(NSString *)identifier {
    UIViewController *viewController = [_contentView dequeueReusablePageWithIdentifier:identifier];
    if ([viewController respondsToSelector:@selector(prepareForReuse)]) {
        [(id<YYPageMenuCtrlReuseProtocol>)viewController prepareForReuse];
    }
    
    for (UIViewController *childViewController in viewController.childViewControllers) {
        if ([childViewController respondsToSelector:@selector(prepareForReuse)]) {
            [(id<YYPageMenuCtrlReuseProtocol>)childViewController prepareForReuse];
        }
    }
    return viewController;
}


- (NSInteger)pageIndexForViewController:(UIViewController *)viewController {
    return [_contentView pageIndexForViewController:viewController];
}
- (UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex {
    return [_contentView viewControllerAtPage:pageIndex];
}
- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated {
    if (pageIndex == _currentPage || _menuTitles.count <= pageIndex) {
        return;
    }
    _contentView.currentPage = pageIndex;
    if (animated && _needPreloading) {
        [self switchAnimation:pageIndex];
    } else {
        [self switchWithoutAnimation:pageIndex];
    }
}

- (void)clearMemoryCache {
    [self.contentView clearMemoryCache];
}


#pragma mark - VTContentViewDataSource
- (UIViewController *)contentView:(YYPageContentView *)contentView viewControllerAtPage:(NSUInteger)pageIndex {
    if (!_pageFlags.dataSourceViewController) {
        return nil;
    }
    UIViewController *viewController = [_dataSource pageView:self viewControllerAtPage:pageIndex];
    if (viewController && ![viewController.parentViewController isEqual:_pageController]) {
        [_pageController addChildViewController:viewController];
        [contentView addSubview:viewController.view];
        [viewController didMoveToParentViewController:_pageController];
        // 设置默认的currentViewController，并触发viewDidAppear
        if (pageIndex == _currentPage) {
            [self resetCurrentViewController:viewController];
        }
    }
    return viewController;
}

- (void)resetCurrentViewController:(UIViewController *)viewController {
    [_pageController setCurrentPage:_currentPage];
    [_pageController setCurrentViewController:viewController];
    viewController.view.frame = [_contentView frameOfViewControllerAtPage:_currentPage];
    if (_pageFlags.viewControllerDidAppear) {
        [_delegate pageView:self viewDidAppear:viewController atPage:_currentPage];
    }
    
    if ([self shouldForwardAppearanceMethods]) {
        [viewController beginAppearanceTransition:NO animated:NO];
        if (YYPageAppearanceStateWillAppear != _pageController.appearanceState) {
            [viewController endAppearanceTransition];
        }
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![scrollView isEqual:_contentView] || _needSkipUpdate || CGRectIsEmpty(self.frame)) {
        return;
    }
    
    NSInteger newIndex;
    NSInteger tempIndex;
    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat scrollWidth = scrollView.frame.size.width;
    newIndex = ceilf(offsetX/scrollWidth);
    BOOL isSwipeToLeft = scrollWidth * _currentPage < offsetX;
    if (isSwipeToLeft) { // 向左滑动
        newIndex = floorf(offsetX/scrollWidth);
        tempIndex = (int)((offsetX + scrollWidth - 0.1)/scrollWidth);
    } else {
        newIndex = ceilf(offsetX/scrollWidth);
        tempIndex = (int)(offsetX/scrollWidth);
    }
    self.scrollEndPage = newIndex;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    _contentView.scrollEnabled = _scrollEnabled;
    if (!decelerate) {
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (![scrollView isEqual:_contentView]) {
        return;
    }
    if (self.scrollEndPage >= 0) {
        self.currentPage = self.scrollEndPage;
    }
}

@end
