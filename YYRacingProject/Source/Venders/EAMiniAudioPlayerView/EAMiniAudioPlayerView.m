//
//  EAAudioPlayerView.m
//  EAAudioPlayerView
//
//  Created by EA.Huang on 5/19/16.
//  Copyright © 2016 EAH. All rights reserved.
//

#import "EAMiniAudioPlayerView.h"
#import "UIButton+EAButtonExtendedHit.h"


@implementation YYAudioInfoModel

@end


@interface EAMiniAudioPlayerView ()
//Used to draw playing progress
@property (nonatomic, strong) UIView    *playButtonContentView;

@property (nonatomic, strong) CAShapeLayer *playProgressBackLayer;//播放进度

@property (nonatomic, strong) CAShapeLayer *playProgressLayer;//播放进度
//下载进度
@property (nonatomic, strong) CAShapeLayer *downloadProgressLayer;
//组件显示的图片
@property (nonatomic, strong) UIImageView *soundIcon;
//The label to display text
@property (nonatomic, strong ,readonly) UILabel* textLabel;
//播放按钮
@property (nonatomic, strong ,readonly) UIButton* playButton;

@property (nonatomic, strong) UIView  *line;

@property (nonatomic, strong) UIView  *infoView;
//关闭按钮
@property (nonatomic, strong) UIButton *cancelBtn;
//音频播放组件背景
@property (nonatomic, strong) UIImageView *showIMG;
//音频播放标题
@property (nonatomic, strong) UILabel *titleLabel;
//音频播放时间
@property (nonatomic, strong) UILabel *timeLabel;
//音频播放副标题
@property (nonatomic, strong) UILabel *detailLabel;
//图片旋转动画
@property (nonatomic, strong) CABasicAnimation *iconAnimation;

@end

static void  *EAAudioPlayerTextLabelContext = &EAAudioPlayerTextLabelContext;

@implementation EAMiniAudioPlayerView

#pragma mark - Setter
- (void)setVideoTitle:(NSString *)videoTitle {
    _videoTitle = videoTitle;
    self.titleLabel.text = _videoTitle;
}

- (void)setVideoSubTitle:(NSString *)videoSubTitle {
    _videoSubTitle = videoSubTitle;
    self.detailLabel.text = _videoSubTitle;
}

- (void)setVideoCoverUrl:(NSString *)videoCoverUrl {
    _videoCoverUrl = videoCoverUrl;
    [self.soundIcon sd_setImageWithURL:[NSURL URLWithString:_videoCoverUrl]];
}

- (void)setVideoBgUrl:(NSString *)videoBgUrl {
    _videoBgUrl = videoBgUrl;
    [self.showIMG sd_setImageWithURL:[NSURL URLWithString:_videoBgUrl]];
}

- (void)setVideoDurationStr:(NSString *)videoDurationStr {
    _videoDurationStr = videoDurationStr;
    self.timeLabel.text = _videoDurationStr;
}

- (void)setStyleConfig:(EAMiniAudioPlayerStyleConfig *)styleConfig
{
    _styleConfig = styleConfig;
    [self setNeedsLayout];
}

- (void)setPlayProgress:(CGFloat)progress
{
    _playProgress = progress;
    [self updatePlayProgress];
}

- (void)setPlayStatus:(MiniAudioPlayStatus)playStatus {
    _playStatus = playStatus;
    switch (_playStatus) {
        case MiniAudioPlayStatusPrepare: {
            [self.playButton setSelected:NO];
        }
            break;
        case MiniAudioPlayStatusPlay: {
            [self.playButton setSelected:YES];
        }
            break;
        case MiniAudioPlayStatusPause: {
            [self.playButton setSelected:NO];
        }
            break;
        case MiniAudioPlayStatusFinish: {
            [self.playButton setSelected:NO];
            //[self closePlayPlayerViewEvent:nil];
        }
            break;
        default:
            break;
    }
    @weakify(self);
    [UIView animateWithDuration:0.5 animations:^{
        @strongify(self);
        [self.infoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.playStatus!=MiniAudioPlayStatusPlay?kSizeScale(33):kSizeScale(10));
            make.right.mas_equalTo(self.line.mas_left).mas_equalTo(10);
            make.top.bottom.mas_equalTo(0);
        }];
    } completion:^(BOOL finished) {
        // 动画完成后的回调
        [self layoutIfNeeded];
    }];
    if (self.playStatus != MiniAudioPlayStatusPlay) {
        [self removePlayIconAnimation];
    }
    else {
        [self addPlayIconAnimation];
    }
}

#pragma mark - Init
- (instancetype)initWithPlayerStyleConfig:(EAMiniAudioPlayerStyleConfig *)config
{
    self = [self init];
    if(self) {
        self.styleConfig = config;
        [self prepareContetxt];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.styleConfig = [EAMiniAudioPlayerStyleConfig defaultConfig];
        [self prepareContetxt];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.styleConfig = [EAMiniAudioPlayerStyleConfig defaultConfig];
        [self prepareContetxt];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.styleConfig = [EAMiniAudioPlayerStyleConfig defaultConfig];
        [self prepareContetxt];
    }
    return self;
}


- (void)prepareContetxt {
    self.tag = 222;
    self.alpha = 0.96;
    self.layer.masksToBounds  = YES;
    self.playStatus = MiniAudioPlayStatusPrepare;
    self.layer.cornerRadius = kSizeScale(23);
    self.backgroundColor = HexRGB(0x525758);
    [self layoutPlayButton];
}

//Update player's progress
- (void)updatePlayProgress
{
    CGFloat startAngle = -M_PI_2;
    CGFloat endAngle = startAngle + self.playProgress * M_PI * 2;
    if (self.playStatus != MiniAudioPlayStatusPlay) {
        self.playStatus = MiniAudioPlayStatusPlay;
    }
    if (self.playStatus == MiniAudioPlayStatusPlay) {
        if (self.playProgress >= 1.0) {
            self.playStatus = MiniAudioPlayStatusFinish;
        }
    }
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetWidth(self.playButtonContentView.bounds) / 2, CGRectGetHeight(self.playButtonContentView.bounds) / 2) radius:ceilf(self.playButtonContentView.layer.cornerRadius) startAngle:startAngle endAngle:endAngle clockwise:YES];
    [self.playProgressLayer setPath:path.CGPath];
}


- (void)createUI
{
    if(!self.playButtonContentView)
    {
        self.playButtonContentView = [[UIView alloc] init];
        self.playButtonContentView.layer.borderWidth = 1;
//        self.playButtonContentView.layer.masksToBounds = YES;
        self.playButtonContentView.backgroundColor = XHClearColor;
        [self addSubview:self.playButtonContentView];
        //线
        self.line = [YYCreateTools createView:HexRGB(0x707576)];
        [self addSubview:self.line];
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(10);
            make.bottom.mas_equalTo(-10);
            make.right.mas_equalTo(-50);
            make.width.mas_equalTo(1);
        }];
        //取消按钮
        self.cancelBtn = [YYCreateTools createBtnImage:@"audio_cancel_icon"];
        [self.cancelBtn addTarget:self action:@selector(closePlayPlayerViewEvent:)
                 forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.cancelBtn];
        
        [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(13));
            make.centerY.equalTo(self);
        }];
        
        self.infoView = [YYCreateTools createView:XHClearColor];
        [self.infoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                action:@selector(contentClickEvent:)]];
        [self addSubview:self.infoView];
        
        [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(self.line.mas_left).mas_equalTo(10);
            make.top.bottom.mas_equalTo(0);
        }];
        
        self.soundIcon = [YYCreateTools createImageView:@""
                                              viewModel:-1];
        [self.soundIcon zy_cornerRadiusAdvance:kSizeScale(16)
                                rectCornerType:UIRectCornerAllCorners];
        [self.infoView addSubview:self.soundIcon];
        
        [self.soundIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.infoView);
            make.left.mas_equalTo(0);
            make.width.height.mas_equalTo(kSizeScale(32));
        }];
        
        self.titleLabel = [YYCreateTools createLabel:nil
                                                font:normalFont(13)
                                           textColor:XHWhiteColor];
        [self.infoView addSubview:self.titleLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.soundIcon.mas_right).mas_offset(8);
            make.top.mas_equalTo(6);
            make.right.mas_equalTo(-kSizeScale(13));
        }];
        
        self.timeLabel = [YYCreateTools createLabel:nil
                                               font:normalFont(10)
                                          textColor:XHBlackLitColor];
        [self.infoView addSubview:self.timeLabel];
        
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.soundIcon.mas_right).mas_offset(8);
            make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(1);
        }];
        
        self.detailLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(10)
                                            textColor:XHBlackLitColor];
        [self.infoView addSubview:self.detailLabel];
        
        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.timeLabel.mas_right).mas_offset(8);
            make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(1);
            make.right.mas_equalTo(-kSizeScale(13));
        }];
        
        self.playProgressLayer = [CAShapeLayer layer];
        self.playProgressBackLayer = [CAShapeLayer layer];
        
        self.playProgressLayer.backgroundColor = [UIColor clearColor].CGColor;
        self.playProgressBackLayer.fillColor = [UIColor clearColor].CGColor;
        self.playProgressLayer.fillColor = [UIColor clearColor].CGColor;
        
        [self.playButtonContentView.layer addSublayer:self.playProgressBackLayer];
        [self.playButtonContentView.layer addSublayer:self.playProgressLayer];

        if(!_playButton)
        {
            _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.playButtonContentView addSubview:self.playButton];
        }
        [_playButton setImage:[UIImage imageNamed:@"audio_stop_icon"] forState:UIControlStateNormal];
        [_playButton setImage:[UIImage imageNamed:@"audio_play_icon"] forState:UIControlStateSelected];
        [_playButton addTarget:self action:@selector(doplayAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.playButtonContentView.backgroundColor = XHClearColor;
    self.playButtonContentView.layer.borderColor =XHClearColor.CGColor;
//    ;
    
    self.playProgressLayer.lineWidth = self.styleConfig.playProgressWidth;
    self.playProgressBackLayer.lineWidth = self.styleConfig.playProgressWidth;

    self.playProgressBackLayer.strokeColor = HexRGB(0x707576).CGColor;
    self.playProgressLayer.strokeColor = self.styleConfig.playProgressColor.CGColor;
}

- (void)layoutPlayButton
{
    if(self.styleConfig.playerStyle & EAMiniPlayerHidePlayButton)
    {
        if(self.playButtonContentView)
        {
            [self.playButtonContentView removeFromSuperview];
            self.playButtonContentView = nil;
        }
    }
    else
    {
        if(!self.playButtonContentView)
        {
            [self createUI];
        }
        
        CGFloat offset = 10;
        CGRect frame = CGRectMake(self.frame.size.width - offset - self.styleConfig.contentInsets.left-27, offset + self.styleConfig.contentInsets.top, self.bounds.size.height - 2 * offset - self.styleConfig.contentInsets.top - self.styleConfig.contentInsets.bottom, self.bounds.size.height - 2 * offset- self.styleConfig.contentInsets.top - self.styleConfig.contentInsets.bottom);
   
        self.playButtonContentView.frame = frame;
        self.playProgressLayer.frame = self.playButtonContentView.bounds;
        self.playProgressBackLayer.frame = self.playButtonContentView.bounds;
        
        
    
        
        
        self.playButtonContentView.layer.cornerRadius = CGRectGetWidth(frame) / 2;
        
        
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetWidth(self.playButtonContentView.bounds) / 2, CGRectGetHeight(self.playButtonContentView.bounds) / 2) radius:ceilf(self.playButtonContentView.layer.cornerRadius) startAngle:0 endAngle:M_PI*2 clockwise:YES];
        [self.playProgressBackLayer setPath:path.CGPath];

        
        //-------------------caculate playbutton's frame--------------------------
        CGSize buttonImageSize = [UIImage imageNamed:@"audio_play_icon"].size;
        CGSize buttonSize = CGSizeMake(frame.size.width, frame.size.height);
        
        //if play button's background image size if samller than frame.size.height / 3
        if((buttonImageSize.height > buttonImageSize.width && buttonImageSize.height < buttonSize.height)
           || (buttonImageSize.width > buttonImageSize.height && buttonImageSize.width < buttonSize.width))
        {
            buttonSize = buttonImageSize;
        }
        
        frame.origin.x = (frame.size.width - buttonSize.width) / 2;
        frame.origin.y = (frame.size.height - buttonSize.height) / 2;
        frame.size.height = buttonSize.height;
        frame.size.width = buttonSize.width;
        
        self.playButton.frame = frame;
        /*self.playButton.hitEdgeInsets = UIEdgeInsetsMake(CGRectGetHeight(self.playButtonContentView.bounds) - CGRectGetHeight(self.playButton.bounds) / 2, CGRectGetWidth(self.playButtonContentView.bounds) - CGRectGetWidth(self.playButton.bounds) / 2, CGRectGetHeight(self.playButtonContentView.bounds) - CGRectGetHeight(self.playButton.bounds) / 2, CGRectGetWidth(self.playButtonContentView.bounds) - CGRectGetWidth(self.playButton.bounds) / 2);*/
    }
}

#pragma mark - Private
/**添加旋转动画**/
- (void)addPlayIconAnimation {
    [self removePlayIconAnimation];
    if (!self.iconAnimation) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        //默认是顺时针效果，若将fromValue和toValue的值互换，则为逆时针效果
        animation.fromValue = [NSNumber numberWithFloat:0.f];
        animation.toValue = [NSNumber numberWithFloat: M_PI *2];
        animation.duration = 25;
        animation.autoreverses = NO;
        animation.fillMode = kCAFillModeForwards;
        animation.repeatCount = MAXFLOAT; //如果这里想设置成一直自旋转，可以设置为MAXFLOAT，否则设置具体的数值则代表执行多少次
        [self.soundIcon.layer addAnimation:animation forKey:nil];
        self.iconAnimation = animation;
    }
}
/**移除旋转动画**/
- (void)removePlayIconAnimation {
    if (self.iconAnimation) {
        [self.soundIcon.layer removeAllAnimations];
        self.iconAnimation = nil;
    }
}


#pragma mark - Event
/**关闭事件**/
- (void)closePlayPlayerViewEvent:(UIButton *)sender {
    if (self.closeAudioPlayView) {
        self.closeAudioPlayView(self);
    }
}

/**播放事件**/
- (void)doplayAction:(UIButton *)sender {
    if (self.playStatus == MiniAudioPlayStatusPlay) {
        self.playStatus = MiniAudioPlayStatusPause;
    }
    else if (self.playStatus == MiniAudioPlayStatusPrepare || self.playStatus == MiniAudioPlayStatusPause) {
        self.playStatus = MiniAudioPlayStatusPlay;
    }
    if (self.playAndPuseAudioPlayView) {
        self.playAndPuseAudioPlayView(self.playStatus);
    }
}

/**内容点击事件**/
- (void)contentClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.contentClickBlock) {
        self.contentClickBlock();
    }
}
    
@end
