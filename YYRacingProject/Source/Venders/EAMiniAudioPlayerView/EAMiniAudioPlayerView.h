//
//  EAAudioPlayerView.h
//  EAAudioPlayerViewDemo
//
//  Created by EA.Huang on 5/19/16.
//  Copyright © 2016 EAH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAMiniAudioPlayerStyleConfig.h"

typedef NS_ENUM(NSInteger, MiniAudioPlayStatus)  {
    MiniAudioPlayStatusPrepare = 0,
    MiniAudioPlayStatusPlay,
    MiniAudioPlayStatusPause,
    MiniAudioPlayStatusFinish
};

@interface YYAudioInfoModel : NSObject

@property (nonatomic, strong) NSNumber *lessonID;
@property (nonatomic, copy) NSString *audioHeaderUrl;
@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *detailTitleStr;
@property (nonatomic, copy) NSString *timeStr;

@end


@interface EAMiniAudioPlayerView : UIView
// 标题
@property (nonatomic, copy) NSString *videoTitle;
// 副标题
@property (nonatomic, copy) NSString *videoSubTitle;
// 封面图片
@property (nonatomic, copy) NSString *videoCoverUrl;
// 时长
@property (nonatomic, copy) NSString *videoDurationStr;
// 背景图片
@property (nonatomic, copy) NSString *videoBgUrl;
// 播放进度
@property (nonatomic, assign) CGFloat playProgress;

@property (nonatomic, strong) YYAudioInfoModel *audioInfoModel;
//播放状态
@property (nonatomic, assign) MiniAudioPlayStatus  playStatus;

@property (nonatomic, copy) void(^contentClickBlock)(void);
@property (nonatomic, copy) void(^closeAudioPlayView)(EAMiniAudioPlayerView *);
@property (nonatomic, copy) void(^playAndPuseAudioPlayView)(MiniAudioPlayStatus playStatus);

//style configration
@property (nonatomic, strong) EAMiniAudioPlayerStyleConfig *styleConfig;

/**
 *  @param playerStyle  EAMiniPlayerStyle
 *  @param cornerRadius The radius on both side of the MiniAudioPlayerView
 */
- (instancetype)initWithPlayerStyleConfig:(EAMiniAudioPlayerStyleConfig *)config;

@end
