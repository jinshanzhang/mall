//
//  YYHxChatViewController.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/12.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

#import <HelpDesk/HDConversation.h>
#import <HelpDesk/HDChatManager.h>
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreServices/CoreServices.h>

#import "IMessageModel.h"
#import "EaseMessageModel.h"
#import "EaseBaseMessageCell.h"
#import "EaseMessageTimeCell.h"

#import "EaseChatToolbar.h"
#import "EMCDDeviceManager+Media.h"
#import "EMCDDeviceManager+ProximitySensor.h"
#import "SatisfactionViewController.h"
#import "TZImagePickerController.h"

NS_ASSUME_NONNULL_BEGIN


@interface YYHxEaseAtTarget : NSObject

@property (nonatomic, copy) NSString    *userId;
@property (nonatomic, copy) NSString    *nickname;

- (instancetype)initWithUserId:(NSString*)userId andNickname:(NSString*)nickname;
@end

typedef void(^YYHxEaseSelectAtTargetCallback)(YYHxEaseAtTarget*);


@class YYHxMessageViewController;

@protocol YYHxMessageViewControllerDelegate <NSObject>

@optional

/*!
 @brief 获取消息自定义cell
 @discussion 用户根据messageModel判断是否显示自定义cell,返回nil显示默认cell,否则显示用户自定义cell
 **/
- (UITableViewCell *)messageViewController:(UITableView *)tableView
                       cellForMessageModel:(id<IMessageModel>)messageModel;

/*!
 @brief 获取消息cell高度
 @discussion 用户根据messageModel判断,是否自定义显示cell的高度
 */
- (CGFloat)messageViewController:(YYHxMessageViewController *)viewController
           heightForMessageModel:(id<IMessageModel>)messageModel
                   withCellWidth:(CGFloat)cellWidth;

/*!
 @brief 接收到消息的已读回执
 @discussion 接收到消息的已读回执的回调,用户可以自定义处理
 */
- (void)messageViewController:(YYHxMessageViewController *)viewController
 didReceiveHasReadAckForModel:(id<IMessageModel>)messageModel;

/*!
 @brief 选中消息
 @discussion 选中消息的回调,用户可以自定义处理
 */
- (BOOL)messageViewController:(YYHxMessageViewController *)viewController
        didSelectMessageModel:(id<IMessageModel>)messageModel;

/*!
 @brief 选中底部功能按钮
 @discussion 消息发送成功的回调,用户可以自定义处理
 */
- (void)messageViewController:(YYHxMessageViewController *)viewController
            didSelectMoreView:(EaseChatBarMoreView *)moreView
                      AtIndex:(NSInteger)index;

@end


@protocol YYHxMessageViewControllerDataSource <NSObject>

@optional

/*!
 @brief 指定消息附件上传或者下载进度的监听者,默认self
 @discussion
 **/
- (id)messageViewController:(YYHxMessageViewController *)viewController
progressDelegateForMessageBodyType:(EMMessageBodyType)messageBodyType;

/*!
 @brief 附件进度有更新
 @discussion
 **/
- (void)messageViewController:(YYHxMessageViewController *)viewController
               updateProgress:(float)progress
                 messageModel:(id<IMessageModel>)messageModel
                  messageBody:(EMMessageBody*)messageBody;

/*!
 @brief 消息时间间隔描述
 @discussion
 **/
- (NSString *)messageViewController:(YYHxMessageViewController *)viewController
                      stringForDate:(NSDate *)date;

/*!
 @brief 将HDMessage类型转换为符合<IMessageModel>协议的类型
 @discussion 将HDMessage类型转换为符合<IMessageModel>协议的类型,设置用户信息,消息显示用户昵称和头像
 **/
- (id<IMessageModel>)messageViewController:(YYHxMessageViewController *)viewController
                           modelForMessage:(HDMessage *)message;

/*!
 @brief 是否允许长按
 @discussion 获取是否允许长按的回调,默认是NO
 **/
- (BOOL)messageViewController:(YYHxMessageViewController *)viewController
   canLongPressRowAtIndexPath:(NSIndexPath *)indexPath;

/*!
 @brief 触发长按手势
 @discussion 获取触发长按手势的回调,默认是NO
 **/
- (BOOL)messageViewController:(YYHxMessageViewController *)viewController
   didLongPressRowAtIndexPath:(NSIndexPath *)indexPath;

/*!
 @brief 是否标记为已读
 @discussion 是否标记为已读的回调
 */
- (BOOL)messageViewControllerShouldMarkMessagesAsRead:(YYHxMessageViewController *)viewController;

/*!
 @brief 是否发送已读回执
 @discussion
 */
- (BOOL)messageViewController:(YYHxMessageViewController *)viewController
shouldSendHasReadAckForMessage:(HDMessage *)message
                         read:(BOOL)read;

/*!
 @brief 判断消息是否为表情消息
 @discussion
 */
- (BOOL)isEmotionMessageFormessageViewController:(YYHxMessageViewController *)viewController
                                    messageModel:(id<IMessageModel>)messageModel;

/*!
 @brief 根据消息获取表情信息
 @discussion
 */
- (EaseEmotion*)emotionURLFormessageViewController:(YYHxMessageViewController *)viewController
                                      messageModel:(id<IMessageModel>)messageModel;

/*!
 @brief 获取表情列表
 @discussion
 */
- (NSArray*)emotionFormessageViewController:(YYHxMessageViewController *)viewController;

/*!
 @brief 获取发送表情消息的扩展字段
 @discussion
 */
- (NSDictionary*)emotionExtFormessageViewController:(YYHxMessageViewController *)viewController
                                        easeEmotion:(EaseEmotion*)easeEmotion;

/*!
 @brief view标记已读
 @discussion
 */
- (void)messageViewControllerMarkAllMessagesAsRead:(YYHxMessageViewController *)viewController;

@end


@interface YYHxMessageViewController : YYBaseViewController
<UINavigationControllerDelegate, UIImagePickerControllerDelegate,
HDChatManagerDelegate, EMChatToolbarDelegate, EaseChatBarMoreViewDelegate, EaseMessageCellDelegate>

//通过单聊用户名，回去单聊会话
- (instancetype)initWithConversationChatter:(NSString *)conversationChatter;

@property (nonatomic, weak) id <YYHxMessageViewControllerDelegate> delegate;

@property (nonatomic, weak) id <YYHxMessageViewControllerDataSource> dataSource;

// 聊天的会话对象
@property (nonatomic, strong) HDConversation *conversation;

// 时间间隔标记
@property (nonatomic) NSTimeInterval messageTimeIntervalTag;

// 列表刷新回调(次数，可以当次数为1，首次进入增加订单消息展示)
@property (nonatomic, copy) void (^messageListRefreshBlock)(NSInteger times);

// 加载的每页message的条数
@property (nonatomic) NSInteger messageCountOfPage; //default 50

// 时间分割cell的高度
@property (nonatomic) CGFloat timeCellHeight;

// 当前页面显示时，是否滚动到最后一条
@property (nonatomic) BOOL scrollToBottomWhenAppear; //default YES;

// 页面是否处于显示状态
@property (nonatomic) BOOL isViewDidAppear;

// tableView的数据源，用户UI显示
@property (nonatomic, strong) NSMutableArray *listData;

// 显示的HDMessage类型的消息列表
@property (nonatomic, strong) NSMutableArray *messsagesSource;

// 底部输入控件
@property (nonatomic, strong) UIView *chatToolbar;

// 底部功能控件
@property (nonatomic, strong) EaseChatBarMoreView *chatBarMoreView;

// 底部表情控件
@property (nonatomic, strong) EaseFaceView *faceView;

// 菜单(消息复制,删除)
@property (nonatomic, strong) UIMenuController *menuController;

// 选中消息菜单索引
@property (nonatomic, strong, nullable) NSIndexPath *menuIndexPath;

// 图片选择器
@property (nonatomic, strong) UIImagePickerController *imagePicker;

@property (nonatomic, strong) TZImagePickerController *tzimagePicker;

// 是否启用下拉加载更多
@property (nonatomic) BOOL showRefreshHeader;

// 是否启用上拉加载更多
@property (nonatomic) BOOL showRefreshFooter;



- (void)tableViewDidTriggerHeaderRefresh;


/*!
@brief 发送文本消息
@discussion
*/
- (void)sendTextMessage:(NSString *)text;

/*!
 @brief 发送文本消息
 @discussion
 */
- (void)sendTextMessage:(NSString *)text withExt:(NSDictionary*)ext;

/*!
 @brief 发送图片消息
 @discussion
 */
- (void)sendImageMessage:(UIImage *)image;


/**
 @brief 发送图片消息
 @discussion
 **/
- (void)sendImageMessageWithData:(NSData *)imageData;

/*!
 @brief 发送位置消息
 @discussion
 */
- (void)sendLocationMessageLatitude:(double)latitude
                          longitude:(double)longitude
                         andAddress:(NSString *)address;

/**
 @brief 发送语音消息
 @discussion
 **/
- (void)sendVoiceMessageWithLocalPath:(NSString *)localPath
                             duration:(NSInteger)duration;

/*!
 @brief 发送视频消息
 @discussion
 */
- (void)sendVideoMessageWithURL:(NSURL *)url;

/*!
 @brief 发送视频消息
 @discussion
 */
- (void)sendFileMessageWith:(HDMessage *)message;

/*!
 @brief 发送消息
 @discussion
 */
- (void)sendMessage:(HDMessage *)message isNeedUploadFile:(BOOL)isUploadFile completion:(void (^)(HDMessage *aMessage, HDError *aError))aCompletionBlock;

/*!
 @brief 添加消息
 @discussion
 */
- (void)addMessageToDataSource:(HDMessage *)message
                      progress:(id)progress;

/*!
 @brief 显示消息长按菜单
 @discussion
 */
- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(EMMessageBodyType)messageType;

/*!
 @brief 判断消息是否要发送已读回执
 @discussion
 */
- (BOOL)shouldSendHasReadAckForMessage:(HDMessage *)message
                                  read:(BOOL)read;


@end

NS_ASSUME_NONNULL_END
