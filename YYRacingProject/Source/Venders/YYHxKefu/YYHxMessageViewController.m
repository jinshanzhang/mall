//
//  YYHxChatViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/12.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHxMessageViewController.h"

#import "NSDate+Category.h"

@interface YYHxEaseAtTarget()

@end

@implementation YYHxEaseAtTarget
- (instancetype)initWithUserId:(NSString*)userId andNickname:(NSString*)nickname
{
    if (self = [super init]) {
        _userId = [userId copy];
        _nickname = [nickname copy];
    }
    return self;
}
@end

@interface YYHxMessageViewController ()
<UITableViewDelegate,
UITableViewDataSource,
SatisfactionDelegate,
TZImagePickerControllerDelegate>
{
    UIMenuItem *_copyMenuItem;
    UIMenuItem *_deleteMenuItem;
    UILongPressGestureRecognizer *_lpgr;
    NSMutableArray *_atTargets;
    
    dispatch_queue_t _messageQueue;
    
    BOOL _isSendingTransformMessage; //正在发送转人工消息
    BOOL _isSendingEvaluateMessage;//点击立即评价按钮
}

@property (nonatomic, assign) NSInteger refreshTimes;
@property (nonatomic, strong) NSMutableArray *atTargets;

@end

@implementation YYHxMessageViewController

#pragma mark - Getter method
- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}

- (TZImagePickerController *)tzimagePicker {
    _tzimagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    _tzimagePicker.allowTakePicture = NO; // 在内部显示拍照按钮
    // 4. 照片排列按修改时间升序
    _tzimagePicker.allowCrop = NO;
    _tzimagePicker.showSelectBtn = NO;
    _tzimagePicker.alwaysEnableDoneBtn = YES;
    _tzimagePicker.sortAscendingByModificationDate = NO;
    return _tzimagePicker;
}

- (NSMutableArray *)atTargets {
    if (!_atTargets) {
        _atTargets = [NSMutableArray array];
    }
    return _atTargets;
}

- (NSMutableArray *)listData {
    if (!_listData) {
        _listData = [NSMutableArray array];
    }
    return _listData;
}

#pragma mark - Setter method

- (void)setShowRefreshHeader:(BOOL)showRefreshHeader {
    _showRefreshHeader = showRefreshHeader;
    if (_showRefreshHeader) {
        __weak YYHxMessageViewController *weakSelf = self;
        self.m_tableView.mj_header = [YYRacingBeckRefreshHeader headerWithRefreshingBlock:^{
            [weakSelf tableViewDidTriggerHeaderRefresh];
        }];
        self.m_tableView.mj_header.accessibilityIdentifier = @"refresh_header";
    }
    else {
        [self.m_tableView setMj_header:nil];
    }
}

- (void)setShowRefreshFooter:(BOOL)showRefreshFooter {
    _showRefreshFooter = showRefreshFooter;
    if (_showRefreshFooter) {
        __weak YYHxMessageViewController *weakSelf = self;
        self.m_tableView.mj_footer = [YYBeckRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf tableViewDidTriggerHeaderRefresh];
        }];
        self.m_tableView.mj_footer.accessibilityIdentifier = @"refresh_footer";
    }
    else {
        [self.m_tableView setMj_footer:nil];
    }
}

- (void)setDataSource:(id<YYHxMessageViewControllerDataSource>)dataSource {
    _dataSource = dataSource;
    
    [self setupEmotion];
}

- (void)setDelegate:(id<YYHxMessageViewControllerDelegate>)delegate {
    _delegate = delegate;
}

- (void)setChatToolbar:(EaseChatToolbar *)chatToolbar {
    [chatToolbar removeFromSuperview];
    
    _chatToolbar = chatToolbar;
    if (_chatToolbar) {
        [self.view addSubview:_chatToolbar];
    }
    CGRect tableFrame = self.m_tableView.frame;
    tableFrame.size.height = self.m_tableView.frame.size.height - _chatToolbar.frame.size.height - kBottom(0);
    self.m_tableView.frame = tableFrame;
    if ([chatToolbar isKindOfClass:[EaseChatToolbar class]]) {
        [(EaseChatToolbar *)chatToolbar setDelegate:self];
        self.chatBarMoreView = (EaseChatBarMoreView*)[(EaseChatToolbar *)self.chatToolbar moreView];
        self.faceView = (EaseFaceView*)[(EaseChatToolbar *)self.chatToolbar faceView];
    }
}

#pragma mark - Life cycle

- (instancetype)initWithConversationChatter:(NSString *)conversationChatter
{
    if ([conversationChatter length] == 0) {
        return nil;
    }
    
    self = [self initWithStyle:UITableViewStylePlain];
    if (self) {
        _conversation = _conversation = [[HDClient sharedClient].chatManager getConversation:conversationChatter];
        _messageCountOfPage = 50;
        _timeCellHeight = kSizeScale(50);
        _showRefreshHeader = YES;
        _showRefreshFooter = NO;
        _refreshTimes = 0;
        _messsagesSource = [NSMutableArray array];
        [_conversation markAllMessagesAsRead:nil];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)tableStyle {
    self = [super init];
    if (self) {
        self.tableStyle = tableStyle;
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.isViewDidAppear = YES;
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:NO];
    
    if (self.scrollToBottomWhenAppear) {
        [self _scrollViewToBottom:NO];
    }
    self.scrollToBottomWhenAppear = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.isViewDidAppear = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = XHWhiteColor;
    CGFloat chatbarHeight = [EaseChatToolbar defaultHeight];
    // 暂时不清楚原因的加入
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideImagePicker) name:@"hideImagePicker" object:nil];
    
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = HexRGB(0xf3f3f3);
    [self.view  addSubview:self.m_tableView];
    self.m_tableView.accessibilityIdentifier = @"yy_hx_message_vc_tableview";
    self.m_tableView.frame = CGRectMake(0, kNavigationH, kScreenW, (kScreenH-kNavigationH));
    
    
    //Initialization
    self.chatToolbar = [[EaseChatToolbar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - chatbarHeight - kBottom(0), self.view.frame.size.width, chatbarHeight) type:EMChatToolbarTypeChat];
    self.chatToolbar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    //Initializa the gesture recognizer
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardHidden:)];
    [self.view addGestureRecognizer:tap];
    
    _lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    _lpgr.minimumPressDuration = 0.5;
    [self.m_tableView addGestureRecognizer:_lpgr];
    
    _messageQueue = dispatch_queue_create("hyphenate.com", NULL);
    
    [[HDClient sharedClient].chatManager addDelegate:self delegateQueue:NULL];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [self tableViewDidTriggerHeaderRefresh];
    
    [self setupEmotion];
    
    self.m_tableView.estimatedRowHeight = 0;
    self.m_tableView.estimatedSectionHeaderHeight = 0;
    self.m_tableView.estimatedSectionFooterHeight = 0;
    // Do any additional setup after loading the view.
}

/**
 *  设置表情
 **/
- (void)setupEmotion {
    if ([self.dataSource  respondsToSelector:@selector(emotionFormessageViewController:)]) {
        NSArray * emotionManagers = [self.dataSource emotionFormessageViewController:self];
        [self.faceView setEmotionManagers:emotionManagers];
    }
    else {
        NSMutableArray *emotions = [NSMutableArray array];
        for (NSString * name in [EaseEmoji allEmoji]) {
            EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:name emotionThumbnail:name emotionOriginal:name emotionOriginalURL:@"" emotionType:EMEmotionDefault];
            [emotions addObject:emotion];
        }
        EaseEmotion *emotion = [emotions firstObject];
        EaseEmotionManager *manager= [[EaseEmotionManager alloc] initWithType:EMEmotionDefault emotionRow:3 emotionCol:7 emotions:emotions tagImage:[UIImage imageNamed:emotion.emotionId]];
        [self.faceView setEmotionManagers:@[manager]];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (_imagePicker){
        [_imagePicker dismissViewControllerAnimated:NO completion:nil];
        _imagePicker = nil;
    }
    
    if (_tzimagePicker) {
        [_tzimagePicker dismissViewControllerAnimated:NO completion:nil];
        _tzimagePicker = nil;
    }
}

#pragma mark - Event method

#pragma mark - Event action
- (void)copyMenuAction:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.listData objectAtIndex:self.menuIndexPath.row];
        pasteboard.string = model.text;
    }
    
    self.menuIndexPath = nil;
}

- (void)deleteMenuAction:(id)sender {
    // 云客服 API不支持删除单条消息
}

#pragma mark - Event gesture
- (void)keyBoardHidden:(UITapGestureRecognizer *)tapRecognizer {
    if (tapRecognizer.state == UIGestureRecognizerStateEnded) {
        [self.chatToolbar endEditing:YES];
    }
}

- (void)hideImagePicker {
    if (_imagePicker && [HDSDKHelper shareHelper].isShowingimagePicker) {
        [_imagePicker dismissViewControllerAnimated:NO completion:nil];
        [_tzimagePicker dismissViewControllerAnimated:NO completion:nil];
    }
}

- (void)didBecomeActive {
    self.messageTimeIntervalTag = -1;
    self.listData = [[self formatMessages:self.messsagesSource] mutableCopy];
    [self.m_tableView reloadData];
    
    if (self.isViewDidAppear)
    {
        NSMutableArray *unreadMessages = [NSMutableArray array];
        for (HDMessage *message in self.messsagesSource)
        {
            if ([self shouldSendHasReadAckForMessage:message read:NO])
            {
                [unreadMessages addObject:message];
            }
        }
        if ([unreadMessages count])
        {
            [self _sendHasReadResponseForMessage:unreadMessages isRead:YES];
        }
        
        [_conversation markAllMessagesAsRead:nil];
        if (self.dataSource && [self.dataSource respondsToSelector:@selector(messageViewControllerMarkAllMessagesAsRead:)]) {
            [self.dataSource messageViewControllerMarkAllMessagesAsRead:self];
        }
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan && [self.listData count] > 0)
    {
        CGPoint location = [recognizer locationInView:self.m_tableView];
        NSIndexPath * indexPath = [self.m_tableView indexPathForRowAtPoint:location];
        BOOL canLongPress = NO;
        if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:canLongPressRowAtIndexPath:)]) {
            canLongPress = [_dataSource messageViewController:self
                                   canLongPressRowAtIndexPath:indexPath];
        }
        
        if (!canLongPress) {
            return;
        }
        
        if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:didLongPressRowAtIndexPath:)]) {
            [_dataSource messageViewController:self
                    didLongPressRowAtIndexPath:indexPath];
        }
        else{
            id object = [self.listData objectAtIndex:indexPath.row];
            if (![object isKindOfClass:[NSString class]]) {
                EaseMessageCell *cell = (EaseMessageCell *)[self.m_tableView cellForRowAtIndexPath:indexPath];
                [cell becomeFirstResponder];
                _menuIndexPath = indexPath;
                //[self showMenuViewController:cell.bubbleView andIndexPath:indexPath messageType:cell.model.bodyType];
            }
        }
    }
}

#pragma mark - Public mehtod
- (void)tableViewDidTriggerHeaderRefresh {
    self.messageTimeIntervalTag = -1;
    NSString *messageId = nil;
    if (self.messsagesSource.count > 0) {
        messageId = [(HDMessage *)[self.messsagesSource firstObject] messageId];
    }
    else {
        messageId = nil;
    }
    
    [self _loadMessageBefore:messageId count:self.messageCountOfPage append:YES];
    
    [self tableViewDidFinishTriggerHeader:YES reload:YES];
}

#pragma mark - Public send message

- (void)addMessageToDataSource:(HDMessage *)message progress:(id)progress {
    [self.messsagesSource addObject:message];
    
    __weak YYHxMessageViewController *weakSelf = self;
    dispatch_async(_messageQueue, ^{
        NSArray *messages = [self formatMessages:@[message]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.listData addObjectsFromArray:messages];
            [weakSelf.m_tableView reloadData];
            
            [weakSelf.m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[weakSelf.listData count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        });
    });
}

- (void)_refreshAfterSendMessage:(HDMessage *)message {
    
}


- (void)sendMessage:(HDMessage *)message isNeedUploadFile:(BOOL)isUploadFile completion:(void (^)(HDMessage *aMessage, HDError *aError))aCompletionBlock{
    if (message && isUploadFile && NO) {
        NSLog(@"发送消息，上传文件；暂时未实现...");
    }
    else {
        __weak typeof(self) weakself = self;

        [self addMessageToDataSource:message progress:nil];
        
        [[HDClient sharedClient].chatManager sendMessage:message progress:^(int progress) {
            if (weakself.dataSource && [weakself.dataSource respondsToSelector:@selector(messageViewController:updateProgress:messageModel:messageBody:)]) {
                [weakself.dataSource messageViewController:self updateProgress:progress messageModel:nil messageBody:message.body];
            }
        } completion:^(HDMessage *aMessage, HDError *aError) {
            if (aCompletionBlock) {
                aCompletionBlock(aMessage, aError);
            }
            [weakself.m_tableView reloadData];
        }];
    }
}

- (void)sendTextMessage:(NSString *)text {
    [self sendTextMessage:text withExt:nil];
}

- (void)sendTextMessage:(NSString *)text withExt:(NSDictionary *)ext {
    HDMessage *message = [HDSDKHelper textHMessageFormatWithText:text to:self.conversation.conversationId];
    [self sendMessage:message isNeedUploadFile:NO completion:nil];
}

- (void)sendImageMessage:(UIImage *)image {
    id progress = nil;
    if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:progressDelegateForMessageBodyType:)]) {
        progress = [_dataSource messageViewController:self progressDelegateForMessageBodyType:EMMessageBodyTypeImage];
    }
    else {
        progress = self;
    }
    HDMessage *message = [HDSDKHelper imageMessageWithImage:image to:self.conversation.conversationId
                                                 messageExt:nil];
    [self sendMessage:message isNeedUploadFile:YES completion:nil];
}

- (void)sendImageMessageWithData:(NSData *)imageData {
    id progress = nil;
    if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:progressDelegateForMessageBodyType:)]) {
        progress = [_dataSource messageViewController:self progressDelegateForMessageBodyType:EMMessageBodyTypeImage];
    }
    else {
        progress = self;
    }
    HDMessage *message = [HDSDKHelper imageMessageWithImageData:imageData
                                                             to:self.conversation.conversationId
                                                     messageExt:nil];
    [self sendMessage:message isNeedUploadFile:YES completion:nil];
}

- (void)sendLocationMessageLatitude:(double)latitude longitude:(double)longitude andAddress:(NSString *)address {
    HDMessage *message = [HDSDKHelper locationHMessageWithLatitude:latitude
                                                         longitude:longitude
                                                           address:address
                                                                to:self.conversation.conversationId
                                                        messageExt:nil];
    [self sendMessage:message isNeedUploadFile:NO completion:nil];
}

- (void)sendVoiceMessageWithLocalPath:(NSString *)localPath
                             duration:(NSInteger)duration
{
    id progress = nil;
    if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:progressDelegateForMessageBodyType:)]) {
        progress = [_dataSource messageViewController:self progressDelegateForMessageBodyType:EMMessageBodyTypeVoice];
    }
    else{
        progress = self;
    }
    
    HDMessage *message = [HDSDKHelper voiceMessageWithLocalPath:localPath
                                                       duration:(int)duration
                                                             to:self.conversation.conversationId
                                                     messageExt:nil];
    [self sendMessage:message isNeedUploadFile:YES completion:nil];
}

- (void)sendVideoMessageWithURL:(NSURL *)url
{
    id progress = nil;
    if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:progressDelegateForMessageBodyType:)]) {
        progress = [_dataSource messageViewController:self progressDelegateForMessageBodyType:EMMessageBodyTypeVideo];
    }
    else{
        progress = self;
    }
    
    HDMessage *message = [HDSDKHelper videoMessageWithLocalPath:[url path]
                                                             to:self.conversation.conversationId
                                                     messageExt:nil];
    [self sendMessage:message isNeedUploadFile:YES completion:nil];
}

- (void)sendFileMessageWith:(HDMessage *)message {
    [self sendMessage:message isNeedUploadFile:YES completion:nil];
}

#pragma mark - Private method

// 显示删除，复制的菜单item项
- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(EMMessageBodyType)messageType
{
    if (_menuController == nil) {
        _menuController = [UIMenuController sharedMenuController];
    }
    
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:@"删除" action:@selector(deleteMenuAction:)];
    }
    
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制" action:@selector(copyMenuAction:)];
    }
    
    if (messageType == EMMessageBodyTypeText) {
        [_menuController setMenuItems:@[_copyMenuItem, _deleteMenuItem]];
    } else {
        [_menuController setMenuItems:@[_deleteMenuItem]];
    }
    [_menuController setTargetRect:showInView.frame inView:showInView.superview];
    [_menuController setMenuVisible:YES animated:YES];
}

// 控制上下拉刷新结束，和列表的刷新
- (void)tableViewDidFinishTriggerHeader:(BOOL)isHeader reload:(BOOL)reload
{
    __weak YYHxMessageViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (reload) {
            [weakSelf.m_tableView reloadData];
        }
        
        if (isHeader) {
            [weakSelf.m_tableView.mj_header endRefreshing];
        }
        else{
            [weakSelf.m_tableView.mj_footer endRefreshing];
        }
        
        if (self.messageListRefreshBlock) {
            self.refreshTimes += 1;
            self.messageListRefreshBlock(self.refreshTimes);
        }
    });
}

// tableview 滑动到底部
- (void)_scrollViewToBottom:(BOOL)animated {
    if (self.m_tableView.contentSize.height > self.m_tableView.frame.size.height) {
        CGPoint offset = CGPointMake(0, self.m_tableView.contentSize.height - self.m_tableView.frame.size.height);
        [self.m_tableView setContentOffset:offset animated:animated];
    }
}

// 发送消息后刷新消息
- (void)_refreshAfterSentMessage:(HDMessage*)aMessage
{
    if ([self.messsagesSource count]) {
        NSString *msgId = aMessage.messageId;
        HDMessage *last = self.messsagesSource.lastObject;
        if ([last isKindOfClass:[EMMessage class]]) {
            
            __block NSUInteger index = NSNotFound;
            index = NSNotFound;
            [self.messsagesSource enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(HDMessage *obj, NSUInteger idx, BOOL *stop) {
                if ([obj isKindOfClass:[HDMessage class]] && [obj.messageId isEqualToString:msgId]) {
                    index = idx;
                    *stop = YES;
                }
            }];
            if (index != NSNotFound) {
                [self.messsagesSource removeObjectAtIndex:index];
                [self.messsagesSource addObject:aMessage];
                
                //格式化消息
                self.messageTimeIntervalTag = -1;
                NSArray *formattedMessages = [self formatMessages:self.messsagesSource];
                [self.listData removeAllObjects];
                [self.listData addObjectsFromArray:formattedMessages];
                [self.m_tableView reloadData];
                [self.m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.listData count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                return;
            }
        }
    }
    [self.m_tableView reloadData];
}

// 消息转换
- (NSArray *)formatMessages:(NSArray *)messages {
    
    NSMutableArray *formattedArray = [[NSMutableArray alloc] init];
    if (messages.count == 0) {
        return formattedArray;
    }
    
    for (HDMessage *message in messages) {
        // Calculate time interval
        CGFloat interval = (self.messageTimeIntervalTag - message.messageTime) / 1000;
        if (self.messageTimeIntervalTag < 0 || interval > 60 || interval < -60) {
            NSDate *meesageDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.messageTime];
            NSString *timeStr = @"";
            if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:stringForDate:)]) {
               timeStr = [_dataSource messageViewController:self stringForDate:meesageDate];
            }
            else {
                timeStr = [meesageDate formattedTime];
            }
            [formattedArray addObject:timeStr];
            self.messageTimeIntervalTag = message.messageTime;
        }
        
        // Construct message model
        id<IMessageModel> model = nil;
        if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:modelForMessage:)]) {
            model = [_dataSource messageViewController:self modelForMessage:message];
        }
        else {
            model = [[EaseMessageModel alloc] initWithMessage:message];
            //
            model.avatarImage = [UIImage imageNamed:@"kefu_logo_icon"];
            model.failImageName = @"kefu_logo_icon";
        }
        
        if (model) {
            [formattedArray addObject:model];
        }
    }
    return formattedArray;
}


// 加载历史聊天数据
- (void)_loadMessageBefore:(NSString *)messageId
                     count:(NSInteger)count
                    append:(BOOL)isAppend {
    __weak typeof(self) weakSelf = self;
    
    void (^refresh)(NSArray *messages) = ^(NSArray *messages) {
        dispatch_async(self->_messageQueue, ^{
           
            // format the message
            NSArray *formatterMessages = [weakSelf formatMessages:messages];
            
            // refresh the page
            dispatch_async(dispatch_get_main_queue(), ^{
                YYHxMessageViewController *strongSelf = weakSelf;
                if (strongSelf) {
                    NSInteger scrollToIndex = 0;
                    if (isAppend) {
                        [strongSelf.messsagesSource insertObjects:messages atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, messages.count)]];
                        
                        // combine the message
                        id object = [strongSelf.listData firstObject];
                        if ([object isKindOfClass:[NSString class]]) {
                            NSString *timestamp = object;
                            // NSEnumerationReverse 倒序看看最后一个是否为时间消息，如是则删除
                            [formatterMessages enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                if ([obj isKindOfClass:[NSString class]] && [timestamp isEqualToString:obj]) {
                                    [strongSelf.listData removeObjectAtIndex:0];
                                    *stop = YES;
                                }
                            }];
                        }
                        
                        scrollToIndex = [strongSelf.listData count];
                        [strongSelf.listData insertObjects:formatterMessages atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [formatterMessages count])]];
                    }
                    else {
                        [strongSelf.messsagesSource removeAllObjects];
                        [strongSelf.messsagesSource addObjectsFromArray:messages];
                        
                        [strongSelf.listData removeAllObjects];
                        [strongSelf.listData addObjectsFromArray:formatterMessages];
                    }
                    
                    HDMessage *lastest = [strongSelf.messsagesSource lastObject];
                    strongSelf.messageTimeIntervalTag = lastest.messageTime;
                    
                    [strongSelf.m_tableView reloadData];
                    
                    [strongSelf.m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.listData count] - scrollToIndex - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                }
                
            });
            
            // re-download all the message that are not successfully download
            for (HDMessage *message in messages) {
                [weakSelf _downloadMessageAttachments:message];
            }
            
            // send the read acknoledgement
            // 云客服没有提供将消息置为已读的API。
            // [weakSelf _sendHasReadResponseForMessage:messages isRead:NO];
        });
    };
    
    [self.conversation loadMessagesStartFromId:messageId count:(int)count searchDirection:HDMessageSearchDirectionUp completion:^(NSArray *aMessages, HDError *aError) {
        if (!aError && [aMessages count]) {
            refresh(aMessages);
        }
    }];
}

// 下载消息附件
- (void)_downloadMessageAttachments:(HDMessage *)message {
    __weak typeof(self) weakSelf = self;
    
    void (^completion)(HDMessage *message , HDError *error) = ^(HDMessage *message, HDError *error) {
        if (!error) {
            [weakSelf _reloadTableViewDataWithMessage:message];
        }
        else {
            NSLog(@"thumbnail for fialure!");
        }
    };
    
    EMMessageBody *messageBody = message.body;
    if (messageBody.type == EMMessageBodyTypeImage) {
        //EMImageMessageBody *imageBody = (EMImageMessageBody *)messageBody;
        [[[HDClient sharedClient] chatManager] downloadThumbnail:message progress:nil completion:completion];
    }
    else if (messageBody.type == EMMessageBodyTypeVideo) {
        //EMVideoMessageBody *videoBody = (EMVideoMessageBody *)messageBody;
        [[[HDClient sharedClient] chatManager] downloadThumbnail:message progress:nil completion:completion];
    }
    else if (messageBody.type == EMMessageBodyTypeVoice) {
        //EMVoiceMessageBody *voiceBody = (EMVoiceMessageBody *)messageBody;
        [[[HDClient sharedClient] chatManager] downloadThumbnail:message progress:nil completion:completion];
    }
}


// 刷新tableview 列表数据
- (void)_reloadTableViewDataWithMessage:(HDMessage *)message {
    if ([self.conversation.conversationId isEqualToString:message.conversationId]) {
        
        for (int i = 0; i < self.listData.count ; i ++) {
            id object = [self.listData objectAtIndex:i];
            if ([object isKindOfClass:[EaseMessageModel class]]) {
                id <IMessageModel> model = object;
                if ([message.messageId isEqualToString:model.messageId]) {
                    id <IMessageModel> model = nil;
                    if (self.dataSource && [self.dataSource respondsToSelector:@selector(messageViewController:modelForMessage:)]) {
                        model = [self.dataSource messageViewController:self modelForMessage:message];
                    }
                    else {
                        //
                        model = [[EaseMessageModel alloc] initWithMessage:message];
                        model.avatarImage = [UIImage imageNamed:@"kefu_logo_icon"];
                        model.failImageName = @"kefu_logo_icon";
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.m_tableView beginUpdates];
                        [self.listData replaceObjectAtIndex:i withObject:model];
                        [self.m_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        [self.m_tableView endUpdates];
                    });
                    break;
                }
            }
        }
    }
}

/*!
 @method
 @brief mov格式视频转换为MP4格式
 @discussion
 @param movUrl   mov视频路径
 @result  MP4格式视频路径
 */
- (NSURL *)_convert2Mp4:(NSURL *)movUrl
{
    NSURL *mp4Url = nil;
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:movUrl options:nil];
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
    
    if ([compatiblePresets containsObject:AVAssetExportPresetHighestQuality]) {
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset
                                                                              presetName:AVAssetExportPresetHighestQuality];
        NSString *mp4Path = [NSString stringWithFormat:@"%@/%d%d.mp4", [EMCDDeviceManager dataPath], (int)[[NSDate date] timeIntervalSince1970], arc4random() % 100000];
        mp4Url = [NSURL fileURLWithPath:mp4Path];
        exportSession.outputURL = mp4Url;
        exportSession.shouldOptimizeForNetworkUse = YES;
        exportSession.outputFileType = AVFileTypeMPEG4;
        dispatch_semaphore_t wait = dispatch_semaphore_create(0l);
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([exportSession status]) {
                case AVAssetExportSessionStatusFailed: {
                    NSLog(@"failed, error:%@.", exportSession.error);
                } break;
                case AVAssetExportSessionStatusCancelled: {
                    NSLog(@"cancelled.");
                } break;
                case AVAssetExportSessionStatusCompleted: {
                    NSLog(@"completed.");
                } break;
                default: {
                    NSLog(@"others.");
                } break;
            }
            dispatch_semaphore_signal(wait);
        }];
        long timeout = dispatch_semaphore_wait(wait, DISPATCH_TIME_FOREVER);
        if (timeout) {
            NSLog(@"timeout.");
        }
        if (wait) {
            //dispatch_release(wait);
            wait = nil;
        }
    }
    
    return mp4Url;
}


//更新转人工消息的ext
- (void)updateTransferMessageExt:(HDMessage *)message {
    HDMessage *_message = message;
    NSMutableDictionary *_ext = [NSMutableDictionary dictionaryWithDictionary:message.ext];
    
    [_ext setValue:@YES forKey:kMesssageExtWeChat_ctrlType_transferToKf_HasTransfer];
    _message.ext = [_ext copy];
    __weak typeof(self) weakSelf = self;
    [[HDClient sharedClient].chatManager updateMessage:_message completion:^(HDMessage *aMessage, HDError *aError) {
        if (!aError) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.m_tableView reloadData];
            });
        }
    }];
}

//图片点击放大操作
- (void)_pictureZoomPreview:(NSMutableArray *)pictures fromView:(EaseMessageCell *)cell {
    __block NSMutableArray *items = [NSMutableArray array];
    [pictures enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        YYPhotoGroupItem *item = [[YYPhotoGroupItem alloc] init];
        item.thumbView = cell.bubbleView;
        if ([obj isKindOfClass:[UIImage class]]) {
            item.shellImage = obj;
        }
        else if ([obj isKindOfClass:[NSURL class]]) {
            item.largeImageURL = obj;
        }
        [items addObject:item];
    }];
    YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:0];
    [v presentFromImageView:cell.bubbleView toContainer:kAppWindow animated:YES completion:nil];
    v.hideViewBlock = ^(NSInteger currentPage) {
        
    };
    return;

}

//图片点击
- (void)_pictureMessageCellSelected:(id<IMessageModel>)model fromCell:(EaseMessageCell *)cell{
    __weak YYHxMessageViewController *weakSelf = self;
    EMImageMessageBody *imageBody = (EMImageMessageBody*)[model.message body];
    
    if ([imageBody type] == EMMessageBodyTypeImage) {
        if (imageBody.thumbnailDownloadStatus == EMDownloadStatusSuccessed) {
            if (imageBody.downloadStatus == EMDownloadStatusSuccessed)
            {
                //send the acknowledgement
                NSString *localPath = model.message == nil ? model.fileLocalPath : [imageBody localPath];
                if (localPath && localPath.length > 0) {
                    UIImage *image = [UIImage imageWithContentsOfFile:localPath];
                    if (image) {
                        [self _pictureZoomPreview:[@[image] mutableCopy] fromView:cell];
                        return;
                    }
                }
            }
            
            [weakSelf showHudInView:weakSelf.view hint:NSEaseLocalizedString(@"message.downloadingImage", @"downloading a image...")];
            
            void (^completion)(HDMessage *aMessage, HDError *error) = ^(HDMessage *aMessage, HDError *error) {
                [weakSelf hideHud];
                if (!error) {
                    NSString *localPath = aMessage == nil ? model.fileLocalPath : [(EMImageMessageBody*)aMessage.body localPath];
                    if (localPath && localPath.length > 0) {
                        UIImage *image = [UIImage imageWithContentsOfFile:localPath];
                        //                        weakSelf.isScrollToBottom = NO;
                        if (image)
                        {
                            [self _pictureZoomPreview:[@[image] mutableCopy] fromView:cell];
                        }
                        else
                        {
                            NSLog(@"Read %@ failed!", localPath);
                        }
                        return ;
                    }
                }
                [weakSelf showHint:NSEaseLocalizedString(@"message.imageFail", @"image for failure!")];
            };
            
            [[HDClient sharedClient].chatManager downloadAttachment:model.message progress:nil completion:completion];
        }else{
            //get the message thumbnail
            [[HDClient sharedClient].chatManager downloadThumbnail:model.message progress:nil completion:^(HDMessage *message, HDError *error) {
                if (!error) {
                    [weakSelf _reloadTableViewDataWithMessage:model.message];
                }else{
                    [weakSelf showHint:NSEaseLocalizedString(@"message.thumImageFail", @"thumbnail for failure!")];
                }
            }];
        }
    }
}

//订单点击
- (void)_orderMessageCellSelected:(id<IMessageModel>)model {
    if ([model.message.ext objectForKey:@"msgtype"]) {
        NSDictionary *messageType = [model.message.ext objectForKey:@"msgtype"];
        if ([messageType objectForKey:@"order"]) {
            NSInteger type = -1; // 0 详情  1 售后详情
            NSDictionary *orderType = [messageType objectForKey:@"order"];
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if ([orderType objectForKey:@"item_url"]) {
                NSArray *multiParams = [NSArray array];
                NSString *itemUrl = [orderType objectForKey:@"item_url"];
                if ([itemUrl containsString:@"=>"]) {
                    type = 1;
                    multiParams = [itemUrl componentsSeparatedByString:@"=>"];
                    [params setObject:[multiParams firstObject] forKey:@"orderId"];
                    [params setObject:[multiParams lastObject]  forKey:@"spuId"];
                }
                else {
                    type = 0;
                    [params setObject:itemUrl forKey:@"orderID"];
                }
            }
            
            if ([orderType objectForKey:@"desc"]) {
                NSNumber *sourePage = [NSNumber numberWithString:[orderType objectForKey:@"desc"]];
                if (type == 0) {
                    [params setObject:sourePage forKey:@"source"];
                    [YYCommonTools skipOrderDetial:self params:params ctrlBlock:^(NSNumber *orderStatus, NSNumber *operators, BOOL isReceive) {
                        
                    }];
                }
                else if (type == 1){
                    [params setObject:sourePage forKey:@"type"];
                    [YYCommonTools skipMyAfterSalePage:self params:params];
                }
            }
            
        }
    }
}

//视频点击
- (void)_videoMessageCellSelected:(id<IMessageModel>)model
{
    _scrollToBottomWhenAppear = NO;
    
    EMVideoMessageBody *videoBody = (EMVideoMessageBody*)model.message.body;
    
    NSString *localPath = [model.fileLocalPath length] > 0 ? model.fileLocalPath : videoBody.localPath;
    if ([localPath length] == 0) {
        [self showHint:NSEaseLocalizedString(@"message.videoFail", @"video for failure!")];
        return;
    }
    
    dispatch_block_t block = ^{
        //send the acknowledgement
        NSURL *videoURL = [NSURL fileURLWithPath:localPath];
        MPMoviePlayerViewController *moviePlayerController = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
        [moviePlayerController.moviePlayer prepareToPlay];
        moviePlayerController.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
        [self presentMoviePlayerViewControllerAnimated:moviePlayerController];
    };
    
    __weak typeof(self) weakSelf = self;
    void (^completion)(HDMessage *aMessage, HDError *error) = ^(HDMessage *aMessage, HDError *error) {
        if (!error)
        {
            [weakSelf _reloadTableViewDataWithMessage:aMessage];
        }
        else
        {
            [weakSelf showHint:NSEaseLocalizedString(@"message.thumImageFail", @"thumbnail for failure!")];
        }
    };
    
    if (videoBody.thumbnailDownloadStatus == EMDownloadStatusFailed || ![[NSFileManager defaultManager] fileExistsAtPath:videoBody.thumbnailLocalPath]) {
        [self showHint:@"begin downloading thumbnail image, click later"];
        [[HDClient sharedClient].chatManager downloadThumbnail:model.message progress:^(int progress) {
            
        } completion:completion];
    }
    
    if (videoBody.downloadStatus == EMDownloadStatusSuccessed && [[NSFileManager defaultManager] fileExistsAtPath:localPath])
    {
        block();
        return;
    }
    
    [self showHudInView:self.view hint:NSEaseLocalizedString(@"message.downloadingVideo", @"downloading video...")];
    [[HDClient sharedClient].chatManager downloadAttachment:model.message progress:^(int progress) {
        
    } completion:^(HDMessage *message, HDError *error) {
        [weakSelf hideHud];
        if (!error) {
            block();
        }else{
            [weakSelf showHint:NSEaseLocalizedString(@"message.videoFail", @"video for failure!")];
        }
    }];
}

#pragma mark - 暂时无用
// 传入消息是否需要发动已读回执
- (BOOL)shouldSendHasReadAckForMessage:(HDMessage *)message
                                  read:(BOOL)read
{
    if (message.direction == EMMessageDirectionSend || ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) || !self.isViewDidAppear)
    {
        return NO;
    }
    
    EMMessageBody *body = message.body;
    if (((body.type == EMMessageBodyTypeVideo) ||
         (body.type == EMMessageBodyTypeVoice) ||
         (body.type == EMMessageBodyTypeImage)) &&
        !read)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}


// 为传入的消息发送已读回执
- (void)_sendHasReadResponseForMessage:(NSArray *)messages
                                isRead:(BOOL)isRead {
    NSMutableArray *unreadMessages = [NSMutableArray array];
    for (NSInteger i = 0; i < messages.count; i ++) {
        HDMessage *message = messages[i];
        BOOL isSend = YES;
        if (_dataSource && [_dataSource respondsToSelector:@selector(messageViewController:shouldSendHasReadAckForMessage:read:)]) {
            isSend = [_dataSource messageViewController:self shouldSendHasReadAckForMessage:message read:isRead];
        }
        else {
            isSend = [self shouldSendHasReadAckForMessage:message read:isRead];
        }
        
        if (isSend) {
            [unreadMessages addObject:message];
        }
    }
    if ([unreadMessages count]) {
        for (HDMessage *message in unreadMessages) {
            // 下面API 仅限于语音是否设置为已读
            //[[HDClient sharedClient].chatManager setMessageListened:message];
        }
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id object = [self.listData objectAtIndex:indexPath.row];
    
    // time cell
    if ([object isKindOfClass:[NSString class]]) {
        NSString *timeCellIdentifier = [EaseMessageTimeCell cellIdentifier];
        EaseMessageTimeCell *timeCell = (EaseMessageTimeCell *)[tableView dequeueReusableCellWithIdentifier:timeCellIdentifier];
        if (!timeCell) {
            timeCell = [[EaseMessageTimeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:timeCellIdentifier];
            timeCell.titleLabelFont = [UIFont systemFontOfSize:12];
            timeCell.titleLabelColor = XHWhiteColor;
        }
        timeCell.title = object;
        return timeCell;
    }
    
    id <IMessageModel> model = object;
    if (_delegate && [_delegate respondsToSelector:@selector(messageViewController:cellForMessageModel:)]) {
        UITableViewCell *cell = [_delegate messageViewController:tableView cellForMessageModel:model];
        if (cell) {
            if ([cell isKindOfClass:[EaseMessageCell class]]) {
                EaseMessageCell *emcell= (EaseMessageCell*)cell;
                if (emcell.delegate == nil) {
                    emcell.delegate = self;
                }
            }
            return cell;
        }
    }
    
    NSString *CellIdentifier = [EaseMessageCell cellIdentifierWithModel:model];
    
    EaseBaseMessageCell *sendCell = (EaseBaseMessageCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (sendCell == nil) {
        sendCell = [[EaseBaseMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier model:model];
        sendCell.selectionStyle = UITableViewCellSelectionStyleNone;
        sendCell.delegate = self;
    }
    
    sendCell.model = model;
    return sendCell;
}

#pragma mark - UITableViewDeledate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id object = [self.listData objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[NSString class]]) {
        return self.timeCellHeight;
    }
    else {
        id <IMessageModel> model = object;
        if (_delegate && [_delegate respondsToSelector:@selector(messageViewController:heightForMessageModel:withCellWidth:)]) {
            CGFloat height = [_delegate messageViewController:self
                                        heightForMessageModel:model withCellWidth:tableView.frame.size.width];
            if (height) {
                return height;
            }
        }
        
        return [EaseBaseMessageCell cellHeightWithModel:model];
    }
}

#pragma mark - HDChatManagerDelegate
// 收到消息
- (void)messagesDidReceive:(NSArray *)aMessages {
    for (HDMessage *message in aMessages) {
        if ([self.conversation.conversationId isEqualToString:message.conversationId]) {
            [_conversation markAllMessagesAsRead:nil];
            [self addMessageToDataSource:message progress:nil];
        }
    }
}


// 收到cmd 消息
- (void)cmdMessagesDidReceive:(NSArray *)aCmdMessages {
    for (HDMessage *message in aCmdMessages) {
        if ([self.conversation.conversationId isEqualToString:message.conversationId]) {
            NSString *msg = [NSString stringWithFormat:@"%@", message.ext];
            NSLog(@"receive cmd message: %@", msg);
            break;
        }
    }
}

// 撤回消息
- (void)messagesDidRecall:(NSArray *)recallMessageIds {
    for (NSString *recallMsgId in recallMessageIds) {
        __block NSUInteger sourceIndex = NSNotFound;
        [self.messsagesSource enumerateObjectsUsingBlock:^(HDMessage *message, NSUInteger idx, BOOL *stop){
            if ([message isKindOfClass:[HDMessage class]]) {
                if ([recallMsgId isEqualToString:message.messageId])
                {
                    sourceIndex = idx;
                    *stop = YES;
                }
            }
        }];
        if (sourceIndex != NSNotFound) {
            [self.messsagesSource removeObjectAtIndex:sourceIndex];
        }
    }
    
    self.listData = [[self formatMessages:self.messsagesSource] mutableCopy];
    [self.m_tableView reloadData];
}

//  消息状态发生变化
- (void)messageStatusDidChange:(HDMessage *)aMessage
                         error:(HDError *)aError {
    [self _refreshAfterSentMessage:aMessage];
}

//  消息附件状态发生改变
- (void)messageAttachmentStatusDidChange:(HDMessage *)aMessage error:(HDError *)aError {
    if (!aError) {
        EMFileMessageBody *fileBody = (EMFileMessageBody*)[aMessage body];
        if ([fileBody type] == EMMessageBodyTypeImage) {
            EMImageMessageBody *imageBody = (EMImageMessageBody *)fileBody;
            if ([imageBody thumbnailDownloadStatus] == EMDownloadStatusSuccessed)
            {
                [self _reloadTableViewDataWithMessage:aMessage];
            }
        }
        else if([fileBody type] == EMMessageBodyTypeVideo) {
            EMVideoMessageBody *videoBody = (EMVideoMessageBody *)fileBody;
            if ([videoBody thumbnailDownloadStatus] == EMDownloadStatusSuccessed)
            {
                [self _reloadTableViewDataWithMessage:aMessage];
            }
        }
        else if([fileBody type] == EMMessageBodyTypeVoice) {
            if ([fileBody downloadStatus] == EMDownloadStatusSuccessed)
            {
                [self _reloadTableViewDataWithMessage:aMessage];
            }
        }
    }
}

#pragma mark - EMChatToolbarDelegate
- (void)chatToolbarDidChangeFrameToHeight:(CGFloat)toHeight
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = self.m_tableView.frame;
        rect.origin.y = kNavigationH;
        rect.size.height = self.view.frame.size.height - kNavigationH - toHeight - kBottom(0);
        self.m_tableView.frame = rect;
    }];
    
    [self _scrollViewToBottom:NO];
}

- (void)inputTextViewWillBeginEditing:(EaseTextView *)inputTextView
{
    if (_menuController == nil) {
        _menuController = [UIMenuController sharedMenuController];
    }
    [_menuController setMenuItems:nil];
}

- (void)inputTextViewDidBeginEditing:(EaseTextView *)inputTextView {
    NSString *from = [[HDClient sharedClient] currentUsername];
    
    EMCmdMessageBody *body = [[EMCmdMessageBody alloc] initWithAction:@"TypingBegin"];
    body.isDeliverOnlineOnly = YES;
    
    HDMessage *message = [[HDMessage alloc] initWithConversationID:self.conversation.conversationId from:from to:self.conversation.conversationId body:body];
    
    [[HDClient sharedClient].chatManager sendMessage:message
                                            progress:^(int progress) {
                                                
                                            } completion:^(HDMessage *aMessage, HDError *aError) {
                                                
                                            }];
}

- (void)didSendText:(NSString *)text
{
    if (text && text.length > 0) {
        [self sendTextMessage:text];
    }
    
    NSString *from = [[HDClient sharedClient] currentUsername];
    
    EMCmdMessageBody *body = [[EMCmdMessageBody alloc] initWithAction:@"TypingEnd"];
    body.isDeliverOnlineOnly = YES;
    
    HDMessage *message = [[HDMessage alloc] initWithConversationID:self.conversation.conversationId from:from to:self.conversation.conversationId body:body];
    [[HDClient sharedClient].chatManager sendMessage:message
                                            progress:^(int progress) {
                                                
                                            } completion:^(HDMessage *aMessage, HDError *aError) {
                                                
                                            }];
}

- (BOOL)didInputAtInLocation:(NSUInteger)location
{
    return NO;
}

- (BOOL)didDeleteCharacterFromLocation:(NSUInteger)location
{
    return NO;
}

- (void)didSendText:(NSString *)text withExt:(NSDictionary*)ext
{
    if ([ext objectForKey:EASEUI_EMOTION_DEFAULT_EXT]) {
        EaseEmotion *emotion = [ext objectForKey:EASEUI_EMOTION_DEFAULT_EXT];
        if (self.dataSource && [self.dataSource respondsToSelector:@selector(emotionExtFormessageViewController:easeEmotion:)]) {
            NSDictionary *ext = [self.dataSource emotionExtFormessageViewController:self easeEmotion:emotion];
            [self sendTextMessage:emotion.emotionTitle withExt:ext];
        } else {
            [self sendTextMessage:emotion.emotionTitle withExt:@{MESSAGE_ATTR_EXPRESSION_ID:emotion.emotionId,MESSAGE_ATTR_IS_BIG_EXPRESSION:@(YES)}];
        }
        return;
    }
    if (text && text.length > 0) {
        [self sendTextMessage:text withExt:ext];
    }
}

#pragma mark - SatisfactionDelegate
- (void)backFromSatisfactionViewController {
    
}

#pragma mark - EaseMessageCellDelegate
- (void)messageCellSelected:(id<IMessageModel>)model fromCell:(EaseMessageCell *)cell{
    switch (model.bodyType) {
        case EMMessageBodyTypeText: {
            HDExtMsgType extMsgType = [HDMessageHelper getMessageExtType:model.message];
            switch (extMsgType) {
                case HDExtEvaluationMsg: {
                    // 满意度
                    SatisfactionViewController *view = [[SatisfactionViewController alloc] init];
                    view.messageModel = model;
                    view.delegate = self;
                    [self.navigationController pushViewController:view animated:YES];
                }
                    break;
                case HDExtOrderMsg: {
                    // 订单
                    [self _orderMessageCellSelected:model];
                }
                    break;
                case HDExtToCustomServiceMsg: {
                    // 转人工
                    if (_isSendingTransformMessage) return;
                    _isSendingTransformMessage = YES;
                    __block HDMessage *message = model.message;
                    NSDictionary *weichat = [message.ext objectForKey:kMesssageExtWeChat];
                    NSDictionary *ctrlArgs = [weichat objectForKey:kMesssageExtWeChat_ctrlArgs];
                    ControlArguments *arguments = [ControlArguments new];
                    arguments.identity = [ctrlArgs valueForKey:@"id"];
                    arguments.sessionId = [ctrlArgs valueForKey:@"serviceSessionId"];
                    HDControlMessage *hcont = [HDControlMessage new];
                    hcont.arguments = arguments;
                    if ([HDMessageHelper getMessageExtType:message] == HDExtToCustomServiceMsg) {
                        //发送透传消息
                        HDMessage *aHMessage = [HDSDKHelper cmdMessageFormatTo:self.conversation.conversationId];
                        [aHMessage addCompositeContent:hcont];
                        __weak typeof(self) weakSelf = self;
                        [[HDClient sharedClient].chatManager sendMessage:aHMessage progress:nil completion:^(HDMessage *aMessage, HDError *aError)
                         {
                             _isSendingTransformMessage = NO;
                             if (!aError) {
                                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                     //更新ext，目的当点击一次转人工客服按钮且cmd发送成功后，此按钮不在被使用
                                     [weakSelf updateTransferMessageExt:message];
                                 });
                             } else {
                                 [weakSelf showHint:NSLocalizedString(@"transferToKf.fail", @"Transfer to the artificial customer service request failed, please confirm the connection status!")];
                             }
                         }];
                    }
                    
                }
                    break;
                   default:
                    break;
            };
        }
            break;
        case EMMessageBodyTypeImage: {
            // 图片
            [self _pictureMessageCellSelected:model fromCell:cell];
        }
            break;
        case EMMessageBodyTypeVideo: {
            // 视频
            [self _videoMessageCellSelected:model];
        }
            break;
            
        default:
            break;
    };
}


#pragma mark - EaseChatBarMoreViewDelegate

- (void)moreView:(EaseChatBarMoreView *)moreView didItemInMoreViewAtIndex:(NSInteger)index
{
    if ([self.delegate respondsToSelector:@selector(messageViewController:didSelectMoreView:AtIndex:)]) {
        [self.delegate messageViewController:self didSelectMoreView:moreView AtIndex:index];
        return;
    }
}

- (void)moreViewPhotoAction:(EaseChatBarMoreView *)moreView
{
    // Hide the keyboard
    [self.chatToolbar endEditing:YES];
    
    [self presentViewController:self.tzimagePicker animated:YES completion:NULL];
    self.isViewDidAppear = NO;
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:YES];
}

- (void)moreViewTakePicAction:(EaseChatBarMoreView *)moreView
{
    // Hide the keyboard
    [self.chatToolbar endEditing:YES];
    
#if TARGET_IPHONE_SIMULATOR
    [self showHint:NSEaseLocalizedString(@"message.simulatorNotSupportCamera", @"simulator does not support taking picture")];
#elif TARGET_OS_IPHONE
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
    [self presentViewController:self.imagePicker animated:YES completion:NULL];
    
    self.isViewDidAppear = NO;
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:YES];
#endif
}

// 视频
- (void)moreViewVideoAction:(EaseChatBarMoreView *)moreView {
    // Hide the keyboard
    [self.chatToolbar endEditing:YES];
    
#if TARGET_IPHONE_SIMULATOR
    [self showHint:NSEaseLocalizedString(@"message.simulatorNotSupportCamera", @"simulator does not support taking picture")];
#elif TARGET_OS_IPHONE
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
    [self presentViewController:self.imagePicker animated:YES completion:NULL];
    
    self.isViewDidAppear = NO;
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:YES];
#endif
}


#pragma mark - TZImagePickerController
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    [self.tzimagePicker dismissViewControllerAnimated:YES completion:nil];
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:NO];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto infos:(NSArray<NSDictionary *> *)infos {
    if ([[photos firstObject] isKindOfClass:[UIImage class]]) {
        [self sendImageMessage:[photos firstObject]];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        NSURL *videoURL = info[UIImagePickerControllerMediaURL];
        // video url:
        // file:///private/var/mobile/Applications/B3CDD0B2-2F19-432B-9CFA-158700F4DE8F/tmp/capture-T0x16e39100.tmp.9R8weF/capturedvideo.mp4
        // we will convert it to mp4 format
        NSURL *mp4 = [self _convert2Mp4:videoURL];
        NSFileManager *fileman = [NSFileManager defaultManager];
        if ([fileman fileExistsAtPath:videoURL.path]) {
            NSError *error = nil;
            [fileman removeItemAtURL:videoURL error:&error];
            if (error) {
                NSLog(@"failed to remove file, error:%@.", error);
            }
        }
        [self sendVideoMessageWithURL:mp4];
        
    }else{
        
        NSURL *url = info[UIImagePickerControllerReferenceURL];
        if (url == nil) {
            UIImage *orgImage = info[UIImagePickerControllerOriginalImage];
            [self sendImageMessage:orgImage];
        } else {
            if ([[UIDevice currentDevice].systemVersion doubleValue] >= 9.0f) {
                PHFetchResult *result = [PHAsset fetchAssetsWithALAssetURLs:@[url] options:nil];
                [result enumerateObjectsUsingBlock:^(PHAsset *asset , NSUInteger idx, BOOL *stop){
                    if (asset) {
                        [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *data, NSString *uti, UIImageOrientation orientation, NSDictionary *dic){
                            if (data != nil) {
                                [self sendImageMessageWithData:data];
                            } else {
                                [self showHint:NSEaseLocalizedString(@"message.smallerImage", @"The image size is too large, please choose another one")];
                            }
                        }];
                    }
                }];
            } else {
                ALAssetsLibrary *alasset = [[ALAssetsLibrary alloc] init];
                [alasset assetForURL:url resultBlock:^(ALAsset *asset) {
                    if (asset) {
                        ALAssetRepresentation* assetRepresentation = [asset defaultRepresentation];
                        Byte* buffer = (Byte*)malloc((size_t)[assetRepresentation size]);
                        NSUInteger bufferSize = [assetRepresentation getBytes:buffer fromOffset:0.0 length:(NSUInteger)[assetRepresentation size] error:nil];
                        NSData* fileData = [NSData dataWithBytesNoCopy:buffer length:bufferSize freeWhenDone:YES];
                        [self sendImageMessageWithData:fileData];
                    }
                } failureBlock:NULL];
            }
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    self.isViewDidAppear = YES;
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.imagePicker dismissViewControllerAnimated:YES completion:nil];
    
    self.isViewDidAppear = YES;
    [[HDSDKHelper shareHelper] setIsShowingimagePicker:NO];
}

@end
