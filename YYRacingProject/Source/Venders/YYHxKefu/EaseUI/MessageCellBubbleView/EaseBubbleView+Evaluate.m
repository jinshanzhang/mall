//
//  EaseBubbleView+Evaluate.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseBubbleView+Evaluate.h"

@implementation EaseBubbleView (Evaluate)

#pragma mark - Private method
- (void)_setupEvaluateBubbleMarginConstraints {
    NSLayoutConstraint *evalTitleMarginTopConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.margin.top];
    NSLayoutConstraint *evalTitleMarginLeftConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateTitleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:self.margin.left];
    NSLayoutConstraint *evalTitleMarginRightConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateTitleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-self.margin.right];
    
    /*NSLayoutConstraint *evalButtonMarginTopConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.evaluateTitleLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10];*/
    NSLayoutConstraint *evalButtonMarginBottomConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-self.margin.bottom];
    NSLayoutConstraint *evalButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:kSizeScale(75)];
    NSLayoutConstraint *evalButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:kSizeScale(25)];
    NSLayoutConstraint *evalButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:self.evaluateButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    
    [self.marginConstraints removeAllObjects];
    
    [self.marginConstraints addObject:evalTitleMarginTopConstraint];
    [self.marginConstraints addObject:evalTitleMarginLeftConstraint];
    [self.marginConstraints addObject:evalTitleMarginRightConstraint];
    
    //[self.marginConstraints addObject:evalButtonMarginTopConstraint];
    [self.marginConstraints addObject:evalButtonCenterXConstraint];
    [self.marginConstraints addObject:evalButtonWidthConstraint];
    [self.marginConstraints addObject:evalButtonHeightConstraint];
    [self.marginConstraints addObject:evalButtonMarginBottomConstraint];

    [self addConstraints:self.marginConstraints];
}

#pragma mark - Public method
- (void)setupEvaluateBubbleView {
    
    self.evaluateTitleLabel = [[UILabel alloc] init];
    self.evaluateTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.evaluateTitleLabel.backgroundColor = [UIColor clearColor];
    self.evaluateTitleLabel.font = [UIFont systemFontOfSize:14];
    self.evaluateTitleLabel.numberOfLines = 0;
    self.evaluateTitleLabel.text = @"请对我的服务进行评价";
    self.evaluateTitleLabel.textColor = XHBlackColor;
    [self.backgroundImageView addSubview:self.evaluateTitleLabel];
    
    self.evaluateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.evaluateButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.evaluateButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [self.evaluateButton setTitleColor:XHGaldLightColor forState:UIControlStateNormal];
    self.evaluateButton.backgroundColor = XHWhiteColor;
    self.evaluateButton.layer.cornerRadius = 2.f;
    self.evaluateButton.layer.borderColor = XHGaldLightColor.CGColor;
    self.evaluateButton.layer.borderWidth = 1;
    [self.evaluateButton setTitle:@"去评价" forState:UIControlStateNormal];
    /*[self.evaluateButton addTarget:self action:@selector(evaluateAction:) forControlEvents:UIControlEventTouchUpInside];*/
    [self.backgroundImageView addSubview:self.evaluateButton];
    
    [self _setupEvaluateBubbleMarginConstraints];
}

- (void)updateEvaluateMargin:(UIEdgeInsets)margin {
    if (_margin.top == margin.top && _margin.bottom == margin.bottom && _margin.left == margin.left && _margin.right == margin.right) {
        return;
    }
    _margin = margin;
    _margin = UIEdgeInsetsMake(15, 15, 12, 12);
    [self removeConstraints:self.marginConstraints];
    [self _setupEvaluateBubbleMarginConstraints];
}

@end
