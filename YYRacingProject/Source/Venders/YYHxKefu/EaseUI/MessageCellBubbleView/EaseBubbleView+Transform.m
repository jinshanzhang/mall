//
//  EaseBubbleView+Transform.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseBubbleView+Transform.h"

@implementation EaseBubbleView (Transform)

#pragma mark - Private method
- (void)_setupTransformBubbleMarginConstraints {
    NSLayoutConstraint *transformTitleMarginTopConstraint = [NSLayoutConstraint constraintWithItem:self.transformTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:self.margin.top];
    NSLayoutConstraint *transformTitleMarginLeftConstraint = [NSLayoutConstraint constraintWithItem:self.transformTitleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:self.margin.left];
    NSLayoutConstraint *transformTitleMarginRightConstraint = [NSLayoutConstraint constraintWithItem:self.transformTitleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-self.margin.right];
    
    /*NSLayoutConstraint *transformButtonMarginTopConstraint = [NSLayoutConstraint constraintWithItem:self.transformuateButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.transformuateTitleLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:10];*/
    NSLayoutConstraint *transformButtonMarginBottomConstraint = [NSLayoutConstraint constraintWithItem:self.transformButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-self.margin.bottom];
    NSLayoutConstraint *transformButtonWidthConstraint = [NSLayoutConstraint constraintWithItem:self.transformButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:kSizeScale(75)];
    NSLayoutConstraint *transformButtonHeightConstraint = [NSLayoutConstraint constraintWithItem:self.transformButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:kSizeScale(25)];
    NSLayoutConstraint *transformButtonCenterXConstraint = [NSLayoutConstraint constraintWithItem:self.transformButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    
    [self.marginConstraints removeAllObjects];
    
    [self.marginConstraints addObject:transformTitleMarginTopConstraint];
    [self.marginConstraints addObject:transformTitleMarginLeftConstraint];
    [self.marginConstraints addObject:transformTitleMarginRightConstraint];
    
    //[self.marginConstraints addObject:transformButtonMarginTopConstraint];
    [self.marginConstraints addObject:transformButtonCenterXConstraint];
    [self.marginConstraints addObject:transformButtonWidthConstraint];
    [self.marginConstraints addObject:transformButtonHeightConstraint];
    [self.marginConstraints addObject:transformButtonMarginBottomConstraint];
    
    [self addConstraints:self.marginConstraints];
}


#pragma mark - Public method

- (void)setupTransformBubbleView {
    self.transformTitleLabel = [[UILabel alloc] init];
    self.transformTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.transformTitleLabel.backgroundColor = [UIColor clearColor];
    self.transformTitleLabel.font = [UIFont systemFontOfSize:14];
    self.transformTitleLabel.numberOfLines = 0;
    self.transformTitleLabel.textColor = XHBlackColor;
    [self.backgroundImageView addSubview:self.transformTitleLabel];
    
    self.transformButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.transformButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.transformButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [self.transformButton setTitleColor:XHGaldLightColor forState:UIControlStateNormal];
    self.transformButton.backgroundColor = XHWhiteColor;
    self.transformButton.layer.cornerRadius = 2.f;
    self.transformButton.layer.borderColor = XHGaldLightColor.CGColor;
    self.transformButton.layer.borderWidth = 1;
    [self.transformButton setTitle:@"人工客服" forState:UIControlStateNormal];
    /*[self.transformButton addTarget:self action:@selector(transformAction:) forControlEvents:UIControlEventTouchUpInside];*/
    [self.backgroundImageView addSubview:self.transformButton];
}

- (void)updateTransformMargin:(UIEdgeInsets)margin {
    if (_margin.top == margin.top && _margin.bottom == margin.bottom && _margin.left == margin.left && _margin.right == margin.right) {
        return;
    }
    _margin = margin;
    _margin = UIEdgeInsetsMake(15, 15, 12, 12);
    [self removeConstraints:self.marginConstraints];
    [self _setupTransformBubbleMarginConstraints];
}


@end
