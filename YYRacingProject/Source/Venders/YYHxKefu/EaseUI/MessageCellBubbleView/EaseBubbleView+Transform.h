//
//  EaseBubbleView+Transform.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseBubbleView.h"

NS_ASSUME_NONNULL_BEGIN

@interface EaseBubbleView (Transform)

/*!
 @method
 @brief 构建转人工类型消息气泡视图
 @discussion
 @result
 */
- (void)setupTransformBubbleView;

/*!
 @method
 @brief 变更转人工类型气泡的边距，并更新改子视图约束
 @discussion
 @param margin 气泡边距
 @result
 */
- (void)updateTransformMargin:(UIEdgeInsets)margin;

@end

NS_ASSUME_NONNULL_END
