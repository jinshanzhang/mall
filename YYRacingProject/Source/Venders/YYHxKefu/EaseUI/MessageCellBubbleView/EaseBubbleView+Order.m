//
//  EaseBubbleView+Order.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseBubbleView+Order.h"

@implementation EaseBubbleView (Order)

#pragma mark - Private method
- (void)_setupOrderBubbleMarginConstraints {
    
    // bgView
    NSLayoutConstraint *orderBgViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.orderBgView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:5.0];
    NSLayoutConstraint *orderBgViewLeftConstraint = [NSLayoutConstraint constraintWithItem:self.orderBgView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:5.0];
    NSLayoutConstraint *orderBgViewRightConstraint = [NSLayoutConstraint constraintWithItem:self.orderBgView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.0];
    NSLayoutConstraint *orderBgViewBottomhtConstraint = [NSLayoutConstraint constraintWithItem:self.orderBgView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-5.0];
    
    // order number
    NSLayoutConstraint *orderNumberTopConstraint = [NSLayoutConstraint constraintWithItem:self.orderNumberLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderBgView attribute:NSLayoutAttributeTop multiplier:1.0 constant:7];
    NSLayoutConstraint *orderNumberLeftConstraint = [NSLayoutConstraint constraintWithItem:self.orderNumberLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.orderBgView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:6];
    NSLayoutConstraint *orderNumberRightConstraint = [NSLayoutConstraint constraintWithItem:self.orderNumberLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.orderBgView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-6];
    
    // good image
    NSLayoutConstraint *goodImageLeftConstraint = [NSLayoutConstraint constraintWithItem:self.orderGoodImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.orderBgView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:6];
    
    NSLayoutConstraint *goodImageTopConstraint = [NSLayoutConstraint constraintWithItem:self.orderGoodImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderNumberLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8];
    
    NSLayoutConstraint *goodImageWidthConstraint = [NSLayoutConstraint constraintWithItem:self.orderGoodImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1.0 constant:kSizeScale(45)];
    
    NSLayoutConstraint *goodImageHeightConstraint = [NSLayoutConstraint constraintWithItem:self.orderGoodImageView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:kSizeScale(45)];
    
    /*NSLayoutConstraint *goodImageBottomConstraint = [NSLayoutConstraint constraintWithItem:self.orderGoodImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.orderBgView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-12];*/

    // title
    NSLayoutConstraint *orderTitleLeftConstraint = [NSLayoutConstraint constraintWithItem:self.orderTitleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.orderGoodImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:6];
    
    NSLayoutConstraint *orderTitleTopConstraint = [NSLayoutConstraint constraintWithItem:self.orderTitleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderGoodImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:3];
    
    NSLayoutConstraint *orderTitleRightConstraint = [NSLayoutConstraint constraintWithItem:self.orderTitleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.orderBgView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-6];
    // price
    NSLayoutConstraint *orderPriceLeftConstraint = [NSLayoutConstraint constraintWithItem:self.orderPriceLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.orderTitleLabel attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    
    NSLayoutConstraint *orderPriceTopConstraint = [NSLayoutConstraint constraintWithItem:self.orderPriceLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.orderTitleLabel attribute:NSLayoutAttributeBottom multiplier:1.0 constant:3];
    
    [self.marginConstraints removeAllObjects];
    
    [self.marginConstraints addObject:orderBgViewTopConstraint];
    [self.marginConstraints addObject:orderBgViewLeftConstraint];
    [self.marginConstraints addObject:orderBgViewRightConstraint];
    [self.marginConstraints addObject:orderBgViewBottomhtConstraint];
    
    [self.marginConstraints addObject:orderNumberTopConstraint];
    [self.marginConstraints addObject:orderNumberLeftConstraint];
    [self.marginConstraints addObject:orderNumberRightConstraint];
    
    [self.marginConstraints addObject:goodImageTopConstraint];
    [self.marginConstraints addObject:goodImageLeftConstraint];
    [self.marginConstraints addObject:goodImageWidthConstraint];
    [self.marginConstraints addObject:goodImageHeightConstraint];
    //[self.marginConstraints addObject:goodImageBottomConstraint];
    
    [self.marginConstraints addObject:orderTitleTopConstraint];
    [self.marginConstraints addObject:orderTitleLeftConstraint];
    [self.marginConstraints addObject:orderTitleRightConstraint];
    
    [self.marginConstraints addObject:orderPriceTopConstraint];
    [self.marginConstraints addObject:orderPriceLeftConstraint];
    
    [self addConstraints:self.marginConstraints];
}

#pragma mark - Public method
- (void)setupOrderBubbleView {
    self.orderBgView = [[UIView alloc] init];
    self.orderBgView.translatesAutoresizingMaskIntoConstraints = NO;
    self.orderBgView.backgroundColor = [UIColor whiteColor];
    [self.backgroundImageView addSubview:self.orderBgView];
    
    self.orderTitleLabel = [[UILabel alloc] init];
    self.orderTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.orderTitleLabel.backgroundColor = [UIColor clearColor];
    self.orderTitleLabel.font = [UIFont systemFontOfSize:13];
    [self.orderBgView addSubview:self.orderTitleLabel];
    
    self.orderNumberLabel = [[UILabel alloc] init];
    self.orderNumberLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.orderNumberLabel.backgroundColor = [UIColor clearColor];
    self.orderNumberLabel.font = [UIFont systemFontOfSize:12];
    [self.orderBgView addSubview:self.orderNumberLabel];
    
    self.orderGoodImageView = [[UIImageView alloc] init];
    self.orderGoodImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.orderGoodImageView.backgroundColor = [UIColor clearColor];
    self.orderGoodImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.orderBgView addSubview:self.orderGoodImageView];
    
    self.orderPriceLabel = [[UILabel alloc] init];
    self.orderPriceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.orderPriceLabel.backgroundColor = [UIColor clearColor];
    self.orderPriceLabel.font = [UIFont systemFontOfSize:15];
    self.orderPriceLabel.textColor = [UIColor redColor];
    [self.orderBgView addSubview:self.orderPriceLabel];
    
    [self _setupOrderBubbleMarginConstraints];
}

- (void)updateOrderMargin:(UIEdgeInsets)margin {
    if (_margin.top == margin.top && _margin.bottom == margin.bottom && _margin.left == margin.left && _margin.right == margin.right) {
        return;
    }
    _margin = margin;
    [self removeConstraints:self.marginConstraints];
    [self _setupOrderBubbleMarginConstraints];
}

@end
