//
//  EaseBubbleView+Evaluate.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseBubbleView.h"

NS_ASSUME_NONNULL_BEGIN

@interface EaseBubbleView (Evaluate)

/*!
 @method
 @brief 构建评价类型消息气泡视图
 @discussion
 @result
 */
- (void)setupEvaluateBubbleView;

/*!
 @method
 @brief 变更评价类型气泡的边距，并更新改子视图约束
 @discussion
 @param margin 气泡边距
 @result
 */
- (void)updateEvaluateMargin:(UIEdgeInsets)margin;

@end

NS_ASSUME_NONNULL_END
