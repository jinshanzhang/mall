//
//  EaseBubbleView+Order.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseBubbleView.h"

NS_ASSUME_NONNULL_BEGIN

@interface EaseBubbleView (Order)

/*!
 @method
 @brief 构建订单类型消息气泡视图
 @discussion
 @result
 */
- (void)setupOrderBubbleView;

/*!
 @method
 @brief 变更订单类型气泡的边距，并更新改子视图约束
 @discussion
 @param margin 气泡边距
 @result
 */
- (void)updateOrderMargin:(UIEdgeInsets)margin;

@end

NS_ASSUME_NONNULL_END
