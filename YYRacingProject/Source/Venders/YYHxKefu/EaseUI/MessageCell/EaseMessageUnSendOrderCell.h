//
//  EaseMessageUnSendOrderCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol EaseMessageUnSendOrderCellDelegate <NSObject>

@optional
- (void)sendOrderClickEvent;

@end


@interface EaseMessageUnSendOrderCell : UITableViewCell

@property (nonatomic, strong) UILabel *orderNumberLabel;
@property (nonatomic, strong) UILabel *orderTitleLabel;
@property (nonatomic, strong) UILabel *orderPriceLabel;

@property (nonatomic, strong) UIButton *sendOrderButton;
@property (nonatomic, strong) UIImageView *orderGoodImageView;
@property (nonatomic, strong) UIView   *orderBgView;

@property (strong, nonatomic) id<IMessageModel> model;

@property (nonatomic, weak) id <EaseMessageUnSendOrderCellDelegate> delegate;

+ (NSString *)cellIdentifier;

@end

NS_ASSUME_NONNULL_END
