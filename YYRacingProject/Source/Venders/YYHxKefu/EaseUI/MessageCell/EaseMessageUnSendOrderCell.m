//
//  EaseMessageUnSendOrderCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "EaseMessageUnSendOrderCell.h"

@implementation EaseMessageUnSendOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = HexRGB(0xf3f3f3);
        [self _setupSubview];
    }
    
    return self;
}

- (void)_setupSubview {
    
    self.orderBgView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.orderBgView];
    
    self.orderNumberLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(12)
                                             textColor:XHBlackLitColor];
    [self.orderBgView addSubview:self.orderNumberLabel];
    
    self.orderGoodImageView = [YYCreateTools createImageView:nil
                                                   viewModel:-1];
    [self.orderBgView addSubview:self.orderGoodImageView];
    
    self.orderTitleLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(13)
                                            textColor:XHBlackColor];
    self.orderTitleLabel.numberOfLines = 0;
    [self.orderBgView addSubview:self.orderTitleLabel];
    
    self.orderPriceLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(13)
                                            textColor:XHRedColor];
    [self.orderBgView addSubview:self.orderPriceLabel];
    
    self.sendOrderButton = [YYCreateTools createBtn:@"发送订单"
                                               font:normalFont(13)
                                          textColor:XHGaldLightColor];
    self.sendOrderButton.layer.borderColor = XHGaldLightColor.CGColor;
    self.sendOrderButton.layer.borderWidth = 1;
    [self.sendOrderButton addTarget:self action:@selector(sendOrderClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.sendOrderButton.v_cornerRadius = kSizeScale(12);
    [self.orderBgView addSubview:self.sendOrderButton];
    
    [self _setSubViewsLayout];
}


- (void)_setSubViewsLayout {
    
    [self.orderBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(146));
    }];
    [self.orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(10));
    }];
    [self.orderGoodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.orderNumberLabel.mas_bottom).offset(kSizeScale(10));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(55), kSizeScale(55)));
    }];
    [self.orderTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.orderGoodImageView);
        make.left.equalTo(self.orderGoodImageView.mas_right).offset(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    [self.orderPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderTitleLabel);
        make.bottom.equalTo(self.orderGoodImageView.mas_bottom);
    }];
    [self.sendOrderButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(120), kSizeScale(24)));
        make.bottom.mas_equalTo(-kSizeScale(16));
    }];
}

- (void)setModel:(id<IMessageModel>)model {
    _model = model;
    if ([model.message.ext objectForKey:@"msgtype"]) {
        NSDictionary *messageType = [model.message.ext objectForKey:@"msgtype"];
        if ([messageType objectForKey:@"order"]) {
            NSDictionary *orderType = [messageType objectForKey:@"order"];
            if ([orderType objectForKey:@"img_url"]) {
                [self.orderGoodImageView sd_setImageWithURL:[NSURL URLWithString:[orderType objectForKey:@"img_url"]]];
            }
            if ([orderType objectForKey:@"order_title"]) {
                self.orderTitleLabel.text = [orderType objectForKey:@"order_title"];
            }
            if ([orderType objectForKey:@"price"]) {
                NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:midFont(10) symbolTextColor:XHRedColor wordSpace:0 price:[orderType objectForKey:@"price"] priceFont:boldFont(13) priceTextColor: XHRedColor symbolOffsetY:0];
                self.orderPriceLabel.attributedText = attribut;
            }
            if ([orderType objectForKey:@"title"]) {
                self.orderNumberLabel.text = [orderType objectForKey:@"title"];
            }
        }
    }
}

#pragma mark - Event
- (void)sendOrderClickEvent:(UIButton *)button {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendOrderClickEvent)]) {
        [self.delegate sendOrderClickEvent];
    }
}

#pragma mark - public

+ (NSString *)cellIdentifier
{
    return @"EaseMessageUnSendOrderCell";
}

@end
