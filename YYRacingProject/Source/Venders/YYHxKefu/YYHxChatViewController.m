//
//  YYHxChatViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/12.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHxChatViewController.h"

#import "EaseMessageUnSendOrderCell.h"
@interface YYHxChatViewController ()
<HDClientDelegate,
EaseMessageUnSendOrderCellDelegate>

@property (nonatomic) NSMutableDictionary *emotionDic;
@property (nonatomic, strong) HDMessage  *needSendMessage;

@end

@implementation YYHxChatViewController

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    self.delegate = self;
    self.dataSource = self;
    self.needSendMessage = nil;
    self.showRefreshHeader = YES;
    [self xh_addTitle:@"在线客服"];
    [self xh_popTopRootViewController:NO];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"orderInfo"]) {
            HDOrderInfo *orderInfo = [self.parameter objectForKey:@"orderInfo"];
            HDMessage *message = [HDSDKHelper textHMessageFormatWithText:@""
                                                                      to:self.conversation.conversationId];
            message.status = HDMessageStatusPending;
            [message addContent:orderInfo];
            NSMutableDictionary *ext = [message.ext mutableCopy];
            [ext setObject:@"orderMessage" forKey:@"custom_message"];
            message.ext = [ext copy];
            self.needSendMessage = message;
        }
    }
    
    self.messageListRefreshBlock = ^(NSInteger times) {
        @strongify(self);
        if (times == 1) {
            [self insertOrderShowMessage];
        }
    };
    // Do any additional setup after loading the view.
}

#pragma mark - Base method



#pragma mark - Private method
- (void)insertOrderShowMessage {
    if (self.needSendMessage) {
        [self addMessageToDataSource:self.needSendMessage progress:nil];
    }
}

/**删除某条消息**/
- (void)deleteSpecialMessage {
    if (self.needSendMessage) {
        __block NSInteger sourecIndex = -1;
        __block NSInteger listDataIndex = -1;
        [self.messsagesSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HDMessage *message = (HDMessage *)obj;
            if ([message.messageId isEqualToString:self.needSendMessage.messageId]) {
                sourecIndex = idx;
                *stop = YES;
            }
        }];
        [self.listData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if (![obj isKindOfClass:[NSString class]]) {
                EaseMessageModel *model = (EaseMessageModel *)obj;
                if ([model.messageId isEqualToString:self.needSendMessage.messageId]) {
                    listDataIndex = idx;
                    *stop = YES;
                }
            }
        }];
        if (sourecIndex > 0 && listDataIndex > 0) {
            [self.messsagesSource removeObjectAtIndex:sourecIndex];
            [self.listData removeObjectAtIndex:listDataIndex];
            [self.m_tableView reloadData];
        }
    }
}


#pragma mark - YYHxMessageViewControllerDelegate
/*!
 @brief 获取消息自定义cell
 @discussion 用户根据messageModel判断是否显示自定义cell,返回nil显示默认cell,否则显示用户自定义cell
 **/
- (UITableViewCell *)messageViewController:(UITableView *)tableView
                       cellForMessageModel:(id<IMessageModel>)messageModel {
    NSDictionary *ext = messageModel.message.ext;
    if ([ext objectForKey:@"em_recall"]) {
        NSString *TimeCellIdentifier = [EaseMessageTimeCell cellIdentifier];
        EaseMessageTimeCell *recallCell = (EaseMessageTimeCell *)[tableView dequeueReusableCellWithIdentifier:TimeCellIdentifier];
        
        if (recallCell == nil) {
            recallCell = [[EaseMessageTimeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TimeCellIdentifier];
            recallCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        EMTextMessageBody *body = (EMTextMessageBody*)messageModel.message.body;
        recallCell.title = body.text;
        return recallCell;
    }
    else if ([ext objectForKey:@"custom_message"] && messageModel.messageStatus == HDMessageStatusPending) {
        NSString *unSendCellIdentifier = [EaseMessageUnSendOrderCell cellIdentifier];
        EaseMessageUnSendOrderCell *recallCell = (EaseMessageUnSendOrderCell *)[tableView dequeueReusableCellWithIdentifier:unSendCellIdentifier];
        
        if (recallCell == nil) {
            recallCell = [[EaseMessageUnSendOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:unSendCellIdentifier];
            recallCell.selectionStyle = UITableViewCellSelectionStyleNone;
            recallCell.delegate = self;
            recallCell.model = messageModel;
        }
        return recallCell;
    }
    return nil;
}

/*!
 @brief 获取消息cell高度
 @discussion 用户根据messageModel判断,是否自定义显示cell的高度
 */
- (CGFloat)messageViewController:(YYHxMessageViewController *)viewController
           heightForMessageModel:(id<IMessageModel>)messageModel
                   withCellWidth:(CGFloat)cellWidth {
    NSDictionary *ext = messageModel.message.ext;
    if ([ext objectForKey:@"em_recall"]) {
        return self.timeCellHeight;
    }
    else if ([ext objectForKey:@"custom_message"] && messageModel.messageStatus == HDMessageStatusPending) {
        return kSizeScale(160);
    }
    return 0;
}

#pragma mark - YYHxMessageViewControllerDataSource

/*!
 @brief 将HDMessage类型转换为符合<IMessageModel>协议的类型
 @discussion 将HDMessage类型转换为符合<IMessageModel>协议的类型,设置用户信息,消息显示用户昵称和头像
 **/
- (id<IMessageModel>)messageViewController:(YYHxMessageViewController *)viewController
                           modelForMessage:(HDMessage *)message {
    id<IMessageModel> model = nil;
    model = [[EaseMessageModel alloc] initWithMessage:message];
    if (model.isSender) {
        model.avatarURLPath = kUserInfo.avatarUrl;
        model.nickname = kUserInfo.userName;
    }
    else {
        model.avatarImage = [UIImage imageNamed:@"kefu_logo_icon"];
        model.failImageName = @"kefu_logo_icon";
        if ([message.ext objectForKey:@"weichat"]) {
            NSDictionary *wechatDict = [message.ext objectForKey:@"weichat"];
            if ([wechatDict objectForKey:@"agent"]) {
                NSDictionary *agentDict = [wechatDict objectForKey:@"agent"];
                if ([agentDict objectForKey:@"userNickname"]) {
                    model.nickname = [agentDict objectForKey:@"userNickname"];
                }
            }
        }
    }
    return model;
}

/*!
 @brief 是否允许长按
 @discussion 获取是否允许长按的回调,默认是NO
 **/
- (BOOL)messageViewController:(YYHxMessageViewController *)viewController
   canLongPressRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

/*!
 @brief 触发长按手势
 @discussion 获取触发长按手势的回调,默认是NO
 **/
- (BOOL)messageViewController:(YYHxMessageViewController *)viewController
   didLongPressRowAtIndexPath:(NSIndexPath *)indexPath {
    id object = [self.listData objectAtIndex:indexPath.row];
    if (![object isKindOfClass:[NSString class]]) {
        EaseMessageCell *cell = (EaseMessageCell *)[self.m_tableView cellForRowAtIndexPath:indexPath];
        if ([cell isKindOfClass:[EaseMessageCell class]]) {
            [cell becomeFirstResponder];
            self.menuIndexPath = indexPath;
            //[self showMenuViewController:cell.bubbleView andIndexPath:indexPath messageType:cell.model.bodyType];
        }
    }
    return YES;
}


/*!
 @brief 判断消息是否为表情消息
 @discussion
 */
- (BOOL)isEmotionMessageFormessageViewController:(YYHxMessageViewController *)viewController
                                    messageModel:(id<IMessageModel>)messageModel {
    BOOL flag = NO;
    if ([messageModel.message.ext objectForKey:MESSAGE_ATTR_IS_BIG_EXPRESSION]) {
        return YES;
    }
    return flag;
}

/*!
 @brief 根据消息获取表情信息
 @discussion
 */
- (EaseEmotion*)emotionURLFormessageViewController:(YYHxMessageViewController *)viewController
                                      messageModel:(id<IMessageModel>)messageModel {
    NSString *emotionId = [messageModel.message.ext objectForKey:MESSAGE_ATTR_EXPRESSION_ID];
    EaseEmotion *emotion = [_emotionDic objectForKey:emotionId];
    if (emotion == nil) {
        emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:emotionId emotionThumbnail:@"" emotionOriginal:@"" emotionOriginalURL:@"" emotionType:EMEmotionGif];
    }
    return emotion;
}

/*!
 @brief 获取表情列表
 @discussion
 */
- (NSArray*)emotionFormessageViewController:(YYHxMessageViewController *)viewController {
    NSMutableArray *emotions = [NSMutableArray array];
    for (NSString *name in [EaseEmoji allEmoji]) {
        EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:name emotionThumbnail:name emotionOriginal:name emotionOriginalURL:@"" emotionType:EMEmotionDefault];
        [emotions addObject:emotion];
    }
    EaseEmotion *temp = [emotions objectAtIndex:0];
    EaseEmotionManager *managerDefault = [[EaseEmotionManager alloc] initWithType:EMEmotionDefault emotionRow:3 emotionCol:7 emotions:emotions tagImage:[UIImage imageNamed:temp.emotionId]];
    
    NSMutableArray *emotionGifs = [NSMutableArray array];
    _emotionDic = [NSMutableDictionary dictionary];
    NSArray *names = @[@"icon_002",@"icon_007",@"icon_010",@"icon_012",@"icon_013",@"icon_018",@"icon_019",@"icon_020",@"icon_021",@"icon_022",@"icon_024",@"icon_027",@"icon_029",@"icon_030",@"icon_035",@"icon_040"];
    int index = 0;
    for (NSString *name in names) {
        index++;
        EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:[NSString stringWithFormat:@"[示例%d]",index] emotionId:[NSString stringWithFormat:@"em%d",(1000 + index)] emotionThumbnail:[NSString stringWithFormat:@"%@_cover",name] emotionOriginal:[NSString stringWithFormat:@"%@",name] emotionOriginalURL:@"" emotionType:EMEmotionGif];
        [emotionGifs addObject:emotion];
        [_emotionDic setObject:emotion forKey:[NSString stringWithFormat:@"em%d",(1000 + index)]];
    }
    /*EaseEmotionManager *managerGif= [[EaseEmotionManager alloc] initWithType:EMEmotionGif emotionRow:2 emotionCol:4 emotions:emotionGifs tagImage:[UIImage imageNamed:@"icon_002_cover"]];
    
    return @[managerDefault,managerGif];*/
    return @[managerDefault];
}

/*!
 @brief 获取发送表情消息的扩展字段
 @discussion
 */
- (NSDictionary*)emotionExtFormessageViewController:(YYHxMessageViewController *)viewController
                                        easeEmotion:(EaseEmotion*)easeEmotion {
    return @{MESSAGE_ATTR_EXPRESSION_ID:easeEmotion.emotionId,MESSAGE_ATTR_IS_BIG_EXPRESSION:@(YES)};
}

/*!
 @brief view标记已读
 @discussion
 */
- (void)messageViewControllerMarkAllMessagesAsRead:(YYHxMessageViewController *)viewController {
    
}

#pragma mark - EaseMessageUnSendOrderCellDelegate
- (void)sendOrderClickEvent {
    if (self.needSendMessage) {
        [self deleteSpecialMessage];
        self.needSendMessage.status = HDMessageStatusSuccessed;
        [self sendMessage:self.needSendMessage isNeedUploadFile:NO completion:^(HDMessage * _Nonnull aMessage, HDError * _Nonnull aError) {
            
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
