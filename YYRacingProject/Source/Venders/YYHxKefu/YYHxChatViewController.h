//
//  YYHxChatViewController.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/12.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHxMessageViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYHxChatViewController : YYHxMessageViewController
<YYHxMessageViewControllerDelegate, YYHxMessageViewControllerDataSource>

@end

NS_ASSUME_NONNULL_END
