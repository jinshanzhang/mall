//
//  SatisfactionViewController.h
//  CustomerSystem-ios
//
//  Created by EaseMob on 15/10/26.
//  Copyright (c) 2015年 easemob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EaseMessageModel.h"
@protocol SatisfactionDelegate <NSObject>

@optional
- (void)commitSatisfactionWithControlArguments:(ControlArguments *)arguments type:(ControlType *)type evaluationTagsArray:(NSMutableArray *)tags;

@required

- (void)backFromSatisfactionViewController;

@end

@interface SatisfactionViewController : YYBaseViewController

@property (nonatomic, strong) id<IMessageModel> messageModel;
@property (nonatomic, weak) id<SatisfactionDelegate> delegate;

@property(nonatomic,copy) void(^EvaluateSuccessBlock)(void);

@end
