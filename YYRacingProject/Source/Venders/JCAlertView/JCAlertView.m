// About me
// GitHub: https://github.com/HJaycee/JCAlertView
// Blog: http://blog.csdn.net/hjaycee
// Email: hjaycee@163.com (Feel free to connect me)

// About you
// Add "Accelerate.frameWork" first in your project otherwise error!

#import "JCAlertView.h"
#import <Accelerate/Accelerate.h>

NSString *const JCAlertViewWillShowNotification = @"JCAlertViewWillShowNotification";

#define JCColor(r, g, b) [UIColor colorWithRed:(r/255.0) green:(g/255.0) blue:(b/255.0) alpha:1.0]
#define JCScreenWidth [UIScreen mainScreen].bounds.size.width
#define JCScreenHeight [UIScreen mainScreen].bounds.size.height
#define JCAlertViewWidth 285
#define JCAlertViewHeight 386
#define JCAlertViewMaxHeight 440
#define JCMargin 10
#define JCButtonHeight 44
#define JCButtonHeightNoIMG 43
#define JCAlertViewTitleLabelHeight 210
#define JCAlertViewTitleColor JCColor(65, 65, 65)
#define JCAlertViewTitleFont [UIFont boldSystemFontOfSize:20]
#define JCAlertViewContentColor JCColor(102, 102, 102)
#define JCAlertViewContentFont [UIFont systemFontOfSize:13]
#define JCAlertViewContentHeight (JCAlertViewHeight - JCAlertViewTitleLabelHeight - JCButtonHeight)
#define JCiOS7OrLater ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)

@class JCViewController;

@protocol JCViewControllerDelegate <NSObject>

@optional
- (void)coverViewTouched;

@end

@interface JCAlertView () <JCViewControllerDelegate,UIScrollViewDelegate>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *IMGURL;


@property (nonatomic, strong) UIColor *leftColor;
@property (nonatomic, strong) UIColor *rightColor;

@property (nonatomic, copy) NSString *WXHeadUrl;

@property (nonatomic, strong) UIImage *showIMG;
@property (nonatomic, strong) NSArray *buttons;
@property (nonatomic, strong) NSArray *clicks;


@property (nonatomic, assign) JCAlertViewType jcAlertViewType;
@property (nonatomic, copy) clickHandleWithIndex clickWithIndex;
@property (nonatomic, weak) JCViewController *vc;
@property (nonatomic, strong) UIImageView *screenShotView;
@property (nonatomic, getter=isCustomAlert) BOOL customAlert;
@property (nonatomic, getter=isDismissWhenTouchBackground) BOOL dismissWhenTouchBackground;
@property (nonatomic, getter=isAlertReady) BOOL alertReady;


- (void)setup;

@end

@interface jCSingleTon : NSObject

@property (nonatomic, strong) UIWindow *backgroundWindow;
@property (nonatomic, weak) UIWindow *oldKeyWindow;
@property (nonatomic, strong) NSMutableArray *alertStack;
@property (nonatomic, strong) JCAlertView *previousAlert;

@end

@implementation jCSingleTon

+ (instancetype)shareSingleTon{
    static jCSingleTon *shareSingleTonInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        shareSingleTonInstance = [jCSingleTon new];
    });
    return shareSingleTonInstance;
}

- (UIWindow *)backgroundWindow{
    if (!_backgroundWindow) {
        _backgroundWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backgroundWindow.windowLevel = UIWindowLevelStatusBar - 1;
    }
    return _backgroundWindow;
}

- (NSMutableArray *)alertStack{
    if (!_alertStack) {
        _alertStack = [NSMutableArray array];
    }
    return _alertStack;
}

@end

@interface JCViewController : UIViewController

@property (nonatomic, strong) UIImageView *screenShotView;
@property (nonatomic, strong) UIButton *coverView;
@property (nonatomic, weak) JCAlertView *alertView;
@property (nonatomic, weak) id <JCViewControllerDelegate> delegate;

@end

@implementation JCViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self addScreenShot];
    [self addCoverView];
    [self addAlertView];
}

- (void)addScreenShot{
    UIWindow *screenWindow = [UIApplication sharedApplication].windows.firstObject;
    UIGraphicsBeginImageContext(screenWindow.frame.size);
    [screenWindow.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *originalImage = nil;
    if (JCiOS7OrLater) {
        originalImage = viewImage;
    } else {
        originalImage = [UIImage imageWithCGImage:CGImageCreateWithImageInRect(viewImage.CGImage, CGRectMake(0, 20, 320, 460))];
    }
    
    CGFloat blurRadius = 4;
    UIColor *tintColor = [UIColor clearColor];
    CGFloat saturationDeltaFactor = 1;
    UIImage *maskImage = nil;
    
    CGRect imageRect = { CGPointZero, originalImage.size };
    UIImage *effectImage = originalImage;
    
//    BOOL hasBlur = blurRadius > __FLT_EPSILON__;
    BOOL hasBlur = YES;

    BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
    if (hasBlur || hasSaturationChange) {
        UIGraphicsBeginImageContextWithOptions(originalImage.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectInContext = UIGraphicsGetCurrentContext();
        CGContextScaleCTM(effectInContext, 1.0, -1.0);
        CGContextTranslateCTM(effectInContext, 0, -originalImage.size.height);
        CGContextDrawImage(effectInContext, imageRect, originalImage.CGImage);
        
        vImage_Buffer effectInBuffer;
        effectInBuffer.data     = CGBitmapContextGetData(effectInContext);
        effectInBuffer.width    = CGBitmapContextGetWidth(effectInContext);
        effectInBuffer.height   = CGBitmapContextGetHeight(effectInContext);
        effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);
        
        UIGraphicsBeginImageContextWithOptions(originalImage.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
        vImage_Buffer effectOutBuffer;
        effectOutBuffer.data     = CGBitmapContextGetData(effectOutContext);
        effectOutBuffer.width    = CGBitmapContextGetWidth(effectOutContext);
        effectOutBuffer.height   = CGBitmapContextGetHeight(effectOutContext);
        effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);
        
        if (hasBlur) {
            CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
            uint32_t radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
            if (radius % 2 != 1) {
                radius += 1;
            }
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
        }
        BOOL effectImageBuffersAreSwapped = NO;
        if (hasSaturationChange) {
            CGFloat s = saturationDeltaFactor;
            CGFloat floatingPointSaturationMatrix[] = {
                0.0722 + 0.9278 * s,  0.0722 - 0.0722 * s,  0.0722 - 0.0722 * s,  0,
                0.7152 - 0.7152 * s,  0.7152 + 0.2848 * s,  0.7152 - 0.7152 * s,  0,
                0.2126 - 0.2126 * s,  0.2126 - 0.2126 * s,  0.2126 + 0.7873 * s,  0,
                0,                    0,                    0,  1,
            };
            const int32_t divisor = 256;
            NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix)/sizeof(floatingPointSaturationMatrix[0]);
            int16_t saturationMatrix[matrixSize];
            for (NSUInteger i = 0; i < matrixSize; ++i) {
                saturationMatrix[i] = (int16_t)roundf(floatingPointSaturationMatrix[i] * divisor);
            }
            if (hasBlur) {
                vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                effectImageBuffersAreSwapped = YES;
            }
            else {
                vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
            }
        }
        if (!effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIGraphicsBeginImageContextWithOptions(originalImage.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -originalImage.size.height);
    
    CGContextDrawImage(outputContext, imageRect, originalImage.CGImage);
    
    if (hasBlur) {
        CGContextSaveGState(outputContext);
        if (maskImage) {
            CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
        }
        CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
        CGContextRestoreGState(outputContext);
    }
    
    if (tintColor) {
        CGContextSaveGState(outputContext);
        CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
        CGContextFillRect(outputContext, imageRect);
        CGContextRestoreGState(outputContext);
    }
    
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.screenShotView = [[UIImageView alloc] initWithImage:outputImage];
    
//    [self.view addSubview:self.screenShotView];
}

- (void)addCoverView{
    self.coverView = [[UIButton alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.coverView.backgroundColor = JCColor(5, 0, 10);
    [self.coverView addTarget:self action:@selector(coverViewClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.coverView];
}

- (void)coverViewClick{
    if ([self.delegate respondsToSelector:@selector(coverViewTouched)]) {
        [self.delegate coverViewTouched];
    }
}

- (void)addAlertView{
    [self.alertView setup];
    [self.view addSubview:self.alertView];
}

- (void)showAlert{
    self.alertView.alertReady = NO;
    
    CGFloat duration = 0.3;
    
    for (UIButton *btn in self.alertView.subviews) {
        btn.userInteractionEnabled = NO;
    }
    
    self.screenShotView.alpha = 0;
    self.coverView.alpha = 0;
    self.alertView.alpha = 0;
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.screenShotView.alpha = 1;
        self.coverView.alpha = 0.65;
        self.alertView.alpha = 1.0;
    } completion:^(BOOL finished) {
        for (UIButton *btn in self.alertView.subviews) {
            btn.userInteractionEnabled = YES;
        }
        self.alertView.alertReady = YES;
    }];
    
    if (JCiOS7OrLater) {
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
        animation.values = @[@(0.8), @(1.05), @(1.1), @(1)];
        animation.keyTimes = @[@(0), @(0.3), @(0.5), @(1.0)];
        animation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        animation.duration = duration;
        [self.alertView.layer addAnimation:animation forKey:@"bouce"];
    } else {
        self.alertView.transform = CGAffineTransformMakeScale(0.8, 0.8);
        [UIView animateWithDuration:duration * 0.3 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            self.alertView.transform = CGAffineTransformMakeScale(1.05, 1.05);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:duration * 0.2 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                self.alertView.transform = CGAffineTransformMakeScale(1.1, 1.1);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:duration * 0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                    self.alertView.transform = CGAffineTransformMakeScale(1, 1);
                } completion:nil];
            }];
        }];
    }
}

- (void)hideAlertWithCompletion:(void(^)(void))completion{
    self.alertView.alertReady = NO;
    
    CGFloat duration = 0.2;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.coverView.alpha = 0;
        self.screenShotView.alpha = 0;
        self.alertView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.screenShotView removeFromSuperview];
        if (completion) {
            completion();
        }
    }];
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.alertView.transform = CGAffineTransformMakeScale(0.4, 0.4);
    } completion:^(BOOL finished) {
        self.alertView.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

@end

@implementation JCAlertView

- (NSArray *)buttons{
    if (!_buttons) {
        _buttons = [NSArray array];
    }
    return _buttons;
}

- (NSArray *)clicks{
    if (!_clicks) {
        _clicks = [NSArray array];
    }
    return _clicks;
}

- (instancetype)initWithCustomView:(UIView *)customView dismissWhenTouchedBackground:(BOOL)dismissWhenTouchBackground{
    if (self = [super initWithFrame:customView.bounds]) {
        [self addSubview:customView];
        self.center = CGPointMake(JCScreenWidth / 2, JCScreenHeight / 2);
        self.customAlert = YES;
        self.dismissWhenTouchBackground = dismissWhenTouchBackground;
    }
    return self;
}

- (void)show{
    [[jCSingleTon shareSingleTon].alertStack addObject:self];
    
    [self showAlert];
}

- (void)dismissWithCompletion:(void(^)(void))completion{
    [self dismissAlertWithCompletion:^{
        if (completion) {
            completion();
        }
    }];
}
#pragma mark 常规弹窗

+(void)showOneButtonsWithTitle:(NSString *)title Message:(NSString *)message ButtonTitle:(NSString *)buttonTitle ButtonType:(JCAlertViewButtonType)buttonType confirmColor:(UIColor *)confirmColor Click:(clickHandle)click1 type:(JCAlertViewType)type
{
    id newClick = click1;
    if (!newClick) {
        newClick = [NSNull null];
    }
    
    JCAlertView *alertView = [JCAlertView new];
    
    [alertView configAlertViewPropertyWithTitle:title Message:message Buttons:@[@{[NSString stringWithFormat:@"%zi", buttonType] :buttonTitle}] Clicks:@[newClick] ClickWithIndex:nil JCAlertViewType:type leftColor:nil rightColor:confirmColor IMGURL:@"" headerIMHG:nil];
}


+ (void)showTwoButtonsWithTitle:(NSString *)title Message:(NSString *)message ButtonType:(JCAlertViewButtonType)buttonType cancelColor:(UIColor *)cancelColor ButtonTitle:(NSString *)buttonTitle Click:(clickHandle)click ButtonType:(JCAlertViewButtonType)buttonType1 confirmColor:(UIColor *)confirmColor ButtonTitle:(NSString *)buttonTitle1 Click:(clickHandle)click1 type:(JCAlertViewType)type{
    id newClick = click;
    if (!newClick) {
        newClick = [NSNull null];
    }
    id newClick1 = click1;
    if (!newClick1) {
        newClick1 = [NSNull null];
    }
    JCAlertView *alertView = [JCAlertView new];
    
    [alertView configAlertViewPropertyWithTitle:title Message:message Buttons:@[@{[NSString stringWithFormat:@"%li", (long)buttonType] : buttonTitle}, @{[NSString stringWithFormat:@"%zi", buttonType1] : buttonTitle1}] Clicks:@[newClick, newClick1] ClickWithIndex:nil JCAlertViewType:type leftColor:cancelColor rightColor:confirmColor IMGURL:@"" headerIMHG:nil];
}


#pragma mark 推荐人头像弹窗
+ (void)showTwoButtonsAndOneIMGWithTitle:(NSString *)title IMG:(NSString *)IMGURL Message:(NSString *)message ButtonType:(JCAlertViewButtonType)buttonType cancelColor:(UIColor *)cancelColor ButtonTitle:(NSString *)buttonTitle Click:(clickHandle)click ButtonType:(JCAlertViewButtonType)buttonType1 confirmColor:(UIColor *)confirmColor ButtonTitle:(NSString *)buttonTitle1 Click:(clickHandle)click1 type:(JCAlertViewType)type{
    
    id newClick = click;
    if (!newClick) {
        newClick = [NSNull null];
    }
    id newClick1 = click1;
    if (!newClick1) {
        newClick1 = [NSNull null];
    }
    JCAlertView *alertView = [JCAlertView new];
    
    [alertView configAlertViewPropertyWithTitle:title Message:message Buttons:@[@{[NSString stringWithFormat:@"%li", (long)buttonType] : buttonTitle}, @{[NSString stringWithFormat:@"%zi", buttonType1] : buttonTitle1}] Clicks:@[newClick, newClick1] ClickWithIndex:nil JCAlertViewType:type leftColor:cancelColor rightColor:confirmColor IMGURL:IMGURL headerIMHG:nil];
}

#pragma mark 带底图的弹出框
+ (void)showTwoButtonsWithMessage:(NSString *)Message headerIMG:(UIImage *)headerIMG ButtonType:(JCAlertViewButtonType)buttonType cancelColor:(UIColor *)cancelColor ButtonTitle:(NSString *)buttonTitle Click:(clickHandle)click ButtonType:(JCAlertViewButtonType)buttonType1 confirmColor:(UIColor *)confirmColor ButtonTitle:(NSString *)buttonTitle1 Click:(clickHandle)click1 type:(JCAlertViewType)type{
    
    if (!kValidString(Message)) {
        return;
    }
    
    id newClick = click;
    if (!newClick) {
        newClick = [NSNull null];
    }
    id newClick1 = click1;
    if (!newClick1) {
        newClick1 = [NSNull null];
    }
    JCAlertView *alertView = [JCAlertView new];
    
    
    NSMutableArray  *titleArray = [NSMutableArray array];
    NSMutableArray  *eventArray = [NSMutableArray array];
    
    NSString *btnKey = [NSString stringWithFormat:@"%li", (long)buttonType];
    if (kValidString(buttonTitle)) {
        [titleArray addObject:[NSDictionary dictionaryWithObject:buttonTitle
                                                          forKey:btnKey]];
    }
    if (kValidString(buttonTitle1)) {
        [titleArray addObject:[NSDictionary dictionaryWithObject:buttonTitle1
                                                          forKey:btnKey]];
    }
    
    for (int i = 0; i < titleArray.count; i ++) {
        if (i == 0) {
            [eventArray addObject:newClick];
        }
        if (i == 1) {
            [eventArray addObject:newClick1];
        }
    }
    
    [alertView configAlertViewPropertyWithTitle:@"" Message:Message Buttons: titleArray  Clicks:eventArray ClickWithIndex:nil JCAlertViewType:type leftColor:cancelColor rightColor:confirmColor IMGURL:@"" headerIMHG:headerIMG];
}

- (void)configAlertViewPropertyWithTitle:(NSString *)title Message:(NSString *)message Buttons:(NSMutableArray *)buttons Clicks:(NSMutableArray *)clicks ClickWithIndex:(clickHandleWithIndex)clickWithIndex JCAlertViewType:(JCAlertViewType)jcAlertViewType leftColor:(UIColor *)leftColor rightColor:(UIColor *)rightColor IMGURL:(NSString *)IMGURL headerIMHG:(UIImage *)headerIMG{
    self.title = title;
    self.message = message;
    self.buttons = buttons;
    self.clicks = clicks;
    self.clickWithIndex = clickWithIndex;
    self.jcAlertViewType = jcAlertViewType;
    self.leftColor = leftColor;
    self.rightColor = rightColor;
    self.IMGURL = IMGURL;
    self.showIMG = headerIMG;
    [[jCSingleTon shareSingleTon].alertStack addObject:self];
    [self showAlert];
}

- (void)showAlert{
    [[NSNotificationCenter defaultCenter] postNotificationName:JCAlertViewWillShowNotification object:self];
    
    NSInteger count = [jCSingleTon shareSingleTon].alertStack.count;
    JCAlertView *previousAlert = nil;
    if (count > 1) {
        NSInteger index = [[jCSingleTon shareSingleTon].alertStack indexOfObject:self];
        previousAlert = [jCSingleTon shareSingleTon].alertStack[index - 1];
    }
    if (previousAlert && previousAlert.vc) {
        if (previousAlert.isAlertReady) {
            [previousAlert.vc hideAlertWithCompletion:^{
                [self showAlertHandle];
            }];
        } else {
            [self showAlertHandle];
        }
    } else {
        [self showAlertHandle];
    }
}

- (void)showAlertHandle{
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    if (keywindow != [jCSingleTon shareSingleTon].backgroundWindow) {
        [jCSingleTon shareSingleTon].oldKeyWindow = [UIApplication sharedApplication].keyWindow;
    }
    
    JCViewController *vc = [[JCViewController alloc] init];
    vc.delegate = self;
    vc.alertView = self;
    self.vc = vc;
    [jCSingleTon shareSingleTon].backgroundWindow.frame = [UIScreen mainScreen].bounds;
    [[jCSingleTon shareSingleTon].backgroundWindow makeKeyAndVisible];
    [jCSingleTon shareSingleTon].backgroundWindow.rootViewController = self.vc;
    [self.vc showAlert];
}

- (void)coverViewTouched{
    if (self.isDismissWhenTouchBackground) {
        [self dismissAlertWithCompletion:nil];
    }
}

- (void)alertBtnClick:(UIButton *)btn{
    [self dismissAlertWithCompletion:^{
        if (self.clicks.count > 0) {
            clickHandle handle = self.clicks[btn.tag];
            if (![handle isEqual:[NSNull null]]) {
                handle();
            }
        } else {
            if (self.clickWithIndex) {
                self.clickWithIndex(btn.tag);
            }
        }
    }];
}

-(void)dismiss
{
    [self dismissAlertWithCompletion:nil];
}

- (void)dismissAlertWithCompletion:(void(^)(void))completion{
    [self.vc hideAlertWithCompletion:^{
        [self stackHandle];
        
        if (completion) {
            completion();
        }
        
        NSInteger count = [jCSingleTon shareSingleTon].alertStack.count;
        if (count > 0) {
            JCAlertView *lastAlert = [jCSingleTon shareSingleTon].alertStack.lastObject;
            [lastAlert showAlert];
        }
    }];
}

- (void)stackHandle{
    [[jCSingleTon shareSingleTon].alertStack removeObject:self];
    
    NSInteger count = [jCSingleTon shareSingleTon].alertStack.count;
    if (count == 0) {
        [self toggleKeyWindow];
    }
}

- (void)toggleKeyWindow{
    [[jCSingleTon shareSingleTon].oldKeyWindow makeKeyAndVisible];
    [jCSingleTon shareSingleTon].backgroundWindow.rootViewController = nil;
    [jCSingleTon shareSingleTon].backgroundWindow.frame = CGRectZero;
}

#pragma mark 绘制UI
- (void)setup{
    
    if (self.subviews.count > 0) {
        return;
    }
    if (self.isCustomAlert) {
        return;
    }
    self.frame = CGRectMake(0, 0, JCAlertViewWidth, JCAlertViewHeight);
    self.center = CGPointMake(kScreenW/2, kScreenH/2);
    //NSInteger count = self.buttons.count;
    /* 提示title*/
    UILabel *titleLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(15)
                                             textColor:XHBlackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    self.backgroundColor = XHWhiteColor;
    self.v_cornerRadius = 4;
    
    [self addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(24);
    }];
    titleLabel.text = self.title;
    /* 提示message*/
    UILabel *contentLabel = [YYCreateTools createLabel:nil
                                      font:JCAlertViewContentFont
                                             textColor:XHBlackColor];
    contentLabel.numberOfLines = 0;
    contentLabel.textAlignment = NSTextAlignmentCenter;
    
    if (self.jcAlertViewType == JCAlertViewTypeDefault) {
        //更新弹窗
        if (self.showIMG) {
            self.backgroundColor = [UIColor clearColor];
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, JCAlertViewWidth, kSizeScale(200))];
            [img setImage:self.showIMG];
            
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, kSizeScale(190), JCAlertViewWidth, JCAlertViewHeight-kSizeScale(190))];
            view.backgroundColor = XHWhiteColor;
            view.v_cornerRadius = 10;
            [self addSubview:view];
            [self addSubview:img];
            
            contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            contentLabel.text = self.message;
            
            UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 200, JCAlertViewWidth, JCAlertViewHeight-200-JCButtonHeight)];
            scroll.delegate =self;
            scroll.backgroundColor = XHWhiteColor;
            [self addSubview:scroll];
            
            contentLabel.frame = CGRectMake(JCMargin+5, 5, JCAlertViewWidth - JCMargin * 2 -10, 80);
            contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            CGSize maximumLabelSize = CGSizeMake(JCAlertViewWidth - JCMargin * 2 -10, 9999);//labelsize的最大值
            CGSize expectSize = [contentLabel sizeThatFits:maximumLabelSize];
            contentLabel.frame = CGRectMake(JCMargin+5, 5, JCAlertViewWidth - JCMargin * 2 -10, expectSize.height+20);
            contentLabel.text = self.message;
            [contentLabel changeSpaceLineSpace:5 wordSpace:0];
            contentLabel.textAlignment = NSTextAlignmentLeft;
            [scroll addSubview:contentLabel];
            
        }else{
            contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            contentLabel.text = self.message;
            CGSize maximumLabelSize = CGSizeMake(JCAlertViewWidth - JCMargin * 2 -10, 9999);//labelsize的最大值
            CGSize expectSize = [contentLabel sizeThatFits:maximumLabelSize];
            contentLabel.frame = CGRectMake(JCMargin+5, 60, JCAlertViewWidth - JCMargin * 2 -10, expectSize.height);
            
            self.frame = CGRectMake(0, 0, JCAlertViewWidth, expectSize.height+125);
            [self addSubview:contentLabel];
        }
        
    }else{
        self.frame = CGRectMake(0, 0, JCAlertViewWidth, 184);

        UIImageView *icon = [YYCreateTools createImageView:@"" viewModel:-1];
        [icon sd_setImageWithURL:[NSURL URLWithString:self.IMGURL]];
        icon.v_cornerRadius = kSizeScale(25);
        [self addSubview:icon];
        
        [icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(45));
            make.top.mas_equalTo(titleLabel.mas_bottom).mas_equalTo(25);
            make.height.width.mas_equalTo(kSizeScale(50));
        }];
        
        [self addSubview:contentLabel];
        
        [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(icon);
            make.left.mas_equalTo(icon.mas_right).mas_offset(kSizeScale(20));
            make.right.mas_equalTo(kSizeScale(-10));
        }];
        contentLabel.text = self.message;
    }
    self.center = CGPointMake(JCScreenWidth / 2, JCScreenHeight / 2);
    
    for (int i = 0; i < self.buttons.count; i ++) {
        CGFloat btnWidth = JCAlertViewWidth / self.buttons.count - JCMargin * 1.5;
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(JCMargin + (JCMargin + btnWidth) * i, self.frame.size.height - JCButtonHeight, btnWidth, JCButtonHeight)];
        NSDictionary *btnDict = self.buttons[i];
        [btn setTitle:[btnDict.allValues firstObject] forState:UIControlStateNormal];
        
        if (i == 0) {
            btn.titleLabel.font = boldFont(15);
            UIView *line = [YYCreateTools createView:XHLightColor];
            [self addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-45);
                make.height.mas_equalTo(1);
            }];
        }else{
            btn.titleLabel.font = self.leftColor == XHRedColor?boldFont(15):normalFont(15);
        }
        [btn setTitleColor:i==0?self.leftColor:self.rightColor forState:UIControlStateNormal];
        [self addSubview:btn];
        btn.tag = i;
        [btn addTarget:self action:@selector(alertBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        if (i == self.buttons.count - 1 && i != 0) {
            UIView *line = [YYCreateTools createView:XHLightColor];
            [self addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self);
                make.top.mas_equalTo(self.mas_bottom).mas_offset(-45);
                make.bottom.mas_equalTo(0);
                make.width.mas_equalTo(1);
            }];
        }
    }
    
    /*
    if (count == 1) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, self.frame.size.height -JCButtonHeight, JCAlertViewWidth,JCButtonHeight)];
        NSDictionary *btnDict = [self.buttons firstObject];
        [btn setTitle:[btnDict.allValues firstObject] forState:UIControlStateNormal];
        btn.titleLabel.font = normalFont(15);

        [btn setTitleColor:self.rightColor forState:UIControlStateNormal];
        [self addSubview:btn];
        btn.tag = 0;
        [btn addTarget:self action:@selector(alertBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    } else if (count == 2) {
        
        UIView *line = [YYCreateTools createView:XHLightColor];
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.bottom.mas_equalTo(-45);
            make.height.mas_equalTo(1);
        }];
        
        UIView *line1 = [YYCreateTools createView:XHLightColor];
        [self addSubview:line1];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.mas_equalTo(self.mas_bottom).mas_offset(-45);
            make.bottom.mas_equalTo(0);
            make.width.mas_equalTo(1);
        }];
        
        for (int i = 0; i < self.buttons.count; i++) {
            CGFloat btnWidth = JCAlertViewWidth / self.buttons.count - JCMargin * 1.5;
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(JCMargin + (JCMargin + btnWidth) * i, self.frame.size.height - JCButtonHeight, btnWidth, JCButtonHeight)];
            NSDictionary *btnDict = self.buttons[i];
            [btn setTitle:[btnDict.allValues firstObject] forState:UIControlStateNormal];
         
            if (i == 0) {
                btn.titleLabel.font = normalFont(15);
            }else{
                btn.titleLabel.font = self.leftColor == XHRedColor?boldFont(15):normalFont(15);
            }
            [btn setTitleColor:i==0?self.leftColor:self.rightColor forState:UIControlStateNormal];
            [self addSubview:btn];
            btn.tag = i;
            [btn addTarget:self action:@selector(alertBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        }
     */
}

//- (void)setButton:(UIButton *)btn BackgroundWithButonType:(JCAlertViewButtonType)buttonType{
//    UIColor *textColor = nil;
//    UIImage *normalImage = nil;
//    UIImage *highImage = nil;
//    switch (buttonType) {
//        case JCAlertViewButtonTypeDefault:
////            normalImage = [UIImage imageNamed:@"JCAlertView.bundle/default_nor"];
////            highImage = [UIImage imageNamed:@"JCAlertView.bundle/default_high"];
//            textColor =XHRedColor;
//            break;
//        case JCAlertViewButtonTypeCancel:
////            normalImage = [UIImage imageNamed:@"JCAlertView.bundle/cancel_nor"];
////            highImage = [UIImage imageNamed:@"JCAlertView.bundle/cancel_high"];
//            textColor =XHBlackLitColor;
//            break;
//        case JCAlertViewButtonTypeWarn:
////            normalImage = [UIImage imageNamed:@"JCAlertView.bundle/warn_nor"];
////            highImage = [UIImage imageNamed:@"JCAlertView.bundle/warn_high"];
//            textColor =XHWhiteColor;
//            break;
//    }
//    [btn setBackgroundImage:[self resizeImage:normalImage] forState:UIControlStateNormal];
//    [btn setBackgroundImage:[self resizeImage:highImage] forState:UIControlStateHighlighted];
//    [btn setTitleColor:textColor forState:UIControlStateNormal];
//}

- (UIImage *)resizeImage:(UIImage *)image{
    return [image stretchableImageWithLeftCapWidth:image.size.width / 2 topCapHeight:image.size.height / 2];
}

@end
// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com

