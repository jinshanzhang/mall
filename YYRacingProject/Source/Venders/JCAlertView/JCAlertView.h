// About me
// GitHub: https://github.com/HJaycee/JCAlertView
// Blog: http://blog.csdn.net/hjaycee
// Email: hjaycee@163.com (Feel free to connect me)

// About you
// Add "Accelerate.frameWork" first in your project otherwise error!

#import <UIKit/UIKit.h>

// maybe useful
UIKIT_EXTERN NSString *const JCAlertViewWillShowNotification;

typedef void(^clickHandle)(void);

typedef void(^clickHandleWithIndex)(NSInteger index);

typedef NS_ENUM(NSInteger, JCAlertViewButtonType) {
    JCAlertViewButtonTypeDefault = 0,
    JCAlertViewButtonTypeCancel,
    JCAlertViewButtonTypeWarn
};
typedef NS_ENUM(NSInteger, JCAlertViewType) {
    JCAlertViewTypeDefault = 0,
    JCAlertViewTypeOneHeadIMG,
    JCAlertViewTypeGuideIMG,
    JCAlertViewTypeNormel,
};

@interface JCAlertView : UIView
// ------------------------Show AlertView with title and message----------------------

+ (void)showOneButtonsWithTitle:(NSString *)title Message:(NSString *)message ButtonTitle:(NSString *)buttonTitle ButtonType:(JCAlertViewButtonType)buttonType confirmColor:(UIColor *)confirmColor  Click:(clickHandle)click1 type:(JCAlertViewType)type;

+ (void)showTwoButtonsWithTitle:(NSString *)title Message:(NSString *)message ButtonType:(JCAlertViewButtonType)buttonType cancelColor:(UIColor *)cancelColor ButtonTitle:(NSString *)buttonTitle Click:(clickHandle)click ButtonType:(JCAlertViewButtonType)buttonType1 confirmColor:(UIColor *)confirmColor ButtonTitle:(NSString *)buttonTitle1 Click:(clickHandle)click1 type:(JCAlertViewType)type;

+ (void)showTwoButtonsAndOneIMGWithTitle:(NSString *)title IMG:(NSString *)IMGURL Message:(NSString *)message ButtonType:(JCAlertViewButtonType)buttonType cancelColor:(UIColor *)cancelColor ButtonTitle:(NSString *)buttonTitle Click:(clickHandle)click ButtonType:(JCAlertViewButtonType)buttonType1 confirmColor:(UIColor *)confirmColor ButtonTitle:(NSString *)buttonTitle1 Click:(clickHandle)click1 type:(JCAlertViewType)type;


+ (void)showTwoButtonsWithMessage:(NSString *)Message headerIMG:(UIImage *)headerIMG ButtonType:(JCAlertViewButtonType)buttonType cancelColor:(UIColor *)cancelColor ButtonTitle:(NSString *)buttonTitle Click:(clickHandle)click ButtonType:(JCAlertViewButtonType)buttonType1 confirmColor:(UIColor *)confirmColor ButtonTitle:(NSString *)buttonTitle1 Click:(clickHandle)click1 type:(JCAlertViewType)type;



// ------------------------Show AlertView with customView-----------------------------
// create a alertView with customView.
// 'dismissWhenTouchBackground' : If you don't want to add a button on customView to call 'dismiss' method manually, set this property to 'YES'.
- (instancetype)initWithCustomView:(UIView *)customView dismissWhenTouchedBackground:(BOOL)dismissWhenTouchBackground;

- (void)show;

// alert will resign keywindow in the completion.
- (void)dismissWithCompletion:(void(^)(void))completion;

@end
// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com
