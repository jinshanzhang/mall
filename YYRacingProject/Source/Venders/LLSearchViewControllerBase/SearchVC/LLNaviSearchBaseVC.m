//
//  LLNaviSearchBaseVC.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/15.
//
//

#import "LLNaviSearchBaseVC.h"
#import "LLSearchNaviBarView.h"
#import "LLSearchBar.h"
#import "NBSSearchShopCategoryView.h"
#import "HistoryAndCategorySearchCategoryViewP.h"
#import "NBSSearchShopCategoryViewCellP.h"
#import "NBSSearchShopHistoryView.h"
#import "HistoryAndCategorySearchHistroyViewP.h"
#import "LLSearchVCConst.h"
#import "UIView+LLRect.h"
@interface LLNaviSearchBaseVC () <UIScrollViewDelegate,HisBtnDelegate>

@property (nonatomic,copy) beginSearchBlock beginSearchBlock;
@property (nonatomic,copy) searchBarDidChangeBlock srdidChangeBlock;

@property (nonatomic,copy) LLSearchResultListView *resultListView;

@property (nonatomic,copy) resultListViewCellDidClickBlock myCellDidClickBlock;


@end

@implementation LLNaviSearchBaseVC
{
    CGFloat KeyboardHeightWhenShow;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.searchTextField resignFirstResponder];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.searchTextField becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    //开始获取数据
    [super viewWillAppear:animated];
    [self.shopHistoryView removeFromSuperview];
    [self.shopHistoryP getSearchCache];
    @LLWeakObj(self);
    [self.shopHistoryP fetchAndHaveSearchCache:^(BOOL isHave) {
        @LLStrongObj(self);
        if (isHave){
            [self setupShopHistory];
        }
        else
            [self myBGScrollView];
        if(self.setUpCustomViewBlock) {
            self.setUpCustomViewBlock();
        }
    }];
    [self.shopHistoryView reloadData];
    [self modifyViewFrame];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    //导航条
    self.navigationItem.hidesBackButton = YES;
    [self setupSearchNaviBar:self.maintitle];

    [self getData];
    //监听键盘时间
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)getData
{
    @LLWeakObj(self);
    //开始获取数据
    [self.shopCategoryP fetchCategotyTypeData:^(NSError *error, id result) {
        @LLStrongObj(self);
        if(!error){
            [self setupShopCategory];
        }
        else
        [self myBGScrollView];
    }];
}

#pragma mark 创建分类界面
- (void)setupShopCategory
{
//    self.searchBar.placeholder = kValidString(self.maintitle)?self.maintitle:@"请输入关键字搜索";
//    self.searchBar.searchField.enablesReturnKeyAutomatically = NO; //这里设置为无文字就灰色不可点
    if (self.type == 1) {
        return;
    }
    self.shopCategoryView = [NBSSearchShopCategoryView searchShopCategoryViewWithPresenter:self.shopCategoryP WithFrame:(CGRect){0,0,ZYHT_ScreenWidth,0}];
    @LLWeakObj(self);
    //分类搜索
    [self.shopCategoryView categoryTagonClick:^(NBSSearchShopCategoryViewCellP *cellP) {
        @LLStrongObj(self);
        [self beignToSearch:NaviBarSearchTypeCategory cellP:cellP tagLabel:nil searchBar:nil modelArr:nil];
    }];

    [self.shopCategoryView setModifyFrameBlock:^(CGRect rect){
        @LLStrongObj(self);
        [self modifyViewFrame];
        [self setupGuideView];
    }];
    //刷新数据
    [self.shopCategoryView reloadData];
    [self.myBGScrollView addSubview:self.shopCategoryView];
}

//创建历史搜索栏
- (void)setupShopHistory
{
    self.shopHistoryView = [NBSSearchShopHistoryView searchShopCategoryViewWithPresenter:self.shopHistoryP WithFrame:(CGRect){0,17,ZYHT_ScreenWidth,0}];
    self.shopHistoryView.delegate = self;
    @LLWeakObj(self);
    //开始搜索历史记录
    [self.shopHistoryView historyTagonClick:^(UILabel *tagLabel) {
        @LLStrongObj(self);
        [self beignToSearch:NaviBarSearchTypeHistory cellP:nil tagLabel:tagLabel searchBar:nil modelArr:nil];
    }];
    //刷新数据
    [self.myBGScrollView addSubview:self.shopHistoryView];
}

#pragma mark 创建引导栏
- (void)setupGuideView
{
//    if (!self.shopGuideView) {
//        self.shopGuideView = [YY_FindGuideView new];
//        _shopGuideView.frame = CGRectMake(0, self.shopCategoryView.y+20, kScreenW, 110);
//        [_myBGScrollView addSubview:self.shopGuideView];
//    }
}

-(void)removeHistory
{
    [self.shopHistoryP clearSearchHistoryWithResult:nil];
    [self changeFrame];
}


//创建导航条
- (void)setupSearchNaviBar:(NSString *)title;
{
    LLSearchNaviBarView *searchNaviBarView = [[LLSearchNaviBarView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kNavigationH)];
//    searchNaviBarView.backgroundColor = [UIColor redColor];
    searchNaviBarView.searbarPlaceHolder = self.maintitle?:@"请输入搜索关键词";
    self.searchTextField = searchNaviBarView.textField;
//    self.searchBar.placeholder = kValidString(self.maintitle)?self.maintitle:@"请输入关键字搜索";

    @LLWeakObj(self);
    [searchNaviBarView showRightOneBtnWithTitle:@"取消" onClick:^(UIButton *btn) {
        @LLStrongObj(self);
        [searchNaviBarView.textField resignFirstResponder];
//        [self.navigationController popViewControllerAnimated:YES];
        [self pushAndPopViewController:NO viewController:nil];
    }];
    //开始搜索导航条输入
    [searchNaviBarView keyBoardSearchBtnOnClick:^(UITextField *searchBar) {
        @LLStrongObj(self);
        //通知外界开始搜索
        [self beignToSearch:NaviBarSearchTypeDefault cellP:nil tagLabel:nil searchBar:searchBar modelArr:nil];
        //保存搜索历史记录
        [self.shopHistoryP saveSearchCache:searchBar.text result:nil];
    }];
        [self.shopHistoryView setClearHistoryBtnOnClick:^{
            @LLStrongObj(self);
            //清空
            [self.shopHistoryP clearSearchHistoryWithResult:nil];
            [self.shopHistoryView removeFromSuperview];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.shopCategoryView.viewOrigin = CGPointMake(0, 0);
            });

        }];
    
    //搜索框即时输入捕捉
    [searchNaviBarView textOfSearchBarDidChangeBlock:^(UITextField *searchBar, NSString *searchText) {
        @LLStrongObj(self);
        LLBLOCK_EXEC(self.srdidChangeBlock,NaviBarSearchTypesearchBarDidChange,searchBar,searchText)
    }];
    
    [self xh_addTitleView:searchNaviBarView];
}



#pragma mark ================ 更新界面 Frame ================
-(void)modifyViewFrame
{
    if (self.shopHistoryView) {
        self.shopCategoryView.y = self.shopHistoryView.bottom+30;
    }
    else
    {
        self.shopCategoryView.y = 30;
    }
//    self.shopGuideView.y = self.shopCategoryView.bottom+30;
}
- (void)changeFrame
{
    [self.shopHistoryView removeFromSuperview];
    self.shopHistoryView = nil;
    [self modifyViewFrame];
}

#pragma mark ================ UISCrollDelegate ================
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.searchTextField resignFirstResponder];
}


//-----------------------------------------------------------------------------------------------------------
#pragma mark ================ 私有方法 ================
//-----------------------------------------------------------------------------------------------------------
- (UIScrollView *)myBGScrollView
{
    if (!_myBGScrollView) {
        _myBGScrollView = ({
            UIScrollView *bgScrollView =  [[UIScrollView alloc] initWithFrame:CGRectMake(0,kNavigationH, ZYHT_ScreenWidth,ZYHT_ScreenHeight-64)];
            bgScrollView.backgroundColor = [UIColor whiteColor];
            bgScrollView.contentSize = CGSizeMake(ZYHT_ScreenWidth, ZYHT_ScreenHeight-60);
            bgScrollView.showsVerticalScrollIndicator = NO;
            bgScrollView.delegate = self;
            [self.view addSubview:bgScrollView];

            bgScrollView;
        });
    }
    return _myBGScrollView;
}


- (void)beignToSearch:(NaviBarSearchType)searchType cellP:(NBSSearchShopCategoryViewCellP *)cellP tagLabel:(UILabel *)tagLabel searchBar:(UITextField *)searchBar modelArr:(NSArray *)arr{

    if (searchType == NaviBarSearchTypeDefault)
    {
        LLBLOCK_EXEC(self.beginSearchBlock,searchType,nil,nil,searchBar,nil);
    }
    else if (searchType == NaviBarSearchTypeCategory)
    {
        LLBLOCK_EXEC(self.beginSearchBlock,searchType,cellP,nil,searchBar,arr);
    }
    else  if (searchType == NaviBarSearchTypeHistory)
    {
        LLBLOCK_EXEC(self.beginSearchBlock,searchType,nil,tagLabel,nil,nil);
    }
}

- (void)dismissVC{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self dismissViewControllerAnimated:NO completion:nil];
}


//创建即时匹配页面
- (LLSearchResultListView *)resultListView
{
    if (!_resultListView) {
        _resultListView = [[LLSearchResultListView alloc] init];
        //列表被点击
        @LLWeakObj(self);
        [_resultListView  resultListViewDidSelectedIndex:^(UITableView *tableView, NSInteger index) {
            @LLStrongObj(self);
            
            [self.shopHistoryP saveSearchCache:self.resultListArray[index] result:nil];
            LLBLOCK_EXEC(self.myCellDidClickBlock,tableView,index);
            //退出搜索控制器
            [self dismissVC];
        }];
        [self.view addSubview:_resultListView];
    }
    return _resultListView;
}

#pragma mark UIKeyboardWillChangeFrameNotification/当键盘的位置大小发生改变时触发
- (void)keyboardWillChangeFrame:(NSNotification *)note
{
    CGRect rect = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    KeyboardHeightWhenShow = rect.size.height;
}

//-----------------------------------------------------------------------------------------------------------
#pragma mark ================ 接口 ================
//-----------------------------------------------------------------------------------------------------------
- (void)beginSearch:(beginSearchBlock)beginSearchBlock {
    _beginSearchBlock = beginSearchBlock;
}

-(void)setSearchBarText:(NSString *)searchBarText{
    
}

-(void)setResultListArray:(NSArray<NSString *> *)resultListArray
{
    _resultListArray = resultListArray;
    self.resultListView.frame = CGRectMake(0, ZYHT_StatusBarAndNavigationBarHeight, self.view.width, ZYHT_ScreenHeight-KeyboardHeightWhenShow-ZYHT_StatusBarAndNavigationBarHeight);
    self.resultListView.resultArray = resultListArray;
}


- (void)searchbarDidChange:(searchBarDidChangeBlock)didChangeBlock;
{
    _srdidChangeBlock = didChangeBlock;
}


/**
 即时匹配结果列表cell点击事件
 */
- (void)resultListViewDidSelectedIndex:(resultListViewCellDidClickBlock)cellDidClickBlock
{
    _myCellDidClickBlock = cellDidClickBlock;
}


-(void)dealloc
{
    YYLog(@"LLNaviSearchBaseVC 页面销毁");
}

@end
