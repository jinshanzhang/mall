//
//  HistoryAndCategorySearchCategoryViewP.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/8/22.
//  Copyright © 2017年 李龙. All rights reserved.
//

#import "HistoryAndCategorySearchCategoryViewP.h"
#import "NBSSearchShopCategoryViewCellP.h"
#import "TagTypeModel.h"
#import "KeyWordModel.h"

#import "HotSearchRequestAPI.h"

@interface HistoryAndCategorySearchCategoryViewP ()

@property (nonatomic,strong) NSMutableArray<NBSSearchShopCategoryViewCellP *> *collectionDataArray;

@property (nonatomic,strong) NSMutableArray *modelDataArray;

@end

@implementation HistoryAndCategorySearchCategoryViewP

- (NSArray<NBSSearchShopCategoryViewCellP *> *)allCategotyTypeData
{
    return self.collectionDataArray;
}

- (NSArray *)categotyModel
{
    return self.modelDataArray;
}

- (NSArray<NSString *> *)allCategotyTypeTitles
{
    return [self.collectionDataArray valueForKeyPath:@"word"];
}


#pragma mark    模拟网络请求
- (void)fetchCategotyTypeData:(FetchNetDataCompleteHandlerBlock)completeBlock
{
    HotSearchRequestAPI *navAPI = [[HotSearchRequestAPI alloc] initHotSearchRequest:[[NSDictionary dictionary]mutableCopy]];

    [navAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request)
     {
         NSDictionary *responObject = request.responseJSONObject;
         if (kValidDictionary(responObject)) {
             self.modelDataArray = [NSMutableArray array];
             self.modelDataArray = [[NSArray modelArrayWithClass:[KeyWordModel class] json:[responObject objectForKey:@"items"]] mutableCopy];

             [self.modelDataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                 [self.collectionDataArray addObject:[NBSSearchShopCategoryViewCellP presenterWithModel:obj]];
             }];
         }
         completeBlock(nil,@"5");//这里实际返回的是你真实网络请求数据
     } failure:^(__kindof YTKBaseRequest * _Nonnull request)
     {

     }];
}
-(NSMutableArray *)modelDataArray
{
    if (!_modelDataArray) {
        _modelDataArray = [[NSMutableArray alloc] init];
    }
    return _modelDataArray;
}

- (NSMutableArray<NBSSearchShopCategoryViewCellP *> *)collectionDataArray
{
    if (!_collectionDataArray) {
        _collectionDataArray = [[NSMutableArray alloc] init];
    }
    return _collectionDataArray;
}

@end
