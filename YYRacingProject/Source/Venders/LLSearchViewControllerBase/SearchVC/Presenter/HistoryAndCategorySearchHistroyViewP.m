//
//  HistoryAndCategorySearchHistroyViewP.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/8/22.
//  Copyright © 2017年 李龙. All rights reserved.
//

#import "HistoryAndCategorySearchHistroyViewP.h"
#import "LLSearchHistorySaveUtils.h"

@implementation HistoryAndCategorySearchHistroyViewP
- (instancetype)initWithType:(NSInteger)type{
    
    self = [self init];
    if(self) {
        if (type == 1) {
            self.saveUtils = [[LLSearchHistorySaveUtils alloc] initWithSearchHistoriesCacheFileName:@"NearByShopSearchOrderNameHistoryCacheFileName"];
        } else if(type == 99) {
            self.saveUtils = [[LLSearchHistorySaveUtils alloc] initWithSearchHistoriesCacheFileName:@"LocationHistoryCacheFileName"];
        } else {
            self.saveUtils = [[LLSearchHistorySaveUtils alloc] initWithSearchHistoriesCacheFileName:@"NearByShopSearchShopNameHistoryCacheFileName"];
        }
    }
    return self;
    
}


- (instancetype)init{
    if (self = [super init]) {
        
        //赋值
        self.saveUtils = [[LLSearchHistorySaveUtils alloc] initWithSearchHistoriesCacheFileName:@"NearByShopSearchShopNameHistoryCacheFileName"];
    }
    return self;
}



@end
