//
//  NBSSearchShopCategoryViewCellP.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/13.
//
//

#import "NBSSearchShopCategoryViewCellP.h"
#import "KeyWordModel.h"


@interface NBSSearchShopCategoryViewCellP ()
@property (nonatomic,strong) KeyWordModel *myNearShopTypeModel;
@end

@implementation NBSSearchShopCategoryViewCellP


+(instancetype)presenterWithModel:(KeyWordModel *)model
{
    NBSSearchShopCategoryViewCellP *presenter = [NBSSearchShopCategoryViewCellP new];
    presenter.myNearShopTypeModel = model;
    return presenter;
}

//---------------------------------------------------------------------------------------------------
#pragma mark ================================== cell所需参数 ==================================
//---------------------------------------------------------------------------------------------------
-(NSString *)word
{
    return _myNearShopTypeModel.word;
}
-(NSString *)url
{
    return _myNearShopTypeModel.linkUrl;
}
-(KeyWordModel *)showModel
{
    return _myNearShopTypeModel;
}



@end
