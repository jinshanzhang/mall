//
//  NBSSearchShopCategoryViewCellP.h
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/13.
//
//



#import <Foundation/Foundation.h>
@class KeyWordModel;

@interface NBSSearchShopCategoryViewCellP : NSObject

+(instancetype)presenterWithModel:(KeyWordModel *)model;

@property (nonatomic,copy,readonly) NSString *word;
@property (nonatomic,copy,readonly) NSString *url;

@property (nonatomic,strong,readonly) KeyWordModel *showModel;


@end

