//
//  LLSearchBar.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/12.
//
//

#import "LLSearchBar.h"
#import "UIView+LLRect.h"

@implementation LLSearchBar

- (instancetype)initWithFrame:(CGRect)frame leftImage:(UIImage *)leftImage placeholderColor:(UIColor *)placeholderColor {
    self = [super initWithFrame:frame];
    if (self) {
        self.hasCentredPlaceholder = YES;
        self.leftImage = leftImage;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    //通过遍历self.subviews找到searchField
    NSUInteger numViews = [self.subviews count];
    for(int i = 0;i < numViews; i++) {
        if([[self.subviews objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            self.searchField = [self.subviews objectAtIndex:i];
        }
    }
    //如果上述方法找不到searchField,试试下面的方法
    NSArray *arraySub = [self subviews];
    UIView *viewSelf = [arraySub objectAtIndex:0];

    NSArray *arrayView = [viewSelf subviews];
    for(int i = 0;i < arrayView.count; i++) {
        if([[arrayView objectAtIndex:i] isKindOfClass:[UITextField class]]) {
            self.searchField = [arrayView objectAtIndex:i];
        }
        if([[arrayView objectAtIndex:i] isKindOfClass:NSClassFromString(@"UISearchBarBackground")]){
            UIView *view = [arrayView objectAtIndex:i];
            view.hidden = YES;
        }
    }
    if (self.searchField) {
        [self.searchField becomeFirstResponder];
        [_searchField setBorderStyle:UITextBorderStyleNone];
        //placeHolder颜色
        _searchField.frame = CGRectMake(5, 0, self.bounds.size.width-5, self.bounds.size.height);
        _searchField.textColor = _textColor;
        _searchField.enablesReturnKeyAutomatically = NO; //这里设置为无文字就灰色不可点
        
        [_searchField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"请输入关键字搜索" attributes:@{NSFontAttributeName: _placeHolderFont, NSForegroundColorAttributeName: _placeholderColor}]];
        _searchField.text = _infotext;
//        [self changeLocation];
        //字体大小
        _searchField.font = normalFont(13);

        UIImageView *searchIcon = [YYCreateTools createImageView:@"home_search_icon"
                                                       viewModel:-1];
        searchIcon.frame = CGRectMake(0, 0, kSizeScale(15), kSizeScale(15));
        _searchField.leftView = searchIcon;
    }
}

-(void)changeLocation
{
    if (_hasCentredPlaceholder) {
        _searchField.frame = CGRectMake(self.bounds.size.width/5+5, 0, self.bounds.size.width, self.bounds.size.height);
    }
}

#pragma mark - Methods
- (void)setHasCentredPlaceholder:(BOOL)hasCentredPlaceholder
{
    _hasCentredPlaceholder = hasCentredPlaceholder;
}
-(void)setIsHideClearButton:(BOOL)isHideClearButton
{
    _isHideClearButton = isHideClearButton;
    if (_isHideClearButton) {
        for (UIView *view in self.subviews) {
            if ([view isKindOfClass:NSClassFromString(@"UIView")] && view.subviews.count > 1) {
                if ( [[view.subviews objectAtIndex:1] isKindOfClass:[UITextField class]]) {
                    UITextField *textField = (UITextField *)[view.subviews objectAtIndex:1];
                    [textField setClearButtonMode:UITextFieldViewModeNever];    // 不需要出现clearButton
                }
                break;
            }
        }
    }
}


@end
