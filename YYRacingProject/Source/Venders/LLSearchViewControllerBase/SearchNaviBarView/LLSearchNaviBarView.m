//
//  LLSearchNaviBarView.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/12.
//
//

#import "LLSearchNaviBarView.h"
#import "LLSearchBar.h"
#import "LLSearchVCConst.h"
#import "UIView+LLRect.h"
#import "UIColor+LLHex.h"

@interface LLSearchNaviBarView ()<UITextFieldDelegate>

@property (nonatomic,strong) UIView *searchBgView;
@property (nonatomic,strong) UIImageView *searLogoIconV;
@property (nonatomic,strong) UIButton *backBtn;
@property (nonatomic,strong) UIButton *rightOneBtn;

@property (nonatomic, strong) UIView *searchBarBgView;

@property (nonatomic,copy) rightOneBtnOnClick rightBtnBlock;
@property (nonatomic,copy) backBtnOnClick backBtnBlock;
@property (nonatomic,copy) keyBoardSearchBtnOnClickBlock keyBoardSearchBtnBlock;
@property (nonatomic,copy) textofSearchBarDidChangeBlock textDidChangeBlock;

@end

@implementation LLSearchNaviBarView
{
    CGFloat searchBgViewH;
    CGFloat leftRightIconWH;
    CGFloat searchIconWH;
    CGFloat LeadMargin;
    CGFloat TrailMargin;
    CGFloat height;
    CGFloat plusheight;

}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {

        leftRightIconWH =  IS_IPHONE_width?55:44;
        searchIconWH = 15.f;
        LeadMargin = 15.f;
        TrailMargin = 13.f;
        searchBgViewH = 32;
        
        if (@available(iOS 11.0, *)) {
            height = 0;
        }else{
            height = 20;
        }
        plusheight =IS_IPHONE_width?-2:2;
        
//        self.frame = CGRectMake(0, 0, ZYHT_ScreenWidth, kNavigationH);
        //[self addSubview:self.rightOneBtn];
        //[self addSubview:self.backBtn];
        //[self addSubview:[self setupSearView]];
        [self createUI];
    }
    return self;
}


//---------------------------------------------------------------------------------------------------
#pragma mark ================================== 创建UI ==================================
//---------------------------------------------------------------------------------------------------

- (UIButton *)backBtn
{
    if (!_backBtn) {
        //左按钮
        _backBtn = ({
            UIButton *backBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 8, leftRightIconWH, leftRightIconWH)];
            [backBtn setImage:[UIImage imageNamed:@"navi_back_w"] forState:UIControlStateNormal];
            [backBtn addTarget:self action:@selector(backBtnOnCLick:) forControlEvents:UIControlEventTouchUpInside];
            backBtn.hidden = YES;
            backBtn;
        });
    }
    return _backBtn;
}


- (UIButton *)rightOneBtn
{
    if (!_rightOneBtn) {
        _rightOneBtn = ({
            UIButton *rightOneBtn = [[UIButton alloc] initWithFrame:CGRectMake(ZYHT_ScreenWidth-leftRightIconWH-5, height+plusheight, leftRightIconWH, leftRightIconWH)];
            rightOneBtn.titleLabel.font = normalFont(14);
            [rightOneBtn addTarget:self action:@selector(rightOneBtnOnCkick:) forControlEvents:UIControlEventTouchUpInside];
            rightOneBtn.hidden = YES;
            rightOneBtn;
        });
    }
    return _rightOneBtn;
}

- (void)createUI {
    @weakify(self);
//    self.searchBar = [[LLSearchBar alloc] initWithFrame:CGRectZero];
//    _searchBar.backgroundColor = XHSearchGrayColor;
//    _searchBar.delegate = self;
//    _searchBar.v_cornerRadius = 2;
//    _searchBar.placeholderColor = XHBlackLitColor;
//    _searchBar.textFont = ZYHT_F_NormalFontOfSize(13);
//    _searchBar.enablesReturnKeyAutomatically = NO;
//    _searchBar.placeHolderFont = [UIFont systemFontOfSize:13 weight:UIFontWeightThin];
//    _searchBar.isHideClearButton = YES;
//    [_searchBar setShowsCancelButton:NO];
    [self addSubview:self.searchBarBgView];
    [self.searchBarBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.height.mas_equalTo(kSizeScale(32));
        make.right.mas_equalTo(-54);
        make.bottom.mas_equalTo(-((44-kSizeScale(32))/2.0 + (IS_IPHONE_X?4:-2)));
//        make.top.equalTo(self).offset(10);
    }];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.titleLabel.font = normalFont(14);
    [rightBtn setTitleColor:HexRGB(0x222222) forState:UIControlStateNormal];
    [rightBtn setTitle:@"取消" forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightOneBtnOnCkick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightBtn];
    
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerY.equalTo(self.searchBarBgView);
        make.right.mas_equalTo(-10);
        //make.left.equalTo(searchBar.mas_right);
        make.height.mas_equalTo(kSizeScale(32));
    }];
}

- (UIView *)searchBarBgView {
    if (!_searchBarBgView) {
        _searchBarBgView = [[UIView alloc] init];
        _searchBarBgView.backgroundColor = ZYHT_Q_RGBAColor(245, 245, 245, 1);
        UIImageView *searchImageView = [[UIImageView alloc] init];
        searchImageView.image = [UIImage imageNamed:@"home_search_icon"];
        [_searchBarBgView addSubview:searchImageView];
        [searchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_searchBarBgView).offset(6.0);
            make.centerY.equalTo(_searchBarBgView);
            make.size.mas_equalTo(CGSizeMake(15, 15));
        }];
        
        [_searchBarBgView addSubview:self.textField];
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(searchImageView.mas_right).offset(6.0);
            make.right.equalTo(_searchBarBgView).offset(-6.0);
            make.centerY.equalTo(_searchBarBgView);
            make.height.mas_equalTo(32);
        }];
        
        _searchBarBgView.layer.cornerRadius = 4;
        _searchBarBgView.clipsToBounds = YES;
    }
    return _searchBarBgView;
}

- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.delegate = self;
        _textField.font = ZYHT_F_NormalFontOfSize(13);
//        _textField.tintColor = XHBlackLitColor;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//        _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入商品、店铺名称" attributes:@{NSForegroundColorAttributeName : XHBlackLitColor}];
        _textField.returnKeyType = UIReturnKeySearch;
        [_textField addTarget:self action:@selector(textValueChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (void)textValueChanged:(UITextField *)textField {
     LLBLOCK_EXEC(self.textDidChangeBlock,self.textField,textField.text);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self searchClick];
    return YES;
}

- (void)searchClick {
    [self.textField resignFirstResponder];
    LLBLOCK_EXEC(_keyBoardSearchBtnBlock,self.textField);
}

//---------------------------------------------------------------------------------------------------
#pragma mark ================================== LLSearchBarDelegate ==================================
//---------------------------------------------------------------------------------------------------
//- (BOOL)searchBarShouldBeginEditing:(LLSearchBar *)searchBar
//{
//    LLBLOCK_EXEC(_searchBarBeignOnClickBlock);
//    return YES;
//}
//
//- (void)searchBarSearchButtonClicked:(LLSearchBar *)searchBar
//{
//    LLBLOCK_EXEC(_keyBoardSearchBtnBlock,searchBar);
//}
//
//- (void)searchBar:(LLSearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    LLBLOCK_EXEC(self.textDidChangeBlock,searchBar,searchText);
//}
//

//---------------------------------------------------------------------------------------------------
#pragma mark ================================== 私有方法 ==================================
//---------------------------------------------------------------------------------------------------
- (void)rightOneBtnOnCkick:(UIButton *)btn
{
    LLBLOCK_EXEC(_rightBtnBlock,btn);
}
//
//
- (void)modifySearchBarViewFrame
{
//    if (!_backBtn.hidden && !_rightOneBtn.hidden) {
//        self.searchBgView.x = self.backBtn.width;
//        self.searchBgView.width = ZYHT_ScreenWidth-self.backBtn.width-self.rightOneBtn.width;
//    }
//    else if (_backBtn.hidden && !_rightOneBtn.hidden)
//    {
//        self.searchBgView.x = LeadMargin;
//        self.searchBgView.width = ZYHT_ScreenWidth-LeadMargin-self.rightOneBtn.width;
//    }
//    else if (!_backBtn.hidden && _rightOneBtn.hidden)
//    {
//        self.searchBgView.x = self.backBtn.width;
//        self.searchBgView.width = ZYHT_ScreenWidth-TrailMargin-self.backBtn.width;
//    }
//
//    self.searLogoIconV.x = self.searchBgView.width -searchBgViewH*0.5-searchIconWH;
//    self.searchBar.width = self.searchBgView.bounds.size.width-10;
}
//
//- (void)backBtnOnCLick:(UIButton *)btn
//{
//    LLBLOCK_EXEC(_backBtnBlock,btn);
//}
//
//
////---------------------------------------------------------------------------------------------------
//#pragma mark ================================== 接口 ==================================
////---------------------------------------------------------------------------------------------------
//- (void)showRightOneBtnWith:(UIImage *)image onClick:(rightOneBtnOnClick)btnBlock
//{
//    _rightBtnBlock = btnBlock;
//    _rightOneBtn.hidden = NO;
//    [_rightOneBtn setImage:image forState:UIControlStateNormal];
//
//    [self modifySearchBarViewFrame];
//}
//
//-(void)showbackBtnWith:(UIImage *)image onClick:(backBtnOnClick)btnBlock
//{
//    _backBtnBlock = btnBlock;
//    _backBtn.hidden = NO;
//
//    if (image)
//        [_backBtn setImage:image forState:UIControlStateNormal];
//
//    [self modifySearchBarViewFrame];
//}

- (void)showRightOneBtnWithTitle:(NSString *)title onClick:(rightOneBtnOnClick)btnBlock {
    _rightBtnBlock = btnBlock;
    _rightOneBtn.hidden = NO;
    [_rightOneBtn setTitle:title forState:UIControlStateNormal];
    [_rightOneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    [self modifySearchBarViewFrame];
}
//
//-(void)setRightOneBtnIconImage:(UIImage *)rightOneBtnIconImage
//{
//    [_rightOneBtn setImage:rightOneBtnIconImage forState:UIControlStateNormal];
//}
//
//-(void)setBackBtnIconImage:(UIImage *)backBtnIconImage
//{
//    if (backBtnIconImage)
//        [_backBtn setImage:backBtnIconImage forState:UIControlStateNormal];
//}
//
//
//
-(void)setSearbarPlaceHolder:(NSString *)searbarPlaceHolder
{
    _textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:searbarPlaceHolder attributes:@{NSForegroundColorAttributeName : XHBlackLitColor}];
//    self.searchBar.searchField.placeholder = searbarPlaceHolder;
}
//
//-(void)setSearch:(NSString *)search
//{
//    self.searchBar.searchField.text = search;
//
//}
//
//- (void)setNav_center:(BOOL)nav_center {
//    self.searchBar.hasCentredPlaceholder = nav_center;
//}
//
//-(void)setIsHideSearchBarRightIcon:(BOOL)isHideSearchBarRightIcon
//{
//    self.searLogoIconV.hidden = isHideSearchBarRightIcon;
//}
//
//-(void)setSearchBarRightIconImage:(UIImage *)searchBarRightIconImage
//{
//    if (!self.searLogoIconV.hidden) {
//        self.searLogoIconV.image = searchBarRightIconImage;
//    }
//}
//
//-(LLSearchBar *)naviSearchBar{
//    return self.searchBar;
//}

-(void)setSearchNaviBarViewType:(LLSearchNaviBarViewType)searchNaviBarViewType{
    _searchNaviBarViewType = searchNaviBarViewType;
    
    switch (searchNaviBarViewType) {
        case searchNaviBarViewDefault:
            break;
        case searchNaviBarViewWhiteAndGray:
        {
//            self.backgroundColor = [UIColor whiteColor];
//            self.searchBgView.backgroundColor = ZYHT_Q_RGBAColor(245, 245, 245, 1);
//            self.searchBar.backgroundColor = ZYHT_Q_RGBAColor(245, 245, 245, 1);
//            self.searchBar.placeholderColor = [UIColor colorWithHexString:@"686a6f"];
//            [_rightOneBtn setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
//
//            //增加下划线
//            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, self.height-0.5, ZYHT_ScreenWidth, 0.5)];
//            view.backgroundColor = [UIColor colorWithHexString:@"d6d7dc"];
//            [self addSubview:view];
        }
            break;
        default:
            break;
    }
    
}
- (void)keyBoardSearchBtnOnClick:(keyBoardSearchBtnOnClickBlock)searchBtnOnClickBlock {
    _keyBoardSearchBtnBlock = searchBtnOnClickBlock;
}
/**
 搜索框框输入文字之后的即时变化
 */
- (void)textOfSearchBarDidChangeBlock:(textofSearchBarDidChangeBlock)textofSearchBarDidChangeBlock
{
    _textDidChangeBlock = textofSearchBarDidChangeBlock;
}

@end

