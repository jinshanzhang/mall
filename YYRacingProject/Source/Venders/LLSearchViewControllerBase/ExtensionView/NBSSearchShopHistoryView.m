//
//  NBSSearchShopHistoryView.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/14.
//
//

#import "NBSSearchShopHistoryView.h"
#import "NBSSearchShopCategoryViewCellP.h"
#import "HistorySearchHistroyViewP.h"
#import "LLTagViewUtils.h"
#import "LLSearchVCConst.h"
#import "UIView+LLRect.h"
#import "UIColor+LLHex.h"



@interface NBSSearchShopHistoryView ()

@property (nonatomic,strong) UILabel *leftTitleLabel;
@property (nonatomic,strong) UIButton *rightMoreBtn;
@property (nonatomic,strong) UIView *coverView;
@property (nonatomic,strong) HistorySearchHistroyViewP *historyTagViewP;
@property (nonatomic,copy) historyTagonClickBlock myOnCickBlock;

@end

@implementation NBSSearchShopHistoryView{
    CGFloat leadPadding;
    CGFloat trailPadding;
    CGFloat topPadding;
    
    CGFloat leftTitleBtnHeight;
    
    CGFloat shopCategoryViewLTPadding;
    CGFloat shopCategoryCoverViewLTPadding;
    CGFloat shopCategoryCoverViewTopPadding;
    CGFloat shopCategoryCoverViewWidth;
    
    CGFloat categoryTagViewHeight;
    
    CGFloat categoryViewHeight;
    
    LLTagViewUtils *tagViewUtils;
}



//初始化
+ (instancetype)searchShopCategoryViewWithPresenter:(HistorySearchHistroyViewP *)presenter WithFrame:(CGRect)rect
{
    NBSSearchShopHistoryView *categoryView = [[NBSSearchShopHistoryView alloc] initWithFrame:rect];
    categoryView.historyTagViewP = presenter;
    return categoryView;
}


-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
        //初始化设置参数
        [self initialization];
        
        self.clipsToBounds = YES;

        //添加子控件
        _leftTitleLabel = [YYCreateTools createLabel:@"历史搜索" font:normalFont(13) textColor:XHBlackLitColor];
        [self addSubview:self.leftTitleLabel];
        
        [_leftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.top.mas_equalTo(0);
        }];
        [self addSubview:self.rightMoreBtn];
        [self addSubview:self.coverView];
        
    }
    return self;
}

static CGFloat shopCategoryViewNotOpenHeight = 0;//分类模块未展开时的初始高度

- (void)initialization
{
    //初始化计量值
    leadPadding = 15;
    trailPadding = 13;
    topPadding = 0;
    
    leftTitleBtnHeight = 15;
    categoryTagViewHeight = 33;
    
    // cover view of categoryViewLLSearchVCConst
    shopCategoryCoverViewLTPadding = leadPadding;
    shopCategoryViewLTPadding = 20;
    shopCategoryCoverViewTopPadding = 15;
    shopCategoryCoverViewWidth = ZYHT_ScreenWidth - shopCategoryCoverViewLTPadding*2;
        //not open
    shopCategoryViewNotOpenHeight = topPadding+leftTitleBtnHeight + shopCategoryCoverViewTopPadding + categoryTagViewHeight;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.leftTitleLabel.frame = CGRectMake(leadPadding, topPadding, 100, leftTitleBtnHeight);
    self.rightMoreBtn.frame = CGRectMake(self.width-24, 0, 12, 12);
    
    self.coverView.frame = CGRectMake(leadPadding,self.leftTitleLabel.bottom+shopCategoryCoverViewTopPadding, shopCategoryCoverViewWidth, categoryViewHeight);
    self.height = shopCategoryViewNotOpenHeight + categoryViewHeight - categoryTagViewHeight;
}


- (UIView *)coverView
{
    if (!_coverView) {
        UIView *view = [[UIView alloc] initWithFrame:(CGRect){0,0,shopCategoryCoverViewWidth,0}];
        view.clipsToBounds = YES;
        _coverView = view;
    }
    return _coverView;
}

- (UIButton *)rightMoreBtn
{
    if (!_rightMoreBtn) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
        [button setImage:[UIImage imageNamed:@"search_history_clean"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(rightBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        _rightMoreBtn = button;
    }
    return _rightMoreBtn;
}

- (void)rightBtnOnClick{
    //展开更多分类
    [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确定要清空历史记录吗?" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        
    } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确定" Click:^{
        [self.delegate removeHistory];
        
        [self clearAllHistoryData];
        
        LLBLOCK_EXEC(self.modifyFrameBlock,CGRectMake(0, 0, 0, 0));
    } type:JCAlertViewTypeDefault];
}
#pragma mark 历史搜索标签
//复制数据
- (void)setuphistoryViewAndData
{
    tagViewUtils = [LLTagViewUtils new];
    [tagViewUtils setupCommonTagsInView:self.coverView tagTexts:[self.historyTagViewP getSearchCache] margin:10 tagHeight:30 tagState:nil color:XHSearchGrayColor];
    
    categoryViewHeight = self.coverView.height;
    tagViewUtils.commonTagStyle = ZYHTCommonTagStyleARCBorderTag;
    
    [tagViewUtils tagLabelOnClick:^(UITapGestureRecognizer *tapGes, UILabel *tagLabel,NSInteger index) {
        LLBLOCK_EXEC(self->_myOnCickBlock,tagLabel);
    }];
}
//---------------------------------------------------------------------------------------------------
#pragma mark ================================= 私有方法 ==================================
//---------------------------------------------------------------------------------------------------
- (void)clearAllHistoryData
{
    LLBLOCK_EXEC(self.clearHistoryBtnOnClick);
}
//-----------------------------------------------------------------------------------------------------------
#pragma mark ================ 接口 ================
//-----------------------------------------------------------------------------------------------------------
-(void)reloadData
{
    [self setuphistoryViewAndData];
}

-(void)historyTagonClick:(historyTagonClickBlock)clickBlock
{
    _myOnCickBlock = clickBlock;
}


@end
