//
//  NBSSearchShopCategoryView.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/7/12.
//
//

#import "NBSSearchShopCategoryView.h"
#import "HistoryAndCategorySearchCategoryViewP.h"
#import "NBSSearchShopCategoryViewCellP.h"
#import "LLTagViewUtils.h"
#import "UIView+LLRect.h"
#import "LLSearchVCConst.h"
#import "UIColor+LLHex.h"


@interface NBSSearchShopCategoryView ()

@property (nonatomic,strong) UILabel *leftTitleLabel;
@property (nonatomic,strong) UIButton *rightMoreBtn;
@property (nonatomic,strong) UIView *coverView;
@property (nonatomic,strong) HistoryAndCategorySearchCategoryViewP *shopCategoryViewP;
@property (nonatomic,copy) categoryTagonClickBlock myOnCickBlock;

@end

@implementation NBSSearchShopCategoryView{
    CGFloat leadPadding;
    CGFloat trailPadding;
    CGFloat topPadding;
    
    CGFloat leftTitleBtnHeight;
    
    CGFloat shopCategoryViewLTPadding;
    CGFloat shopCategoryCoverViewLTPadding;
    CGFloat shopCategoryCoverViewTopPadding;
    CGFloat shopCategoryCoverViewWidth;
    
    CGFloat categoryTagViewHeight;
    
    CGFloat categoryViewHeight;
    
    BOOL _isShowAllCategory; //是否展开显示全部分类
    
    LLTagViewUtils *tagViewUtils;
}

//初始化
+ (instancetype)searchShopCategoryViewWithPresenter:(HistoryAndCategorySearchCategoryViewP *)presenter WithFrame:(CGRect)rect{
    NBSSearchShopCategoryView *categoryView = [[NBSSearchShopCategoryView alloc] initWithFrame:rect];
    categoryView.shopCategoryViewP = presenter;
    return categoryView;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        //初始化设置参数
        [self initialization];
        
        self.clipsToBounds = YES;
        //添加子控件
        
        self.leftTitleLabel = [YYCreateTools createLabel:@"热门搜索" font:normalFont(13) textColor:XHBlackLitColor];
        [self addSubview:self.leftTitleLabel];
        
        [_leftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.top.mas_equalTo(0);
            make.width.mas_equalTo(150);
        }];
        
        [self addSubview:self.coverView];
        [self addSubview:self.rightMoreBtn];
        [self addSubview:self.coverView];
    }
    return self;
}

static CGFloat shopCategoryViewNotOpenHeight = 0;//分类模块未展开时的初始高度
static CGFloat shopCategoryViewOpenHeight = 0;//展开时的高度

- (void)initialization
{
    //初始化计量值
    leadPadding = 15;
    trailPadding = 13;
    topPadding = 0;
    
    leftTitleBtnHeight = 15;
    
    categoryTagViewHeight = 100;
    // cover view of categoryView
    shopCategoryCoverViewLTPadding = leadPadding;
    shopCategoryViewLTPadding = 20;
    shopCategoryCoverViewTopPadding = 15;
    shopCategoryCoverViewWidth = ZYHT_ScreenWidth - shopCategoryCoverViewLTPadding*2;
    
    //not open
    shopCategoryViewNotOpenHeight = topPadding+leftTitleBtnHeight + shopCategoryCoverViewTopPadding + categoryTagViewHeight;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.leftTitleLabel.frame = CGRectMake(leadPadding, topPadding, 120, leftTitleBtnHeight);
    self.rightMoreBtn.centerY = self.leftTitleLabel.centerY;
    
    //open cayegotyView height
    shopCategoryViewOpenHeight = shopCategoryViewNotOpenHeight + categoryViewHeight - categoryTagViewHeight;
    //分类展示view
    self.coverView.frame = CGRectMake(leadPadding,self.leftTitleLabel.bottom+shopCategoryCoverViewTopPadding, shopCategoryCoverViewWidth, categoryViewHeight);
    self.height = shopCategoryViewNotOpenHeight+categoryViewHeight - categoryTagViewHeight;
   
    //通知高度更新
    LLBLOCK_EXEC(self.modifyFrameBlock,self.frame);
}


- (UIView *)coverView
{
    if (!_coverView) {
        UIView *view = [[UIView alloc] initWithFrame:(CGRect){0,0,shopCategoryCoverViewWidth,0}];
        view.backgroundColor = [UIColor whiteColor];
        view.clipsToBounds = YES;
        _coverView = view;
    }
    return _coverView;
}




- (UIButton *)rightMoreBtn
{
    if (!_rightMoreBtn) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
        [button  setTitle:@"更多" forState:UIControlStateNormal];
        button.titleLabel.font = ZYHT_F_MediumFontOfSize(13);
        button.titleLabel.textAlignment = NSTextAlignmentRight;
        [button setTitleColor:[UIColor colorWithHexString:@"b2b2b2"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(rightMoreBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
        
        _rightMoreBtn = button;
    }
    return _rightMoreBtn;
}

- (void)rightMoreBtnOnClick
{
    //展开更多分类
    [self openShopCategoryView];
}


#pragma mark 绘制tag
//复制数据
- (void)setupCategoryViewAndData
{
    tagViewUtils = [LLTagViewUtils new];
    
    [tagViewUtils setupCommonTagsInView:self.coverView tagTexts:[self.shopCategoryViewP allCategotyTypeTitles] margin:10 tagHeight:30 tagState:self.shopCategoryViewP.categotyModel color:XHSearchGrayColor];

    categoryViewHeight = self.coverView.height;
    tagViewUtils.commonTagStyle = ZYHTCommonTagStyleARCBorderTag;

    @LLWeakObj(self);
    [tagViewUtils tagLabelOnClick:^(UITapGestureRecognizer *tapGes, UILabel *tagLabel,NSInteger index) {
        @LLStrongObj(self);
        if (tagLabel.tag <= self.shopCategoryViewP.allCategotyTypeData.count) {
            NBSSearchShopCategoryViewCellP *cellP = self.shopCategoryViewP.allCategotyTypeData[tagLabel.tag];
            LLBLOCK_EXEC(self.myOnCickBlock,cellP);
        }
    }];
}

//---------------------------------------------------------------------------------------------------
#pragma mark ================================= 私有方法 ==================================
//---------------------------------------------------------------------------------------------------
- (void)openShopCategoryView
{
    _isShowAllCategory = !_isShowAllCategory;
    
    [self layoutSubviews];
}


//-----------------------------------------------------------------------------------------------------------
#pragma mark ================ 接口 ================
//-----------------------------------------------------------------------------------------------------------


-(void)reloadData
{
    [self setupCategoryViewAndData];
}

- (void)categoryTagonClick:(categoryTagonClickBlock)clickBlock {
    _myOnCickBlock = clickBlock;
}


@end

