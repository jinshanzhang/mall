//
//  AddressTableViewCell.m
//  ChooseLocation
//
//  Created by Sekorm on 16/8/26.
//  Copyright © 2016年 HY. All rights reserved.
//

#import "AddressTableViewCell.h"
#import "AddressItem.h"

@interface AddressTableViewCell ()

@end
@implementation AddressTableViewCell

-(void)createUI{
    
    
    self.addressLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHLoginColor];
    [self.contentView addSubview:self.addressLabel];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.mas_equalTo(27);
    }];
    
    self.selectFlag = [YYCreateTools createImageView:@"addressCheck_icon" viewModel:-1];
    [self.contentView addSubview:self.selectFlag];
    
    [self.selectFlag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.mas_equalTo(-12);
    }];
}

- (void)setItem:(AddressItem *)item{
    _item = item;
    _addressLabel.text = item.name;
    _addressLabel.textColor = item.isSelected ? XHLoginColor : XHBlackColor ;
    _addressLabel.font = normalFont(14);
    _selectFlag.hidden = !item.isSelected;
}
@end
