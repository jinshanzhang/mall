//
//  AddressTableViewCell.h
//  ChooseLocation
//
//  Created by Sekorm on 16/8/26.
//  Copyright © 2016年 HY. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@class AddressItem;
@interface AddressTableViewCell : YYBaseTableViewCell
@property (nonatomic,strong) AddressItem * item;

@property (strong, nonatomic)  UILabel *addressLabel;
@property (strong, nonatomic)  UIImageView *selectFlag;
@end
