//
//  ChooseLocationView.h
//  ChooseLocation
//
//  Created by Sekorm on 16/8/22.
//  Copyright © 2016年 HY. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef void (^chooseFinish)(NSString *parameter1 , NSString *parameter2);


@interface ChooseLocationView : UIView

@property (nonatomic, copy) NSString * address;

@property (nonatomic, copy) void(^chooseFinish)(NSString *address, NSMutableArray *idArr);

@property (nonatomic,copy) NSString * areaCode;

@property (nonatomic,strong) NSMutableArray *codeArr;

@property (nonatomic,strong) NSMutableArray *titleArr;

@property (nonatomic, copy) void(^close)(void);
//判断哪个页面用到的选择地址
@property (nonatomic, assign) BOOL signChoose;


@end
