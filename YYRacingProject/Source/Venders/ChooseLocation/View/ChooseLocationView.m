//
//  ChooseLocationView.m
//  ChooseLocation
//
//  Created by Sekorm on 16/8/22.
//  Copyright © 2016年 HY. All rights reserved.
//

#import "ChooseLocationView.h"
#import "AddressView.h"
#import "UIView+Frame.h"
#import "AddressTableViewCell.h"
#import "AddressItem.h"
//#import "CitiesDataTool.h"
#import "FindCommonAddressRequestAPI.h"

#define HYScreenW [UIScreen mainScreen].bounds.size.width

static  CGFloat  const  kHYTopViewHeight = 40; //顶部视图的高度
static  CGFloat  const  kHYTopTabbarHeight = 30; //地址标签栏的高度

@interface ChooseLocationView ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic,weak) AddressView * topTabbar;
@property (nonatomic,weak) UIScrollView * contentView;
@property (nonatomic,weak) UIView * underLine;
@property (nonatomic,strong) NSMutableArray * dataSouce;
@property (nonatomic,strong) NSMutableArray * cityDataSouce;
@property (nonatomic,strong) NSMutableArray * districtDataSouce;
@property (nonatomic,strong) NSMutableArray * streetDataSouce;

@property (nonatomic,strong) NSMutableArray * tableViews;
@property (nonatomic,strong) NSMutableArray * topTabbarItems;

@property (nonatomic,weak) UIButton * selectedBtn;
@end

@implementation ChooseLocationView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
        [self getProvinceData];
        self.backgroundColor = XHWhiteColor;
    }
    return self;
}

#pragma mark - setUp UI

- (void)setUp{

    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, kHYTopViewHeight)];
    [self addSubview:topView];
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"所在地区";
    titleLabel.font = normalFont(14);
    titleLabel.textColor = XHBlackLitColor;
    [titleLabel sizeToFit];
    [topView addSubview:titleLabel];
    titleLabel.centerY = topView.height * 0.5;
    titleLabel.centerX = topView.width * 0.5;
    
    
    UIButton *closebtn = [YYCreateTools createBtnImage:@"goodCoupon_close_icon"];
    [topView addSubview:closebtn];
    [closebtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel);
        make.right.mas_equalTo(-20);
    }];
    
    closebtn.actionBlock = ^(UIButton *sender) {
        if (self.close) {
            self.close();
        }
    };
    
    
    UIView * separateLine = [self separateLine];
    [topView addSubview: separateLine];
    separateLine.top = topView.height - separateLine.height;
    topView.backgroundColor = [UIColor whiteColor];
    AddressView * topTabbar = [[AddressView alloc]initWithFrame:CGRectMake(0, topView.height, self.frame.size.width, kHYTopViewHeight)];
    topTabbar.backgroundColor = XHLoginColor;
    [self addSubview:topTabbar];
    _topTabbar = topTabbar;
    [self addTopBarItem];
    
    UIView * separateLine1 = [self separateLine];
    [topTabbar addSubview: separateLine1];
    separateLine1.top = topTabbar.height - separateLine.height;
//    [_topTabbar layoutIfNeeded];
    topTabbar.backgroundColor = [UIColor whiteColor];

    UIView * underLine = [[UIView alloc] initWithFrame:CGRectZero];
    [topTabbar addSubview:underLine];
    _underLine = underLine;
    underLine.height = 2.0f;
    UIButton * btn = self.topTabbarItems.lastObject;
    [self changeUnderLineFrame:btn];
    underLine.top = separateLine1.top - underLine.height;
    _underLine.backgroundColor = XHLoginColor;
    
    UIScrollView * contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(topTabbar.frame), self.frame.size.width, self.height - kHYTopViewHeight - kHYTopTabbarHeight)];
    contentView.contentSize = CGSizeMake(HYScreenW, 0);
    [self addSubview:contentView];

    _contentView = contentView;
    _contentView.pagingEnabled = YES;
    _contentView.backgroundColor = [UIColor whiteColor];
    [self addTableView];
    _contentView.delegate = self;
}


- (void)addTableView{
    UITableView * tabbleView = [[UITableView alloc]initWithFrame:CGRectMake(self.tableViews.count * HYScreenW, 0, HYScreenW, _contentView.height)];
    [_contentView addSubview:tabbleView];
    [self.tableViews addObject:tabbleView];
    tabbleView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tabbleView.delegate = self;
    tabbleView.dataSource = self;
    tabbleView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0);
    Class common = [AddressTableViewCell class];
    registerClass(tabbleView, common);
    
}

- (void)addTopBarItem{
    UIButton * topBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [topBarItem setTitle:@"请选择" forState:UIControlStateNormal];
    [topBarItem setTitleColor:XHLoginColor forState:UIControlStateNormal];
    [topBarItem setTitleColor:XHLoginColor forState:UIControlStateSelected];
    topBarItem.titleLabel.font = [UIFont systemFontOfSize:kSizeScale(14)];
    topBarItem.centerY = _topTabbar.height * 0.5;
    topBarItem.height =kHYTopViewHeight;
    topBarItem.width =_topTabbar.width/4;
    topBarItem.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;

    [self.topTabbarItems addObject:topBarItem];
    [_topTabbar addSubview:topBarItem];
    [topBarItem addTarget:self action:@selector(topBarItemClick:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - TableViewDatasouce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([self.tableViews indexOfObject:tableView] == 0){
        return self.dataSouce.count;
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        return self.cityDataSouce.count;
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        return self.districtDataSouce.count;
    }else if ([self.tableViews indexOfObject:tableView] == 3){
        return self.streetDataSouce.count;
    }
    return self.dataSouce.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class common = [AddressTableViewCell class];
    AddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    AddressItem * item;
    //省级别
    if([self.tableViews indexOfObject:tableView] == 0){
        item = self.dataSouce[indexPath.row];
    //市级别
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        item = self.cityDataSouce[indexPath.row];
    //县级别
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        item = self.districtDataSouce[indexPath.row];
    //街道级别
    }else if ([self.tableViews indexOfObject:tableView] == 3){
        item = self.streetDataSouce[indexPath.row];
    }
    cell.item = item;
    return cell;
}


#pragma mark - TableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressItem * item;
    if([self.tableViews indexOfObject:tableView] == 0){
        for (AddressItem *item  in self.dataSouce) {
            item.isSelected = NO;
        }
        item = self.dataSouce[indexPath.row];
        [self getNextDataWithID:item.addressId type:2 index:indexPath tableview:tableView];
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        for (AddressItem *item  in self.cityDataSouce) {
            item.isSelected = NO;
        }
        item = self.cityDataSouce[indexPath.row];
        [self getNextDataWithID:item.addressId type:3 index:indexPath tableview:tableView];
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        for (AddressItem *item  in self.districtDataSouce) {
            item.isSelected = NO;
        }
       item = self.districtDataSouce[indexPath.row];
    [self getNextDataWithID:item.addressId type:4 index:indexPath tableview:tableView];
    }else if ([self.tableViews indexOfObject:tableView] == 3){
        for (AddressItem *item  in self.streetDataSouce) {
            item.isSelected = NO;
        }
        item = self.streetDataSouce[indexPath.row];
        [self getNextDataWithID:item.addressId type:5 index:indexPath tableview:tableView];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressItem * item;
    if([self.tableViews indexOfObject:tableView] == 0){
        item = self.dataSouce[indexPath.row];
    }else if ([self.tableViews indexOfObject:tableView] == 1){
        item = self.cityDataSouce[indexPath.row];
    }else if ([self.tableViews indexOfObject:tableView] == 2){
        item = self.districtDataSouce[indexPath.row];
    }
    item.isSelected = NO;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark - private
//点击按钮,滚动到对应位置
- (void)topBarItemClick:(UIButton *)btn{
    NSInteger index = [self.topTabbarItems indexOfObject:btn];
    [UIView animateWithDuration:0.5 animations:^{
        self.contentView.contentOffset = CGPointMake(index * HYScreenW, 0);
        [self changeUnderLineFrame:btn];
    }];
    if (self.titleArr) {
        if (index >0 ) {
            UIButton *btn = self.topTabbarItems[index-1];
            [self getDataByTopWithID:(int)btn.tag type:index+1];
        }
    }
}

//调整指示条位置
- (void)changeUnderLineFrame:(UIButton  *)btn{
    _selectedBtn.selected = NO;
    btn.selected = YES;
    _selectedBtn = btn;
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    _underLine.mj_x = kScreenW/4*index;
//    _underLine.centerX = btn.frame.size.width/2;
    _underLine.width = btn.width;
  }

//完成地址选择,执行chooseFinish代码块
- (void)setUpAddress:(NSString *)address addressID:(NSInteger)addressID{
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    UIButton * btn = self.topTabbarItems[index];
    [btn setTitle:address forState:UIControlStateNormal];

    [self changeUnderLineFrame:btn];
    NSMutableString * addressStr = [[NSMutableString alloc] init];
    NSMutableArray *IDArr = [NSMutableArray array];
    for (UIButton * btn  in self.topTabbarItems) {
        [addressStr appendString:btn.currentTitle];
        [addressStr appendString:@" "];
        [IDArr addObject:@(btn.tag)];
    }
    
    self.address = addressStr;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.hidden = YES;
        if (self.chooseFinish) {
            self.chooseFinish(addressStr, IDArr);
        }
    });
}

//当重新选择省或者市的时候，需要将下级视图移除。
- (void)removeLastItem{

    [self.tableViews.lastObject performSelector:@selector(removeFromSuperview) withObject:nil withObject:nil];
    [self.tableViews removeLastObject];
    
    [self.topTabbarItems.lastObject performSelector:@selector(removeFromSuperview) withObject:nil withObject:nil];
    [self.topTabbarItems removeLastObject];
}

//滚动到下级界面,并重新设置顶部按钮条上对应按钮的title
- (void)scrollToNextItem:(NSString *)preTitle addressID:(int)addressID{
    NSInteger index = self.contentView.contentOffset.x / HYScreenW;
    UIButton * btn = self.topTabbarItems[index];
    btn.tag = addressID;
    [btn setTitle:preTitle forState:UIControlStateNormal];

    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.contentSize = (CGSize){self.tableViews.count * HYScreenW,0};
        CGPoint offset = self.contentView.contentOffset;
        self.contentView.contentOffset = CGPointMake(offset.x + HYScreenW, offset.y);
        [self changeUnderLineFrame:btn];

    }];
}

-(void)setStreetTitleItem_addressID:(int)addressID {
    UIButton * btn = self.topTabbarItems.lastObject;
    btn.tag = addressID;
}



#pragma mark - <UIScrollView>
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView != self.contentView) return;
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        NSInteger index = scrollView.contentOffset.x / HYScreenW;
        UIButton * btn = weakSelf.topTabbarItems[index];
        [weakSelf changeUnderLineFrame:btn];
    }];
}

#pragma mark - 开始就有地址时.
-(void)setCodeArr:(NSMutableArray *)codeArr{
    _codeArr = codeArr;
    //2.1 添加市级别,地区级别列表
    for (int i = 0; i<codeArr.count-1; i++) {
        [self addTableView];
        [self addTopBarItem];
    }

}

-(void)setTitleArr:(NSMutableArray *)titleArr{
    _titleArr = titleArr;
    for (int i = 0 ; i < 2 ; i++) {
        UIButton *btn = self.topTabbarItems[i];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        btn.tag = [_codeArr[i] intValue];
    }
}
//初始化选中状态
- (void)setSelectedTitle:(NSString *)title source:(NSMutableArray *)source index:(NSInteger)index{
    for (AddressItem * item in source) {
        if ([item.name isEqualToString:title]) {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:[source indexOfObject:item] inSection:0];
            item.isSelected = YES;
            UITableView * tableView  = self.tableViews[index];
            [tableView reloadData];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
        }
    }
}

#pragma mark - getter 方法

//分割线
- (UIView *)separateLine{
    
    UIView * separateLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1 / [UIScreen mainScreen].scale)];
    separateLine.backgroundColor =  XHLightColor;
    return separateLine;
}

- (NSMutableArray *)tableViews{
    
    if (_tableViews == nil) {
        _tableViews = [NSMutableArray array];
    }
    return _tableViews;
}

- (NSMutableArray *)topTabbarItems{
    if (_topTabbarItems == nil) {
        _topTabbarItems = [NSMutableArray array];
    }
    return _topTabbarItems;
}

//省级别数据源
-(void)getProvinceData{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(1) forKey:@"type"];
    [YYCenterLoading showCenterLoading];
    FindCommonAddressRequestAPI *api = [[FindCommonAddressRequestAPI alloc] initCommonAddressRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.dataSouce = [[NSArray modelArrayWithClass:[AddressItem class] json:[responDict objectForKey:@"commonAddressList"]] mutableCopy];
            UITableView *table = self.tableViews[0];
            [table reloadData];
        }
        if (self.codeArr) {
            [self setSelectedTitle:self->_titleArr[0] source:self.dataSouce index:0];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)getNextDataWithID:(int)addressID type:(NSInteger)type index:(NSIndexPath *)index tableview:(UITableView *)table{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(addressID) forKey:@"id"];
    [dict setObject:@(type) forKey:@"type"];
    [YYCenterLoading showCenterLoading];
    FindCommonAddressRequestAPI *api = [[FindCommonAddressRequestAPI alloc] initCommonAddressRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
           NSMutableArray *arr = [[NSMutableArray modelArrayWithClass:[AddressItem class] json:[responDict objectForKey:@"commonAddressList"]] mutableCopy];
            AddressItem *item;
          //点击省
          if (type == 2) {
            item = self.dataSouce[index.row];
             self.cityDataSouce = arr;
           
             if(self.cityDataSouce.count == 0){
                 for (int i = 0; i < self.tableViews.count && self.tableViews.count != 1; i++) {
                        [self removeLastItem];
                        [self setUpAddress:item.name addressID:0];
                 }
                 //之前未选中省，第一次选择省
                 [self addTopBarItem];
                 [self addTableView];
                 [self scrollToNextItem:item.name addressID:item.addressId];
             }
                NSInteger count = self.tableViews.count;
                for (int i = 0; i < count ; i++) {
                    if (self.tableViews.count == 1) {
                        continue ;
                    }
                    [self removeLastItem];
                }
                [self addTopBarItem];
                [self addTableView];
                [self scrollToNextItem:item.name addressID:item.addressId];
         }
            //点击市
        if (type == 3) {
            item = self.cityDataSouce[index.row];
            self.districtDataSouce = arr;
            if(self.districtDataSouce.count == 0){
                [self setUpAddress:item.name addressID:0];
                return;
            }
            
            if (self.signChoose == YES) {
                [self setStreetTitleItem_addressID:item.addressId];
                [self setUpAddress:item.name addressID:item.addressId];
                return;
            }
            
            NSIndexPath * indexPath0 = [table indexPathForSelectedRow];
           if ([indexPath0 compare:index] == NSOrderedSame && indexPath0){
                for (int i = 0; i < self.tableViews.count && self.tableViews.count != 2 ; i++) {
                   [self removeLastItem];
                }
                [self addTopBarItem];
                [self addTableView];
                [self scrollToNextItem:item.name addressID:item.addressId];
            }else{
                [self addTopBarItem];
                [self addTableView];
                [self scrollToNextItem:item.name addressID:item.addressId];
            }
        }
        if (type == 4) {
            item = self.districtDataSouce[index.row];
            self.streetDataSouce = arr;
            if(self.streetDataSouce.count == 0){
                if (self.tableViews.count == 4) {
                    [self removeLastItem];
                }
                [self setStreetTitleItem_addressID:item.addressId];
                [self setUpAddress:item.name addressID:item.addressId];
                return;
            }
            for (int i = 0; i < self.tableViews.count && self.tableViews.count != 3 ; i++) {
                [self removeLastItem];
            }
            [self addTopBarItem];
            [self addTableView];
            
            [self scrollToNextItem:item.name addressID:item.addressId];
        }
        if (type == 5) {
            item = self.streetDataSouce[index.row];
            [self setStreetTitleItem_addressID:item.addressId];
            [self setUpAddress:item.name addressID:item.addressId];
        }
        item.isSelected = YES;
        [table reloadData];
     }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


-(void)getDataByTopWithID:(int)addressID type:(NSInteger)type{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(addressID) forKey:@"id"];
    [dict setObject:@(type) forKey:@"type"];
    [YYCenterLoading showCenterLoading];
    FindCommonAddressRequestAPI *api = [[FindCommonAddressRequestAPI alloc] initCommonAddressRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            NSMutableArray *arr = [[NSMutableArray modelArrayWithClass:[AddressItem class] json:[responDict objectForKey:@"commonAddressList"]] mutableCopy];
            //点击市
            if (type == 2) {
                self.cityDataSouce = arr;
                [self setSelectedTitle:self->_titleArr[1] source:self.cityDataSouce index:1];
            }
            //点击市
            if (type == 3) {
                self.districtDataSouce = arr;
                [self setSelectedTitle:self->_titleArr[2] source:self.districtDataSouce index:2];
            }
            if (type == 4) {
                self.streetDataSouce = arr;
                [self setSelectedTitle:self->_titleArr[3] source:self.streetDataSouce index:3];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}




@end
