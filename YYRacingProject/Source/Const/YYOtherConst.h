//
//  YYOtherConst.h
//  YIYanProject
//
//  Created by cjm on 2018/4/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#ifndef YYOtherConst_h
#define YYOtherConst_h

#if DEBUG
#pragma mark - DEBUG
//=========================测试版本需要三方平台的Key=========================
//个推
#define  kGeTuiAppID            @"yT3v4ymJAB7rxRWHuP4iB3"
#define  kGeTuiAppKey           @"EJvhA2BEtl6Cq3u0UT9cr1"
#define  kGeTuiAppSecret        @"gy2bWIyw7L6qwpZYrWAsJ8"

//bugly
#define  kBulyAppID             @"7650b67923"
#define  kBulyAppKey            @"16f76f18-391d-4718-a3f8-cebfdbaeeaa2"

//友盟
#define  kYouMengAppKey         @"5f34da63d30932215477d25f"
#else

#pragma mark - Release
//=========================生成版本需要三方平台的Key=========================
//个推
#define  kGeTuiAppID            @"QKfchvZ6Ev6nn0fUmxaaC2"
#define  kGeTuiAppKey           @"Y2TaNGAVkHAkbXEvJs3XD7"
#define  kGeTuiAppSecret        @"HaIhzsyWwU6M6SMCbodF38"
//bugly
#define  kBulyAppID             @"54ba3b4591"
#define  kBulyAppKey            @"b2857b22-1799-491b-924f-323b2b9035c2"
//友盟
#define  kYouMengAppKey         @"5f34da63d30932215477d25f"

#endif

//=========================通用版本需要三方平台的Key=========================
//阿里百川
#define  kTaoBaoAppKey          @"tbopen24820646"
#define  kTaoBaoPid             @"mm_41331782_43656630_399846800"
//微信


#define  kWeChatAppID           @"wxd24d7010881fe4e7"
#define  kWeChatAppKey          @"H6G5n3ioNeCBgN0maQtdhroR8bbRJYQq"
//QQ
#define  kQQAppID               @"1106786854"
#define  kQQAppKey              @"6wFSnay3nsLQZWzw"

//环信
#define  kHYAppKey              @"1430180906068336#kefuchannelapp58189"
#define  kHYIMID                @"kefuchannelimid_129118"  //IM 服务号
#define  kTenantId              @"58189"
//高德地图
#define  KGDMapKey                 @"f2a3daecf1a9399f7d43dacf68506cee"

//小程序名字
#define  kApplet                @"gh_cac6aea0aa52"
#pragma mark - Notification Macro
//=========================通用版本需要三方平台的Key=========================
// 账号登录通知
#define  LoginAndQuitSuccessNotification    @"LoginAndQuitSuccessNotification"
// 详情页离开顶部
#define  GoodDetailLeaveTopNotification     @"GoodDetailLeaveTopNotification"
// 详情页到达顶部
#define  GoodDetailArriveTopNotification    @"GoodDetailArriveTopNotification"
// 是否让课程详情置顶
#define  CourseDetailScrollTopNotification  @"CourseDetailScrollTopNotification"


// 订单列表收益是否展示
#define  OrderListEarnShowNotification      @"OrderListEarnShowNotification"
// 底部资源获取
#define  HandlerTabbarShowNotification      @"HandlerTabbarShowNotification"
// 过期底部提醒操作
#define  OverTimeBottomNotification         @"OverTimeBottomNotification"
// 刷新商品详情状态
#define  RefreshGoodDetailNotification      @"RefreshGoodDetailNotification"
#define  KillTimerNotification              @"KillTimerNotification"

// 刷新订单详情状态
#define  RefreshOrderDetailNotification     @"RefreshOrderDetailNotification"
// 调试通知
#define  DebugNotification                  @"DebugNotification"
// 爆款scrol弹回原点
#define  MainHotScroll                      @"LMainHotScrollNotification"


// 查询订单状态
#define  CheckOrder                         @"CheckOrder"

#define  kShippingRedCountPath            @"root.yiyan"
#define  kShippingDetailRedCountPath      @"root.yiyan.detail"
#define  kShippingSubDetailRedCountPath   @"root.yiyan.detail.sub"

//#define  kYouMengAppKey         @"5f34da63d30932215477d25f"
#pragma mark - WebPage Macro
// 用户协议
#define  UserAgreementURL              @"http://m.sszj888.com/private/index.html"
// 隐私政策协议
#define  PrivacyURL                    @"http://m.sszj888.com/private/private.html"
// 常见问题
#define  QustionWebSite                @"https://m.shellselect.vip/#/activity/106"


#define  OnAprilTwelve                 @"4.12"
#endif /* YYOtherConst_h */
