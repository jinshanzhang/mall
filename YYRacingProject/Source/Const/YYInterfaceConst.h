//
//  YYInterfaceConst.h
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface YYInterfaceConst : NSObject

/**路径**/
extern NSString *INTERFACE_PATH;
extern NSString *INTERFACE_PATH2;
extern NSString *INTERFACE_PATH3;
extern NSString *INTERFACE_PATH4;

#pragma mark - Common
/**上传图片**/
extern NSString *UPLOADPHOTO;
/**资源位**/
extern NSString *COMMONSOURCE;
/**底部tabbar 资源位**/
extern NSString *MENUTABBAR;
/**通用资源位接口**/
extern NSString *COMMONRESOURCE;
/**双十一口令检测**/
extern NSString *PASSWORDCHECK;
/**短信跳转**/
extern NSString *NOTESKIP;
/**提交位置信息**/
extern NSString *LOCATIONRECOMMEND;
/**我的专属二维码**/
extern NSString *SELLERQRCODE;
/**生成商品二维码**/
extern NSString *PRODUCEQRCODE;
#pragma mark - Home
/**首页**/
extern NSString *HOMELIST;
extern NSString *LISTPAGE;
extern NSString *TIMEAXLE;
extern NSString *GETVERSION;
extern NSString *SUPERHOT;
/**分享首页**/
extern NSString *HOMESHARE;
/**获取推送token**/
extern NSString *PUSHREGISTER;
/**分类一级类目**/
extern NSString *TOPCATEGORY;
/**分类详情**/
extern NSString *CATEGORYDETAIL;
/**分类类目组合**/
extern NSString *CATEGORYCOMPOSE;

/**首页类目**/
extern NSString *HOMECATEGORY;
/**首页子列表资源位**/
extern NSString *HOMESUBLISTSOURCE;
/**首页子列表商品**/
extern NSString *HOMESUBLISTGOODS;
/**首页列表商品分页**/
extern NSString *HOMESUBLISTGOODPAGE;
/**首页超级品牌分页**/
extern NSString *HOMESUPERBRANDPAGE;

#pragma mark - Circle
/**蜀黍之家圈话题**/
extern NSString *TOPIC;
/**蜀黍之家圈内容**/
extern NSString *CIRCLECONTENT;
/**蜀黍之家圈点赞和取消赞**/
extern NSString *CIRCLEFAVOR;
/**蜀黍之家圈分享计数**/
extern NSString *CIRCLESHARE;
/**实时生成商品二维码海报图**/
extern NSString *CIRCLEQRCOMMODDITY;
/**实时生成邀约二维码海报图**/
extern NSString *CIRCLEQRUSER;

#pragma mark - Good
/**商品详情**/
extern NSString *GOODDETAIL;
/**商品推荐**/
extern NSString *GOODRECOMMEND;
/**商品素材列表**/
extern NSString *GOODCSDLIST;
/**素材圈反馈**/
extern NSString *GOODCSDFEEDBACK;

#pragma mark - Cart
extern NSString *CART;
/**sku选中状态**/
extern NSString *CARTHANDLE;
/**加减sku数量**/
extern NSString *ACTIONUPDATESKUCNT;
/**预览订单**/
extern NSString *PREORDER;
/**添加删除购物车**/
extern NSString *ACTIONUPDATECART;
/**获取购物车数量**/
extern NSString *CARTTOTALCOUNT;
/**提交订单并支付**/
extern NSString *SUMMITORDER;
/**确认支付结果**/
extern NSString *QUERYPAY;
/**预览支付**/
extern NSString *PREVIEWPAY;
/**支付订单**/
extern NSString  *PAYORDER;

#pragma mark - Order
/**我的订单列表**/
extern NSString  *MINEORDERLIST;
/**我的订单详情**/
extern NSString  *ORDERDETAILS;
/**取消订单**/
extern NSString  *CANCLEORDER;
/**删除订单**/
extern NSString  *DELETEORDER;
/**确认收货**/
extern NSString  *CONFIRMORDER;
/**查看物流**/
extern NSString  *LOOKLOGISTICS;
/**删除订单**/
extern NSString  *STOREORDERDELETE;
/**店铺订单详情**/
extern NSString  *STOREORDERDETAILS;
#pragma mark - Address
/**收货地址列表**/
extern NSString  *LISTADDRESS;
/**查询省市区街道**/
extern NSString  *COMMONADDRESS;
/**新增地址**/
extern NSString  *UPDATEADDRESS;
/**设置收货地址为默认**/
extern NSString  *CHANGEADDRESSFLAG;
/**删除收货地址**/
extern NSString  *DALETEADDRESS;
/**查询收货地址**/
extern NSString  *CHECKADDRESS;

#pragma mark - Login
/**登录背景图片**/
extern NSString *LOGINIMAGE;
/**登录**/
extern NSString  *LOGIN;
/**登出**/
extern NSString  *LOGOUT;
/**获取验证码**/
extern NSString  *LOGINCODE;
/**获取邀请人信息**/
extern NSString  *GETPROVIDER;
/**绑定邀请人**/
extern NSString  *BINDPROVIDER;
/**绑定wx**/
extern NSString  *AuthApp;
/**游客登录开关**/
extern NSString  *LOGINSET;

#pragma mark - Store
/**我的店铺信息**/
extern NSString  *STOREINFO;
/**我的店铺订单**/
extern NSString  *STOREORDERS;
/**修改店铺昵称**/
extern NSString  *STORENICKNAME;
/**修改店铺logo**/
extern NSString  *STORELOGO;
/**修改店铺名片**/
extern NSString  *STORECARD;
/**获取店铺奖励提示**/
extern NSString  *STOREAWARDTIP;
/**店主任务**/
extern NSString  *STORETASK;

#pragma mark - Coupon
/**首页优惠券弹框**/
extern NSString  *COUPONPOPUP;
/**首页领取优惠券**/
extern NSString  *RECEIVECOUPONS;
/**首页优惠券浮窗**/
extern NSString  *COUPONFLOAT;
/**我的优惠券Tab数量**/
extern NSString  *COUPONCOUNT;
/**我的优惠券列表**/
extern NSString  *COUPONLIST;
/**订单优惠券列表**/
extern NSString  *ORDERCOUPONLIST;
/**优惠券分享**/
extern NSString  *COUPONSHARE;

#pragma mark - Courses
/**获取播放授权**/
extern NSString  *COURSEPLAYAUTH;
/**获取播放视频信息**/
extern NSString  *COURSEVIDEOINFO;
/**根据商品ID获取课程内容**/
extern NSString  *COURSELISTCONTENT;
/**获取课程资源播放鉴权**/
extern NSString  *COURSEAUTHENTICATION;
/**获取课程详情**/
extern NSString  *COURSEDETAIL;
/**获取课程内容详情**/
extern NSString  *COURSECONTENTDETAIL;
/**获取我的课程**/
extern NSString  *MINECOURSELIST;
/**获取课程分享URL**/
extern NSString  *COURSEURL;

#pragma mark - Mine
/**个人中心**/
extern NSString  *USERCENTER;
/**更新头像**/
extern NSString  *UPDATEHEADS;
/**更新昵称**/
extern NSString  *UPDATENICK;
/**客服列表**/
extern NSString  *CUSTOMERSERVER;
/**绑定微信**/
extern NSString  *BINDWECHAT;
/**查询手机号**/
extern NSString  *PHONENUMBER;
/**支付密码账户校验验证码**/
extern NSString  *ACCOUNYIDENTIFY;
/**支付密码账户发送验证码**/
extern NSString  *ACCOUNTSENDIDENTIFY;
/**支付密码更新**/
extern NSString  *PAYPASSWPRDUPDATE;
/**余额支付确认密码**/
extern NSString  *CONFIRMPAYPASSWORD;

#pragma mark - Balance
/**提现信息**/
extern NSString  *DRAWINFO;
/**申请提现**/
extern NSString  *APPLYWITHDRAW;
/**余额列表**/
extern NSString  *BALANCELIST;
/**提现列表**/
extern NSString  *DRAWRECORD;
/**提现详情**/
extern NSString  *DRAWDETAIL;
/**银行列表**/
extern NSString  *BANKFIND;
/**认证签约**/
extern NSString  *CERTIFYUSER;
/**签约回显**/
extern NSString  *CERTIFYINFO;

#pragma mark - Search
/**搜索热词**/
extern NSString  *KEYWORD;
/**搜索结果**/
extern NSString  *SEARCHRESULT;
/**结果反馈**/
extern NSString  *FEEDBACK;
/**订单搜索**/
extern NSString  *ORDERRESEARCH;



#pragma mark - AfterSale
/**售后类型**/
extern NSString  *AFRERSALETYPE;
/**提交售后**/
extern NSString  *AFRERSALEINSERT;
/**售后填入信息**/
extern NSString  *AFRERSALECONFIG;

/**申请退款默认金额**/
extern NSString  *SALEAFTERMONEY;
/**查询售后**/
extern NSString  *SALEAFTERCHECK;
/**修改售后**/
extern NSString  *SALEAFTERMODIFY;
/**售后列表**/
extern NSString  *SALEAFTERLIST;
/**售后详情**/
extern NSString  *SALEAFTERDETAIL;
/**取消售后*/
extern NSString  *SALEAFTERCANCEL;
/**申请仲裁*/
extern NSString  *APPLYARBITRATE;
/**上传物流**/
extern NSString  *APPLYLOGISTICS;
/**物流公司列表**/
extern NSString  *LOGISTICCOMPANY;
/**售后物流**/
extern NSString  *EXPRESSDETAIL;
/**售后确认收货**/
extern  NSString *CONFIRMEXPRESS;

#pragma mark ScanVSller
/**扫一扫获取信息**/
extern NSString  *SCANVSELLER;
/**扫一扫绑定合伙人**/
extern NSString  *BINDVSELLER;

@end
