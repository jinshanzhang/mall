//
//  YYEnumTypeConst.h
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#ifndef YYEnumTypeConst_h
#define YYEnumTypeConst_h


// 页面加载类型
typedef NS_ENUM(NSInteger , YYLoadType){
    YYLoadFirstType = 0,
    YYLoadPullUpLoadType,
    YYLoadPullDownLoadType
};


typedef NS_ENUM(NSInteger , YYPushType) {
    YYPushToH5 = 1,
    YYPushToClassify = 2,
    YYPushToGoodDetail = 3,
    YYPushToHomepage = 4,
    YYPushToMyOrder = 5,
    YYPushToShpoOrder = 6,
    YYPushToMyshop = 7,
};

typedef NS_ENUM(NSInteger , YYPageControllerType) {
    YYComposePageType = 0,
    YYOtherPageType     
};

// 商品跳转类型
typedef NS_ENUM(NSInteger , YYShopPushType){
    YYShopDetail = 1,
    YYShopCompose,
    YYShopWeb,
    YYShopTaobao,
    YYShopSearch
};

// 出错页展示类型
typedef NS_ENUM(NSInteger , YYEmptyViewType) {
    YYEmptyViewNoDataType = 0,   //无数据
    YYEmptyViwStrollNoDataType,  //逛逛无数据
    YYEmptyViewNoNetworkType,    //无网络
    YYEmptyViewOverTimeType,     //超时
    YYEmptyViewNetworkErrorType, //网络错误
    YYEmptyViewOrderNoDataType,  //订单无数据
    YYEmptyViewCourseNoDataType,
    YYEmptyViewCouponNoDataType, //优惠券无数据
    YYEmptyViewNoOder,
    YYEmptyViewOtherType,        //其他类型
    YYEmptyViewCarts,            //购物车无数据
    YYEmptyViewAddress,          //地址无数据
    YYEmptyViewUnderCarringType,  //商品下架
    YYEmptyViewNoOrderCoupon,    //订单优惠券
    YYEmptyViewNoDraw,           //无提现
    YYEmptyViewAlxeNoDataType,   //倒计时类目没有数据
    YYEmptyViewNoBalance,        //无余额
    YYEmptyViewGoodDetailCsdNoDataType //商品详情无素材
};

// 价格筛选类型
typedef NS_ENUM(NSInteger , YYPriceFilterType) {
    YYPriceFilterTypeNormal = 0,  //正常
    YYPriceFilterTypeDown,         //下
    YYPriceFilterTypeUp          //上
};

// 弹出动画类型
typedef NS_ENUM(NSInteger , YYPopAnimateType) {
    YYPopAnimateDownUp = 0,     //从下到上
    YYPopAnimateScale           //缩放弹出
};

// 蜀黍之家默认图类型
typedef NS_ENUM(NSInteger , YYPlaceholderImageType) {
    YYPlaceholderImageHomeBannerType = 0,  //首页banner 类型
    YYPlaceholderImageListGoodType,        //列表商品类型
    YYPlaceholderImageHotGoodType,         //今日最热类型
    YYPlaceholderImageGoodDetailType       //商品详情icon
};

// 商品状态类型
typedef NS_ENUM(NSInteger, YYGoodSalingStatusType) {
    YYGoodSalingStatusNoraml = 1,     //正常
    YYGoodSalingStatusPreHot,         //预热
    YYGoodSalingStatusSpecial         //特卖
};


// 订单操作类型
typedef NS_ENUM(NSInteger, YYOrderOperatorType) {
    YYOrderOperatorCancel = 0,
    YYOrderOperatorPay,
    YYOrderOperatorLookLogistic, //查物流
    YYOrderOperatorConfirm,      //确认收货
    YYOrderOperatorDelete,       //删除
    YYOrderOperatorCommit,        //提交
    YYOrderOperatorDetail,        //详情
    YYOrderOperatorRefund,        //退款
};

// 商品售后类型
typedef NS_ENUM(NSInteger, YYGoodAfterSaleOperatorType) {
    YYGoodAfterSaleOperatorApply = 0,       //申请退款
    YYGoodAfterSaleOperatorApplying,        //售后中
    YYGoodAfterSaleOperatorFinish,          //售后失败
    YYGoodAfterSaleOperatorCancel,          //售后取消
    YYGoodAfterSaleOperatorAlreadyCancel,   //售后已取消
    YYGoodAfterSaleOperatorSuccess          //售后成功
};

// 售后操作类型
typedef NS_ENUM(NSInteger, YYAfterSaleOperatorType) {
    YYAfterSaleOperatorModifyApply = 0, //修改申请
    YYAfterSaleOperatorCancel,          //取消售后
    YYAfterSaleOperatorApplyArbitration,//申请仲裁
    YYAfterSaleOperatorReApply,         //重新申请
    YYAfterSaleOperatorUploadLogistic,  //上传物流
    YYAfterSaleOperatorCheckLogistic,   //查看物流
    YYAfterSaleOperatorConfirm,         //确认收货
    
};

// 物流状态类型
typedef NS_ENUM(NSInteger, YYLogisticsDrawType) {
    YYLogisticsDrawTopType = 0,
    YYLogisticsDrawMidType,
    YYLogisticsDrawBottomType
};

// 分享操作类型
// 0 好友   1 朋友圈    2  QQ好友     3 QQ空间     4 保存图片    5 复制链接
typedef NS_ENUM(NSUInteger, YYShareOperatorType) {
    YYShareOperatorWechatSession = 0, //微信好友
    YYShareOperatorWechatTimeLine,    //微信朋友圈
    YYShareOperatorQQ,                //QQ好友
    YYShareOperatorQzone,             //QQ空间
    YYShareOperatorSavePicture,       //保存图片
    YYShareOperatorCopyLink           //复制链接
};


// 分享类型
typedef NS_ENUM(NSInteger , YYSharePlatType) {
    YYSharePlatWeChat = 0,       //微信好友
    YYSharePlatFriend,           //微信朋友圈
    YYSharePlatQQzone,           //QQ空间
    YYSharePlatQQ,               //QQ好友
    YYSharePlatOther
};

// 价格筛选类型
typedef NS_ENUM(NSInteger , YYTagType) {
    YYTagTaobao = 0,  //taobao
    YYTagTianmao = 1,  //tianmao
    YYTagNormal = 2,  //
    YYTagWithPost = 3,  //包邮
    YYTagRecommend = 4,  //推荐
};

typedef NS_ENUM(NSInteger, YYMultiScrollModelType) {
    YYMultiScrollModelSuperType = 0,
    YYMultiScrollModelGoodType,
};


// 一排多个滚动样式
typedef NS_ENUM(NSInteger, YYSingleRowScrollType) {
    YYSingleRowScrollHomeBrand = 0,  //首页品牌
    YYSingleRowScrollSuperHot,       //首页超级爆款
    YYSingleRowScrollGoodRecommend,  //商品详情推荐
    YYSingleRowScrollNewVIP,         //新人特惠
    YYSingleRowScrollEmpty,          //加载更多
};

#endif /* YYEnumTypeConst_h */
