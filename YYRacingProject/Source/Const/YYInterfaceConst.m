//
//  YYInterfaceConst.m
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYInterfaceConst.h"

@implementation YYInterfaceConst

NSString *INTERFACE_PATH = @"/ironman/v1/";
NSString *INTERFACE_PATH2 = @"/ironman/v2/";
NSString *INTERFACE_PATH3 = @"/ironman/v3/";
NSString *INTERFACE_PATH4 = @"/ironman/v4/";

#pragma mark - Common
NSString *UPLOADPHOTO = @"actions/uploadImg";

NSString *COMMONSOURCE = @"commonResource/getResourceNiche";

NSString *GETVERSION = @"getVersion";

NSString *MENUTABBAR = @"commonResource/getMenuBar";

NSString *COMMONRESOURCE = @"commonResource/getResourceNicheList";

NSString *PASSWORDCHECK = @"team/activity/getShareUrl";

NSString *PUSHREGISTER = @"push/register";

NSString *NOTESKIP = @"linkUrl/getInfo";

NSString *LOCATIONRECOMMEND = @"itemRecommend";

NSString *SELLERQRCODE = @"vsellerQrParam";

NSString *PRODUCEQRCODE = @"item/qrshare";
#pragma mark - Home
NSString *HOMESHARE = @"share/homePage";
NSString *SUPERHOT = @"holdTrendy";
NSString *HOMECATEGORY = @"index/classification";
NSString *HOMESUBLISTSOURCE = @"index/niches";
NSString *HOMESUBLISTGOODS = @"index/sales";
NSString *HOMESUBLISTGOODPAGE = @"index/resource";
NSString *HOMESUPERBRANDPAGE = @"index/superbrand";

#pragma mark - List
NSString *HOMELIST = @"index";
NSString *LISTPAGE = @"resource";
NSString *TIMEAXLE = @"tabSearch";

#pragma mark - Category;
NSString *TOPCATEGORY = @"category/topCats";
NSString *CATEGORYDETAIL = @"category/topCatDetail";
NSString *CATEGORYCOMPOSE = @"category/items";

#pragma mark - Circle
NSString *TOPIC  =  @"timeline/topic";
NSString *CIRCLECONTENT  = @"timeline/moment";
NSString *CIRCLEFAVOR  =  @"timeline/actions/favor";
NSString *CIRCLESHARE  =  @"timeline/actions/share";
NSString *CIRCLEQRCOMMODDITY = @"timeline/qr/commodity";
NSString *CIRCLEQRUSER = @"timeline/qr/user";

#pragma mark - Good
NSString *GOODDETAIL = @"item/detail";
NSString *GOODRECOMMEND = @"item/recommend";
NSString *GOODCSDLIST = @"timeline/materialList";
NSString *GOODCSDFEEDBACK = @"timeline/feedback";

#pragma mark - Cart
NSString *CART = @"cart";

NSString *ACTIONUPDATESKUCNT = @"actions/updateCartSku";

NSString *PREORDER = @"previewOrder";

NSString *ACTIONUPDATECART = @"actions/updateCart";

NSString *CARTTOTALCOUNT = @"simpleCart";

NSString *SUMMITORDER = @"actions/summitOrder";

NSString *CARTHANDLE = @"cartHandle";

#pragma mark - Order
NSString  *MINEORDERLIST = @"buyer/myOrders";
NSString  *ORDERDETAILS = @"buyer/orders";
NSString  *QUERYPAY = @"queryPay";
NSString  *PREVIEWPAY = @"previewPay";
NSString  *PAYORDER = @"actions/payOrder";
NSString  *STOREORDERDELETE = @"vshop/deleteOrder";
NSString  *STOREORDERDETAILS = @"vshop/orders";
NSString  *CANCLEORDER = @"actions/cancelOrder";
NSString  *DELETEORDER = @"actions/deleteOrder";
NSString  *CONFIRMORDER = @"actions/confirmShipment";
NSString  *LOOKLOGISTICS = @"actions/checkShipment";

#pragma mark - Address
NSString  *LISTADDRESS = @"list/receiveAddress";

#pragma mark - Store
NSString  *STOREINFO  = @"vshop/info";
NSString  *STOREORDERS = @"vshop/myOrders";
NSString  *COMMONADDRESS = @"find/commonAddress";
NSString  *UPDATEADDRESS = @"update/receiveAddress";
NSString  *CHANGEADDRESSFLAG = @"update/defaultFlag";
NSString  *DALETEADDRESS = @"delete/receiveAddress";
NSString  *CHECKADDRESS = @"find/receiveAddress";
NSString  *STORENICKNAME = @"vshop/name";
NSString  *STORELOGO = @"vshop/logo";
NSString  *STORECARD = @"vshop/card";
NSString  *STOREAWARDTIP = @"vshop/getSalesAwardTip";
NSString  *STORETASK = @"task/taskRecordView";

#pragma mark - Login
NSString  *LOGIN = @"user/login";
NSString  *LOGOUT = @"user/logout";
NSString  *LOGINCODE = @"verificationCode/loginCode";
NSString  *GETPROVIDER = @"user/getPromotionProvider";
NSString  *BINDPROVIDER = @"user/bindPromotionCode";
NSString  *AuthApp = @"wechat/authApp";
NSString  *LOGINSET = @"blackbox";
NSString  *LOGINIMAGE = @"loginImage";



#pragma mark - Coupon
NSString  *COUPONPOPUP = @"buyer/couponPopup";
NSString  *RECEIVECOUPONS = @"buyer/receiveCoupons";
NSString  *COUPONFLOAT = @"buyer/couponFloat";
NSString  *COUPONCOUNT = @"buyer/myCouponCount";
NSString  *COUPONLIST = @"buyer/myCoupons";
NSString  *ORDERCOUPONLIST = @"buyer/couponUse";
NSString  *COUPONSHARE = @"couponShare/applyToShare";

#pragma mark - Courses
/**播放数据获取**/
NSString  *COURSEPLAYAUTH = @"video/getToken";
/**获取播放视频信息**/
NSString  *COURSEVIDEOINFO = @"video/getPlayInfo";
/**根据商品ID获取课程内容**/
NSString  *COURSELISTCONTENT = @"intellectual/getCourseDetailByAppItemId";
/**获取课程资源播放鉴权**/
NSString  *COURSEAUTHENTICATION = @"intellectual/getVideoIdByResourceId";
/**获取课程详情**/
NSString  *COURSEDETAIL = @"intellectual/getCourseDetail";
/**获取课程内容详情**/
NSString  *COURSECONTENTDETAIL = @"intellectual/getLessonDetail";
/**获取我的课程**/
NSString  *MINECOURSELIST = @"intellectual/getCourses";
/**获取课程分享URL**/
NSString  *COURSEURL = @"share/info";
#pragma mark - Mine
NSString  *USERCENTER = @"userCenter";
NSString  *UPDATEHEADS = @"user/updateAvatar";
NSString  *UPDATENICK = @"user/updateNick";
NSString  *CUSTOMERSERVER = @"customerService";

NSString  *BINDWECHAT = @"user/bindWechat";

NSString  *PHONENUMBER = @"find/phoneNumber";
NSString  *ACCOUNYIDENTIFY = @"account/identify";
NSString  *ACCOUNTSENDIDENTIFY = @"account/sendIdentify";
NSString  *PAYPASSWPRDUPDATE = @"accountPassword/update";

NSString  *CONFIRMPAYPASSWORD = @"confirm/accountPassword";


#pragma mark -  Balance

NSString  *DRAWINFO = @"balance/drawInfo";
NSString  *BANKFIND = @"bank/findAll";
NSString  *APPLYWITHDRAW = @"balance/withdraw";
NSString  *BALANCELIST = @"vshop/balance/list";
NSString  *DRAWRECORD = @"balance/drawRecord";
NSString  *DRAWDETAIL  = @"balance/drawDetail";

NSString  *CERTIFYUSER = @"vshop/balance/certifyUser";
NSString  *CERTIFYINFO = @"vshop/balance/getAccountInfo";

#pragma mark -  Search

NSString *KEYWORD = @"search/keywords";
NSString *SEARCHRESULT = @"search/index";
NSString *FEEDBACK = @"search/feedback";

NSString *ORDERRESEARCH = @"vshop/searchMyOrders";

#pragma mark - AfterSale

NSString *AFRERSALETYPE = @"list/afterSaleType";

NSString *AFRERSALEINSERT = @"insert/afterSale";

NSString *AFRERSALECONFIG = @"list/afterSaleConfig";

NSString  *SALEAFTERMONEY = @"get/defaultMoney";
NSString  *SALEAFTERCHECK = @"find/afterSale";
NSString  *SALEAFTERLIST = @"list/afterSale";
NSString  *SALEAFTERDETAIL = @"find/afterSaleDetail";
NSString  *SALEAFTERCANCEL = @"cancel/afterSale";
NSString  *APPLYARBITRATE = @"buyer/arbitrate";
NSString  *APPLYLOGISTICS = @"upload/expressInfo";
NSString  *LOGISTICCOMPANY= @"list/expressCompany";
NSString  *EXPRESSDETAIL= @"find/expressDetail";
NSString  *CONFIRMEXPRESS= @"confirm/supplementExpress";


#pragma mark SCANSELLER

NSString  *SCANVSELLER= @"scanVsellerQr";
NSString  *BINDVSELLER= @"addCityVseller";

@end
