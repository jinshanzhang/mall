//
//  YYMacro.h
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
#ifndef YYMacro_h
#define YYMacro_h

// 线上 1   测试 2   开发3   预发 4
#ifdef DEBUG
//#define NULLSAFE_ENABLED 0        //是否禁用NullSafe分类
#define YYLog(format, ...)          NSLog((@"%s [Line %d] " format), __FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define YYLog(...)
#endif

#ifdef DEBUG

#define SLog(format, ...) printf("class: <%p %s:(%d) > method: %s \n%s\n", self, [[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, __PRETTY_FUNCTION__, [[NSString stringWithFormat:(format), ##__VA_ARGS__] UTF8String] )

#else

#define SLog(format, ...)

#endif

// 判断是否时iPhone X, 鉴于iPhone X/XS/XR/XS Max底部都会有安全距离，所以可以利用safeAreaInsets.bottom > 0.0
static inline BOOL isIPhoneXSeries() {
    BOOL iPhoneXSeries = NO;
    if (UIDevice.currentDevice.userInterfaceIdiom != UIUserInterfaceIdiomPhone) {
        return iPhoneXSeries;
    }
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.bottom > 0.0) {
            iPhoneXSeries = YES;
        }
    }
    return iPhoneXSeries;
}

/**屏幕宽高**/
#define kScreenW             [UIScreen mainScreen].bounds.size.width
#define kScreenH             [UIScreen mainScreen].bounds.size.height

#define kScale               [[NSString stringWithFormat:@"%.2f",kScreenW/375.0] floatValue]
/**屏幕比例**/
#define kSizeScale(x)        kScale * x
#define kFontScale(x)        [[NSString stringWithFormat:@"%.f",kScale * x] floatValue]

/**设备是否是iphone 和 6p**/
#define IS_IPHONE            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5          (IS_IPHONE && kScreenH <= 568.0)
#define IS_IPHONE_6P         (IS_IPHONE && kScreenH == 736.0)
#define IS_IPHONE_width      (IS_IPHONE && kScreenW > 375)

#define IS_IPHONE_X          isIPhoneXSeries()
//(IS_IPHONE && kScreenH == 812.0)

#define kStatuH              (IS_IPHONE_X ? 44.0 : 22.0)
#define kSafeBottom          (IS_IPHONE_X ? 34.0 : 0.0)
#define kTabbarH             (IS_IPHONE_X ? 83.0 : 49.0)
#define kNavigationH         (IS_IPHONE_X ? 88.0 : 64.0)
#define kBottom(x)           (kSafeBottom + x)

//获取app信息
#define kAPPBundle       [[NSBundle mainBundle] infoDictionary]
#define kAPPName         [kAPPBundle objectForKey:@"CFBundleDisplayName"]
#define kAPPVersion      [kAPPBundle objectForKey:@"CFBundleShortVersionString"]
//#define kDeviceUUID      [[UIDevice currentDevice] identifierForVendor].UUIDString
#define kDeviceUUID      [FCUUID uuidForDevice]

#define kSystemName      [[UIDevice currentDevice] systemName]
#define kSystemVersions  [[UIDevice currentDevice] systemVersion]

//获取安全区域
#define EYViewSafeAreaInsets(view) ({UIEdgeInsets i; if(@available(iOS 11.0, *)) {i = view.safeAreaInsets;} else {i = UIEdgeInsetsZero;} i;})

#define  adjustsScrollViewInsets(scrollView)\
do {\
_Pragma("clang diagnostic push")\
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"")\
if ([scrollView respondsToSelector:NSSelectorFromString(@"setContentInsetAdjustmentBehavior:")]) {\
NSMethodSignature *signature = [UIScrollView instanceMethodSignatureForSelector:@selector(setContentInsetAdjustmentBehavior:)];\
NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];\
NSInteger argument = 2;\
invocation.target = scrollView;\
invocation.selector = @selector(setContentInsetAdjustmentBehavior:);\
[invocation setArgument:&argument atIndex:2];\
[invocation retainArguments];\
[invocation invoke];\
}\
_Pragma("clang diagnostic pop")\
} while (0)


/**字体**/
#define CurrentDeviceVersion [[[UIDevice currentDevice] systemVersion] floatValue]

#define normalFont(x)        CurrentDeviceVersion >= 9.0  ? [UIFont fontWithName:@"PingFangSC-Regular" size:kFontScale(x)] : (CurrentDeviceVersion >= 8.2 ? [UIFont systemFontOfSize:kFontScale(x) weight:UIFontWeightMedium] : [UIFont systemFontOfSize:kFontScale(x)])

#define midFont(x)           CurrentDeviceVersion >= 9.0 ? [UIFont systemFontOfSize:kFontScale(x)] : (CurrentDeviceVersion >= 8.2 ? [UIFont systemFontOfSize:kFontScale(x) weight:UIFontWeightMedium] : [UIFont systemFontOfSize:kFontScale(x)])

#define boldFont(x)          CurrentDeviceVersion >= 9.0 ? [UIFont fontWithName:@"PingFangSC-Medium" size:kFontScale(x)] : (CurrentDeviceVersion >= 8.2 ? [UIFont systemFontOfSize:kFontScale(x) weight:UIFontWeightBold] : [UIFont systemFontOfSize:kFontScale(x)])

#define RegularFont(s) [UIFont fontWithName:@"PingFangSC-Regular" size:s]
#define MediumFont(s) [UIFont fontWithName:@"PingFangSC-Medium" size:s]


/**颜色**/
#define HexRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define HexRGBALPHA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define XHLineColor         HexRGB(0xcccccc)
#define XHLightColor        HexRGB(0xf5f5f5)
#define XHMainLightColor    HexRGB(0xf7f7f7)
#define XHStoreGrayColor    HexRGB(0xF3F3F3)
#define XHSearchGrayColor   HexRGB(0xF4F4F4)

#define XHGaldLightColor    HexRGB(0xE2A848)
#define XHGaldMidColor      HexRGB(0xE6B96E)

#define XHBlackColor        HexRGB(0x222222)
#define XHTimeBlackColor    HexRGB(0x333333)
#define XHBlack4Color       HexRGB(0x444444)
#define XHBlackMidColor     HexRGB(0X666666)
#define XHBlackLitColor     HexRGB(0x999999)

#define XHRedColor          HexRGB(0xE60014)
#define XHLightRedColor     HexRGB(0xF195A3)

#define XHLoginColor          HexRGB(0x3785F7)
#define XHLightLoginColor     HexRGB(0xf2dcb6)

#define XHWhiteColor        HexRGB(0xffffff)

#define XHGrayColor         HexRGB(0xffcccccc)
#define XHBlueColor         HexRGB(0xff3870ff)
#define XHBlueDeepColor     HexRGB(0x191946)
#define XHLightBlueColor    HexRGB(0x0092ff)
#define XHGreenColor        HexRGB(0xFF1AAD19)
#define XHOrangeColor       HexRGB(0xFFFF9400)

#define XHMainColor     HexRGB(0x3785F7)

#define XHSeparateLineColor  HexRGB(0xFFE9E8E8)
#define XHNEwLineColor       HexRGB(0xF1F1F1)
#define XHLoginLineColor       HexRGB(0xf2dcb6)


#define XHClearColor         [UIColor clearColor]

#define ThemeColor           HexRGB(0x00c1de)
#define TheBackgroundColor   HexRGB(0x1e222d)

/**常用的宏**/
#define  strFromCls(x)              NSStringFromClass(x)
#define  registerClass(obj,cls)     [obj registerClass:cls forCellReuseIdentifier:strFromCls(cls)]
#define  MOBILENUMBER               @"0123456789"

// 获取view的UIEdgeInsets
#define  VIEWSAFEAREAINSETS(view) ({UIEdgeInsets i; if(@available(iOS 11.0, *)) {i = view.safeAreaInsets;} else {i = UIEdgeInsetsZero;} i;})

// 开始时间
#define FunStart CFAbsoluteTimeGetCurrent()
#define FunEnd   CFAbsoluteTimeGetCurrent() - FunStart

//测试函数运行耗时
#define END NSLog((@"------------Time: %f\n[文件名:%s]\n""[函数名:%s]\n""[行号:%d]\n-----------"), CFAbsoluteTimeGetCurrent(), __FILE__, __FUNCTION__, __LINE__)

/**数据判断**/
#define kValidString(s)             ( s != nil && ![s isEqual:[NSNull null]] &&![s isEqualToString:@""] && [s isKindOfClass:[NSString class]] && s.length > 0 )

// Judge whether it is a vaid dictionary.
#define kValidDictionary(objDict)   (objDict != nil && ([objDict isKindOfClass:[NSDictionary class]]||[objDict isKindOfClass:[NSMutableDictionary class]]) && [[objDict allKeys] count] > 0&& ![objDict isEqual:[NSNull null]])

// Judge whether it is a valid array.
#define kValidArray(objArray)       (objArray != nil && ![objArray isEqual:[NSNull null]]&& ([objArray isKindOfClass:[NSArray class]] || [objArray isKindOfClass:[NSMutableArray class]])&& [objArray count] > 0)

//发送通知
#define kPostNotification(name,obj) [[NSNotificationCenter defaultCenter] postNotificationName:name object:obj]

#define kUserDefault                [NSUserDefaults standardUserDefaults]
//发送请求
#define kRequestUrl(url)            [NSString stringWithFormat:@"%@%@",BSBaseUrl,url]

/**单例**/
#define WMSingletonH(name) + (instancetype)shared##name;
// .m文件
#define WMSingletonM(name) \
static id _instance; \
\
+ (instancetype)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
\
+ (instancetype)shared##name \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instance; \
}
/**window-view**/
#define  kAppWindow     [[[UIApplication sharedApplication] delegate] window]

#endif /* YYMacro_h */
