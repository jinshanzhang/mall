//
//  AppDelegate.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AppDelegate.h"
#import "YYFPSLabel.h"
#import "CYLStoreButtonSubclass.h"
#import "AppDelegate+Monitor.h"
#import "YY_PhoneLoginViewCtrl.h"
#import "YY_WaitOpenStoreViewController.h"
#import "YYTabbarItemInfoModel.h"
#import "YYTabBarControllerConfig.h"
#import "WelcomeViewController.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
//#import <AMapLocationKit/AMapLocationKit.h>

@interface AppDelegate ()<UITabBarControllerDelegate>

//@property (nonatomic, strong) AMapLocationManager *locationManager;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setRootViewController) name:@"SetRootVCNoty" object:nil];
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    config.baseUrl = kEnvConfig.baserUrl;
    
    //微信支付
    [WXApi registerApp:kWeChatAppID universalLink:@"https://m.sszj888.com/app/"];

    // 请求底部资源
//    [kWholeConfig tabbarMenuIconRequest];
    // 双十一口令弹框
//    [kWholeConfig passwordCheckRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(loginAndQuitOperatorHandler:) name:LoginAndQuitSuccessNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(overTimeOperatorHandler:) name:OverTimeBottomNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handlerTabbarSource) name:HandlerTabbarShowNotification object:nil];
    [self monitorApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = XHWhiteColor;
    
    [self.window makeKeyAndVisible];
    
    [NSThread sleepForTimeInterval:1];
    
    [IQKeyboardManager sharedManager].enable = YES;
    
    //之前的登录逻辑
//    kPostNotification(LoginAndQuitSuccessNotification, nil);
    
    application.statusBarStyle = UIStatusBarStyleLightContent;
    NSString *key = @"CFBundleShortVersionString";
    // 取出沙盒中存储的上次使用软件的版本号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults stringForKey:key];
    // 获得当前软件的版本号
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    if ([currentVersion isEqualToString:lastVersion]) {
        YYTabBarControllerConfig *tabBarControllerConfig = [[YYTabBarControllerConfig alloc] init];
        CYLTabBarController *tabBarController = tabBarControllerConfig.tabBarController;
        self.tabbar = tabBarController;
        self.tabbar.delegate = self;
        [self.window setRootViewController:tabBarController];
    } else {
        // 新版本
        [UIApplication sharedApplication].statusBarHidden = YES;
        self.window.rootViewController = [[WelcomeViewController alloc] init];
    }
    
    // iOS8系统以上
    if ([UIDevice currentDevice].systemVersion.doubleValue >= 8.0) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    // iOS8系统以下
    else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
    }
    
    //高德地图
    [AMapServices sharedServices].apiKey = KGDMapKey;
    return YES;
}

- (void)setRootViewController {
    YYTabBarControllerConfig *tabBarControllerConfig = [[YYTabBarControllerConfig alloc] init];
    CYLTabBarController *tabBarController = tabBarControllerConfig.tabBarController;
    self.tabbar = tabBarController;
    self.tabbar.delegate = self;
    [self.window setRootViewController:tabBarController];
}

//#pragma mark ******定位代理方法******
//
//- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location{
//
//}
//
//- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location reGeocode:(AMapLocationReGeocode *)reGeocode {
//    //获取定位位置
////    myCoordinate.latitude = location.coordinate.latitude;
////    myCoordinate.longitude = location.coordinate.longitude;
//    NSLog(@"latitude = %f , longtitude = %f",location.coordinate.latitude,location.coordinate.longitude);
//    NSLog(@"获取到的地址：%@ %@",reGeocode.province,reGeocode.city);
//}

#pragma mark ******地图代理方法******

//- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
//
//    [userLocation setTitle:@"现在位置"];
//
//    [self.mapView setCenterCoordinate:userLocation.coordinate animated:YES];
//
//}


//- (void)tabBarController:(UITabBarController *)tabBarController didSelectControl:(UIControl *)control {
//    if(tabBarController.selectedIndex == 2 || tabBarController.selectedIndex == 3) {
//        if(!kIsLogin) {
//            [YYCommonTools pushToLogin];
//            return;
//        }
//    }
//}
//
//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
//    if(tabBarController.selectedIndex == 2 || tabBarController.selectedIndex == 3) {
//        if(!kIsLogin) {
//            [YYCommonTools pushToLogin];
//            return NO;
//        }
//    }
//    return YES;
//}
//
//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    if(tabBarController.selectedIndex == 2 || tabBarController.selectedIndex == 3) {
//        if(!kIsLogin) {
//            [YYCommonTools pushToLogin];
//            return;
//        }
//    }
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // 电话进来 或 锁屏 的时候，你的程序挂起。
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // 进入后台
    [kUserDefault setObject:@([[NSDate date] timeIntervalSince1970]) forKey:@"tabbarTime"];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // 进入前台
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    // 重新切换回app
    if (!kIsLogin)
        return;
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    NSNumber *tabbarTime = [kUserDefault objectForKey:@"tabbarTime"];
    currentTime = currentTime - [tabbarTime integerValue];
    NSInteger diff = (NSInteger)currentTime/60;
    if (diff > 5) {
        [kWholeConfig tabbarMenuIconRequest];
        [kWholeConfig commonResourceRequest];
        [kUserDefault setObject:@([[NSDate date] timeIntervalSince1970]) forKey:@"tabbarTime"];
    }
    [kWholeConfig passwordCheckRequest];
    kPostNotification(CheckOrder, nil);
    [YYCommonTools checkVersionUpdate:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    kPostNotification(@"backLogin", nil);
    // 挂起状态复原
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // 程序终止
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if ([url.host isEqualToString:@"pay"]) {
        return [WXApi handleOpenURL:url delegate:(id<WXApiDelegate>)self];
    }
    
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderPaySuccessNoty" object:nil userInfo:nil];
        }];
    }
    return YES;
}

-(void)onResp:(BaseResp*)resp{
    if ([resp isKindOfClass:[PayResp class]]){
//        PayResp*response=(PayResp*)resp;
        kPostNotification(@"wxpost", nil);
//        switch(response.errCode){
//            case WXSuccess:
//                kPostNotification(@"wxpost", nil);
//                //服务器端查询支付通知或查询API返回的结果再提示成功
//                break;
//            case <#expression#>:
//                kPostNotification(@"wxpost", nil);
//            default:
//                break;
//        }
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    if ([url.host isEqualToString:@"pay"]) {
        return [WXApi handleOpenURL:url delegate:(id<WXApiDelegate>)self];
        
    }

    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OrderPaySuccessNoty" object:nil userInfo:nil];
        }];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
    if ([userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb]) {
        NSURL *webUrl = userActivity.webpageURL;
        NSURLComponents *components = [NSURLComponents componentsWithURL:webUrl
                                                 resolvingAgainstBaseURL:YES];
        NSLog(@"components = %@", components);
        if ([components.host isEqualToString:@"q.ishellvip.com"]) {
            NSString *currentClsName = [self.window.rootViewController className];
            if ([currentClsName isEqualToString:@"YYBaseTabBarViewController"]) {
                if (kValidString(components.path)) {
                    NSString *path = components.path;
                    NSMutableDictionary *params = [NSMutableDictionary dictionary];
                    NSArray *componentArrays = [path componentsSeparatedByString:@"/"];
                    if (kValidArray(componentArrays)) {
                        NSUInteger index = 0;
                        // 这里存在一个坑componentsSeparatedByString截取字符串变为数组，第一个对象有可能为@"";
                        if ([[componentArrays firstObject] isEqualToString:@""]) {
                            index = index + 1;
                            [params setObject:[componentArrays objectAtIndex:index] forKey:@"key"];
                        }
                        [kWholeConfig noteLinkUniversalSkip:params];
                    }
                }
            }
        }
        else {
            // 不能识别safrai 打开
            [[UIApplication sharedApplication] openURL:webUrl];
        }
    }
    return YES;
}

#pragma mark - Private method
/**
 * 登录状态变更
 */
- (void)loginAndQuitOperatorHandler:(NSNotification *)notification {
    NSNumber *sourceType = notification.object;
    //self.window.rootViewController = [[UIViewController alloc] init];
    if (kIsLogin) {
        // 请求所以的资源位
        [kWholeConfig commonResourceRequest];
        if ([sourceType integerValue] == 1) {
            YY_WaitOpenStoreViewController *waitCtrl = [[YY_WaitOpenStoreViewController alloc] init];
            waitCtrl.hidesBottomBarWhenPushed = YES;
            [self.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:waitCtrl]];
        }
        else {
            // 注册自定义button
            if (!kIsTest) {
//                [CYLStoreButtonSubclass registerPlusButton];
            }
            // 获取用户数据
            [kWholeConfig gainUserCenterInfo:^(BOOL isSucess) {

            }isLoading:NO];
            
            YYTabBarControllerConfig *tabBarControllerConfig = [[YYTabBarControllerConfig alloc] init];
            CYLTabBarController *tabBarController = tabBarControllerConfig.tabBarController;
            self.tabbar = tabBarController;
            [self.window setRootViewController:tabBarController];

            kPostNotification(HandlerTabbarShowNotification, nil);
        
            // 获取店铺数据
            [kWholeConfig storeDetailInfoRequest:^(BOOL isSuccess) {
            }isLoading:NO];
            
            // 获取购物车数据
            [kWholeConfig gainGoodSkuTotalCountRequest:^(NSInteger total) {
                if (total == 0) {
                    [RJBadgeController clearBadgeForKeyPath:kShippingRedCountPath];
                    [RJBadgeController clearBadgeForKeyPath:kShippingDetailRedCountPath];
                    [RJBadgeController clearBadgeForKeyPath:kShippingSubDetailRedCountPath];
                }
                [RJBadgeController setBadgeForKeyPath:kShippingDetailRedCountPath count:total];
            }];
        }
    }
    else {
//        YY_PhoneLoginViewCtrl *login = [[YY_PhoneLoginViewCtrl alloc] init];
//        [self.window setRootViewController:[[UINavigationController alloc] initWithRootViewController:login]];
        [YYCommonTools pushToLogin];
    }
}

/**优惠券过期操作**/
- (void)overTimeOperatorHandler:(NSNotification *)notification {
    if (kUserInfo.isAllowHomeExpireTip) {

        NSMutableDictionary *params = [YYCommonTools objectTurnToDictionary:NO];
        [params setObject:@(NO) forKey:@"isAllowHomeExpireTip"];
        [params setObject:[[NSString alloc] getCurrentTimestamp] forKey:@"homeImgOvertime"];
        [kUserManager modifyUserInfo:params];
        
        TabarItemRemindView *remindView = [[TabarItemRemindView alloc] initWithFrame:CGRectZero];
        remindView.alpha = 0.0;
        remindView.remindURL = kUserInfo.couponExpireSoonIconImg;
        
        [kAppWindow addSubview:remindView];
        [remindView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kSizeScale(98), kSizeScale(20)));
            make.right.mas_equalTo(-kSizeScale(8));
            make.bottom.mas_equalTo(-kTabbarH);
        }];

        [UIView animateWithDuration:1.3
                         animations:^{
                 remindView.alpha = 1.0;
                         } completion:^(BOOL finished) {
                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                 [UIView animateWithDuration:1.5
                                                  animations:^{
                                                     remindView.alpha = 0.0;
                                                  } completion:^(BOOL finished) {
                                                     [remindView removeFromSuperview];
                                                  }];
                             });
                         }];
    }
}

// 处理tabbar 资源
- (void)handlerTabbarSource {
    // tabbar 背景图
    __block UIImageView *bgImageView = nil;
    // tabbar 背景图是否已赋值
    __block NSInteger   bgAssignTime = 0;
    if (!self.tabbar) {
        return;
    }
    for (int i = 0; i < self.tabbar.tabBar.subviews.count; i ++) {
        UIView *view = (UIView *)[self.tabbar.tabBar.subviews objectAtIndex:i];
        if (view.tag == 499) {
            bgImageView = (UIImageView *)view;
        }
    }
    
    NSMutableArray *array = [kUserManager loadTabbarMenuInfo];
    if (kValidArray(array)) {
        __block YYTabbarItemInfoModel *backModel = nil;
        NSString *tabbarPath = [YYCommonTools documentCustomPath:@"/tabbar"];
        // 这里不用model存储的图片路径， 是因为从iOS8，Xcode6之后, 每次运行后沙盒的路径都会变化，生成新的一个路径。
        [self.tabbar.tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            __block NSInteger currentIndex = idx;
            __block UITabBarItem *item = obj;
            [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYTabbarItemInfoModel *itemModel = (YYTabbarItemInfoModel *)obj;
                if (!backModel && [itemModel.index integerValue] == -1) {
                    backModel = itemModel;
                }
                if (backModel && bgImageView && bgAssignTime == 0) {
                    UIImage *bgImage = [UIImage imageWithContentsOfFile:[tabbarPath stringByAppendingPathComponent:backModel.normalIconPath]];
                    bgImageView.image = bgImage;
                    bgAssignTime ++;
                }
                if ([itemModel.index integerValue] == currentIndex) {
                    UIImage *normalImage = [UIImage imageWithContentsOfFile:[tabbarPath stringByAppendingPathComponent:itemModel.normalIconPath]];
                    normalImage = [normalImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    UIImage *selectImage = [UIImage imageWithContentsOfFile:[tabbarPath stringByAppendingPathComponent:itemModel.selectIconPath]];
                    selectImage = [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                    if (!normalImage || !selectImage) {
                        return ;
                    }
                    item = [item initWithTitle:itemModel.title
                                         image:normalImage
                                 selectedImage:selectImage];
                    [item setTitleTextAttributes: @{NSForegroundColorAttributeName:[NSString colorWithHexString:itemModel.normalColor] ,NSFontAttributeName:normalFont(10)} forState:UIControlStateNormal];
                    
                    [item setTitleTextAttributes: @{NSForegroundColorAttributeName:[NSString colorWithHexString:itemModel.selectColor],NSFontAttributeName:normalFont(10)} forState:UIControlStateSelected];
                    *stop = YES;
                }
            }];
        }];
    }
}

//iOS8+需要使用这个方法
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    // 检查当前用户是否允许通知,如果允许就调用 registerForRemoteNotifications
    if (notificationSettings.types != UIUserNotificationTypeNone) {
        [application registerForRemoteNotifications];
    }
}

// 注册远程通知成功后，会调用这个方法，把 deviceToken 返回给我们
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
     NSString *deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""]
        stringByReplacingOccurrencesOfString:@" " withString:@""];
    [kUserDefault setObject:deviceTokenString forKey:@"deviceToken"];
}

// 注册远程通知失败后，会调用这个方法
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"注册远程通知失败----%@", error.localizedDescription);
}
// 当接收到远程通知
// 前台（会调用）
// 从后台进入到前台（会调用）
// 完全退出再进入APP 不会执行这个方法
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"收到远程通知----%@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"收到远程通知2----%@", userInfo);
    NSDictionary *dict = [userInfo objectForKey:@"aps"];
    NSMutableDictionary *params
    = [NSMutableDictionary dictionary];
    [params setObject:[dict objectForKey:@"linkType"]?:@"" forKey:@"linkType"];
    [params setObject:[dict objectForKey:@"linkUrl"]?:@"" forKey:@"url"];
    [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    // 调用系统回调代码块的作用
    //  > 系统会估量app消耗的电量，并根据传递的 `UIBackgroundFetchResult` 参数记录新数据是否可用
    //  > 调用完成的处理代码时，应用的界面缩略图会更新
    completionHandler(UIBackgroundFetchResultNewData);
}
@end

