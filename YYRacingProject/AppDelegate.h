//
//  AppDelegate.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CYLTabBarController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,WXApiDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) CYLTabBarController *tabbar;

@end

