//
//  AppDelegate+Monitor.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AppDelegate+Monitor.h"
@implementation AppDelegate (Monitor)

- (void)monitorApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (@available(iOS 11.0, *)) {
        [UIScrollView appearance].contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    //友盟
    [UMConfigure initWithAppkey:kYouMengAppKey channel:@"App Store"];
    [MobClick setScenarioType:E_UM_NORMAL];
    [MobClick setCrashReportEnabled:YES];

    //友盟推送
    UMessageRegisterEntity * entity = [[UMessageRegisterEntity alloc] init];
    //type是对推送的几个参数的选择，可以选择一个或者多个。默认是三个全部打开，即：声音，弹窗，角标
    entity.types = UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionSound|UMessageAuthorizationOptionAlert;
    [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions Entity:entity     completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            
        }else{
            
        }
    }];
    //友盟分享
    [UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession
                                          appKey:kWeChatAppID
                                       appSecret:kWeChatAppKey
                                     redirectURL:nil];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatTimeLine
                                          appKey:kWeChatAppID
                                       appSecret:kWeChatAppKey
                                     redirectURL:nil];
    [UMConfigure setLogEnabled:YES];

    //环信客服
    HDOptions *option = [[HDOptions alloc] init];
    option.appkey = kHYAppKey;
    option.tenantId = kTenantId;
    option.enableConsoleLog = YES; // 是否打开日志信息
    //option.apnsCertName = apnsCertName;
    option.visitorWaitCount = YES; // 打开待接入访客排队人数功能
    option.showAgentInputState = YES; // 是否显示坐席输入状态
    HDClient *client = [HDClient sharedClient];
    HDError *initError = [client initializeSDKWithOptions:option];
    if (initError) {
        YYLog(@"initError == %@", initError);
    }
    
    //腾讯bug
    BuglyConfig *config = [[BuglyConfig alloc] init];
    // 设置自定义日志上报的级别，默认不上报自定义日志
    config.version = kAPPVersion;
    config.reportLogLevel = BuglyLogLevelWarn;
    [Bugly startWithAppId:kBulyAppID config:config];
    //初始化同盾SDK
    [[YYCommonTools sharedYYCommonTools] makeFMDevice];
    
#pragma mark - Other
    [YYCommonTools startSetWKWebViewUserAgent];
}
@end
