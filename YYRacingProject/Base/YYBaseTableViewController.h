//
//  YYBaseTableViewController.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YYBaseTableViewController : YYBaseViewController
<
YTKRequestDelegate,
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, assign) BOOL        isHideScrollTop;//是否隐藏滚动到顶部按钮
@property (nonatomic, assign) BOOL        changeScrollTop;//是否隐藏滚动到顶部按钮

@property (nonatomic, assign) int         current;       //当前页码
@property (nonatomic, copy)   NSString    *emptyTitle;   //空时内容
@property (nonatomic, copy)   NSString    *footerTitle;  //尾部内容
@property (nonatomic, strong) UIColor     *footerBgStyleColor; 
@property (nonatomic, assign) YYEmptyViewType  NoDataType;  //没数据展示时empty 类型
@property (nonatomic, assign) YYLoadType  loadType;      //加载类型
@property (nonatomic, assign) BOOL isAddReachBottomView;   //是否增加到达底部
@property (nonatomic, assign) BOOL isAddPullDownRefresh; //是否增加下拉刷新
@property (nonatomic, assign) BOOL isAddPullUpRefresh;   //是否增加上拉加载

@property (nonatomic, strong) NSMutableArray *dataIds;   //数据ids
@property (nonatomic, strong) NSMutableArray *listData;  //显示的数据源
@property (nonatomic, assign) NSInteger      totalLength;//当前页已经加载多少条数据
@property (nonatomic, assign) NSInteger      pageLength; //每页多少条

@property (nonatomic, assign) BOOL isSubReload;          //是否子类实现刷新
@property (nonatomic, assign) BOOL isCustomView;         //是否使用自定义emptyview

- (instancetype)initWithTableView;

- (void)loadNewer;
- (void)loadFirstPage;
- (void)loadNextPage;
- (void)loadPullDownPage;

- (void)reloadTableView;
- (YYBaseRequestAPI *)baseRequest;

- (BOOL)canLoadMore;
- (void)scrollViewTop;
- (void)scrollTopEventOperator:(UIButton *)sender;

- (NSArray *)parseResponce:(NSDictionary *)responseDic;

@end
