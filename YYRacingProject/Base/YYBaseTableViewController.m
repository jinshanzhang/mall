//
//  YYBaseTableViewController.m
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewController.h"

#import "YYBaseFootTableViewCell.h"

@interface YYBaseTableViewController ()

//@property (nonatomic, strong) UIButton *scrollTopBtn;

@end

@implementation YYBaseTableViewController

#pragma mark - Public method

- (instancetype)initWithTableView {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.tableStyle = UITableViewStylePlain;
        self.NoDataType = YYEmptyViewNoDataType;
        self.current = 0;  //第一页
        self.pageLength = 0;
        self.totalLength = 0;
        self.isSubReload = NO;
        self.isCustomView = NO;
        self.isHideScrollTop = NO;
        self.isAddReachBottomView = YES;
        self.isAddPullUpRefresh = YES;
        self.isAddPullDownRefresh = YES;
        self.footerTitle = @"我是有底线的";
        self.dataIds = [NSMutableArray array];
        self.listData = [NSMutableArray array];
    }
    return self;
}

- (void)loadNewer {
    [self performSelector:@selector(loadFirstPage) withObject:nil];
}

- (void)loadFirstPage {
    YYBaseRequestAPI *baseRequest = [self baseRequest];
    if (!baseRequest) {
        return;
    }
    self.loadType = YYLoadFirstType;
    [YYCenterLoading showCenterLoading];
    baseRequest.delegate = self;
    [baseRequest start];
}

- (void)loadPullDownPage {
    self.current = 0;
    [self.dataIds removeAllObjects];
    YYBaseRequestAPI *baseRequest = [self baseRequest];
    if (!baseRequest) {
        return;
    }
    self.loadType = YYLoadPullDownLoadType;
    baseRequest.delegate = self;
    [baseRequest start];
}

- (void)loadNextPage {
    YYBaseRequestAPI *baseRequest = [self baseRequest];
    if (!baseRequest) {
        return;
    }
    self.loadType = YYLoadPullUpLoadType;
    baseRequest.delegate = self;
    [baseRequest start];
}

- (YYBaseRequestAPI *)baseRequest {
    return nil;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    return nil;
}

- (BOOL)canLoadMore {
    return YES;
}

- (void)scrollViewTop {
    [self performSelector:@selector(scrollTopEventOperator:) withObject:nil];
}
#pragma mark - Life cycle

- (void)setIsCustomView:(BOOL)isCustomView {
    _isCustomView = isCustomView;
}

- (void)loadView {
    [super loadView];
    
    Class currentCls = [YYBaseFootTableViewCell class];
    
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    
    [self.m_tableView registerClass:currentCls
             forCellReuseIdentifier:strFromCls(currentCls)];
    
    if (self.isAddPullDownRefresh) {
        self.m_tableView.mj_header = [YYRacingBeckRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullDownRefresh)];
    }
    if (self.isAddPullUpRefresh) {
        self.m_tableView.mj_footer = [YYBeckRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(pullUpRefresh)];
        self.m_tableView.mj_footer.hidden = YES;
    }
    
//    if (!self.isHideScrollTop) {
//        self.scrollTopBtn = [YYCreateTools createBtnImage:@"list_scrolltop_icon"];
//        [self.scrollTopBtn addTarget:self action:@selector(scrollTopEventOperator:) forControlEvents:UIControlEventTouchUpInside];
//        self.scrollTopBtn.hidden = YES;
//        [self.view addSubview:self.scrollTopBtn];
//        
//        if (self.changeScrollTop) {
//            [self.scrollTopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.size.mas_equalTo(CGSizeMake(kSizeScale(39), kSizeScale(39)));
//                make.bottom.mas_equalTo(-(kTabbarH));
//                make.right.mas_equalTo(-kSizeScale(10));
//            }];
//        }else{
//            [self.scrollTopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.size.mas_equalTo(CGSizeMake(kSizeScale(39), kSizeScale(39)));
//                make.bottom.mas_equalTo(-(kTabbarH-kSizeScale(20)));
//                make.right.mas_equalTo(-kSizeScale(10));
//            }];
//        }
//    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setter method
- (void)setIsAddPullUpRefresh:(BOOL)isAddPullUpRefresh {
    _isAddPullUpRefresh = isAddPullUpRefresh;
    self.m_collectionView.mj_footer.hidden = !_isAddPullUpRefresh;
}

- (void)setIsAddPullDownRefresh:(BOOL)isAddPullDownRefresh {
    _isAddPullDownRefresh = isAddPullDownRefresh;
    self.m_collectionView.mj_header.hidden = !_isAddPullDownRefresh;
}
#pragma mark - YTKRequestDelegate
- (void)requestFinished:(__kindof YTKBaseRequest *)request {
    BOOL isAdd = YES;
    [YYCenterLoading hideCenterLoading];
    NSDictionary *temp = request.responseJSONObject;
    NSArray *items = [self parseResponce:temp];
    if (_loadType == YYLoadFirstType || _loadType == YYLoadPullDownLoadType) {
        [self.listData removeAllObjects];
        if (kValidArray(items)) {
            [self.listData addObjectsFromArray:items];
        }
    }
    else if (_loadType == YYLoadPullUpLoadType) {
        if (kValidArray(items)) {
            [self.listData addObjectsFromArray:items];
        }
    }
    if ([self canLoadMore] && self.listData.count > 0) {
        self.m_tableView.mj_footer.hidden = NO;
    }
    else {
        self.m_tableView.mj_footer.hidden = YES;
    }
    if ([[self.listData firstObject] isKindOfClass:[NSNumber class]]) {
        isAdd = [[self.listData firstObject] boolValue];
        [self.listData removeFirstObject];
    }
    [self tableViewRefreshEndOperator:YES isHeaderEnd:YES];
    if (!kValidArray(self.listData) && isAdd) {
        if (!self.isCustomView) {
            self.emptyType = self.NoDataType;
            self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:self.NoDataType
                                                               title:self.emptyTitle
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
        }
    }
}


- (void)requestFailed:(__kindof YTKBaseRequest *)request {
    //  request.error.code = -1001 表示网络超时
    [self performSelector:@selector(hideLoadingView) afterDelay:1.5];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.m_tableView.mj_footer.hidden = YES;
        [self tableViewRefreshEndOperator:YES isHeaderEnd:YES];
        if (!kValidArray(self.listData) && !self.isCustomView) {
            self.emptyType = YYEmptyViewNetworkErrorType;
            self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:self.emptyType
                                                               title:nil
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
        }
    });
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (kValidArray(self.listData)&&![self canLoadMore]&&self.isAddReachBottomView) {
        return self.listData.count + 1;
    }
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    UITableViewCell *cell = nil;
    Class currentCls = [YYBaseFootTableViewCell class];
    if (row == self.listData.count&&![self canLoadMore]&&self.isAddReachBottomView) {
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        }
        [(YYBaseFootTableViewCell *)cell setFooterContent:self.footerTitle];
        [(YYBaseFootTableViewCell *)cell setStyleBgColor:self.footerBgStyleColor];
        return cell;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (row == self.listData.count&&![self canLoadMore]&&self.isAddReachBottomView) {
        return 54.0;
    }
    else
        return 0.0f;
}
#pragma mark - Private method
- (void)pullDownRefresh {
    [self loadPullDownPage];
}

- (void)pullUpRefresh {
    [self loadNextPage];
}

- (void)reloadTableView {
    [self tableViewRefreshEndOperator:YES isHeaderEnd:YES];
    if (!kValidArray(self.listData)) {
        if (!self.isCustomView) {
            self.emptyType = self.NoDataType;
            self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:self.NoDataType
                                                               title:self.emptyTitle
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
        }
    }
}

- (void)tableViewRefreshEndOperator:(BOOL)footerEnd
                        isHeaderEnd:(BOOL)headerEnd {
    if (footerEnd) {
        [self.m_tableView.mj_footer endRefreshing];
    }
    if (headerEnd) {
        [self.m_tableView.mj_header endRefreshing];
    }
    if (!self.isSubReload) {
        [self.m_tableView reloadData];
    }
}

- (void)scrollTopEventOperator:(UIButton *)sender {
    [self.m_tableView setContentOffset:CGPointMake(0, 0) animated:NO];
}


- (void)hideLoadingView {
    [YYCenterLoading hideCenterLoading];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat offsetY = scrollView.contentOffset.y;
//    if (offsetY > kScreenH * 1.5) {
//        self.scrollTopBtn.hidden = NO;
//    }
//    else {
//        self.scrollTopBtn.hidden = YES;
//    }
}

#pragma mark - Event
- (void)emptyBtnOperator:(UIButton *)sender {
    //获取全局配置
    [YYCommonTools emptyPageHandler:self.emptyType operator:^(NSInteger index) {
        if (index == 1 || index == 5) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }else if (index == YYEmptyViewCarts){
            if (self.tabBarController.selectedIndex == 0) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                self.tabBarController.selectedIndex = 0;
            }
        }else {
            [self.m_tableView.mj_header beginRefreshing];
        }
    }];
}
@end
