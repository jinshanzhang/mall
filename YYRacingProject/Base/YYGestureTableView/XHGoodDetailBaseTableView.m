//
//  XHGoodDetailBaseTableView.m
//  test
//
//  Created by born on 2016/12/14.
//  Copyright © 2016年 蔡金明. All rights reserved.
//

#import "XHGoodDetailBaseTableView.h"

@implementation XHGoodDetailBaseTableView
/* 忽略tableview header 点击，使其向下层传递*/
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    if (self.tableHeaderView && CGRectContainsPoint(self.tableHeaderView.frame, point)) {
        return YES;   //如果等于No 导致tableHeaderView 上的UIScrollview 不能滚动
    }
    return [super pointInside:point withEvent:event];
}
@end
