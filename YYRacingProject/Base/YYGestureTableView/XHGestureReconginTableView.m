//
//  XHGestureReconginTableView.m
//  test
//
//  Created by born on 2016/12/14.
//  Copyright © 2016年 蔡金明. All rights reserved.
//

#import "XHGestureReconginTableView.h"

@implementation XHGestureReconginTableView
/*此方法返回YES,手势事件会一直往下传递，不论当前层次是否对该事件进行响应*/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    CGPoint currentPoint = [gestureRecognizer locationInView:self];
    if ([[[YYCommonTools getCurrentVC] className] isEqualToString:@"YY_GoodDetailViewController"]) {
        NSInteger row = [self numberOfRowsInSection:0];
        CGRect sectionThird = [self rectForRowAtIndexPath:[NSIndexPath indexPathForRow:(row<=0?0:row-1) inSection:0]];
        if (currentPoint.y < sectionThird.origin.y) {
            return NO;
        }
        return YES;
    }
    else {
        if (currentPoint.x < 0) {
            // 临时方法处理，侧滑table跳动。
            return NO;
        }
        return YES;
    }
    return YES;
}

/** 此方法用来让某些情况接收手势事件，某些不可以接收。**/
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // 截获商品详情轮播图向下传递事件
    if ([NSStringFromClass([touch.view.superview class]) isEqualToString:@"UIScrollView"]) {
        return NO;
    }
    return  YES;
}
@end
