//
//  YYBaseRequestAPI.m
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//
#import "YYBaseRequestAPI.h"
#import "YY_PhoneLoginViewCtrl.h"

@interface YYBaseRequestAPI()

@end

@implementation YYBaseRequestAPI
/**
 *  请求返回的数据格式
 */
- (YTKResponseSerializerType)responseSerializerType {
    return YTKResponseSerializerTypeJSON;
}

- (YTKRequestSerializerType)requestSerializerType {
    return YTKRequestSerializerTypeJSON;
}
/**
 * header
 **/
- (NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    NSString  *x_platform = @"shellvip";
    NSString  *x_source = @"ios";
    NSString  *x_appversion = kAPPVersion;
    NSString  *x_imei = kDeviceUUID;
    NSString  *x_model = kSystemName;
    NSString  *x_osversion = kSystemVersions;
    NSString  *x_channel = @"ios";
    
    NSMutableDictionary *diction = [NSMutableDictionary dictionary];
    if(kValidString(self.token)) {
        [diction setObject:self.token forKey:@"x-token"];
    }
    [diction setObject:x_platform forKey:@"x-platform"];
    [diction setObject:x_source forKey:@"x-source"];
    [diction setObject:x_appversion forKey:@"x-app-version"];
    [diction setObject:x_imei forKey:@"x-imei"];
    [diction setObject:x_model forKey:@"x-model"];
    [diction setObject:x_osversion forKey:@"x-os-version"];
    [diction setObject:x_channel forKey:@"x-channel"];
    [diction setObject:@"application/json" forKey:@"Content-Type"];
    [diction setObject:@"application/json" forKey:@"Accept"];
    [diction setObject:self.headTimestamp forKey:@"x-timestamp"];
    //2.1 加入x-sign
    if(kValidString(self.token)) {
        NSString *tempSign = [NSString stringWithFormat:@"%@&%@&%@&%@&%@",self.token, x_platform, x_source, self.headTimestamp, kEnvConfig.signKey];
        [diction setObject:[[CocoaSecurity md5:tempSign] hexLower] forKey:@"x-sign"];
    }
    return diction;
}

- (id)requestArgument {
    self.token = @"$";
    if (kIsLogin) {
        self.token = kUserInfo.useToken;
    }
    NSString *timeMsec = [YYCommonTools currentTimeMsec];
    self.headTimestamp = timeMsec;
    return self.inParams;
}

- (NSTimeInterval)requestTimeoutInterval {
    return 20;
}
/**
 *  请求失败过滤
 */
- (void)requestFailedFilter {
    [super requestFailedFilter];
    [self formatServerData];
    [self hideLoadingView];
}

- (void)requestCompleteFilter {
    [super requestCompleteFilter];
    [self formatServerData];
}

- (id)responseJSONObject {
    return self.formatResponseJSONObject;
}

#pragma mark - Private method
/**将服务器数据格式处理**/
- (void)formatServerData {
    NSData *data = [self.responseString dataUsingEncoding:NSUTF8StringEncoding];
    if (data) {
        // 初次加工数据
        BOOL isExist = NO;
        id innerData = nil;
        NSString *key = @"ret";
        NSString  *message = nil;
        self.formatResponseJSONObject = nil;
        NSMutableDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSEnumerator *enumerator = [dictionary keyEnumerator];
        NSInteger retCode = [dictionary[key] intValue];
        if ([dictionary containsObjectForKey:@"msg"]) {
            message = [dictionary objectForKey:@"msg"];
        }
        
        if ([dictionary containsObjectForKey:@"data"]) {
            innerData = [dictionary objectForKey:@"data"];
        }
        while ((key = [enumerator nextObject])) {
            isExist = YES;
            if (retCode == 0) {
                if (innerData) {
                    if (innerData && ![innerData isKindOfClass:[NSNull class]]) {
                        self.formatResponseJSONObject = innerData;
                    }
                    else {
                        self.formatResponseJSONObject = @{@"data" : @"ok" };
                    }
                }
            }
            else if (retCode == 106 || retCode == 109) {
                [kUserManager clearUserData];
//                [YYCommonTools showTipMessage:@"请先登录"];
//                YY_PhoneLoginViewCtrl *login = [[YY_PhoneLoginViewCtrl alloc] init];
//                login.modalPresentationStyle = UIModalPresentationFullScreen;
//                [[YYCommonTools getCurrentVC].navigationController presentViewController:login animated:YES completion:nil];
//                kPostNotification(LoginAndQuitSuccessNotification, nil);
            }
            else {
                if (!(retCode >= 132 && retCode <= 134)) {
//                    [YYCommonTools showTipMessage:message];
                }
                else{
                    // 对面对面打卡几种类型过滤掉    弹框
                    //[YYCommonTools showTipMessage:message];
                    self.formatResponseJSONObject = innerData;
                }
            }
        }
    }
}

#pragma mark - Private
- (void)hideLoadingView {
    [YYCenterLoading hideCenterLoading];
}

@end
