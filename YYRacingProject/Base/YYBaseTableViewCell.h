//
//  YYBaseTableViewCell.h
//  YIYanProject
//
//  Created by cjm on 2018/4/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYBaseTableViewCell : UITableViewCell

- (void)createUI;

@end
