//
//  YYBaseViewController.h
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "BaseViewController.h"
#import "YYBaseCustomNavigationBar.h"

typedef  void (^ClickBlock)(UIButton *sender);

@interface YYBaseViewController : BaseViewController

@property (nonatomic, assign) YYEmptyViewType emptyType;

@property (nonatomic, assign) BOOL                      isTopRoot;
@property (nonatomic, strong) YYBaseCustomNavigationBar *navigationBar;
@property (nonatomic, strong) NSDictionary              *parameter;
@property (nonatomic, copy)   NSString                  *parameterJsonString;

@property (nonatomic, assign) UITableViewStyle tableStyle;
@property (nonatomic, strong) UITableView      *m_tableView;
@property (nonatomic, strong) UICollectionViewFlowLayout *m_flowLayout;
@property (nonatomic, strong) UICollectionView *m_collectionView;
@property (nonatomic, strong) YYReachability  *reachability;



-(void)monitorNetwork;

- (void)xh_hideNavigation:(BOOL)isHide;
- (void)xh_hideBackBarItem:(BOOL)isHide;
- (void)xh_alphaNavigation:(CGFloat)alpha;


- (void)xh_addTitle:(NSString *)title;
- (void)xh_addTitleView:(UIView *)titleView;
- (void)xh_navBottomLine:(UIColor *)lineColor;
- (void)xh_navTitleColor:(UIColor *)titleColor;
- (void)xh_navBackgroundColor:(UIColor *)backgroundColor;
- (void)xh_updateRightBtn:(CGFloat)topOffset
              rightOffset:(CGFloat)rightOffset;

- (void)xh_popTopRootViewController:(BOOL)isTopRoot;

- (void)xh_addNavigationItemAlpha:(CGFloat)alpha
                           isLeft:(BOOL)isLeft;

- (void)xh_addNavigationItemFont:(UIFont *)itemFont
                      titltColor:(UIColor *)titleColor
                          isLeft:(BOOL)isLeft;

- (void)xh_addNavigationItemTextColor:(UIColor *)textColor
                                 font:(UIFont *)font
                               isLeft:(BOOL)isLeft;

- (void)xh_addNavigationItemImageName:(NSString *)imageName
                               isLeft:(BOOL)isLeft;

- (void)xh_addNavigationItemWithImageName:(NSString *)imageName
                                   isLeft:(BOOL)isLeft
                               clickEvent:(ClickBlock)event;


- (void)xh_addNavigationItemWithTitle:(NSString *)title
                               isLeft:(BOOL)isLeft
                           clickEvent:(ClickBlock)event;

- (void)xh_addNavigationRightButtonRightPadding:(CGFloat)padding
                                    buttonTitle:(NSString *)buttonTitle;
    
- (void)xh_dismissViewController;


@end
