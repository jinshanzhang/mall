//
//  YYBaseRequestAPI.h
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <YTKNetwork/YTKNetwork.h>
@interface YYBaseRequestAPI : YTKBaseRequest

@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *newtoken;
@property (nonatomic, strong) NSString *headTimestamp;
@property (nonatomic, strong) NSMutableDictionary *inParams;
@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, strong) id formatResponseJSONObject;

@end
