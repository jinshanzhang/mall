//
//  CYLStoreButtonSubclass.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "CYLPlusButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface CYLStoreButtonSubclass : CYLPlusButton<CYLPlusButtonSubclassing>


@end

NS_ASSUME_NONNULL_END
