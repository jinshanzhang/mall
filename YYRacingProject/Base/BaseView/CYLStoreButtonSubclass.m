//
//  CYLStoreButtonSubclass.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "CYLStoreButtonSubclass.h"

#import "YY_WKWebViewController.h"
#import "YY_StoreViewController.h"
@interface CYLStoreButtonSubclass ()


@end

@implementation CYLStoreButtonSubclass

#pragma mark - Life cycle
+ (void)load {
    
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.adjustsImageWhenHighlighted = NO;
    }
    return self;
}

#pragma mark - CYLPlusButtonSubclassing Methods
+ (id)plusButton {
    CYLStoreButtonSubclass *button = [[CYLStoreButtonSubclass alloc] init];
   
    [button setImage:[UIImage imageNamed:@"tabbar_six_default_icon"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"tabbar_six_select_icon"] forState:UIControlStateSelected];

    // button.titleLabel.font = [UIFont systemFontOfSize:9.5];
    // [button sizeToFit];
    //UITabBarController *tabBarVC = [[UITabBarController alloc] init];
    //(这儿取你当前tabBarVC的实例)
    //CGFloat tabBarHeight = tabBarVC.tabBar.frame.size.height;
    button.frame = CGRectMake(0, 0, kScreenW/5, 58);
    // or set frame in this way `button.frame = CGRectMake(0.0, 0.0, 250, 100);`
    //    button.backgroundColor = [UIColor redColor];
    
    // if you use `+plusChildViewController` , do not addTarget to plusButton.
    //[button addTarget:button action:@selector(clickPublish) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

#pragma mark - CYLPlusButtonSubclassing

+ (UIViewController *)plusChildViewController {
    if (kIsFan) {
        YY_WKWebViewController *fanStore = [[YY_WKWebViewController alloc] init];
        fanStore.parameter = @{@"url":kEnvConfig.fanShop,
                               @"pageType": @(1)};
        UINavigationController *fanStoreNav = [[UINavigationController alloc] initWithRootViewController:fanStore];
        return fanStoreNav;
    }
    else {
        YY_StoreViewController *store = [[YY_StoreViewController alloc]init];
        UINavigationController *storeNav = [[UINavigationController alloc] initWithRootViewController:store];
        return storeNav;
    }
}

+ (NSUInteger)indexOfPlusButtonInTabBar {
    return 2;
}

+ (BOOL)shouldSelectPlusChildViewController {
    //BOOL isSelected = CYLExternPlusButton.selected;
    /*if (isSelected) {
        NSLog(@"🔴类名与方法名：%@（在第%@行），描述：%@", @(__PRETTY_FUNCTION__), @(__LINE__), @"PlusButton is selected");
    } else {
        NSLog(@"🔴类名与方法名：%@（在第%@行），描述：%@", @(__PRETTY_FUNCTION__), @(__LINE__), @"PlusButton is not selected");
    }*/
    return YES;
}

+ (CGFloat)multiplierOfTabBarHeight:(CGFloat)tabBarHeight {
    return  IS_IPHONE_X ? 0.25 : 0.4;
}

+ (CGFloat)constantOfPlusButtonCenterYOffsetForTabBarHeight:(CGFloat)tabBarHeight {
    return  0;
}

@end
