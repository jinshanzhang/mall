//
//  YYTabBarControllerConfig.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYTabBarControllerConfig.h"

#import "YY_CartViewCtrl.h"
#import "YY_MyViewController.h"
#import "YY_MainViewController.h"
#import "YY_CircleViewController.h"
#import "YY_WKWebViewController.h"
#import "YY_StoreViewController.h"

#import "YY_MealCardZoneViewController.h"
#import "YY_LifeCircleViewController.h"

@interface YYTabBarControllerConfig ()<UITabBarControllerDelegate,CYLTabBarControllerDelegate>

@property (nonatomic, readwrite, strong) CYLTabBarController *tabBarController;

@end

@implementation YYTabBarControllerConfig

/**
 *  lazy load tabBarController
 *
 *  @return CYLTabBarController
 */
- (CYLTabBarController *)tabBarController {
    if (_tabBarController == nil) {
        /**
         * 以下两行代码目的在于手动设置让TabBarItem只显示图标，不显示文字，并让图标垂直居中。
         * 等效于在 `-tabBarItemsAttributesForController` 方法中不传 `CYLTabBarItemTitle` 字段。
         * 更推荐后一种做法。
         */
        UIEdgeInsets imageInsets = UIEdgeInsetsMake(4, 0, -4, 0);//UIEdgeInsetsMake(4.5, 0, -4.5, 0);
        UIOffset titlePositionAdjustment = UIOffsetZero;//UIOffsetMake(0, MAXFLOAT);
        CYLTabBarController *tabBarController = [CYLTabBarController tabBarControllerWithViewControllers:self.viewControllers
                                                                                   tabBarItemsAttributes:self.tabBarItemsAttributesForController
                                                                                             imageInsets:imageInsets
                                                                                 titlePositionAdjustment:titlePositionAdjustment
                                                                                                 context:self.context
                                                 ];
        [self customizeTabBarAppearance:tabBarController];
        _tabBarController = tabBarController;
    }
    return _tabBarController;
}

- (NSArray *)viewControllers {
    YY_MainViewController *home = [[YY_MainViewController alloc] init];
    YY_CartViewCtrl *cart = [[YY_CartViewCtrl alloc]init];
    YY_MyViewController *mine = [[YY_MyViewController alloc]init];
    YY_CircleViewController *circle = [[YY_CircleViewController alloc] init];
    
    YY_WKWebViewController *mealCardZoneVC = [[YY_WKWebViewController alloc] init];
    mealCardZoneVC.parameter = @{
                                  @"pageType" : @"1",
                                  @"url" : @"http://m.sszj888.com/#/activites/sqkhzkj730"
                                };
//    YY_WKWebViewController *liftCircleVC = [[YY_WKWebViewController alloc] init];
//    liftCircleVC.parameter = @{
//                               @"pageType" : @"1",
//                               @"url" : @"https://m.renminbidaxie.com/#/activites/jjry31"
//                              };
    UINavigationController *mealCardZoneNav = [[UINavigationController alloc] initWithRootViewController:mealCardZoneVC];
//    UINavigationController *liftCircleNav = [[UINavigationController alloc] initWithRootViewController:liftCircleVC];
    UINavigationController *homeNav = [[UINavigationController alloc] initWithRootViewController:home];
    UINavigationController *circleNav = [[UINavigationController alloc] initWithRootViewController:circle];
    
    UINavigationController *cartNav = [[UINavigationController alloc] initWithRootViewController:cart];
    UINavigationController *mineNav = [[UINavigationController alloc] initWithRootViewController:mine];
//    if (kIsFan) {
//        YY_WKWebViewController *fanStore = [[YY_WKWebViewController alloc] init];
//        fanStore.parameter = @{@"url":kEnvConfig.fanShop,
//                               @"pageType": @(1)};
//        UINavigationController *fanStoreNav = [[UINavigationController alloc] initWithRootViewController:fanStore];
//        if (kIsTest) {
//            return @[homeNav,cartNav,mineNav];
//        }
//        else
////            return @[homeNav,circleNav,fanStoreNav,cartNav,mineNav];
//            return @[homeNav,mealCardZoneNav,fanStoreNav,cartNav,mineNav];
//    }
//    else {
//        YY_StoreViewController *store = [[YY_StoreViewController alloc]init];
//        UINavigationController *storeNav = [[UINavigationController alloc] initWithRootViewController:store];
////        return @[homeNav,circleNav,storeNav,cartNav,mineNav];
//        return @[homeNav,mealCardZoneNav,liftCircleNav,cartNav,mineNav];
//    }
    
//    liftCircleNav
    return @[homeNav,mealCardZoneNav,cartNav,mineNav];
}

- (NSArray *)tabBarItemsAttributesForController {
    
    NSDictionary *homeAttributes = @{
                                     CYLTabBarItemTitle: @"首页",
                                     CYLTabBarItemImage: @"tab_good_unselect_icon",
                                     CYLTabBarItemSelectedImage: @"tab_good_select_icon"
                                     };
    NSDictionary *circleAttributes = @{
                                       CYLTabBarItemTitle: @"蜀黍专区",
                                       CYLTabBarItemImage: @"tab_meal_card_unselect_icon",
                                       CYLTabBarItemSelectedImage: @"tab_meal_card_select_icon"
                                       };
    NSDictionary *nineAttributes = @{
                                     CYLTabBarItemTitle: @"生活好券",
                                     CYLTabBarItemImage: @"life_circle_unselect_icon",
                                     CYLTabBarItemSelectedImage: @"life_circle_select_icon"
                                     };
    NSDictionary *inviteAttributes = @{
                                       CYLTabBarItemTitle: @"购物车",
                                       CYLTabBarItemImage: @"tab_car_unselect_icon",
                                       CYLTabBarItemSelectedImage: @"tab_car_select_icon"
                                       };
    NSDictionary *mineAttributes = @{
                                     CYLTabBarItemTitle: @"我的",
                                     CYLTabBarItemImage: @"tab_my_unselect_icon",
                                     CYLTabBarItemSelectedImage: @"tab_my_select_icon"
                                     };
//    if (kIsTest) {
//        return @[
//                 homeAttributes,
//                 nineAttributes,
//                 inviteAttributes,
//                 mineAttributes
//                 ];
//    }
//    else {
        return @[
                 homeAttributes,
                 circleAttributes,
//                 nineAttributes,
                 inviteAttributes,
                 mineAttributes
                 ];
//    }
}

/**
 *  更多TabBar自定义设置：比如：tabBarItem 的选中和不选中文字和背景图片属性、tabbar 背景图片属性等等
 */
- (void)customizeTabBarAppearance:(CYLTabBarController *)tabBarController {
#warning CUSTOMIZE YOUR TABBAR APPEARANCE
    // Customize UITabBar height
    // 自定义 TabBar 高度
    //tabBarController.tabBarHeight = CYL_IS_IPHONE_X ? 65 : 40;
    
    NSMutableDictionary *normalDict = [NSMutableDictionary dictionary];
    NSMutableDictionary *selectDict = [NSMutableDictionary dictionary];
    
    [normalDict setObject:MediumFont(10) forKey:NSFontAttributeName];
    [normalDict setObject:XHBlackMidColor forKey:NSForegroundColorAttributeName];
    [[UITabBarItem appearance]setTitleTextAttributes:normalDict forState:UIControlStateNormal];
    
    [selectDict setObject:MediumFont(10) forKey:NSFontAttributeName];
    [selectDict setObject:HexRGB(0x222222) forKey:NSForegroundColorAttributeName];
    [[UITabBarItem appearance]setTitleTextAttributes:selectDict forState:UIControlStateSelected];
    
    
    UITabBar *tabBarAppearance = [UITabBar appearance];
    tabBarAppearance.translucent = NO;
    UIImage *scaleBackgroundImage = [UIImage imageWithColor:XHWhiteColor];
    scaleBackgroundImage = [UIImage scaleImage:scaleBackgroundImage toScale:1.0];
    [tabBarAppearance setBackgroundImage:scaleBackgroundImage];
    
    [tabBarController.tabBar setShadowImage:[UIImage imageWithColor:XHLightColor]];
    
    tabBarController.moreNavigationController.navigationBarHidden = YES;
    // set the text color for unselected state
    // 普通状态下的文字属性
    // NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    // normalAttrs[NSForegroundColorAttributeName] = [UIColor grayColor];
    
    // set the text color for selected state
    // 选中状态下的文字属性
    // NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    // selectedAttrs[NSForegroundColorAttributeName] = [UIColor blackColor];
    
    // set the text Attributes
    // 设置文字属性
    // UITabBarItem *tabBar = [UITabBarItem appearance];
    // [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    // [tabBar setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    
    // Set the dark color to selected tab (the dimmed background)
    // TabBarItem选中后的背景颜色
    // [self customizeTabBarSelectionIndicatorImage];
    
    // update TabBar when TabBarItem width did update
    // If your app need support UIDeviceOrientationLandscapeLeft or UIDeviceOrientationLandscapeRight，
    // remove the comment '//'
    // 如果你的App需要支持横竖屏，请使用该方法移除注释 '//'
    // [self updateTabBarCustomizationWhenTabBarItemWidthDidUpdate];
    
    // set the bar shadow image
    // This shadow image attribute is ignored if the tab bar does not also have a custom background image.So at least set somthing.
    //[[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    //[[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
    //    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"tapbar_top_line"]];
    //    [[UITabBar appearance] setTintColor:[UIColor redColor]];
    // set the bar background image
    // 设置背景图片
    //UITabBar *tabBarAppearance = [UITabBar appearance];
    
    //FIXED: #196
    // UIImage *tabBarBackgroundImage = [UIImage imageNamed:@"tab_bar"];
    // UIImage *scanedTabBarBackgroundImage = [UIImage scaleImage:tabBarBackgroundImage toScale:1.0];
    // [tabBarAppearance setBackgroundImage:scanedTabBarBackgroundImage];
    
    // remove the bar system shadow image
    // 去除 TabBar 自带的顶部阴影
    // iOS10 后 需要使用 `-[CYLTabBarController hideTabBadgeBackgroundSeparator]` 见 AppDelegate 类中的演示;
    // [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
}


@end
