//
//  YYTabBarControllerConfig.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/22.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CYLTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYTabBarControllerConfig : NSObject

@property (nonatomic, readonly, strong) CYLTabBarController *tabBarController;
@property (nonatomic, copy) NSString *context;

@end

NS_ASSUME_NONNULL_END
