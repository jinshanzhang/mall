//
//  YYBaseFootTableViewCell.m
//  YIYanProject
//
//  Created by cjm on 2018/4/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseFootTableViewCell.h"
@interface YYBaseFootTableViewCell()

@property (nonatomic, strong) UIView   *cellbackgroundView;
@property (nonatomic, strong) UILabel  *line;
@property (nonatomic, strong) UILabel  *m_textLabel;

@end


@implementation YYBaseFootTableViewCell

- (void)createUI {

    self.cellbackgroundView = [YYCreateTools createView:XHStoreGrayColor];
    [self.contentView addSubview:self.cellbackgroundView];
    
    [self.cellbackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    self.line = [YYCreateTools createLabel:nil
                                      font:normalFont(14)
                                 textColor:HexRGB(0xeeeeee)];
    self.line.backgroundColor = HexRGB(0xeeeeee);
    [self.cellbackgroundView addSubview:self.line];
    
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1);
        make.centerY.equalTo(self.cellbackgroundView);
    }];
    
    self.m_textLabel = [YYCreateTools createLabel:nil
                                             font:normalFont(12)
                                        textColor:HexRGB(0xcccccc)];
    self.m_textLabel.textAlignment = NSTextAlignmentCenter;
    [self.cellbackgroundView addSubview:self.m_textLabel];
    
    [self.m_textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.line);
        make.size.mas_equalTo(CGSizeMake(120, 20));
    }];
}

- (void)setFooterContent:(NSString *)footerContent {
    _footerContent = footerContent;
    self.m_textLabel.text = _footerContent;
}

- (void)setStyleBgColor:(UIColor *)styleBgColor {
    _styleBgColor = styleBgColor;
    self.backgroundColor = self.contentView.backgroundColor = self.m_textLabel.backgroundColor = self.cellbackgroundView.backgroundColor = styleBgColor;
}

@end
