//
//  YYBaseCustomNavigationBar.h
//  YIYanProject
//
//  Created by cjm on 2018/5/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YYBaseCustomNavigationBar : UIView

@property (nonatomic, strong) NSString *navigationTitle;  //导航标题
@property (nonatomic, strong) UIColor  *navigationTitleColor;
@property (nonatomic, strong) UIFont   *navigationTitleFont;

@property (nonatomic, assign) BOOL     isHideBackBarItem;
@property (nonatomic, strong) UIFont   *rightButtonFont;
@property (nonatomic, strong) UIColor  *rightButtonTitleColor;

@property (nonatomic, strong) UIColor  *backgroundColor;
@property (nonatomic, strong) NSString *backgroundImageName;

@property (nonatomic, strong) UIColor  *bottomLineColor;

@property (nonatomic, strong) UILabel       *navigationTitleLable;  //导航标题
@property (nonatomic, strong) UIButton      *backButton;
@property (nonatomic, strong) UIButton      *rightButton;
@property (nonatomic, strong) UIView        *bottomLine;            //导航底线
@property (nonatomic, strong) UIView        *backgroundView;        //导航背景
@property (nonatomic, strong) UIImageView   *backgroundImageView;   //导航背景图片

- (void)yy_setBackgroundAlpha:(CGFloat)alpha;

- (void)yy_setNavigationButtonAlpha:(CGFloat)alpha
                             isLeft:(BOOL)isLeft;

- (void)yy_setNavigationButtonTextColor:(UIColor *)textColor
                                 font:(UIFont *)font
                               isLeft:(BOOL)isLeft;

- (void)yy_setNavigationButtonImageName:(NSString *)imageName
                                    isLeft:(BOOL)isLeft;

- (void)yy_setNavigationButtonImageName:(NSString *)imageName
                        isLeft:(BOOL)isLeft
                   buttonBlock:(void(^)(UIButton *sender))buttonBlock;

- (void)yy_setNavigationButtonTitle:(NSString *)title
                                 isLeft:(BOOL)isLeft
                            buttonBlock:(void(^)(UIButton *sender))buttonBlock;

- (void)yy_setNavigationRightButtonRightPadding:(CGFloat)padding
                                    buttonTitle:(NSString *)buttonTitle;

- (void)updateRightBtnLayout:(CGFloat)topOffset
                 rightOffset:(CGFloat)rightOffset;

+ (int)navBarBottom;

+ (instancetype)yyBaseCustomNavigationBar;

@end
