//
//  YYBaseFootTableViewCell.h
//  YIYanProject
//
//  Created by cjm on 2018/4/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface YYBaseFootTableViewCell : YYBaseTableViewCell

@property (nonatomic, strong) NSString *footerContent;
@property (nonatomic, strong) UIColor  *styleBgColor;

@end
