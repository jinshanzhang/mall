//
//  YYBaseCustomNavigationBar.m
//  YIYanProject
//
//  Created by cjm on 2018/5/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseCustomNavigationBar.h"
#import "sys/utsname.h"

@interface YYBaseCustomNavigationBar()

@end

@implementation YYBaseCustomNavigationBar


#pragma mark -Setter method
- (UILabel *)navigationTitleLable {
    if (!_navigationTitleLable) {
        _navigationTitleLable = [UILabel new];
        _navigationTitleLable.textAlignment = NSTextAlignmentCenter;
        _navigationTitleLable.hidden = YES;
    }
    return _navigationTitleLable;
}

- (UIButton *)backButton {
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _backButton.isIgnore = YES;
        _backButton.imageView.contentMode = UIViewContentModeCenter;
        _backButton.hidden = YES;
    }
    return _backButton;
}

- (UIButton *)rightButton {
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightButton.isIgnore = YES;
        _rightButton.imageView.contentMode = UIViewContentModeCenter;
        _rightButton.hidden = YES;
        _rightButton.titleLabel.font = normalFont(14);
        [_rightButton setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [_rightButton setTitleColor:XHBlackColor forState:UIControlStateSelected];
    }
    return _rightButton;
}

- (UIView *)bottomLine {
    if (!_bottomLine) {
        _bottomLine = [UIView new];
    }
    return _bottomLine;
}

- (UIView *)backgroundView {
    if (!_backgroundView) {
        _backgroundView = [UIView new];
    }
    return _backgroundView;
}

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.hidden = YES;
    }
    return _backgroundImageView;
}

- (void)setNavigationTitle:(NSString *)navigationTitle {
    _navigationTitle = navigationTitle;
    self.navigationTitleLable.hidden = NO;
    self.navigationTitleLable.text = _navigationTitle;
}

- (void)setNavigationTitleColor:(UIColor *)navigationTitleColor {
    _navigationTitleColor = navigationTitleColor;
    self.navigationTitleLable.textColor = _navigationTitleColor;
}

- (void)setNavigationTitleFont:(UIFont *)navigationTitleFont {
    _navigationTitleFont = navigationTitleFont;
    self.navigationTitleLable.font = _navigationTitleFont;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    _backgroundColor = backgroundColor;
    self.backgroundView.hidden = NO;
    self.backgroundView.backgroundColor = _backgroundColor;
    self.backgroundImageView.hidden = YES;
}

- (void)setBackgroundImageName:(NSString *)backgroundImageName {
    _backgroundImageName = backgroundImageName;
    self.backgroundView.hidden = YES;
    self.backgroundImageView.image = [UIImage imageNamed:_backgroundImageName];
    self.backgroundImageView.hidden = NO;
}

- (void)setBottomLineColor:(UIColor *)bottomLineColor {
    _bottomLineColor = bottomLineColor;
    self.bottomLine.backgroundColor = _bottomLineColor;
}

- (void)setIsHideBackBarItem:(BOOL)isHideBackBarItem {
    _isHideBackBarItem = isHideBackBarItem;
    self.backButton.hidden = _isHideBackBarItem;
}

- (void)setRightButtonFont:(UIFont *)rightButtonFont {
    _rightButtonFont = rightButtonFont;
    self.rightButton.titleLabel.font = _rightButtonFont;
}

- (void)setRightButtonTitleColor:(UIColor *)rightButtonTitleColor {
    _rightButtonTitleColor = rightButtonTitleColor;
    [self.rightButton setTitleColor:_rightButtonTitleColor forState:UIControlStateNormal];
}

#pragma mark -Life cycle

- (instancetype)init {
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

#pragma mark -Public method
- (void)yy_setBackgroundAlpha:(CGFloat)alpha {
    self.backgroundView.alpha = alpha;
    self.navigationTitleLable.alpha = alpha;
    self.backgroundImageView.alpha = alpha;
}

- (void)yy_setNavigationButtonAlpha:(CGFloat)alpha
                             isLeft:(BOOL)isLeft {
    if (isLeft) {
        [self.backButton setAlpha:alpha];
    }
    else {
        [self.rightButton setAlpha:alpha];
    }
}

- (void)yy_setNavigationButtonTextColor:(UIColor *)textColor
                                   font:(UIFont *)font
                                 isLeft:(BOOL)isLeft {
    if (isLeft) {
        _backButton.titleLabel.font = font;
        [_backButton setTitleColor:textColor forState:UIControlStateNormal];
        [_backButton setTitleColor:textColor forState:UIControlStateSelected];
    }
    else {
        _rightButton.titleLabel.font = font;
        [_rightButton setTitleColor:textColor forState:UIControlStateNormal];
        [_rightButton setTitleColor:textColor forState:UIControlStateSelected];
    }
}

- (void)yy_setNavigationButtonImageName:(NSString *)imageName isLeft:(BOOL)isLeft {
    if (!kValidString(imageName)) {
        return;
    }
    if (isLeft) {
        [self.backButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [self.backButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
    }
    else {
        [self.rightButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [self.rightButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
    }
}

- (void)yy_setNavigationButtonImageName:(NSString *)imageName isLeft:(BOOL)isLeft buttonBlock:(void (^)(UIButton *))buttonBlock {
    if (!kValidString(imageName)) {
        return;
    }
    if (isLeft) {
        self.backButton.hidden = NO;
        [self.backButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [self.backButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
        self.backButton.actionBlock = ^(UIButton *sender) {
            buttonBlock(sender);
        };
        return;
    }
    else {
        self.rightButton.hidden = NO;
        [self.rightButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [self.rightButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateSelected];
        self.rightButton.actionBlock = ^(UIButton *sender) {
            buttonBlock(sender);
        };
        return;
    }
}

- (void)yy_setNavigationButtonTitle:(NSString *)title
                             isLeft:(BOOL)isLeft
                        buttonBlock:(void(^)(UIButton *sender))buttonBlock {
    if (!kValidString(title)) {
        return;
    }
    if (isLeft) {
        self.backButton.hidden = NO;
        [self.backButton setTitle:title forState:UIControlStateNormal];
        [self.backButton setTitle:title forState:UIControlStateSelected];
        self.backButton.actionBlock = ^(UIButton *sender) {
            buttonBlock(sender);
        };
        return;
    }
    else {
        self.rightButton.hidden = NO;
        [self.rightButton setTitle:title forState:UIControlStateNormal];
        self.rightButton.titleLabel.font = boldFont(15);
        [self.rightButton setTitle:title forState:UIControlStateSelected];
        self.rightButton.actionBlock = ^(UIButton *sender) {
            buttonBlock(sender);
        };
        return;
    }
}

- (void)yy_setNavigationRightButtonRightPadding:(CGFloat)padding
                                    buttonTitle:(NSString *)buttonTitle {
    if (!kValidString(buttonTitle)) {
        return;
    }
    NSInteger top = IS_IPHONE_X ? 44 : 20;
    CGSize titleSize = [YYCommonTools sizeWithText:buttonTitle
                                              font:boldFont(15)
                                           maxSize:CGSizeMake(kSizeScale(100), kSizeScale(40))];
    [self.rightButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(top);
        make.size.mas_equalTo(CGSizeMake(titleSize.width, 44));
        make.right.mas_equalTo(-padding);
    }];
}

- (void)updateRightBtnLayout:(CGFloat)topOffset
                 rightOffset:(CGFloat)rightOffset {
    [self.rightButton layoutIfNeeded];
    
    [self.rightButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(rightOffset));
        make.top.mas_equalTo(kSizeScale(topOffset));
        make.size.mas_equalTo(CGSizeMake(self.rightButton.width, self.rightButton.height));
    }];
}

+ (instancetype)yyBaseCustomNavigationBar {
    YYBaseCustomNavigationBar *navigationBar = [[self alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kNavigationH)];
    return navigationBar;
}

+ (int)navBarBottom {
    return 44 + CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
}

#pragma makr -Private method
- (void)createUI {
    [self addSubview:self.backgroundView];
    [self addSubview:self.backgroundImageView];
    [self addSubview:self.backButton];
    [self addSubview:self.rightButton];
    [self addSubview:self.navigationTitleLable];
    [self addSubview:self.bottomLine];
    
    [self updateFrame];
    
    self.backgroundView.backgroundColor = [UIColor whiteColor];
}

- (void)updateFrame {
    
    NSInteger top = IS_IPHONE_X ? 44 : 20;
    NSInteger margin = 0;
    NSInteger buttonHeight = 44;
    NSInteger buttonWidth = kSizeScale(44);
    NSInteger titleLabelHeight = 44;
    NSInteger titleLabelWidth = 180;
    
    self.backgroundView.frame = self.bounds;
    self.backgroundImageView.frame = self.bounds;
    
    self.backButton.frame = CGRectMake(margin, top, 44, buttonHeight);
    self.rightButton.frame = CGRectMake(kScreenW - buttonWidth - margin, top, buttonWidth, buttonHeight);
    self.navigationTitleLable.frame = CGRectMake((kScreenW - titleLabelWidth) / 2, top, titleLabelWidth, titleLabelHeight);
    self.bottomLine.frame = CGRectMake(0, (CGFloat)(self.bounds.size.height-0.5), kScreenW, 0.5);
    [self.rightButton enlargeTouchAreaWithTop:10 right:-10 bottom:-10 left:10];
}

@end
