//
//  YYBaseViewController.m
//  YIYanProject
//
//  Created by cjm on 2018/4/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"
#import "YYAudioManagerClass.h"

@interface YYBaseViewController ()
@property (nonatomic, strong) UIScrollView              *NetScro;

@end

@implementation YYBaseViewController

#pragma mark - Setter method

- (UITableView *)m_tableView {
    if (!_m_tableView) {
        _m_tableView = [[UITableView alloc]initWithFrame:CGRectZero style:self.tableStyle];
        _m_tableView.allowsSelection = NO;
        _m_tableView.scrollsToTop = YES;
        _m_tableView.backgroundColor = XHWhiteColor;
        if (@available(iOS 11, *)) {
            //目前如此
            _m_tableView.estimatedRowHeight = 0;
            _m_tableView.estimatedSectionHeaderHeight = 0;
            _m_tableView.estimatedSectionFooterHeight = 0;
        }else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        _m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _m_tableView;
}

- (YYBaseCustomNavigationBar *)navigationBar {
    if (!_navigationBar) {
        _navigationBar = [YYBaseCustomNavigationBar yyBaseCustomNavigationBar];
        _navigationBar.navigationTitleFont = boldFont(17);
        _navigationBar.navigationTitleColor = XHBlackColor;
    }
    return _navigationBar;
}

#pragma mark -Life cycle
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // 音频组件只存在这些页面
    [[YYAudioManagerClass shared] setNeedFilterShowClassNames:[@[@"YY_AudioDetailViewController",@"YY_MainFeaturedViewController",@"YY_MainOtherSortViewController",@"YY_WKWebViewController",@"YY_ChapterViewController",@"YY_MineCourceViewController",@"YY_GoodDetailViewController",@"YY_GoodDetailDetailViewController",@"YY_GoodDetailNoticeViewController",@"YY_GoodDetailArgumentViewController",@"YY_GoodCourceDetailViewController"] mutableCopy]];
    // 音频组件这些页面特殊处理
    [[YYAudioManagerClass shared] setNeedFilterUpdateFrameClassNames:[@[@"YY_MainFeaturedViewController", @"YY_MainOtherSortViewController",@"YY_WKWebViewController",@"YY_GoodDetailViewController",@"YY_GoodDetailDetailViewController",@"YY_GoodDetailNoticeViewController",@"YY_GoodDetailArgumentViewController",@"YY_GoodCourceDetailViewController"] mutableCopy]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /**此句不加导致10.0 以下系统刷新控件下移**/
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.view addSubview:self.navigationBar];
    [self xh_hideNavigation:NO];
    self.view.backgroundColor = XHWhiteColor;
    [self xh_navBottomLine:XHLightColor];
    /*
    原生导航处理
    self.navigationItem.da_navigationBarBarTintColor = XHWhiteColor;
    self.navigationItem.da_navigationBarBackgroundViewAlpha = 1;
    self.edgesForExtendedLayout = UIRectEdgeNone;    //禁止四周延伸
    self.automaticallyAdjustsScrollViewInsets = NO;  //禁止自动调整滚动视图的内边距
    self.view.backgroundColor = XHWhiteColor;
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_2 && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_11_0
    [self useMethodToFindBlackLineAndHind];
#endif*/
    // Do any additional setup after loading the view.
}

- (void)monitorNetwork{
    @weakify(self);
    //必须声明成属性才会调用，直接创建监听不到。
    self.reachability = [YYReachability reachabilityWithHostname:@"www.baidu.com"];
    self.reachability.notifyBlock = ^(YYReachability * _Nonnull reachability) {
        YYReachabilityStatus status = reachability.status;
        @strongify(self);
        switch (status) {
            case YYReachabilityStatusNone:
            {
                YYLog(@"-------------%@",@"网络中断");
                [self showNetSet];
                break;
            }
            case YYReachabilityStatusWWAN:
            {
                YYLog(@"-------------%@",@"4G");
                [self.NetScro removeFromSuperview];
                break;
            }
            case YYReachabilityStatusWiFi:
            {
                YYLog(@"-------------%@",@"WIFI");
                [self.NetScro removeFromSuperview];
                break;
            }
            default:
                break;
        }
    };
}

-(void)showNetSet
{
    if (!self.NetScro) {
        self.NetScro = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kStatuH, kScreenW, kScreenH)];
        _NetScro.backgroundColor = XHWhiteColor;
        _NetScro.contentSize = CGSizeMake(0, kSizeScale(1130));
        [self.view addSubview:self.NetScro];
        
        UIImageView *img = [YYCreateTools createImageView:@"login_nonet" viewModel:-1];
        [_NetScro addSubview:img];
        
        [img mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.NetScro);
            make.top.mas_equalTo(30);
        }];
//        self.NetScro.hidden = YES;
        UILabel *titleLabe = [YYCreateTools createLabel:@"点击【设置】允许蜀黍之家使用网络" font:normalFont(16) textColor:XHBlackColor];
        [img addSubview:titleLabe];
        [titleLabe mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.NetScro);
            make.bottom.mas_equalTo(-10);
        }];
        
        UIButton *btn = [YYCreateTools createBtn:@"前往设置" font:normalFont(14) textColor:XHWhiteColor];
        [self.NetScro addSubview:btn];
        btn.backgroundColor = XHRedColor;
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(img.mas_bottom).mas_offset(10);
            make.centerX.mas_equalTo(self.NetScro);
            make.size.mas_equalTo(CGSizeMake(92, 30));
        }];

        btn.actionBlock = ^(UIButton *sender) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        };
        
        UIImageView *totalIMG = [YYCreateTools createImageView:@"login_showset" viewModel:-1];
        [_NetScro addSubview:totalIMG];
        [totalIMG mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(btn.mas_bottom).mas_offset(20);
            make.width.mas_equalTo(kScreenW);
            make.height.mas_equalTo(770);
        }];
    }
}

- (void)checkNet
{
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"已为\"蜀黍之家\"关闭蜂窝移动数据" message:@"您可以在\"设置\"中为此应用打开蜂窝移动数据" delegate:self cancelButtonTitle:@"设置" otherButtonTitles:@"确定", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *btnTitle = [alertView buttonTitleAtIndex:buttonIndex];
    if ([btnTitle isEqualToString:@"设置"]) {
        if (@available(iOS 9.0, *)) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 自定义导航栏 --- 这个方法需要和下面的方法同时使用
// [self.navigationController setNavigationBarHidden:YES animated:YES];
- (BOOL)fd_prefersNavigationBarHidden {
    return YES;
}

#pragma mark - AddMethod
- (void)xh_hideNavigation:(BOOL)isHide {
    self.navigationBar.hidden = isHide;
}

-  (void)xh_hideBackBarItem:(BOOL)isHide {
    self.navigationBar.isHideBackBarItem = isHide;
}

- (void)xh_alphaNavigation:(CGFloat)alpha {
    [self.navigationBar yy_setBackgroundAlpha:alpha];
}

- (void)xh_addTitle:(NSString *)title {
    self.navigationBar.navigationTitle = title;
}

- (void)xh_addTitleView:(UIView *)titleView {
    [self.navigationBar addSubview:titleView];
}

- (void)xh_navBottomLine:(UIColor *)lineColor {
    self.navigationBar.bottomLineColor = lineColor;
}
- (void)xh_navTitleColor:(UIColor *)titleColor {
    self.navigationBar.navigationTitleColor = titleColor;
}

- (void)xh_navBackgroundColor:(UIColor *)backgroundColor {
    self.navigationBar.backgroundColor = backgroundColor;
}

- (void)xh_updateRightBtn:(CGFloat)topOffset
              rightOffset:(CGFloat)rightOffset {
    [self.navigationBar updateRightBtnLayout:topOffset
                                 rightOffset:rightOffset];
}

- (void)xh_dismissViewController{
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTarget:self action:@selector(dismissAction) image:[UIImage imageNamed:@"detail_navbar_back"] imageEdgeInsets: UIEdgeInsetsMake(0, -10, 0, 0)];
}

- (void)xh_addNavigationItemFont:(UIFont *)itemFont
                      titltColor:(UIColor *)titleColor
                          isLeft:(BOOL)isLeft {
    if (isLeft) {
        return;
    }
    self.navigationBar.rightButtonFont = itemFont;
    self.navigationBar.rightButtonTitleColor = titleColor;
}

- (void)xh_popTopRootViewController:(BOOL)isTopRoot {
    @weakify(self);
    __block BOOL isTempRoot = isTopRoot;
    [self.navigationBar yy_setNavigationButtonImageName:@"detail_navbar_back"
                                        isLeft:YES
                                   buttonBlock:^(UIButton *sender) {
                                       @strongify(self);
                                       if (isTempRoot) {
                                           [self.navigationController popToRootViewControllerAnimated:YES];
                                       }
                                       else {
                                           [self.navigationController popViewControllerAnimated:YES];
                                       }
                                   }];
}

- (void)xh_addNavigationItemAlpha:(CGFloat)alpha
                           isLeft:(BOOL)isLeft {
    [self.navigationBar yy_setNavigationButtonAlpha:alpha
                                             isLeft:isLeft];
}

- (void)xh_addNavigationItemTextColor:(UIColor *)textColor
                                 font:(UIFont *)font
                               isLeft:(BOOL)isLeft {
    [self.navigationBar yy_setNavigationButtonTextColor:textColor
                                                   font:font
                                                 isLeft:isLeft];
}

- (void)xh_addNavigationItemImageName:(NSString *)imageName
                               isLeft:(BOOL)isLeft {
    [self.navigationBar yy_setNavigationButtonImageName:imageName
                                                    isLeft:isLeft];
}

- (void)xh_addNavigationItemWithImageName:(NSString *)imageName
                                   isLeft:(BOOL)isLeft
                               clickEvent:(ClickBlock)event {
    [self.navigationBar yy_setNavigationButtonImageName:imageName
                                                 isLeft:isLeft
                                            buttonBlock:^(UIButton *sender) {
                                                event(sender);
                                            }];
}

- (void)xh_addNavigationItemWithTitle:(NSString *)title
                               isLeft:(BOOL)isLeft
                           clickEvent:(ClickBlock)event {
    [self.navigationBar yy_setNavigationButtonTitle:title
                                             isLeft:isLeft
                                        buttonBlock:^(UIButton *sender) {
                                            event(sender);
                                        }];
}

- (void)xh_addNavigationRightButtonRightPadding:(CGFloat)padding
                                    buttonTitle:(NSString *)buttonTitle {
    [self.navigationBar yy_setNavigationRightButtonRightPadding:padding
                                                    buttonTitle:buttonTitle];
}

#pragma mark - Private
/**按钮点击操作**/
- (void)pushAction {
    if (self.isTopRoot)
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)useMethodToFindBlackLineAndHind {
    UIImageView* blackLineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    //隐藏黑线（在viewWillAppear时隐藏，在viewWillDisappear时显示）
    blackLineImageView.hidden = YES;
}

- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
@end
