//
//  YY_CartViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewController.h"

@interface YY_CartViewCtrl : YYBaseTableViewController
typedef NS_ENUM(NSInteger , CartRightBtnFuc) {
    CartSubmit = 0,     //
    CartManage = 1,
};

@property (nonatomic, assign) CartRightBtnFuc func;

@end
