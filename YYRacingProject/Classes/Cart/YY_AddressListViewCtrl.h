//
//  YY_AddressListViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewController.h"

@interface YY_AddressListViewCtrl : YYBaseTableViewController

@property (nonatomic, copy) void(^setFinish)(int addreID);

@end
