//
//  YY_OrderUnpaidViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_OrderUnpaidViewCtrl : YYBaseViewController

@property (nonatomic, assign) long orderID;
@property (nonatomic, assign) BOOL canback;


@end

