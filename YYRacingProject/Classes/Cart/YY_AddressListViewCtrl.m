//
//  YY_AddressListViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_AddressListViewCtrl.h"
#import "AddressListCell.h"
#import "AddressListRequestAPI.h"
#import "ChangeAddressDefaultRequestAPI.h"
#import "DeleteAddressRequestAPI.h"
#import "CheckAddressRequestAPI.h"
#import "YY_SetUpAddressViewCtrl.h"
#import "AddressModel.h"

@interface YY_AddressListViewCtrl ()

@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation YY_AddressListViewCtrl

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.NoDataType = YYEmptyViewAddress;
        self.emptyTitle = @"您的收货地址还是空的哦";
        self.isAddPullDownRefresh = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [MobClick event:@"storeAddress"];
    [self xh_addTitle:@"收货地址管理"];
    [self xh_popTopRootViewController:NO];
    
    [self creatFooterView];
    [self loadNewer];
    
    Class current = [AddressListCell class];
    registerClass(self.m_tableView, current);
    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.estimatedRowHeight = 128;
    self.m_tableView.allowsSelection  = YES;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(50));
    }];
    // Do any additional setup after loading the view.
}



-(void)creatFooterView{
    UIView *backView = [YYCreateTools createView:XHClearColor];
    [self.view addSubview:backView];
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.m_tableView);
        make.top.equalTo(self.m_tableView.mas_bottom);
        make.height.mas_equalTo(50);
    }];
//    backView.backgroundColor = [UIColor addGoldGradient:CGSizeMake(kScreenW, 50)];
    backView.backgroundColor = XHMainColor;
    
    UIImageView *icon = [YYCreateTools createImageView:@"addressAdd_icon" viewModel:-1];
    [backView addSubview:icon];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(126));
        make.centerY.equalTo(backView);
    }];
    
    UILabel *label = [YYCreateTools createLabel:@"新增收货地址" font:normalFont(15) textColor:XHWhiteColor];
    [backView addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(icon.mas_right).mas_offset(12);
        make.centerY.equalTo(backView);
    }];
    [backView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toNewAddress)]];
}
-(void)toNewAddress{
    YY_SetUpAddressViewCtrl *vc = [YY_SetUpAddressViewCtrl new];
    vc.setFinish = ^(NSString *str) {
        [self loadNewer];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -request
-(void)changeAddressFlag:(int)addressID
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(addressID) forKey:@"id"];
    
    [YYCenterLoading showCenterLoading];
    ChangeAddressDefaultRequestAPI *api = [[ChangeAddressDefaultRequestAPI alloc] initChangeAddressDefaultRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            [YYCommonTools showTipMessage:@"设置成功"];
            [self loadNewer];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)checkAddress:(int)addressID
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(addressID) forKey:@"addressId"];
    
    [YYCenterLoading showCenterLoading];
    CheckAddressRequestAPI *api = [[CheckAddressRequestAPI alloc] initCheckAddressRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            ReceiveAddressModel *model = [ReceiveAddressModel modelWithJSON:responDict];
            YY_SetUpAddressViewCtrl *vc = [YY_SetUpAddressViewCtrl new];
            vc.receiveModel = model;
            vc.title = @"修改收货地址";
            vc.setFinish = ^(NSString *str) {
                [self loadNewer];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}


-(void)deleteAddress:(int)addressID
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSArray *arr = [NSArray arrayWithObject:@(addressID)];
    [dict setObject:arr forKey:@"addressIdList"];
    
    [YYCenterLoading showCenterLoading];
    DeleteAddressRequestAPI*api = [[DeleteAddressRequestAPI alloc] initDeleteAddressRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            [self loadNewer];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}



#pragma mark - Base
- (YYBaseRequestAPI *)baseRequest {
    AddressListRequestAPI *collectAPI = [[AddressListRequestAPI alloc] initAddressListRequest:[[NSDictionary dictionary] mutableCopy]];
    return collectAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.dataArr = [NSMutableArray array];
        self.dataArr = [[NSArray modelArrayWithClass:[AddressModel class] json:[responseDic objectForKey:@"receiveAddressList"]] mutableCopy];
        [self.m_tableView reloadData];
        return self.dataArr;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [YYCreateTools createView:XHLightColor];
    sectionView.frame = CGRectMake(0, 0, kScreenW, 10);
    return sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 10;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Class currentCls = [AddressListCell class];
    AddressListCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    }
    cell.model = self.dataArr[indexPath.section];
    cell.changeStatus = ^(int addressID) {
    
        [self changeAddressFlag:addressID];
    };
    cell.deleteAddress = ^(int addressID) {
        
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确认删除此地址吗?" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
            
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确定" Click:^{
            [self deleteAddress:addressID];

        } type:JCAlertViewTypeDefault];
    };
    cell.checkAddress = ^(int addressID) {
        [self checkAddress:addressID];
    };
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.setFinish) {
        AddressModel *model = self.dataArr[indexPath.section];
        self.setFinish(model.addressId);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
