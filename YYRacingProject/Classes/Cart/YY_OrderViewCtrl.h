//
//  YY_OrderViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"
#import "PreOrderModel.h"

@interface YY_OrderViewCtrl : YYBaseViewController

@property (nonatomic, strong) PreOrderModel *preOrderModel;

@property (nonatomic, strong) NSMutableArray *preOrderSkuArr;

@property (nonatomic, assign) int source;

@property (nonatomic, strong) NSMutableArray *finishIdArr;

@property (nonatomic, strong) NSMutableArray *operationArr;


@end
