//
//  YY_OrderUnpaidViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_OrderUnpaidViewCtrl.h"
#import "OrderCommonCell.h"
#import "ChannerCell.h"
#import "PreviewPayRequestAPI.h"
#import "payOrderRequestAPI.h"
#import "QueryPayRequestAPI.h"
#import "YY_OrderCompleteViewCtrl.h"
#import "groomGoodsModel.h"

@interface YY_OrderUnpaidViewCtrl ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImageView *headerView;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) UIButton  *timeSubmitBtn;

@property (nonatomic, strong) NSMutableArray *channelArr;
@property (nonatomic, strong) NSMutableArray *summaryList;
@property (nonatomic, strong) NSNumber  *systemTime;
@property (nonatomic, assign) NSInteger diffValue;
@property (nonatomic, assign) int   channelId;
@property (nonatomic, assign) int  payGateWay;
@property (nonatomic, strong) NSString  *PindID;

@property (nonatomic, assign) BOOL isMealZonePay;

@end

@implementation YY_OrderUnpaidViewCtrl

-(UITableView *)tableView
{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.backgroundColor = XHLightColor;
        self.tableView.estimatedRowHeight = 100;

        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isMealZonePay = YES;
    
    [self xh_addTitle:@"立即支付"];
    [self xh_popTopRootViewController:NO];
    [self creatHeader];
    [self getPreviewPay];

    self.tableView.tableHeaderView = self.headerView;
    [self.view addSubview:self.tableView];
 
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(50));
    }];
    Class common = [OrderCommonCell class];
    registerClass(self.tableView, common);
    Class channel = [ChannerCell class];
    registerClass(self.tableView, channel);
    [self creatFooter];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:OYCountDownNotification object:nil];
    [kCountDownManager start];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(InfoNotificationAction:) name:@"wxpost" object:nil];
    // Do any additional setup after loading the view.
}
// Do any additional setup after loading the view.
- (void)InfoNotificationAction:(NSNotification *)notification
{
    [self checkOrderWith:self.orderID pingId:self.PindID payStatus:1];
}
#pragma mark -Event
- (void)countDownNotification {
    NSInteger timeInterval = [kCountDownManager timeIntervalWithIdentifier:@"payUnpaid"];
    
    if (self.diffValue > 0) {
        NSInteger count =  self.diffValue - timeInterval;
        int seconds = count % 60;
        int minutes = (count / 60) % 60;
        NSString *timeFormat = nil;
        if (count > 0) {
            timeFormat = [NSString stringWithFormat:@"去支付(%02d:%02d)", minutes, seconds];
        }
        else {
            timeFormat = @"去支付";
        }
        [self.timeSubmitBtn setTitle:timeFormat forState:UIControlStateNormal];
        [self.timeSubmitBtn setTitle:timeFormat forState:UIControlStateSelected];
    }
}

-(void)creatFooter{
    @weakify(self);
    self.footerView = [YYCreateTools createView:XHClearColor];
    [self.view addSubview:self.footerView];
    
    [self.footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.tableView);
        make.top.equalTo(self.tableView.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
//    self.footerView.backgroundColor = [UIColor addGoldGradient:CGSizeMake(kScreenW, 50)];
    self.footerView.backgroundColor = XHMainColor;

    
    
    self.timeSubmitBtn = [YYCreateTools createBtn:@"" font:normalFont(17) textColor:XHWhiteColor];
    [self.footerView addSubview:self.timeSubmitBtn];
    [self.timeSubmitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(0);
    }];
    
    self.timeSubmitBtn.actionBlock = ^(UIButton *sender) {
        [weak_self toPayOrder];
    };}


-(void)getPreviewPay{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLong:self.orderID] forKey:@"orderId"];
    
    [YYCenterLoading showCenterLoading];
    PreviewPayRequestAPI *api = [[PreviewPayRequestAPI alloc] initPreviewPayRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.summaryList = [NSMutableArray array];
            self.summaryList =[[NSArray modelArrayWithClass:[OrderPriceModel class] json:[responDict objectForKey:@"summaryList"]] mutableCopy];
            self.channelArr = [[NSArray modelArrayWithClass:[ChannelModel class] json:[responDict objectForKey:@"channel"]] mutableCopy];
            
//            //判断是否是饭卡支付
//            if (self.isMealZonePay) {
//                ChannelModel *model = [[ChannelModel alloc] init];
//                model.channelId = 0;
//                model.channelUrl = @"meal_zone_pay_icon";
//                model.channelName = @"饭卡支付";
//                model.info = @"蜀黎之家独家支付";
//                model.defaultFlag = 1;
//                self.channelArr = [NSMutableArray arrayWithObject:model];
//            }
            
            for (ChannelModel *model in self.channelArr) {
                if (model.defaultFlag==1) {
                    self.channelId = model.channelId;
                }
            }
            [kCountDownManager addSourceWithIdentifier:@"payUnpaid"];
            self.systemTime = [responDict objectForKey:@"systemTime"];
            NSInteger endTime = [[responDict objectForKey:@"payDeadlineTime"] integerValue]/1000;
            NSInteger systemTime = [self.systemTime integerValue]/1000;
            self.diffValue = endTime - systemTime;
            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


-(void)toPayOrder{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLong:self.orderID] forKey:@"orderId"];
    [dict setObject:@(self.channelId) forKey:@"channelId"];
    [YYCenterLoading showCenterLoading];
    payOrderRequestAPI *api = [[payOrderRequestAPI alloc] initOrderPayRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.payGateWay = [[responDict objectForKey:@"payGateway"] intValue];
            self.PindID =[responDict objectForKey:@"pingId"];

            if (self.payGateWay ==4 ) {
                [YYCommonTools WXPay:self chargeJason:[responDict objectForKey:@"charge"] block:^(NSString *result) {
                }];
            }else{
                [YYCommonTools pingPay:self chargeJason:[responDict objectForKey:@"charge"] block:^(NSString *result) {
                    if ([result isEqualToString:@"success"]) {
                        [self checkOrderWith:self.orderID pingId:[responDict objectForKey:@"pingId"] payStatus:1];
                    }
                }];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark 确认订单
-(void)checkOrderWith:(long)orderId pingId:(NSString *)pingId payStatus:(int)payStatus
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLong:orderId] forKey:@"orderId"];
    [dict setObject:@(payStatus) forKey:@"appPayStatus"];
    [dict setObject:pingId forKey:@"pingId"];
    [dict setObject:@(self.payGateWay) forKey:@"payGateway"];
    
    [YYCenterLoading showCenterLoading];
    QueryPayRequestAPI *api = [[QueryPayRequestAPI alloc] initQueryPayRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            
            int status = [[responDict objectForKey:@"payStatus"] intValue];
            if (status == 1) {
                int type =[[responDict objectForKey:@"orderType"] intValue];
                //商品数组
                NSMutableArray *itemArr = [[NSMutableArray modelArrayWithClass:[groomGoodsModel class] json:[responDict objectForKey:@"items"]]mutableCopy];
                //地址信息
                NSDictionary *addrDic = [responDict objectForKey:@"addr"];
                
                int displayType = [[responDict objectForKey:@"displayType"] intValue];;
                
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:itemArr,@"item",addrDic,@"add",@(displayType),@"display", nil];
                
                [YYCommonTools skipOrderComplete:self params:[dic mutableCopy] type:type isSelfGetOrder:NO];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

- (void)creatHeader{
    
    //orderComplete_view
    self.headerView = [YYCreateTools createImageView:@"" viewModel:-1];
    self.headerView.backgroundColor = XHMainColor;
    self.headerView.frame = CGRectMake(0, 0, kScreenW, 87);

    UIImageView *icon = [YYCreateTools createImageView:@"noPaid_icon" viewModel:-1];
    [self.headerView addSubview:icon];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(129));
        make.top.mas_equalTo(24);
        make.width.height.mas_equalTo(16);
    }];
    
    UILabel *titleLable= [YYCreateTools createLabel:@"订单未支付" font:normalFont(18) textColor:XHWhiteColor];
    [self.headerView addSubview:titleLable];
    
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(icon.mas_right).mas_offset(10);
        make.centerY.equalTo(icon);
    }];
    UILabel *detailLabel = [YYCreateTools createLabel:@"请在30分钟内完成订单，订单超时将自动关闭哦~" font:normalFont(12) textColor:XHWhiteColor];
    [self.headerView addSubview:detailLabel];

    [detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headerView);
        make.top.mas_equalTo(titleLable.mas_bottom).mas_offset(12);
    }];
}

#pragma mark tabeview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return  self.summaryList.count;
    }
    return self.channelArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        Class common = [OrderCommonCell class];
        OrderCommonCell *commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!commonCell) {
            commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        commonCell.type = CellWithTwoLabel;
        commonCell.priceModel = self.summaryList[indexPath.row];
        return commonCell;
    }else{
        Class channel = [ChannerCell class];
        ChannerCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(channel)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(channel)];
            cell.backgroundColor = XHWhiteColor;
        }
        cell.model = self.channelArr[indexPath.row];
        return cell;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *sectionView = [YYCreateTools createView:XHLightColor];

    sectionView.frame = CGRectMake(0, 0, kScreenW, 40);
    UIView *whiteView =[YYCreateTools createView:XHWhiteColor];
    [sectionView addSubview:whiteView];
    whiteView.frame = CGRectMake(0, 8, kScreenW, 32);
        
    UILabel *title = [YYCreateTools createLabel:section == 0?@"费用详情":@"支付方式" font:normalFont(13) textColor:XHBlackLitColor];
    [whiteView addSubview:title];
    [title mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.bottom.mas_equalTo(whiteView.mas_bottom).mas_equalTo(-4);
        }];

    return sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        for (int i = 0; i < self.channelArr.count; i++) {
            ChannelModel *model = self.channelArr[i];
            if (i == indexPath.row) {
                model.defaultFlag = 1;
                self.channelId = model.channelId;
            }else{
                model.defaultFlag = 0;
            }
        }
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
