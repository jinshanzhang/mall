//
//  GoodDetailCouponView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailCouponView.h"
#import "ChooseOrderCouponCell.h"
#import "ReceiveCouponRequestAPI.h"
#import "CouponListInfoModel.h"
#import "GoodCouponCell.h"

@interface GoodDetailCouponView ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *subTableView;

@end

@implementation GoodDetailCouponView

- (instancetype)initCouponView {
    self = [super init];
    if (self) {
        @weakify(self);
        self.backgroundColor = XHMainLightColor;
        self.frame = CGRectMake(0, kScreenH, kScreenW, kSizeScale(450)+kSafeBottom);
        [self setUp];
        
        self.dissClickBlock = ^{
            @strongify(self);
            if (self.callBack) {
                self.callBack(self.CouponArr);
            }
            [self showViewOfAnimateType:YYPopAnimateDownUp];
        };
    }
    return self;
}


-(void)setUp{
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"领券";
    titleLabel.font = boldFont(15);
    titleLabel.textColor = XHBlackColor;
    [self addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(15);
    }];
    
    UIButton *closebtn = [YYCreateTools createBtnImage:@"goodCoupon_close_icon"];
    [self addSubview:closebtn];
    [closebtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel);
        make.right.mas_equalTo(-20);
    }];
    closebtn.actionBlock = ^(UIButton *sender) {
        if (self.callBack) {
            self.callBack(self.CouponArr);
        }
        [self showViewOfAnimateType:YYPopAnimateDownUp];
    };

    self.subTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 51, kScreenW, self.height-kBottom(kSizeScale(50))-50)];
    [self addSubview:_subTableView];
    self.subTableView.backgroundColor = XHMainLightColor;
    _subTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _subTableView.delegate = self;
    _subTableView.dataSource = self;
    _subTableView.estimatedRowHeight = 100;

    Class common = [GoodCouponCell class];
    registerClass(self.subTableView, common);

    UIButton *confirmBtn =[YYCreateTools createBtn:@"完 成" font:normalFont(17) textColor:XHWhiteColor];
    [self addSubview:confirmBtn];
    confirmBtn.backgroundColor = XHLoginColor;

    confirmBtn.actionBlock = ^(UIButton *sender) {
        if (self.callBack) {
            self.callBack(self.CouponArr);
        }
        [self showViewOfAnimateType:YYPopAnimateDownUp];
    };
    
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kSafeBottom);
        make.height.mas_equalTo(kSizeScale(50));
    }];
}

-(void)setCouponArr:(NSMutableArray *)CouponArr{
    if (!_CouponArr) {
        _CouponArr = CouponArr ;
    }
    [self.subTableView reloadData];
}

#pragma mark - TableViewDatasouce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _CouponArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class common = [GoodCouponCell class];
    GoodCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    cell.backgroundColor = XHMainLightColor;
    cell.model = [CouponListInfoModel modelWithJSON:self.CouponArr[indexPath.row]];
    cell.receiveBlock = ^(NSString *couponKey) {
        [self receiveCouponWith:couponKey row:indexPath.row];
    };
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)receiveCouponWith:(NSString *)couponKey row:(NSInteger)row {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if (kValidString(couponKey)) {
        [dict setObject:couponKey forKey:@"couponKey"];
    }
    ReceiveCouponRequestAPI *api = [[ReceiveCouponRequestAPI alloc] initRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            int status = [[responDict objectForKey:@"status"] intValue];
            if (status == 1) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.CouponArr[row]];
                NSInteger hold = [[dic objectForKey:@"holdNum"] integerValue];
                hold = hold + 1;
                NSInteger total = [[dic objectForKey:@"totalNum"] integerValue];
                total = total + 1;
                NSInteger status = [[dic objectForKey:@"limitStatus"] integerValue];
                if (total < [[dic objectForKey:@"limitNum"] integerValue]) {
                    status = 1;
                }else{
                    status = 3;
                }
                [dic setValue:@(status) forKey:@"limitStatus"];
                [dic setValue:@(hold) forKey:@"holdNum"];
                [dic setValue:@(total) forKey:@"totalNum"];
                [dic setValue:@(status) forKey:@"limitStatus"];

                [self.CouponArr replaceObjectAtIndex:row withObject:dic];
                [self.subTableView reloadData];
                [YYCommonTools showTipMessage:[responDict objectForKey:@"log"]];
            }else if (status == 7){
                NSDictionary *dic = [responDict objectForKey:@"image"];
                HomeBannerInfoModel *model = [[HomeBannerInfoModel alloc] init];
                model.imageUrl = [dic objectForKey:@"imgUrl"];
                model.width = [[dic objectForKey:@"width"] floatValue];
                model.height = [[dic objectForKey:@"height"] floatValue];
                model.url = [responDict objectForKey:@"shopUrl"];
                model.HideBottom= YES;
                if (self.receiveFinish) {
                    self.receiveFinish(model);
                 }
            }else if (status == 2){
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.CouponArr[row]];
                NSInteger status = [[dic objectForKey:@"limitStatus"] integerValue];
                NSInteger hold = [[dic objectForKey:@"holdNum"] integerValue];
                if (hold == 0) {
                    status = 2;
                }else{
                    status = 3;
                }
                [dic setValue:@(status) forKey:@"limitStatus"];
                [self.CouponArr replaceObjectAtIndex:row withObject:dic];
                [self.subTableView reloadData];
                [YYCommonTools showTipMessage:[responDict objectForKey:@"log"]];
            }
            else{
                [YYCommonTools showTipMessage:[responDict objectForKey:@"log"]];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

@end
