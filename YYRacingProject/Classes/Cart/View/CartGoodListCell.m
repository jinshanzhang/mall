//
//  CartGoodListCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CartGoodListCell.h"


@implementation CartGoodListCell


-(void)createUI{
    @weakify(self);

    //*********商品图***********
    self.iconImg = [YYCreateTools createImageView:@"" viewModel:-1];
//    self.iconImg.backgroundColor = XHRedColor;
    self.iconImg.v_cornerRadius = 2;
    [self addSubview:self.iconImg];
    
    [self.iconImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(40));
        make.top.mas_equalTo(12);
        make.height.width.mas_equalTo(kSizeScale(92));
        make.bottom.mas_equalTo(-12);
    }];
    
    //*********名称***********
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
    self.titleLabel.font = normalFont(14);
    self.titleLabel.textColor = XHBlackColor;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.numberOfLines = 2;
    [self.titleLabel sizeToFit];
    [self.titleLabel setContentMode:UIViewContentModeTop];
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(kSizeScale(-12));
        make.left.mas_equalTo(kSizeScale(145));
    }];
    
    //*********简介***********
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    self.detailLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.detailLabel];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).mas_equalTo(7);
        make.right.mas_equalTo(kSizeScale(-12));
        make.left.mas_equalTo(kSizeScale(145));
    }];

    //*********价格***********
    self.moneyLabel = [YYCreateTools createLabel:@"" font:normalFont(15) textColor:XHRedColor];
    [self addSubview:self.moneyLabel];
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(145));
        make.bottom.equalTo(self.iconImg.mas_bottom);
    }];

    //*********勾选***********
    self.selectBtn = [YYCreateTools createBtnImage:@"addressButton_icon"];
    self.selectBtn.selected = NO;
    [self.selectBtn setImage:[UIImage imageNamed:@"addressButtonSelected_icon"] forState:UIControlStateSelected];
    [self addSubview:self.selectBtn];
    
    [self.selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(40);
    }];
    self.selectBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        self.skuModel.select = !self.selectBtn.selected;
        self.skuModel.pitchOn = self.skuModel.select;
        self.selectBtn.selected = !self.selectBtn.selected;
        NSLog(@"%@",self.selectBtn.selected == YES?@"YES":@"NO");

        if (self.cellClickBlock) {
            self.cellClickBlock(self.selectBtn.selected == NO?[self.skuModel.skuPrice floatValue]
                                *self.skuModel.skuCnt:-[self.skuModel.skuPrice floatValue]
                                *self.skuModel.skuCnt,self.skuModel);
        }
    };

    self.invalidLabel = [YYCreateTools createLabel:@"失效" font:normalFont(9) textColor:XHWhiteColor];
    self.invalidLabel.v_cornerRadius = kSizeScale(8);
    self.invalidLabel.backgroundColor = XHGrayColor;
    self.invalidLabel.hidden = YES;
    self.invalidLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.invalidLabel];

    [self.invalidLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconImg);
        make.left.mas_equalTo(kSizeScale(6));
        make.right.mas_equalTo(self.iconImg.mas_left).mas_equalTo(-kSizeScale(6));
        make.width.mas_equalTo(kSizeScale(28));
        make.height.mas_equalTo(kSizeScale(16));
    }];
    
    self.numberView = [[YYNumberView alloc] initWithFrame:CGRectZero];
    self.numberView.minValue = 1;
    self.numberView.isOpenTipMsg = YES;
    self.numberView.isOpenTenLimit = YES;
    [self addSubview:self.numberView];
    [self.numberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(kSizeScale(-12));
        make.bottom.equalTo(self.iconImg.mas_bottom);
        make.width.mas_equalTo(kSizeScale(85));
        make.height.mas_equalTo(kSizeScale(25));
    }];
    
    self.numberView.addClick = ^(NSInteger number) {
        @strongify(self)
        self.skuModel.skuCnt++;
        if (self.cellNumerBlock) {
            self.cellNumerBlock(1, self.skuModel);
        }
        if (self.selectBtn.selected == NO) {
            [self NumberCountMoney_money:[self.skuModel.skuPrice floatValue]];
        }
    };

    self.numberView.reduceClick = ^(NSInteger number) {
        @strongify(self)
        self.skuModel.skuCnt = (int)number;
        if (self.cellNumerBlock) {
            self.cellNumerBlock(-1, self.skuModel);
        }
        if (self.selectBtn.selected == NO) {
            [self NumberCountMoney_money:-[self.skuModel.skuPrice floatValue]];
        }
    };
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(self);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(1);
    }];
}

-(void)NumberCountMoney_money:(NSInteger)money
{
    if (self.cellClickBlock) {
    self.cellClickBlock(self.selectBtn.selected == NO?-money:money,self.skuModel);
    }
}

-(void)setSkuModel:(CartSkuModel *)skuModel{
    _skuModel = skuModel;
//    _skuModel.select = skuModel.pitchOn;
    [self.iconImg sd_setImageWithURL:[NSURL URLWithString:skuModel.imageUrl?:skuModel.skuCover] placeholderImage:nil];

    self.titleLabel.text = skuModel.skuTitle;
    self.detailLabel.text = skuModel.skuProp;
    
    NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(12) symbolTextColor:XHRedColor wordSpace:1 price:skuModel.skuPrice priceFont:normalFont(15) priceTextColor:XHRedColor symbolOffsetY:0];
    self.moneyLabel.attributedText = attribut;
    UIColor *statusColor;
    if (skuModel.skuStatus == 1) {
        statusColor = XHBlackLitColor;
        self.invalidLabel.hidden = NO;
        self.selectBtn.hidden = YES;
        self.numberView.hidden = YES;
    }else{
        statusColor = XHBlackColor;
        self.invalidLabel.hidden = YES;
        self.selectBtn.hidden = NO;
        self.numberView.hidden = NO;
    }
    self.titleLabel.textColor = statusColor;
    self.detailLabel.textColor = XHBlackLitColor;
    self.moneyLabel.textColor = skuModel.skuStatus == 1?statusColor:XHRedColor;
    self.selectBtn.selected = skuModel.pitchOn;
    
    self.max = skuModel.totalCnt<skuModel.limitCnt?skuModel.totalCnt:skuModel.limitCnt;
    self.numberView.maxValue = self.max;
    self.numberView.currentValue = skuModel.skuCnt;
}





@end
