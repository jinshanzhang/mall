//
//  ChannerCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ChannerCell.h"

@implementation ChannerCell


-(void)createUI{
    self.channelIcon = [YYCreateTools createImageView:@"" viewModel:-1];
    [self addSubview:_channelIcon];
    
    [self.channelIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(12);
        make.bottom.mas_equalTo(-12);
        make.width.height.mas_equalTo(kSizeScale(36));
    }];
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:boldFont(14) textColor:XHBlackColor];
    [self addSubview:_titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.channelIcon.mas_right).mas_offset(kSizeScale(12));
//        make.top.mas_equalTo(12);
        make.bottom.equalTo(self.mas_centerY).offset(-3.75);
        make.height.mas_equalTo(14);
    }];
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:MediumFont(12) textColor:XHBlackLitColor];
    [self addSubview:_detailLabel];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.channelIcon.mas_right).mas_offset(kSizeScale(12));
//        make.bottom.equalTo(self.channelIcon);
        make.top.equalTo(self.mas_centerY).offset(3.75);
        make.height.mas_equalTo(12);
    }];
    
    self.statusBtn = [YYCreateTools createImageView:@"mine_cellButtonSelected_icon" viewModel:-1];
    [self addSubview:self.statusBtn];
    
    [self.statusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.channelIcon);
        make.right.mas_equalTo(-12);
    }];
    
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
}


-(void)setModel:(ChannelModel *)model{
    _model = model;
    if([model.channelUrl rangeOfString:@"http"].location != NSNotFound) {
       [self.channelIcon sd_setImageWithURL:[NSURL URLWithString:model.channelUrl]];
    } else {
        self.channelIcon.image = [UIImage imageNamed:model.channelUrl];
    }
    self.titleLabel.text = model.channelName;
    self.detailLabel.text = model.info;
    if (model.defaultFlag == 1) {
        [self.statusBtn setImage:[UIImage imageNamed:@"addressButtonSelected_icon"]];
    }else{
        [self.statusBtn setImage:[UIImage imageNamed:@"addressButton_icon"]];
    }
}





@end
