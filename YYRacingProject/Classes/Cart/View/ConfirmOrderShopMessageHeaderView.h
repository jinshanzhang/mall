//
//  ConfirmOrderShopMessageHeaderView.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/6.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseView.h"
#import "PreOrderModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface ConfirmOrderShopMessageHeaderView : BaseView

@property (nonatomic, strong) SellerShopModel * model;

@end

NS_ASSUME_NONNULL_END
