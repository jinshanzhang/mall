//
//  PreOrderAddressCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "AddressModel.h"

@interface PreOrderAddressCell : YYBaseTableViewCell

@property (nonatomic, strong) AddressModel *model;
@property (nonatomic, assign) int   hasAddr;

@end
