//
//  OrderGoodListCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderGoodListCell.h"

@implementation OrderGoodListCell

-(void)createUI{    
    //*********商品图***********
    self.iconImg = [YYCreateTools createImageView:@"" viewModel:-1];
    self.iconImg.v_cornerRadius = 2;
    [self addSubview:self.iconImg];
    
    [self.iconImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(12);
        make.height.width.mas_equalTo(kSizeScale(80));
        make.bottom.mas_equalTo(-12);
    }];
    //*********名称***********
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
    self.titleLabel.font = normalFont(14);
    self.titleLabel.textColor = XHBlackColor;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.numberOfLines = 2;
    [self.titleLabel sizeToFit];
    [self.titleLabel setContentMode:UIViewContentModeTop];
    
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(kSizeScale(-12));
        make.left.mas_equalTo(self.iconImg.mas_right).mas_offset(kSizeScale(12));
    }];
    
    //*********简介***********
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    self.detailLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.detailLabel];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).mas_equalTo(5);
        make.left.mas_equalTo(self.titleLabel.mas_left);
        make.right.mas_equalTo(kSizeScale(-24));
    }];
    
    //*********价格***********
    self.moneyLabel = [YYCreateTools createLabel:@"" font:boldFont(9) textColor:XHBlackColor];
    [self addSubview:self.moneyLabel];
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.titleLabel.mas_left);
        make.bottom.equalTo(self.iconImg.mas_bottom);
    }];
    
    //*********数量***********
    self.countLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
    [self addSubview:self.countLabel];
    
    [self.countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.detailLabel.mas_bottom);
        make.right.mas_equalTo(kSizeScale(-12));
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
}


-(void)setSkuModel:(CartSkuModel *)skuModel{
    _skuModel = skuModel;
    [self.iconImg sd_setImageWithURL:[NSURL URLWithString:skuModel.imageUrl?:skuModel.skuCover] placeholderImage:nil];
    
    self.titleLabel.text = skuModel.skuTitle;
    self.detailLabel.text = skuModel.skuProp;
    
    NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(9) symbolTextColor:XHBlackColor wordSpace:1 price:[skuModel.price objectForKey:@"priceTag"]  priceFont:boldFont(12) priceTextColor:XHBlackColor symbolOffsetY:0];
    self.moneyLabel.attributedText = attribut;
    
    self.countLabel.text = [NSString stringWithFormat:@"×%d",skuModel.skuCnt];
}


@end
