//
//  OrderSkuTitleCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface OrderSkuTitleCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@end
