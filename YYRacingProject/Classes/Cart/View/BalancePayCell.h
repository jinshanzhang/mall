//
//  BalancePayCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "BalanceModel.h"
@interface BalancePayCell : YYBaseTableViewCell

@property (nonatomic, strong) BalanceModel *balanceModel;

@property (nonatomic, strong) UILabel      *titleLabel;

@property (nonatomic, strong) UILabel      *detailLabel;

@property (nonatomic, strong) UILabel      *amountLabel;

@property (nonatomic, strong) UISwitch     *chooseSwich;

@property (nonatomic, copy) void (^cellClickBlock)(BOOL swich);


@end
