//
//  AddressListCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AddressListCell.h"

@implementation AddressListCell

-(void)createUI{

    self.titleLabel = [YYCreateTools createLabel:@"dashuaige" font:normalFont(15) textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(16));
    }];
    
    self.phoneLabel = [YYCreateTools createLabel:@"123123123123" font:normalFont(15) textColor:XHBlackColor];
    [self.contentView addSubview:self.phoneLabel];
    
    [self.phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(kSizeScale(-10));
    }];
    
    self.addressLabel = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHBlackLitColor];
    self.addressLabel.numberOfLines = 2;
    [self.contentView addSubview:self.addressLabel];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(12);
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(kSizeScale(-16));
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.addressLabel.mas_bottom).mas_offset(12);
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.height.mas_equalTo(1);
    }];
    
    self.setDefaultBtn = [YYCreateTools createBtn:@" 设为默认地址" font:normalFont(13) textColor:XHBlackColor];
    [self.contentView addSubview:self.setDefaultBtn];
    [self.setDefaultBtn setImage:[UIImage imageNamed:@"addressButtonSelected_icon"] forState:UIControlStateNormal];
    
    [self.setDefaultBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(12);
        make.left.mas_equalTo(kSizeScale(16));
        make.height.mas_equalTo(20);
        make.bottom.mas_equalTo(-12);
    }];
    @weakify(self);
    self.setDefaultBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        if (self.model.defaultFlag == 1) {
            return;
        }
        if (self.changeStatus) {
            self.changeStatus(self.model.addressId);
        }
    };
    
    self.deleteBtn = [YYCreateTools createBtn:@" 删除" font:normalFont(13) textColor:XHBlackColor];
    [self.contentView addSubview:self.deleteBtn];
    [self.deleteBtn setImage:[UIImage imageNamed:@"addressDelete_icon"] forState:UIControlStateNormal];
    
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(kSizeScale(-16));
        make.height.mas_equalTo(20);
        make.bottom.mas_equalTo(-12);
    }];
    
    self.deleteBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        
        if (self.model.defaultFlag == 1) {
            [YYCommonTools showTipMessage:@"默认地址不可删除"];
            return;
        }
        if (self.deleteAddress) {
            self.deleteAddress(self.model.addressId);
        }
    };
    
    
    self.editBtn = [YYCreateTools createBtn:@" 编辑" font:normalFont(13) textColor:XHBlackColor];
    [self.contentView addSubview:self.editBtn];
    [self.editBtn setImage:[UIImage imageNamed:@"addressEdit_icon"] forState:UIControlStateNormal];
    
    [self.editBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(lineView.mas_bottom).mas_offset(12);
        make.right.mas_equalTo(self.deleteBtn.mas_left).mas_offset(-kSizeScale(30));
        make.height.mas_equalTo(20);
        make.bottom.mas_equalTo(-12);
    }];
    
    self.editBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        if (self.checkAddress) {
            self.checkAddress(self.model.addressId);
        }
    };
    
    
}


-(void)setModel:(AddressModel *)model{
    _model = model;
    
    self.titleLabel.text = model.contactName;
    self.addressLabel.text = model.addressInfo;
    self.phoneLabel.text = model.phone;
    if (model.defaultFlag == 1) {
        [self.setDefaultBtn setImage:[UIImage imageNamed:@"addressButtonSelected_icon"] forState:UIControlStateNormal];
    }else{
        [self.setDefaultBtn setImage:[UIImage imageNamed:@"addressButton_icon"] forState:UIControlStateNormal];
    }
}





@end
