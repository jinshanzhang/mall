//
//  OrderSkuTitleCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderSkuTitleCell.h"

@implementation OrderSkuTitleCell

-(void)createUI{

    UIView *view = [YYCreateTools createView:XHLoginColor];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(19);
        make.bottom.mas_equalTo(-19);
        make.width.height.mas_equalTo(kSizeScale(4));
    }];

    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHBlackColor];
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(view.mas_right).mas_offset(10);
        make.centerY.equalTo(view);
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
}

@end
