//
//  ChooseCoupon.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCoupon : UIView

@property (nonatomic, copy) void(^close)(void);

@property (nonatomic, strong) NSMutableArray *validCouponList;

@property (nonatomic, strong) NSMutableArray *invalidCouponList;

@property (nonatomic, copy) void(^chooseFinish)(NSMutableArray *idArr);

@end
