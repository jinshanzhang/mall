//
//  DCPaymentView.m
//  DCPayAlertDemo
//
//  Created by dawnnnnn on 15/12/9.
//  Copyright © 2015年 dawnnnnn. All rights reserved.
//

#import "DCPaymentView.h"
#import "ConfirmPayPasswordAPI.h"
#import "YY_PaymentPasswordViewController.h"

#define TITLE_HEIGHT 46
#define PAYMENT_WIDTH [UIScreen mainScreen].bounds.size.width-80
#define PWD_COUNT 6
#define DOT_WIDTH 10
#define KEYBOARD_HEIGHT 216
#define KEY_VIEW_DISTANCE 200
#define ALERT_HEIGHT 170
#define TagValue 200

@interface DCPaymentView ()<UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *pwdIndicatorArr;
}
@property (nonatomic, strong) UITextField *pwdTextField;

@property (nonatomic, strong) UIView *paymentAlert, *inputView;
@property (nonatomic, strong) UIButton *closeBtn;
@property (nonatomic, strong) UILabel *titleLabel, *line, *detailLabel, *amountLabel;
@property (nonatomic, strong) UIWindow *showWindow;
@property (nonatomic, copy)   NSString *password;

@end

@implementation DCPaymentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        [self drawView];
    }
    return self;
}

- (void)drawView {
    
    if (!_paymentAlert) {
        _paymentAlert = [[UIView alloc]initWithFrame:CGRectMake(60, kSizeScale(400), [UIScreen mainScreen].bounds.size.width-120, ALERT_HEIGHT)];
        _paymentAlert.layer.cornerRadius = 4.f;
        _paymentAlert.layer.masksToBounds = YES;
        _paymentAlert.backgroundColor = XHWhiteColor;
        [self addSubview:_paymentAlert];
        
        _titleLabel = [YYCreateTools createLabel:@"" font:boldFont(15) textColor:XHBlackColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [_paymentAlert addSubview:_titleLabel];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kSizeScale(24));
            make.centerX.equalTo(self->_paymentAlert);
        }];
        
        self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackMidColor];
        _detailLabel.frame = CGRectMake(0, 50, _paymentAlert.width, 20);
        _detailLabel.textAlignment = NSTextAlignmentCenter;
        [_paymentAlert addSubview:_detailLabel];
        
        
        self.closeBtn = [YYCreateTools createBtn:@"X" font:normalFont(14) textColor:XHBlackColor];
        [_paymentAlert addSubview:_closeBtn];
        
        [_closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.top.mas_equalTo(10);
            make.width.height.mas_equalTo(15);
        }];
        [_closeBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        

        self.inputView = [YYCreateTools createView:XHWhiteColor];
        self.inputView.frame = CGRectMake(kSizeScale(15), self.detailLabel.originY+self.detailLabel.height+10, self.paymentAlert.width-kSizeScale(30), 35);
        _inputView.layer.borderWidth = 1.f;
        _inputView.layer.borderColor = HexRGB(0xE7E6E6).CGColor;
        [_paymentAlert addSubview:_inputView];
        
        
        
        pwdIndicatorArr = [[NSMutableArray alloc]init];
        _pwdTextField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        _pwdTextField.hidden = YES;
        _pwdTextField.delegate = self;
        _pwdTextField.keyboardType = UIKeyboardTypeNumberPad;
        [_inputView addSubview:_pwdTextField];
        
        [_pwdTextField addTarget:self action:@selector(loginInputPasswordStrInputBegin) forControlEvents:UIControlEventEditingDidBegin];
        [_pwdTextField addTarget:self action:@selector(loginInputPasswordStrInputEnd) forControlEvents:UIControlEventEditingDidEnd];

        CGFloat width = self.inputView.width/PWD_COUNT;
        for (int i = 0; i < PWD_COUNT; i ++) {
            UILabel *dot = [[UILabel alloc]initWithFrame:CGRectMake((width-DOT_WIDTH)/2.f + i*width, (_inputView.bounds.size.height-DOT_WIDTH)/2.f, DOT_WIDTH, DOT_WIDTH)];
            dot.backgroundColor = [UIColor blackColor];
            dot.layer.cornerRadius = DOT_WIDTH/2.;
            dot.clipsToBounds = YES;
            dot.hidden = YES;
            [_inputView addSubview:dot];
            [pwdIndicatorArr addObject:dot];
                if (i == PWD_COUNT-1) {
                continue;
            }
            UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake((i+1)*width, 0, .5f, _inputView.bounds.size.height)];
            line.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1.];
            [_inputView addSubview:line];
        }
        UIButton *forgetBtn = [YYCreateTools createBtn:@"忘记密码?" font:normalFont(12) textColor:XHBlueColor];
        [_paymentAlert addSubview:forgetBtn];

        [forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.inputView.mas_bottom).mas_offset(10);
            make.centerX.equalTo(self.paymentAlert);
        }];
        
        forgetBtn.actionBlock = ^(UIButton *sender) {
            [self dismiss];
            YYBaseViewController *ctrl = (YYBaseViewController *)[YYCommonTools getCurrentVC];
            YY_PaymentPasswordViewController *vc = [YY_PaymentPasswordViewController new];
            [ctrl.navigationController pushViewController:vc animated:YES];
        };
        
    
     }
}

-(void)submitCode{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[self.password encode:self.password] forKey:@"password"];
    
    ConfirmPayPasswordAPI *API = [[ConfirmPayPasswordAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
                if (self.completeHandle) {
                self.completeHandle([responDict objectForKey:@"passwordToken"]);
                }
                [self performSelector:@selector(dismiss) withObject:nil afterDelay:.3f];
        }else{
            self.pwdTextField.text = @"";
            [self setDotWithCount:0];
        }
    }failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


- (void)show {
    if (self.superview) {
        [self removeFromSuperview];
    }
    UIView *oldView = [[UIApplication sharedApplication].keyWindow viewWithTag:TagValue];
    if (oldView) {
        [oldView removeFromSuperview];
    }
    UIView *iview = [[UIView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    iview.tag = TagValue;
    iview.userInteractionEnabled = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideAlertView)];
    [iview addGestureRecognizer:tap];
    iview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [[UIApplication sharedApplication].keyWindow addSubview:iview];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.center = [UIApplication sharedApplication].keyWindow.center;
    self.transform = CGAffineTransformScale(self.transform,0.1,0.1);
    self.alpha = 1;
    [self.pwdTextField becomeFirstResponder];
    self.transform = CGAffineTransformIdentity;
}

-(void)loginInputPasswordStrInputBegin{
    _paymentAlert.frame =  CGRectMake(60, kSizeScale(200), [UIScreen mainScreen].bounds.size.width-120, ALERT_HEIGHT);
}

-(void)loginInputPasswordStrInputEnd{
    [UIView animateWithDuration:0.3f animations:^{
        self.paymentAlert.originY = kSizeScale(260);
    } completion:^(BOOL finished) {
    }];
}

- (void)dismiss {
    
    [_pwdTextField resignFirstResponder];
    if (self.superview) {
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 0;
        } completion:^(BOOL finished) {
            UIView *bgview = [[UIApplication sharedApplication].keyWindow viewWithTag:TagValue];
            if (bgview) {
                [bgview removeFromSuperview];
            }
            [self removeFromSuperview];
        }];
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.text.length >= PWD_COUNT && string.length) {
        //输入的字符个数大于6，则无法继续输入，返回NO表示禁止输入
        return NO;
    }
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",@"^[0-9]*$"];
    if (![predicate evaluateWithObject:string]) {
        return NO;
    }
    NSString *totalString;
    if (string.length <= 0)
    {
        if (textField.text.length ==0 ) {
            return NO;
        }
        totalString = [textField.text substringToIndex:textField.text.length-1];
    }
    else {
        totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    }
    [self setDotWithCount:totalString.length];
    self.password = totalString;
    NSLog(@"_____total %@",totalString);
    
    if (totalString.length == 6) {
        [self submitCode];
     }
    return YES;
}

- (void)setDotWithCount:(NSInteger)count {
    for (UILabel *dot in pwdIndicatorArr) {
        dot.hidden = YES;
    }
    for (int i = 0; i< count; i++) {
        ((UILabel*)[pwdIndicatorArr objectAtIndex:i]).hidden = NO;
    }
}

#pragma mark - 
- (void)setTitleStr:(NSString *)titleStr {
    _titleStr = titleStr;
    _titleLabel.text = _titleStr;
}

- (void)setDetail:(NSString *)detail {
    _detail = detail;
    _detailLabel.text = _detail;
}

- (void)setAmount:(CGFloat)amount {
    _amount = amount;
    _amountLabel.text = [NSString stringWithFormat:@"￥%.2f  ",amount];
}

@end
