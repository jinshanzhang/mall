//
//  OrderCommonCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderPriceModel.h"

@interface OrderCommonCell : UITableViewCell

typedef NS_ENUM(NSInteger , UseCellType) {
    CellwithOneTextFild = 0,     //
    CellWithTwoLabel = 1,
    CellWithTextView = 2,
    CellWithTwoLabelAndArrow = 3
};

@property (nonatomic, assign) UseCellType type;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UITextField  *showTextField;


@property (nonatomic, strong) YYTextView *showTextView;

@property (nonatomic, strong) UIImageView *arrowImageView;

@property (nonatomic, strong) UIView  *lineView;

@property (nonatomic, assign) int originX;

@property (nonatomic, strong) OrderPriceModel *priceModel;




@end
