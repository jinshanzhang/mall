//
//  OrderCommonCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderCommonCell.h"
#import "UITextView+Placeholder.h"

@implementation OrderCommonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

-(void)setType:(UseCellType)type{
    _type = type;
    
    for(UIView *view in self.contentView.subviews){
        if(view){
            [view removeFromSuperview];
        }
    }
    if (self.type == CellwithOneTextFild) {

        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.showTextField];
        self.titleLabel.font = normalFont(13);
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.top.mas_equalTo(15);
            make.bottom.mas_equalTo(-15);
        }];
        [self.showTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(96));
            make.centerY.equalTo(self.titleLabel);
            make.right.mas_equalTo(kSizeScale(-12));
        }];
    }else if (self.type == CellWithTwoLabel){
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.detailLabel];

        self.titleLabel.font = normalFont(12);
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.top.mas_equalTo(10);
            make.bottom.mas_equalTo(-10);
        }];
        
        self.detailLabel.font = normalFont(12);
        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(kSizeScale(-10));
            make.bottom.mas_equalTo(-10);
        }];
    }else if (self.type == CellWithTwoLabelAndArrow){
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.detailLabel];
        
        self.titleLabel.font = normalFont(15);
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.top.mas_equalTo(15);
            make.bottom.mas_equalTo(-15);
        }];
        
        self.arrowImageView = [YYCreateTools createImageView:@"cellMore_icon"
                                                   viewModel:-1];
        [self.contentView addSubview:self.arrowImageView];
        
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.mas_equalTo(-kSizeScale(9));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
        }];
        self.detailLabel.font = normalFont(15);
        self.detailLabel.textAlignment = NSTextAlignmentRight;

        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(85));
            make.right.mas_equalTo(kSizeScale(-20));
            make.bottom.mas_equalTo(-15);
        }];
    }
    else if (self.type == CellWithTextView){
        [self.contentView addSubview:self.titleLabel];
        self.titleLabel.font = normalFont(14);
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.top.mas_equalTo(10);
        }];
        [self.contentView addSubview:self.showTextView];
        [self.showTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(5);
            make.left.mas_equalTo(kSizeScale(92));
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(50);
        }];
    }
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

-(void)createUI{
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints =NO;
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
    [self.contentView addSubview:self.detailLabel];
    
    self.showTextField = [YYCreateTools createTextField:@"" font:normalFont(13) texColor:XHBlackColor];
    [self.contentView addSubview:_showTextField];
    
    self.showTextView = [[YYTextView alloc] init];
    self.showTextView.font = normalFont(14);
    self.showTextView.textColor = XHBlackColor;
    self.showTextView.backgroundColor = XHWhiteColor;
    self.showTextView.textAlignment  = NSTextAlignmentLeft;
    self.showTextView.returnKeyType = UIReturnKeyDone;

    [self.showTextView setContentOffset:CGPointZero];
    [self.contentView addSubview:self.showTextView];
}


-(void)setPriceModel:(OrderPriceModel *)priceModel{
    _priceModel = priceModel;
    self.titleLabel.text = _priceModel.title;
    self.detailLabel.text = _priceModel.value;
    self.detailLabel.textColor = [YYCommonTools hexStringToColor:_priceModel.color];
    self.detailLabel.font = _priceModel.bold == 1?boldFont(12):normalFont(12);
}

-(void)setOriginX:(int)originX{
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(originX);
    }];
}


@end
