//
//  ChooseOrderCouponCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "CouponListInfoModel.h"

@interface ChooseOrderCouponCell : YYBaseTableViewCell

@property (nonatomic, strong) UIView  *coupon_bgView;
@property (nonatomic, strong) UIImageView  *dotter_ImageView;
@property (nonatomic, strong) UIImageView  *overtime_ImageView;
@property (nonatomic, strong) UIImageView  *coupon_StatuImageView;
@property (nonatomic, strong) UIImageView  *lauchImageView;

@property (nonatomic, strong) UILabel      *couponPriceLabel;  // 优惠券折扣金额
@property (nonatomic, strong) UILabel      *conditionPriceLabel;// 优惠券使用范围
@property (nonatomic, strong) UILabel      *couponTitleLabel;
@property (nonatomic, strong) UILabel      *couponTimeLabel;
@property (nonatomic, strong) UILabel      *couponUnuseLabel;

@property (nonatomic, strong) UIView       *separateLine;

@property (nonatomic, strong) UILabel      *couponScopeLabel;  // 优惠券使用范围
@property (strong, nonatomic) UIImageView  *selectFlag;

@property (strong, nonatomic) UIButton     *receiveBtn;
@property (nonatomic, strong) UIImageView  *receiveIMG;

@property (nonatomic, strong) CouponListInfoModel  *model;

@property (nonatomic, copy) void(^receiveBlock)(NSString *couponKey);

@end
