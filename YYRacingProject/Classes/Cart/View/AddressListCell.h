//
//  AddressListCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "AddressModel.h"

@interface AddressListCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *phoneLabel;

@property (nonatomic, strong) UILabel *addressLabel;

@property (nonatomic, strong) UIButton *setDefaultBtn;
@property (nonatomic, strong) UIButton *deleteBtn;
@property (nonatomic, strong) UIButton *editBtn;

@property (nonatomic, strong) AddressModel *model;

@property (nonatomic, copy) void(^changeStatus)(int addressID);
@property (nonatomic, copy) void(^deleteAddress)(int addressID);
@property (nonatomic, copy) void(^checkAddress)(int addressID);


@end
