//
//  ChooseOrderCouponCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ChooseOrderCouponCell.h"


@implementation ChooseOrderCouponCell

- (void)createUI{
    self.backgroundColor = self.contentView.backgroundColor = XHWhiteColor;
    self.coupon_bgView = [YYCreateTools createView:XHWhiteColor];
    self.coupon_bgView.v_cornerRadius = kSizeScale(7);
    self.coupon_bgView.layer.masksToBounds = YES;
    self.coupon_bgView.layer.borderColor = XHSeparateLineColor.CGColor;
    self.coupon_bgView.layer.borderWidth =1;
    self.coupon_bgView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:self.coupon_bgView];
    
    self.couponPriceLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(10)
                                             textColor:XHRedColor];
    self.couponPriceLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.couponPriceLabel];
    
    self.dotter_ImageView = [YYCreateTools createImageView:@"coupon_dotted_icon"
                                                 viewModel:-1];
    [self.coupon_bgView addSubview:self.dotter_ImageView];
    
    
    self.couponPriceLabel.attributedText = [YYCommonTools containSpecialSymbolHandler:@"¥"
                                                                           symbolFont:normalFont(14) symbolTextColor:XHRedColor
                                                                            wordSpace:2
                                                                      symbolIsAddLeft:YES
                                                                                price:@"99"
                                                                            priceFont:boldFont(30) priceTextColor:XHRedColor symbolOffsetY:0];
    
    self.conditionPriceLabel = [YYCreateTools createLabel:nil
                                                     font:normalFont(12)
                                                textColor:XHRedColor];
    self.conditionPriceLabel.textAlignment = NSTextAlignmentCenter;
    [self.coupon_bgView addSubview:self.conditionPriceLabel];
    
    self.couponTitleLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(16)
                                             textColor:XHBlackColor];
    [self.coupon_bgView addSubview:self.couponTitleLabel];
    
    self.couponTimeLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(10)
                                                textColor:XHBlackLitColor];
    [self.coupon_bgView addSubview:self.couponTimeLabel];
    
    self.couponScopeLabel = [YYCreateTools createLabel:@"全场通用(部分类目除外)"
                                                  font:normalFont(11)
                                             textColor:XHBlackLitColor];
    [self.coupon_bgView addSubview:self.couponScopeLabel];
    
    
    self.lauchImageView = [YYCreateTools createImageView:@"arrowLaunch_icon" viewModel:-1];
    [self.coupon_bgView addSubview:self.lauchImageView];
    self.lauchImageView.hidden = YES;
    
    self.selectFlag = [YYCreateTools createImageView:@"addressButtonSelected_icon" viewModel:-1];
    [self.coupon_bgView addSubview:self.selectFlag];
    
    self.receiveIMG = [YYCreateTools createImageView:@"couponReceived_icon" viewModel:-1];
    self.receiveIMG.hidden  = YES;
    [self.coupon_bgView addSubview:self.receiveIMG];
    
    self.receiveBtn = [YYCreateTools createBtn:@"立即领取" font:normalFont(11) textColor:XHWhiteColor];
    self.receiveBtn.v_cornerRadius = kSizeScale(14);
    self.receiveBtn.hidden = YES;
    self.receiveBtn.backgroundColor = XHLoginColor;
    [self.coupon_bgView addSubview:self.receiveBtn];

    @weakify(self);
    self.receiveBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        if (self.receiveBlock) {
            self.receiveBlock(self.model.couponKey);
        }
    };

    self.separateLine = [YYCreateTools createView:XHSeparateLineColor];
    [self.coupon_bgView addSubview:self.separateLine];
    self.separateLine.hidden = YES;

    self.couponUnuseLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(11)
                                             textColor:XHBlackLitColor];
    [self.coupon_bgView addSubview:self.couponUnuseLabel];
    self.couponUnuseLabel.hidden = YES;

    [self.coupon_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(kSizeScale(-12));
        make.height.mas_equalTo(kSizeScale(90));
        make.top.mas_equalTo(17);
        make.bottom.mas_equalTo(-2);
    }];

    [self.dotter_ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(90));
        make.top.mas_equalTo(17);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(1), kSizeScale(57)));
    }];

    [self.couponPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(17));
        make.right.equalTo(self.dotter_ImageView.mas_left).offset(-kSizeScale(5)).priorityHigh();
        make.top.equalTo(self.dotter_ImageView.mas_top).offset(-kSizeScale(2));
    }];
    
    [self.conditionPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.dotter_ImageView.mas_left).offset(-kSizeScale(5));
        make.left.mas_equalTo(kSizeScale(5));
        make.bottom.equalTo(self.dotter_ImageView.mas_bottom).offset(-kSizeScale(2));
    }];

    [self.couponTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dotter_ImageView.mas_right).offset(kSizeScale(10));
        make.top.equalTo(self.dotter_ImageView.mas_top).offset(-kSizeScale(4));
    }];

    [self.couponTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.couponTitleLabel);
        make.centerY.equalTo(self.dotter_ImageView.mas_centerY).offset(kSizeScale(1));
    }];

    [self.couponScopeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponTitleLabel);
        make.bottom.equalTo(self.dotter_ImageView.mas_bottom).offset(kSizeScale(1));
    }];
    
    [self.lauchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.couponScopeLabel.mas_right).mas_equalTo(5);
        make.centerY.equalTo(self.couponScopeLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(9), kSizeScale(9)));
    }];
    
    [self.selectFlag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.coupon_bgView);
        make.right.mas_equalTo(-24);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(21), kSizeScale(21)));
    }];
    
    [self.receiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.coupon_bgView);
        make.right.mas_equalTo(kSizeScale(-12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(64), kSizeScale(27)));
    }];
    
    [self.receiveIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.mas_equalTo(0);
        make.width.height.mas_equalTo(kSizeScale(45));
    }];
    
    [self.separateLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(kSizeScale(-16));
        make.top.mas_equalTo(self.dotter_ImageView.mas_bottom).mas_offset(16);
        make.height.mas_equalTo(1);
    }];
    
    [self.couponUnuseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.top.mas_equalTo(self.separateLine.mas_bottom).mas_offset(kSizeScale(10));
    }];
}

-(void)setModel:(CouponListInfoModel *)model{
    _model = model;
    [self.coupon_bgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(kSizeScale(-12));
        make.top.mas_equalTo(kSizeScale(17));
        make.bottom.mas_equalTo(-1);
        make.height.mas_equalTo(model.launch == YES?kSizeScale(125):kSizeScale(90));
    }];
    
    self.separateLine.hidden = !model.launch;
    self.couponUnuseLabel.hidden =!model.launch;

    self.lauchImageView.image = [UIImage imageNamed:model.launch == YES?@"arrowRetract_icon":@"arrowLaunch_icon"];
    
    if (kValidString(model.denomination)) {
        self.couponPriceLabel.attributedText = [YYCommonTools containSpecialSymbolHandler:([model.type integerValue]==1?@"¥":@"折")
                                                                               symbolFont:normalFont(14) symbolTextColor:XHRedColor
                                                                                wordSpace:2
                                                                          symbolIsAddLeft:([model.type integerValue]==1?YES:NO)
                                                                                    price:model.denomination
                                                                                priceFont:boldFont(30) priceTextColor:XHRedColor
                                                                            symbolOffsetY:0];
    }
    self.couponTitleLabel.text = model.couponName;
    self.conditionPriceLabel.text = model.denominationCondition;
    self.couponTimeLabel.text = model.effectTime;
    self.couponScopeLabel.text = model.scopeTitle;
    
    self.selectFlag.image = [UIImage imageNamed:model.selected == 1?@"addressButtonSelected_icon":@"addressButton_icon"];
    self.couponUnuseLabel.text = model.scopeDetail;
    
    if (model.userCouponStatus == 1) {
        [self.receiveBtn setTitle:@"继续领取" forState:UIControlStateNormal];
    }
}


@end
