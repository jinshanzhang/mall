//
//  ChannerCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "ChannelModel.h"

@interface ChannerCell : YYBaseTableViewCell

@property (nonatomic, strong) UIImageView *channelIcon;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UIImageView *statusBtn;

@property (nonatomic, strong) ChannelModel *model;

@property (nonatomic, copy) void (^cellClickBlock)(ChannelModel *model);

@property (nonatomic, assign) int selectID;

@end
