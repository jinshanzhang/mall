//
//  ChooseCoupon.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ChooseCoupon.h"
#import "AddressView.h"
#import "ChooseOrderCouponCell.h"
#import "CouponListInfoModel.h"
static  CGFloat  const  kHYTopViewHeight = 48; //顶部视图的高度
static  CGFloat  const  kHYTopTabbarHeight = 49; //地址标签栏的高度

@interface ChooseCoupon ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView * contentView;
@property (nonatomic,strong) UIView * underLine;
@property (nonatomic,strong) AddressView * topTabbar;

@property (nonatomic,strong) NSMutableArray * tableViews;
@property (nonatomic,strong) NSMutableArray * topTabbarItems;
@property (nonatomic,strong) UIButton * selectedBtn;

@property (nonatomic,strong) NSMutableArray *modelArr;
@property (nonatomic,assign)  BOOL  selectFirst;


@end

@implementation ChooseCoupon

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
        self.backgroundColor = XHWhiteColor;
    }
    return self;
}

-(void)setUp{

    
    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 45)];
    [self addSubview:topView];
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"优惠券";
    titleLabel.font = normalFont(15);
    titleLabel.textColor = XHBlackColor;
    [titleLabel sizeToFit];
    [topView addSubview:titleLabel];
    titleLabel.centerY = topView.height * 0.5;
    titleLabel.centerX = topView.width * 0.5;
    
    
    UIButton *closebtn = [YYCreateTools createBtnImage:@"goodCoupon_close_icon"];
    [topView addSubview:closebtn];
    [closebtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel);
        make.right.mas_equalTo(-20);
    }];
    
    closebtn.actionBlock = ^(UIButton *sender) {
        if (self.close) {
            self.close();
        }
    };
    UIView * separateLine = [self separateLine];
    [topView addSubview: separateLine];
    separateLine.top = topView.height - separateLine.height;
    AddressView * topTabbar = [[AddressView alloc]initWithFrame:CGRectMake(0, topView.height, self.frame.size.width, 45)];
    [self addSubview:topTabbar];
    _topTabbar = topTabbar;
    
    [self addTopBarItem];
    
    UIView * separateLine1 = [self separateLine];
    [topTabbar addSubview: separateLine1];
    separateLine1.top = topTabbar.height - separateLine.height;
    
    
    UIView * underLine = [[UIView alloc] initWithFrame:CGRectZero];
    [topTabbar addSubview:underLine];
    
    _underLine = underLine;
    [self changeUnderLineFrame:0];
    underLine.top = separateLine1.top - underLine.height;
    _underLine.backgroundColor = XHLoginColor;
    
    UIScrollView * contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(topTabbar.frame), self.frame.size.width, self.height - kHYTopViewHeight - kHYTopTabbarHeight-50)];
    contentView.contentSize = CGSizeMake(kScreenW*2, 0);
    [self addSubview:contentView];
    _contentView = contentView;
    _contentView.pagingEnabled = YES;
    _contentView.delegate = self;
    
    
    UIButton *confirmBtn =[YYCreateTools createBtn:@"确 定" font:normalFont(17) textColor:XHWhiteColor];
    [self addSubview:confirmBtn];
    confirmBtn.backgroundColor = XHLoginColor;
    
    confirmBtn.actionBlock = ^(UIButton *sender) {
        
        NSMutableArray *arr = [NSMutableArray array];
        for (CouponListInfoModel *model in self.validCouponList) {
            if (model.selected == 1) {
                [arr addObject:model.couponId];
            }
        }
        if (self.validCouponList.count == 0) {
            arr = [@[@(-1)] mutableCopy];
        }
        if (self.chooseFinish) {
            self.chooseFinish(arr);
        }
    };
    
    [confirmBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kSafeBottom);
        make.height.mas_equalTo(50);
    }];
    [self addTableView];
}

#pragma mark 绘制两个tableview
- (void)addTableView{
    for (int i = 0;  i <2 ; i++) {
        UITableView * tabbleView = [[UITableView alloc]initWithFrame:CGRectMake(i * kScreenW, 0, kScreenW, self.height - kHYTopViewHeight - kHYTopTabbarHeight-kBottom(45))];
        [_contentView addSubview:tabbleView];
        [self.tableViews addObject:tabbleView];
        tabbleView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tabbleView.delegate = self;
        tabbleView.dataSource = self;
        tabbleView.estimatedRowHeight = 100;
        tabbleView.tag = i;
        tabbleView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewNoOrderCoupon
                                                           title:@""
                                                          target:self
                                                          action:nil];
        Class common = [ChooseOrderCouponCell class];
        registerClass(tabbleView, common);
    }
}


#pragma mark - TableViewDatasouce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([self.tableViews indexOfObject:tableView] == 0){
        return _validCouponList.count;
    }else{
        return _invalidCouponList.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class common = [ChooseOrderCouponCell class];
    ChooseOrderCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    
    if([self.tableViews indexOfObject:tableView] == 0){
        cell.model = _validCouponList[indexPath.row];
    }else{
        cell.lauchImageView.hidden = NO;
        cell.model = _invalidCouponList[indexPath.row];
        cell.selectFlag.hidden =YES;
        cell.couponTitleLabel.textColor = XHBlackLitColor;
        cell.conditionPriceLabel.textColor = XHBlackLitColor;
        cell.couponPriceLabel.textColor = XHBlackLitColor;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.tableViews indexOfObject:tableView] == 1){
        CouponListInfoModel *model = _invalidCouponList[indexPath.row];
        model.launch = !model.launch;
        [tableView reloadData];
    }else{
        
        for (CouponListInfoModel *item  in self.validCouponList) {
            if (item == _validCouponList[indexPath.row]) {
                item.selected = item.selected == 1?0:1;
            }else{
                if (item.selected == 1) {
                    item.selected = 0;
                }
            }
        }
        [tableView reloadData];
    }
}

//分割线
- (UIView *)separateLine{
    
    UIView * separateLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1 / [UIScreen mainScreen].scale)];
    separateLine.backgroundColor =  XHSeparateLineColor;
    return separateLine;
}

- (NSMutableArray *)topTabbarItems{
    if (_topTabbarItems == nil) {
        _topTabbarItems = [NSMutableArray array];
    }
    return _topTabbarItems;
}

- (NSMutableArray *)tableViews{
    
    if (_tableViews == nil) {
        _tableViews = [NSMutableArray array];
    }
    return _tableViews;
}

-(void)setValidCouponList:(NSMutableArray *)validCouponList{
    _validCouponList = validCouponList;
    
    UIButton *btn =  self.topTabbarItems[0];
    [btn setTitle:[NSString stringWithFormat:@"可用优惠券(%lu)",(unsigned long)validCouponList.count] forState:UIControlStateNormal];
    
    [self.tableViews[0] reloadData];
}

-(void)setInvalidCouponList:(NSMutableArray *)invalidCouponList{
    
    _invalidCouponList = invalidCouponList;
    UIButton *btn =  self.topTabbarItems[1];
    [btn setTitle:[NSString stringWithFormat:@"不可用优惠券(%lu)",(unsigned long)invalidCouponList.count] forState:UIControlStateNormal];
    
    [self.tableViews[1] reloadData];
}

//调整指示条位置
- (void)changeUnderLineFrame:(NSInteger)index{
    [_underLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.centerX.equalTo(self.topTabbarItems[index]);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(2);
    }];
    
    
}


- (void)addTopBarItem{
    UIButton * topBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    [topBarItem setTitle:@"可用优惠券(10)" forState:UIControlStateNormal];
    [topBarItem setTitleColor:XHLoginColor forState:UIControlStateNormal];
    [topBarItem setTitleColor:XHLoginColor forState:UIControlStateSelected];
    topBarItem.titleLabel.font = [UIFont systemFontOfSize:kSizeScale(14)];
    topBarItem.centerY = _topTabbar.height * 0.5;
    topBarItem.height =kHYTopViewHeight;
    topBarItem.width =_topTabbar.width/2;
    topBarItem.tag = 0;
    topBarItem.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    
    UIButton * topBarItem2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [topBarItem2 setTitle:@"不可用优惠券(2)" forState:UIControlStateNormal];
    [topBarItem2 setTitleColor:XHBlackColor forState:UIControlStateNormal];
    [topBarItem2 setTitleColor:XHBlackColor forState:UIControlStateSelected];
    topBarItem2.titleLabel.font = [UIFont systemFontOfSize:kSizeScale(14)];
    topBarItem2.centerY = _topTabbar.height * 0.5;
    topBarItem2.centerX = _topTabbar.width/4 * 3;
    topBarItem2.height =kHYTopViewHeight;
    topBarItem2.width =_topTabbar.width/2;
    topBarItem2.tag = 1;
    topBarItem2.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    

    [self.topTabbarItems addObject:topBarItem];
    [self.topTabbarItems addObject:topBarItem2];

    [_topTabbar addSubview:topBarItem];
    [_topTabbar addSubview:topBarItem2];
    [topBarItem addTarget:self action:@selector(topBarItemClick:) forControlEvents:UIControlEventTouchUpInside];
    [topBarItem2 addTarget:self action:@selector(topBarItemClick:) forControlEvents:UIControlEventTouchUpInside];
    self.selectFirst = YES;
}

- (void)topBarItemClick:(UIButton *)btn{
    
    if (btn.tag == 0) {
        self.selectFirst = YES;
    }else{
        self.selectFirst = NO;
    }
    if (self.selectFirst== YES) {
        UIButton *otherBtn = self.topTabbarItems[1];
        [otherBtn setTitleColor:XHBlackColor forState:UIControlStateNormal];
    }else{
        UIButton *otherBtn = self.topTabbarItems[0];
        [otherBtn setTitleColor:XHBlackColor forState:UIControlStateNormal];
    }
    [btn setTitleColor:XHLoginColor forState:UIControlStateNormal];

    
    NSInteger index = [self.topTabbarItems indexOfObject:btn];
    [UIView animateWithDuration:0.5 animations:^{
        self.contentView.contentOffset = CGPointMake(index * kScreenW, 0);
        [self changeUnderLineFrame:btn.tag];
    }];
    
    
}

#pragma mark - <UIScrollView>
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView != self.contentView) return;
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:0.25 animations:^{
        NSInteger index = scrollView.contentOffset.x / kScreenW;
        UIButton * btn = weakSelf.topTabbarItems[index];
        [weakSelf changeUnderLineFrame:btn.tag];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
