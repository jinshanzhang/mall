//
//  ConfirmOrderShopMessageHeaderView.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/6.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "ConfirmOrderShopMessageHeaderView.h"
#import "JXMapNavigationView.h"

@interface ConfirmOrderShopMessageHeaderView()

@property (nonatomic, strong) UIImageView * shopLogoImageView;
@property (nonatomic, strong) UILabel * shopTitleLabel;
@property (nonatomic, strong) UIImageView * productImageView;
@property (nonatomic, strong) UILabel * shopNameLabel;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UIButton * phoneButton;
@property (nonatomic, strong) UIButton * locationButton;


@property (nonatomic, strong)JXMapNavigationView *mapNavigationView;

@end

@implementation ConfirmOrderShopMessageHeaderView

- (void)js_createSubViews {
    [self addSubview:self.shopLogoImageView];
    [self addSubview:self.shopTitleLabel];
    [self addSubview:self.productImageView];
    [self addSubview:self.shopNameLabel];
    [self addSubview:self.addressLabel];
    [self addSubview:self.locationButton];
    [self addSubview:self.phoneButton];
}

- (void)js_layoutSubViews {
    
    [self.shopLogoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(kSizeScale(12));
        make.width.height.mas_equalTo(kSizeScale(25));
    }];
    
    [self.shopTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopLogoImageView.mas_right).offset(kSizeScale(12));
        make.centerY.equalTo(self.shopLogoImageView);
    }];
    
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopLogoImageView);
        make.top.equalTo(self.shopLogoImageView.mas_bottom).offset(kSizeScale(8));
        make.width.height.mas_equalTo(kSizeScale(60));
    }];
    
    [self.phoneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-kSizeScale(12));
        make.centerY.equalTo(self.productImageView);
        make.width.mas_equalTo(kSizeScale(24));
        make.height.mas_equalTo(kSizeScale(24));
    }];
    
    [self.locationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.phoneButton.mas_left).offset(-kSizeScale(32));
        make.centerY.equalTo(self.phoneButton);
        make.width.mas_equalTo(kSizeScale(24));
        make.height.mas_equalTo(kSizeScale(24));
    }];
    
    [self.shopNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.productImageView).offset(kSizeScale(5));
       make.left.equalTo(self.productImageView.mas_right).offset(kSizeScale(12));
       make.right.equalTo(self.locationButton.mas_left).offset(-kSizeScale(22));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.shopNameLabel.mas_bottom).offset(kSizeScale(5));
       make.left.equalTo(self.shopNameLabel);
       make.right.equalTo(self.locationButton.mas_left).offset(-kSizeScale(22));
    }];
}

#pragma mark LazyLoad

- (UIImageView *)shopLogoImageView {
    if(!_shopLogoImageView) {
        _shopLogoImageView = [[UIImageView alloc] init];
        _shopLogoImageView.image = [UIImage imageNamed:@"shopLogo"];
    }
    return _shopLogoImageView;
}

- (UILabel *)shopTitleLabel {
    if(!_shopTitleLabel) {
        _shopTitleLabel = [UILabel creatLabelWithTitle:@"门店自提" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(16)];
        _shopTitleLabel.numberOfLines = 1;
    }
    return _shopTitleLabel;
}

- (UIImageView *)productImageView {
    if(!_productImageView) {
        _productImageView = [[UIImageView alloc] init];
        _productImageView.backgroundColor = XHLightColor;
//        _productImageView.backgroundColor = [UIColor redColor];
    }
    return _productImageView;
}

- (UILabel *)shopNameLabel {
    if(!_shopNameLabel) {
        _shopNameLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(15)];
        _shopNameLabel.numberOfLines = 1;
    }
    return _shopNameLabel;
}

- (UILabel *)addressLabel {
    if(!_addressLabel) {
        _addressLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackMidColor textAlignment:NSTextAlignmentLeft font:midFont(13)];
        _addressLabel.numberOfLines = 2;
    }
    return _addressLabel;
}

- (UIButton *)phoneButton {
    if(!_phoneButton) {
        _phoneButton = [UIButton creatButtonWithTitle:@"" textColor:nil font:nil target:self action:@selector(phoneOperation)];
        [_phoneButton setImage:[UIImage imageNamed:@"btn_xiangqing_dianhua"] forState:UIControlStateNormal];
    }
    return _phoneButton;
}

- (UIButton *)locationButton {
    if(!_locationButton) {
        _locationButton = [UIButton creatButtonWithTitle:@"" textColor:nil font:nil target:self action:@selector(locationOperation)];
        [_locationButton setImage:[UIImage imageNamed:@"btn_xiangqing_dizhi"] forState:UIControlStateNormal];
    }
    return _locationButton;
}


- (JXMapNavigationView *)mapNavigationView{
    if (_mapNavigationView == nil) {
        _mapNavigationView = [[JXMapNavigationView alloc]init];
    }
    return _mapNavigationView;
}

- (void)locationOperation {
    if(self.model.latitude.intValue > 0) {
        NSString *address = [self removeForeAndBehindSpaceWithString:[NSString stringWithFormat:@"   %@%@%@%@%@",self.model.province,self.model.city,self.model.district,self.model.street,self.model.address]];
        [self.mapNavigationView showMapNavigationViewWithtargetLatitude:self.model.latitude.doubleValue targetLongitute:self.model.longitude.doubleValue toName:address];
        [[YYCommonTools getCurrentVC].view addSubview:_mapNavigationView];
    }
}

- (void)phoneOperation {
    if(kValidString(self.model.phone)) {
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.model.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
    }
}

- (void)setModel:(SellerShopModel *)model {
    _model = model;
    if(model) {
        if(kValidString(model.shopLogo)) {
            [self.productImageView sd_setImageWithURL:[NSURL URLWithString:model.shopLogo]];
        }
        if(kValidString(model.shopName)) {
            self.shopNameLabel.text = model.shopName;
        }
        NSString *address = [self removeForeAndBehindSpaceWithString:[NSString stringWithFormat:@"   %@%@%@%@%@",model.province,model.city,model.district,model.street,model.address]];
        self.addressLabel.text = address;
    }
}


- (NSString *)removeForeAndBehindSpaceWithString:(NSString *)str {
    NSString *tmp = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return [tmp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}
@end
