//
//  PreOrderAddressCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "PreOrderAddressCell.h"
@interface PreOrderAddressCell()
@property (nonatomic, strong) UILabel *receiverNameLabel;
@property (nonatomic, strong) UILabel *receiverPhoneLabel;
@property (nonatomic, strong) UILabel *addressTipLabel;
@property (nonatomic, strong) UILabel *introduceLabel;
@property (nonatomic, strong) UILabel *guideLabel;
@end

@implementation PreOrderAddressCell

-(void)createUI{
    
    UIImageView *icon = [YYCreateTools createImageView:@"preAddress_icon" viewModel:-1];
    [self addSubview:icon];
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(33);
        make.bottom.mas_equalTo(-35);
        make.width.height.mas_equalTo(kSizeScale(24));
    }];
    
    
    self.introduceLabel = [YYCreateTools createLabel:@"请填写收货人信息" font:normalFont(15) textColor:XHBlackColor];
    [self addSubview:self.introduceLabel];
    [self.introduceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(icon.mas_right).mas_offset(12);
        make.centerY.equalTo(icon);
    }];

    self.receiverNameLabel = [YYCreateTools createLabel:@"" font:boldFont(15) textColor:XHBlackColor];
    self.receiverNameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.receiverNameLabel];
    
    [self.receiverNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(icon.mas_right).mas_offset(kSizeScale(12));
        make.top.mas_equalTo(14);
    }];
    
    self.receiverPhoneLabel = [YYCreateTools createLabel:@"" font:boldFont(15) textColor:XHBlackColor];
    self.receiverPhoneLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.receiverPhoneLabel];
    
    [self.receiverPhoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    make.left.mas_equalTo(self.receiverNameLabel.mas_right).mas_offset(kSizeScale(15));
        make.top.mas_equalTo(14);
    }];
    
    self.addressTipLabel = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHBlackColor];
    self.addressTipLabel.textAlignment = NSTextAlignmentLeft;
    self.addressTipLabel.numberOfLines = 2;
    [self addSubview:self.addressTipLabel];
    [self.addressTipLabel sizeToFit];
    [self.addressTipLabel setContentMode:UIViewContentModeTop];
    
    [self.addressTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(icon.mas_right).mas_offset(kSizeScale(12));
        make.right.mas_equalTo(kSizeScale(-40));
        make.top.mas_equalTo(self.receiverNameLabel.mas_bottom).mas_offset(8);
//        make.bottom.mas_equalTo(-10);
    }];
    
    UIImageView *more = [YYCreateTools createImageView:@"cellMore_icon" viewModel:-1];
    [self addSubview:more];
    
    [more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(icon);
        make.right.mas_equalTo(-12);
    }];
    
    UIImageView *lineView = [YYCreateTools createImageView:@"addressColorLine" viewModel:-1];
    [self addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self);
    }];
}

-(void)setModel:(AddressModel *)model{
    _model = model;
    if (_hasAddr == 0) {
        return;
    }
    self.receiverNameLabel.text = model.receiverName?:@"";
    self.receiverPhoneLabel.text = model.receiverPhone?:@"";
    self.addressTipLabel.text = model.address?:@"";
}

-(void)setHasAddr:(int)hasAddr{
    _hasAddr = hasAddr;
    if (hasAddr == 1) {
        self.introduceLabel.hidden = YES;
    }
}



@end
