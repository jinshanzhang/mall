//
//  GoodDetailCouponView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeBannerInfoModel.h"
#import "YYBaseViewShareView.h"

@interface GoodDetailCouponView : YYBaseViewShareView

@property (nonatomic, copy) void(^close)(void);

@property (nonatomic, strong) NSMutableArray *CouponArr;

@property (nonatomic, copy) void(^receiveFinish)(HomeBannerInfoModel *bannerModel);

@property (nonatomic, copy) void(^callBack)(NSMutableArray *changeArr);


- (instancetype)initCouponView;


@end
