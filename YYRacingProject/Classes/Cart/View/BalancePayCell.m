//
//  BalancePayCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "BalancePayCell.h"

@implementation BalancePayCell

-(void)createUI{
    
    self.titleLabel = [YYCreateTools createLabel:@"可用余额" font:normalFont(13) textColor:XHBlackLitColor];
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(16);
    }];
    
    self.detailLabel = [YYCreateTools createLabel:@"余额支付:" font:normalFont(15) textColor:XHBlackColor];
    [self addSubview:self.detailLabel];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(8);
        make.bottom.mas_equalTo(-16);
    }];

    self.amountLabel = [YYCreateTools createLabel:@"" font:normalFont(15) textColor:XHRedColor];
    [self addSubview:self.amountLabel];
    
    [self.amountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.detailLabel.mas_right).mas_offset(10);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(8);
        make.bottom.mas_equalTo(-16);
    }];
    
    self.chooseSwich = [[UISwitch alloc] initWithFrame:CGRectZero];
    [self addSubview:self.chooseSwich];
    self.chooseSwich.onTintColor = XHLoginColor;
    [self.chooseSwich mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.mas_equalTo(-15);
    }];
   [self.chooseSwich addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
}

#pragma mark private method
-(void)switchAction:(id)sender
{
    if (self.cellClickBlock) {
        self.cellClickBlock(self.chooseSwich.on);
    }
}

-(void)setBalanceModel:(BalanceModel *)balanceModel
{
    _balanceModel = balanceModel;
    self.titleLabel.text = [NSString stringWithFormat:@"可用余额: ￥%@",_balanceModel.totalAmount];
    self.amountLabel.text =[NSString stringWithFormat:@"-￥%@",_balanceModel.payAmount];
    if ([balanceModel.totalAmount floatValue] == 0) {
        self.chooseSwich.enabled = NO;
    }
}

@end
