//
//  CartGoodListCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "CartSkuModel.h"

@interface CartGoodListCell : YYBaseTableViewCell

@property (nonatomic, strong) UIImageView *iconImg;

@property (nonatomic, strong) UIButton *selectBtn;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UILabel *moneyLabel;

@property (nonatomic, strong) UILabel *invalidLabel;


@property (nonatomic, strong) CartSkuModel *skuModel;

@property (nonatomic, strong) YYNumberView *numberView;

@property (nonatomic, assign) BOOL selectAll;

@property (nonatomic, copy) void (^cellClickBlock)(float amount, CartSkuModel *model);

@property (nonatomic, copy) void (^cellNumerBlock)(NSInteger skuCnt,CartSkuModel *model);

@property (nonatomic, assign) int max;

@end
