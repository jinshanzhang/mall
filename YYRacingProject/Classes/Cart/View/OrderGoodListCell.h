//
//  OrderGoodListCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "CartSkuModel.h"

@interface OrderGoodListCell : YYBaseTableViewCell
@property (nonatomic, strong) UIImageView *iconImg;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UILabel *moneyLabel;

@property (nonatomic, strong) UILabel *countLabel;

@property (nonatomic, strong) CartSkuModel *skuModel;

@end
