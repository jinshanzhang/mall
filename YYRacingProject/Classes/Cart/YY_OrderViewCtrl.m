//
//  YY_OrderViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_OrderViewCtrl.h"
#import "PreOrderAddressCell.h"
#import "OrderCommonCell.h"
#import "ChannerCell.h"
#import "BalancePayCell.h"
#import "OrderGoodListCell.h"
#import "OrderSkuTitleCell.h"
#import "ConfirmOrderShopMessageHeaderView.h"

#import "SubmitOrderRequestAPI.h"
#import "YY_OrderCompleteViewCtrl.h"
#import "YY_OrderUnpaidViewCtrl.h"
#import "YY_SetUpAddressViewCtrl.h"
#import "YY_AddressListViewCtrl.h"
#import "YY_PaymentPasswordViewController.h"

#import "QueryPayRequestAPI.h"
#import "ChooseCoupon.h"
#import "BuyerCouponUseRequestAPI.h"
#import "CouponListInfoModel.h"
#import "groomGoodsModel.h"
#import "DCPaymentView.h"

@interface YY_OrderViewCtrl ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *skuArr;
@property (nonatomic, strong) NSMutableArray *remarksArr;
@property (nonatomic, strong) NSMutableArray *SummaryListForCell;

@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, strong) UILabel *addressView;
@property (nonatomic, strong) UILabel *totalLabel;
@property (nonatomic, strong) UILabel *wordLabel;
@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UIButton *summitOrderBtn;

@property (nonatomic, assign) int   channelId;
@property (nonatomic, copy)   NSString  *userName;
@property (nonatomic, copy)   NSString  *userID;
@property (nonatomic, copy)   NSString  *result;

@property (nonatomic, assign) long  longValue;
@property (nonatomic, assign) int  payGateWay;

@property (nonatomic, strong) NSString  *PindID;

@property (nonatomic, strong) UIView  *cover;
@property (nonatomic, strong) ChooseCoupon *chooseCouponView;
//是否有地址
@property (nonatomic, assign) BOOL hasAdd;
//是否需要提交姓名
@property (nonatomic, assign) BOOL needID;
//是否支出余额支付
@property (nonatomic, assign) BOOL supportBalancePay;
//是否打开余额支付
@property (nonatomic, assign) BOOL openBalancePay;

//饭卡支付
@property (nonatomic, assign) BOOL isMealZonePay;

//是否是自提商品
@property (nonatomic, assign) BOOL isSinceTheLiftProduct;

@property (nonatomic, strong) ConfirmOrderShopMessageHeaderView * headerView;

@end

@implementation YY_OrderViewCtrl
-(UITableView *)tableView
{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.backgroundColor = XHLightColor;
        self.tableView.allowsSelection = YES;
        self.tableView.estimatedRowHeight = 100;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

- (ConfirmOrderShopMessageHeaderView *)headerView {
    if(!_headerView) {
        _headerView = [[ConfirmOrderShopMessageHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(120))];
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}

- (UIView *)cover{
    if (!_cover) {
        _cover = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _cover.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        @weakify(self);
        [_cover addSubview:self.chooseCouponView];
        _chooseCouponView.chooseFinish = ^(NSMutableArray *idArr) {
            @strongify(self);
            self.finishIdArr = idArr;
            [UIView animateWithDuration:0.25 animations:^{
                self.preOrderModel.coupon.couponIds = idArr;
                self.view.transform = CGAffineTransformIdentity;
                self.cover.hidden = YES;
            }];
            [self preSubmitWithAddreID:self.preOrderModel.addr.addrId couponArr:idArr];
        };

        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapCover:)];
        [_cover addGestureRecognizer:tap];
        tap.delegate = self;
        _cover.hidden = YES;
        
    }
    return _cover;
}

- (ChooseCoupon *)chooseCouponView{
    if (!_chooseCouponView) {
        @weakify(self);
        _chooseCouponView = [[ChooseCoupon alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 465 -kSafeBottom, [UIScreen mainScreen].bounds.size.width, 465+kSafeBottom)];
        _chooseCouponView.close = ^{
            @strongify(self);
            self.cover.hidden = YES;
        };
    }
    return _chooseCouponView;
}

- (void)tapCover:(UITapGestureRecognizer *)tap{
    _cover.hidden = YES;
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_chooseCouponView.frame, point)){
        return NO;
    }
    return YES;
}

- (void)setPreOrderModel:(PreOrderModel *)preOrderModel {
    _preOrderModel = preOrderModel;
//    self.isMealZonePay = preOrderModel.isMealZonePay;
    self.isSinceTheLiftProduct = [preOrderModel.deliveryType integerValue] == 2;
    if(self.isSinceTheLiftProduct) {
        self.tableView.tableHeaderView = self.headerView;
    }
    self.headerView.model = preOrderModel.sellerShopsVO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"确认订单"];
    [self xh_navBottomLine:XHLightColor];
    [self.navigationBar yy_setNavigationButtonImageName:@"detail_navbar_back"
                                                 isLeft:YES
                                            buttonBlock:^(UIButton *sender) {
                                                [JCAlertView showTwoButtonsWithTitle:@"确认离开" Message:@"精心挑选了这么久，真的要放弃吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"去意已决" Click:^{
                                                    [self.navigationController popViewControllerAnimated:YES];
                                                } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"继续下单" Click:^{
                                                } type:JCAlertViewTypeDefault];
                                            }];
    //sku section数组
    [self reloadShopInfo];
    
//    if(self.isMealZonePay) {
//        ChannelModel *model = [[ChannelModel alloc] init];
//        model.channelId = 0;
//        model.channelUrl = @"meal_zone_pay_icon";
//        model.channelName = @"饭卡支付";
//        model.info = @"蜀黎之家独家支付";
//        model.defaultFlag = 1;
//
//        self.preOrderModel.channel = [NSMutableArray arrayWithObject:model];
//    }
    //当前channelID
    for (ChannelModel *model in self.preOrderModel.channel) {
        if (model.defaultFlag == 1) {
            self.channelId = model.channelId;
        }
    }
    
    //判断个section开关
    [self reloadSection];
    [self.view addSubview:self.tableView];
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(50));
    }];
    
    [self creatFooter];
    self.tableView.tableFooterView = self.footerView;

    Class current = [PreOrderAddressCell class];
    registerClass(self.tableView, current);
    Class common = [OrderCommonCell class];
    registerClass(self.tableView, common);
    Class channel = [ChannerCell class];
    registerClass(self.tableView, channel);
    Class skuList = [OrderGoodListCell class];
    registerClass(self.tableView, skuList);
    Class skuTitle = [OrderSkuTitleCell class];
    registerClass(self.tableView, skuTitle);
    Class balance = [BalancePayCell class];
    registerClass(self.tableView, balance);
//    Class shop = [ConfirmOrderSHopMessageListCell class];
//    registerClass(self.tableView, shop);
    [self creatFooterView];
    [self  changeSummartList];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.cover];
    
}

-(void)changeSummartList{
    self.SummaryListForCell = [self.preOrderModel.summaryList mutableCopy];
    for (OrderPriceModel *model  in self.preOrderModel.summaryList) {
        int type = [model.type intValue];
        if (type == 11 || type == 4 || (type == 5 && self.isSinceTheLiftProduct)) {
            //自提商品不展示运费
            [self.SummaryListForCell removeObject:model];
        }
    }
}

- (void)InfoNotificationAction:(NSNotification *)notification
{
    [YYCenterLoading showCenterLoading];
    if (!kValidString(self.result)) {
        if (!kValidString(self.PindID)) {
            [YYCommonTools showTipMessage:@"下单遇到小问题"];
            return;
        }
    }
    if (self.payGateWay == 4) {
        [self checkOrderWith:self.longValue pingId:self.PindID payStatus:0];
    }else{
       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (!kValidString(self.result)) {
                [self checkOrderWith:self.longValue pingId:self.PindID payStatus:0];
            }
        });
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)creatFooterView
{
    @weakify(self);
    UIView *submmitView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:submmitView];
    submmitView.layer.masksToBounds = YES;
    submmitView.layer.borderColor = XHLightColor.CGColor;
    [submmitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.tableView);
        make.top.equalTo(self.tableView.mas_bottom);
        make.height.mas_equalTo(50);
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [submmitView addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    //**********提交按钮**************
    self.summitOrderBtn = [YYCreateTools createBtn:@"提交订单" font:normalFont(17) textColor:XHWhiteColor];
    [submmitView addSubview:self.summitOrderBtn];
    
    [self.summitOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.right.mas_offset(1);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kSizeScale(kSizeScale(125)));
    }];
//    [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:[UIColor addGoldGradient:CGSizeMake(kSizeScale(125), kSizeScale(50))]] forState:UIControlStateNormal];
//
    [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:XHMainColor] forState:UIControlStateNormal];
    
    self.summitOrderBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        if (kUserInfo.payAccountPasswordFlag == NO) {
            if (self.openBalancePay) {
                [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"您还未设置支付密码，为了您的账户 安全请先设置支付密码" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
                } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"立即设置" Click:^{
                    YY_PaymentPasswordViewController *vc = [YY_PaymentPasswordViewController new];
                    [self.navigationController pushViewController:vc animated:YES];
                } type:JCAlertViewTypeDefault];
                return;
            }
        }
        if (self.openBalancePay == YES) {
            DCPaymentView *payAlert = [[DCPaymentView alloc]init];
            payAlert.titleStr = @"提示";
            payAlert.detail = @"输入蜀黍之家余额支付密码";
            [payAlert show];
            
            payAlert.completeHandle = ^(NSString *passwordToken) {
                [weak_self submitOrder_passwordToken:passwordToken];
            };
        } else {
            [weak_self submitOrder_passwordToken:@""];
        }
    };
    
    self.wordLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHRedColor];
    [submmitView addSubview:_wordLabel];
    [_wordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(8);
        make.right.mas_equalTo(self.summitOrderBtn.mas_left).mas_equalTo(-10);
    }];

    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(10) textColor:XHBlackLitColor];
    [submmitView addSubview:_detailLabel];
    [_detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.wordLabel.mas_bottom).mas_offset(2);
        make.right.mas_equalTo(self.summitOrderBtn.mas_left).mas_equalTo(-10);
    }];
    
    [self changeSubmitShowMoney];
    
    AddressModel *model = self.preOrderModel.addr;
    self.addressView = [YYCreateTools createLabel:[NSString stringWithFormat:@"    配送 : %@",model.address] font:normalFont(12) textColor:HexRGB(0xFF9400)];
    self.addressView.backgroundColor = XHClearColor;
    self.addressView.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:self.addressView];
    self.addressView.hidden = YES;
    
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(submmitView.mas_top);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(32);
    }];
}

-(void)creatFooter
{
    self.footerView = [YYCreateTools createView:XHLightColor];
    _footerView.frame = CGRectMake(0, 0, kScreenW, 40);
}

-(void)changeSubmitShowMoney{
    
    NSString *totle;
    NSString *freight;
    NSString *taxation;
    NSString *balance;
    
    for (OrderPriceModel *model in self.preOrderModel.summaryList) {
        int type = [model.type intValue];
        if (type == 4) {
            totle = model.priceTag;
        }else if (type ==5 ){
            freight = model.priceTag;
        }else if (type == 6){
            taxation = model.priceTag;
        }else if (type == 11){
            balance = model.priceTag;
        }
    }
    NSString *detail;
    if (self.supportBalancePay) {
        detail = [NSString stringWithFormat:@"余额支付:-¥%@",balance];
    }else{
        if (self.preOrderModel.needId == 0) {
            if ([freight floatValue] > 0) {
                detail = [NSString stringWithFormat:@"含运费¥%@",freight];
            }else{
                detail = @"免运费";
            }
        }else{
            if ([freight floatValue] > 0) {
                detail = [NSString stringWithFormat:@"含税费¥%@  运费¥%@",taxation,freight];
            }else{
                detail = [NSString stringWithFormat:@"含税费¥%@  免运费",taxation];
            }
        }
    }
    NSString *word = [NSString stringWithFormat:@"实付金额: ¥%@", totle];
    NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc]initWithString:word];
//    //找出特定字符在整个字符串中的位置
//    NSRange redRange = NSMakeRange(0, [[contentStr string] rangeOfString:@":"].location+1);
//    //修改特定字符的颜色
//    [contentStr addAttribute:NSForegroundColorAttributeName value:XHBlackColor range:redRange];
//    //修改特定字符的字体大小
//    [contentStr addAttribute:NSFontAttributeName value:normalFont(14) range:redRange];
    [self.wordLabel setAttributedText:contentStr];
    if(!self.isSinceTheLiftProduct) {
        _detailLabel.text = [NSString stringWithFormat:@"( %@ )",detail];
    }
    
}
#pragma mark 更新商品数据
-(void)reloadShopInfo{
    self.remarksArr =[NSMutableArray array];
    self.skuArr = [NSMutableArray array];
    for (int i = 0 ; i < self.preOrderModel.orderList.count; i++) {
        [self.remarksArr addObject:[NSMutableDictionary dictionaryWithObject:@"" forKey:@"remark"]];
        PreOrderLIstModel *model = self.preOrderModel.orderList[i];
        [self.skuArr addObject:model];
        for (int i = 0 ; i < model.skuList.count; i++) {
//            CartSkuModel *skuModel = model.skuList[i];
            [self.skuArr addObject:model.skuList[i]];
        }
        for (int i = 0; i< model.summaryList.count; i++) {
            OrderPriceModel *priceModel = model.summaryList[i];
            if(self.isSinceTheLiftProduct && priceModel.type.integerValue == 0) {
                //自提商品不展示运费
            } else {
                [self.skuArr addObject:model.summaryList[i]];
            }
        }
        [self.skuArr addObject:@""];
    }
}

-(void)reloadSection{
    //判断个section开关
    self.operationArr =  [YYCommonTools getPreorderShowOperatorCells:@(self.preOrderModel.operation)];
    [self.operationArr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSNumber *value = (NSNumber *)obj;
        if ([value integerValue] == 1) {
            if (idx == 0) {
                self.hasAdd = YES;
            }
            if (idx == 1) {
                self.needID = YES;
            }
            if (idx == 2) {
                self.supportBalancePay = YES;
            }
            if (idx == 3) {
                self.openBalancePay = YES;
            }
        }
    }];
}


#pragma mark 获取优惠券列表
-(void)getCouponListWithOrder:(NSString *)orderToken{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:orderToken forKey:@"orderToken"];
    [YYCenterLoading showCenterLoading];
    BuyerCouponUseRequestAPI *api = [[BuyerCouponUseRequestAPI alloc] initCouponUseRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            NSMutableArray *validCouponList = [[NSMutableArray modelArrayWithClass:[CouponListInfoModel class] json:[responDict objectForKey:@"validCouponList"]]mutableCopy];
            NSMutableArray *invalidCouponList = [[NSMutableArray modelArrayWithClass:[CouponListInfoModel class] json:[responDict objectForKey:@"invalidCouponList"]]mutableCopy];
            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
            [UIView animateWithDuration:0.25 animations:^{
            }];
            self.cover.hidden = !self.cover.hidden;
            self.chooseCouponView.invalidCouponList = invalidCouponList;
            self.chooseCouponView.validCouponList = validCouponList;
            self.chooseCouponView.hidden = self.cover.hidden;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark 重新预提交订单
-(void)preSubmitWithAddreID:(int)addID couponArr:(NSMutableArray *)couponIDs
{
    NSString *deliveryType = @"1";
    if(self.isSinceTheLiftProduct) {
       deliveryType = @"2";
    }
    [kWholeConfig submitOrderWithSkulist:self.preOrderSkuArr souce:_source addID:addID?:(0) previewSwitch:self.openBalancePay?1:0  couponId:couponIDs?:@[@(-1)] payment:@"" deliveryType:deliveryType block:^(PreOrderModel *model) {
        self.preOrderModel = model;
        if (!kValidArray(model.coupon.couponIds)) {
            self.preOrderModel.coupon.couponIds = [NSMutableArray array];
        }
        AddressModel *addmodel = self.preOrderModel.addr;
        self.addressView.text = [NSString stringWithFormat:@"    配送 : %@",addmodel.address];
        [self reloadShopInfo];
        [self reloadSection];
        [self changeSubmitShowMoney];
        [self  changeSummartList];
        [self.tableView reloadData];
    } failBlock:^(PreOrderModel *model) {

    }];
}

#pragma mark 提交订单并支付
-(void)submitOrder_passwordToken:(NSString *)passwordToken
{
    if (self.needID == YES) {
        if (!self.userName) {
            [YYCommonTools showTipMessage:@"请输入姓名"];
            return;
        }
        if (!self.userID) {
            [YYCommonTools showTipMessage:@"请输入身份证"];
            return;
        }
    }
    if (!self.hasAdd) {
        [YYCommonTools showTipMessage:@"请选择收货地址"];
        return;
    }
    
    for (ChannelModel *model in self.preOrderModel.channel) {
        
    }
    
    if(self.preOrderModel.isMealZonePay) {
        [JCAlertView showTwoButtonsWithTitle:@"支付提示" Message:@"该笔订单的支付方式为\"饭卡支付\",点击\"立即支付\"将从您的饭卡中扣款." ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
            
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"立即支付" Click:^{
            [self submitOrderWithPasswordToken:passwordToken];
        } type:JCAlertViewTypeDefault];
    } else {
        [self submitOrderWithPasswordToken:passwordToken];
    }
}

- (void)submitOrderWithPasswordToken:(NSString *)passwordToken {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.preOrderModel.orderToken forKey:@"orderToken"];
    [dict setObject:@(self.preOrderModel.addr.addrId) forKey:@"addrId"];
    [dict setObject:self.userID?:@"" forKey:@"id"];
    [dict setObject:self.userName?:@"" forKey:@"name"];
    [dict setObject:@(self.channelId) forKey:@"channelId"];
    [dict setObject:passwordToken forKey:@"passwordToken"];
    if(self.isSinceTheLiftProduct) {
       [dict setObject:@"2" forKey:@"deliveryType"];
    }
    for (int i = 0 ; i < self.remarksArr.count; i++) {
        PreOrderLIstModel *model = self.preOrderModel.orderList[i];
        NSMutableDictionary *dic = self.remarksArr[i];
        [dic setObject:[NSNumber numberWithInt:model.previewSubOrderId] forKey:@"previewSubOrderId"];
        [self.remarksArr replaceObjectAtIndex:i withObject:dic];
    }
    [dict setObject:self.remarksArr forKey:@"remarks"];
    
    [YYCenterLoading showCenterLoading];
    SubmitOrderRequestAPI *api = [[SubmitOrderRequestAPI alloc] initSubmitOrderRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            int hasPaid = [[responDict objectForKey:@"hasPaid"]  intValue];
            self.longValue = [[responDict objectForKey:@"orderId"] longValue];
            self.PindID =[responDict objectForKey:@"pingId"];
            self.payGateWay = [[responDict objectForKey:@"payGateway"] intValue];
            if (hasPaid == 0) {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(InfoNotificationAction:) name:CheckOrder object:nil];
                NSLog(@"self.PindID *************%@",self.PindID);
                if (self.payGateWay ==4 ) {
                    [YYCommonTools WXPay:self chargeJason:[responDict objectForKey:@"charge"] block:^(NSString *result) {
                        self.result = nil;
                    }];
                } else {
                    [YYCommonTools pingPay:self chargeJason:[responDict objectForKey:@"charge"] block:^(NSString *result) {
                        self.result = result;
                        if ([result isEqualToString:@"success"]) {
                            [self checkOrderWith:self.longValue pingId:self.PindID payStatus:1];
                        }else{
                            [YYCommonTools skipOrderPay:self orderID:self.longValue canBackRoot:YES];
                        }
                    }];
                }
            }else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self checkOrderWith:self.longValue pingId:self.PindID payStatus:1];
                });
            }
        } else {
           NSDictionary *dataDict = api.responseObject;
           [YYCommonTools showTipMessage:[dataDict objectForKey:@"msg"]];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark 确认订单
-(void)checkOrderWith:(long)orderId pingId:(NSString *)pingId payStatus:(int)payStatus
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSNumber numberWithLong:orderId] forKey:@"orderId"];
    [dict setObject:@(payStatus) forKey:@"appPayStatus"];
    [dict setObject:pingId?:@"" forKey:@"pingId"];
    [dict setObject:@(self.payGateWay) forKey:@"payGateway"];
//    [YYCenterLoading showCenterLoading];
    QueryPayRequestAPI *api = [[QueryPayRequestAPI alloc] initQueryPayRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            int status = [[responDict objectForKey:@"payStatus"] intValue];
            if (status == 1) {
                int type =[[responDict objectForKey:@"orderType"] intValue];
                //商品数组
                NSMutableArray *itemArr = [[NSMutableArray modelArrayWithClass:[groomGoodsModel class] json:[responDict objectForKey:@"items"]]mutableCopy];
                //地址信息
                NSDictionary *addrDic = [responDict objectForKey:@"addr"];
                
                int displayType = [[responDict objectForKey:@"displayType"] intValue];;
                
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:itemArr,@"item",addrDic,@"add",@(displayType),@"display", nil];
                
                [YYCommonTools skipOrderComplete:self params:[dic mutableCopy] type:type isSelfGetOrder:self.isSinceTheLiftProduct];
            } else {
                [YYCommonTools skipOrderPay:self orderID:self.longValue canBackRoot:YES];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if(self.isMealZonePay) {
//        return 3;
//    }
    if(self.isSinceTheLiftProduct) {
        return 4;
    }
    if (self.needID == YES&&self.supportBalancePay == YES) {
        return 7;
    }
    if ((self.needID == NO&&self.supportBalancePay == YES)||(self.needID==YES&&self.supportBalancePay==NO) ) {
        return 6;
    }
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if(self.isMealZonePay) {
//        if(section == 0) {
//            //地址
//            return 1;
//        }
//        if(section == 1) {
//           //支付方式
//            return 1;
//        }
//        //商品信息
//        return self.skuArr.count;
//    }
    if(self.isSinceTheLiftProduct) {
        if(section == 0) {
            //支付方式
            return 2;
        }
        if(section == 1) {
            //优惠券
            return 1;
        }
        if(section == 2) {
           //商品信息
           return self.skuArr.count;
        }
        //订单信息
        return self.SummaryListForCell.count;
    }
    
//    if ((indexPath.section == 2 && self.needID == YES)||(indexPath.section==1 && self.needID == NO))
    if (self.preOrderModel.needId == 1) {
        if (section == 0||section==3) {
            return 1;
        }else if (section == 1||section == 2){
            return 2;
        }else if (section == 4){
            return self.skuArr.count;
        }
        else{
            return self.SummaryListForCell.count;
        }
    }else{
        if (section == 0 ||section == 2) {
            return 1;
        } else if (section == 1){
//            return 2;
            return  self.preOrderModel.channel.count;
        }else if(section == 3){
            if (self.supportBalancePay == YES) {
                return 1;
            }else{
                return self.skuArr.count;
            }
        }else if (section == 4) {
            if (self.supportBalancePay == YES) {
                return self.skuArr.count;
            }else{
                return self.SummaryListForCell.count;
            }
        }
        else{
            return self.SummaryListForCell.count;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isSinceTheLiftProduct)  {
        if (indexPath.section == 0) {
            Class channel = [ChannerCell class];
            ChannerCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(channel)];
            if (!cell) {
                cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(channel)];
                cell.backgroundColor = XHWhiteColor;
            }
            ChannelModel *model = self.preOrderModel.channel[indexPath.row];
            if (self.channelId) {
                if (model.channelId == self.channelId) {
                    model.defaultFlag = 1;
                }else{
                    model.defaultFlag = 0;
                }
            }
            cell.model = model;
            return cell;
        }
        else if (indexPath.section == 1) {
            Class common = [OrderCommonCell class];
            OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
            if (!cell) {
                cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
            }
            cell.type = CellWithTwoLabelAndArrow;
            cell.detailLabel.textColor = XHRedColor;
            if (kValidArray(self.preOrderModel.coupon.couponIds)) {
                cell.detailLabel.text = kValidString(self.preOrderModel.coupon.reducedPrice)?[NSString stringWithFormat:@"-¥ %@",self.preOrderModel.coupon.reducedPrice]:@"不使用";
            }else{
                cell.detailLabel.text = self.finishIdArr.count==0?@"不使用":@"暂无可用";
                cell.detailLabel.textColor =self.finishIdArr.count==0?XHRedColor:XHGrayColor;
            }
            cell.titleLabel.text = self.preOrderModel.coupon.title;
            return cell;
        }
        else if (indexPath.section == 2) {
            Class skuList = [OrderGoodListCell class];
            OrderGoodListCell *skuCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuList)];
            if (!skuCell) {
                skuCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuList)];
            }
            Class skuTitle = [OrderSkuTitleCell class];
            OrderSkuTitleCell *skuTitleCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuTitle)];
            if (!skuTitleCell) {
                skuTitleCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuTitle)];
            }
            Class common = [OrderCommonCell class];
            OrderCommonCell *commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
            if (!commonCell) {
                commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
            }
            id object = self.skuArr[indexPath.row];
            if ([object isKindOfClass:[PreOrderLIstModel class]]) {
                PreOrderLIstModel *model= self.skuArr[indexPath.row];
//                skuTitleCell.titleLabel.text = model.sellerAddr;
                skuTitleCell.hidden = YES;
                return skuTitleCell;
            }else if ([object isKindOfClass:[CartSkuModel class]]){
                skuCell.skuModel = self.skuArr[indexPath.row];
                return skuCell;
            }else if ([object isKindOfClass:[OrderPriceModel class]]){
                commonCell.type = CellWithTwoLabel;
                commonCell.priceModel = self.skuArr[indexPath.row];
                return commonCell;
            }else{
                commonCell.type = CellwithOneTextFild;
                int textFieldtag = 0;
                if (self.remarksArr.count == 1) {
                    textFieldtag = 0;
                }else{
                    if (indexPath.row < self.skuArr.count-1) {
                        id nextObject = self.skuArr[indexPath.row+1];
                        for (int i = 0 ; i < self.preOrderModel.orderList.count; i++) {
                            PreOrderLIstModel *model = self.preOrderModel.orderList[i];
                            if (nextObject == model) {
                                textFieldtag = i-1;
                            }
                        }
                    }else{
                        textFieldtag = (int)self.remarksArr.count-1;
                    }
                }
                commonCell.showTextField.tag = textFieldtag;
                commonCell.titleLabel.text = @"买家留言:";
                commonCell.showTextField.placeholder = @"200个字以内(选填)";
                commonCell.showTextField.delegate = self;
                commonCell.showTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
                [commonCell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                return commonCell;
            }
        }
        Class common = [OrderCommonCell class];
        OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        cell.type = CellWithTwoLabel;
        OrderPriceModel *model = self.SummaryListForCell[indexPath.row];
        cell.priceModel = model;
        return cell;
    }
    if (indexPath.section == 0) {
        Class currentCls = [PreOrderAddressCell class];
        PreOrderAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
            cell.backgroundColor = XHWhiteColor;
        }
        cell.hasAddr = self.preOrderModel.hasAddr;
        cell.model = self.preOrderModel.addr;
        return cell;
    }
    if (indexPath.section == 1 && self.needID == YES) {
        Class common = [OrderCommonCell class];
        OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        cell.type = CellwithOneTextFild;
        cell.titleLabel.text = indexPath.row == 0?@"身份证姓名":@"身份证号";
        cell.showTextField.placeholder = indexPath.row==0?@"添加收货人身份证姓名以便清关":@"添加收货人身份证以便清关";
        cell.showTextField.text = indexPath.row==0?self.userName:self.userID;
        cell.showTextField.delegate = self;
        cell.showTextField.tag = indexPath.row;
        return cell;
    }
    if ((indexPath.section == 2 && self.needID == YES)||(indexPath.section==1 && self.needID == NO)) {
        
        Class channel = [ChannerCell class];
        ChannerCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(channel)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(channel)];
            cell.backgroundColor = XHWhiteColor;
        }
        ChannelModel *model = self.preOrderModel.channel[indexPath.row];
        if (self.channelId) {
            if (model.channelId == self.channelId) {
                model.defaultFlag = 1;
            }else{
                model.defaultFlag = 0;
            }
        }
        cell.model =model;
        return cell;
    }
    if ((indexPath.section == 3&&self.preOrderModel.needId == 1)||(indexPath.section == 2&&self.needID == NO)) {
        Class common = [OrderCommonCell class];
        OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        cell.type = CellWithTwoLabelAndArrow;
        cell.detailLabel.textColor = XHRedColor;
        if (kValidArray(self.preOrderModel.coupon.couponIds)) {
            cell.detailLabel.text = kValidString(self.preOrderModel.coupon.reducedPrice)?[NSString stringWithFormat:@"-¥ %@",self.preOrderModel.coupon.reducedPrice]:@"不使用";
        }else{
            cell.detailLabel.text = self.finishIdArr.count==0?@"不使用":@"暂无可用";
            cell.detailLabel.textColor =self.finishIdArr.count==0?XHRedColor:XHGrayColor;
        }
        cell.titleLabel.text = self.preOrderModel.coupon.title;
        return cell;
    }
    if (indexPath.section == 3 && self.needID == NO && self.supportBalancePay == YES) {
        Class balance = [BalancePayCell class];
        BalancePayCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(balance)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(balance)];
                    cell.backgroundColor = XHWhiteColor;
        }
        cell.balanceModel = self.preOrderModel.balance;
        cell.chooseSwich.on = self.openBalancePay;
        cell.cellClickBlock = ^(BOOL swich) {
            self.openBalancePay = swich;
            [self preSubmitWithAddreID:self.preOrderModel.addr.addrId couponArr:self.preOrderModel.coupon.couponIds?:[@[@(-1)] mutableCopy]];
        };
        return cell;
        
    }
    
    if ((indexPath.section == 4&& self.needID == YES)||(indexPath.section == 4&& self.needID == NO &&self.supportBalancePay == YES)||(indexPath.section == 3 &&self.needID == NO && self.supportBalancePay == NO)) {
        Class skuList = [OrderGoodListCell class];
        OrderGoodListCell *skuCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuList)];
        if (!skuCell) {
            skuCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuList)];
        }
        Class skuTitle = [OrderSkuTitleCell class];
        OrderSkuTitleCell *skuTitleCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuTitle)];
        if (!skuTitleCell) {
            skuTitleCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(skuTitle)];
        }
        Class common = [OrderCommonCell class];
        OrderCommonCell *commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!commonCell) {
            commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        id object = self.skuArr[indexPath.row];
        if ([object isKindOfClass:[PreOrderLIstModel class]]) {
            PreOrderLIstModel *model= self.skuArr[indexPath.row];
            skuTitleCell.titleLabel.text = model.sellerAddr;
            return skuTitleCell;
        }else if ([object isKindOfClass:[CartSkuModel class]]){
            skuCell.skuModel = self.skuArr[indexPath.row];
            return skuCell;
        }else if ([object isKindOfClass:[OrderPriceModel class]]){
            commonCell.type = CellWithTwoLabel;
            commonCell.priceModel = self.skuArr[indexPath.row];
            return commonCell;
        }else{
            commonCell.type = CellwithOneTextFild;
            int textFieldtag = 0;
            if (self.remarksArr.count == 1) {
                textFieldtag = 0;
            }else{
                if (indexPath.row < self.skuArr.count-1) {
                    id nextObject = self.skuArr[indexPath.row+1];
                    for (int i = 0 ; i < self.preOrderModel.orderList.count; i++) {
                        PreOrderLIstModel *model = self.preOrderModel.orderList[i];
                        if (nextObject == model) {
                            textFieldtag = i-1;
                        }
                    }
                }else{
                    textFieldtag = (int)self.remarksArr.count-1;
                }
            }
            commonCell.showTextField.tag = textFieldtag;
            commonCell.titleLabel.text = @"买家留言:";
            commonCell.showTextField.placeholder = @"200个字以内(选填)";
            commonCell.showTextField.delegate = self;
            commonCell.showTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [commonCell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            return commonCell;
        }
    }
    if ((indexPath.section == 5 && self.needID == YES)||(indexPath.section == 5 && self.needID == NO &&self.supportBalancePay == YES)||(indexPath.section == 4 && self.supportBalancePay == NO && self.needID==NO)){
        Class common = [OrderCommonCell class];
        OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        cell.type = CellWithTwoLabel;
        OrderPriceModel *model = self.SummaryListForCell[indexPath.row];
        cell.priceModel = model;
        return cell;
    }
    else{
        return nil;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [YYCreateTools createView:XHLightColor];
    UILabel *title = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHBlackLitColor];
    UIView *whiteView =[YYCreateTools createView:XHWhiteColor];
    whiteView.frame = CGRectMake(0, 8, kScreenW, 38);
    if(self.isSinceTheLiftProduct)  {
         if (section == 0) {
            sectionView.frame = CGRectMake(0, 0, kScreenW, 30);
            [sectionView addSubview:whiteView];
            [whiteView addSubview:title];
            title.text = @"支付方式";
            [title mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(kSizeScale(12));
                make.bottom.mas_equalTo(whiteView.mas_bottom).mas_equalTo(-4);
            }];
        } else {
            sectionView.frame = CGRectMake(0, 0, kScreenW, 8);
        }
        return sectionView;
    }
    if ((section == 4 && self.needID == NO)||(section == 5 && self.preOrderModel.needId == YES) ) {
        sectionView.frame = CGRectMake(0, 0, kScreenW, 30);
        [sectionView addSubview:whiteView];
        [whiteView addSubview:title];
        title.text = @"订单信息";
        [title mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.bottom.mas_equalTo(whiteView.mas_bottom).mas_equalTo(-4);
        }];
    }if ((section == 2 && self.needID == YES )||(section == 1 && self.needID == NO) ) {
        sectionView.frame = CGRectMake(0, 0, kScreenW, 30);
        [sectionView addSubview:whiteView];
        [whiteView addSubview:title];
        title.text = @"支付方式";
        [title mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.bottom.mas_equalTo(whiteView.mas_bottom).mas_equalTo(-4);
        }];
    }else{
        sectionView.frame = CGRectMake(0, 0, kScreenW, 8);
    }
    return sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(self.isSinceTheLiftProduct)  {
        if (section == 0) {
            return 40;
        } else {
            return 8;
        }
    }
    if (section == 0) {
        return 0;
    }else if ((section == 4 && self.needID == NO && self.supportBalancePay == YES)|| (section == 5 && self.needID == YES && self.supportBalancePay == YES)){
        return 40;
    }else if ((section == 2 && self.needID == YES)||(section == 1 && self.needID == NO)){
        return 40;
    }else{
        return 8;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.isSinceTheLiftProduct)  {
        if(indexPath.section == 2) {
            id object = self.skuArr[indexPath.row];
            if ([object isKindOfClass:[PreOrderLIstModel class]]) {
                return 0.00001f;
            }
        }
    }
    return UITableViewAutomaticDimension;
}

-(void)reloadChannelSection{
    if(self.isSinceTheLiftProduct) {
        [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
       [self.tableView reloadSections:[[NSIndexSet alloc] initWithIndex:self.preOrderModel.needId == 0?1:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.isSinceTheLiftProduct)  {
        if (indexPath.section == 0) {
            for (int i = 0; i < self.preOrderModel.channel.count; i++) {
                ChannelModel *model = self.preOrderModel.channel[i];
                if (i == indexPath.row) {
                    model.defaultFlag = 1;
                    self.channelId = model.channelId;
                }else{
                    model.defaultFlag = 0;
                }
            }
            [self reloadChannelSection];
        } else if (indexPath.section == 1) {
            [self getCouponListWithOrder:self.preOrderModel.orderToken];
        } else {

        }
        return;
    }
    if (indexPath.section == 0) {
        if (self.preOrderModel.hasAddr == 0) {
            YY_SetUpAddressViewCtrl *vc = [YY_SetUpAddressViewCtrl new];
            vc.setFinish = ^(NSString *finish) {
                [self preSubmitWithAddreID:0 couponArr:self.preOrderModel.coupon.couponIds?:[@[@(-1)] mutableCopy]];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            YY_AddressListViewCtrl *vc = [YY_AddressListViewCtrl new];
            vc.setFinish = ^(int addreID) {
            self.preOrderModel.addr.addrId = addreID;
                [self preSubmitWithAddreID:addreID couponArr:self.preOrderModel.coupon.couponIds?:[NSMutableArray array]];
            };
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if ((indexPath.section == 2&&self.preOrderModel.needId == 1)||(indexPath.section == 1&&self.preOrderModel.needId ==0) ){
        for (int i = 0; i < self.preOrderModel.channel.count; i++) {
            ChannelModel *model = self.preOrderModel.channel[i];
            if (i == indexPath.row) {
                model.defaultFlag = 1;
                self.channelId = model.channelId;
            }else{
                model.defaultFlag = 0;
            }
        }
        [self reloadChannelSection];        
    }else if ((indexPath.section == 3&&self.preOrderModel.needId == 1)||(indexPath.section == 2&&self.preOrderModel.needId ==0)){
        [self getCouponListWithOrder:self.preOrderModel.orderToken];
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    if ([textField.placeholder isEqualToString:@"添加收货人身份证姓名以便清关"]) {
        self.userName = textField.text;
    }else if ([textField.placeholder isEqualToString:@"添加收货人身份证以便清关"]){
        self.userID = textField.text;
    }
    return YES;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y>40) {
        if(!self.isSinceTheLiftProduct) {
            self.addressView.hidden = self.preOrderModel.hasAddr == 1?NO:YES;
            self.addressView.backgroundColor = HexRGB(0xFDF0D5);
        }
    }else{
        self.addressView.backgroundColor = XHClearColor;
        self.addressView.hidden = YES;
    }
}


#pragma mark 备注字段
- (void)textFieldDidChange:(UITextField *)textField {
    NSInteger kMaxLength =200;
    NSString *toBeString = textField.text;
    if (toBeString.length > kMaxLength) {
        textField.text = [toBeString substringToIndex:kMaxLength];
    }
    if (kValidArray(self.remarksArr)) {
        NSMutableDictionary *dic = self.remarksArr[textField.tag];
        [dic setObject:textField.text forKey:@"remark"];
        [self.remarksArr replaceObjectAtIndex:textField.tag withObject:dic];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
