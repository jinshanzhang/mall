//
//  UpdateCartRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UpdateCartRequestAPI.h"

@implementation UpdateCartRequestAPI

- (instancetype)initUpdateCarRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,ACTIONUPDATECART];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
