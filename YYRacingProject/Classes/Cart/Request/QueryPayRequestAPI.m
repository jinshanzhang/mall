//
//  QueryPayRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "QueryPayRequestAPI.h"

@implementation QueryPayRequestAPI

- (instancetype)initQueryPayRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,QUERYPAY];
}




- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
