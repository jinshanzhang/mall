//
//  UpdateCartRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface UpdateCartRequestAPI : YYBaseRequestAPI

- (instancetype)initUpdateCarRequest:(NSMutableDictionary *)params;


@end
