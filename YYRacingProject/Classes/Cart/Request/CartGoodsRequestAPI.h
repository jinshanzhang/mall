//
//  CartGoodsRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface CartGoodsRequestAPI : YYBaseRequestAPI

- (instancetype)initCartGoodRequest:(NSMutableDictionary *)params;

@end
