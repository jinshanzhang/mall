//
//  ReceiveCouponRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface ReceiveCouponRequestAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;

@end
