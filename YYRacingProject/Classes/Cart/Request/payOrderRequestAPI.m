//
//  payOrderRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "payOrderRequestAPI.h"

@implementation payOrderRequestAPI

- (instancetype)initOrderPayRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,PAYORDER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
