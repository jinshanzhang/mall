//
//  AddAddressRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/1.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AddAddressRequestAPI.h"

@implementation AddAddressRequestAPI


- (instancetype)initUpdateAddressRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}



- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,UPDATEADDRESS];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}


@end
