//
//  AddressListRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface AddressListRequestAPI : YYBaseRequestAPI

- (instancetype)initAddressListRequest:(NSMutableDictionary *)params;

@end



