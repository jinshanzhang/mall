//
//  BuyerCouponUseRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "BuyerCouponUseRequestAPI.h"

@implementation BuyerCouponUseRequestAPI


- (instancetype)initCouponUseRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,ORDERCOUPONLIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
