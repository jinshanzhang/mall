//
//  CartHandleRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartHandleRequestAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;


@end

NS_ASSUME_NONNULL_END
