//
//  CheckAddressRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface CheckAddressRequestAPI : YYBaseRequestAPI


- (instancetype)initCheckAddressRequest:(NSMutableDictionary *)params;

@end
