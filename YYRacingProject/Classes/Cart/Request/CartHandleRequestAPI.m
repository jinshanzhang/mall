//
//  CartHandleRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "CartHandleRequestAPI.h"

@implementation CartHandleRequestAPI


- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CARTHANDLE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
