//
//  ApplyWithDrawRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface ApplyWithDrawRequestAPI : YYBaseRequestAPI

- (instancetype)initApplyWighdrawRequest:(NSMutableDictionary *)params;

@end
