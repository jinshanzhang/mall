//
//  AddressListRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AddressListRequestAPI.h"

@implementation AddressListRequestAPI


- (instancetype)initAddressListRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LISTADDRESS];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
