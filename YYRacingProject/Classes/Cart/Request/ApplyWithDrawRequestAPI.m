//
//  ApplyWithDrawRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ApplyWithDrawRequestAPI.h"

@implementation ApplyWithDrawRequestAPI


- (instancetype)initApplyWighdrawRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,APPLYWITHDRAW];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
