//
//  QueryPayRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface QueryPayRequestAPI : YYBaseRequestAPI

- (instancetype)initQueryPayRequest:(NSMutableDictionary *)params;

@end
