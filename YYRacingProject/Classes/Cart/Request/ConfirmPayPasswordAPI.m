//
//  ConfirmPayPasswordAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ConfirmPayPasswordAPI.h"

@implementation ConfirmPayPasswordAPI


- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CONFIRMPAYPASSWORD];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
