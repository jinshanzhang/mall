//
//  CartSkuTotalCountRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CartSkuTotalCountRequestAPI.h"

@implementation CartSkuTotalCountRequestAPI

- (instancetype)initGoodSkuTotalCountRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CARTTOTALCOUNT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
