//
//  FindCommonAddressRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/31.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface FindCommonAddressRequestAPI : YYBaseRequestAPI

- (instancetype)initCommonAddressRequest:(NSMutableDictionary *)params;

@end
