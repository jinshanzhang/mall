//
//  ChangeAddressDefaultRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ChangeAddressDefaultRequestAPI.h"

@implementation ChangeAddressDefaultRequestAPI



- (instancetype)initChangeAddressDefaultRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CHANGEADDRESSFLAG];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
