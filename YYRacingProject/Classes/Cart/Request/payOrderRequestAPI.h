//
//  payOrderRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface payOrderRequestAPI : YYBaseRequestAPI


- (instancetype)initOrderPayRequest:(NSMutableDictionary *)params;

@end
