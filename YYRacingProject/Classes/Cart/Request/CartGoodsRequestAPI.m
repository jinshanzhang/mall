//
//  CartGoodsRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CartGoodsRequestAPI.h"

@interface CartGoodsRequestAPI()


@end

@implementation CartGoodsRequestAPI

- (instancetype)initCartGoodRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CART];
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
