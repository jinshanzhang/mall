//
//  PreOrderRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface PreOrderRequestAPI : YYBaseRequestAPI


- (instancetype)initPreOrderRequest:(NSMutableDictionary *)params;

@end
