//
//  SubmitOrderRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "SubmitOrderRequestAPI.h"

@implementation SubmitOrderRequestAPI


- (instancetype)initSubmitOrderRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH2,SUMMITORDER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
