//
//  ConfirmPayPasswordAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface ConfirmPayPasswordAPI : YYBaseRequestAPI


- (instancetype)initRequest:(NSMutableDictionary *)params;

@end
