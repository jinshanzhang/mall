//
//  CartSkuTotalCountRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface CartSkuTotalCountRequestAPI : YYBaseRequestAPI

- (instancetype)initGoodSkuTotalCountRequest:(NSMutableDictionary *)params;

@end
