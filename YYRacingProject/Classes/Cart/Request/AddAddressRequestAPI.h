//
//  AddAddressRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/1.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface AddAddressRequestAPI : YYBaseRequestAPI

- (instancetype)initUpdateAddressRequest:(NSMutableDictionary *)params;


@end
