//
//  PreOrderRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "PreOrderRequestAPI.h"

@implementation PreOrderRequestAPI

-(instancetype)initPreOrderRequest:(NSMutableDictionary *)params
{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,PREORDER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
