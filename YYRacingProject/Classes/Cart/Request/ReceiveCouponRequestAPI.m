//
//  ReceiveCouponRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "ReceiveCouponRequestAPI.h"

@implementation ReceiveCouponRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH2,RECEIVECOUPONS];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
