//
//  BuyerCouponUseRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface BuyerCouponUseRequestAPI : YYBaseRequestAPI

- (instancetype)initCouponUseRequest:(NSMutableDictionary *)params;


@end
