//
//  UpdateSkuCntRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UpdateSkuCntRequestAPI.h"

@implementation UpdateSkuCntRequestAPI

- (instancetype)initUpdateSkuCntRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,ACTIONUPDATESKUCNT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
