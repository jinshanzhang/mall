//
//  SubmitOrderRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface SubmitOrderRequestAPI : YYBaseRequestAPI

- (instancetype)initSubmitOrderRequest:(NSMutableDictionary *)params;

@end
