//
//  YY_OrderCompleteViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_OrderCompleteViewCtrl : YYBaseViewController

/*****订单类型 0-普通订单；1-开店大礼包订单*****/
@property (nonatomic, assign) int orderType;

@property (nonatomic, assign) int displayType;//0-正常；1-单品优惠券商品

@property (nonatomic, assign) BOOL isSelfGetOrder;


@end
