//
//  OrderCompleteViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_OrderCompleteViewCtrl.h"

#import "MineOrderSourceVivew.h"
#import "HomeBannerInfoModel.h"

#import "MineCenterInfoRequestAPI.h"
#import "YYCommonResourceAPI.h"
#import "YY_WKWebViewController.h"
#import "MineCourceCell.h"



static NSInteger requestTimes = 0;
@interface YY_OrderCompleteViewCtrl ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, assign) BOOL    isUpgrades;
@property (nonatomic, assign) BOOL    isRequestEnd;
@property (nonatomic, strong) NSTimer *userCheckTimer;
@property (nonatomic, strong) MineOrderSourceVivew *sourceView;
@property (nonatomic, strong) HomeBannerInfoModel *bannerModel;
@property (nonatomic, assign) BOOL    showSource;
@property (nonatomic, strong) UITableView *subTableView;

@property (nonatomic, strong) UIView  *headerView;

@property (nonatomic ,strong) NSDictionary *AddDic;
@property (nonatomic ,strong) NSMutableArray *itemArr;

@property (nonatomic, strong) UIView * addressView;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UILabel *nameLabel;

@end

@implementation YY_OrderCompleteViewCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"支付结果"];
    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
    
    self.AddDic = [self.parameter objectForKey:@"add"];
    self.itemArr = [self.parameter objectForKey:@"item"];
    self.displayType = [[self.parameter objectForKey:@"display"] intValue];

    self.subTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, kNavigationH, kScreenW, kScreenH-kNavigationH)];
    [self.view addSubview:_subTableView];
    _subTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _subTableView.delegate = self;
    _subTableView.dataSource = self;
    _subTableView.estimatedRowHeight = 100;
    _subTableView.backgroundColor = XHClearColor;
    Class common = [MineCourceCell class];
    registerClass(self.subTableView, common);
    
    [self commonResource];

    if (self.orderType == 1) {
        // 大礼包商品
        [YYQRCodeCenterLoading showCenterLoading:@"店铺升级中..."
                                       superView:self.view];
        [self pullUserCenter];
        self.userCheckTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.userCheckTimer forMode:NSRunLoopCommonModes];
    }
    // Do any additional setup after loading the view.
}

- (void)xh_popTopRootViewController:(BOOL)isTopRoot {
    @weakify(self);
    __block BOOL isTempRoot = isTopRoot;
    [self.navigationBar yy_setNavigationButtonImageName:@"detail_navbar_back"
                                                 isLeft:YES
                                            buttonBlock:^(UIButton *sender) {
                                                @strongify(self);
                                                if (isTempRoot) {
                                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                                }
                                                else {
                                                    NSMutableArray *array = self.navigationController.viewControllers.mutableCopy;
                                                    if ([array[0] isKindOfClass:[YY_WKWebViewController class]]) {
                                                        [YYCommonTools tabbarSelectIndex:0];
                                                        [self.navigationController popToRootViewControllerAnimated:YES];
                                                    }
                                                    [self.navigationController popViewControllerAnimated:YES];
                                                }
                                            }];
}


-(void)commonResource{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@"4" forKey:@"type"];

    YYCommonResourceAPI *api = [[YYCommonResourceAPI alloc] initRequest:dic];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
           NSMutableArray *arr =[[NSArray modelArrayWithClass:[HomeBannerInfoModel class] json:[responDict objectForKey:@"resources"]] mutableCopy];
            self.showSource =[[responDict objectForKey:@"switchFlag"] boolValue];
            if (kValidArray(arr)) {
                self.bannerModel = arr[0];
            }
            [self creatTitleView];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark 创建头部视图
- (void)creatTitleView {
    self.headerView = [YYCreateTools createView:XHClearColor];
    
    self.headerView.frame = CGRectMake(0, 0, kScreenW, self.bannerModel?kSizeScale(350):kSizeScale(200));
    if (self.displayType == 0) {
        self.headerView.height = kSizeScale(350);
    }
    [self.view addSubview:self.headerView];
    self.sourceView = [[MineOrderSourceVivew alloc] initWithFrame:CGRectZero];
    [self.headerView addSubview:self.sourceView];

    @weakify(self);
    if (self.showSource&&self.bannerModel) {
        [self.sourceView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(0);
            make.height.mas_equalTo(kSizeScale(60));
        }];
        self.sourceView.infoModel = self.bannerModel;
        self.sourceView.headerClickBlock = ^(NSMutableDictionary *params) {
            @strongify(self);
            [YYCommonTools skipMultiCombinePage:self params:params];
        };
    }
    UIImageView *headerBack_view = [YYCreateTools createImageView:@"orderComplete_view" viewModel:-1];
    [_headerView addSubview:headerBack_view];
    
    [headerBack_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headerView);
        make.top.mas_equalTo(self.bannerModel?60:0);
        make.width.mas_equalTo(kScreenW);
        make.height.mas_equalTo(kSizeScale(82));
    }];
    
    UILabel *titleLabel = [YYCreateTools createLabel:@"支付成功!" font:boldFont(18) textColor:XHWhiteColor];
    [headerBack_view addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(headerBack_view);
    }];
        
    UIImageView *icon = [YYCreateTools createImageView:@"paySuccess_icon" viewModel:-1];
    [_headerView addSubview:icon];
    
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel);
        make.right.mas_equalTo(titleLabel.mas_left).mas_offset(-8);
    }];
    
    
    UIView *bottom_view = [YYCreateTools createView:XHWhiteColor];
    [self.headerView addSubview:bottom_view];
    [bottom_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headerBack_view.mas_bottom).mas_equalTo(5);
        make.bottom.left.right.mas_equalTo(self.headerView);
    }];
    
    UILabel *detailLabel = [YYCreateTools createLabel:@"包裹将寄送到以下地址:" font:normalFont(13) textColor:XHBlackLitColor];
    [bottom_view addSubview:detailLabel];
    self.detailLabel = detailLabel;
    
    [detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.top.mas_equalTo(25);
    }];

    UILabel *nameLabel = [YYCreateTools createLabel:[self.AddDic objectForKey:@"receiverName"] font:normalFont(15) textColor:XHBlackColor];
    [bottom_view addSubview:nameLabel];
    self.nameLabel = nameLabel;
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.top.mas_equalTo(detailLabel.mas_bottom).mas_offset(18);
    }];
    
    UILabel *phoneLabel = [YYCreateTools createLabel:[self.AddDic objectForKey:@"receiverPhone"] font:normalFont(15) textColor:XHBlackColor];
    [bottom_view addSubview:phoneLabel];
    self.phoneLabel= phoneLabel;
   
    [phoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nameLabel.mas_right).mas_offset(13);
        make.top.mas_equalTo(detailLabel.mas_bottom).mas_offset(18);
    }];
    
    UILabel *addressLabel = [YYCreateTools createLabel:[self.AddDic objectForKey:@"address"] font:normalFont(13) textColor:XHBlackLitColor];
    addressLabel.numberOfLines = 0;
    [bottom_view addSubview:addressLabel];
    self.addressLabel = addressLabel;

    [addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(13);
        make.right.mas_equalTo(-13);
        make.top.mas_equalTo(nameLabel.mas_bottom).mas_offset(6);
    }];

    
    if (self.displayType == 1) {
        addressLabel.hidden = YES;
        phoneLabel.hidden = YES;
        nameLabel.hidden = YES;
    }


    UIButton *backHomePage = [YYCreateTools createBtn:@"继续购物" font:normalFont(14) textColor:XHWhiteColor];
    backHomePage.backgroundColor = XHLoginColor;

    backHomePage.layer.cornerRadius = kSizeScale(2);
    [bottom_view addSubview:backHomePage];
    
    [backHomePage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(96));
        make.bottom.mas_equalTo(-26);
        make.width.mas_equalTo(kSizeScale(80));
        make.height.mas_equalTo(kSizeScale(32));
    }];
    
    backHomePage.actionBlock = ^(UIButton *sender) {
        [YYCommonTools tabbarSelectIndex:0];
        [self.navigationController popToRootViewControllerAnimated:YES];
    };
    
    UIButton *orderDetail = [YYCreateTools createBtn:self.displayType==1?@"查看优惠券":@"查看订单" font:normalFont(14) textColor:XHWhiteColor];
    orderDetail.backgroundColor = XHLoginColor;

    orderDetail.layer.cornerRadius = kSizeScale(2);
    [bottom_view addSubview:orderDetail];
    
    [orderDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(kSizeScale(-96));
        make.bottom.mas_equalTo(-26);
        make.width.mas_equalTo(kSizeScale(80));
        make.height.mas_equalTo(kSizeScale(32));
    }];
    
    orderDetail.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        if(self.isSelfGetOrder) {
            [YYCommonTools skipOrderList:self params:@{@"source": @(1),
            @"currentPage":@(5)}];
            return;
        }
        if (self.displayType == 1) {
            [YYCommonTools skipCouponPage:self];
        } else {
            [YYCommonTools skipOrderList:self params:@{@"source": @(1),@"from":@"order"}];
        }
    };
    _subTableView.tableHeaderView = self.headerView;
    
    self.isSelfGetOrder = self.isSelfGetOrder;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark- tableview delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.displayType == 1?self.itemArr.count:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class common = [MineCourceCell class];
    MineCourceCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    groomGoodsModel *model  = self.itemArr[indexPath.row];
    cell.groomModel = model;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    groomGoodsModel *model = self.itemArr[indexPath.row];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(1) forKey:@"linkType"];
    if (kValidString(model.url)) {
        [params setObject:model.url forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:self params:params];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [YYCreateTools createView:XHLightColor];
    sectionView.frame = CGRectMake(0, 0, kScreenW, kSizeScale(50));
    UILabel *titleLable = [YYCreateTools createLabel:@"- 优惠券可用于以下商品 -" font:boldFont(16) textColor:XHBlackColor];
    [sectionView addSubview:titleLable];
    
    [titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(sectionView);
    }];
    
    return sectionView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return self.displayType == 1?kSizeScale(50):0;
}

#pragma mark - Private method
- (void)timerAction {
    if (!self.isUpgrades) {
        // 拉取用户信息检测以及升级了
        if (self.isRequestEnd) {
            [self pullUserCenter];
        }
        if (requestTimes > 3) {
            [self.userCheckTimer invalidate];
            self.userCheckTimer = nil;
            [YYCommonTools showTipMessage:@"升级失败"];
        }
    }
    else {
        [self.userCheckTimer invalidate];
        self.userCheckTimer = nil;
        kPostNotification(LoginAndQuitSuccessNotification, @(2));
        [YYCommonTools showTipMessage:@"升级成功"];
        [YYQRCodeCenterLoading hideCenterLoading:@"店铺升级中..."
                                       superView:self.view];
    }
}

- (void)pullUserCenter {
    MineCenterInfoRequestAPI *centerInfoAPI = [[MineCenterInfoRequestAPI alloc] initMineCenterInfoRequest:[[NSDictionary dictionary] mutableCopy]];
    [centerInfoAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        requestTimes ++ ;
        if (kValidDictionary(responDict)) {
            NSMutableDictionary *tempParams = [YYCommonTools objectTurnToDictionary:NO];
            if ([[responDict objectForKey:@"level"] intValue] != kUserInfo.level) {
                self.isUpgrades = YES;
            }
            [tempParams setObject:[responDict objectForKey:@"level"] forKey:@"level"];
            [kUserManager modifyUserInfo:tempParams];
            self.isRequestEnd = YES;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.isRequestEnd = YES;
        requestTimes ++;
    }];
}

- (void)setIsSelfGetOrder:(BOOL)isSelfGetOrder {
    _isSelfGetOrder = isSelfGetOrder;
    if(isSelfGetOrder) {
        self.addressLabel.hidden = YES;
        self.phoneLabel.hidden = YES;
        self.detailLabel.hidden = YES;
        self.nameLabel.hidden = YES;
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

