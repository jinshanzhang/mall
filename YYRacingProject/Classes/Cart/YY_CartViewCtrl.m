//
//  YY_CartViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_CartViewCtrl.h"
#import "CartGoodListCell.h"
#import "CartGoodsRequestAPI.h"
#import "CartGoodModel.h"
#import "UpdateSkuCntRequestAPI.h"
#import "PreOrderRequestAPI.h"
#import "CartHandleRequestAPI.h"
#import "UpdateCartRequestAPI.h"
#import "YY_OrderViewCtrl.h"
#import "PreOrderModel.h"
#import "YY_OrderUnpaidViewCtrl.h"

@interface YY_CartViewCtrl ()

@property (nonatomic, strong) UIButton *deleteUesdBtn;

@property (nonatomic, strong) UIButton *selectAllBtn;

@property (nonatomic, strong) UIButton *summitOrderBtn;

@property (nonatomic, strong) UILabel  *totalMoneyLabel;

@property (nonatomic, strong) UILabel  *totalLabel;


@property (nonatomic, strong) UIView   *footerView;

@property (nonatomic, strong) NSMutableArray *normorGoodlArr;

@property (nonatomic, strong) NSMutableArray *invalidGoodlArr;

@property (nonatomic, strong) CartGoodModel  *goodModel;

@property (nonatomic, assign) BOOL           selectAll;

@property (nonatomic, assign) float         submmitAmount;

@property (nonatomic, strong) NSMutableArray *selectToDeleteArr;
@end

@implementation YY_CartViewCtrl

#pragma mark - Life cycle
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    self.selectAllBtn.selected = YES;
    if(!kIsLogin) {
        [YYCommonTools pushToLogin];
        return;
    }
    self.selectAll = YES;
    [self loadPullDownPage];
}

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.NoDataType = YYEmptyViewCarts;
        self.emptyTitle = @"您的购物车还是空的哦";
        self.isAddPullDownRefresh = NO;
        self.isAddReachBottomView = NO;
        self.changeScrollTop = YES;
    }
    return self;
}

- (void)pullDownRefresh {
    [self loadPullDownPage];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [MobClick event:@"cart"];

    self.view.backgroundColor = XHLightColor;
    [self xh_addTitle:@"购物车"];
    if (self.hidesBottomBarWhenPushed == YES) {
        [self xh_popTopRootViewController:NO];
    }
    self.func = CartSubmit;
    [self xh_addNavigationItemWithTitle:@"管理  " isLeft:NO clickEvent:^(UIButton *sender) {
        self.func = self.func == CartSubmit?CartManage:CartSubmit;
        [self.navigationBar.rightButton setTitle:self.func == CartManage?@"完成  ":@"管理  " forState:UIControlStateNormal];
        [self.navigationBar.rightButton setTitleColor:self.func == CartManage?XHRedColor:XHBlackColor forState:UIControlStateNormal];
        int number = [self AreadyCheckSku];
        NSString *submitString =  [NSString stringWithFormat:@"结算(%lu)",(unsigned long)number];
        NSString *manageString =  [NSString stringWithFormat:@"删除(%lu)",(unsigned long)number];
        [self.summitOrderBtn setTitle:self.func == CartManage?manageString:submitString forState:UIControlStateNormal];
        [self checkSubmitBtnEnable];
    }];
    Class current = [CartGoodListCell class];
    registerClass(self.m_tableView, current);
    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.estimatedRowHeight = 108;
    self.m_tableView.allowsSelection = YES;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
   
    if (self.hidesBottomBarWhenPushed == YES) {
        [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kNavigationH);
            make.left.right.equalTo(self.view);
            make.bottom.mas_equalTo(-kBottom(50));
        }];
    }else{
        [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kNavigationH);
            make.left.right.equalTo(self.view);
            make.bottom.mas_equalTo(-50);
        }];
    }
    [self creatFooterView];

    // Do any additional setup after loading the view.
}

#pragma mark 当前已选金额
- (void)currentMoney{
    
    self.submmitAmount = 0;
    for (int i = 0;  i  < self.normorGoodlArr.count ; i++) {
        CartSkuModel *model = self.normorGoodlArr[i];
        if (model.select) {
        self.submmitAmount  = self.submmitAmount + (model.skuCnt * [model.skuPrice floatValue]);
        }
    }
    self.totalMoneyLabel.text = [NSString stringWithFormat:@"¥%.2f",self.submmitAmount ];
}

#pragma mark 当前选择的sku
-(int)AreadyCheckSku
{
    int number = 0;
    for (CartSkuModel *areadyModel in self.normorGoodlArr) {
        //==yes表示已选择
        if (areadyModel.select == YES) {
            number++;
        }
    }
    return number;
}

-(void)creatFooterView
{
    @weakify(self);
    self.footerView = [YYCreateTools createView:XHWhiteColor];
    _footerView.layer.masksToBounds = YES;
//    _footerView.layer.borderWidth = 0.5;
    _footerView.layer.borderColor = XHLightColor.CGColor;
    _footerView.hidden = YES;
    [self.view addSubview:_footerView];
    [_footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.m_tableView);
        make.top.equalTo(self.m_tableView.mas_bottom);
        make.height.mas_equalTo(50);
    }];

    //**********全选按钮**************
    self.selectAllBtn = [YYCreateTools createBtnImage:@"addressButton_icon"];
//    self.selectAllBtn.selected = YES;
    [self.selectAllBtn setImage:[UIImage imageNamed:@"addressButtonSelected_icon"] forState:UIControlStateSelected];
    [_footerView addSubview:self.selectAllBtn];
    
    [self.selectAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(2);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kSizeScale(kSizeScale(40)));
    }];
    
    self.selectAllBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self)
        
        self.selectAllBtn.selected = !self.selectAllBtn.selected;
        self.selectAll = !self.selectAll;
        NSMutableArray *changeArr = [NSMutableArray array];
        
        for (CartSkuModel *model  in self.normorGoodlArr) {
            model.pitchOn = self.selectAll;
            model.select = model.pitchOn;
            [changeArr addObject:@(model.skuId)];
        }
        [self handleSkuCntWithType:self.selectAll?@(0):@(1) skuArr:changeArr];
        
        [self.m_tableView reloadData];
        
        if (!self.selectAll) {
            //点击全选也要区分是在结算状态下还是管理删除状态下
            [self.summitOrderBtn setTitle:self.func == CartSubmit?@"结算(0)":@"删除(0)" forState:UIControlStateNormal];
        }else{
            int number = [self AreadyCheckSku];
            [self.summitOrderBtn setTitle:[NSString stringWithFormat:self.func == CartSubmit?@"结算(%d)":@"删除(%d)",number] forState:UIControlStateNormal];
         }
        
        [self checkSubmitBtnEnable];
        [self currentMoney];
    };
    
    UILabel *wordLabel = [YYCreateTools createLabel:@"全选" font:normalFont(15) textColor:XHBlackColor];
    [_footerView addSubview:wordLabel];
    [wordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.selectAllBtn.mas_right);
        make.top.bottom.mas_equalTo(0);
        make.width.mas_equalTo(kSizeScale(kSizeScale(40)));
    }];
    
    //**********提交按钮**************
    self.summitOrderBtn = [YYCreateTools createBtn:@"" font:normalFont(17) textColor:XHWhiteColor];
    [_footerView addSubview:self.summitOrderBtn];

    [self.summitOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(0);
        make.right.mas_equalTo(1);
        make.top.mas_equalTo(-1);
        make.width.mas_equalTo(kSizeScale(kSizeScale(125)));
    }];
    
//    [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:[UIColor addGoldGradient:CGSizeMake(kSizeScale(125), kSizeScale(50))]] forState:UIControlStateNormal];
    [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:XHMainColor] forState:UIControlStateNormal];
    
#warning 订单拦截
    __block YY_CartViewCtrl *  blockSelf = self;
    self.summitOrderBtn.actionBlock = ^(UIButton *sender) {
        if (self.func == CartSubmit) {
#pragma mark 预提交订单
            NSMutableArray *arr = [NSMutableArray array];
//            NSMutableArray *marr = [NSMutableArray array];
            for (CartSkuModel *model in weak_self.normorGoodlArr) {
                    //选中状态
                if (model.select == YES) {
                    [arr addObject:@{@"skuId": @(model.skuId),@"skuCnt":@(model.skuCnt)}];
//                    if(model.goodsType == 1) {
//                        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"只支持同一个店铺所选商品全部是自提商品才能一起下单" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
//                        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确定" Click:^{
//
//                        } type:JCAlertViewTypeDefault];
//                        return;
//                    }
                }
             }
            [kWholeConfig submitOrderWithSkulist:arr souce:0 addID:0 previewSwitch:1 couponId:@[@(-1)] payment:@"" deliveryType:@""  block:^(PreOrderModel *model) {
                [YYCommonTools skipPreOrder:weak_self params:model skuArr:arr source:0];
            } failBlock:^(PreOrderModel *model) {
                NSLog(@"获取到的数据是:%@",model.className);
            }];
        }else{
            blockSelf.selectToDeleteArr = [NSMutableArray array];
            NSMutableArray *arr = [NSMutableArray array];
            NSInteger index = 0;
            for (int i = 0; i < blockSelf.normorGoodlArr.count; i++) {
                CartSkuModel *model = blockSelf.normorGoodlArr[i];
                if (model.select) {
                    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                    [dic setObject:@(model.skuId) forKey:@"skuId"];
                    [dic setObject:@(model.skuCnt) forKey:@"skuCnt"];
                    [blockSelf.selectToDeleteArr addObject:model];
                    [arr addObject:dic];
                    index = i;
                }
            }
            [JCAlertView showTwoButtonsWithTitle:@"提示" Message:[NSString stringWithFormat:@"确定要删除这%lu件商品吗?",(unsigned long)arr.count] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
            } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确定" Click:^{
                [weak_self deleteCartArr:arr index:index type:0];
            } type:JCAlertViewTypeDefault];
        }
    };
    


    self.totalMoneyLabel = [YYCreateTools createLabel:@"0" font:normalFont(15) textColor:XHRedColor];

    [_footerView addSubview:self.totalMoneyLabel];
    self.totalMoneyLabel.textAlignment = NSTextAlignmentCenter;
    [self.totalMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(0);
        make.right.mas_equalTo(self.summitOrderBtn.mas_left).mas_offset(-8);
    }];
    
    self.totalLabel = [YYCreateTools createLabel:@"总计: " font:normalFont(15) textColor:XHBlackColor];
    [_footerView addSubview:self.totalLabel];
    
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.totalMoneyLabel);
        make.right.mas_equalTo(self.totalMoneyLabel.mas_left);
    }];
}

#pragma mark 删除购物车
-(void)deleteCartArr:(NSMutableArray *)arr index:(NSInteger)index type:(NSInteger )type{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:arr forKey:@"skuList"];
    [dic setObject:@(1) forKey:@"action"];
    [YYCenterLoading showCenterLoading];
    UpdateCartRequestAPI *api = [[UpdateCartRequestAPI alloc] initUpdateCarRequest:dic];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            //type == 0 删除的是有效商品 1是无效商品
            if (type == 0) {
              if (arr.count == 1) {
                CartSkuModel *model = self.normorGoodlArr[index];
                [self.normorGoodlArr removeObject:model];
              }else{
                  
                for (CartSkuModel *model in self.selectToDeleteArr) {
                    [self.normorGoodlArr removeObject:model];
               }
              }
            }else{
                self.invalidGoodlArr  = [NSMutableArray array];
                self.goodModel.expiredSkuList = self.invalidGoodlArr;
            }
            
            [self.m_tableView reloadData];
            [self currentMoney];
            int number = [self AreadyCheckSku];
            [self.summitOrderBtn setTitle:[NSString stringWithFormat:self.func == CartSubmit?@"结算(%lu)":@"删除(%lu)",(unsigned long)number] forState:UIControlStateNormal];
            if (self.normorGoodlArr.count == 0 && self.invalidGoodlArr.count == 0) {
                [self loadNewer];
            }
            //删除商品改变购物车下标数字
            [RJBadgeController setBadgeForKeyPath:kShippingSubDetailRedCountPath count:self.normorGoodlArr.count];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
    
}



#pragma mark 开关选择状态
-(void)handleSkuCntWithType:(NSNumber *)type skuArr:(NSMutableArray *)skuArr;
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:type forKey:@"type"];
    [dic setObject:skuArr forKey:@"skuIds"];
    
    CartHandleRequestAPI *api = [[CartHandleRequestAPI alloc] initRequest:dic];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}
#pragma mark 加减sku数量
-(void)updateSkuCntWithModel:(CartSkuModel *)model index:(NSInteger)index count:(NSInteger)count
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    NSMutableDictionary *skudic = [NSMutableDictionary dictionary];
    [skudic setObject:@(model.skuId) forKey:@"skuId"];
    [skudic setObject:@(count) forKey:@"skuCnt"];
    [dic setObject:@[skudic] forKey:@"skuList"];

    [YYCenterLoading showCenterLoading];
    UpdateSkuCntRequestAPI *api = [[UpdateSkuCntRequestAPI alloc] initUpdateSkuCntRequest:dic];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            //增加删减后返回了最近的sku信息
            CartSkuModel *model = [CartSkuModel modelWithJSON:[responDict objectForKey:@"skuList"][0]];
            [self.normorGoodlArr replaceObjectAtIndex:index withObject:model];
//            [self.m_tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)deleteInvalidSku{
    NSMutableArray *arr = [NSMutableArray array];
    for (CartSkuModel *model  in self.invalidGoodlArr) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:@(model.skuId) forKey:@"skuId"];
        [dic setObject:@(model.skuCnt) forKey:@"skuCnt"];
        [arr addObject:dic];
    }
    [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确认清空失效宝贝吗?" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
    } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确定" Click:^{
        [self deleteCartArr:arr index:1 type:1];
    } type:JCAlertViewTypeDefault];
    
}
// 判断结算,删除是否能点击
-(void)checkSubmitBtnEnable{
    int number = 0;
    for (CartSkuModel *areadyModel in self.normorGoodlArr) {
        //==yes表示已选择
        if (areadyModel.select == YES) {
            number++;
        }
    }
    if (number == 0) {
        [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:XHBlackLitColor] forState:UIControlStateNormal];
        self.summitOrderBtn.enabled = NO;
    }else{
        self.summitOrderBtn.enabled = YES;

//        [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:self.func == CartSubmit?[UIColor addGoldGradient:CGSizeMake(kSizeScale(125), kSizeScale(50))]:XHRedColor] forState:UIControlStateNormal];
        [self.summitOrderBtn setBackgroundImage:[UIImage imageWithColor:self.func == CartSubmit?XHMainColor:XHRedColor] forState:UIControlStateNormal];
 
    }
}


#pragma mark - Base
- (YYBaseRequestAPI *)baseRequest {
    CartGoodsRequestAPI *collectAPI = [[CartGoodsRequestAPI alloc] initCartGoodRequest:[[NSDictionary dictionary] mutableCopy]];
    return collectAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.invalidGoodlArr = [NSMutableArray array];
        self.normorGoodlArr  = [NSMutableArray array];
        self.goodModel = [CartGoodModel modelWithJSON:responseDic];
        //正常商品数组
        self.normorGoodlArr = self.goodModel.skuList;
        //失效商品数组
        self.invalidGoodlArr = self.goodModel.expiredSkuList;
        
        //刚进页面判断是不是全选状态
        for (CartSkuModel *model in self.normorGoodlArr) {
            model.select = model.pitchOn;
        }

        if ( kValidArray(self.normorGoodlArr)|| kValidArray(self.invalidGoodlArr)) {
            self.footerView.hidden= NO;
        }else{
            self.footerView.hidden= YES;
        }
        
        self.totalMoneyLabel.text = [NSString stringWithFormat:@"¥%@",self.goodModel.totalAmount];
        self.submmitAmount = [self.goodModel.totalAmount floatValue];
        int number = [self AreadyCheckSku];
    
       //判断刚进入是否是全选状态
        self.selectAll = number==self.normorGoodlArr.count?YES:NO;
        self.selectAllBtn.selected =number==self.normorGoodlArr.count?YES:NO;
        
        [self.summitOrderBtn setTitle:[NSString stringWithFormat:self.func == CartSubmit?@"结算(%d)":@"删除(%d)",number] forState:UIControlStateNormal];

        [RJBadgeController setBadgeForKeyPath:kShippingSubDetailRedCountPath count:self.normorGoodlArr.count];
        [self.m_tableView reloadData];
        [self checkSubmitBtnEnable];
        [self currentMoney];
        return self.normorGoodlArr.count!=0?self.normorGoodlArr:self.invalidGoodlArr;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return NO;
}

#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (kValidArray(self.goodModel.expiredSkuList)) {
        return 2;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 8)];
    headerView.backgroundColor = XHClearColor;
    return headerView;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.goodModel.skuList.count;
    }else{
        if (self.goodModel.expiredSkuList.count>0) {
            return self.goodModel.expiredSkuList.count + 1;
        }else{
            return 0;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1 && indexPath.row == self.invalidGoodlArr.count) {
        static NSString * ID = @"myCell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:ID];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        }
        if (!self.deleteUesdBtn) {
            self.deleteUesdBtn = [YYCreateTools createBtn:@"清空失效宝贝"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [_deleteUesdBtn setTitleColor:XHLoginColor forState:UIControlStateNormal];
        _deleteUesdBtn.titleLabel.font = normalFont(13);
        _deleteUesdBtn.layer.borderColor = XHLoginColor.CGColor;
        _deleteUesdBtn.layer.borderWidth = kSizeScale(0.5);
        _deleteUesdBtn.layer.cornerRadius = kSizeScale(kSizeScale(15));
        [cell addSubview:_deleteUesdBtn];
        [_deleteUesdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(cell);
            make.width.mas_offset(kSizeScale(105));
            make.height.mas_offset(kSizeScale(32));
        }];
        //一键删除无效商品
        [_deleteUesdBtn addTarget:self action:@selector(deleteInvalidSku) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else{
      Class currentCls = [CartGoodListCell class];
      CartGoodListCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
      if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.backgroundColor = XHWhiteColor;
      }
        if (indexPath.section == 0) {
            cell.skuModel = self.normorGoodlArr[indexPath.row];
//            cell.selectAll = self.selectAll;
            //点击勾选的回调
            cell.cellClickBlock = ^(float amount, CartSkuModel *model) {
                
            NSMutableArray *skuArr = [NSMutableArray arrayWithObject:@(model.skuId)];

            [self handleSkuCntWithType:cell.selectBtn.selected == YES?@(0):@(1) skuArr:skuArr];
            //算价格
            self.submmitAmount =self.submmitAmount -amount;
            self.totalMoneyLabel.text =[NSString stringWithFormat:@"¥%.2f",self.submmitAmount];
            [self.normorGoodlArr replaceObjectAtIndex:indexPath.row withObject:model];
            //number 已经选择sku的数量
            int number = [self AreadyCheckSku];
            [self.summitOrderBtn setTitle:[NSString stringWithFormat:self.func == CartSubmit?@"结算(%lu)":@"删除(%lu)",(unsigned long)number] forState:UIControlStateNormal];
                //全选按钮状态 判断已选sku是否等于总的sku
                self.selectAllBtn.selected = number == self.normorGoodlArr.count?YES:NO;
                //是否全选状态
                self.selectAll = number ==self.normorGoodlArr.count?YES:NO;
                [self checkSubmitBtnEnable];
            };
            //点击数量按钮回调
            cell.cellNumerBlock = ^(NSInteger skuCnt, CartSkuModel *model) {
            [self updateSkuCntWithModel:model index:indexPath.row count:skuCnt];
            };
        }else{
            cell.skuModel = self.invalidGoodlArr[indexPath.row];
        }
    return cell;
    }
}

-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *rowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
         CartSkuModel *model = self.normorGoodlArr[indexPath.row];
         NSMutableDictionary *skudic = [NSMutableDictionary dictionary];
         [skudic setObject:@(model.skuId) forKey:@"skuId"];
         [skudic setObject:@(model.skuCnt) forKey:@"skuCnt"];
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确定要删除这1件商品吗?" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确定" Click:^{
            [self deleteCartArr:[@[skudic] mutableCopy] index:indexPath.row type:0];
        } type:JCAlertViewTypeDefault];
    }];
    rowAction.backgroundColor = XHLoginColor;
    NSArray *arr = @[rowAction];
    return arr;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1&& indexPath.row == self.goodModel.expiredSkuList.count) {
        return 70;
    }else{
        return UITableViewAutomaticDimension;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 ) {
        return YES;
    }else{
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CartSkuModel *model;
    if (indexPath.section == 0) {
        model = _normorGoodlArr[indexPath.row];
    }
    else {
        if (indexPath.row == _invalidGoodlArr.count) {
            return;
        }
        model = _invalidGoodlArr[indexPath.row];
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(model.itemUri)) {
        [params setObject:model.itemUri forKey:@"url"];
    }
    [params setObject:@(1) forKey:@"linkType"];
    [YYCommonTools skipMultiCombinePage:self params:params];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
