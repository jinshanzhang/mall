//
//  YY_SetUpAddressViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_SetUpAddressViewCtrl.h"
#import "OrderCommonCell.h"
#import "UITextView+Placeholder.h"
#import "YY_AddressListViewCtrl.h"
#import "ChooseLocationView.h"
#import "AddAddressRequestAPI.h"

@interface YY_SetUpAddressViewCtrl ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,YYTextViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) YYTextView  *detailTextView;

@property (nonatomic, strong) NSArray *titleArr;

@property (nonatomic, strong) UISwitch *chooseSwich;
@property (nonatomic, strong) UIView   *footerView;
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UILabel  *addressLabel;

@property (nonatomic, strong) ChooseLocationView *chooseLocationView;
@property (nonatomic,strong)  UIView  *cover;



@end

@implementation YY_SetUpAddressViewCtrl


-(UITableView *)tableView
{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.backgroundColor = XHLightColor;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_addTitle:self.title?:@"新增收货地址"];
    [self xh_popTopRootViewController:NO];
    [self initFooterView];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tap1.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap1];
    
    self.name = self.receiveModel.contactName;
    self.phone = self.receiveModel.phone;
    self.address = self.receiveModel.address;
    
    
    self.provinceId = self.receiveModel.provinceId;
    self.cityId = self.receiveModel.cityId;
    self.regionId = self.receiveModel.regionId;
    self.streetId = self.receiveModel.streetId;
    
    
    
    self.defaultFlag = self.receiveModel.defaultFlag;
    
    self.titleArr = [NSArray arrayWithObjects:@"收货人",@"联系电话",@"所在地区",@"详细地址", nil];
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = self.footerView;
    // Do any additional setup after loading the view.
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.bottom.equalTo(self.view);
    }];
    Class common = [OrderCommonCell class];
    registerClass(self.tableView, common);
    [self.view addSubview:self.cover];
    
    if (!self.receiveModel.province) {
        return;
    }
    NSMutableArray *codeArr = [NSMutableArray arrayWithObjects:@(self.receiveModel.provinceId),@(self.receiveModel.cityId),@(self.receiveModel.regionId),@(self.receiveModel.streetId), nil];
    NSMutableArray *titleArr = [NSMutableArray arrayWithObjects:self.receiveModel.province,self.receiveModel.city,self.receiveModel.region,self.receiveModel.street, nil];

    if (!self.receiveModel.street) {
        [codeArr removeLastObject];
        [titleArr removeLastObject];
    }
    self.chooseLocationView.codeArr = codeArr;
    self.chooseLocationView.titleArr = titleArr;
}
-(void)viewTapped:(UITapGestureRecognizer*)tap1
{
    [self.view endEditing:YES];
}

-(void)initFooterView {
    @weakify(self);
    self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(150))];
    self.submitBtn = [YYCreateTools createBtn:@"保存" font:normalFont(17) textColor:XHWhiteColor];
    self.submitBtn.backgroundColor = XHLoginColor;
    self.submitBtn.v_cornerRadius = 5;
    [self.footerView addSubview:self.submitBtn];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(20);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(49);
    }];
    self.submitBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self updateAddress];
    };
}

-(void)updateAddress{
    if (self.phone.length != 11 ||![[self.phone substringToIndex:1] isEqualToString:@"1"]) {
        [YYCommonTools showTipMessage:@"电话格式不正确，请重新确认"];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.provinceId)?:@(0) forKey:@"provinceId"];
    [dict setObject:@(0) forKey:@"countryId"];
    [dict setObject:@(self.cityId)?:@(0) forKey:@"cityId"];
    [dict setObject:@(self.regionId)?:@(0) forKey:@"regionId"];
    [dict setObject:@(self.streetId)?:@(0) forKey:@"streetId"];
    [dict setObject:self.detailTextView.text?:self.address forKey:@"address"];
    [dict setObject:self.phone?:@"" forKey:@"phone"];
    [dict setObject:self.name?:@"" forKey:@"contactName"];
    [dict setObject:@(self.defaultFlag) forKey:@"defaultFlag"];
    [dict setObject:@(self.receiveModel.receiveId)?:@(0) forKey:@"id"];

    [YYCenterLoading showCenterLoading];
    AddAddressRequestAPI *api = [[AddAddressRequestAPI alloc] initUpdateAddressRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            if (self.setFinish) {
                self.setFinish(@"");
            }
         [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.receiveModel.defaultFlag == 1) {
        return 1;
    }
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 4;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        Class common = [OrderCommonCell class];
        OrderCommonCell *commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        if (!commonCell) {
            commonCell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
        }
        commonCell.titleLabel.text = self.titleArr[indexPath.row];
        if (indexPath.row<2) {
            commonCell.type = CellwithOneTextFild;
            commonCell.showTextField.placeholder = indexPath.row==0?@"请输入收货人姓名":@"请输入收货人联系电话";
            commonCell.showTextField.text = indexPath.row==0?self.name:self.phone;
            commonCell.showTextField.delegate = self;
            commonCell.showTextField.tag = indexPath.row;
            commonCell.showTextField.font = normalFont(14);
            if (indexPath.row == 1) {
                commonCell.showTextField.keyboardType = UIKeyboardTypeNumberPad;
            }
            [commonCell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        }
        if (indexPath.row == 3) {
        commonCell.type = CellWithTextView;
        commonCell.showTextView.placeholderText = @"请输入详细地址信息，如道路、门牌号、小区、楼栋号、单元室等";
        commonCell.showTextView.delegate = self;
        commonCell.showTextView.font = normalFont(14);
        commonCell.showTextView.text = self.address;
//            self.detailTextView.returnKeyType = UIReturnKeyDone;
        self.detailTextView = commonCell.showTextView;
        }
        
        if (indexPath.row == 2) {
            commonCell.type = CellWithTwoLabel;
            self.addressLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
            [commonCell addSubview:self.addressLabel];
            self.addressLabel.text = [NSString stringWithFormat:@"%@%@%@%@",self.receiveModel.province?:@"",self.receiveModel.city?:@"",self.receiveModel.region?:@"",self.receiveModel.street?:@"请选择"];
            [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(commonCell);
                make.left.mas_equalTo(kSizeScale(95));
                make.right.mas_equalTo(kSizeScale(-40));
            }];
            
            if ([self.addressLabel.text isEqualToString:@"请选择"]) {
                self.addressLabel.textAlignment = NSTextAlignmentRight;
            }else{
                self.addressLabel.textAlignment = NSTextAlignmentLeft;
            }
            
            UIImageView *arrowImageView = [YYCreateTools createImageView:@"cellMore_icon"
                                                       viewModel:-1];
            [commonCell addSubview:arrowImageView];
            
            [arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(kSizeScale(5),kSizeScale(9)));
                make.centerY.equalTo(commonCell);
                make.right.mas_equalTo(kSizeScale(-16));
            }];
        }
        commonCell.titleLabel.font = normalFont(14);
        return commonCell;
    }else{
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cellIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *titleLabel = [YYCreateTools createLabel:@"设为默认地址" font:normalFont(14) textColor:XHBlackColor];
        [cell addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell);
            make.left.mas_equalTo(10);
        }];
        self.chooseSwich = [[UISwitch alloc] initWithFrame:CGRectZero];
        self.chooseSwich.onTintColor = XHLoginColor;
        [cell addSubview:self.chooseSwich];
        [self.chooseSwich mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(cell);
            make.right.mas_equalTo(-15);
        }];
        [self.chooseSwich addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventValueChanged];
        return cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 3) {
            return 60;
        }else{
            return 44;
        }
    }else{
        return 44;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return section == 0?0:10;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 10)];
    headerView.backgroundColor = XHClearColor;
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        [UIView animateWithDuration:0.25 animations:^{
        }];
        self.cover.hidden = !self.cover.hidden;
        self.chooseLocationView.hidden = self.cover.hidden;
    }
}

#pragma mark private method
-(void)switchAction:(id)sender
{
    self.defaultFlag = self.chooseSwich.on?1:0;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_chooseLocationView.frame, point)){
        return NO;
    }
    return YES;
}

- (void)tapCover:(UITapGestureRecognizer *)tap{
    _cover.hidden = YES;
}

- (UIView *)cover{
    if (!_cover) {
        _cover = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _cover.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        [_cover addSubview:self.chooseLocationView];
        __weak typeof (self) weakSelf = self;
        _chooseLocationView.chooseFinish = ^(NSString *address, NSMutableArray *idArr)
        {
            weakSelf.provinceId = [idArr[0] intValue];
            weakSelf.cityId = [idArr[1] intValue];
            weakSelf.regionId = [idArr[2] intValue];
            if (idArr.count>3) {
                weakSelf.streetId =[idArr[3] intValue];
            }
            [UIView animateWithDuration:0.25 animations:^{
                weakSelf.addressLabel.text = address;
                weakSelf.addressLabel.textAlignment = NSTextAlignmentLeft;
                weakSelf.view.transform = CGAffineTransformIdentity;
                weakSelf.cover.hidden = YES;
            }];
        };
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapCover:)];
        [_cover addGestureRecognizer:tap];
        tap.delegate = self;
        _cover.hidden = YES;
    }
    return _cover;
}

- (ChooseLocationView *)chooseLocationView{
    @weakify(self);
    if (!_chooseLocationView) {
        _chooseLocationView = [[ChooseLocationView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 350, [UIScreen mainScreen].bounds.size.width, 350)];
        _chooseLocationView.close = ^{
            @strongify(self);
            self.cover.hidden = YES;
        };
    }
    return _chooseLocationView;
}


- (void)textFieldDidChange:(UITextField *)textField {
    
    if (textField.tag == 0) {
    NSInteger kMaxLength = 12;
    NSString *toBeString = textField.text;
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage; //ios7之前使用[UITextInputMode currentInputMode].primaryLanguage
    if ([lang isEqualToString:@"zh-Hans"]) { //中文输入
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        if (!position) {// 没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
    }else {//中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
    self.name = textField.text;
    NSLog(@"________%@",self.name);
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag == 1) {
        if (textField.text.length +string.length >11) {
            return NO;
        }
        self.phone = newString;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ([textView.text length] > 50) {
        textView.text = [textView.text substringWithRange:NSMakeRange(0, 50)];
        [textView.undoManager removeAllActions];
        [textView becomeFirstResponder];
        return;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
