//
//  YY_SetUpAddressViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"
#import "AddressModel.h"
#import "ReceiveAddressModel.h"

@interface YY_SetUpAddressViewCtrl : YYBaseViewController


@property (nonatomic, assign) int provinceId;
@property (nonatomic, assign) int cityId;
@property (nonatomic, assign) int regionId;
@property (nonatomic, assign) int streetId;
@property (nonatomic, assign) int defaultFlag;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *address;

@property (nonatomic, strong) ReceiveAddressModel *receiveModel;
@property (nonatomic, copy) void(^setFinish)(NSString *finish);

@end
