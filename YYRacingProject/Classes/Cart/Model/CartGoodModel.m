//
//  CartGoodModel.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CartGoodModel.h"

@implementation CartGoodModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{@"skuList":[CartSkuModel class],@"expiredSkuList":[CartSkuModel class]};
}

@end
