//
//  PreOrderModel.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "PreOrderModel.h"

@implementation PreOrderModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{@"channel":[ChannelModel class],@"orderList":[PreOrderLIstModel class],@"coupon":[OrderCouponModel class],@"balance":[BalanceModel class],@"summaryList":[OrderPriceModel class],@"sellerShopsVO":[SellerShopModel class]};
}


@end
