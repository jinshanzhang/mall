//
//  PreOrderLIstModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CartSkuModel.h"
#import "ChannelModel.h"
#import "OrderPriceModel.h"

@interface PreOrderLIstModel : NSObject

@property (nonatomic, copy) NSString *brandName;

@property (nonatomic, copy) NSString *sellerName;

@property (nonatomic, copy) NSString *sellerAddr;

@property (nonatomic, strong) NSArray *skuList;

@property (nonatomic, strong) NSArray *summaryList;

@property (nonatomic, assign) int previewSubOrderId;

@end
