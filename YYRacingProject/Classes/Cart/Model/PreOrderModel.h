//
//  PreOrderModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"
#import "ChannelModel.h"
#import "PreOrderLIstModel.h"
#import "OrderCouponModel.h"
#import "BalanceModel.h"
#import "OrderPriceModel.h"
#import "SellerShopModel.h"

@interface PreOrderModel : NSObject

//是否有默认地址：0-无；1-有
@property (nonatomic, assign) int hasAddr;
//默认地址信息
@property (nonatomic ,strong) AddressModel *addr;
//是否需要填身份证信息 0-否；1-是
@property (nonatomic, assign) int needId;
//支付通道
@property (nonatomic, strong) NSMutableArray *channel;
//小计信息
@property (nonatomic, strong) NSArray *summaryList;

@property (nonatomic, strong) NSArray *orderList;

@property (nonatomic, copy)  NSString *orderToken;
//优惠券信息
@property (nonatomic, strong) OrderCouponModel *coupon;
//判断开关
@property (nonatomic, assign) int operation;

@property (nonatomic, strong) NSNumber * deliveryType;

@property (nonatomic, strong) BalanceModel  *balance;

@property (nonatomic, strong) SellerShopModel * sellerShopsVO;

//饭卡支付
@property (nonatomic, assign) BOOL isMealZonePay;

//@property (nonatomic, strong) NSNumber * goodsType;

@end
