//
//  AddressModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject

//地址id
@property (nonatomic, assign) int addrId;

@property (nonatomic, assign) int addressId;
//收货人姓名
@property (nonatomic, copy) NSString *receiverName;

@property (nonatomic, copy) NSString *contactName;
//收货人电话
@property (nonatomic, copy) NSString *receiverPhone;

@property (nonatomic, copy) NSString *phone;
//收货人地址
@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *addressInfo;
//是否默认
@property (nonatomic, assign) int  defaultFlag;

@property (nonatomic, copy) NSString  *title;

@end
