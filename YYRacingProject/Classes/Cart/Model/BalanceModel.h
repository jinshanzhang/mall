//
//  BalanceModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BalanceModel : NSObject

@property (nonatomic, copy) NSString *totalAmount;
@property (nonatomic, copy) NSString *payAmount;

@end
