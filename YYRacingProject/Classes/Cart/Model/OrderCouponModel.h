//
//  OrderCouponModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/31.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderCouponModel : NSObject

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *reducedPrice;

@property (nonatomic, strong) NSMutableArray *couponIds;

@end

