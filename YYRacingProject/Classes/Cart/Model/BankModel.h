//
//  BankModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankModel : NSObject

@property (nonatomic, assign) int  bankId;

@property (nonatomic, strong) NSString  *name;

@property (nonatomic, assign) BOOL  select;

@end
