//
//  CartGoodModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CartSkuModel.h"

@interface CartGoodModel : NSObject

@property (nonatomic, strong) NSMutableArray *skuList;

@property (nonatomic, strong) NSMutableArray *expiredSkuList;


@property (nonatomic, copy) NSString *totalAmount;

@end
