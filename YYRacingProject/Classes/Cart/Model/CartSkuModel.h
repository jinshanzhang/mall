//
//  CartSkuModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CartSkuModel : NSObject

//skuId
@property (nonatomic, assign) int skuId;
//个数
@property (nonatomic, assign) int skuCnt;
//状态 0-正常；1-失效
@property (nonatomic, assign) int skuStatus;
//下单限购个数
@property (nonatomic, assign) int limitCnt;
//库存量
@property (nonatomic, assign) int totalCnt;
//商品id
@property (nonatomic, assign) int itemId;
//商品URL
@property (nonatomic, copy) NSString *itemUri;
//商品标题
@property (nonatomic, copy) NSString *skuTitle;
//sku属性信息
@property (nonatomic, copy) NSString *skuProp;
//单价
@property (nonatomic, copy) NSString *skuPrice;

@property (nonatomic, copy) NSString  *imageUrl;
//商品主图
@property (nonatomic, copy) NSString *skuCover;

@property (nonatomic, assign) BOOL    select;   //情况是反的

@property (nonatomic, strong) NSDictionary *price;

@property (nonatomic, assign) BOOL  pitchOn;

@property (nonatomic, assign) NSInteger goodsType;


@end
