//
//  ReceiveAddressModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReceiveAddressModel : NSObject

//地址id
@property (nonatomic, assign) int receiveId;
//省市区ID
@property (nonatomic, assign) int provinceId;
@property (nonatomic, assign) int cityId;
@property (nonatomic, assign) int regionId;
@property (nonatomic, assign) int streetId;


@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *region;
@property (nonatomic, copy) NSString *street;

//收货人姓名
@property (nonatomic, copy) NSString *contactName;
//收货人电话
@property (nonatomic, copy) NSString *phone;
//收货人地址
@property (nonatomic, copy) NSString *address;
//是否默认
@property (nonatomic, assign) int  defaultFlag;
@end
