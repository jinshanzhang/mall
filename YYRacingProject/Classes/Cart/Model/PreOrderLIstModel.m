//
//  PreOrderLIstModel.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "PreOrderLIstModel.h"

@implementation PreOrderLIstModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{@"skuList":[CartSkuModel class],@"summaryList":[OrderPriceModel class]};
}

@end
