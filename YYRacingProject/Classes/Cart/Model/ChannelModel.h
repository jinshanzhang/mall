//
//  ChannelModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/7/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChannelModel : NSObject
//支付渠道id
@property (nonatomic, assign) int channelId;
//是否默认 0-否；1-是
@property (nonatomic, assign) int defaultFlag;
//渠道名
@property (nonatomic, copy) NSString *channelName;
//渠道logo
@property (nonatomic, copy) NSString *channelUrl;
//说明
@property (nonatomic, copy) NSString *info;




@end
