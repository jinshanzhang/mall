//
//  OrderPriceModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderPriceModel : NSObject

@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, copy) NSString *priceTag;

@property (nonatomic, assign) int bold;
@property (nonatomic, copy)  NSString *color;
@property (nonatomic, copy)  NSString *value;

@end
