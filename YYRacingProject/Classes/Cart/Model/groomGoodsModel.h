//
//  groomGoodsModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/2.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface groomGoodsModel : NSObject

@property (nonatomic, assign) int  appItemId;//商品Id

@property (nonatomic, copy) NSString  *url;//跳转链接

@property (nonatomic, copy) NSString  *title;

@property (nonatomic, copy) NSString  *price;
@property (nonatomic, copy) NSString  *commission;

@property (nonatomic, copy) NSString  *imageUrl;

@property (nonatomic, copy) NSString  *originPrice;


@end

NS_ASSUME_NONNULL_END
