//
//  StoreHeaderInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreHeaderInfoView : UIView

@property (nonatomic, strong) NSString *storeHeadImage;
@property (nonatomic, strong) NSString *storeName;
@property (nonatomic, strong) NSString *storeInviteCode;  //店铺邀请码

@property (nonatomic, copy) void (^headerClickBlock)(NSInteger type);

- (void)setHeaderInComeView:(NSMutableArray *)tipArrs
                     moneys:(NSMutableArray *)moneys;

@end
