//
//  StoreOrderInfoCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/6.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "StoreOrderInfoCell.h"

#define  OrderStatuBtnTag  300

@interface StoreOrderInfoCell ()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UILabel *leftTipLabel;
@property (nonatomic, strong) UILabel *rightTipLabel;

@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIView  *bottomLine;

@property (nonatomic, strong) UIView  *bottomView;
@property (nonatomic, strong) NSMutableArray *btnArrs;
@property (nonatomic, strong) NSMutableArray *itemArrs;

@end

@implementation StoreOrderInfoCell

-(void)createUI{
    self.contentView.backgroundColor = XHStoreGrayColor;
    self.topView = [YYCreateTools createView:XHWhiteColor];
    self.topView.v_cornerRadius  = 5;
    [self addSubview:self.topView];
    
    self.leftTipLabel = [YYCreateTools createLabel:@"我的订单"
                                              font:boldFont(15)
                                         textColor:XHBlackColor];
    [self.topView addSubview:self.leftTipLabel];
    
    self.rightTipLabel = [YYCreateTools createLabel:@"查看全部订单"
                                               font:midFont(13)
                                          textColor:XHBlackLitColor];
    [self.topView addSubview:self.rightTipLabel];
    
    [self.topView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mineOrderClickEvent:)]];
    
    self.rightImageView = [YYCreateTools createImageView:@"mine_more_icon"
                                               viewModel:-1];
    [self.topView addSubview:self.rightImageView];
    
    self.bottomLine = [YYCreateTools createView:XHNEwLineColor];
    [self.topView addSubview:self.bottomLine];
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(kSizeScale(136));
        make.bottom.mas_equalTo(-12);
    }];
    
    [self.leftTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(16));
        make.left.mas_equalTo(kSizeScale(13));
    }];
    
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(13));
        make.centerY.equalTo(self.leftTipLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
    }];
    
    [self.rightTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftTipLabel);
        make.right.mas_equalTo(self.rightImageView.mas_left).mas_offset(-kSizeScale(6));
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(48);
        make.height.mas_equalTo(0.5);
    }];
    
    @weakify(self);
    self.btnArrs = [NSMutableArray array];
    self.bottomView = [YYCreateTools createView:XHWhiteColor];
    self.bottomView.v_cornerRadius = 5;
    [self addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(self.topView);
        make.top.mas_equalTo(self.bottomLine.mas_bottom);
    }];
    for (int i = 0; i < 4; i ++ ) {
        YYButton *btn = [[YYButton alloc] initWithFrame:CGRectZero];
        btn.space = kSizeScale(3);
        btn.tag = i+1;
        btn.titleFont = midFont(13);
        CGPoint offset = CGPointMake(IS_IPHONE_5?-kSizeScale(30) :-kSizeScale(35), IS_IPHONE_5 ?kSizeScale(20) :kSizeScale(22));
        [btn setBadgeOffset:offset];
        [btn setTitleTextColor:XHBlackColor];
        [btn setBadgeBgColor:XHRedColor];
        btn.btnClickEventBlock = ^(BOOL status, NSInteger btnTag) {
            @strongify(self);
            NSInteger type = btnTag; //1 . 2 . 3 . 4
            switch (type) {
                case 1:
                    [MobClick event:@"storeUnpaid"];
                    break;
                case 2:
                    [MobClick event:@"storeUntake"];
                    break;
                case 3:
                    [MobClick event:@"storeFinish"];
                    break;
                case 4:
                    [MobClick event:@"myAfterSale"];
                    break;
                default:
                    break;
            }
            if (self.orderClickBlock) {
                self.orderClickBlock(type);
            }
        };
        [self.bottomView addSubview:btn];
        [self.btnArrs addObject:btn];
    }
}
- (void)setMineOrder:(NSMutableArray *)imageNames
              titles:(NSMutableArray *)titles
            topSpace:(CGFloat)topSpace
           itemSpace:(CGFloat)itemSpace
          itemHeight:(CGFloat)itemHeight {
    
    if (kValidArray(imageNames) && kValidArray(titles)) {
        [self yy_multipleViewsAutoLayout:self.btnArrs
                               superView:self.bottomView
                                 padding:0
                                viewSize:CGSizeMake((kScreenW-kSizeScale(12)*2)/4.0, kSizeScale(76)) startSildeLeft:YES];
        
        [self.btnArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YYButton *btn = (YYButton *)obj;
            
            btn.normalTitle = titles[idx];
            btn.selectTitle = titles[idx];
            btn.IMGspace = topSpace;
            btn.space = itemSpace;
            NSString *normalImageName = imageNames[idx];
            if (kValidString(normalImageName)) {
                btn.normalImageName = btn.selectImageName = normalImageName;
            }
            btn.select = YES;
        }];
    }
}



- (void)mineOrderClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.orderClickBlock) {
        self.orderClickBlock(0);
    }
}


- (void)setRedCounts:(NSMutableArray *)redCounts {
    @weakify(self);
    _redCounts = redCounts;
    if (!kValidArray(_redCounts)) {
        return;
    }
    [self.btnArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self);
        YYButton *btn = (YYButton *)obj;
        if (idx < self.redCounts.count) {
            NSDictionary *redValue = [self.redCounts objectAtIndex:idx];
            if (btn.tag == [[redValue objectForKey:@"tabType"] integerValue] ) {
                NSLog(@"redvaleu      %@",redValue);
                [btn showBadgeWithValue:[[redValue objectForKey:@"cnt"] integerValue]];
            }
        }
    }];
}



@end

