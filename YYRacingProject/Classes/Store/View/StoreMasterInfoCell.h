//
//  StoreMasterInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface StoreMasterInfoCell : YYBaseTableViewCell

@property (nonatomic, assign) BOOL  isShowSubTitle;

@property (nonatomic, strong) NSString  *cellTitle;
@property (nonatomic, strong) NSString  *cellSubTitle;
@property (nonatomic, strong) NSString  *cellUserHead;

@property (nonatomic, copy) void (^cellClickBlock)(StoreMasterInfoCell *setCell);
@end
