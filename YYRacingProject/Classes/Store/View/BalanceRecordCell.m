//
//  BalanceRecordCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "BalanceRecordCell.h"

@implementation BalanceRecordCell

-(void)createUI{
    
    self.contentView.backgroundColor = XHLightColor;
    
    UIView *bg_view = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:bg_view];
    
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(8);
    }];
    
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(18) textColor:XHBlackColor];
    [bg_view addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(15);
    }];
    
    self.timeLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [bg_view addSubview:self.timeLabel];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_equalTo(10);
        make.bottom.mas_equalTo(-15);
    }];
    
    
    self.arrowImageView = [YYCreateTools createImageView:@"cellMore_icon"
                                                       viewModel:-1];
    [bg_view addSubview:self.arrowImageView];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(5),kSizeScale(9)));
        make.centerY.equalTo(self.contentView);
        make.right.mas_equalTo(kSizeScale(-16));
    }];
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [bg_view addSubview:self.detailLabel];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.arrowImageView.mas_left).mas_equalTo(-13);
        make.centerY.equalTo(self.contentView);
    }];
    
}

-(void)setModel:(BalanceRecordModel *)model{
    _model = model;
    self.arrowImageView.hidden = YES;
    
    [self.detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(self.contentView);
    }];
    
    self.titleLabel.text = model.operName;
    self.timeLabel.text = model.operTime;
    self.titleLabel.font = normalFont(15);
    self.detailLabel.font = boldFont(18);
    if (model.operType == 1) {
        self.detailLabel.text =[NSString stringWithFormat:@"-%@",model.amount];
        self.detailLabel.textColor = XHGreenColor;
    }else{
        self.detailLabel.text =[NSString stringWithFormat:@"+%@",model.amount];
        self.detailLabel.textColor = XHRedColor;
    }
}

-(void)setDrawModel:(WithDrawRecordModel *)drawModel{
    
    self.titleLabel.text = [NSString stringWithFormat:@"￥%@",drawModel.amountOfCash];
    self.timeLabel.text = drawModel.applyTime;
    
    switch (drawModel.presentStatus) {
        case 1:
            self.detailLabel.text = @"待审核";
            self.detailLabel.textColor = XHOrangeColor;
            break;
        case 2:
            self.detailLabel.text = @"审核通过";
            self.detailLabel.textColor = XHRedColor;
            break;
        case 3:
            self.detailLabel.text = @"提现失败";
            self.detailLabel.textColor = XHBlackLitColor;
            break;
        case 4:
            self.detailLabel.text = @"提现成功";
            self.detailLabel.textColor = XHGreenColor;
            break;
        default:
            break;
    }
    
    
}






@end
