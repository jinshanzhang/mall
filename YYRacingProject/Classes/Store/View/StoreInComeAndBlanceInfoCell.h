//
//  StoreInComeAndBlanceInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface StoreInComeAndBlanceInfoCell : YYBaseTableViewCell

@property (nonatomic, copy) void (^BlanceClickBlock)(NSInteger type);
- (void)setStoreInComeView:(NSMutableArray *)tipArrs
                     moneys:(NSMutableArray *)moneys;

@end
