//
//  StoreBottonSourceCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "StoreBottonSourceCell.h"

@implementation StoreBottonSourceCell

- (UICollectionView *)iconListView {
    if (!_iconListView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumInteritemSpacing = 0.0f;
        flowLayout.minimumLineSpacing = 0.f;
        flowLayout.sectionInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        _iconListView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                           collectionViewLayout:flowLayout];
        _iconListView.delegate = self;
        _iconListView.dataSource = self;
        _iconListView.showsHorizontalScrollIndicator = NO;
        _iconListView.backgroundColor = XHClearColor;
    }
    return _iconListView;
}

-(void)createUI{
    self.contentView.backgroundColor = XHStoreGrayColor;
    self.bottomView = [YYCreateTools createView:XHWhiteColor];
    self.bottomView.v_cornerRadius  = 5;
    [self addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(-kSizeScale(20));
    }];
    
    [self.bottomView addSubview:self.iconListView];
    Class currentCls = [HomeActivityIconInfoCell class];
    [self.iconListView registerClass:currentCls
          forCellWithReuseIdentifier:strFromCls(currentCls)];
}

-(void)setSourceArr:(NSMutableArray *)sourceArr{
    _sourceArr = sourceArr;
    [self.iconListView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.bottomView);
        make.height.mas_equalTo(sourceArr.count>4? kSizeScale(180) : kSizeScale(90));
    }];
    [self.iconListView reloadData];
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.sourceArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class current = [HomeActivityIconInfoCell class];
    HomeActivityIconInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
    if (row < self.sourceArr.count && kValidArray(self.sourceArr)) {
        HomeBannerInfoModel *bannerModel = [self.sourceArr objectAtIndex:row];
        cell.extendParams = [@{@"isStore":@(YES)} mutableCopy];
        [cell setActivityView:bannerModel.title iconUrl:bannerModel.imageUrl];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((kScreenW-24)/4.0,kSizeScale(90));
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (self.headerClickBlock) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (row < self.sourceArr.count) {
            //友盟打点
            switch (row) {
                case 0:
                    [MobClick event:@"storefans"];
                    break;
                case 1:
                    [MobClick event:@"storehost"];
                    break;
                case 2:
                    [MobClick event:@"storeInviteReward"];
                    break;
                case 3:
                    [MobClick event:@"storeSaleReward"];
                    break;
                case 4:
                    [MobClick event:@"storeMentor"];
                    break;
                default:
                    break;
            }
            HomeBannerInfoModel *bannerModel = [self.sourceArr objectAtIndex:row];
            [params setObject:@(bannerModel.linkType) forKey:@"linkType"];
            [params setObject:bannerModel.url forKey:@"url"];
            self.headerClickBlock(params);
        }
    }
}

@end
