//
//  StoreTaskView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreTaskModel.h"
#import "GGProgressView.h"
#import "yyCountDownManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreTaskView : UIView

@property (nonatomic, strong) UIImageView *taskIMG;
@property (nonatomic, strong) StoreTaskModel *taskModel;

@property (nonatomic, strong) UILabel *totalLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) GGProgressView *progressView;

@end

NS_ASSUME_NONNULL_END
