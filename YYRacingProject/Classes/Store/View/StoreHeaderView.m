//
//  StoreHeaderView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/2.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "StoreHeaderView.h"
#import "YYOrderShowView.h"

@interface StoreHeaderView()

@property (nonatomic, strong) UIImageView *userHeaderView;
@property (nonatomic, strong) UIImageView *identityImageView;  //身份标识
@property (nonatomic, strong) UIImageView *rewardImageView;  //奖励底图
@property (nonatomic, strong) UILabel *rewardtitle;  //奖励底图
@property (nonatomic, strong) UIImageView *setImageView;  //奖励底图
@property (nonatomic, strong) UIImageView *setImageViewMore;  //奖励底图

@property (nonatomic, strong) UILabel     *userNameLabel;
@property (nonatomic, strong) UILabel     *inviteCodeLabel;
@property (nonatomic, strong) UIButton    *codeCopyBtn;
@property (nonatomic, strong) UIButton    *shareBtn;

@property (nonatomic, strong) UIView      *bottomView;
@property (nonatomic, strong) UIImageView      *inComeBottomView;

@property (nonatomic, strong) NSMutableArray   *inComeSubViews;
@property (nonatomic, strong) NSMutableArray   *inComeLineViews;

@property (nonatomic, strong) NSMutableArray *inComeAndBlanceSubViews;
@property (nonatomic, strong) NSMutableArray *inComeAndBlanceSubLineViews;
@property (nonatomic, strong) UIView *inComeAndBlanceView;


@end

@implementation StoreHeaderView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.inComeSubViews = [NSMutableArray array];
        self.inComeLineViews = [NSMutableArray array];
        [self creatUI];
    }   return self;
}

-(void)creatUI{
    
    self.bottomView = [YYCreateTools createView:XHStoreGrayColor];
    [self addSubview:self.bottomView];
    CGRect frame;
    if (IS_IPHONE_X) {
        frame =  CGRectMake(0, 0, kScreenW, kScreenW/1.6);
    }else{
        frame =  CGRectMake(0, 0, kScreenW, kScreenW/1.9);
    }
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(frame.size.height);
        make.bottom.left.right.mas_equalTo(0);
    }];

    self.userHeaderView = [YYCreateTools createImageView:nil viewModel:-1];
    self.userHeaderView.v_cornerRadius = kSizeScale(25);
    [self.userHeaderView zy_attachBorderWidth:1 color:XHWhiteColor];
    self.userHeaderView.backgroundColor = XHWhiteColor;
    [self addSubview:self.userHeaderView];
    
    [self.userHeaderView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setClick:)]];

    
    self.identityImageView = [YYCreateTools createImageView:@"Identity_icon"
                                                  viewModel:-1];
    [self addSubview:self.identityImageView];
    
    self.userNameLabel = [YYCreateTools createLabel:nil
                                               font:boldFont(18)
                                          textColor:XHWhiteColor];
    self.userNameLabel.userInteractionEnabled = YES;
    [self addSubview:self.userNameLabel];
    
    [self.userNameLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setClick:)]];

    self.inviteCodeLabel = [YYCreateTools createLabel:nil
                                                 font:midFont(12)
                                            textColor:XHWhiteColor];
    self.inviteCodeLabel.userInteractionEnabled = YES;
    [self addSubview:self.inviteCodeLabel];
    
    [self.inviteCodeLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setClick:)]];

    
    self.codeCopyBtn = [YYCreateTools createBtn:@"点击复制"
                                           font:midFont(10)
                                      textColor:XHWhiteColor];
    self.codeCopyBtn.layer.borderColor = XHWhiteColor.CGColor;
    self.codeCopyBtn.layer.borderWidth =0.5 ;
    self.codeCopyBtn.layer.cornerRadius = 3;
    [self.codeCopyBtn addTarget:self action:@selector(codeCopyClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.codeCopyBtn];
    
    self.shareBtn = [YYCreateTools createBtn:@"分享店铺"
                                           font:midFont(10)
                                      textColor:XHWhiteColor];
    self.shareBtn.layer.borderColor = XHWhiteColor.CGColor;
    self.shareBtn.layer.borderWidth = 0.5;
    self.shareBtn.layer.cornerRadius = 3;
    [self.shareBtn addTarget:self action:@selector(shareClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.shareBtn];
    
    //收益总计
    self.inComeAndBlanceSubViews = [NSMutableArray array];
    self.inComeAndBlanceSubLineViews = [NSMutableArray array];
    
    self.inComeAndBlanceView = [YYCreateTools createView:XHClearColor];
    [self addSubview:self.inComeAndBlanceView];
    
    [self.inComeAndBlanceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.userHeaderView.mas_bottom).mas_offset(kSizeScale(8));
        make.height.mas_equalTo(kSizeScale(60)).priorityHigh();
    }];
    
    for (int i = 0; i < 3; i ++) {
        YYOrderShowView *showView = [[YYOrderShowView alloc] initWithFrame:CGRectZero];
        showView.topSpace = (IS_IPHONE_5 ? kSizeScale(13) : kSizeScale(10));
        showView.itemSpace = kSizeScale(0);
        showView.tag = i;
        [showView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blanceClickOperatorOrder:)]];
        [self.inComeAndBlanceView addSubview:showView];
        [self.inComeAndBlanceSubViews addObject:showView];
        if (i > 0) {
            UIImageView *lineView = [YYCreateTools createImageView:@"header_line_view" viewModel:-1];
            [self.inComeAndBlanceView addSubview:lineView];
            [self.inComeAndBlanceSubLineViews addObject:lineView];
        }
    }
    
    CGFloat inComeshowViewWidth = (kScreenW-kSizeScale(24) - 3)/3.0;
    [self yy_multipleViewsAutoLayout:self.inComeAndBlanceSubViews
                           superView:self.inComeAndBlanceView
                             padding:1
                            viewSize:CGSizeMake(inComeshowViewWidth, kSizeScale(60))];
    [self yy_multipleViewsAutoLayout:self.inComeAndBlanceSubLineViews
                           superView:self.inComeAndBlanceView
                             padding:inComeshowViewWidth + 1
                            viewSize:CGSizeMake(1, kSizeScale(30))];
    //明细收益
    self.inComeBottomView = [YYCreateTools createImageView:@"headerIncome_view" viewModel:-1];
    [self addSubview:self.inComeBottomView];
    
    for (int i = 0; i < 3; i ++) {
        YYOrderShowView *showView = [[YYOrderShowView alloc] initWithFrame:CGRectZero];
        showView.topSpace = (IS_IPHONE_5 ? kSizeScale(22) : kSizeScale(20));
        showView.itemSpace = kSizeScale(1);
        [showView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blanceClickOperator:)]];
        [self.inComeBottomView addSubview:showView];
        [self.inComeSubViews addObject:showView];
        if (i > 0) {
            UIImageView *lineView = [YYCreateTools createImageView:@"header_line_view2" viewModel:-1];
            [self.inComeBottomView addSubview:lineView];
            [self.inComeLineViews addObject:lineView];
        }
    }
    [self.userHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(14));
        make.top.mas_equalTo(kStatuH +kSizeScale(15));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(50)));
    }];
    
    [self.identityImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.userHeaderView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(60)));
    }];

    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userHeaderView.mas_right).offset(kSizeScale(10));
        make.top.equalTo(self.userHeaderView.mas_top).offset(kSizeScale(2));
    }];
    
    [self.inviteCodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userNameLabel);
        make.top.equalTo(self.userNameLabel.mas_bottom).offset(kSizeScale(4));
    }];
    
    [self.codeCopyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.inviteCodeLabel.mas_right).offset(kSizeScale(12));
        make.centerY.equalTo(self.inviteCodeLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(52), kSizeScale(18)));
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.codeCopyBtn.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.inviteCodeLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(52), kSizeScale(18)));
    }];


    [self.inComeBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(13));
        make.bottom.mas_equalTo(0);
        make.right.mas_equalTo(-kSizeScale(13));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(350), kSizeScale(70)));
    }];
    
    [self.inComeBottomView layoutIfNeeded];

    CGFloat showViewWidth = (self.inComeBottomView.width - 3)/3.0;
    [self yy_multipleViewsAutoLayout:self.inComeSubViews
                           superView:self.inComeBottomView
                             padding:1
                            viewSize:CGSizeMake(showViewWidth, kSizeScale(84))];
    [self yy_multipleViewsAutoLayout:self.inComeLineViews
                           superView:self.inComeBottomView
                             padding:showViewWidth + 1
                            viewSize:CGSizeMake(1, kSizeScale(30))];
    
    self.setImageView = [YYCreateTools createImageView:@"storeset_icon" viewModel:-1];
    [self addSubview:self.setImageView];
    [self.setImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setClick:)]];
    
    [self.setImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.centerY.equalTo(self.userNameLabel);
    }];
 
}

- (void)setStoreName:(NSString *)storeName {
    _storeName = storeName;
    self.userNameLabel.text = _storeName;
}

- (void)setStoreInviteCode:(NSString *)storeInviteCode {
    _storeInviteCode = storeInviteCode;
    if (kValidString(_storeInviteCode)) {
        self.inviteCodeLabel.text = [NSString stringWithFormat:@"邀请码：%@",_storeInviteCode];
    }
}



- (void)setStoreHeadImage:(NSString *)storeHeadImage {
    _storeHeadImage = storeHeadImage;
    if (kValidString(_storeHeadImage)) {
        [self.userHeaderView sd_setImageWithURL:[NSURL URLWithString:_storeHeadImage] placeholderImage:[UIImage imageNamed:@"user_default_icon"]];
    }
    else {
        self.userHeaderView.image = [UIImage imageNamed:@"user_default_icon"];
    }
    self.identityImageView.hidden = (kUserInfo.level<2)?YES:NO;
    if (kUserInfo.level >= 2) {
        NSString *identityPhotoName = nil;
        if (kUserInfo.level == 2) {
            identityPhotoName = @"owner_header_icon";
        }
        if (kUserInfo.level == 3) {
            identityPhotoName = @"agent_header_icon";
        }
         if (kUserInfo.level == 4) {
            identityPhotoName = @"partner_header_icon";
        }
        if (kValidString(identityPhotoName)) {
            self.identityImageView.image = [UIImage imageNamed:identityPhotoName];
        }
    }
}

- (void)setStoreInComeView:(NSMutableArray *)tipArrs
                    moneys:(NSMutableArray *)moneys {
    if (kValidArray(tipArrs) && kValidArray(moneys)) {
        if ((tipArrs.count == moneys.count) && (tipArrs.count == self.inComeAndBlanceSubViews.count)) {
            [self.inComeAndBlanceSubViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYOrderShowView *showView = (YYOrderShowView *)obj;
                NSMutableAttributedString *topAttri = [[NSMutableAttributedString alloc] initWithString:tipArrs[idx]];
                [topAttri yy_setAttributes:HexRGB(0xFFE0B0)
                                      font:boldFont(18)
                                   content:topAttri
                                 alignment:NSTextAlignmentCenter];

                NSMutableAttributedString *bottomAttri = [[NSMutableAttributedString alloc] initWithString:moneys[idx]];
                [bottomAttri yy_setAttributes:XHWhiteColor
                                         font:normalFont(10)
                                      content:bottomAttri
                                    alignment:NSTextAlignmentCenter];
                showView.topAttributed = bottomAttri;
                showView.bottomAttributed =  topAttri;
            }];
        }
    }
}

- (void)setHeaderInComeView:(NSMutableArray *)tipArrs
                     moneys:(NSMutableArray *)moneys {
    if (kValidArray(tipArrs) && kValidArray(moneys)) {
        if ((tipArrs.count == moneys.count) && (tipArrs.count == self.inComeSubViews.count)) {
            [self.inComeSubViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYOrderShowView *showView = (YYOrderShowView *)obj;
                NSMutableAttributedString *topAttri = [[NSMutableAttributedString alloc] initWithString:tipArrs[idx]];
                [topAttri yy_setAttributes:HexRGB(0x823700)
                                      font:normalFont(12)
                                   content:topAttri
                                 alignment:NSTextAlignmentCenter];
                
                NSMutableAttributedString *bottomAttri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",moneys[idx]]];
                [bottomAttri yy_setAttributes:HexRGB(0x823700)
                                         font:boldFont(15)
                                      content:bottomAttri
                                    alignment:NSTextAlignmentCenter];
                
                showView.topAttributed = bottomAttri;
                showView.bottomAttributed =  topAttri;
            }];
        }
    }
}

-(void)setRewardDic:(NSDictionary *)rewardDic{
    if (rewardDic) {
        _rewardDic = rewardDic;
        
        [self.inComeBottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            //        make.top.mas_equalTo(self.userHeaderView.mas_bottom).mas_equalTo(kSizeScale(68));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(350), kSizeScale(70)));
            make.left.mas_equalTo(kSizeScale(13));
            make.bottom.mas_equalTo(-36);
            make.right.mas_equalTo(-kSizeScale(13));
        }];
        
        self.rewardImageView = [YYCreateTools createImageView:@"reward_view" viewModel:-1];
        [self.bottomView addSubview:self.rewardImageView];
        
        [self.rewardImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rewardClick:)]];
        
        self.rewardtitle = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
        [self.rewardImageView addSubview:self.rewardtitle];
        
        [self.rewardImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.inComeBottomView.mas_bottom).mas_equalTo(-6);
            make.left.mas_equalTo(kSizeScale(13));
            make.right.mas_equalTo(-kSizeScale(13));
        }];

        [self.rewardtitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(12);
            make.centerY.equalTo(self.rewardImageView).mas_offset(kSizeScale(3));
        }];
        
        self.setImageViewMore = [YYCreateTools createImageView:@"reward_more" viewModel:-1];
        [self.rewardImageView addSubview:self.setImageViewMore];

        [self.setImageViewMore mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-12);
            make.centerY.equalTo(self.rewardtitle);
        }];
        
        NSArray *detailArr = [rewardDic objectForKey:@"tips"];
        NSString *detail1 =  [NSString stringWithFormat:@"%@: ",[rewardDic objectForKey:@"title"]];
        NSString *detail2 =  detailArr[0];
        NSString *detail3 =  detailArr[1];
        NSString *detail4 =  detailArr.count>2?detailArr[2]:@"";
        NSString *detail5 =  detailArr.count>2?detailArr[3]:@"";
        
        NSMutableAttributedString *aString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@%@%@",detail2,detail3,detail4?:@"",detail5?:@""]];
        [aString addAttribute:NSForegroundColorAttributeName value:XHBlackColor range:NSMakeRange(0,detail2.length)];
        [aString addAttribute:NSForegroundColorAttributeName value:XHRedColor range:NSMakeRange(detail2.length,detail3.length)];
        [aString addAttribute:NSForegroundColorAttributeName value:XHBlackColor range:NSMakeRange(detail2.length+detail3.length,detail4.length)];
        [aString addAttribute:NSForegroundColorAttributeName value:XHRedColor range:NSMakeRange(detail2.length+detail3.length+detail4.length,detail5.length)];
        self.rewardtitle.attributedText = aString;
    }
}

- (void)blanceClickOperatorOrder:(UITapGestureRecognizer *)tapGesture {
    if (self.BlanceClickBlock) {
        self.BlanceClickBlock(tapGesture.view.tag);
    }
}

- (void)blanceClickOperator:(UITapGestureRecognizer *)tapGesture {
    if (self.BlanceClickBlock) {
        self.BlanceClickBlock(3);
    }
}

- (void)rewardClick:(UITapGestureRecognizer *)tapGesture {
    if (self.rewardClickBlock) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(3) forKey:@"linkType"];
        [params setObject:[self.rewardDic objectForKey:@"url"]  forKey:@"url"];
        self.rewardClickBlock(params);
    }
}

- (void)setClick:(UITapGestureRecognizer *)tapGesture {
    if (self.setClickBlock) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        self.setClickBlock(params);
    }
}

- (void)codeCopyClickEvent:(UIButton *)sender {
    [YYCommonTools pasteboardCopy:kUserInfo.promotionCode
                     appendParams:nil];
}

- (void)shareClickEvent:(UIButton *)sender {
    if (self.shareClickBlock) {
        self.shareClickBlock([NSMutableDictionary dictionary]);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

