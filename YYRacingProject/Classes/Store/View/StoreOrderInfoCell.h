//
//  StoreOrderInfoCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/6.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreOrderInfoCell : YYBaseTableViewCell

@property (nonatomic, copy) void (^orderClickBlock)(NSInteger type);
@property (nonatomic, assign) NSInteger superType;
@property (nonatomic, strong) NSMutableArray  *redCounts;
- (void)setMineOrder:(NSMutableArray *)imageNames
              titles:(NSMutableArray *)titles
            topSpace:(CGFloat)topSpace
           itemSpace:(CGFloat)itemSpace
          itemHeight:(CGFloat)itemHeight;

@end

NS_ASSUME_NONNULL_END
