//
//  StoreHeaderView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/2.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreHeaderView : UIView


@property (nonatomic, strong) NSString *storeHeadImage;
@property (nonatomic, strong) NSString *storeName;
@property (nonatomic, strong) NSString *storeInviteCode;  //店铺邀请码
@property (nonatomic, strong) NSDictionary *rewardDic;

@property (nonatomic, copy) void (^headerClickBlock)(NSInteger type);
@property (nonatomic, copy) void (^BlanceClickBlock)(NSInteger type);
@property (nonatomic, copy) void (^rewardClickBlock)(NSMutableDictionary *param);
@property (nonatomic, copy) void (^setClickBlock)(NSMutableDictionary *param);
@property (nonatomic, copy) void (^shareClickBlock)(NSMutableDictionary *param);

- (void)setUpdateUserInfo;

- (void)setHeaderInComeView:(NSMutableArray *)tipArrs
                     moneys:(NSMutableArray *)moneys;

- (void)setStoreInComeView:(NSMutableArray *)tipArrs
                    moneys:(NSMutableArray *)moneys;

@end

NS_ASSUME_NONNULL_END
