//
//  StoreInviteAndShareInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//
#define  InviteBtnTag  330
#import "StoreInviteAndShareInfoCell.h"

@interface StoreInviteAndShareInfoCell()

@property (nonatomic, strong) UIView *shareView;

@property (nonatomic, strong) NSMutableArray *shareSubViews;
@property (nonatomic, strong) NSMutableArray *shareSubTwoViews;

@end

@implementation StoreInviteAndShareInfoCell

- (void) createUI {
    
    self.shareSubViews = [NSMutableArray array];
    for (int i = 0; i < 2; i++) {
        YYAnimatedImageView *showIMG = [YYAnimatedImageView new];
        [self addSubview:showIMG];
        [self.shareSubViews addObject:showIMG];
    }
}

-(void)setSourceArr:(NSMutableArray *)sourceArr{
    _sourceArr = sourceArr;
    for (int i = 0; i < sourceArr.count; i++) {
        YYAnimatedImageView *showIMG = self.shareSubViews[i];
        showIMG.tag = i;
        showIMG.userInteractionEnabled = YES;
        showIMG.contentMode = UIViewContentModeScaleAspectFit;
        HomeBannerInfoModel *model = sourceArr[i];
        if (sourceArr.count ==1 ) {
            showIMG.frame = CGRectMake(0, 0, kScreenW, kSizeScale(75));
        }else{
            showIMG.frame = CGRectMake(kScreenW/2*i, 0, kScreenW/2, kSizeScale(75));
        }
        
        [showIMG setImageURL:[NSURL URLWithString:model.imageUrl]];
        [showIMG addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(capulesViewClickEvent:)]];
    }
}


- (void)capulesViewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.headerClickBlock) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        UIView *view = [tapGesture view];
        HomeBannerInfoModel *model = self.sourceArr[view.tag];
        [params setObject:@(model.linkType) forKey:@"linkType"];
        [params setObject:model.url forKey:@"url"];
        self.headerClickBlock(params);
    }
}


@end
