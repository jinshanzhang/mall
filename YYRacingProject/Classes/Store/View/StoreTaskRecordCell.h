//
//  StoreTaskRecordCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreTaskRecordCell : YYBaseTableViewCell

@property (nonatomic, strong) NSMutableArray *taskArr;

@property (nonatomic, copy) NSString  *taskurl;


@property (nonatomic, copy) void (^headerClickBlock)(NSMutableDictionary *params);


@end

NS_ASSUME_NONNULL_END
