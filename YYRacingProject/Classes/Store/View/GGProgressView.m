//
//  GGProgressView.m
//
//  Created by GG on 2016/10/20.
//  Copyright © 2016年 GG. All rights reserved.
//

#import "GGProgressView.h"
@interface GGProgressView()
{
    UIView *_progressView;
    float _progress;
}

@end

@implementation GGProgressView

-(instancetype)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame progressViewStyle:GGProgressViewStyleDefault];
}

- (instancetype)initWithFrame:(CGRect)frame progressViewStyle:(GGProgressViewStyle)style
{
    if (self=[super initWithFrame:frame]) {
        if (!_progressView) {
      
        _progressView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 0, frame.size.height)];
//        _progress=0;
        self.progressViewStyle=style;
        [self addSubview:_progressView];
        
         self.titleIMG = [YYCreateTools createImageView:@"progress_title_view" viewModel:-1];
         self.titleIMG.frame = CGRectMake(-14, -22, 48, 18);
        [self addSubview:self.titleIMG];
        
         self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(9) textColor:HexRGB(0xFFD99C)];
//         self.titleLabel.backgroundColor = XHBlackColor;
         self.titleLabel.textAlignment = NSTextAlignmentCenter;
         self.titleLabel.v_cornerRadius = 6;
         [self.titleIMG addSubview:self.titleLabel];
          
         [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.titleIMG);
            make.top.mas_equalTo(0);
            make.height.mas_equalTo(16);
        }];
            
//         [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.centerX.equalTo(self->_progressView.mas_right);
//            make.bottom.mas_equalTo(self->_progressView.mas_top).mas_offset(-10);
//            make.height.mas_equalTo(18);
//         }];
        }
    }
    return self;
}
#pragma mark 样式
-(void)setProgressViewStyle:(GGProgressViewStyle)progressViewStyle
{
    _progressViewStyle=progressViewStyle;
    if (progressViewStyle==GGProgressViewStyleTrackFillet) {
        self.layer.masksToBounds=NO;
        self.layer.cornerRadius=self.bounds.size.height/2;
    }
    else if (progressViewStyle==GGProgressViewStyleAllFillet)
    {
        self.layer.masksToBounds=NO;
        self.layer.cornerRadius=self.bounds.size.height/2;
        _progressView.layer.cornerRadius=self.bounds.size.height/2;
    }
}

//-(void)setActualText:(NSString *)actualText{
//    _titleLabel.text = actualText;
//}

-(void)setTrackTintColor:(UIColor *)trackTintColor
{
    _trackTintColor=trackTintColor;
    if (self.trackImage) {
    }
    else
    {
        self.backgroundColor=trackTintColor;
    }
}

-(void)setProgress:(float)progress
{
    _progress=MIN(progress, 1);
        @weakify(self);
    if (_progress != 0) {
        
        if (_progress>=1) {
            self.titleLabel.text = @"已完成";
        }
        [UIView animateWithDuration:2 animations:^{
            @strongify(self);
            self->_progressView.frame=CGRectMake(0, 0, self.bounds.size.width*self->_progress, self.bounds.size.height);
//            self.titleLabel.v_centerX =self.bounds.size.width*self->_progress;
            self.titleIMG.frame =CGRectMake((self.bounds.size.width*self->_progress-22),-20,48,18);
        }];
    }else{
        _progressView.frame = CGRectMake(0,0, 10, self.bounds.size.height);
    }
}

-(float)progress
{
    return _progress;
}

-(void)setProgressTintColor:(UIColor *)progressTintColor
{
    _progressTintColor=progressTintColor;
    _progressView.backgroundColor=progressTintColor;
}

-(void)setTrackImage:(UIImage *)trackImage
{
    _trackImage=trackImage;
    if(self.isTile)
    {
        self.backgroundColor=[UIColor colorWithPatternImage:trackImage];
    }else
    {
        self.backgroundColor=[UIColor colorWithPatternImage:[self stretchableWithImage:trackImage]];
    }
}



-(void)setIsTile:(BOOL)isTile
{
    _isTile = isTile;
    if (self.progressImage) {
        [self setProgressImage:self.progressImage];
    }
    if (self.trackImage) {
        [self setTrackImage:self.trackImage];
    }
}
-(void)setProgressImage:(UIImage *)progressImage
{
    _progressImage = progressImage;
    if(self.isTile)
    {
        _progressView.backgroundColor=[UIColor colorWithPatternImage:progressImage];
    }
    else
    {
        _progressView.backgroundColor=[UIColor colorWithPatternImage:[self stretchableWithImage:progressImage]];
    }
}
- (UIImage *)stretchableWithImage:(UIImage *)image{
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0.f);
    [image drawInRect:self.bounds];
    UIImage *lastImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return lastImage;
}
@end
