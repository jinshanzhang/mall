//
//  StoreBannerCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "StoreBannerCell.h"

@interface StoreBannerCell()<SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *topBannerView;


@end

@implementation StoreBannerCell

-(void)createUI{
    self.contentView.backgroundColor = XHStoreGrayColor;
    self.topBannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectZero];
    _topBannerView.v_cornerRadius = 5;
    _topBannerView.autoScroll = YES;
    _topBannerView.delegate = self;
    _topBannerView.backgroundColor = XHLightRedColor;
    _topBannerView.pageControlDotSize = CGSizeMake(7, 7);
    _topBannerView.pageDotImage = [UIImage imageNamed:@"home_banner_default_icon"];
    _topBannerView.placeholderImage = [UIImage imageWithColor:XHLightColor];
    _topBannerView.currentPageDotImage = [UIImage imageNamed:@"home_banner_selected_icon"];
    [self addSubview:self.topBannerView];
}

-(void)setImgArr:(NSArray *)imgArr{
    _imgArr = imgArr;
    
    [self.topBannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(kSizeScale(80));
        make.bottom.mas_equalTo(-12);
    }];
//    self.topBannerView.frame = CGRectMake(0, 0, kScreenW, kSizeScale(80));
    NSMutableArray *arr = [NSMutableArray array];
    for (HomeResourceItemInfoModel *model  in imgArr) {
        [arr addObject:model.imageUrl];
    }
    self.topBannerView.imageURLStringsGroup = arr;
}

#pragma mark - SDCycleDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerDidSelected:)]) {
        if (index < self.imgArr.count && kValidArray(self.imgArr)) {
            [self.delegate headerDidSelected:self.imgArr[index]];
        }
    }
}

@end
