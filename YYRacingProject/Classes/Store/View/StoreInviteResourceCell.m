//
//  StoreInviteResourceCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "StoreInviteResourceCell.h"

@interface StoreInviteResourceCell()

@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) NSMutableArray  *gridViews;

@end

@implementation StoreInviteResourceCell

-(void)createUI{
    self.contentView.backgroundColor = XHStoreGrayColor;
    self.bottomView = [YYCreateTools createView:XHClearColor];
    [self addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(kSizeScale(12));
        make.bottom.mas_equalTo(-kSizeScale(-12));
    }];
}

-(void)setResourceArr:(NSMutableArray *)resourceArr{
    _resourceArr = resourceArr;
    self.gridViews = [NSMutableArray array];
    [self.bottomView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (int i =0 ; i <resourceArr.count; i++) {
        UIImageView *gridImageView = [YYCreateTools createImageView:@"" viewModel:-1];
        gridImageView.tag =  i;
        gridImageView.userInteractionEnabled = YES;
        [gridImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityClickEvent:)]];
        [self.bottomView addSubview:gridImageView];
        [self.gridViews addObject:gridImageView];
    }
    
    [self layoutsize];

    int line1 = 0;
    int line2 = 0;
    for (int i = 0; i < resourceArr.count; i++) {
        UIImageView *itemView = self.gridViews[i];
        HomeResourceItemInfoModel *itemModel = resourceArr[i];
        [itemView yy_sdWebImage:itemModel.imageUrl placeholderImageType:YYPlaceholderImageListGoodType];
        if (i == 0) {
            line1 = [itemModel.height intValue]/2;
        }else if (i == 4){
            line2 = [itemModel.height intValue]/2;
        }
    }
    
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(12);
        make.height.mas_equalTo(kSizeScale(line1+line2));
        make.bottom.mas_equalTo(-12);
    }];
}

-(void)layoutsize{
    //创建一个空view 代表上一个view
    __block UIView *lastView = nil;
    // 间距为10
    int intes = 0;
    // 每行4个
    int num = 4;
    // 循环创建view
    for (int i = 0; i < _resourceArr.count; i++) {
        UIImageView *itemView = _gridViews[i];
        HomeResourceItemInfoModel *itemModel = _resourceArr[i];
        int height = [itemModel.height intValue]/2;
        int width = [itemModel.width intValue]/2;
        // 添加约束
        [itemView mas_makeConstraints:^(MASConstraintMaker *make) {
            // 给个高度约束
            make.height.mas_equalTo(kSizeScale(height));
            make.width.mas_equalTo(kSizeScale(width));
            // 2. 判断是否是第一列
            if (i % num == 0) {
                // 一：是第一列时 添加左侧与父视图左侧约束
                make.left.mas_equalTo(itemView.superview).offset(intes);
            } else {
                // 二： 不是第一列时 添加左侧与上个view左侧约束
                make.left.mas_equalTo(lastView.mas_right).offset(intes);
            }
            // 3. 判断是否是最后一列 给最后一列添加与父视图右边约束
            if (i % num == (num - 1)) {
                make.right.mas_equalTo(itemView.superview).offset(-intes);
            }// 4. 判断是否为第一列
            if (i / num == 0) {
            // 第一列添加顶部约束
                make.top.mas_equalTo(itemView.superview).offset(intes*10);
            } else {
                // 其余添加顶部约束 intes*10 是我留出的距顶部高度
                make.top.mas_equalTo(intes * kSizeScale(10) + ( i / num )* (kSizeScale(80) + intes));
            }
        }];
        // 每次循环结束 此次的View为下次约束的基准
        lastView = itemView;
    }
}

#pragma mark - Event
- (void)activityClickEvent:(UITapGestureRecognizer *)tapGesture {
    NSInteger tag = ((UIImageView *)tapGesture.view).tag;
    if (self.headerClickBlock) {
        HomeResourceItemInfoModel *model  = self.resourceArr[tag];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:model.linkType forKey:@"linkType"];
        [params setObject:model.url forKey:@"url"];
        self.headerClickBlock(params);
    }
}

@end
