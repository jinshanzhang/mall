//
//  StoreTaskView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "StoreTaskView.h"


@interface StoreTaskView ()

@property (nonatomic, strong) NSMutableArray *evaluationItems;
@property (nonatomic, strong) NSMutableArray *countDownItems;
@property (nonatomic, strong) UIImageView *moreTaskIMG;

@end

@implementation StoreTaskView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.evaluationItems = [NSMutableArray array];
        self.countDownItems = [NSMutableArray array];
        [self creatUI];
    }   return self;
}

- (void)creatUI{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:YYCountDownNotification object:nil];
    self.taskIMG = [YYCreateTools createImageView:@"StoreTask_view" viewModel:-1];
    [self addSubview:self.taskIMG];
    
    [self.taskIMG addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ClickEvent:)]];

    
    [self.taskIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.bottom.right.mas_equalTo(-12);
        make.height.mas_equalTo(kSizeScale(105));
    }];

    self.progressView= [[GGProgressView alloc]initWithFrame:CGRectMake(kSizeScale(24), kSizeScale(30), kSizeScale(250), kSizeScale(8))];
    //设置
    _progressView.clipsToBounds = NO;
    _progressView.progressTintColor=HexRGB(0xeed8a3     );
    _progressView.trackTintColor=XHWhiteColor;
    _progressView.progressViewStyle=GGProgressViewStyleAllFillet;
    //添加
    [self.taskIMG addSubview:_progressView];
    
    self.totalLabel =[YYCreateTools createLabel:@"" font:boldFont(13) textColor:HexRGB(0xe6000f)];
    [self.taskIMG addSubview:self.totalLabel];
    
    [self.totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.progressView.mas_right).mas_offset(kSizeScale(8));
        make.centerY.equalTo(self.progressView);
        make.height.mas_equalTo(15);
    }];
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHWhiteColor];
    [self.taskIMG addSubview:self.detailLabel];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.progressView.mas_bottom).mas_offset(kSizeScale(8));
        make.centerX.equalTo(self.taskIMG);
    }];
    
    
    for (int i = 0; i < 8; i ++) {
        BOOL isEven = ((i % 2 == 0)?YES:NO);
        UILabel *lbl = [YYCreateTools createLabel:nil
                                             font:midFont(10)
                                        textColor:XHBlackColor];
        lbl.layer.cornerRadius = 2;
        lbl.clipsToBounds = YES;
    
        if (!isEven) {
            if (i == 1) {
                lbl.text = @"天";
            }else if (i == 3){
                lbl.text = @"时";
            }else if (i == 5){
                lbl.text = @"分";
            }else if (i == 7){
                lbl.text = @"秒后结束";
            }
        }
        else {
            [self.evaluationItems addObject:lbl];
        }
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.textColor = (!isEven?XHBlackColor:XHLoginColor);
        lbl.backgroundColor = (isEven?XHTimeBlackColor:XHClearColor);
        [self.taskIMG addSubview:lbl];
        [self.countDownItems addObject:lbl];
    }
    [self countDownItemViewLayout:NO];
}

- (void)countDownItemViewLayout:(BOOL)isCenter {
    UILabel *tempLbl = nil;
    for (NSInteger i = self.countDownItems.count - 1; i >= 0 ; i --) {
        UILabel *subLbl = self.countDownItems[i];
        BOOL isEven = ((i % 2 == 0)?YES:NO);
        if (tempLbl) {
            [subLbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(tempLbl.mas_left).mas_offset(2);
                make.size.mas_equalTo(isEven? CGSizeMake(kSizeScale(18),kSizeScale(18)): CGSizeMake(kSizeScale(20), kSizeScale(20)));
                make.centerY.equalTo(tempLbl);
            }];
        }
        else {
            [subLbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(subLbl.superview.mas_right).offset(-kSizeScale(86));
                make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(20)));
                if (isCenter) {
                    make.centerX.equalTo(subLbl.superview.mas_centerX);
                }
                else {
                    make.bottom.equalTo(subLbl.superview.mas_bottom).offset(-kSizeScale(13));
                }
            }];
        }
        tempLbl = subLbl;
    }
}

/*- (void)willMoveToWindow:(UIWindow*)newWindow {
    if (newWindow == nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:YYCountDownNotification object:nil];
    }
}

//添加通知方法'
- (void)didMoveToWindow {
    if (self.window) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:YYCountDownNotification object:nil];
    }
}*/

- (void)countDownNotification {
    double now = (long)self.taskModel.currentTimestamp;
    double end = (long)self.taskModel.endTimestamp;
    
    CGFloat timeInterval = [kConfigCountDown timeIntervalWithIdentifier:@"storeCtrl"];
    double leftTime = end - now;
    double differTime = leftTime - timeInterval;
//    NSLog(@"[differTime = %f, %f]", differTime, timeInterval);
    if (differTime <=0) {
        differTime = 0;
    }
        double secondeDiffertime = differTime/1000;
        
        NSInteger currentDay = (NSInteger)((secondeDiffertime)/(3600*24));
        
        NSInteger currentHour = (NSInteger)((secondeDiffertime-(currentDay*24*3600))/3600);
        
        NSInteger currentMinute = (NSInteger)(secondeDiffertime-(currentDay*24*3600)-(currentHour*3600))/60;
        
        NSInteger currentSeconds = (NSInteger)(secondeDiffertime-(currentDay*24*3600)-currentHour*3600-currentMinute*60);
        //天数
        NSString *days = [NSString stringWithFormat:@"%@", (long)currentDay < 10?[NSString stringWithFormat:@"0%ld",(long)currentDay]:@(currentDay)];
        //小时数
        NSString *hours = [NSString stringWithFormat:@"%@", (currentHour < 10 ?[NSString stringWithFormat:@"0%ld",(long)currentHour]:@(currentHour))];
        //分钟数
        NSString *minute = [NSString stringWithFormat:@"%@", (currentMinute < 10 ?[NSString stringWithFormat:@"0%ld",(long)currentMinute]:@(currentMinute))];
        //秒数
        NSString *second = [NSString stringWithFormat:@"%@", (currentSeconds < 10 ?[NSString stringWithFormat:@"0%ld",(long)currentSeconds]:@(currentSeconds))];
        for (int i = 0; i < self.evaluationItems.count; i ++) {
            UILabel *lbl = [self.evaluationItems objectAtIndex:i];
            if (i == 0) {
                lbl.text = days;
            }
            else if (i == 1) {
                lbl.text = hours;
            }
            else if (i == 2) {
                lbl.text = minute;
            }
            else if (i == 3) {
                lbl.text = second;
            }
        }
}


-(void)setTaskModel:(StoreTaskModel *)taskModel{
    if (!_taskModel) {
    _progressView.progress= (CGFloat)taskModel.actualValue /(CGFloat)taskModel.targetValue ;
        _progressView.titleLabel.text = _progressView.progress>=1?@"已完成":[NSString stringWithFormat:@"%.2f%@",taskModel.actualValue,taskModel.actualUnit?:@"元"];
        self.totalLabel.text = [NSString stringWithFormat:@"%.0f%@",taskModel.targetValue,taskModel.actualUnit?:@"元"];
    NSArray *detailArr = taskModel.tips;
    NSString *detail1 =  detailArr[0];
    NSString *detail2 =  detailArr.count>1?detailArr[1]:@"";
        NSString *detail3 =  detailArr.count>1?detailArr[2]:@"";
        NSString *detail4 =  detailArr.count>1?detailArr[3]:@"";

    NSMutableAttributedString *aString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@%@%@%@",detail1,detail2,detail3,detail4]];
    [aString addAttribute:NSForegroundColorAttributeName value:XHBlackColor range:NSMakeRange(0,detail1.length)];
    [aString addAttribute:NSForegroundColorAttributeName value:HexRGB(0xe6000f) range:NSMakeRange(detail1.length,detail2.length)];
    [aString addAttribute:NSForegroundColorAttributeName value:XHBlackColor range:NSMakeRange(detail1.length+detail2.length,detail3.length)];
    [aString addAttribute:NSForegroundColorAttributeName value:HexRGB(0xe6000f) range:NSMakeRange(detail1.length+detail2.length+detail3.length,detail4.length)];
    self.detailLabel.attributedText = aString;
    }
    _taskModel = taskModel;
}

- (void)ClickEvent:(UITapGestureRecognizer *)tapGesture {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(3) forKey:@"linkType"];
    [params setObject:_taskModel.url forKey:@"url"];
    [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
