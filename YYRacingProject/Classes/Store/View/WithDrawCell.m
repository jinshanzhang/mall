//
//  WithDrawCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "WithDrawCell.h"

@implementation WithDrawCell

-(void)createUI{
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:boldFont(17) textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    
    self.nameLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.cardLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [self.contentView addSubview:self.cardLabel];
    
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(13);
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(3);
    }];
    [self.cardLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.nameLabel.mas_right).mas_offset(5);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(3);
    }];
    
}

-(void)setModel:(DrawInfoModel *)model{
    self.titleLabel.text = model.bankName;
    self.nameLabel.text = [NSString stringWithFormat:@"%@ |",model.name];
    NSString *str2 = [model.accountNo substringFromIndex:model.accountNo.length-4];
    self.cardLabel.text = [NSString stringWithFormat:@"尾号%@",str2];

    
}

@end
