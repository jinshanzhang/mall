//
//  StoreMasterInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "StoreMasterInfoCell.h"

@interface StoreMasterInfoCell()

@property (nonatomic, strong) UILabel     *leftTitle;
@property (nonatomic, strong) UILabel     *leftSubTitle;
@property (nonatomic, strong) UIImageView *userHeadPicture;
@property (nonatomic, strong) UIImageView *arrowImageView;

@property (nonatomic, strong) UIView      *bottomLine;

@end

@implementation StoreMasterInfoCell

- (void)createUI {
    
    self.leftTitle = [YYCreateTools createLabel:nil
                                           font:midFont(15)
                                      textColor:XHBlackColor];
    [self.contentView addSubview:self.leftTitle];
    
    self.leftSubTitle = [YYCreateTools createLabel:nil
                                           font:midFont(12)
                                      textColor:XHBlackLitColor];
    self.leftSubTitle.hidden = YES;
    [self.contentView addSubview:self.leftSubTitle];

    self.userHeadPicture = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.contentView addSubview:self.userHeadPicture];
    
    self.arrowImageView = [YYCreateTools createImageView:@"mine_more_icon"
                                               viewModel:-1];
    [self.contentView addSubview:self.arrowImageView];
    
    self.bottomLine = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomLine];
    
    // 布局
    [self.leftTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.centerY.equalTo(self.contentView);
    }];
    [self.leftSubTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(22));
    }];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(1));
    }];
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
    }];
    [self.userHeadPicture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left).offset(-kSizeScale(10));
        make.centerY.equalTo(self.arrowImageView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(44),kSizeScale(44)));
    }];
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellClickEvent:)]];
}

- (void)setIsShowSubTitle:(BOOL)isShowSubTitle {
    _isShowSubTitle = isShowSubTitle;
    if (_isShowSubTitle) {
        //显示副标题
        self.leftSubTitle.hidden = NO;
        [self.userHeadPicture zy_cornerRadiusAdvance:kSizeScale(0) rectCornerType:UIRectCornerAllCorners];
        [self.leftTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.leftSubTitle);
            make.bottom.equalTo(self.leftSubTitle.mas_top).offset(-kSizeScale(5));
        }];
    }
    else {
        self.leftSubTitle.hidden = YES;
        [self.userHeadPicture zy_cornerRadiusAdvance:kSizeScale(15) rectCornerType:UIRectCornerAllCorners];
        [self.leftTitle mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.centerY.equalTo(self.contentView);
        }];
    }
}

- (void)setCellTitle:(NSString *)cellTitle {
    _cellTitle = cellTitle;
    self.leftTitle.text = _cellTitle;
}

- (void)setCellSubTitle:(NSString *)cellSubTitle {
    _cellSubTitle = cellSubTitle;
    self.leftSubTitle.text = _cellSubTitle;
}

- (void)setCellUserHead:(NSString *)cellUserHead {
    _cellUserHead = cellUserHead;
    if (kValidString(_cellUserHead)) {
        [self.userHeadPicture sd_setImageWithURL:[NSURL URLWithString:_cellUserHead] placeholderImage:[UIImage imageNamed:@"user_default_icon"]];
    }
    else {
        self.userHeadPicture.image = [UIImage imageNamed:@"user_default_icon"];
    }
}

#pragma mark - Event
- (void)cellClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.cellClickBlock) {
        self.cellClickBlock(self);
    }
}
@end
