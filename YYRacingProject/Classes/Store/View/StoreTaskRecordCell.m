//
//  StoreTaskRecordCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "StoreTaskRecordCell.h"
#import "StoreTaskView.h"

@interface StoreTaskRecordCell ()

@property (nonatomic, strong) UIView *bottonView;
@property (nonatomic, strong) NSMutableArray *taskViewArr;
@property (nonatomic, strong) UIImageView *moreTaskIMG;
@property (nonatomic, strong) UILabel  *rightTipLabel;
@property (nonatomic, strong) UIImageView *rightImageView;

@end

@implementation StoreTaskRecordCell

-(void)createUI{
    self.contentView.backgroundColor = XHStoreGrayColor;
    self.bottonView = [YYCreateTools createView:XHWhiteColor];
    self.bottonView.v_cornerRadius = 5;
    [self addSubview:self.bottonView];
    
    [self.bottonView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(-kSizeScale(12));
    }];

    UILabel *title = [YYCreateTools createLabel:@"店主任务" font:boldFont(15) textColor:XHBlackColor];
    [self.bottonView addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(16);
    }];

    self.rightTipLabel = [YYCreateTools createLabel:@"查看全部任务"
                                               font:midFont(13)
                                          textColor:XHBlackLitColor];
    self.rightTipLabel.userInteractionEnabled = YES;
    [self.bottonView addSubview:self.rightTipLabel];
    
    
    [self.rightTipLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityClickEvent:)]];
    
    self.rightImageView = [YYCreateTools createImageView:@"mine_arrow_icon"
                                               viewModel:-1];
    [self.bottonView addSubview:self.rightImageView];


    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(title);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(5), kSizeScale(9)));
    }];
    
    [self.rightTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.rightImageView.mas_left).offset(-kSizeScale(8));
        make.centerY.equalTo(title);
    }];
    
}

-(void)setTaskArr:(NSMutableArray *)taskArr{
    _taskArr = taskArr;
    
    for (UIView *view in self.bottonView.subviews) {
        if ([view isKindOfClass: [StoreTaskView class]]) {
            [view removeFromSuperview];
        }
    }
    self.taskViewArr = [NSMutableArray array];
    for (int i = 0;i< taskArr.count ; i++) {
        StoreTaskView *view = [[StoreTaskView alloc] init];
        view.taskModel = taskArr[i];
        [self.bottonView addSubview:view];
        [self.taskViewArr addObject:view];
    }
    [self.bottonView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(taskArr.count*117)+kSizeScale(50));
    }];
//
    if (self.taskArr.count>1) {

     [self.taskViewArr mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:kSizeScale(117) leadSpacing:kSizeScale(50) tailSpacing:0];
    
     [self.taskViewArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
     }];
    }else{
        StoreTaskView *view = self.taskViewArr[0];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kSizeScale(50));
            make.left.right.mas_equalTo(0);
        }];
    }
}



- (void)activityClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.headerClickBlock) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(3) forKey:@"linkType"];
        [params setObject:self.taskurl forKey:@"url"];
        self.headerClickBlock(params);
    }
}


@end
