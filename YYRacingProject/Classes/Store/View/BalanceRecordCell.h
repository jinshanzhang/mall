//
//  BalanceRecordCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "BalanceRecordModel.h"
#import "WithDrawRecordModel.h"

@interface BalanceRecordCell : YYBaseTableViewCell

@property (nonatomic, strong) BalanceRecordModel *model;

@property (nonatomic, strong) WithDrawRecordModel *drawModel;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UIImageView *arrowImageView;
@end
