//
//  StoreBottonSourceCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "HomeBannerInfoModel.h"
#import "HomeActivityIconInfoCell.h"

@interface StoreBottonSourceCell : YYBaseTableViewCell
<UICollectionViewDelegate,
UICollectionViewDataSource>

@property (nonatomic, strong) UIView *bottomView;

@property (nonatomic, strong) NSMutableArray *sourceArr;

@property (nonatomic, copy) void (^headerClickBlock)(NSMutableDictionary *params);

@property (nonatomic, strong) NSMutableArray    *iconLists;

@property (nonatomic, strong) UICollectionView  *iconListView;

@end
