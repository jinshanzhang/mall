//
//  StoreBannerCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "HomeResourceItemInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StoreBannerDelegate <NSObject>

@optional
- (void)headerDidSelected:(id)model;

@end

@interface StoreBannerCell : YYBaseTableViewCell

@property (nonatomic,strong) NSArray *imgArr;

@property (nonatomic, weak) id <StoreBannerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
