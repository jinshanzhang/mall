//
//  StoreInviteAndShareInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "HomeBannerInfoModel.h"

@interface StoreInviteAndShareInfoCell : YYBaseTableViewCell

@property (nonatomic, copy) void (^headerClickBlock)(NSMutableDictionary *params);
@property (nonatomic, strong) NSMutableArray *sourceArr;

@end
