//
//  WithDrawCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "DrawInfoModel.h"

@interface WithDrawCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *cardLabel;
@property (nonatomic, strong) DrawInfoModel *model;


@end
