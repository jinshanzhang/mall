//
//  StoreInComeAndBlanceInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "StoreInComeAndBlanceInfoCell.h"

@interface StoreInComeAndBlanceInfoCell()

@property (nonatomic, strong) UIView *inComeAndBlanceView;

@property (nonatomic, strong) NSMutableArray *inComeAndBlanceSubViews;
@property (nonatomic, strong) NSMutableArray *inComeAndBlanceSubLineViews;

@end

@implementation StoreInComeAndBlanceInfoCell

- (void)createUI {
    self.inComeAndBlanceSubViews = [NSMutableArray array];
    self.inComeAndBlanceSubLineViews = [NSMutableArray array];
    
    self.inComeAndBlanceView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.inComeAndBlanceView];
    
    [self.inComeAndBlanceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(85)).priorityHigh();
    }];
    
    for (int i = 0; i < 3; i ++) {
        YYOrderShowView *showView = [[YYOrderShowView alloc] initWithFrame:CGRectZero];
        showView.topSpace = (IS_IPHONE_5 ? kSizeScale(22) : kSizeScale(20));
        showView.itemSpace = kSizeScale(5);
        [showView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blanceClickOperator:)]];
        [self.inComeAndBlanceView addSubview:showView];
        [self.inComeAndBlanceSubViews addObject:showView];
        if (i > 0) {
            UIView *lineView = [YYCreateTools createView:XHLightColor];
            [self.inComeAndBlanceView addSubview:lineView];
            [self.inComeAndBlanceSubLineViews addObject:lineView];
        }
    }
    
    CGFloat showViewWidth = (kScreenW - 3)/3.0;
    [self yy_multipleViewsAutoLayout:self.inComeAndBlanceSubViews
                           superView:self.inComeAndBlanceView
                             padding:1
                            viewSize:CGSizeMake(showViewWidth, kSizeScale(85))];
    [self yy_multipleViewsAutoLayout:self.inComeAndBlanceSubLineViews
                           superView:self.inComeAndBlanceView
                             padding:showViewWidth + 1
                            viewSize:CGSizeMake(1, kSizeScale(40))];
}

- (void)setStoreInComeView:(NSMutableArray *)tipArrs
                    moneys:(NSMutableArray *)moneys {
    if (kValidArray(tipArrs) && kValidArray(moneys)) {
        if ((tipArrs.count == moneys.count) && (tipArrs.count == self.inComeAndBlanceSubViews.count)) {
            [self.inComeAndBlanceSubViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYOrderShowView *showView = (YYOrderShowView *)obj;
                NSMutableAttributedString *topAttri = [[NSMutableAttributedString alloc] initWithString:tipArrs[idx]];
                [topAttri yy_setAttributes:XHBlackColor
                                      font:boldFont(15)
                                   content:topAttri
                                 alignment:NSTextAlignmentCenter];
                
                NSMutableAttributedString *bottomAttri = [[NSMutableAttributedString alloc] initWithString:moneys[idx]];
                [bottomAttri yy_setAttributes:XHBlackLitColor
                                         font:midFont(12)
                                      content:bottomAttri
                                    alignment:NSTextAlignmentCenter];
                
                showView.topAttributed = topAttri;
                showView.bottomAttributed =  bottomAttri;
            }];
        }
    }
}

#pragma mark - Event
- (void)blanceClickOperator:(UITapGestureRecognizer *)tapGesture {
    if (self.BlanceClickBlock) {
        NSInteger type = ceilf(kScreenW/(tapGesture.view.frame.size.width + tapGesture.view.origin.x));
        self.BlanceClickBlock(type);
    }    
}

@end
