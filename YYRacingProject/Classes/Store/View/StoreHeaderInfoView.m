//
//  StoreHeaderInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "StoreHeaderInfoView.h"

@interface StoreHeaderInfoView()

@property (nonatomic, strong) UIImageView *userHeaderView;
@property (nonatomic, strong) UIImageView *identityImageView;  //身份标识

@property (nonatomic, strong) UILabel     *userNameLabel;
@property (nonatomic, strong) UILabel     *inviteCodeLabel;
@property (nonatomic, strong) UIButton    *codeCopyBtn;

@property (nonatomic, strong) UIView      *bottomView;
@property (nonatomic, strong) UIView      *inComeBottomView;

@property (nonatomic, strong) NSMutableArray   *inComeSubViews;
@property (nonatomic, strong) NSMutableArray   *inComeLineViews;

@end

@implementation StoreHeaderInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = HexRGB(0xFF2045);
        self.inComeSubViews = [NSMutableArray array];
        self.inComeLineViews = [NSMutableArray array];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.bottomView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.bottomView];
    
    self.userHeaderView = [YYCreateTools createImageView:nil viewModel:-1];
    [self.userHeaderView zy_cornerRadiusAdvance:kSizeScale(25) rectCornerType:UIRectCornerAllCorners];
    [self.userHeaderView zy_attachBorderWidth:1 color:XHWhiteColor];
    [self addSubview:self.userHeaderView];
    
    self.identityImageView = [YYCreateTools createImageView:@"Identity_icon"
                                                  viewModel:-1];
    [self addSubview:self.identityImageView];
    
    self.userNameLabel = [YYCreateTools createLabel:nil
                                               font:boldFont(17)
                                          textColor:XHWhiteColor];
    [self addSubview:self.userNameLabel];
    
    self.inviteCodeLabel = [YYCreateTools createLabel:nil
                                               font:midFont(12)
                                          textColor:XHWhiteColor];
    [self addSubview:self.inviteCodeLabel];
    
    self.codeCopyBtn = [YYCreateTools createBtn:@"点击复制"
                                           font:midFont(10)
                                      textColor:XHWhiteColor];
    self.codeCopyBtn.layer.borderColor = XHWhiteColor.CGColor;
    self.codeCopyBtn.layer.borderWidth = 0.5;
    [self.codeCopyBtn addTarget:self action:@selector(codeCopyClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.codeCopyBtn];
    
    self.inComeBottomView = [YYCreateTools createView:XHWhiteColor];
    self.inComeBottomView.layer.cornerRadius = 4.0;
    self.inComeBottomView.layer.shadowRadius = 10.0;
    self.inComeBottomView.layer.shadowColor = [UIColor blackColor].CGColor;//设置阴影的颜色
    self.inComeBottomView.layer.shadowOpacity = 0.06;//设置阴影的透明度
    self.inComeBottomView.layer.shadowOffset = CGSizeMake(0, 2);//设置阴影的偏移量
    [self addSubview:self.inComeBottomView];
    
    for (int i = 0; i < 3; i ++) {
        YYOrderShowView *showView = [[YYOrderShowView alloc] initWithFrame:CGRectZero];
        showView.topSpace = (IS_IPHONE_5 ? kSizeScale(29) : kSizeScale(26));
        showView.itemSpace = 3;
        [showView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blanceClickOperator:)]];
        [self.inComeBottomView addSubview:showView];
        [self.inComeSubViews addObject:showView];
        if (i > 0) {
            UIView *lineView = [YYCreateTools createView:XHLightColor];
            [self.inComeBottomView addSubview:lineView];
            [self.inComeLineViews addObject:lineView];
        }
    }
    
    /**布局**/
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(kSizeScale(44));
    }];
    
    [self.userHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(14));
        make.bottom.equalTo(self.bottomView.mas_top).offset(-kSizeScale(57));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(50)));
    }];
    
    [self.identityImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.userHeaderView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(60)));
    }];
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userHeaderView.mas_right).offset(kSizeScale(10));
        make.top.equalTo(self.userHeaderView.mas_top).offset(kSizeScale(3));
    }];
    
    [self.inviteCodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userNameLabel);
        make.top.equalTo(self.userNameLabel.mas_bottom).offset(kSizeScale(3));
    }];
    
    [self.codeCopyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.inviteCodeLabel.mas_right).offset(kSizeScale(15));
        make.centerY.equalTo(self.inviteCodeLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(20)));
    }];
    
    [self.inComeBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(84));
        make.bottom.equalTo(self.bottomView.mas_top).offset(kSizeScale(42));
    }];
    
    [self.inComeBottomView layoutIfNeeded];
    CGFloat showViewWidth = (kSizeScale(350) - 3)/3.0;
    [self yy_multipleViewsAutoLayout:self.inComeSubViews
                           superView:self.inComeBottomView
                             padding:1
                            viewSize:CGSizeMake(showViewWidth, kSizeScale(84))];
    [self yy_multipleViewsAutoLayout:self.inComeLineViews
                           superView:self.inComeBottomView
                             padding:showViewWidth + 1
                            viewSize:CGSizeMake(1, kSizeScale(40))];
}

- (void)setStoreName:(NSString *)storeName {
    _storeName = storeName;
    self.userNameLabel.text = _storeName;
}

- (void)setStoreInviteCode:(NSString *)storeInviteCode {
    _storeInviteCode = storeInviteCode;
    if (kValidString(_storeInviteCode)) {
        self.inviteCodeLabel.text = [NSString stringWithFormat:@"邀请码：%@",_storeInviteCode];
    }
}
- (void)setUpdateUserInfo {
    self.userNameLabel.text = kUserInfo.userName;
    if (kIsFan) {
        [self.userNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userHeaderView.mas_right).offset(kSizeScale(10));
            make.centerY.equalTo(self.userHeaderView);
        }];
    }
    else {
        [self.userNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.userHeaderView.mas_right).offset(kSizeScale(10));
            make.top.equalTo(self.userHeaderView.mas_top).offset(kSizeScale(3));
        }];
    }
    if (kValidString(kUserInfo.avatarUrl)) {
        [self.userHeaderView sd_setImageWithURL:[NSURL URLWithString:kUserInfo.avatarUrl] placeholderImage:[UIImage imageNamed:@"user_default_icon"]];
    }
    else {
        self.userHeaderView.image = [UIImage imageNamed:@"user_default_icon"];
    }
    self.inviteCodeLabel.text = [NSString stringWithFormat:@"邀请码：%@",kUserInfo.promotionCode];
    self.inviteCodeLabel.hidden = kIsFan;
    self.codeCopyBtn.hidden = kIsFan;
}

- (void)setStoreHeadImage:(NSString *)storeHeadImage {
    _storeHeadImage = storeHeadImage;
    if (kValidString(_storeHeadImage)) {
        [self.userHeaderView sd_setImageWithURL:[NSURL URLWithString:_storeHeadImage] placeholderImage:[UIImage imageNamed:@"user_default_icon"]];
    }
    else {
        self.userHeaderView.image = [UIImage imageNamed:@"user_default_icon"];
    }
    
    self.identityImageView.hidden = (kUserInfo.level<2)?YES:NO;
    if (kUserInfo.level >= 2) {
        NSString *identityPhotoName = nil;
        
        if (kUserInfo.level == 2) {
            identityPhotoName = @"owner_header_icon";
        }
        if (kUserInfo.level == 3) {
            identityPhotoName = @"agent_header_icon";
        }
        if (kUserInfo.level == 4) {
            identityPhotoName = @"partner_header_icon";
        }
        if (kValidString(identityPhotoName)) {
            self.identityImageView.image = [UIImage imageNamed:identityPhotoName];
        }
    }
}


- (void)setHeaderInComeView:(NSMutableArray *)tipArrs
                     moneys:(NSMutableArray *)moneys {
    if (kValidArray(tipArrs) && kValidArray(moneys)) {
        if ((tipArrs.count == moneys.count) && (tipArrs.count == self.inComeSubViews.count)) {
            [self.inComeSubViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYOrderShowView *showView = (YYOrderShowView *)obj;
                NSMutableAttributedString *topAttri = [[NSMutableAttributedString alloc] initWithString:tipArrs[idx]];
                [topAttri yy_setAttributes:XHBlackColor
                                      font:midFont(10)
                                   content:topAttri
                                 alignment:NSTextAlignmentCenter];
                
                NSMutableAttributedString *bottomAttri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",moneys[idx]]];
                [bottomAttri yy_setAttributes:XHRedColor
                                      font:boldFont(18)
                                   content:bottomAttri
                                    alignment:NSTextAlignmentCenter];
                
                showView.topAttributed = topAttri;
                showView.bottomAttributed =  bottomAttri;
            }];
        }
    }
}

#pragma mark - Event
- (void)codeCopyClickEvent:(UIButton *)sender {
    [YYCommonTools pasteboardCopy:self.storeInviteCode
                     appendParams:nil];
}



- (void)blanceClickOperator:(UITapGestureRecognizer *)tapGesture {
    if (self.headerClickBlock) {
        NSInteger type = ceilf(kScreenW/(tapGesture.view.frame.size.width + tapGesture.view.origin.x));
        self.headerClickBlock(type);
    }   
}
@end
