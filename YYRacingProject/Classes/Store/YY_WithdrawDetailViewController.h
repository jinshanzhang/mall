//
//  YY_WithdrawDetailViewController.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_WithdrawDetailViewController : YYBaseViewController


@property (nonatomic, assign) int withdrawId;

@end
