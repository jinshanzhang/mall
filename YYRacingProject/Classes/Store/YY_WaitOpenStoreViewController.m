//
//  YY_WaitOpenStoreViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_WaitOpenStoreViewController.h"
#import <WebKit/WebKit.h>
#import "YYWebBottomShareView.h"
#import "YY_WebScriptMessageDelegate.h"

@interface YY_WaitOpenStoreViewController ()
<
WKUIDelegate,
WKNavigationDelegate,
WKScriptMessageHandler>

@property (nonatomic, weak)   WKWebView             *m_webView;
@property (nonatomic, weak)   UIProgressView        *progressView;


@property (nonatomic, strong) YYWebBottomShareView  *shareView;

@end

@implementation YY_WaitOpenStoreViewController

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addNavigationItemWithImageName:@"detail_navbar_back"
                                     isLeft:YES
                                 clickEvent:^(UIButton *sender) {
                                    kPostNotification(LoginAndQuitSuccessNotification, @(2));
                                 }];
    [self xh_addNavigationItemWithTitle:@"暂不开店"
                                 isLeft:NO
                             clickEvent:^(UIButton *sender) {
                                 kPostNotification(LoginAndQuitSuccessNotification, @(2));
                             }];
    [self xh_addNavigationRightButtonRightPadding:12
                                      buttonTitle:@"暂不开店"];
    // 偏好设置， 涉及js交互
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.preferences = [[WKPreferences alloc] init];
    config.preferences.javaScriptEnabled = YES;
    config.preferences.javaScriptCanOpenWindowsAutomatically = YES;
    config.processPool = [[WKProcessPool alloc] init];
    config.allowsInlineMediaPlayback = YES;
    config.processPool = [[WKProcessPool alloc] init];
    
    //通过JS与webview内容交互
    WKUserContentController *content = [[WKUserContentController alloc]init];
    
    YY_WebScriptMessageDelegate *scriptDelegate = [[YY_WebScriptMessageDelegate alloc] initWithDelegate:self];
    [content addScriptMessageHandler:scriptDelegate name:@"toDetail"];
    [content addScriptMessageHandler:scriptDelegate name:@"toShare"];
    [content addScriptMessageHandler:scriptDelegate name:@"toPreviewPay"];
    [content addScriptMessageHandler:scriptDelegate name:@"getToken"];
    [content addScriptMessageHandler:scriptDelegate name:@"saveCode"];
    [content addScriptMessageHandler:scriptDelegate name:@"backUpperPage"];//返回上一个页面
    
    [content addScriptMessageHandler:scriptDelegate name:@"getTokenWithoutAction"];
    config.userContentController = content;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    webView.UIDelegate = self;
    webView.scrollView.bounces = NO;
    webView.allowsBackForwardNavigationGestures = YES;
    webView.navigationDelegate = self;
    webView.backgroundColor = HexRGB(0xfff9f8);
    [self.view addSubview:webView];
    
    self.m_webView = webView;
    
    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];

    UIProgressView *progreeView = [[UIProgressView alloc] initWithFrame:CGRectZero];
    progreeView.backgroundColor = [UIColor blueColor];
    //设置进度条的高度，下面这句代码表示进度条的宽度变为原来的1倍，高度变为原来的1.5倍.
    progreeView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.view addSubview:progreeView];
    self.progressView = progreeView;
    
    [progreeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(2);
    }];
    
    [webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    [webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];
    // 修改UserAgent
    [self.m_webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
        NSString *userAgent = result;
        NSString *newUserAgent = [userAgent stringByAppendingString:@"shellselect"];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newUserAgent, @"UserAgent", nil];
        [kUserDefault registerDefaults:dictionary];
        [kUserDefault synchronize];
        //setCustomUserAgent iOS9 新增方法
        if (@available(iOS 9.0, *)) {
            [self.m_webView setCustomUserAgent:newUserAgent];
        }
    }];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:kEnvConfig.fanShop]];
    [webView loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"title"]) {
        if (object == self.m_webView) {
            [self xh_addTitle:self.m_webView.title];
        }
    }
    else if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.progress = self.m_webView.estimatedProgress;
        if (self.progressView.progress == 1) {
            __weak typeof (self)weakSelf = self;
            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
            } completion:^(BOOL finished) {
                weakSelf.progressView.hidden = YES;
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    self.progressView.hidden = NO;
    [YYCenterLoading showCenterLoading];
    //开始加载网页的时候将progressView的Height恢复为1.5倍
    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    //防止progressView被网页挡住
    [self.view bringSubviewToFront:self.progressView];
}

// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    self.progressView.hidden = YES;
    [YYCenterLoading hideCenterLoading];
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    self.progressView.hidden = YES;
    [YYCenterLoading hideCenterLoading];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    //判断web是否新开窗口跳转一个新页面， target = "_black"
    if (navigationAction.targetFrame == nil) {
        [webView loadRequest:navigationAction.request];
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    @weakify(self);
    NSString *messageKey = message.name;
    // 返回上一层页面
    if ([messageKey isEqualToString:@"backUpperPage"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    // 保存二维码
    if ([messageKey isEqualToString:@"saveCode"]) {
        NSDictionary *messageBody = [NSDictionary dictionaryWithDictionary:message.body];
        NSString *imgUrl = [messageBody objectForKey:@"imgUrl"];
        if (kValidString(imgUrl)) {
            [kSaveManager savePhotoReachAlbum:[@[imgUrl] mutableCopy]];
        }
    }
    // 跳到商品详情。
    if ([messageKey isEqualToString:@"toDetail"]) {
        NSDictionary *messageBody = [NSDictionary dictionaryWithDictionary:message.body];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if ([messageBody objectForKey:@"url"]) {
            [params setObject:[messageBody objectForKey:@"url"] forKey:@"url"];
        }
        if ([messageBody objectForKey:@"linkType"]) {
            [params setObject:[messageBody objectForKey:@"linkType"] forKey:@"linkType"];
        }
        [YYCommonTools skipMultiCombinePage:self
                                     params:params];
    }
    // 弹出分享
    if ([messageKey isEqualToString:@"toShare"]) {
        BOOL  isSkipBorad = NO;
        NSNumber *withBorad = nil;
        //withBorad 0 不换起面板(或者null)  1 换起来
        // 0 好友   1 朋友圈    2  QQ好友     3 QQ空间     4 保存图片    5复制链接
        __block NSMutableArray *shareIcons = [NSMutableArray array];
        __block NSMutableArray *shareTitles = [NSMutableArray array];
        __block NSMutableArray *umShareArray = [NSMutableArray array];
        NSDictionary *messageBody = [NSDictionary dictionaryWithDictionary:message.body];
        if ([messageBody objectForKey:@"withBoard"]) {
            withBorad = [messageBody objectForKey:@"withBoard"];
        }
        if (withBorad) {
            if ([withBorad integerValue] == 1) {
                isSkipBorad = YES;
            }
        }
        NSDictionary *shareDict = [messageBody objectForKey:@"shareBodys"];
        NSArray *shareContents = [NSArray modelArrayWithClass:[YYShareInfoModel class] json:shareDict];
        [shareContents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger type = -1;
            BOOL  isShareLink = NO;
            NSString  *icon, *title = nil;
            YYShareInfoModel *shareModel = (YYShareInfoModel *)obj;
            type = shareModel.channel;
            if (type >= 5) { type = 5; }
            
            YYShareCombineInfoModel *umShareModel = [[YYShareCombineInfoModel alloc] init];
            umShareModel.title = kValidString(shareModel.shareTitle)?shareModel.shareTitle:nil;
            umShareModel.detailTitle = kValidString(shareModel.shareDesc)?shareModel.shareDesc:nil;
            umShareModel.webpageUrl = kValidString(shareModel.shareLinkUrl)?shareModel.shareLinkUrl:nil;
            umShareModel.thumbImage = kValidString(shareModel.shareImgUrl)?shareModel.shareImgUrl:nil;
            umShareModel.shareImage = kValidString(shareModel.shareImgUrl)?shareModel.shareImgUrl:nil;
            if (shareModel.shareType) {
                if ([shareModel.shareType integerValue] == 3) {
                    isShareLink = YES;
                }
            }
            umShareModel.isFriendShareLink = isShareLink;
            if (shareModel.shareImgUrl) {
                umShareModel.shareImageArray = [@[shareModel.shareImgUrl] mutableCopy];
            }
            umShareModel.operatorType = type;
            [umShareArray addObject:umShareModel];
            
            switch (shareModel.channel) {
                case 0: {
                    icon = @"wechat_icon"; title = @"微信好友";
                }
                    break;
                case 1: {
                    icon = @"friend_circle_icon"; title = @"朋友圈";
                }
                    break;
                case 4: {
                    icon = @"save_icon"; title = @"保存图片";
                }
                    break;
                case 5: {
                    icon = @"copy_link_icon"; title = @"复制链接";
                }
                    break;
                case 6: {
                    icon = @"share_password_icon"; title = @"分享口令";
                }
                    break;
                default:
                    break;
            }
            if (kValidString(icon)) {
                [shareIcons addObject:icon];
            }
            if (kValidString(title)) {
                [shareTitles addObject:title];
            }
        }];
        if (!isSkipBorad) {
            if (!kValidArray(umShareArray)) {
                [YYCommonTools showTipMessage:@"分享参数不能为空"];
                return;
            }
            YYShareCombineInfoModel *shareModel = [umShareArray firstObject];
            [kShareManager umengShareModel:shareModel
                                  callBack:^(BOOL status) {
                                      
                                  }];
        }
        else {
            self.shareView = [YYWebBottomShareView initYYPopView:[[YYWebBottomShareView alloc] initShowBottomView: shareIcons showTitles: shareTitles extend:[@{} mutableCopy]]];
            [self.shareView showViewOfAnimateType:YYPopAnimateDownUp];
            self.shareView.shareBlock = ^(NSInteger atIndex) {
                @strongify(self);
                YYLog(@"atIndex = %ld", atIndex);
                YYShareCombineInfoModel *shareModel = [umShareArray objectAtIndex:atIndex];
                //1.9.0 以后去除限制shareModel.isFriendShareLink = YES;
                [kShareManager umengShareModel:shareModel
                                      callBack:^(BOOL status) {
                                          
                                      }];
                [self.shareView showViewOfAnimateType:YYPopAnimateDownUp];
            };
        }
    }
    
    // 跳到确定订单页面
    if ([message.name isEqualToString:@"toPreviewPay"]) {
        NSDictionary *body = message.body;
        NSDictionary *dict = @{@"skuId":[body objectForKey:@"skuId"],
                               @"skuCnt":[body objectForKey:@"skuCnt"]};
        [kWholeConfig submitOrderWithSkulist:[@[dict] mutableCopy]
                                       souce:1
                                       addID:0
                               previewSwitch:1
                                    couponId:@[@(-1)]
                                     payment:@""
                                deliveryType:@"1" 
                                       block:^(PreOrderModel *model) {
                                           [YYCommonTools skipPreOrder:(YYBaseViewController *)[YYCommonTools getCurrentVC]
                                                                params:model
                                                                skuArr:[@[dict] mutableCopy]
                                                                source:1];
                                       } failBlock:^(PreOrderModel *model) {
                                           
                                       }];
    }
    // web 获取token操作处理
    if ([message.name isEqualToString:@"getToken"]) {
        NSString *jsStr = [NSString stringWithFormat:@"nativeFns('%@','%@','%@')",message.body,kUserInfo.useToken, kAPPVersion];
        [self.m_webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            
        }];
    }
    
    if ([message.name isEqualToString:@"getTokenWithoutAction"]) {
        NSString *jsStr = [NSString stringWithFormat:@"nativeFns('20000','%@')",kUserInfo.useToken];
        [self.m_webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            
        }];
    }
}

- (void)dealloc {
    //注意移除，否则导致11.0以下闪退
    [self.m_webView removeObserver:self forKeyPath:@"title"];
    [self.m_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"toShare"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"toDetail"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"toPreviewPay"];
    
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"getToken"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"saveCode"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"backUpperPage"];
    
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"getTokenWithoutAction"];
    
    self.m_webView = nil;
}

@end
