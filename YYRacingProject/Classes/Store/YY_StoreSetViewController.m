//
//  YY_StoreSetViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_StoreSetViewController.h"
#import "MineSettingInfoCell.h"
#import "StoreMasterInfoCell.h"

#import "YYUploadPhotoRequestAPI.h"
#import "YYStoreModifyUserHeadInfoRequestAPI.h"
@interface YY_StoreSetViewController ()
<UITableViewDelegate,
UITableViewDataSource,
TZImagePickerControllerDelegate>

@property (nonatomic, assign) NSInteger      selectRow;
@property (nonatomic, copy)   NSString       *modifyImageUrl;
@property (nonatomic, strong) NSMutableArray *cellTitles;

@end

@implementation YY_StoreSetViewController

#pragma mark - Life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.m_tableView) {
        [self.m_tableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"店铺设置"];
    [self xh_popTopRootViewController:NO];
    Class currentCls = [MineSettingInfoCell class];
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    registerClass(self.m_tableView, currentCls);
    currentCls = [StoreMasterInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    self.cellTitles = [@[@"店铺名称",@"店铺头像",@"店主名片"] mutableCopy];
    [self.m_tableView reloadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request method
/**更新店铺名片和店铺头像**/
- (void)updateStoreHeadRequest:(NSMutableDictionary *)parmas {
    YYStoreModifyUserHeadInfoRequestAPI *headAPI = [[YYStoreModifyUserHeadInfoRequestAPI alloc] initStoreModifyPictureInfoRequest:parmas];
    [headAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSMutableDictionary *params = [YYCommonTools objectTurnToDictionary:YES];
            if (self.selectRow == 1) {
                [[params objectForKey:@"homepage"] setObject:self.modifyImageUrl forKey:@"logo"];
            }
            else {
                [[params objectForKey:@"homepage"] setObject:self.modifyImageUrl forKey:@"card"];
            }
            [kUserManager modifyStoreInfo:params];
    
            [self.m_tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}

#pragma mark - Private method
/**上传头像操作**/
- (void)modifyUserHeaderOperator {
    @weakify(self);
    [yyImagePickerManagerClass openImagePickerCtrl:1
                                       columnCount:4
                                       isAllowCrop:YES
                                          delegate:self
                                          viewCtrl:self
                                           myBlock:^(NSArray *array) {
                                               @strongify(self);
                                               NSMutableArray *selectArrs = [array mutableCopy];
                                               NSMutableDictionary *params = [NSMutableDictionary dictionary];
                                               [params setObject:selectArrs forKey:@"photos"];
                                               [YYCenterLoading showCenterLoading];
                                               YYUploadPhotoRequestAPI *uploadAPI = [[YYUploadPhotoRequestAPI alloc] initUploadPhotoInfoRequest:params];
                                               [uploadAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
                                                   NSDictionary *responDict = request.responseJSONObject;
                                                   if (kValidDictionary(responDict)) {
                                                       NSArray *imgs = [responDict objectForKey:@"imgs"];
                                                       if (kValidArray(imgs)) {
                                                           NSString *imageKey = @"";
                                                           NSDictionary *imgDict = [imgs firstObject];
                                                           if ([imgDict objectForKey:@"imageKey"]) {
                                                               imageKey = [imgDict objectForKey:@"imageKey"];
                                                           }
                                                           if ([imgDict objectForKey:@"imageUrl"]) {
                                                               self.modifyImageUrl = [imgDict objectForKey:@"imageUrl"];
                                                           }
                                                           NSMutableDictionary *paramDicts = [NSMutableDictionary dictionary];
                                                           if (self.selectRow == 1) {
                                                               [paramDicts setObject:imageKey forKey:@"logo"];
                                                               [paramDicts setObject:@(1) forKey:@"photoType"];
                                                           }
                                                           else {
                                                               [paramDicts setObject:imageKey forKey:@"card"];
                                                               [paramDicts setObject:@(2) forKey:@"photoType"];
                                                           }
                                                           [self updateStoreHeadRequest:paramDicts];
                                                       }
                                                       
                                                   }
                                               } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
                                                   
                                               }];
                                           }];
}
/**cell 点击修改操作**/
- (void)cellClickOperator:(NSIndexPath *)currentIndex {
    NSInteger row = currentIndex.row;
    self.selectRow = row;
    if (row == 0) {
        [YYCommonTools skipNickNameSet:self
                                params:@{@"level":@(2)}];
    }
    else {
        [self modifyUserHeaderOperator];
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    Class currentCls = [MineSettingInfoCell class];
    if (row == 0) {
        MineSettingInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.setTitle = [self.cellTitles objectAtIndex:row];
        cell.isShowHead = (row != 0 ? YES : NO);
        cell.setContent = kStoreInfo.homepage.nickName;
        cell.cellClickBlock = ^(MineSettingInfoCell *setCell) {
            @strongify(self);
            NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:setCell];
            [self cellClickOperator:currentIndex];
        };
        return cell;
    }
    else {
        currentCls = [StoreMasterInfoCell class];
        StoreMasterInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.isShowSubTitle = (row != 1 ? YES : NO);
        cell.cellUserHead = (row != 1 ? kStoreInfo.homepage.card : kStoreInfo.homepage.logo);
        cell.cellTitle = [self.cellTitles objectAtIndex:row];
        cell.cellSubTitle = (row == 1 ? @"" : @"上传微信二维码名片方便导师/粉丝联系你");
        cell.cellClickBlock = ^(StoreMasterInfoCell *setCell) {
            @strongify(self);
            NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:setCell];
            [self cellClickOperator:currentIndex];
        };
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    return (row > 0? kSizeScale(80) : kSizeScale(48));
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
