//
//  YY_WithdrawDetailViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_WithdrawDetailViewController.h"
#import "OrderCommonCell.h"
#import "YYStoreDrawDetailRequestAPI.h"
#import "DrawDetailModel.h"

@interface YY_WithdrawDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)  UIView  *headerView;
@property (nonatomic, strong)  UIView  *statusLine;

@property (nonatomic, strong)  UIImageView *applyIMG;
@property (nonatomic, strong)  UIImageView *checkIMG;
@property (nonatomic, strong)  UIImageView *resultIMG;

@property (nonatomic, strong)  UILabel *applyTime;
@property (nonatomic, strong)  UILabel *checkTime;
@property (nonatomic, strong)  UILabel *resultTime;

@property (nonatomic, strong)  UILabel *statusLabel;
@property (nonatomic, strong)  UILabel *statusDetail;

@property (nonatomic, strong)  DrawDetailModel *drawModel;

@property (nonatomic, strong)  NSMutableArray *titleArr;
@property (nonatomic, strong)  NSMutableArray *detailArr;


@end

@implementation YY_WithdrawDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_addTitle:@"提现详情"];
    [self xh_popTopRootViewController:NO];
    [self creatHeader];

    Class currentCls = [OrderCommonCell class];
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    registerClass(self.m_tableView, currentCls);
    
    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    
    self.m_tableView.tableHeaderView = self.headerView;
    [self getData];

    // Do any additional setup after loading the view.
}
-(void)getData{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.withdrawId) forKey:@"withdrawId"];

    [YYCenterLoading showCenterLoading];
    YYStoreDrawDetailRequestAPI *api = [[YYStoreDrawDetailRequestAPI alloc] initWithDrawDetailRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.drawModel = [DrawDetailModel modelWithJSON:responDict];
            if (self.drawModel.presentStatus == 1) {
                self.statusLabel.text = @"待审核";
                [self.statusLine mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo((self.checkTime.centerX-self.applyTime.centerX)/2);
                }];
                self.checkTime.hidden = YES;
                self.resultTime.hidden = YES;
                
            }else if (self.drawModel.presentStatus == 2){
                self.statusLabel.text = @"审核通过";
                [self.statusLine mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo((self.checkTime.centerX-self.applyTime.centerX)/2*3);
                }];
                [self.checkIMG setImage:[UIImage imageNamed:@"withdrawCheck_red_icon"]];
                self.resultTime.hidden = YES;
            }else{
                if (self.drawModel.presentStatus == 3) {
                    self.statusLabel.text = @"审核失败";
                }else{
                    self.statusLabel.text = @"提现成功";
                }
                [self.statusLine mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(self.resultIMG.centerX-self.applyTime.centerX);
                }];
                [self.checkIMG setImage:[UIImage imageNamed:@"withdrawCheck_red_icon"]];
                [self.resultIMG setImage:[UIImage imageNamed:@"withdrawResult_red_icon"]];
            }
            
            NSArray *array = [self.drawModel.applyTime componentsSeparatedByString:@" "];
            self.applyTime.text = [NSString stringWithFormat:@"%@\n%@",array[0],array[1]];
            
            NSArray *array1 = [self.drawModel.auditTime componentsSeparatedByString:@" "];
            self.checkTime.text = [NSString stringWithFormat:@"%@\n%@",array1[0],array1[1]];
            
            NSArray *array2 = [self.drawModel.payTime componentsSeparatedByString:@" "];
            self.resultTime.text = [NSString stringWithFormat:@"%@\n%@",array2[0],array2[1]];
            
            self.statusDetail.text = self.drawModel.auditStatement;
            
            self.titleArr = [NSMutableArray arrayWithObjects:@"提现金额",@"预计手续费",@"银行卡姓名",@"账户",@"申请提现时间", nil];
            self.detailArr = [NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"￥%@",self.drawModel.applicationAmount],[NSString stringWithFormat:@"￥%@",self.drawModel.serviceFee],self.drawModel.name,self.drawModel.accountNo,self.drawModel.applyTime,nil];            
            [self.m_tableView reloadData];
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

-(void)creatHeader{
    self.headerView = [YYCreateTools createView:XHClearColor];
    self.headerView.frame  = CGRectMake(0, 0,kScreenW , 250);

    UIView *bg_view = [YYCreateTools createView:XHWhiteColor];
    [self.headerView addSubview:bg_view];

    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(10);
        make.bottom.mas_equalTo(-10);
    }];

    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [bg_view addSubview:lineView];

    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(55);
        make.right.mas_equalTo(-55);
        make.top.mas_equalTo(42);
        make.height.mas_equalTo(1);
    }];

    self.statusLine = [YYCreateTools createView:XHLoginColor];
    [bg_view addSubview:self.statusLine];
    
    [self.statusLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(55));
        make.top.mas_equalTo(42);
        make.height.mas_equalTo(1);
        make.width.mas_equalTo(10);
    }];

    
    self.applyIMG = [YYCreateTools createImageView:@"withdrawApply_red_icon" viewModel:-1];
    [bg_view addSubview:self.applyIMG];
    
    [self.applyIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(55));
        make.top.mas_equalTo(30);
    }];
    
    self.checkIMG = [YYCreateTools createImageView:@"withdrawCheck_gray_icon" viewModel:-1];
    [bg_view addSubview:self.checkIMG];
    
    [self.checkIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bg_view);
        make.top.mas_equalTo(30);
    }];
    
    self.resultIMG = [YYCreateTools createImageView:@"withdrawResult_gray_icon" viewModel:-1];
    [bg_view addSubview:self.resultIMG];
    
    [self.resultIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(55));
        make.top.mas_equalTo(30);
    }];
    
    UILabel *applyLabel = [YYCreateTools createLabel:@"提交申请" font:normalFont(13) textColor:XHBlackColor];
    [bg_view addSubview:applyLabel];
    
    [applyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.applyIMG);
        make.top.mas_equalTo(self.applyIMG.mas_bottom).mas_offset(12);
    }];
    
    UILabel *acheckLabel = [YYCreateTools createLabel:@"提现审核" font:normalFont(13) textColor:XHBlackColor];
    [bg_view addSubview:acheckLabel];
    
    [acheckLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.checkIMG);
        make.top.mas_equalTo(self.checkIMG.mas_bottom).mas_offset(12);
    }];
    
    UILabel *resultLabel = [YYCreateTools createLabel:@"提现结果" font:normalFont(13) textColor:XHBlackColor];
    [bg_view addSubview:resultLabel];
    
    [resultLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.resultIMG);
        make.top.mas_equalTo(self.resultIMG.mas_bottom).mas_offset(12);
    }];
    
    self.applyTime = [YYCreateTools createLabel:@"" font:normalFont(10) textColor:XHBlackLitColor];
    [bg_view addSubview:self.applyTime];
    self.applyTime.textAlignment = NSTextAlignmentCenter;
    self.applyTime.numberOfLines = 2;
    
    [self.applyTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.applyIMG);
        make.top.mas_equalTo(applyLabel.mas_bottom).mas_offset(10);
    }];
    
    self.checkTime = [YYCreateTools createLabel:@"" font:normalFont(10) textColor:XHBlackLitColor];
    [bg_view addSubview:self.checkTime];
    self.checkTime.textAlignment = NSTextAlignmentCenter;
    self.checkTime.numberOfLines = 2;
    
    [self.checkTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.checkIMG);
        make.top.mas_equalTo(acheckLabel.mas_bottom).mas_offset(10);
    }];
    
    self.resultTime = [YYCreateTools createLabel:@"" font:normalFont(10) textColor:XHBlackLitColor];
    [bg_view addSubview:self.resultTime];
    self.resultTime.textAlignment = NSTextAlignmentCenter;
    self.resultTime.numberOfLines = 2;
    
    [self.resultTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.resultIMG);
        make.top.mas_equalTo(resultLabel.mas_bottom).mas_offset(10);
    }];
    
    UIView *seperateLine = [YYCreateTools createView:XHLightColor];
    [bg_view addSubview:seperateLine];
    
    [seperateLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(self.checkTime.mas_bottom).mas_offset(24);
        make.height.mas_equalTo(2);
    }];
    
    self.statusLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
    [bg_view addSubview:self.statusLabel];
    
    [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(seperateLine.mas_bottom).mas_offset(15);
    }];
    
    self.statusDetail = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [bg_view addSubview:self.statusDetail];
    
    [self.statusDetail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(self.statusLabel.mas_bottom).mas_offset(8);
    }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class currentCls = [OrderCommonCell class];
    OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    cell.type = CellWithTwoLabel;
    cell.titleLabel.text =self.titleArr[indexPath.row];
    cell.detailLabel.text =self.detailArr[indexPath.row];
    if (indexPath.row == 0) {
        cell.detailLabel.textColor = XHRedColor;
    }
    cell.titleLabel.textColor = XHBlackLitColor;
    cell.lineView.hidden = YES;
    cell.originX = 16;
 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(35);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 40)];
    headerView.backgroundColor = XHWhiteColor;
    
    UILabel *title = [YYCreateTools createLabel:@"订单信息" font:normalFont(13) textColor:XHBlackLitColor];
    [headerView addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.centerY.equalTo(headerView);
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [headerView addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(-2);
        make.height.mas_equalTo(1);
    }];
    return headerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
