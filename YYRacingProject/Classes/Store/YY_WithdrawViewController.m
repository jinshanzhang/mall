//
//  YY_WithdrawViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_WithdrawViewController.h"
#import "WithDrawCell.h"
#import "YYStoreDrawInfoRequestAPI.h"
#import "DrawInfoModel.h"
#import "ApplyWithDrawRequestAPI.h"
@interface YY_WithdrawViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic, strong) UIView    *footerView;
@property (nonatomic, strong) UIButton  *applyBtn;
@property (nonatomic, strong) UITextField  *moneyTextfield;
@property (nonatomic, strong) UILabel  *moneyLabel;
@property (nonatomic, strong) UILabel  *timeLabel;
@property (nonatomic, strong) DrawInfoModel *drawInfo;

@end

@implementation YY_WithdrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creatFooter];
    [self getData];

    [self xh_addTitle:@"申请提现"];
    [self xh_popTopRootViewController:NO];
    Class currentCls = [WithDrawCell class];
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.scrollEnabled = NO;
    registerClass(self.m_tableView, currentCls);
    
    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    self.m_tableView.tableFooterView = self.footerView;
    
    // Do any additional setup after loading the view.
}

-(void)getData{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];

    [YYCenterLoading showCenterLoading];
    YYStoreDrawInfoRequestAPI *api = [[YYStoreDrawInfoRequestAPI alloc] initDrawInfoRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.drawInfo =[DrawInfoModel modelWithJSON:responDict];
            self.moneyLabel.text = [NSString stringWithFormat:@"可用余额: ¥%@",self.drawInfo.applyingIncome];
            self.timeLabel.text = self.drawInfo.auditTime;
            [self.m_tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];

}
-(void)applyDraw{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.moneyTextfield.text forKey:@"fee"];

    [YYCenterLoading showCenterLoading];
    ApplyWithDrawRequestAPI *api = [[ApplyWithDrawRequestAPI alloc] initApplyWighdrawRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            [JCAlertView showOneButtonsWithTitle:@"提示" Message:[NSString stringWithFormat:@"您的提现申请成功,%@",self.drawInfo.auditTime] ButtonTitle:@"我知道了" ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor Click:^{
                [self.navigationController popViewControllerAnimated:YES];
            } type:JCAlertViewTypeDefault];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)creatFooter{
    
    self.footerView = [YYCreateTools createView:XHClearColor];
    self.footerView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    
    UIView *bg_view = [YYCreateTools createView:XHWhiteColor];
    [self.footerView addSubview:bg_view];
    
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(120);
    }];
    
    UILabel *titleLabel = [YYCreateTools createLabel:@"提现金额(元)" font:normalFont(14) textColor:XHBlackLitColor];
    [bg_view addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(15);
    }];
    
    UILabel *dollerLabel = [YYCreateTools createLabel:@"¥" font:normalFont(35) textColor:XHBlackColor];
    [bg_view addSubview:dollerLabel];
    
    [dollerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.bottom.mas_equalTo(-20);
    }];
    
    self.moneyTextfield = [YYCreateTools createTextField:@"" font:normalFont(20) texColor:XHBlackColor];
    self.moneyTextfield.placeholder = @"输入提现金额";
    [bg_view addSubview:self.moneyTextfield];
    self.moneyTextfield.keyboardType  =  UIKeyboardTypeDecimalPad;
    [self.moneyTextfield addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    [self.moneyTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dollerLabel.mas_right).mas_offset(10);
        make.centerY.equalTo(dollerLabel);
        make.width.mas_equalTo(200);
    }];

    UIButton *btn = [YYCreateTools createBtn:@"全部" font:normalFont(14) textColor:XHLoginColor];
    [bg_view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(dollerLabel);
    }];
    
    btn.actionBlock = ^(UIButton *sender) {
        self.moneyTextfield.text = self.drawInfo.applyingIncome;
        [self textValueChanged];
    };
    
    self.moneyLabel = [YYCreateTools createLabel:@"0.00" font:normalFont(14) textColor:XHBlackColor];
    [self.footerView addSubview:self.moneyLabel];
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(bg_view.mas_bottom).mas_offset(16);
    }];
    
    self.applyBtn = [YYCreateTools createBtn:@"申请提现" font:normalFont(17) textColor:XHWhiteColor];
    [self.footerView addSubview:self.applyBtn];
    [self.applyBtn setBackgroundImage:[UIImage imageWithColor:XHLoginColor] forState:UIControlStateNormal];
    [self.applyBtn setBackgroundImage:[UIImage imageWithColor:XHGrayColor] forState:UIControlStateDisabled];
    self.applyBtn.enabled = NO;
    self.applyBtn.v_cornerRadius = 4;
    
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.moneyLabel.mas_bottom).mas_offset(24);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(50);
    }];
    @weakify(self);
    self.applyBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self applyDraw];
    };
    
    self.timeLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [self.footerView addSubview:self.timeLabel];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.footerView);
        make.top.mas_equalTo(self.applyBtn.mas_bottom).mas_offset(15);
    }];
}

#pragma mark - UITextField delegate method
- (void)textFieldDidChange:(UITextField *)textField {
    
    if ([textField.text floatValue] > [self.drawInfo.applyingIncome floatValue]) {
        self.moneyTextfield.text =self.drawInfo.applyingIncome;
    }
    [self textValueChanged];
}
- (void)textValueChanged {
    self.applyBtn.enabled = self.moneyTextfield.text.length >= 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    @weakify(self);
    Class currentCls = [WithDrawCell class];
    WithDrawCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    cell.model = self.drawInfo;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(67);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 10)];
    headerView.backgroundColor = XHClearColor;
    return headerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
