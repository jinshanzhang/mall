//
//  YY_StoreViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_StoreViewController.h"
#import "YY_StoreBalanceViewController.h"

#import "StoreOrderInfoCell.h"
#import "StoreHeaderInfoView.h"
#import "HomeBannerInfoModel.h"
#import "HomeResourceItemInfoModel.h"

#import "StoreInviteAndShareInfoCell.h"
#import "StoreInviteResourceCell.h"
#import "StoreTaskRecordCell.h"

#import "StoreBottonSourceCell.h"
#import "UIScrollView+StretchHeaderView.h"
#import "StoreBannerCell.h"


#import "YYStorePageInfoRequestAPI.h"
#import "YYStoreSaleAwardTipAPI.h"
#import "YYHomeScreeningShareInfoRequestAPI.h"
#import "YYStoreTaskRecordAPI.h"
#import "YYStoreOrdersInfoRequestAPI.h"
#import "YYCommonResourceAPI.h"
#import "YYCommonResourceInfoRequestAPI.h"

#import "StoreHeaderView.h"
#import "StoreTaskModel.h"

#import "yyCountDownManager.h"

typedef void(^resultBlock)(NSString *result);

@interface YY_StoreViewController ()

<UITableViewDelegate,
UITableViewDataSource,
StoreBannerDelegate>

@property (nonatomic, strong) NSMutableArray  *defaultEarnArrs; //默认收益
@property (nonatomic, strong) StoreInfoModel  *storeModel;      //店铺信息
@property (nonatomic, strong) NSMutableArray  *waistArr;
@property (nonatomic, strong) NSMutableArray  *bottonArr;
@property (nonatomic, strong) NSMutableArray  *bannerArr;
@property (nonatomic, strong) NSMutableArray  *taskArr;
@property (nonatomic, strong) NSMutableArray  *redValueArr;

@property (nonatomic, copy) NSString  *taskUrl;

@property (nonatomic, assign) BOOL             showWaist;
@property (nonatomic, assign) BOOL             showBotton;
@property (nonatomic, assign) BOOL             showBanner;

@property (nonatomic, strong) UIImageView *headerView;
@property(nonatomic,assign)   CGRect originFrame;
@property (nonatomic, strong) StoreHeaderView *headerClearView;
@property (nonatomic, strong) NSDictionary *rewardTipDic;
@property (nonatomic, strong) YYCompositeShareInfoModel *screenShareModel; //场次分享

@property (nonatomic, assign) int   inviteHeight;
@property (nonatomic, assign) int   taskHeight;

@end

@implementation YY_StoreViewController

#pragma mark -Setter method

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.isAddPullUpRefresh = NO;
        self.isAddPullDownRefresh = YES;
//        self.emptyTitle = @"您暂时还没有任何售后呢~";
//        self.NoDataType = YYEmptyViewOrderNoDataType;
        self.tableStyle = UITableViewStylePlain;
    }
    return self;
}


#pragma mark -Life cycle
-(void)creatHeaderView
{
    CGRect frame;
    if (IS_IPHONE_X) {
        frame =  CGRectMake(0, 0, kScreenW, kScreenW/1.6);
    }else{
        frame =  CGRectMake(0, 0, kScreenW, kScreenW/1.9);
    }
    @weakify(self);
    self.headerView = [UIImageView new];
    self.headerView.frame = frame;
    [self.headerView setImage:[UIImage imageNamed:@"headerorigin_view"]];
    self.originFrame = frame;
    
    self.headerClearView = [[StoreHeaderView alloc] initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height+21)];

    _headerClearView.backgroundColor = [UIColor clearColor];
    self.headerClearView.setClickBlock = ^(NSMutableDictionary * _Nonnull param) {
        @strongify(self);
        [YYCommonTools skipStoreSet:self];
    };
    self.headerClearView.shareClickBlock = ^(NSMutableDictionary * _Nonnull param) {
        @strongify(self);
        if (self.screenShareModel) {
        [YYCommonTools goodShareOperator:self.screenShareModel
                            expandParams:nil];
        }
    };
    //订单相关跳转
    self.headerClearView.BlanceClickBlock = ^(NSInteger type) {
       @strongify(self);
        if (type == 1) {
            [YYCommonTools skipBalance:self params:nil];
        }
        else {
            [YYCommonTools skipOrderList:self params:@{@"source": @(2)}];
        }
    };
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [MobClick event:@"storeView"];
    self.redValueArr = [NSMutableArray array];

    [self xh_alphaNavigation:0];
    [self xh_navBottomLine:XHClearColor];
    [self setNeedsStatusBarAppearanceUpdate];
 
    [self xh_addNavigationItemTextColor:XHWhiteColor
                                   font:midFont(15)
                                 isLeft:NO];

    UIView *bottom = [YYCreateTools createView:XHStoreGrayColor];
    [self.view addSubview:bottom];
    [bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(kScreenH/2);
        make.height.mas_offset(kScreenH);
    }];
    [self creatHeaderView];
    
    [kConfigCountDown setCountDownTimeInterval:0.1f];
    [kConfigCountDown addSourceWithIdentifier:@"storeCtrl"];
    [kConfigCountDown start];
    
    [self.view addSubview:self.headerView];
    Class  currentCls = [StoreInviteResourceCell class];
    self.tableStyle = UITableViewStyleGrouped;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 100;
    self.m_tableView.backgroundColor = XHClearColor;
    registerClass(self.m_tableView, currentCls);
    currentCls = [StoreTaskRecordCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [StoreOrderInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [StoreBottonSourceCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [StoreBannerCell class];
    registerClass(self.m_tableView, currentCls);

    [self.view addSubview:self.m_tableView];
    self.m_tableView.tableFooterView = [UIView new];
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.view);
    }];
    
    self.m_tableView.tableHeaderView = self.headerClearView;
    // Do any additional setup after loading the view.
    self.m_tableView.mj_header = [YYRacingBeckRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullDownRefresh)];

    [self getData];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)pullDownRefresh{
    [self getData];
    [self.m_tableView.mj_header endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma makr - Request
- (void)getSourceblock:(resultBlock)Block {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"69,23,1" forKey:@"types"];
    YYCommonResourceInfoRequestAPI *resourceAPI = [[YYCommonResourceInfoRequestAPI alloc] initCommonResourceRequest:dict];
    [resourceAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict) && [responDict objectForKey:@"items"]) {
            NSArray *tempItems = [responDict objectForKey:@"items"];
            for (NSDictionary *dic in tempItems) {
                NSInteger type =[[dic objectForKey:@"type"] integerValue];
                if (type == 1) {
                    self.bottonArr = [[NSArray modelArrayWithClass:[HomeBannerInfoModel class] json:[dic objectForKey:@"resources"]] mutableCopy];
                    self.showBotton = [[dic objectForKey:@"switchFlag"] boolValue];
                }
                if (type == 69) {
                self.waistArr =[[NSArray modelArrayWithClass:[HomeResourceItemInfoModel class] json:[dic objectForKey:@"resources"]] mutableCopy];
                self.showWaist = [[dic objectForKey:@"switchFlag"] boolValue];
                    int line1 = 0;
                    int line2 = 0;
                    for (int i = 0; i < self.waistArr.count; i++) {
                        HomeResourceItemInfoModel *itemModel = self.waistArr[i];
                        if (i == 0) {
                            line1 = [itemModel.height intValue]/2;
                        }else if (i == 4){
                            line2 = [itemModel.height intValue]/2;
                        }
                    }
                    self.inviteHeight = kSizeScale(line1+line2+24);
                }
                if (type == 23) {
                self.bannerArr =[[NSArray modelArrayWithClass:[HomeResourceItemInfoModel class] json:[dic objectForKey:@"resources"]] mutableCopy];
                self.showBanner = [[dic objectForKey:@"switchFlag"] boolValue];
                }
            }
            Block(@"");
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];

}

/**刷新店铺信息**/
#pragma mark 4个网络请求

-(void)getStoreOwnerInfoblock:(resultBlock)Block{
    @weakify(self);
    [kWholeConfig storeDetailInfoRequest:^(BOOL isSuccess) {
        @strongify(self);
        self.headerClearView.storeHeadImage = kStoreInfo.homepage.logo;
        self.headerClearView.storeName = kStoreInfo.homepage.nickName;
        self.headerClearView.storeInviteCode = kStoreInfo.user.promotionCode;
        Block(@"1");
    }isLoading:NO];
}

-(void)getStoreAwardTipInfoblock:(resultBlock)Block{
    @weakify(self);
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    YYStoreSaleAwardTipAPI *tipAPI = [[YYStoreSaleAwardTipAPI alloc] initRequest:dict];
    [tipAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
                if (!self.rewardTipDic) {
                    self.rewardTipDic = responDict;
                    self.headerClearView.v_h = self.headerClearView.frame.size.height+36;
                    self.headerClearView.rewardDic = self.rewardTipDic;
                    self.m_tableView.tableHeaderView = self.headerClearView;
                    self.headerClearView.rewardClickBlock = ^(NSMutableDictionary * _Nonnull param)
                    {
                        @strongify(self);
                        [YYCommonTools skipMultiCombinePage:self params:param];
                    };
                }
        }
        Block(@"1");
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        Block(@"0");
    }];
}

-(void)getTaskAPIblock:(resultBlock)Block{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(1) forKey:@"type"];
    YYStoreTaskRecordAPI *afterListAPI = [[YYStoreTaskRecordAPI alloc] initRequest:params];
    
    [afterListAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            self.taskArr = [[NSArray modelArrayWithClass:[StoreTaskModel class] json:[responDict objectForKey:@"items"]] mutableCopy];
                self.taskUrl = [responDict objectForKey:@"url"];
        }
        Block(@"1");
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        Block(@"0");
    }];
}

- (void)homeScreeningblock:(resultBlock)Block;
 {
    self.screenShareModel = [[YYCompositeShareInfoModel alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    YYHomeScreeningShareInfoRequestAPI *screenAPI = [[YYHomeScreeningShareInfoRequestAPI alloc] initHomeScreeningShareInfoRequest:params];
    [screenAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            self.screenShareModel.isNeedComposite = @(NO);  // 是否需要进行合成操作
            if ([responDict objectForKey:@"skipUrl"]) {
                self.screenShareModel.shareUrl = [responDict objectForKey:@"skipUrl"];
            }
            if ([responDict objectForKey:@"title"]) {
                self.screenShareModel.goodsTitle = [responDict objectForKey:@"title"];
            }
            if ([responDict objectForKey:@"content"]) {
                self.screenShareModel.goodsShortTitle = [responDict objectForKey:@"content"];
            }
            if ([responDict objectForKey:@"smallImageUrl"]) {
                self.screenShareModel.goodsShareImage = [responDict objectForKey:@"smallImageUrl"];
            }
            if ([responDict objectForKey:@"shareImageUrl"]) {
                self.screenShareModel.goodsShareBigImages = [responDict objectForKey:@"shareImageUrl"];
            }
        }
        Block(@"1");
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        Block(@"0");
    }];
}

- (void)getData {
    [kConfigCountDown reloadSourceWithIdentifier:@"storeCtrl"];
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_queue_create("queue", DISPATCH_QUEUE_SERIAL);
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        [self getStoreOwnerInfoblock:^(NSString *result) {
                dispatch_group_leave(group);
        }];
        NSLog(@"任务1");
    });
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        [self getStoreAwardTipInfoblock:^(NSString *result) {
            dispatch_group_leave(group);
        }];
        NSLog(@"任务2");
    });

    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        NSLog(@"任务3");
        self.waistArr = [NSMutableArray array];
        [self getSourceblock:^(NSString *result) {
            dispatch_group_leave(group);
        }];
    });
    
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        [self homeScreeningblock:^(NSString *result) {
            dispatch_group_leave(group);
        }];
        NSLog(@"任务4");
    });

    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        [self getTaskAPIblock:^(NSString *result) {
            dispatch_group_leave(group);
        }];
        NSLog(@"任务5");
    });
    
    dispatch_group_notify(group, queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"完成");
            [self.headerClearView setHeaderInComeView:[@[@"今日订单(笔)",@"今日收益(元)",@"本月收益(元)"] mutableCopy]
                                               moneys:kStoreInfo.curEarnArrs];
            [self.headerClearView setStoreInComeView:(kStoreInfo == nil ? self.defaultEarnArrs: kStoreInfo.totalEarnArrs)
                                              moneys:[@[@"待入账收益(元)",@"可用余额(元)",@"累计收益(元)"] mutableCopy]];
            self.redValueArr = kStoreInfo.orderCnts;
            [self.m_tableView reloadData];
        });
    });
}

#pragma mark -UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger section = indexPath.section;
    Class currentCls = [StoreInviteResourceCell class];
    if (section == 0) {
        StoreInviteResourceCell *cell =  [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (kValidArray(self.waistArr)) {
            cell.resourceArr = self.waistArr;
            cell.headerClickBlock = ^(NSMutableDictionary *params) {
                @strongify(self);
                [YYCommonTools skipMultiCombinePage:self params:params];
            };
        }
        cell.hidden = self.showWaist&&kValidArray(self.waistArr)?NO:YES;
        return cell;
    }
    else if (section == 1) {
        currentCls = [StoreTaskRecordCell class];
        StoreTaskRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (kValidArray(self.taskArr)) {
            cell.taskArr = self.taskArr;
            cell.taskurl = self.taskUrl;
            cell.headerClickBlock = ^(NSMutableDictionary * _Nonnull params) {
                @strongify(self);
                [YYCommonTools skipMultiCombinePage:self params:params];
            };
        }
        cell.hidden = kValidArray(self.taskArr)?NO:YES;
        return cell;
    }else if (section == 2 ){
        currentCls = [StoreBannerCell class];
        StoreBannerCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.hidden = self.showBanner&&kValidArray(self.bannerArr)?NO:YES;
        if (kValidArray(self.bannerArr)) {
            cell.imgArr = self.bannerArr;
            cell.delegate = self;
        }
        return cell;
    }
    else if (section == 3) {
            currentCls = [StoreOrderInfoCell class];
            StoreOrderInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
            [cell setMineOrder:[@[@"store_wait_pay_icon",@"store_wait_receive_icon",@"store_finish_icon",@"store_custome_service_icon"] mutableCopy] titles:[@[@"待付款",@"待收货",@"已完成",@"售后管理"] mutableCopy] topSpace:-10 itemSpace:kSizeScale(10) itemHeight:kSizeScale(88)];
           cell.redCounts = self.redValueArr;
             cell.orderClickBlock = ^(NSInteger type) {
                @strongify(self);
                if (type != 4) {
                  [YYCommonTools skipOrderList:self params:@{@"source": @(2),
                                                                @"currentPage":@(type)}];
                }else {
                [YYCommonTools skipMyAfterSalePage:self params:@{@"type": @(1)}];
                    }
                };
            return cell;
    }
    else {
        currentCls = [StoreBottonSourceCell class];
        StoreBottonSourceCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.sourceArr = self.bottonArr;
        cell.headerClickBlock = ^(NSMutableDictionary *params) {
            @strongify(self);
            [YYCommonTools skipMultiCombinePage:self params:params];
        };
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [YYCreateTools createView:XHStoreGrayColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
        return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    if (section == 0) {
        return UITableViewAutomaticDimension;
    }else if (section == 1)
    {
        return kValidArray(self.taskArr)?UITableViewAutomaticDimension:0;
    }else if (section==2){
        return self.showBanner&&kValidArray(self.bannerArr)?kSizeScale(80)+12:0;
    }else if (section==3 ) {
            return kSizeScale(136);
    }
    else if (section == 4) {
        return kSizeScale(self.bottonArr.count > 4?kSizeScale(200):kSizeScale(110));
    }
    return kSizeScale(48);
}



#pragma mark - Deprecated
/**点击其他模块**/
- (void)storeOtherMoubles:(NSInteger)type {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(3) forKey:@"linkType"];
    if (type == 1) {
        return;
    }
    else if (type == 2) {
        [YYCommonTools skipQRCode:self
                           params:@{@"title": @"我的导师",
                                    @"descript": @"微信扫码添加导师微信，享受一对一培训\n获取培训资料，每日爆款推荐等",
                                    @"type":@(2)}];
        return;
    }
    else {
        [params setObject:(type == 3 ? kEnvConfig.hostShop
                           : kEnvConfig.inviteFans) forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:self
                                 params:params];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        self.headerView.frame = ({
            CGRect frame = self.originFrame;
            frame.size.height = self.originFrame.size.height - offsetY;
            if (IS_IPHONE_X) {
                frame.size.width = frame.size.height *1.9;
            } else{
                frame.size.width = frame.size.height *2.3;
            }
            frame.origin.x = self.originFrame.origin.x - (frame.size.width - self.originFrame.size.width);
            frame;
        });
        self.headerView.centerX = kScreenW/2;
    } else{
        [self xh_addNavigationItemAlpha:5/offsetY isLeft:NO];
        self.headerView.frame = ({
            CGRect frame = self.originFrame;
            frame.size.height = self.originFrame.size.height - offsetY;
            frame;
        });
    }
}

#pragma mark - FriendCircleHeaderDelegate
- (void)headerDidSelected:(id)model {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    HomeResourceItemInfoModel *bannerModel = (HomeResourceItemInfoModel *)model;
    NSNumber *number = bannerModel.linkType;
    if (bannerModel) {
        [params setObject:number forKey:@"linkType"];
        [params setObject:bannerModel.url forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:self params:params];
}

/**点击邀请分享或开店大礼包**/
- (void)inviteFanAndOpenShopClick:(NSInteger)type {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(3) forKey:@"linkType"];
    [params setObject:(type != 1 ? kEnvConfig.inviteFans
                       : kEnvConfig.hostShop) forKey:@"url"];
    [YYCommonTools skipMultiCombinePage:self
                                 params:params];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
