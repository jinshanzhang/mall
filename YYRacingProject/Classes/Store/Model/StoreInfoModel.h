//
//  StoreInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class StoreMentorInfoModel,
StoreEarnInfoModel,
StoreHostInfoModel,
StorePromotionInfoModel;

@interface StoreInfoModel : NSObject

@property (nonatomic, strong) StoreMentorInfoModel *homepage;
@property (nonatomic, strong) StoreMentorInfoModel *superiorHomepage;
@property (nonatomic, strong) StoreEarnInfoModel *income;
@property (nonatomic, strong) NSMutableArray   *orderCnts;
@property (nonatomic, strong) StoreHostInfoModel *user;

@property (nonatomic, strong) NSMutableArray   *curEarnArrs;
@property (nonatomic, strong) NSMutableArray   *totalEarnArrs;

@property (nonatomic, strong) StorePromotionInfoModel *fansPromotion;
@property (nonatomic, strong) StorePromotionInfoModel *sellerPromotion;

@end

// 店铺导师信息
@interface StoreMentorInfoModel : NSObject

@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *logo;
@property (nonatomic, copy) NSString *card;

@end

// 店铺收益
@interface StoreEarnInfoModel : NSObject

@property (nonatomic, copy) NSString *todayOrderCnt; //今日订单数
@property (nonatomic, copy) NSString *todayIncome;     //今日收益
@property (nonatomic, copy) NSString *currentMonthIncome; //本月收益
@property (nonatomic, copy) NSString *totalIncome;      //累计收益
@property (nonatomic, copy) NSString *balanceIncome;    //可用余额
@property (nonatomic, copy) NSString *unBalanceIncome;  //待入账收益

@end

// 店铺店主信息
@interface StoreHostInfoModel : NSObject

@property (nonatomic, copy) NSString *avatarUrl;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *promotionCode;  //邀请码

@end


@interface StorePromotionInfoModel : NSObject

@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *link;

@end



