//
//  StoreTaskModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreTaskModel : NSObject


@property (nonatomic,assign) long currentTimestamp;
@property (nonatomic,assign) long startTimestamp;
@property (nonatomic,assign) long endTimestamp;

@property (nonatomic,assign) CGFloat actualValue;

@property (nonatomic,assign) CGFloat targetValue;

@property (nonatomic, strong) NSMutableArray *tips;
@property (nonatomic, copy)  NSString *actualUnit;
@property (nonatomic, copy)  NSString *url;

@end

NS_ASSUME_NONNULL_END
