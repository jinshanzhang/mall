//
//  BalanceRecordModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BalanceRecordModel : NSObject

@property (nonatomic, assign) int operType;//变更类型,0增加 1减少

@property (nonatomic, copy)  NSString   *operTime;//变更时间

@property (nonatomic, copy)  NSString   *amount;//提现金额

@property (nonatomic, copy)  NSString   *operName;//变更名称





@end
