//
//  StoreInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "StoreInfoModel.h"

@implementation StoreInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"homepage" : [StoreMentorInfoModel class],
             @"superiorHomepage" : [StoreMentorInfoModel class],
             @"income" : [StoreEarnInfoModel class],
             @"user" : [StoreHostInfoModel class],
             @"fansPromotion" : [StorePromotionInfoModel class],
             @"sellerPromotion" : [StorePromotionInfoModel class]
             };
}

@end


@implementation StoreMentorInfoModel

@end

@implementation StoreEarnInfoModel

@end

@implementation StoreHostInfoModel

@end

@implementation StorePromotionInfoModel



@end



