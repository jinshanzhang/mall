//
//  DrawDetailModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface DrawDetailModel : YYBaseRequestAPI


@property (nonatomic,copy) NSString *applicationAmount;
@property (nonatomic,copy) NSString *serviceFee;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *accountNo;
@property (nonatomic,copy) NSString *applyTime;
@property (nonatomic,copy) NSString *auditTime;
@property (nonatomic,copy) NSString *payTime;
@property (nonatomic,copy) NSString *auditStatement;

@property (nonatomic, assign) int presentStatus;





@end
