//
//  DrawInfoModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrawInfoModel : NSObject

@property (nonatomic, copy) NSString *bankName;

@property (nonatomic, copy) NSString *accountNo;
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *applyingIncome;

@property (nonatomic, copy) NSString *auditTime;

@property (nonatomic, assign) int ceritifiedFlag;

@property (nonatomic, assign) int contractFlag;





@end
