//
//  WithDrawRecordModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WithDrawRecordModel : NSObject

@property (nonatomic, copy) NSString *amountOfCash;
@property (nonatomic, copy) NSString *applyTime;
@property (nonatomic, assign) int presentStatus;//1:待审核，2：审核通过，3：提现失败，4：提现成功


@property (nonatomic, assign) int withdrawId;



@end
