//
//  YY_StoreBalanceViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/28.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_StoreBalanceViewController.h"
#import "YY_WithdrawViewController.h"
#import "OrderCommonCell.h"
#import "YY_BalanceRecordViewController.h"
#import "YY_WithdrawRecordController.h"
#import "YY_MineSignViewController.h"

@interface YY_StoreBalanceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSMutableArray *cellTitles;
@property (nonatomic, strong) UIView    *headView;
@property (nonatomic, strong) UILabel   *moneyLabel;
@property (nonatomic, strong) UIView    *footerView;
@property (nonatomic, strong) UIButton  *applyBtn;


@end

@implementation YY_StoreBalanceViewController


-(void)viewWillAppear:(BOOL)animated{
    @weakify(self);
    
    [kWholeConfig gainUserCenterInfo:^(BOOL isSucess) {
    } isLoading:YES];
    
    [kWholeConfig storeDetailInfoRequest:^(BOOL isSuccess) {
        @strongify(self);
        self.moneyLabel.text = kStoreInfo.income.balanceIncome;
        [self.m_tableView reloadData];
    }isLoading:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatHeader];
    [self creatFooter];
    
    [self xh_addTitle:@"可用余额"];
    [self xh_popTopRootViewController:NO];
    Class currentCls = [OrderCommonCell class];
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.allowsSelection = YES;
    registerClass(self.m_tableView, currentCls);
    
    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    self.cellTitles = [@[@"提现记录",@"余额变动记录"] mutableCopy];
    self.m_tableView.tableHeaderView = self.headView;
    self.m_tableView.tableFooterView = self.footerView;
    [self.m_tableView reloadData];
    // Do any additional setup after loading the view.
}


-(void)creatHeader{
    self.headView = [YYCreateTools createView:XHWhiteColor];
     self.headView.frame = CGRectMake(0, 0, kScreenW, 120);
    
    UILabel *title = [YYCreateTools createLabel:@"可用余额(元)" font:normalFont(12) textColor:XHBlackLitColor];
    [self.headView addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headView);
        make.top.mas_equalTo(34);
    }];
    
    self.moneyLabel = [YYCreateTools createLabel:@"0.00" font:boldFont(36) textColor:XHBlackColor];
    [self.headView addSubview:self.moneyLabel];
    
    [self.moneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headView);
        make.top.mas_equalTo(title.mas_bottom).mas_offset(12);
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self.headView addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.headView);
        make.height.mas_equalTo(1);
    }];
    
}

-(void)creatFooter{
    @weakify(self);
    self.footerView = [YYCreateTools createView:XHClearColor];
    self.footerView.frame = CGRectMake(0, 0, kScreenW, 400);
    
    self.applyBtn = [YYCreateTools createBtn:@"申请提现" font:normalFont(17) textColor:XHWhiteColor];
    [self.footerView addSubview:self.applyBtn];
    self.applyBtn.backgroundColor = XHLoginColor;
    self.applyBtn.v_cornerRadius = 4;
    
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(50);
    }];
    self.applyBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        if (kUserInfo.contractFlag == 1) {
            [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"您还未完成签约认证,完成后才可以提现至银行卡" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
                
            } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"去认证" Click:^{
                
                YY_MineSignViewController *vc = [YY_MineSignViewController new];
                [self.navigationController pushViewController:vc animated:YES];
                
            } type:JCAlertViewTypeDefault];
            return ;
        }
        
        if ([self.moneyLabel.text floatValue]<1) {
            [YYCommonTools showTipMessage:@"您的余额不足,暂时无法提现"];
            return;
        }
        YY_WithdrawViewController *vc = [YY_WithdrawViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    };
    
    UILabel *titleLabel = [YYCreateTools createLabel:@"温馨提示" font:normalFont(14) textColor:XHBlackColor];
    [self.footerView addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.width.mas_equalTo(100);
        make.top.mas_equalTo(self.applyBtn.mas_bottom).mas_offset(70);
    }];
    
    UILabel *title1 = [YYCreateTools createLabel:@"1. " font:normalFont(13) textColor:XHBlackLitColor];
    [self.footerView addSubview:title1];
    [title1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(15);
    }];
    
    UILabel *detail1 = [YYCreateTools createLabel:@"订单确认收货，且售后期完成后，对应佣金结算进入可用余额中;" font:normalFont(13) textColor:XHBlackLitColor];
    detail1.numberOfLines = 0;
    [self.footerView addSubview:detail1];
    [detail1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(15);
    }];
    
    UILabel *title2 = [YYCreateTools createLabel:@"2. " font:normalFont(13) textColor:XHBlackLitColor];
    [self.footerView addSubview:title2];
    [title2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(detail1.mas_bottom).mas_offset(2);
    }];
    
    UILabel *detail2 = [YYCreateTools createLabel:@"每个月可提现2次，10号和25号进行提现审核，审核通过后1-3天到账;" font:normalFont(13) textColor:XHBlackLitColor];
    detail2.numberOfLines = 0;
    [self.footerView addSubview:detail2];
    [detail2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(detail1.mas_bottom).mas_offset(2);
    }];
    
    
    UILabel *title3 = [YYCreateTools createLabel:@"3. " font:normalFont(13) textColor:XHBlackLitColor];
    [self.footerView addSubview:title3];
    [title3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(detail2.mas_bottom).mas_offset(2);
    }];
    
    UILabel *detail3 = [YYCreateTools createLabel:@"10号审核上个月25号至本月9号的提现申请,25号审核本月10号到本月24号的提现申请;" font:normalFont(13) textColor:XHBlackLitColor];
    detail3.numberOfLines = 0;
    [self.footerView addSubview:detail3];
    [detail3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(detail2.mas_bottom).mas_offset(2);
    }];
    
    
    UILabel *title4 = [YYCreateTools createLabel:@"4. " font:normalFont(13) textColor:XHBlackLitColor];
    [self.footerView addSubview:title4];
    [title4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(detail3.mas_bottom).mas_offset(2);
    }];
    
    UILabel *detail4 = [YYCreateTools createLabel:@"已有提现申请中,不可再次发起提现申请;" font:normalFont(13) textColor:XHBlackLitColor];
    detail4.numberOfLines = 0;
    [self.footerView addSubview:detail4];
    [detail4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(detail3.mas_bottom).mas_offset(2);
    }];

    UILabel *title5 = [YYCreateTools createLabel:@"5. " font:normalFont(13) textColor:XHBlackLitColor];
    [self.footerView addSubview:title5];
    [title5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(detail4.mas_bottom).mas_offset(2);
    }];

    UILabel *last = [YYCreateTools createLabel:@"请务必确认提现账户正确,因体提现账户填写错误导致提现未到账,损失由您承担。" font:normalFont(13) textColor:XHBlackLitColor];
    last.numberOfLines = 0;
    [self.footerView addSubview:last];
    [last mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(detail4.mas_bottom).mas_offset(2);
    }];


    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == 0? self.cellTitles.count : 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    @weakify(self);
    Class currentCls = [OrderCommonCell class];
    OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    cell.titleLabel.text = self.cellTitles[indexPath.row];
    cell.titleLabel.textColor = XHBlackColor;
 
    cell.type = CellWithTwoLabel;
    
    UIImageView *arrowImageView = [YYCreateTools createImageView:@"cellMore_icon"
                                                           viewModel:-1];
    [cell addSubview:arrowImageView];
    [arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kSizeScale(5),kSizeScale(9)));
            make.centerY.equalTo(cell);
            make.right.mas_equalTo(kSizeScale(-16));
        }];
    cell.titleLabel.font = normalFont(15);
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(46);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        YY_WithdrawRecordController *vc = [YY_WithdrawRecordController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        YY_BalanceRecordViewController *vc = [YY_BalanceRecordViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
