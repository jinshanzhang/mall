//
//  YY_QRCodeViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_QRCodeViewController.h"

#import "MineCustomerServiceInfoRequestAPI.h"
@interface YY_QRCodeViewController ()

@property (nonatomic, strong) UIView  *headerView;
@property (nonatomic, strong) UIImageView *qrcodeImageView;
@property (nonatomic, strong) UIImageView *serviceTimeImageView;
@property (nonatomic, strong) YYLabel     *descriLabel;
@property (nonatomic, strong) UIButton    *saveQRBtn;

@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSString *qrCodeURLString;
@property (nonatomic, strong) NSString *descriptString;
@end

@implementation YY_QRCodeViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = XHLightColor;
    [self xh_popTopRootViewController:NO];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"title"]) {
            [self xh_addTitle:[self.parameter objectForKey:@"title"]];
        }
        if ([self.parameter objectForKey:@"descript"]) {
            self.descriptString = [self.parameter objectForKey:@"descript"];
        }
        if ([self.parameter objectForKey:@"type"]) {
            self.type = [self.parameter objectForKey:@"type"];
        }
    }
    if ([self.type integerValue] == 1) {
        [self createServiceUI];
        [self linkCustomerServiceRequest];
    }
    else {
        [self createUI];
        self.qrCodeURLString = kStoreInfo.superiorHomepage.card;
        [self.qrcodeImageView yy_sdWebImage:kStoreInfo.superiorHomepage.card placeholderImageType:YYPlaceholderImageListGoodType];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private method
/**初始化子视图  导师类型**/
- (void)createUI {
    self.headerView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.top.mas_equalTo(kSizeScale(16)+kNavigationH);
    }];
    
    self.qrcodeImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.headerView addSubview:self.qrcodeImageView];
    
    [self.qrcodeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(60));
        make.centerX.equalTo(self.headerView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(175), kSizeScale(175)));
    }];
    
    self.descriLabel = [YYCreateTools createLabel:self.descriptString
                                             font:midFont(14)
                                        textColor:XHBlackColor
                                         maxWidth:kScreenW-(kSizeScale(16)*2+kSizeScale(40)*2)
                                    fixLineHeight:kSizeScale(20)];
    self.descriLabel.textAlignment = NSTextAlignmentCenter;
    self.descriLabel.numberOfLines = 2;
    [self.headerView addSubview:self.descriLabel];
    
    [self.descriLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(40));
        make.right.mas_equalTo(-kSizeScale(40));
        make.top.equalTo(self.qrcodeImageView.mas_bottom).offset(kSizeScale(42));
        make.bottom.equalTo(self.headerView.mas_bottom).offset(-kSizeScale(50)).priorityHigh();
    }];
    
    self.saveQRBtn = [YYCreateTools createButton:@"保存二维码"
                                         bgColor:XHRedColor
                                       textColor:XHWhiteColor];
    self.saveQRBtn.titleLabel.font = boldFont(17);
    self.saveQRBtn.layer.cornerRadius = 4.0;
    self.saveQRBtn.clipsToBounds = YES;
    [self.saveQRBtn addTarget:self action:@selector(saveClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveQRBtn];
    
    [self.saveQRBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.headerView);
        make.top.equalTo(self.headerView.mas_bottom).offset(kSizeScale(32));
        make.height.mas_equalTo(kSizeScale(50));
    }];
}

/**初始化子视图  客服类型**/
- (void)createServiceUI {
    self.headerView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.top.mas_equalTo(kSizeScale(16)+kNavigationH);
    }];
    
    self.qrcodeImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.headerView addSubview:self.qrcodeImageView];
    
    [self.qrcodeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(60));
        make.centerX.equalTo(self.headerView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(175), kSizeScale(175)));
    }];
    
    self.descriLabel = [YYCreateTools createLabel:self.descriptString
                                             font:midFont(14)
                                        textColor:XHBlackColor
                                         maxWidth:kScreenW-(kSizeScale(16)*2+kSizeScale(40)*2)
                                    fixLineHeight:kSizeScale(20)];
    self.descriLabel.textAlignment = NSTextAlignmentCenter;
    self.descriLabel.numberOfLines = 2;
    [self.headerView addSubview:self.descriLabel];
    
    [self.descriLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(40));
        make.right.mas_equalTo(-kSizeScale(40));
        make.top.equalTo(self.qrcodeImageView.mas_bottom).offset(kSizeScale(30));
    }];
    
    self.serviceTimeImageView = [YYCreateTools createImageView:@"service_time_icon"
                                                     viewModel:-1];
    [self.headerView addSubview:self.serviceTimeImageView];
    [self.serviceTimeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descriLabel.mas_bottom).offset(kSizeScale(28));
        make.centerX.equalTo(self.headerView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(240), kSizeScale(17)));
        make.bottom.equalTo(self.headerView.mas_bottom).offset(-kSizeScale(34)).priorityHigh();
    }];
    
    self.saveQRBtn = [YYCreateTools createButton:@"保存二维码"
                                         bgColor:XHRedColor
                                       textColor:XHWhiteColor];
    self.saveQRBtn.titleLabel.font = boldFont(17);
    self.saveQRBtn.layer.cornerRadius = 4.0;
    self.saveQRBtn.clipsToBounds = YES;
    [self.saveQRBtn addTarget:self action:@selector(saveClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveQRBtn];
    
    [self.saveQRBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.headerView);
        make.top.equalTo(self.headerView.mas_bottom).offset(kSizeScale(32));
        make.height.mas_equalTo(kSizeScale(50));
    }];
    
}

#pragma mark - Request
/**联系客服请求**/
- (void)linkCustomerServiceRequest {
    [YYCenterLoading showCenterLoading];
    MineCustomerServiceInfoRequestAPI *customerAPI = [[MineCustomerServiceInfoRequestAPI alloc] initCustomerServiceInfoRequest:[[NSDictionary dictionary] mutableCopy]];
    [customerAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            if ([responDict objectForKey:@"qrCode"]) {
                NSString *qrCode = [responDict objectForKey:@"qrCode"];
                self.qrCodeURLString = qrCode;
                [self.qrcodeImageView yy_sdWebImage:qrCode placeholderImageType:YYPlaceholderImageListGoodType];
            }
            if ([responDict objectForKey:@"wxId"]) {
                NSString *wxID = [responDict objectForKey:@"wxId"];
                self.descriptString = [NSString stringWithFormat:@"微信扫码添加客服微信\n或直接搜索微信号：%@", wxID];
                self.descriLabel.text = self.descriptString;
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - Event
- (void)saveClickEvent:(UIButton *)sender {
    if (kValidString(self.qrCodeURLString)) {
        [kSaveManager savePhotoReachAlbum:[@[self.qrCodeURLString] mutableCopy]];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
