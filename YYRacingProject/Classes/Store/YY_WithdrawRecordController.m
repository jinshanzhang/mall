//
//  YY_WithdrawRecordController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_WithdrawRecordController.h"
#import "BalanceRecordCell.h"
#import "WithDrawRecordModel.h"
#import "YYStoreWithdrawListRequestAPI.h"
#import "YY_WithdrawDetailViewController.h"



@interface YY_WithdrawRecordController ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) NSString *cursor;
@property (nonatomic, strong) NSNumber *hasMore;

@end

@implementation YY_WithdrawRecordController

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.NoDataType = YYEmptyViewNoDraw;
        self.tableStyle = UITableViewStyleGrouped;
        self.footerTitle = @"没有更多提现记录了";
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cursor = @"";
    [self xh_popTopRootViewController:NO];
    [self xh_addTitle:@"提现记录"];
    
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 72.0;
    self.m_tableView.allowsSelection = YES;
    
    Class current = [BalanceRecordCell class];
    registerClass(self.m_tableView, current);
    
    self.view.backgroundColor = self.m_tableView.backgroundColor = XHLightColor;
    
    [self loadNewer];
    // Do any additional setup after loading the view.
}

#pragma mark - Baser Request
- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.cursor forKey:@"cursor"];
    YYStoreWithdrawListRequestAPI *ListAPI = [[YYStoreWithdrawListRequestAPI alloc] initWithDrawListRequest:params];
    return ListAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.cursor = [responseDic objectForKey:@"cursor"];
        self.hasMore = [responseDic objectForKey:@"hasMore"];
        NSArray *couponList = [NSArray modelArrayWithClass:[WithDrawRecordModel class] json:[responseDic objectForKey:@"presentRecord"]];
        return couponList;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return [self.hasMore boolValue];
}

- (void)loadPullDownPage {
    self.cursor = @"";
    [super loadPullDownPage];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        NSInteger row = indexPath.row;
        Class currentCls = [BalanceRecordCell class];
        BalanceRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (kValidArray(self.listData) && row < self.listData.count) {
            WithDrawRecordModel *Model = [self.listData objectAtIndex:row];
            cell.drawModel = Model;
        }
        return cell;
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [YYCreateTools createView:XHLightColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        return  UITableViewAutomaticDimension;
    }
    return height;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.listData && indexPath.row<self.listData.count) {
    WithDrawRecordModel *model = self.listData[indexPath.row];
    YY_WithdrawDetailViewController *vc= [YY_WithdrawDetailViewController new];
    vc.withdrawId = model.withdrawId;
    [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
