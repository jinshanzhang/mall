//
//  YY_BalanceRecordViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_BalanceRecordViewController.h"
#import "BalanceRecordCell.h"
#import "BalanceRecordModel.h"
#import "YYStoreBalanceListRequestAPI.h"

@interface YY_BalanceRecordViewController ()<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) NSString *cursor;
@property (nonatomic, strong) NSNumber *hasMore;

@end

@implementation YY_BalanceRecordViewController

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.NoDataType = YYEmptyViewNoBalance;
        self.tableStyle = UITableViewStyleGrouped;
        self.footerTitle = @"没有更多余额记录了";

    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cursor = @"";
    [self xh_popTopRootViewController:NO];
    [self xh_addTitle:@"余额记录"];

    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 72.0;
    
    Class current = [BalanceRecordCell class];
    registerClass(self.m_tableView, current);

    self.view.backgroundColor = self.m_tableView.backgroundColor = XHLightColor;
    
    
    [self loadNewer];
    // Do any additional setup after loading the view.
}

#pragma mark - Baser Request
- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.cursor forKey:@"cursor"];
    YYStoreBalanceListRequestAPI *ListAPI = [[YYStoreBalanceListRequestAPI alloc] initBalanceListRequest:params];
    return ListAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.cursor = [responseDic objectForKey:@"cursor"];
        self.hasMore = [responseDic objectForKey:@"hasMore"];
        NSArray *couponList = [NSArray modelArrayWithClass:[BalanceRecordModel class] json:[responseDic objectForKey:@"items"]];
        return couponList;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return [self.hasMore boolValue];
}

- (void)loadPullDownPage {
    self.cursor = @"";
    [super loadPullDownPage];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        NSInteger row = indexPath.row;
        Class currentCls = [BalanceRecordCell class];
        BalanceRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (kValidArray(self.listData) && row < self.listData.count) {
            BalanceRecordModel *Model = [self.listData objectAtIndex:row];
            cell.model = Model;
        }
        return cell;
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [YYCreateTools createView:XHLightColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        return  UITableViewAutomaticDimension;
    }
    return height;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
