//
//  YYStoreModifyUserHeadInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYStoreModifyUserHeadInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initStoreModifyPictureInfoRequest:(NSMutableDictionary *)params;

@end
