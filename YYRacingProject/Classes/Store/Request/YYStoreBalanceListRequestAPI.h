//
//  YYStoreBalanceListRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYStoreBalanceListRequestAPI : YYBaseRequestAPI


- (instancetype)initBalanceListRequest:(NSMutableDictionary *)params ;

@end
