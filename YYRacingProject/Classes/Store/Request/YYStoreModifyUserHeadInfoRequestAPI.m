//
//  YYStoreModifyUserHeadInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreModifyUserHeadInfoRequestAPI.h"

@implementation YYStoreModifyUserHeadInfoRequestAPI

- (instancetype)initStoreModifyPictureInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSNumber *pictureType;
    NSString *requestMethod;
    if ([self.inParams objectForKey:@"photoType"]) {
        pictureType = [self.inParams objectForKey:@"photoType"];
        if ([pictureType integerValue] == 1) {
            requestMethod = STORELOGO;
        }
        else {
            requestMethod = STORECARD;
        }
        [self.inParams removeObjectForKey:@"photoType"];
    }
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,requestMethod];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
