//
//  YYStoreTaskRecordAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYStoreTaskRecordAPI.h"

@implementation YYStoreTaskRecordAPI

- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,STORETASK];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
