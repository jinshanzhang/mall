//
//  YYStoreOrdersInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreOrdersInfoRequestAPI.h"

@implementation YYStoreOrdersInfoRequestAPI

- (instancetype)initStoreOrdersInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,STOREORDERS];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end

