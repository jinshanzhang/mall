//
//  YYStoreOrdersInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYStoreOrdersInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initStoreOrdersInfoRequest:(NSMutableDictionary *)params;

@end
