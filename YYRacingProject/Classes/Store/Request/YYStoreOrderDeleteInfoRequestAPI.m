//
//  YYStoreOrderDeleteInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreOrderDeleteInfoRequestAPI.h"

@implementation YYStoreOrderDeleteInfoRequestAPI

- (instancetype)initOrderDeleteInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,STOREORDERDELETE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
