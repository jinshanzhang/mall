//
//  YYStoreDrawDetailRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreDrawDetailRequestAPI.h"

@implementation YYStoreDrawDetailRequestAPI

- (instancetype)initWithDrawDetailRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *orderID = nil;
    if (kValidDictionary(self.inParams)) {
        orderID = [self.inParams objectForKey:@"withdrawId"];
        [self.inParams removeObjectForKey:@"withdrawId"];
    }
    return [NSString stringWithFormat:@"%@%@/%@",INTERFACE_PATH,DRAWDETAIL,orderID];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}



@end
