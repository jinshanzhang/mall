//
//  YYStoreOrderDetailInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreOrderDetailInfoRequestAPI.h"

@implementation YYStoreOrderDetailInfoRequestAPI

- (instancetype)initOrderDetailInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *orderID = nil;
    if (kValidDictionary(self.inParams)) {
        orderID = [self.inParams objectForKey:@"orderID"];
        [self.inParams removeObjectForKey:@"orderID"];
    }
    return [NSString stringWithFormat:@"%@%@/%@",INTERFACE_PATH3,STOREORDERDETAILS,orderID];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end






