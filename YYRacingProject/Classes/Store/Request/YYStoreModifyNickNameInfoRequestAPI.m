//
//  YYStoreModifyNickNameInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreModifyNickNameInfoRequestAPI.h"

@implementation YYStoreModifyNickNameInfoRequestAPI

- (instancetype)initStoreNickNameInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,STORENICKNAME];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
