//
//  YYStoreDrawDetailRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYStoreDrawDetailRequestAPI : YYBaseRequestAPI
- (instancetype)initWithDrawDetailRequest:(NSMutableDictionary *)params ;

@end
