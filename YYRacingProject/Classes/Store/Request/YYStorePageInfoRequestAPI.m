//
//  YYStorePageInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStorePageInfoRequestAPI.h"

@implementation YYStorePageInfoRequestAPI

- (instancetype)initStorePageInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,STOREINFO];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
