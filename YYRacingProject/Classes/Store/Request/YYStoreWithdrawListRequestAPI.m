//
//  YYStoreWithdrawListRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYStoreWithdrawListRequestAPI.h"

@implementation YYStoreWithdrawListRequestAPI


- (instancetype)initWithDrawListRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,DRAWRECORD];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
