//
//  YYStoreOrderDetailInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYStoreOrderDetailInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initOrderDetailInfoRequest:(NSMutableDictionary *)params;

@end
