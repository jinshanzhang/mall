//
//  YY_SubmitInviteViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_SubmitInviteViewCtrl.h"
#import "YYGetProviderRequestAPI.h"

@interface YY_SubmitInviteViewCtrl ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITextField *telTextField;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UIButton *passBtn;

@property (nonatomic, copy)   NSString *promotionCode;

@end
@implementation YY_SubmitInviteViewCtrl

- (UITextField *)telTextField
{
    if (!_telTextField)
    {
        _telTextField = [[UITextField alloc] initWithFrame:CGRectMake(30, 10, 200, 60)];
        _telTextField.font = boldFont(18);
        _telTextField.delegate = self;
        _telTextField.keyboardType = UIKeyboardTypePhonePad;
        [_telTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"请填写邀请码" attributes:@{NSFontAttributeName: normalFont(15)}]];
    }
    return _telTextField;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self xh_popTopRootViewController:self.wxlogin];
    [self xh_navBackgroundColor:XHClearColor];
    self.view.backgroundColor = XHWhiteColor;
    
    [self xh_navBottomLine:XHClearColor];
    
    [self initHeadView];
    
    [self initFooterView];
    
    [self creatUI];
    
    [self creatTip];
    
    [self.telTextField addTarget:self action:@selector(textValueChanged) forControlEvents:UIControlEventEditingChanged];
    // Do any additional setup after loading the view.
}

-(void)creatTip{
    @weakify(self);
    self.submitBtn = [YYCreateTools createBtn:@"确定" font:boldFont(16) textColor:XHWhiteColor];
    self.submitBtn.v_cornerRadius = 4;
    self.submitBtn.enabled = NO;
    [self.view addSubview:self.submitBtn];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.bottom.mas_equalTo(-kSizeScale(90));
        make.height.mas_equalTo(45);
    }];
    
    self.submitBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self checkInvite];
    };
    
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:XHLoginColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:XHLightLoginColor] forState:UIControlStateDisabled];
    
    self.passBtn = [YYCreateTools createBtn:@"没有邀请人?" font:normalFont(13) textColor:XHBlackLitColor];
    [self.view addSubview:self.passBtn];
    [self.passBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.submitBtn.mas_bottom).mas_offset(10);
        make.centerX.equalTo(self.submitBtn);
    }];
    
    self.passBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self introTips];
    };
}

-(void)introTips
{
    [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"联系蜀黍之家助理微信\nbeikefz" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
    } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"复制微信号" Click:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = @"beikefz";
        [YYCommonTools  showTipMessage:@"已经复制到粘贴板"];
    } type:JCAlertViewTypeDefault];
}

-(void)initHeadView
{
    self.headerView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(100))];
    UIView *redLine = [YYCreateTools createView:XHLoginColor];
    [_headerView addSubview:redLine];
    
    [redLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(50);
        make.width.mas_equalTo(4);
        make.height.mas_equalTo(20);
    }];
    
    if (@available(iOS 8.2, *)) {
        UILabel *titleLabel = [YYCreateTools createLabel:@"填写邀请码" font:boldFont(23) textColor:XHBlackColor];
        [_headerView addSubview:titleLabel];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(redLine.mas_right).mas_offset(12);
            make.centerY.equalTo(redLine);
        }];
    } else {
        // Fallback on earlier versions
    }
}
-(void)initFooterView
{
    @weakify(self);
    self.footerView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kScreenH-100-55)];

    UILabel *title = [YYCreateTools createLabel:@"邀请码详情规则" font:boldFont(14) textColor:XHBlackColor];
    [self.footerView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(30);
    }];
    
    UILabel *detail = [YYCreateTools createLabel:@"1. 请联系你的邀请人，获取邀请码 \n2. 每个用户只能填写一次邀请码，填写之后无法修改" font:normalFont(13) textColor:XHBlackLitColor];
    [detail changeSpaceLineSpace:2 wordSpace:0];
    detail.numberOfLines = 2;
    [self.footerView addSubview:detail];
    
    [detail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(title.mas_bottom).mas_offset(10);
    }];
    
}

-(void)creatUI
{
    UIView *backgroundIMG = [UIView new];
    [backgroundIMG setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:backgroundIMG];
    
    [self.view insertSubview:self.navigationBar aboveSubview:backgroundIMG];
    
    [backgroundIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(0);
    }];
 
    self.m_tableView.frame = CGRectMake(0, kNavigationH, kScreenW, kScreenH);
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.tableFooterView = _footerView;
    self.m_tableView.tableHeaderView = _headerView;
    self.m_tableView.backgroundColor = XHClearColor;
    [self.view addSubview:self.m_tableView];
    
}

#pragma mark - table协议
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cellIdentifier"];
    cell.backgroundColor = XHClearColor;
    [cell addSubview:self.telTextField];
    
    UIView *line = [YYCreateTools createView:XHNEwLineColor];
    [cell addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.bottom.mas_equalTo(-1);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(1);
    }];
    return cell;
}
#pragma mark - UITextField delegate method
- (void)textValueChanged {
    self.submitBtn.enabled = (self.telTextField.text.length >= 3 && self.telTextField.text.length < 9);
}

#pragma mark request
-(void)checkInvite
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.telTextField.text forKey:@"promotionCode"];
    [YYCenterLoading showCenterLoading];
    YYGetProviderRequestAPI *loginAPI = [[YYGetProviderRequestAPI alloc] initWithProviderRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request)
     {
         NSDictionary *responDict = request.responseJSONObject;
         [YYCenterLoading hideCenterLoading];
         if (kValidDictionary(responDict)) {
             self.promotionCode = [responDict objectForKey:@"promotionCode"];
             [JCAlertView showTwoButtonsAndOneIMGWithTitle:@"推荐人确认" IMG:[responDict objectForKey:@"avatarUrl"]  Message:[responDict objectForKey:@"nickName"] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消重填" Click:^{
                 
             } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"确认并登录" Click:^{
                 [kWholeConfig bindPromotionRequset:self.promotionCode
                                              token:self.useToken];
             } type:JCAlertViewTypeOneHeadIMG];
         }
     } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
     }];
}

@end


