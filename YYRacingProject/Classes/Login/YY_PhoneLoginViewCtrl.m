//
//  YY_PhoneLoginViewCtrl.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_PhoneLoginViewCtrl.h"
#import "YYReceiveCodeRequestAPI.h"
#import "YY_SubmitInviteViewCtrl.h"

#import "LoginUserLoginInfoRequestAPI.h"
#import "YYLoginImageRequestAPI.h"
#import "TouristLoginSetAPI.h"
#import "YY_PhoneLoginViewCtrl.h"
#import "YYWxAuthAppRequestAPI.h"
#import "YY_SubmitInviteViewCtrl.h"
#import "yyPhotoDownloadCacheManager.h"

#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "VideoCutter.h"

#import "LoginUserLoginInfoRequestAPI.h"
#import "AppDelegate.h"

@interface YY_PhoneLoginViewCtrl ()<UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate>
{
    UIView *headerView;
    UIView *footerView;
    NSInteger seconds; // 倒计时秒数
    NSString *count;
    NSString *pwd;
}

@property (nonatomic, strong) UIButton * closeButton;

@property (nonatomic, strong) UITextField *telTextField;
@property (nonatomic, strong) UITextField *pwdTextField;

@property (nonatomic, strong) UIButton *codeBtn;

@property (nonatomic, copy)   NSString *telPhone;
@property (nonatomic, copy)   NSString *phone;
@property (nonatomic, copy)   NSString *smscode;
@property (nonatomic, copy)   NSString *promotionCode;
@property (nonatomic, assign)  BOOL  promotionState;

@property (nonatomic, assign) BOOL firstlogin;

@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) NSMutableArray *inviteModelArr;

@property (nonatomic, copy) NSString *blackBox;

@property (nonatomic, assign) int guestMode;
@property (nonatomic, strong) UIImage *adcIMG;

@end

@implementation YY_PhoneLoginViewCtrl

- (UIButton *)closeButton {
    if(!_closeButton) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setImage:[UIImage imageNamed:@"navbar_close_black"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (void)closeBtnClick {
//    self.navigationController.tabBarController.selectedIndex = 0;
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [[YYCommonTools getCurrentVC].navigationController popToRootViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"KPopToRootVCNoty" object:nil];
    appDelegate.tabbar.selectedIndex = 0;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIButton *)codeBtn
{
    if (!_codeBtn)
    {
        _codeBtn = [UIButton new];
        _codeBtn.frame = CGRectMake(kScreenW - 116, 10, kSizeScale(100), 60);
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _codeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        _codeBtn.titleLabel.font = midFont(15);
        _codeBtn.layer.cornerRadius = 5;
        _codeBtn.enabled = NO;
        [_codeBtn setTitleColor:XHLoginColor forState:UIControlStateNormal];
        [_codeBtn setTitleColor:HexRGB(0x999999) forState:UIControlStateDisabled];
        [_codeBtn addTarget:self action:@selector(vCodeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _codeBtn;
}

- (UITextField *)telTextField
{
    if (!_telTextField)
    {
        _telTextField = [[UITextField alloc] initWithFrame:CGRectMake(16, 10, kScreenW - 32, 60)];
        _telTextField.font = midFont(15);
//        _telTextField.backgroundColor = [UIColor redColor];
        _telTextField.textColor = HexRGB(0x222222);
        _telTextField.delegate = self;
        _telTextField.tag = 1;
        _telTextField.keyboardType = UIKeyboardTypePhonePad;
        [_telTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"请输入手机号" attributes:@{NSFontAttributeName: normalFont(15)}]];
        
    }
    return _telTextField;
}

- (UITextField *)pwdTextField
{
    if (!_pwdTextField)
    {
        _pwdTextField = [[UITextField alloc] initWithFrame:CGRectMake(16, 10, kScreenW - 32, 60)];
        _pwdTextField.font = midFont(15);
        _pwdTextField.keyboardType = UIKeyboardTypePhonePad;
        _pwdTextField.delegate = self;
        [_pwdTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"请输入验证码" attributes:@{NSFontAttributeName: normalFont(15)}]];

    }
    return _pwdTextField;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    if (![kUserDefault boolForKey:@"isContainPrivacy"]) {
//        YYAlertView *alert = [[YYAlertView alloc] initWithFrame:CGRectMake(0, 0, kScreenW-kSizeScale(58), kSizeScale(317)) title:@"欢迎使用“蜀黍之家”APP，我们深知个人信息对您的重要性，并会尽全力保护您的个人信息安全可靠。我们致力于维持您对我们的信任，恪守以下原则，保护您的个人信息，详情查看《蜀黍之家用户隐私政策》。" subTitle:@"如同意此政策，请点击“同意”并开始使用蜀黍之家，我们承诺，我们将按业界成熟的安全标准，采取相应的安全保护措施来保护您的个人信息。" alertType:YYAlertPrivacyPolicyType otherBlock:^(NSInteger index) {
//            NSMutableDictionary *params = [NSMutableDictionary dictionary];
//            [params setObject:@(3) forKey:@"linkType"];
//            [params setObject:PrivacyURL forKey:@"url"];
//            [YYCommonTools skipMultiCombinePage:self
//                                         params:params];
//        } confirmBlock:^{
//            [kUserDefault setBool:YES forKey:@"isContainPrivacy"];
//        } cancelBlock:^{
//            exit(0);
//        }];
//        [alert showAlertView];
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self xh_popTopRootViewController:NO];
    [self xh_navBackgroundColor:XHClearColor];
    [self xh_navBottomLine:XHClearColor];
    [self initHeadView];
    [self initFooterView];
    
    [self monitorNetwork];
    [self getVersionData];
    [self getAdvPic];
    
    [self creatUI];
    [self creatTips];
    
    [self.telTextField addTarget:self action:@selector(textValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.pwdTextField addTarget:self action:@selector(textValueChanged) forControlEvents:UIControlEventEditingChanged];
//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//    /*
//     * 获取设备指纹黑盒数据，请确保在应用开启时已经对SDK进行初始化，切勿在get的时候才初始化
//     * 如果此处获取到的blackBox特别长(超过400字节)，说明初始化尚未完成(一般需要1-3秒)，或者由于网络问题导致初始化失败，进入了降级处理
//     * 降级不影响正常设备信息的获取，只是会造成blackBox字段超长，且无法获取设备真实IP
//     * 降级数据平均长度在2KB以内,一般不超过3KB,数据的长度取决于采集到的设备信息的长度,无法100%确定最大长度
//     */
//    self.blackBox = manager->getDeviceInfo();
    YYLog(@"同盾设备指纹数据: %@", _blackBox);
    [self monitorNetwork];
}


- (void)getVersionData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    TouristLoginSetAPI *loginAPI = [[TouristLoginSetAPI alloc] initWithRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            self.guestMode = [[responDict objectForKey:@"guestMode"] intValue];
            [self creatUI];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 *  广告图片
 */
- (void)getAdvPic{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    YYLoginImageRequestAPI *loginAPI = [[YYLoginImageRequestAPI alloc] initRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            
             self.adcIMG = [kPhotoCacheConfig readImageSource:@"photo"
                                                      directory:@"login"];
            
            NSMutableArray *tempArray = [NSMutableArray array];
            [tempArray addObject:[responDict objectForKey:@"imageUrl"]];
            [kPhotoCacheConfig downloadPhotoSources:tempArray
                                          directory:@"login"
                                           iconName:@"photo"
                                        isAddSuffix:NO
                                          callBlock:^(BOOL isSuccess) {
                                              UIImage *image = [kPhotoCacheConfig readImageSource:@"photo"
                                                                                        directory:@"login"];
                                              if (image) {
                                                  self.adcIMG = image;
                                              }
                                          }];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

-(void)initHeadView
{
    headerView =[[UIView alloc] initWithFrame:CGRectMake(0, 33, kScreenW, kSizeScale(55 + 20))];
//    headerView.backgroundColor = [UIColor redColor];
    UIView *redLine = [YYCreateTools createView:XHLoginColor];
    [headerView addSubview:redLine];
    
    [redLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(33);
        make.width.mas_equalTo(4);
        make.height.mas_equalTo(19);
    }];
 
    if (@available(iOS 8.2, *)) {
        
        UILabel *titleLabel = [YYCreateTools createLabel:self.bindTitle?:@"手机快捷登录" font:boldFont(23) textColor:XHBlackColor];
        [headerView addSubview:titleLabel];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(redLine.mas_right).mas_offset(11);
            make.centerY.equalTo(redLine);
        }];
    } else {
        // Fallback on earlier versions
    }
}

-(void)creatTips
{
    @weakify(self);
    self.loginBtn =[YYCreateTools createBtn:@"立即登录" font:boldFont(16) textColor:XHWhiteColor];
    self.loginBtn.v_cornerRadius = 4;
    self.loginBtn.enabled = NO;
    [footerView addSubview:self.loginBtn];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16).priorityHigh();
//        make.bottom.mas_equalTo(-kSizeScale(90));
        make.centerY.equalTo(footerView);
        make.height.mas_equalTo(49);
    }];
    
    /*UIImage *bgImg = [UIImage gradientColorImageFromColors:@[HexRGB(0xfe432c),HexRGB(0xfd2263)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(120, 36)];
     */
    [self.loginBtn setBackgroundImage:[UIImage imageWithColor:XHLoginColor] forState:UIControlStateNormal];
    [self.loginBtn setBackgroundImage:[UIImage imageWithColor:HexRGBALPHA(0x3785F7, 0.5)] forState:UIControlStateDisabled];
    
    _loginBtn.actionBlock = ^(UIButton *sender) {
        
        @strongify(self);
        [self userLoginRequest];
    };
    
    UILabel *titleLabel = [YYCreateTools createLabel:@"登录代表你已同意蜀黍之家" font:normalFont(12) textColor:XHBlackLitColor];
    titleLabel.textAlignment = NSTextAlignmentRight;
    [footerView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(80);
//        make.right.equalTo(footerView.mas_centerX).offset(-8);
        make.top.mas_equalTo(self.loginBtn.mas_bottom).mas_offset(15);
    }];
    
//    UIButton *secretBtn = [YYCreateTools createBtn:@"《隐私协议》" font:normalFont(12) textColor:XHLightBlueColor];
//    [footerView addSubview:secretBtn];
//    [secretBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(titleLabel.mas_right).mas_offset(1);
////        make.top.mas_equalTo(self.loginBtn.mas_bottom).mas_offset(8);
//        make.centerY.equalTo(titleLabel);
//        make.height.mas_equalTo(20);
//    }];
//    secretBtn.actionBlock = ^(UIButton *sender) {
//        NSDictionary *dict = @{@"linkType":@(3), @"url":PrivacyURL};
//        [YYCommonTools skipMultiCombinePage:self params:dict];
//    };
//    及隐私权政策
    UIButton *userBtn = [YYCreateTools createBtn:@"《用户协议》" font:normalFont(12) textColor:XHLightBlueColor];
    [footerView addSubview:userBtn];
    [userBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(titleLabel.mas_right).mas_offset(5);
//        make.top.mas_equalTo(self.loginBtn.mas_bottom).mas_offset(8);
        make.centerY.equalTo(titleLabel);
        make.height.mas_equalTo(25);
    }];
    userBtn.actionBlock = ^(UIButton *sender) {
        NSDictionary *dict = @{@"linkType":@(3), @"url":UserAgreementURL};
        [YYCommonTools skipMultiCombinePage:self params:dict];
    };
   
}
-(void)introTips
{
    [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"联系蜀黍之家助理微信\nbeikefz" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
    } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"复制微信号" Click:^{
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = @"beikefz";
        [YYCommonTools  showTipMessage:@"已经复制到粘贴板"];
    } type:JCAlertViewTypeDefault];

}

-(void)initFooterView {
    
    footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(97))];

//    @weakify(self);
//    UIButton *btn = [YYCreateTools createBtn:@"无法登录? 请联系客服"];
//    [btn setTitleColor:XHBlackLitColor forState:UIControlStateNormal];
//    btn.titleLabel.font = normalFont(13);
//    [footerView addSubview:btn];
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(10);
//        make.left.mas_equalTo(30);
//        make.height.mas_equalTo(kSizeScale(46));
//    }];
//    btn.actionBlock = ^(UIButton *sender) {
//        @strongify(self);
//        [self introTips];
//    };
}

-(void)creatUI
{
    UIView *backgroundIMG = [UIView new];
    [backgroundIMG setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:backgroundIMG];
    [self.view insertSubview:self.navigationBar aboveSubview:backgroundIMG];
    
    [backgroundIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(0);
    }];

    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.tableFooterView = footerView;
    self.m_tableView.tableHeaderView = headerView;
    self.m_tableView.backgroundColor = XHClearColor;
    [self.view addSubview:self.m_tableView];
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
        make.bottom.mas_equalTo(kBottom(0));
    }];
    
    [self.view addSubview:self.closeButton];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.left.equalTo(self.view).offset(4);
        make.top.equalTo(self.view).offset(30);
        make.top.mas_equalTo(33);
    }];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.closeButton];
}

#pragma mark - table协议
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 71.5;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cellIdentifier"];
    cell.backgroundColor = XHClearColor;
    
    
    UIView *line = [YYCreateTools createView:HexRGB(0xF9F9F9)];
    [cell addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.bottom.mas_equalTo(-1);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(1.0);
    }];

    
    if (indexPath.row == 0) {
        [cell addSubview:self.telTextField];

    }else if (indexPath.row ==1)
    {
        [cell addSubview:self.pwdTextField];
        [cell addSubview:self.codeBtn];
        self.codeBtn.centerY = self.pwdTextField.centerY;
    }
    return cell;
}

-(void)vCodeClick
{
    [_pwdTextField becomeFirstResponder];
    if (self.telTextField.text.length != 11) {
        [YYCommonTools showTipMessage:@"请输入写正确的手机号"];
        return;
    }
    [self sendCodeRequest];
}


#pragma mark -Request
/**
 *  发送验证码
 */
- (void)sendCodeRequest
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.telTextField.text forKey:@"phoneNum"];

    YYReceiveCodeRequestAPI *sendCodeAPI = [[YYReceiveCodeRequestAPI alloc] initWithCodeRequest:dict];
    [sendCodeAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [YYCommonTools  sendCodeMethod:self.phone
                                                        codeBtn:self.codeBtn
                                                     phoneBlock:^(NSString *phone) {
                                                     }];
            [YYCommonTools  showTipMessage:@"验证码已发送"];
        }
    }failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}
/**
 *  登录请求
 */
- (void)userLoginRequest {
    self.telPhone = [NSString removeBlankAndLine:self.telTextField.text];
    self.telPhone = [self.telPhone stringByReplacingOccurrencesOfString:@"\\p{Cf}" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, self.telPhone.length)];
    self.smscode = [NSString removeBlankAndLine:self.pwdTextField.text];
    self.phone = self.telPhone;
    
    if (!kValidString(self.pwdTextField.text)) {
        [YYCommonTools showTipMessage:@"请输入验证码"];
        return;
    }
    
    [YYCenterLoading showCenterLoading];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.smscode forKey:@"smscode"];
    [dict setObject:self.wxOpenId?:@"" forKey:@"wxOpenId"];
    [dict setObject:self.wxUniqueId?:@"" forKey:@"wxUniqueId"];
    [dict setObject:self.phone forKey:@"phone"];
    [dict setObject:self.blackBox?:@"" forKey:@"blackBox"];

    LoginUserLoginInfoRequestAPI *loginAPI = [[LoginUserLoginInfoRequestAPI alloc] initWithLoginRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            int  blockStatus  = [[responDict objectForKey:@"blockStatus"] intValue];
            if (blockStatus == 3) {
                YY_SubmitInviteViewCtrl *vc = [YY_SubmitInviteViewCtrl new];
                vc.useToken = [responDict objectForKey:@"accessToken"];
                vc.wxlogin = self.wxUniqueId?YES:NO;
                vc.advimg = self.advimg;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (blockStatus == 2){
                NSDictionary *dic = [responDict objectForKey:@"promotionProvider"];
                [JCAlertView showTwoButtonsAndOneIMGWithTitle:@"推荐人确认" IMG:[dic objectForKey:@"avatarUrl"]  Message:[dic objectForKey:@"nickName"] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
                    [self.navigationController popViewControllerAnimated:YES];
                } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"确认并登录" Click:^{
                    [kWholeConfig bindPromotionRequset:[dic objectForKey:@"promotionCode"] token:[responDict objectForKey:@"accessToken"]];
                } type:JCAlertViewTypeOneHeadIMG];
                
            }else{
                NSString *accessToken = [responDict objectForKey:@"accessToken"];
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                [params setObject:@(2) forKey:@"source"];
                if (kValidString(accessToken)) {
                    [params setObject:accessToken forKey:@"useToken"];
                }
                [YYCommonTools loginWithToken:params];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - UITextField delegate method
- (void)textValueChanged {
    
    self.telPhone = self.telTextField.text;
    self.telPhone = [NSString removeBlankAndLine:self.telTextField.text];
    self.telPhone = [self.telPhone stringByReplacingOccurrencesOfString:@"\\p{Cf}" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, self.telPhone.length)];
    
    self.loginBtn.enabled = (self.telPhone.length >= 11 && self.pwdTextField.text.length != 0);
    self.codeBtn.enabled = (self.telPhone.length == 11);
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == 1) {
        if (textField.text.length +string.length >11) {
            return NO;
        }
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
