//
//  VideoCutter.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/15.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoCutter : NSObject
-(void)cropVideoWithUrl:(NSURL *)url andStartTime:(CGFloat)startTime andDuration:(CGFloat)duration andCompletion:(void(^)(NSURL * videoPath,NSError * error))task;

@end

NS_ASSUME_NONNULL_END
