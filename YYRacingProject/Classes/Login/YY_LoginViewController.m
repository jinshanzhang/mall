//
//  YY_LoginViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_LoginViewController.h"

#import "LoginUserLoginInfoRequestAPI.h"
#import "YYLoginImageRequestAPI.h"
#import "TouristLoginSetAPI.h"
#import "YY_PhoneLoginViewCtrl.h"
#import "YYWxAuthAppRequestAPI.h"
#import "YY_SubmitInviteViewCtrl.h"
#import "yyPhotoDownloadCacheManager.h"

#import <AVKit/AVKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "VideoCutter.h"


@interface YY_LoginViewController ()<UIAlertViewDelegate>

@property (nonatomic, strong) UIButton  *fanBtn;
@property (nonatomic, strong) UIButton  *memberBtn;


@property (nonatomic, strong) UIButton *WXlogin;
@property (nonatomic, strong) UIButton *PhoneLogin;
@property (nonatomic, strong) UIImageView *login_imageview;


@property (nonatomic, copy) NSString *wxOpenId;
@property (nonatomic, copy) NSString *wxUniqueId;
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString *blackBox;

@property (nonatomic, assign) int guestMode;
@property (nonatomic, strong) UIImage *adcIMG;


//@property (nonatomic, strong) AVPlayerViewController *moviePlayer;

@property (nonatomic, strong) UIImageView *coverIMG;

@end

@implementation YY_LoginViewController

-(void)viewDidDisappear:(BOOL)animated{
//    self.moviePlayer = nil;
//    [self.moviePlayer.player removeTimeObserver:self];
}

#pragma mark -Life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (![kUserDefault boolForKey:@"isContainPrivacy"]) {
        YYAlertView *alert = [[YYAlertView alloc] initWithFrame:CGRectMake(0, 0, kScreenW-kSizeScale(58), kSizeScale(317)) title:@"欢迎使用“蜀黍之家”APP，我们深知个人信息对您的重要性，并会尽全力保护您的个人信息安全可靠。我们致力于维持您对我们的信任，恪守以下原则，保护您的个人信息，详情查看《蜀黍之家用户隐私政策》。" subTitle:@"如同意此政策，请点击“同意”并开始使用蜀黍之家，我们承诺，我们将按业界成熟的安全标准，采取相应的安全保护措施来保护您的个人信息。" alertType:YYAlertPrivacyPolicyType otherBlock:^(NSInteger index) {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:@(3) forKey:@"linkType"];
            [params setObject:UserAgreementURL forKey:@"url"];
            [YYCommonTools skipMultiCombinePage:self
                                         params:params];
        } confirmBlock:^{
            [kUserDefault setBool:YES forKey:@"isContainPrivacy"];
        } cancelBlock:^{
            exit(0);
        }];
        [alert showAlertView];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self xh_navBackgroundColor:XHClearColor];
    [self xh_navBottomLine:XHClearColor];
    self.view.backgroundColor = XHWhiteColor;
    
//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//    /* 获取设备指纹黑盒数据，请确保在应用开启时已经对SDK进行初始化，切勿在get的时候才初始化
//     * 如果此处获取到的blackBox特别长(超过400字节)，说明初始化尚未完成(一般需要1-3秒)，或者由于网络问题导致初始化失败，进入了降级处理
//     * 降级不影响正常设备信息的获取，只是会造成blackBox字段超长，且无法获取设备真实IP
//     * 降级数据平均长度在2KB以内,一般不超过3KB,数据的长度取决于采集到的设备信息的长度,无法100%确定最大长度
//     */
//    self.blackBox = manager->getDeviceInfo();
//    YYLog(@"同盾设备指纹数据: %@", _blackBox);
    
    [self monitorNetwork];
    [self getVersionData];
    [self getAdvPic];
    [self creatView];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(InfoNotificationAction:) name:@"backLogin" object:nil];
    // Do any additional setup after loading the view.
    
    
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(removePlayerOnPlayerLayer)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];
    [center addObserver:self
               selector:@selector(resetPlayerToPlayerLayer)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];
}

- (void)removePlayerOnPlayerLayer {
//    [self.moviePlayer.player pause];
}

- (void)resetPlayerToPlayerLayer {
//    [self.moviePlayer.player play];
}

-(void)creatView{
//    self.moviePlayer = [[AVPlayerViewController alloc]init] ; //这个刚开始没写 一直没有视图，但是不一定放在这里是最好的。
//    NSString *videoPath = [[NSBundle mainBundle] pathForResource:@"video_ad.mp4" ofType:nil] ;
//    //视频地址
//    NSURL *videoUrl = [[NSURL alloc] initFileURLWithPath:videoPath] ;
//    self.moviePlayer.videoGravity = AVLayerVideoGravityResizeAspectFill; //设置播放器填充属性
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidReachEnd) name:AVPlayerItemDidPlayToEndTimeNotification object:self.moviePlayer.player.currentItem] ;
//    self.moviePlayer.view.frame = self.view.frame ;
//
//    self.moviePlayer.showsPlaybackControls = false ;
//    self.moviePlayer.view.alpha = 1;

    self.view.userInteractionEnabled = YES ; //关闭用户交互

//    [self.view addSubview:self.moviePlayer.view] ;
    
    self.coverIMG = [YYCreateTools createImageView:@"" viewModel:-1];
//    self.coverIMG.image =[YYCommonTools getVideoPreViewImage:videoUrl];
    self.coverIMG.contentMode = UIViewContentModeScaleAspectFill;
//    [self.moviePlayer.view addSubview:self.coverIMG];
    [self.view addSubview:self.coverIMG];
    
    [self.coverIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(self.view);
    }];
//    [self setFKCMoviePlayer:videoUrl];
    
//    UIView *alpha_view = [YYCreateTools createView:XHBlackColor];
//    alpha_view.alpha =0.5;
//    [self.view addSubview:alpha_view];
//
//    [alpha_view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.right.bottom.mas_equalTo(self.view);
//    }];
//    [self.view sendSubviewToBack:self.moviePlayer.view] ;
}

-(void)setFKCMoviePlayer:(NSURL *)url
{
//    @weakify(self);
//    VideoCutter * videoCutter = [VideoCutter new] ; //这是一个对视频进行一次处理的类，等下上代码
//    [videoCutter cropVideoWithUrl:url andStartTime:-1 andDuration:0 andCompletion:^(NSURL *videoPath, NSError *error) {
//        if ([videoPath isKindOfClass: [NSURL class]]) {
//            dispatch_queue_t globalDispatchQueueDefault = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            dispatch_async(globalDispatchQueueDefault, ^{
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    self.moviePlayer.player = [[AVPlayer alloc] initWithURL:videoPath] ;
//                    [self.moviePlayer.player play];
//                    [self.moviePlayer.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
//                        @strongify(self);
//                        //当前播放的时间
//                        NSTimeInterval interval = CMTimeGetSeconds(time);
////                        NSInteger current = round(interval);
//                        //输出当前播放的时间
//                        if ((int)interval >= 0.5) {
//                            [self.coverIMG removeFromSuperview];
//                        }
//                    }];
//                    self.moviePlayer.player.volume =1.0 ;
//                });
//            }) ;
//        }
//    }] ;
}

-(void)playerItemDidReachEnd
{
//    [self.moviePlayer.player seekToTime:kCMTimeZero] ;
//    [self.moviePlayer.player play] ;
}

- (void)InfoNotificationAction:(NSNotification *)notification
{
    [self monitorNetwork];
    [self getVersionData];
    [self getAdvPic];
}

/**
 *  广告图片
 */
- (void)getAdvPic{

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    YYLoginImageRequestAPI *loginAPI = [[YYLoginImageRequestAPI alloc] initRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            
             self.adcIMG = [kPhotoCacheConfig readImageSource:@"photo"
                                                      directory:@"login"];
            
            NSMutableArray *tempArray = [NSMutableArray array];
            [tempArray addObject:[responDict objectForKey:@"imageUrl"]];
            [kPhotoCacheConfig downloadPhotoSources:tempArray
                                          directory:@"login"
                                           iconName:@"photo"
                                        isAddSuffix:NO
                                          callBlock:^(BOOL isSuccess) {
                                              UIImage *image = [kPhotoCacheConfig readImageSource:@"photo"
                                                                                        directory:@"login"];
                                              if (image) {
                                                  self.adcIMG = image;
                                              }
                                          }];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

/**
 *  游客开关
 */
- (void)getVersionData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    TouristLoginSetAPI *loginAPI = [[TouristLoginSetAPI alloc] initWithRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            self.guestMode = [[responDict objectForKey:@"guestMode"] intValue];
            [self creatUI];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private method
-(void)creatUI {
    @weakify(self);

    self.PhoneLogin = [YYCreateTools createBtn:@"手机快速登录" font:boldFont(17) textColor:XHBlackColor];
    self.PhoneLogin.backgroundColor = XHWhiteColor;
    self.PhoneLogin.alpha = 0.8;
    self.PhoneLogin.v_cornerRadius = kSizeScale(4);
    [self.view addSubview:self.PhoneLogin];

    [self.PhoneLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.height.mas_equalTo(kSizeScale(46));
        make.bottom.mas_equalTo(-kBottom(60));
    }];
    
    self.PhoneLogin.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        YY_PhoneLoginViewCtrl *vc = [YY_PhoneLoginViewCtrl new];
        vc.advimg = self.adcIMG;
        [self.navigationController pushViewController:vc animated:YES];
    };
    
    self.WXlogin = [YYCreateTools createBtn:@"微信授权登录" font:boldFont(17) textColor:XHWhiteColor];
    self.WXlogin.backgroundColor = HexRGB(0x1cac1a);
    self.WXlogin.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.WXlogin];
    [self.WXlogin setTitleEdgeInsets:UIEdgeInsetsMake(0,-20, 0, 0)];
    [self.WXlogin setImage:[UIImage imageNamed:@"login_wx_icon"] forState:UIControlStateNormal];
    [self.WXlogin setImageEdgeInsets:UIEdgeInsetsMake(0, kSizeScale(-40), 0, 0)];

    self.WXlogin.v_cornerRadius = kSizeScale(4);
    [self.WXlogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.bottom.equalTo(self.PhoneLogin.mas_top).offset(-16);
        make.height.mas_equalTo(kSizeScale(46));
    }];
    
    UIButton *touristBtn = [YYCreateTools createBtn:@"游客登录" font:boldFont(17) textColor:XHWhiteColor];
    touristBtn.backgroundColor = HexRGB(0x1cac1a);
    [self.view addSubview:touristBtn];
    touristBtn.v_cornerRadius = kSizeScale(4);
    [touristBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30);
        make.bottom.equalTo(self.WXlogin.mas_top).offset(-20);
        make.height.mas_equalTo(kSizeScale(46));
    }];
    touristBtn.hidden = self.guestMode == 1?NO:YES;
    
    touristBtn.actionBlock = ^(UIButton *sender) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:@"1234" forKey:@"smscode"];
        [dict setObject:self.wxOpenId?:@"" forKey:@"wxOpenId"];
        [dict setObject:self.wxUniqueId?:@"" forKey:@"wxUniqueId"];
        [dict setObject:@"15068894038" forKey:@"phone"];
        [dict setObject:self.blackBox?:@"" forKey:@"blackBox"];
        
        LoginUserLoginInfoRequestAPI *loginAPI = [[LoginUserLoginInfoRequestAPI alloc] initWithLoginRequest:dict];
        [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [YYCenterLoading hideCenterLoading];
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
            NSString *accessToken = [responDict objectForKey:@"accessToken"];
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:@(2) forKey:@"source"];
            if (kValidString(accessToken)) {
                [params setObject:accessToken forKey:@"useToken"];
            }
            [YYCommonTools loginWithToken:params];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        }];
    };
    
    self.WXlogin.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
            if (!error) {
                UMSocialUserInfoResponse *resp = result;
                NSLog(@"%@",resp);
                self.wxUniqueId = resp.unionId;
                self.wxOpenId = resp.openid;
                self.userName = resp.name;
                self.iconUrl =resp.iconurl;
                self.gender = resp.gender;
                [self bindWX];
            }
            else {
                [YYCommonTools showTipMessage:@"跳转微信失败"];
            }
        }];
    };
    self.WXlogin.adjustsImageWhenHighlighted = NO;
    
    
    [UIView animateWithDuration:2 animations:^{
        self.WXlogin.alpha = 0.8;
        self.PhoneLogin.alpha = 0.8;
    }];
}

#pragma mark - Request
/**登录**/
- (void)loginRequest {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"" forKey:@"smscode"];
    [dict setObject:self.wxOpenId forKey:@"wxOpenId"];
    [dict setObject:self.wxUniqueId forKey:@"wxUniqueId"];
    [dict setObject:@"" forKey:@"phone"];
    [dict setObject:self.blackBox?:@"" forKey:@"blackBox"];
    
    LoginUserLoginInfoRequestAPI *loginAPI = [[LoginUserLoginInfoRequestAPI alloc] initWithLoginRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            int  blockStatus  = [[responDict objectForKey:@"blockStatus"] intValue];
            if (blockStatus == 0) {
                if ([responDict objectForKey:@"accessToken"]) {
                    NSString *accessToken = [responDict objectForKey:@"accessToken"];
                    NSMutableDictionary *params = [NSMutableDictionary dictionary];
                    [params setObject:@(2) forKey:@"source"];
                    if (kValidString(accessToken)) {
                        [params setObject:accessToken forKey:@"useToken"];
                    }
                    [YYCommonTools loginWithToken:params];
                }
            }else if (blockStatus == 1){
                YY_PhoneLoginViewCtrl *vc = [YY_PhoneLoginViewCtrl new];
                vc.wxUniqueId  = self.wxUniqueId;
                vc.wxOpenId = self.wxOpenId;
                vc.bindTitle =@"绑定手机号";
                [self.navigationController pushViewController:vc animated:YES];
            }else if (blockStatus == 2){
                NSDictionary *dic = [responDict objectForKey:@"promotionProvider"];
                [JCAlertView showTwoButtonsAndOneIMGWithTitle:@"推荐人确认" IMG:[dic objectForKey:@"avatarUrl"]  Message:[dic objectForKey:@"nickName"] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
                } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHLoginColor ButtonTitle:@"确认并登录" Click:^{
                    [kWholeConfig bindPromotionRequset:[dic objectForKey:@"promotionCode"]
                                                 token:[responDict objectForKey:@"accessToken"]];
                } type:JCAlertViewTypeOneHeadIMG];
            }else{
                YY_SubmitInviteViewCtrl *vc = [YY_SubmitInviteViewCtrl new];
                vc.useToken = [responDict objectForKey:@"accessToken"];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark - Request
/**绑定微信**/
-(void)bindWX {
    if (!self.wxUniqueId||!self.wxOpenId) {
        [YYCommonTools  showTipMessage:@"系统繁忙,请稍后再试."];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.wxOpenId forKey:@"openId"];
    [dict setObject:self.wxUniqueId forKey:@"unionId"];
    [dict setObject:self.userName forKey:@"userName"];
    [dict setObject:self.iconUrl forKey:@"avatarUrl"];
    [dict setObject:@"" forKey:@"country"];
    [dict setObject:@"" forKey:@"city"];
    [dict setObject:@"" forKey:@"province"];
    if ([self.gender isEqualToString:@"m"]) {
        self.gender = @"1";
    }else{
        self.gender = @"2";
    }
    [dict setObject:self.gender?:@"0" forKey:@"sex"];
    YYWxAuthAppRequestAPI *loginAPI = [[YYWxAuthAppRequestAPI alloc] initWithAuthAppRequest:dict];
    [YYCenterLoading showCenterLoading:YES];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self loginRequest];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
