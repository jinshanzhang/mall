//
//  YY_SubmitInviteViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_SubmitInviteViewCtrl : YYBaseViewController

@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *smscode;

@property (nonatomic, copy) NSString *wxOpenId;
@property (nonatomic, copy) NSString *wxUniqueId;

@property (nonatomic, copy) NSString *useToken;
@property (nonatomic, assign) BOOL  wxlogin;

@property (nonatomic, strong) UIImage *advimg;
@end
