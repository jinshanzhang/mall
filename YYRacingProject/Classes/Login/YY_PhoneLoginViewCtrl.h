//
//  YY_PhoneLoginViewCtrl.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_PhoneLoginViewCtrl : YYBaseViewController

@property (nonatomic, copy) NSString *wxOpenId;

@property (nonatomic, copy) NSString *wxUniqueId;

@property (nonatomic, strong) UIImage *advimg;

@property (nonatomic, copy) NSString *bindTitle;

@end
