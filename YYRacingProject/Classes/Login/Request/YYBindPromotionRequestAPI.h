//
//  YYBindPromotionRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYBindPromotionRequestAPI : YYBaseRequestAPI

- (instancetype)initWithBindCodeRequest:(NSMutableDictionary *)params;

@end
