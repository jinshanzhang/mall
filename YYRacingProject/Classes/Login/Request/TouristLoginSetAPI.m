//
//  TouristLoginSetAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "TouristLoginSetAPI.h"

@implementation TouristLoginSetAPI

- (instancetype)initWithRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOGINSET];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
