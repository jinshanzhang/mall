//
//  LoginUserQuitInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface LoginUserQuitInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initWithQuitLoginRequest:(NSMutableDictionary *)params;

@end
