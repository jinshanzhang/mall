//
//  YYReceiveCodeRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYReceiveCodeRequestAPI.h"

@implementation YYReceiveCodeRequestAPI

- (instancetype)initWithCodeRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOGINCODE];;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
