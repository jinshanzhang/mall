//
//  YYBindPromotionRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBindPromotionRequestAPI.h"
@implementation YYBindPromotionRequestAPI

- (instancetype)initWithBindCodeRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
        self.newtoken = [params objectForKey:@"token"];
        
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,BINDPROVIDER];;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    
    NSString  *x_platform = @"shellvip";
    NSString  *x_source = @"ios";
    NSString  *x_appversion = kAPPVersion;
    NSString  *x_imei = kDeviceUUID;
    NSString  *x_model = kSystemName;
    NSString  *x_osversion = kSystemVersions;
    NSString  *x_channel = @"ios";
    
    NSMutableDictionary *diction = [NSMutableDictionary dictionary];
    [diction setObject:self.newtoken forKey:@"x-token"];
    [diction setObject:x_platform forKey:@"x-platform"];
    [diction setObject:x_source forKey:@"x-source"];
    [diction setObject:x_appversion forKey:@"x-app-version"];
    [diction setObject:x_imei forKey:@"x-imei"];
    [diction setObject:x_model forKey:@"x-model"];
    [diction setObject:x_osversion forKey:@"x-os-version"];
    [diction setObject:x_channel forKey:@"x-channel"];
    [diction setObject:@"application/json" forKey:@"Content-Type"];
    [diction setObject:@"application/json" forKey:@"Accept"];
    [diction setObject:self.headTimestamp forKey:@"x-timestamp"];
    //2.1 加入x-sign
    NSString *tempSign = [NSString stringWithFormat:@"%@&%@&%@&%@&%@",self.newtoken, x_platform, x_source, self.headTimestamp, kEnvConfig.signKey];
    [diction setObject:[[CocoaSecurity md5:tempSign] hexLower] forKey:@"x-sign"];
    
    return diction;
}

@end
