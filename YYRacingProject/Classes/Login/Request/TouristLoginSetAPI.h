//
//  TouristLoginSetAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface TouristLoginSetAPI : YYBaseRequestAPI

- (instancetype)initWithRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
