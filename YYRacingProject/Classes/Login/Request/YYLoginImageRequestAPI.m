//
//  YYLoginImageRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/2.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYLoginImageRequestAPI.h"

@implementation YYLoginImageRequestAPI
- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOGINIMAGE];;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
