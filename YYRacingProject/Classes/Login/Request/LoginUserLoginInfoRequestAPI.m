//
//  LoginUserLoginInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "LoginUserLoginInfoRequestAPI.h"

@implementation LoginUserLoginInfoRequestAPI

- (instancetype)initWithLoginRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOGIN];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
