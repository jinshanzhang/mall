//
//  YYLoginImageRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/2.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"


@interface YYLoginImageRequestAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;


@end

