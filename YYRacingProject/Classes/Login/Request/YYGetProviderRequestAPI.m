//
//  YYGetProviderRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYGetProviderRequestAPI.h"

@implementation YYGetProviderRequestAPI

- (instancetype)initWithProviderRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *providerCode = nil;
    if (kValidDictionary(self.inParams)) {
        providerCode = [self.inParams objectForKey:@"promotionCode"];
        [self.inParams removeObjectForKey:@"promotionCode"];
    }
    
    return [NSString stringWithFormat:@"%@%@?promotionCode=%@",INTERFACE_PATH,GETPROVIDER,providerCode];;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
