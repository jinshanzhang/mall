//
//  LoginUserQuitInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "LoginUserQuitInfoRequestAPI.h"

@implementation LoginUserQuitInfoRequestAPI

- (instancetype)initWithQuitLoginRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOGOUT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
