//
//  LoginUserLoginInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface LoginUserLoginInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initWithLoginRequest:(NSMutableDictionary *)params;

@end
