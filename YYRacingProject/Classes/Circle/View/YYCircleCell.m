//
//  YYCircleCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircleCell.h"

@implementation YYCircleToolBarView

- (instancetype)initWithFrame:(CGRect)frame {
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kYYCellContentWidth;
        frame.size.height = kYYCellToolBarHeight;
    }
    self = [super initWithFrame:frame];
    self.exclusiveTouch = YES;
    
    _likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _likeButton.exclusiveTouch = YES;
    _likeButton.left = 0;
    _likeButton.size = CGSizeMake(kYYCellTooBarItemWidth, self.height);
    [_likeButton setBackgroundImage:[UIImage imageWithColor:XHLightColor] forState:UIControlStateHighlighted];
    
    _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _shareButton.exclusiveTouch = YES;
    _shareButton.left = _likeButton.right + 60;
    _shareButton.size = CGSizeMake(kYYCellTooBarItemWidth, self.height);
    [_shareButton setBackgroundImage:[UIImage imageWithColor:XHLightColor] forState:UIControlStateHighlighted];
    
    _downloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _downloadButton.exclusiveTouch = YES;
    _downloadButton.hidden = YES;
    _downloadButton.left = _likeButton.right + 60;
    _downloadButton.size = CGSizeMake(kYYCellTooBarItemWidth, self.height);
    [_downloadButton setBackgroundImage:[UIImage imageWithColor:XHLightColor] forState:UIControlStateHighlighted];
    
    _likeImageView = [[[UIImageView alloc] init] initWithImage:[UIImage imageNamed:@"circle_zan_default_icon"]];
    _likeImageView.top = (self.height - 14)/2.0;
    _likeImageView.size = CGSizeMake(14, 14);
    [_likeButton addSubview:_likeImageView];
    
    _shareImageView = [[[UIImageView alloc] init] initWithImage:[UIImage imageNamed:@"circle_share_icon"]];
    _shareImageView.top = (self.height - 14)/2.0;
    _shareImageView.size = CGSizeMake(14, 14);
    [_shareButton addSubview:_shareImageView];
    
    _downloadImageView = [[[UIImageView alloc] init] initWithImage:[UIImage imageNamed:@"video_download_mini_icon"]];
    _downloadImageView.top = (self.height - 14)/2.0;
    _downloadImageView.size = CGSizeMake(14, 14);
    [_downloadButton addSubview:_downloadImageView];
    
    _likeLabel = [YYLabel new];
    _likeLabel.userInteractionEnabled = NO;
    _likeLabel.height = self.height;
    _likeLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    _likeLabel.displaysAsynchronously = YES;
    _likeLabel.ignoreCommonProperties = YES;
    _likeLabel.fadeOnHighlight = NO;
    _likeLabel.fadeOnAsynchronouslyDisplay = NO;
    [_likeButton addSubview:_likeLabel];
    
    _shareLabel = [YYLabel new];
    _shareLabel.userInteractionEnabled = NO;
    _shareLabel.height = self.height;
    _shareLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    _shareLabel.displaysAsynchronously = YES;
    _shareLabel.ignoreCommonProperties = YES;
    _shareLabel.fadeOnHighlight = NO;
    _shareLabel.fadeOnAsynchronouslyDisplay = NO;
    [_shareButton addSubview:_shareLabel];
    
    _downloadLabel = [YYLabel new];
    _downloadLabel.userInteractionEnabled = NO;
    _downloadLabel.height = self.height;
    _downloadLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    _downloadLabel.displaysAsynchronously = YES;
    _downloadLabel.ignoreCommonProperties = YES;
    _downloadLabel.fadeOnHighlight = NO;
    _downloadLabel.fadeOnAsynchronouslyDisplay = NO;
    [_downloadButton addSubview:_downloadLabel];
    
    [self addSubview:_likeButton];
    [self addSubview:_shareButton];
    [self addSubview:_downloadButton];
    
    @weakify(self);
    [_likeButton addBlockForControlEvents:UIControlEventTouchUpInside block:^(id sender) {
        YYCircleCell *cell = weak_self.cell;
        if ([cell.delegate respondsToSelector:@selector(cellDidClickZan:)]) {
            [cell.delegate cellDidClickZan:cell];
        }
    }];
    
    [_shareButton addBlockForControlEvents:UIControlEventTouchUpInside block:^(id sender) {
        YYCircleCell *cell = weak_self.cell;
        if ([cell.delegate respondsToSelector:@selector(cellDidClickShare:)]) {
            [cell.delegate cellDidClickShare:cell];
        }
    }];
    
    [_downloadButton addBlockForControlEvents:UIControlEventTouchUpInside block:^(id sender) {
        YYCircleCell *cell = weak_self.cell;
        if ([cell.delegate respondsToSelector:@selector(cellDidClickDownload:)]) {
            [cell.delegate cellDidClickDownload:cell];
        }
    }];

    return self;
}

- (void)setLayout:(YYCircleLayout *)layout {
    
    _shareLabel.width = layout.toolbarShareTextWidth;
    _likeLabel.width = layout.toolbarLikeTextWidth;
    _downloadLabel.width = layout.toolbarDownloadTextWidth;
    
    _shareLabel.textLayout = layout.toolbarShareTextLayout;
    _likeLabel.textLayout = layout.toolbarLikeTextLayout;
    _downloadLabel.textLayout = layout.toolbarDownloadTextLayout;
    
    [self adjustImage:_likeImageView label:_likeLabel inButton:_likeButton];
    [self adjustImage:_shareImageView label:_shareLabel inButton:_shareButton];
    [self adjustImage:_downloadImageView label:_downloadLabel inButton:_downloadButton];
    
    self.shareButton.hidden = layout.isHideShare;
    self.downloadButton.hidden = !layout.isHideShare;
    
    _likeImageView.image = layout.circleModel.hasFavor ? [self likeImage] : [self unlikeImage];
}

#pragma mark - Private
- (void)adjustImage:(UIImageView *)image label:(YYLabel *)label inButton:(UIButton *)button {
    label.left = image.right + 6;
}

- (UIImage *)unlikeImage {
    return  [UIImage imageNamed:@"circle_zan_default_icon"];
}

- (UIImage *)likeImage {
    return  [UIImage imageNamed:@"circle_zan_selected_icon"];
}

@end


@implementation YYCircleLikeView

- (instancetype)initWithFrame:(CGRect)frame {
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kYYCellContentWidth;
        frame.size.height = 0;
    }
    self = [super initWithFrame:frame];
    
    self.exclusiveTouch = YES;
    self.backgroundColor = [UIColor clearColor];
    
    self.backgroundView = [[UIImageView alloc] init];
    UIImage *backImage = [UIImage imageNamed:@"all_zanHeader_icon"];
    backImage = [backImage resizableImageWithCapInsets:UIEdgeInsetsMake(5, 0, 0, 10) resizingMode:UIImageResizingModeStretch];
    self.backgroundView.width = kYYCellContentWidth;
    self.backgroundView.image = backImage;
    [self addSubview:self.backgroundView];
    
    self.scrollView = [[YYMultiItemScrollView alloc] initWithFrame:CGRectZero];
    self.scrollView.left = 10;
    self.scrollView.miniItemSpace = 5;
    self.scrollView.minimumLineSpacing = 5;
    self.scrollView.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.scrollView.contentBackgroundColor = HexRGB(0xF3F3F5);
    self.scrollView.width = kYYCellContentWidth - 20;
    self.scrollView.scrollEnabled = NO;
    [self addSubview:self.scrollView];
    
    return self;
}

- (void)setLayout:(YYCircleLayout *)layout {
    self.scrollView.itemSize = CGSizeMake(kSizeScale(30), kSizeScale(30));
    self.scrollView.miniItemSpace = self.scrollView.minimumLineSpacing = kSizeScale(5);
    
    if (layout.zanHeight <= 0) {
        self.backgroundView.hidden = self.scrollView.hidden = YES;
        return;
    }
    
    self.backgroundView.hidden = self.scrollView.hidden = NO;
    self.backgroundView.height = layout.zanHeight;
    self.scrollView.top = 13;
    self.scrollView.height = layout.zanHeight - 13;
}

@end

@interface YYCircleView ()
<YYMultiItemScrollViewDelegate>

@end

@implementation YYCircleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (frame.size.width == 0 && frame.size.height == 0) {
        frame.size.width = kScreenWidth;
        frame.size.height = 0;
    }
    self = [super initWithFrame:frame];

    self.exclusiveTouch = YES;
    self.backgroundColor = XHMainLightColor;
    
    // 容器
    _contentView = [UIView new];
    _contentView.top = kSizeScale(8);
    _contentView.left = kSizeScale(12);
    _contentView.width = kScreenWidth - kSizeScale(24);
    _contentView.height = 1;
    _contentView.v_cornerRadius = kSizeScale(5);
    _contentView.backgroundColor = XHWhiteColor;
    
    static UIImage *bottomLineBG;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        bottomLineBG = [UIImage imageWithSize:CGSizeMake(1, 3) drawBlock:^(CGContextRef context) {
            CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
            CGContextSetShadowWithColor(context, CGSizeMake(0, 0.4), 2, [UIColor colorWithWhite:0 alpha:0.08].CGColor);
            CGContextAddRect(context, CGRectMake(-2, -2, 4, 2));
            CGContextFillPath(context);
        }];
    });
    UIImageView *bottomLine = [[UIImageView alloc] initWithImage:bottomLineBG];
    bottomLine.width = _contentView.width;
    bottomLine.top = _contentView.height - 1;
    bottomLine.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [_contentView addSubview:bottomLine];
    [self addSubview:_contentView];
    
    // 头像
    _avatarView = [[UIImageView alloc] init];
    _avatarView.size = CGSizeMake(kYYCellProfileHeight, kYYCellProfileHeight);
    _avatarView.origin = CGPointMake(kYYCellLeftPaddingProfile, kYYCellTopPaddingProfile);
    [_contentView addSubview:_avatarView];

    // 昵称
    _nameLabel = [YYLabel new];
    _nameLabel.size = CGSizeMake(0, 20);
    _nameLabel.left = _avatarView.right + kYYCellPaddingItem;
    _nameLabel.top = _avatarView.top + 3;
    _nameLabel.displaysAsynchronously = YES;
    _nameLabel.ignoreCommonProperties = YES;
    _nameLabel.fadeOnAsynchronouslyDisplay = NO;
    _nameLabel.fadeOnHighlight = NO;
    _nameLabel.lineBreakMode = NSLineBreakByClipping;
    _nameLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    [_contentView addSubview:_nameLabel];
    
    _tagImageView = [[UIImageView alloc] initWithImage:bottomLineBG];
    _tagImageView.left = 0;
    _tagImageView.top = _avatarView.top + 3;
    [_contentView addSubview:_tagImageView];
    
    // 文本
    _textLabel = [YYLabel new];
    _textLabel.left = _nameLabel.left;
    _textLabel.top = _nameLabel.bottom + kYYCellTopPaddingText;
    _textLabel.width = kYYCellContentWidth;
    _textLabel.textVerticalAlignment = YYTextVerticalAlignmentTop;
    _textLabel.displaysAsynchronously = YES;
    _textLabel.ignoreCommonProperties = YES;
    _textLabel.fadeOnAsynchronouslyDisplay = NO;
    _textLabel.fadeOnHighlight = NO;
    [self.textLabel addGestureRecognizer:[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressCopyEvent:)]];

    [_contentView addSubview:_textLabel];
    
    //图片
    _gridView = [[YYMultiItemScrollView alloc] initWithFrame:CGRectZero];
    _gridView.left = _textLabel.left;
    _gridView.scrollEnabled = NO;
    _gridView.scrollDirection = UICollectionViewScrollDirectionVertical;
    _gridView.multiScrollDelegate = self;
    [_contentView addSubview:_gridView];
    
    //时间
    _createTimeLabel = [YYLabel new];
    _createTimeLabel.left = _nameLabel.left;
    _createTimeLabel.width = kYYCellContentWidth;
    _createTimeLabel.textVerticalAlignment = YYTextVerticalAlignmentTop;
    _createTimeLabel.displaysAsynchronously = YES;
    _createTimeLabel.ignoreCommonProperties = YES;
    _createTimeLabel.fadeOnAsynchronouslyDisplay = NO;
    _createTimeLabel.fadeOnHighlight = NO;
    [_contentView addSubview:_createTimeLabel];
    
    //工具栏
    _toolBarView = [[YYCircleToolBarView alloc] init];
    [_contentView addSubview:_toolBarView];
    
    //点赞
    _likeView = [YYCircleLikeView new];
    [_contentView addSubview:_likeView];
    
    return self;
}

- (void)setLayout:(YYCircleLayout *)layout {
    _layout = layout;
    
    __block NSString *tagUrl = nil;
    CGSize nameSize = CGSizeZero;
    NSInteger pictureCount = _layout.circleModel.pictureItems.count;
    
    self.height = layout.height;
    _contentView.height = self.height - _contentView.top;
    
    [_avatarView setImageURL:[NSURL URLWithString:_layout.circleModel.avatarUrl]];
    [_avatarView zy_cornerRadiusAdvance:3 rectCornerType:UIRectCornerAllCorners];

    nameSize = [YYCommonTools sizeWithText:_layout.circleModel.nickname
                                      font:[UIFont boldSystemFontOfSize:kYYCellNameFont] maxSize:CGSizeMake(MAXFLOAT, 20)];
    _nameLabel.width = nameSize.width;
    _nameLabel.textLayout = _layout.nickNameLayout;
    
    _tagImageView.left = _nameLabel.right + kSizeScale(5);
    if (_layout.circleModel.hasTag) {
        __block NSInteger tagType = _layout.circleModel.tagType;
        [_layout.circleModel.tagItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YYTagModel *model = (YYTagModel *)obj;
            if (model.type == tagType) {
                tagUrl = model.url;
                *stop = YES;
            }
        }];
        _tagImageView.size = CGSizeMake(kSizeScale(30), kSizeScale(16));
    }
    else {
        _tagImageView.size = CGSizeZero;
    }
    [self.tagImageView sd_setImageWithURL:[NSURL URLWithString:tagUrl] placeholderImage:[UIImage imageWithColor:XHLineColor]];
    
    _textLabel.height = layout.textHeight;
    _textLabel.textLayout = layout.textLayout;
    
    if (_layout.circleModel.pictureItems.count == 0 || _layout.pictureHeight <= 0) {
        _gridView.top = _gridView.height = _gridView.width = _layout.pictureHeight;
        return;
    }
    else {
        _gridView.top = _textLabel.bottom + kYYCellPaddingItem;
        _gridView.height = _layout.pictureHeight;
        if (pictureCount == 1) {
            _gridView.width = _layout.pictureSize.width;
            _gridView.miniItemSpace = _gridView.minimumLineSpacing = 0;
        }
        else {
            _gridView.miniItemSpace = _gridView.minimumLineSpacing = kYYCellPaddingPic;
            _gridView.width = [self _gridWidthWithItemCount:pictureCount
                                                  itemWidth:_layout.pictureSize.width];
        }
        _gridView.itemSize = _layout.pictureSize;
        _gridView.dataSource = [self girdPictureData];
    }
    
    _createTimeLabel.top = _gridView.bottom + kYYCellPaddingItem;
    _createTimeLabel.height = layout.createTimeHeight;
    _createTimeLabel.textLayout = layout.createTimeLayout;
    
    _toolBarView.top = _createTimeLabel.bottom;
    _toolBarView.left = _createTimeLabel.left;
    [_toolBarView setLayout:_layout];
    
    _likeView.top = _toolBarView.bottom;
    _likeView.left = _toolBarView.left;
    [_likeView setLayout:_layout];
    
    _likeView.scrollView.dataSource = [self likeData];
}

#pragma mark - Private
// 计算图片布局的宽度
- (CGFloat)_gridWidthWithItemCount:(NSInteger)itemCount
                      itemWidth:(CGFloat)itemWidth {
    NSInteger rowItemCount = 0;
    CGFloat totalWidth = 0.0;
    
    if (itemCount == 1) {
        rowItemCount = itemCount;
    }
    else if (itemCount == 2 || itemCount == 4) {
        rowItemCount = 2;
    }
    else {
        rowItemCount = 3;
    }
    totalWidth = (rowItemCount-1) * kYYCellPaddingPic + rowItemCount * itemWidth;
    return totalWidth;
}

- (NSMutableArray *)girdPictureData {
    if (_layout.circleModel.pictureItems.count <= 0) {
        return nil;
    }
    NSMutableArray *listData = [NSMutableArray array];
    for (int i = 0; i < _layout.circleModel.pictureItems.count; i ++) {
        YYPictureModel *itemModel = [_layout.circleModel.pictureItems objectAtIndex:i];
        YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] initWithPictureItem:itemModel];
        [listData addObject:model];
    }
    return listData;
}

- (NSMutableArray *)likeData {
    if (_layout.circleModel.favorUserItems.count <= 0) {
        return nil;
    }
    NSMutableArray *listData = [NSMutableArray array];
    for (int i = 0; i < _layout.circleModel.favorUserItems.count; i ++) {
        YYFavorUserModel *favorModel = [_layout.circleModel.favorUserItems objectAtIndex:i];
        YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] initWithFavorUserItem:favorModel];
        [listData addObject:model];
    }
    return listData;
}

#pragma mark - YYMultiItemScrollViewDelegate
- (void)multiScrollView:(YYMultiItemScrollView *)scrollView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_cell.delegate && [_cell.delegate respondsToSelector:@selector(photoItemsView:clickItemAtIndexPath:clickItemModel:)] && _layout.circleModel) {
        [_cell.delegate photoItemsView:[scrollView allItemOfMultiView] clickItemAtIndexPath:indexPath clickItemModel:_layout.circleModel];
    }
}

- (void)longPressCopyEvent:(UIGestureRecognizer *)gesture {
    [YYCommonTools pasteboardCopy:self.layout.circleModel.contents
                         appendParams:@{@"title":@"复制成功"}];
}
@end


@implementation YYCircleCell

#pragma mark - Life cycle
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundView.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        
        [self setSubViews];
    }
    return self;
}

/*- (void)prepareForReuse {
    // ignore
}*/

- (void)setLayout:(YYCircleLayout *)layout {
    self.height = layout.height;
    self.contentView.height = self.height;
    self.circleView.layout = layout;
}

#pragma mark - Private method
- (void)setSubViews {
    self.circleView = [YYCircleView new];
    self.circleView.cell = self;
    self.circleView.likeView.cell = self;
    self.circleView.toolBarView.cell = self;
    [self.contentView addSubview:self.circleView];
}



@end
