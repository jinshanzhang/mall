//
//  FriendCircelPhotosView.h
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FriendCirclePhotosDelegate;

@interface FriendCircelPhotosView : UIView

@property (nonatomic, strong) NSMutableArray *photoArrs;
@property (nonatomic, weak) id <FriendCirclePhotosDelegate> delegate;

@end


@protocol FriendCirclePhotosDelegate<NSObject>
@optional
// 图片点击
- (void)photoView:(FriendCircelPhotosView *)photoView  clickViews:(NSArray *)clickViews  didClickImageAtIndex:(NSUInteger)index;
@end
