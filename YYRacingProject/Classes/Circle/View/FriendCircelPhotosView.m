//
//  FriendCircelPhotosView.m
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircelPhotosView.h"
#import "YYImageInfoModel.h"

#define   kWBCellPaddingPic   5
#define   kPhotoItemTag       10

@interface CircelPhotoItemView : UIView

@property (nonatomic, strong) UIImageView *placeholderImageView;
@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic, strong) UIView      *priceView;
@property (nonatomic, strong) UILabel     *priceContentLabel;

@end


@implementation  CircelPhotoItemView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.photoImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    self.photoImageView.size = CGSizeMake(100, 100);
    self.photoImageView.clipsToBounds = YES;
    self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.photoImageView];
    
    self.placeholderImageView = [YYCreateTools createImageView:nil
                                                     viewModel:-1];
    [self.photoImageView addSubview:self.placeholderImageView];
    
    self.priceView = [UIView new];
    UIImage *bgImg = [UIImage gradientColorImageFromColors:@[HexRGB(0xFF3934), HexRGB(0xFE5C37)]
                                              gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(40, 16)];
    UIColor *bgcolor = [UIColor colorWithPatternImage:bgImg];
    self.priceView.backgroundColor = bgcolor;
    [self.photoImageView addSubview:self.priceView];
    
    self.priceContentLabel = [UILabel new];
    self.priceContentLabel.userInteractionEnabled = NO;
    self.priceContentLabel.textAlignment = NSTextAlignmentCenter;
    self.priceContentLabel.font = normalFont(12);
    self.priceContentLabel.backgroundColor = self.priceView.backgroundColor;
    self.priceContentLabel.textColor = XHLightColor;
    [self.priceView addSubview:self.priceContentLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.photoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.placeholderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.photoImageView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(49), kSizeScale(49)));
    }];
    
    [self.priceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self.photoImageView);
        make.height.mas_equalTo(16);
    }];
    
    [self.priceContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.priceView);
        make.width.greaterThanOrEqualTo(@(kSizeScale(34))).priorityHigh();
    }];
}

@end



@interface FriendCircelPhotosView()

@property (nonatomic, strong) NSMutableArray *picViews;

@end

@implementation FriendCircelPhotosView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.picViews = [NSMutableArray array];
        for (int i = 0; i < 9; i++) {
            CircelPhotoItemView *itemView = [[CircelPhotoItemView alloc] initWithFrame:CGRectZero];
            itemView.tag = i + kPhotoItemTag;
            [itemView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(photoItemClickGesture:)]];
            [self.picViews addObject:itemView];
            [self addSubview:itemView];
        }
    }
    return self;
}

- (void)setPhotoArrs:(NSMutableArray *)photoArrs {
    _photoArrs = photoArrs;
    CGFloat  height =  0.0;
    NSInteger count = _photoArrs.count;
    CGSize  picSize =  count >= 2 ? CGSizeMake(kSizeScale(78), kSizeScale(78)) : CGSizeZero;
    if (count == 2 || count == 4) {
        height = (int)(count / 2) * (picSize.height + kWBCellPaddingPic);
    }
    else {
        if (count != 1) {
            height =  (int)ceilf(count/3.0) * (picSize.height + kWBCellPaddingPic);
        }
        else {
            YYImageInfoModel *imageModel = [_photoArrs objectAtIndex:0];
            CGSize size = [self longPictureSizeTransform:CGSizeMake([imageModel.width floatValue]/2.0, [imageModel.height floatValue]/2.0)];
            if (imageModel.type == 4) {
                size = CGSizeMake(197, 197);
            }
            height = size.height;
        }
    }
    
    for (int i = 0; i < 9; i++) {
        YYImageInfoModel *imageModel;
        if (i < count) {
            imageModel = [_photoArrs objectAtIndex:i];
        }
        CircelPhotoItemView *imageView = self.picViews[i];
        imageView.placeholderImageView.image = (imageModel.type == 4?[UIImage imageNamed:@"video_pause_mini_icon"]:nil);
        if (i >= count) {
            imageView.alpha = 0;
        }
        else {
            imageView.alpha = 1;
            CGPoint origin = {0};
            if (count == 2 || count == 4) {
                origin.x = (i % 2) * (picSize.width + kWBCellPaddingPic);
                origin.y = (int)(i / 2) * (picSize.height + kWBCellPaddingPic);
            }
            else {
                if (count != 1) {
                    origin.x =  (i % 3) * (picSize.width + kWBCellPaddingPic);
                    origin.y =  (int)(i / 3) * (picSize.height + kWBCellPaddingPic);
                }
                else {
                    CGSize size = [self longPictureSizeTransform:CGSizeMake([imageModel.width floatValue]/2.0, [imageModel.height floatValue]/2.0)];
                    if (imageModel.type == 4) {
                        size = CGSizeMake(197, 197);
                    }
                    picSize = size;
                }
            }
            imageView.priceView.alpha = imageView.priceContentLabel.alpha = (imageModel.type != 1 ? 0 : 1);
            imageView.priceContentLabel.text = (imageModel.type != 1 ? @"" : [NSString stringWithFormat:@"¥%@", imageModel.price]);
            imageView.frame = (CGRect){.origin = origin, .size = picSize};
            [imageView.photoImageView sd_setImageWithURL:[NSURL URLWithString:imageModel.imageUrl]
                                        placeholderImage:[UIImage imageWithColor:HexRGB(0xf7f7f7)]];
        }
    }
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];
}

#pragma mark - Event
- (void)photoItemClickGesture:(UITapGestureRecognizer *)tapGesture {
    CircelPhotoItemView *itemView = (CircelPhotoItemView *)tapGesture.view;
    if (itemView) {
        if ([self.delegate respondsToSelector:@selector(photoView:clickViews:didClickImageAtIndex:)]) {
            [self.delegate photoView:self
                          clickViews:[self.picViews subarrayWithRange:NSMakeRange(0, self.photoArrs.count)] didClickImageAtIndex:itemView.tag-kPhotoItemTag];
        }
    }
}

/*- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        @weakify(self);
        self.picViews = [NSMutableArray array];
        for (int i = 0; i < 9; i++) {
            YYControl *imageView = [YYControl new];
            imageView.size = CGSizeMake(100, 100);
            imageView.hidden = YES;
            imageView.clipsToBounds = YES;
            imageView.exclusiveTouch = YES;
            
            imageView.touchBlock = ^(YYControl *view, YYGestureRecognizerState state, NSSet *touches, UIEvent *event) {
                @strongify(self);
                if (state == YYGestureRecognizerStateEnded) {
                    UITouch *touch = touches.anyObject;
                    CGPoint p = [touch locationInView:view];
                    if (CGRectContainsPoint(view.bounds, p)) {
                        if ([self.delegate respondsToSelector:@selector(photoView:clickViews:didClickImageAtIndex:)]) {
                            [self.delegate photoView:self clickViews:[self.picViews subarrayWithRange:NSMakeRange(0, self.photoArrs.count)] didClickImageAtIndex:i];
                        }
                    }
                }
            };
            UIView  *badegView = [UIView new];
            UIImage *bgImg = [UIImage gradientColorImageFromColors:@[HexRGB(0xFF3934), HexRGB(0xFE5C37)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(40, 16)];
            UIColor *bgcolor = [UIColor colorWithPatternImage:bgImg];
            badegView.backgroundColor = bgcolor;
            [imageView addSubview:badegView];
            
            UILabel *badge = [UILabel new];
            badge.userInteractionEnabled = NO;
            badge.textAlignment = NSTextAlignmentCenter;
            badge.font = normalFont(12);
            badge.backgroundColor = badegView.backgroundColor;
            badge.textColor = XHLightColor;
            [badegView addSubview:badge];
            
            [badegView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.bottom.equalTo(imageView);
                make.height.mas_equalTo(16);
            }];
            
            [badge mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(badegView);
                make.width.greaterThanOrEqualTo(@(kSizeScale(34))).priorityHigh();
            }];
            
            [self.picViews addObject:imageView];
            [self addSubview:imageView];
        }
    }
    return self;
}

- (void)setPhotoArrs:(NSMutableArray *)photoArrs {
    _photoArrs = photoArrs;
    CGFloat  height =  0.0;
    NSInteger count = _photoArrs.count;
    CGSize  picSize =  count >= 2 ? CGSizeMake(kSizeScale(78), kSizeScale(78)) : CGSizeZero;
    if (count == 2 || count == 4) {
        height = (int)(count / 2) * (picSize.height + kWBCellPaddingPic);
    }
    else {
        if (count != 1) {
            height =  (int)ceilf(count/3.0) * (picSize.height + kWBCellPaddingPic);
        }
        else {
            YYImageInfoModel *imageModel = [_photoArrs objectAtIndex:0];
            CGSize size = [self longPictureSizeTransform:CGSizeMake([imageModel.width floatValue]/2.0, [imageModel.height floatValue]/2.0)];
            height = size.height;
        }
    }
    for (int i = 0; i < 9; i++) {
        YYImageInfoModel *imageModel;
        if (i < count) {
            imageModel = [_photoArrs objectAtIndex:i];
        }
        UIView *imageView = self.picViews[i];
        @weakify(imageView);
        if (i >= count) {
            imageView.hidden = YES;
        }
        else {
            UIView *badgeView = imageView.subviews.firstObject;
            UILabel *badge = badgeView.subviews.firstObject;
            CGPoint origin = {0};
            if (count == 2 || count == 4) {
                origin.x = (i % 2) * (picSize.width + kWBCellPaddingPic);
                origin.y = (int)(i / 2) * (picSize.height + kWBCellPaddingPic);
            }
            else {
                if (count != 1) {
                    origin.x =  (i % 3) * (picSize.width + kWBCellPaddingPic);
                    origin.y =  (int)(i / 3) * (picSize.height + kWBCellPaddingPic);
                }
                else {
                    CGSize size = [self longPictureSizeTransform:CGSizeMake([imageModel.width floatValue]/2.0, [imageModel.height floatValue]/2.0)];
                    picSize = size;
                }
            }
            if (imageModel.type != 1) {
                badge.hidden = YES;
                badgeView.hidden = YES;
            }
            else {
                badge.hidden = NO;
                badgeView.hidden = NO;
                badge.text = [NSString stringWithFormat:@"¥%@", imageModel.price];
            }
            imageView.frame = (CGRect){.origin = origin, .size = picSize};
            imageView.hidden = NO;
            [imageView.layer removeAnimationForKey:@"contents"];
            [imageView.layer setImageWithURL:[NSURL URLWithString:imageModel.imageUrl]
                                 placeholder:[UIImage imageWithColor:HexRGB(0xf7f7f7)]
                                     options:YYWebImageOptionAvoidSetImage
                                  completion:^(UIImage *image, NSURL *url, YYWebImageFromType from, YYWebImageStage stage, NSError *error) {
                                      @strongify(imageView);
                                      if (!imageView) return;
                                      if (image && stage == YYWebImageStageFinished) {
                                      
                                          ((YYControl *)imageView).image = image;
                                          imageView.contentMode = UIViewContentModeScaleAspectFill;
                                          //CGFloat scale = imageView.height / imageView.width;
                                          if (scale > 0.99) {
                                              长图，截取中间部分
                                              imageView.contentMode = UIViewContentModeScaleAspectFill;
                                              imageView.layer.contentsRect = CGRectMake(0, 0, 1, 1);
                                          }
                                          int width = imageModel.width;
                                          int height = imageModel.height;
                                          CGFloat scale = (height / width) / (imageView.height / imageView.width);
                                          if (scale < 0.99 || isnan(scale)) {  宽图把左右两边裁掉
                                              imageView.contentMode = UIViewContentModeScaleAspectFill;
                                              imageView.layer.contentsRect = CGRectMake(0, 0, 1, 1);
                                          } else {  高图只保留顶部
                                              imageView.contentMode = UIViewContentModeScaleToFill;
                                              imageView.layer.contentsRect = CGRectMake(0, 0, 1, (float)width / height);
                                          }
                                          //imageView.layer.contentsRect = CGRectMake(0, 0, 1, 1);
                                          if (from != YYWebImageFromMemoryCacheFast) {
                                              CATransition *transition = [CATransition animation];
                                              transition.duration = 0.15;
                                              transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
                                              transition.type = kCATransitionFade;
                                              [imageView.layer addAnimation:transition forKey:@"contents"];
                                          }
                                      }
                                  }];
        }
    }
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height);
    }];
}*/

/**长图容器处理**/
- (CGSize)longPictureSizeTransform:(CGSize)originSize {
    //宽高比
    CGFloat  scale = 0.0;
    CGFloat  maxValue = kSizeScale(163);
    CGFloat  minValue = kSizeScale(55);
    CGFloat  originWidth = originSize.width;
    CGFloat  originHeight = originSize.height;
    CGFloat  originScale = originWidth/originHeight;
    
    if (originWidth <= 0 || originHeight <= 0) {
        return CGSizeZero;
    }
    
    if (originScale < minValue/maxValue) {
        //高同比例压缩，截取中间326
        scale = maxValue/originHeight;
        return CGSizeMake(originWidth*scale < minValue ? minValue : originWidth*scale , maxValue);
    }
    else if (originScale >= minValue/maxValue && originScale <= 1) {
        //高压缩到最大值，宽同比例压缩
        scale = maxValue/originHeight;
        return CGSizeMake(scale*originWidth > minValue ? scale*originWidth : minValue, maxValue);
    }
    else if (maxValue/minValue >= originScale && maxValue/minValue > 1) {
        //宽压缩到326， 高同比例压缩
        scale = maxValue/originWidth;
        return CGSizeMake(maxValue , originHeight*scale < minValue ? minValue : originHeight*scale);
    }
    else if (originScale > maxValue/minValue) {
        //宽同比例压缩, 截取中间326
        scale = maxValue/originWidth;
        return CGSizeMake(maxValue , originHeight*scale < minValue ? minValue : originHeight*scale);
    }
    return CGSizeZero;
}

@end
