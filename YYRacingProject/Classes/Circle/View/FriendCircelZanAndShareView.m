//
//  FriendCircelZanAndShareView.m
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircelZanAndShareView.h"

@interface FriendCircelZanAndShareView()

@property (nonatomic, strong) YYButton *zanBtn;
@property (nonatomic, strong) YYButton *shareBtn;
@property (nonatomic, strong) YYButton *downloadBtn;

@property (nonatomic, strong) NSMutableAttributedString *zanAttribute;
@property (nonatomic, strong) NSMutableAttributedString *shareAttribute;
@property (nonatomic, strong) NSMutableAttributedString *downloadAttribute;

@end

@implementation FriendCircelZanAndShareView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
        self.isShareBtnTransformDownloadBtn = NO;
    }
    return self;
}

- (void)layoutSubviews {
    CGFloat btnWidth = (self.width)/4.0;
    [self.zanBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
    }];
    
    [self.shareBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.right.mas_equalTo(-btnWidth);
    }];
    
    [self.downloadBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(btnWidth);
        make.right.mas_equalTo(-btnWidth);
    }];
}

- (void)createUI {
    @weakify(self);
    self.zanAttribute = [[NSMutableAttributedString alloc] initWithString:@"赞"];
    
    self.zanBtn = [[YYButton alloc] initWithFrame:CGRectZero];
    self.zanBtn.IMGspace = 10;
    self.zanBtn.isHorizontalLayout = YES;
    self.zanBtn.space = 0;
    self.zanBtn.normalImageName = @"circle_zan_default_icon";
    self.zanBtn.selectImageName = @"circle_zan_selected_icon";
    self.zanBtn.select = NO;

    self.zanBtn.normalAttibuteTitle = self.zanAttribute;
    self.zanBtn.selectAttibuteTitle = self.zanAttribute;
    
    [self addSubview:self.zanBtn];
    [self.zanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    self.shareBtn = [[YYButton alloc] initWithFrame:CGRectZero];
    self.shareBtn.IMGspace = 10;
    self.shareBtn.isHorizontalLayout = YES;
    self.shareBtn.space = 0;
    self.shareBtn.normalImageName = @"circle_share_icon";
    self.shareBtn.selectImageName = @"circle_share_icon";
    self.shareBtn.select = NO;
    
    self.shareAttribute = [[NSMutableAttributedString alloc] initWithString:@"分享"];
    self.shareBtn.normalAttibuteTitle = self.shareAttribute;
    
    [self addSubview:self.shareBtn];
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    self.downloadBtn = [[YYButton alloc] initWithFrame:CGRectZero];
    self.downloadBtn.IMGspace = 10;
    self.downloadBtn.isHorizontalLayout = YES;
    self.downloadBtn.space = 0;
    self.downloadBtn.normalImageName = @"video_download_mini_icon";
    self.downloadBtn.selectImageName = @"video_download_mini_icon";
    self.downloadBtn.select = NO;
    self.downloadBtn.hidden = YES;
    
    self.downloadAttribute = [[NSMutableAttributedString alloc] initWithString:@"一键下载"];
    self.downloadBtn.normalAttibuteTitle = self.downloadAttribute;
    
    [self addSubview:self.downloadBtn];
    [self.downloadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.height.mas_equalTo(20);
    }];
    
    self.zanBtn.btnClickBlock = ^(BOOL status) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(zanClickEvent:)]) {
            [self.delegate zanClickEvent:status];
        }
    };
    
    self.shareBtn.btnClickBlock = ^(BOOL status) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(shareClickEvent)]) {
            [self.delegate shareClickEvent];
        }
    };
    
    self.downloadBtn.btnClickBlock = ^(BOOL status) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(downloadClickEvent)]) {
            [self.delegate downloadClickEvent];
        }
    };
}

- (void)setZanStatus:(BOOL)zanStatus {
    _zanStatus = zanStatus;
    self.zanBtn.select = _zanStatus;
}

- (void)setIsShareBtnTransformDownloadBtn:(BOOL)isShareBtnTransformDownloadBtn {
    _isShareBtnTransformDownloadBtn = isShareBtnTransformDownloadBtn;
    if (_isShareBtnTransformDownloadBtn) {
        self.shareBtn.hidden = !(self.downloadBtn.hidden = NO);
    }
    else {
        self.shareBtn.hidden = !(self.downloadBtn.hidden = YES);
    }
}

- (void)setZanCount:(NSInteger)zanCount {
    _zanCount = zanCount;
    self.zanAttribute = [[NSMutableAttributedString alloc] initWithString:@"赞"];
    NSMutableAttributedString *count = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_zanCount<=0?@"":@(_zanCount)]];
    
    [count yy_setAttributes:XHBlackLitColor
                       font:midFont(11)
                    content:count
                  alignment:NSTextAlignmentLeft];
    
    [self.zanAttribute appendAttributedString:count];
    self.zanBtn.normalAttibuteTitle = self.zanAttribute;
    self.zanBtn.selectAttibuteTitle = self.zanAttribute;
}

- (void)setShareCount:(NSInteger)shareCount {
    _shareCount = shareCount;
    self.shareAttribute = [[NSMutableAttributedString alloc] initWithString:@"分享"];
    NSMutableAttributedString *count = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_shareCount<=0?@"":@(_shareCount)]];
    [count yy_setAttributes:XHBlackLitColor
                       font:midFont(11)
                    content:count
                  alignment:NSTextAlignmentLeft];
    [self.shareAttribute appendAttributedString:count];
    self.shareBtn.normalAttibuteTitle = self.shareAttribute;
}

@end
