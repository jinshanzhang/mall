//
//  FriendCircelZanAndShareView.h
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FriendZanAndShareDelegate<NSObject>

@optional
- (void)zanClickEvent:(BOOL)status;
- (void)shareClickEvent;
- (void)downloadClickEvent;
@end

@interface FriendCircelZanAndShareView : UIView

@property (nonatomic, assign) BOOL   zanStatus;
@property (nonatomic, assign) BOOL   isShareBtnTransformDownloadBtn;
@property (nonatomic, assign) NSInteger zanCount;
@property (nonatomic, assign) NSInteger shareCount;

@property (nonatomic, weak) id <FriendZanAndShareDelegate> delegate;

@end
