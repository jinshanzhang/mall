//
//  YYCircleTopicCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircleTopicCell.h"

#import "YYMultiItemScrollModel.h"
@interface YYCircleTopicCell ()

@property (nonatomic, strong) UIImageView  *topicImageView;

@end

@implementation YYCircleTopicCell

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    [self.topicImageView yy_sdWebImage:viewModel.imageUrl placeholderImageType:YYPlaceholderImageHomeBannerType];
}

- (void)createUI {
    self.topicImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    self.topicImageView.clipsToBounds = YES;
    self.topicImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topicImageView.image = [UIImage imageNamed:@"good_detail_default_icon"];
    
    [self.contentView addSubview:self.topicImageView];
    
    [self.topicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

@end
