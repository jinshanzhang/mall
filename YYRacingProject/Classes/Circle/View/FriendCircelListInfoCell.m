//
//  FriendCircelListInfoCell.m
//  YIYanProject
//
//  Created by cjm on 2018/6/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircelListInfoCell.h"

#import "HomeTagsInfoModel.h"

#import "YYanYYLabel.h"
#import "FriendCircelZansView.h"
#import "FriendCircelPhotosView.h"
#import "FriendCircelZanAndShareView.h"

#import "CYLTabBarController.h"
@interface FriendCircelListInfoCell()
<
FriendZanAndShareDelegate,
FriendCirclePhotosDelegate>

@property (nonatomic, strong) UIImageView *userHeader;
@property (nonatomic, strong) UILabel     *userName;
@property (nonatomic, strong) UILabel *publicContent;
@property (nonatomic, strong) UIImageView *tagImageView;

@property (nonatomic, strong) UILabel     *publicTime;
@property (nonatomic, strong) UIButton    *foldBtn;

@property (nonatomic, strong) FriendCircelZansView   *zanView;
@property (nonatomic, strong) FriendCircelPhotosView *photoView;
@property (nonatomic, strong) FriendCircelZanAndShareView *operatorView;  //赞和分享视图

@property (nonatomic, strong) UIView      *lineView;

@property (nonatomic, strong) MASConstraint *foldButtonHeight;
@property (nonatomic, strong) MASConstraint *contentHeight;
@property (nonatomic, assign) NSInteger   currentIndex;

@end

@implementation FriendCircelListInfoCell

#pragma mark - Life cycle
- (void)createUI {
    self.currentIndex = 0;
    // 头像
    self.userHeader = [YYCreateTools createImageView:nil
                                           viewModel:-1];
    [self.contentView addSubview:self.userHeader];
    
    [self.userHeader mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(15));
        make.top.mas_equalTo(kSizeScale(15));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(40)));
    }];
    
    // 用户名
    self.userName = [YYCreateTools createLabel:nil
                                          font:boldFont(15)
                                     textColor:HexRGB(0x495C85)];
    [self.contentView addSubview:self.userName];
    
    [self.userName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userHeader);
        make.left.equalTo(self.userHeader.mas_right).offset(kSizeScale(10));
    }];
    
    //标签
    self.tagImageView = [YYCreateTools createImageView:nil
                                             viewModel:-1];
    [self.contentView addSubview:self.tagImageView];
    
    [self.tagImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userName.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.userName);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(30), kSizeScale(16)));
    }];
    
    // 文本内容
    self.publicContent = [YYCreateTools createLabel:nil
                                               font:midFont(14)
                                          textColor:XHBlackColor];
    self.publicContent.numberOfLines = 0;
    self.publicContent.preferredMaxLayoutWidth = kScreenW - kSizeScale(85);
    
    /*
    // 下面写法，在iOS 10.0 以前会导致LineHeight错乱。
    self.publicContent = [YYanYYLabel new];
    self.publicContent.textColor = XHBlackColor;
    self.publicContent.font = midFont(14);
    self.publicContent.numberOfLines = 0;
    //self.publicContent.lineBreakMode = NSLineBreakByCharWrapping;
    self.publicContent.displaysAsynchronously = YES;
    self.publicContent.preferredMaxLayoutWidth = kScreenW;
    self.publicContent.textVerticalAlignment = YYTextVerticalAlignmentTop;
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 10.1f) {
        YYTextLinePositionSimpleModifier *modifiers = [YYTextLinePositionSimpleModifier new];
        modifiers.fixedLineHeight = kSizeScale(18);
        self.publicContent.linePositionModifier = modifiers;
    }*/
    [self.contentView addSubview:self.publicContent];
    
    [self.publicContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userName.mas_bottom).offset(kSizeScale(5));
        make.left.equalTo(self.userName);
        make.right.mas_equalTo(-kSizeScale(20));
        self.contentHeight = make.height.mas_equalTo(kSizeScale(18*6));
    }];
    
    // 是否显示全文
    self.foldBtn = [YYCreateTools createBtn:@"全文"
                                       font:midFont(14)
                                  textColor:HexRGB(0x495C85)];
    self.foldBtn.selected = NO;
    self.foldBtn.hidden = YES;
    [self.foldBtn setTitle:@"全文" forState:UIControlStateNormal];
    [self.foldBtn setTitle:@"收起" forState:UIControlStateSelected];
    [self.contentView addSubview:self.foldBtn];
    [self.foldBtn addTarget:self action:@selector(foldBtnClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.foldBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.publicContent.mas_left).offset(-5);
        make.top.equalTo(self.publicContent.mas_bottom).offset(3);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(40), 23)).priorityHigh();
        self.foldButtonHeight = make.height.mas_equalTo(0);
    }];
    
    // 内容图片
    self.photoView = [[FriendCircelPhotosView alloc] initWithFrame:CGRectZero];
    self.photoView.delegate = self;
    [self.contentView addSubview:self.photoView];
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.publicContent);
        make.top.equalTo(self.foldBtn.mas_bottom).offset(kSizeScale(5)).priorityHigh();
    }];
    
    // 发布时间
    self.publicTime = [YYCreateTools createLabel:nil
                                            font:midFont(11)
                                       textColor:XHBlackMidColor];
    [self.contentView addSubview:self.publicTime];
    
    [self.publicTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.publicContent);
        make.top.equalTo(self.photoView.mas_bottom).offset(kSizeScale(5)).priorityHigh();
    }];
    
    // 点赞和分享操作
    self.operatorView = [[FriendCircelZanAndShareView alloc] init];
    self.operatorView.delegate = self;
    [self.contentView addSubview:self.operatorView];
    [self.operatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(68));
        make.right.equalTo(self.contentView);
        make.height.mas_equalTo(30);
        make.top.equalTo(self.publicTime.mas_bottom).offset(5).priorityHigh();
    }];
    
    // 赞的头像
    self.zanView = [[FriendCircelZansView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.zanView];
    
    [self.zanView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.publicContent);
        make.top.equalTo(self.operatorView.mas_bottom).offset(0).priorityHigh();
    }];
    
    // 底线
    self.lineView = [YYCreateTools createView:HexRGB(0xEFEFEF)];
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.zanView.mas_bottom).offset(10).priorityHigh();
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)setListModel:(FriendCircleListInfoModel *)listModel {
    _listModel = listModel;
    if (_listModel) {
        [self.userHeader yy_sdWebImage:_listModel.avatarUrl
                  placeholderImageType:YYPlaceholderImageHomeBannerType];
        self.userName.text = _listModel.nickname;
        
        //遍历获取标签
        __block NSString *tagUrl;
        if (_listModel.hasTag) {
            self.tagImageView.hidden = NO;
            __block NSInteger tagType = _listModel.tagType;
            [_listModel.tags enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeTagsInfoModel *model = (HomeTagsInfoModel *)obj;
                if (model.tagType == tagType) {
                    tagUrl = model.tagUrl;
                    *stop = YES;
                }
            }];
            [self.tagImageView sd_setImageWithURL:[NSURL URLWithString:tagUrl]
                                 placeholderImage:[UIImage imageWithColor:XHWhiteColor]];
        }
        else {
            self.tagImageView.hidden = YES;
        }
        if (kValidString(_listModel.contents)) {
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_listModel.contents];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:2]; //调整行间距
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_listModel.contents length])];
            self.publicContent.attributedText = attributedString;
        }
        self.publicTime.text = _listModel.createTime;
        
        [self.contentHeight uninstall];
        [self.foldButtonHeight install];
        
        /*CGFloat textHeight = [YYCommonTools sizeWithText:_listModel.contents
                                                                    font:midFont(14)
                                                                       maxSize:CGSizeMake(kScreenW-kSizeScale(78), MAXFLOAT)].height;
        if (textHeight > kSizeScale(18*6)) {
            //显示全文按钮
            if (!_listModel.isFold) {
                [self.contentHeight install];
            }
            else {
                [self.contentHeight uninstall];
            }
            [self.foldButtonHeight uninstall];
            self.foldBtn.hidden = NO;
            self.foldBtn.selected = _listModel.isFold;
        }
        else {
            //不显示全文按钮
            [self.contentHeight uninstall];
            [self.foldButtonHeight install];
            self.foldBtn.hidden = YES;
        }*/
        
        self.photoView.photoArrs = _listModel.imageList;
        
        self.zanView.zanArrs = _listModel.zanList;
        
        self.operatorView.isShareBtnTransformDownloadBtn = (_listModel.viewType == 4?YES:NO);
        self.operatorView.zanStatus = _listModel.hasFavor;
        self.operatorView.zanCount = _listModel.favorCnt;
        self.operatorView.shareCount = _listModel.shareCnt;
    }
}
#pragma mark - Event
- (void)foldBtnClickEvent:(UIButton *)sender {
    self.foldBtn.selected = !self.foldBtn.selected;
    self.listModel.isFold = !self.listModel.isFold;
    if (self.foldAndShowAllClickBlock) {
        self.foldAndShowAllClickBlock(self, self.listModel);
    }
}

#pragma mark - FriendZanAndShareDelegate

- (void)zanClickEvent:(BOOL)status {
    if (self.zanBlock) {
        self.zanBlock(self, self.listModel);
    }
}

- (void)shareClickEvent {
    if (self.shareBlock) {
        self.shareBlock(self);
    }
}

- (void)downloadClickEvent {
    if (self.downloadBlock) {
        self.downloadBlock(self);
    }
}

#pragma mark - FriendCirclePhotosDelegate
- (void)photoView:(FriendCircelPhotosView *)photoView clickViews:(NSArray *)clickViews didClickImageAtIndex:(NSUInteger)index {
    BOOL isSkipAlbum = YES;
    self.currentIndex = index;
    YYImageInfoModel *videoModel = nil;
    NSMutableArray *items = [NSMutableArray array];
    for (NSUInteger i = 0; i < self.listModel.imageList.count; i++) {
        YYImageInfoModel *model = self.listModel.imageList[i];
        if (model.type == 4) {// 是否是视频
            isSkipAlbum = NO;
            videoModel = model;
        }
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = clickViews[i];
        item.imageModel = model;
        item.isAlreadyReplace = NO;
        item.largeImageURL = [NSURL URLWithString:model.bigImageUrl];
        [items addObject:item];
    }
    
    if (isSkipAlbum) {
        YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:self.currentIndex];
        [v presentFromImageView:clickViews[index] toContainer:kAppWindow animated:YES completion:nil];
        v.skipGoodBlock = ^(YYImageInfoModel *infoModel) {
            if (self.skipGoodDetailBlock) {
                self.skipGoodDetailBlock(infoModel);
            }
        };
        v.hideViewBlock = ^(NSInteger currentPage) {
            self.currentIndex = currentPage;
        };
    }
    else {
        if (videoModel) {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if (kValidString(videoModel.videoUrl)) {
                [params setObject:videoModel.videoUrl forKey:@"videoURL"];
            }
            if (kValidString(videoModel.imageUrl)) {
                [params setObject:videoModel.imageUrl forKey:@"placeholdImageURL"];
            }
            [YYCommonTools skipVideoPlayPage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
        }
    }
}
@end
