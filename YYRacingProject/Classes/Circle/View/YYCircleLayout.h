//
//  YYCircleLayout.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YYCircleModel.h"

NS_ASSUME_NONNULL_BEGIN

#define kYYCellPaddingPic   kSizeScale(5)                     // 图片之前的间距
#define kYYCellProfileHeight   kSizeScale(39)                 // 头像的高度
#define kYYCellPaddingItem   kSizeScale(10)                   // 元素间的间距
#define kYYCellTopPaddingText kSizeScale(4)                   // 文本顶部间隔
#define kYYCellToolBarHeight   kSizeScale(30)                 // 工具栏高度
#define kYYCellTooBarItemWidth kSizeScale(80)                 // 工具栏item宽度
#define kYYCellTopPaddingProfile   kSizeScale(16)             // 名片距离cell顶部的间距
#define kYYCellLeftPaddingProfile   kSizeScale(18)            // 名片距离cell左边的间距
#define kYYCellLeftPaddingContent   kSizeScale(67)            // 内容左边的间距
#define kYYCellRightPaddingContent   kSizeScale(26)           // 内容右边的间距

#define kYYCellContentWidth   kScreenW - (kSizeScale(12)*2 + kSizeScale(65) + kSizeScale(19)) // 文本内容的宽度

#define kYYCellNameFont   15
#define kYYCellNameTextColor   HexRGB(0x495C85)

#define kYYCellTextFont   14
#define kYYCellTextColor   HexRGB(0x666666)

#define KYYCellPriceFont   11
#define KYYCellPriceColor   HexRGB(0xfafafa)

@interface YYCircleLayout : NSObject

- (instancetype)initWithCircleModel:(YYCircleModel *)circleModel;
- (void)layout;


// 数据
@property (nonatomic, strong) YYCircleModel *circleModel;


// 布局
@property (nonatomic, assign) CGFloat marginTop;   //顶部留白

@property (nonatomic, assign) CGFloat avatarHeight; // 头像高度

@property (nonatomic, assign) CGFloat nickNameHeight;
@property (nonatomic, strong) YYTextLayout *nickNameLayout;   //昵称

@property (nonatomic, assign) CGFloat textHeight;
@property (nonatomic, strong) YYTextLayout *textLayout;   //正文

@property (nonatomic, assign) CGFloat pictureHeight;
@property (nonatomic, assign) CGSize  pictureSize;   //图片

@property (nonatomic, assign) CGFloat createTimeHeight;
@property (nonatomic, strong) YYTextLayout *createTimeLayout; //日期

@property (nonatomic, assign) CGFloat toolBarHeight; //工具栏
@property (nonatomic, strong) YYTextLayout *toolbarLikeTextLayout;
@property (nonatomic, strong) YYTextLayout *toolbarShareTextLayout;
@property (nonatomic, strong) YYTextLayout *toolbarDownloadTextLayout;

@property (nonatomic, assign) CGFloat toolbarLikeTextWidth;
@property (nonatomic, assign) CGFloat toolbarShareTextWidth;
@property (nonatomic, assign) CGFloat toolbarDownloadTextWidth;

@property (nonatomic, assign) BOOL  isHideShare;

@property (nonatomic, assign) CGFloat zanHeight; //赞

@property (nonatomic, assign) CGFloat marginBottom;   //底部留白

@property (nonatomic, assign) CGFloat height;

@end


@interface YYCTextLinePositionModifier : NSObject <YYTextLinePositionModifier>

@property (nonatomic, strong) UIFont *font; // 基准字体 (例如 Heiti SC/PingFang SC)
@property (nonatomic, assign) CGFloat paddingTop; //文本顶部留白
@property (nonatomic, assign) CGFloat paddingBottom; //文本底部留白
@property (nonatomic, assign) CGFloat lineHeightMultiple; //行距倍数
- (CGFloat)heightForLineCount:(NSUInteger)lineCount;

@end


NS_ASSUME_NONNULL_END
