//
//  FriendTopicCollectionInfoCell.m
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendTopicCollectionInfoCell.h"

@interface FriendTopicCollectionInfoCell()

@property (nonatomic, strong) UIImageView  *topicImageView;
@property (nonatomic, strong) UIView       *topicBackgroundView;
@property (nonatomic, strong) UILabel      *topicContent;

@end

@implementation FriendTopicCollectionInfoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    // 话题背景图片
    self.topicImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    self.topicImageView.clipsToBounds = YES;
    self.topicImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topicImageView.image = [UIImage imageNamed:@"good_detail_default_icon"];

    [self addSubview:self.topicImageView];
    
    [self.topicImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];

    // 话题蒙层
    /*self.topicBackgroundView = [YYCreateTools createView:XHBlackColor2];
    self.topicBackgroundView.alpha = .3;
    self.topicBackgroundView.layer.cornerRadius = 3;
    [self addSubview:self.topicBackgroundView];
    
    [self.topicBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    // 话题内容
    self.topicContent = [YYCreateTools createLabel:@"#世界杯大爆料爆料"
                                              font:normalFont(13)
                                         textColor:XHLightColor];
    self.topicContent.textAlignment = NSTextAlignmentCenter;
    self.topicContent.preferredMaxLayoutWidth = kSizeScale(80);
    self.topicContent.numberOfLines = 0;
    [self addSubview:self.topicContent];
    
    [self.topicContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];*/
}

- (void)setTopicURLStr:(NSString *)topicURLStr {
    _topicURLStr = topicURLStr;
    [self.topicImageView yy_sdWebImage:_topicURLStr placeholderImageType:YYPlaceholderImageHomeBannerType];
}

- (void)setTopicTitle:(NSString *)topicTitle {
    _topicTitle = topicTitle;
    //self.topicContent.text = _topicTitle;
}

@end
