//
//  YYCircelMenuBarItem.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircelMenuBarItem.h"

@interface YYCircelMenuBarItem()

@end

@implementation YYCircelMenuBarItem

#pragma mark - Setter
- (void)setItemSelect:(BOOL)itemSelect {
    [super setItemSelect:itemSelect];
    UIColor *textColor = nil, *backgroundColor = nil;
    if (itemSelect) {
        textColor = XHWhiteColor;
        backgroundColor = XHRedColor;
    }
    else {
        textColor = XHBlack4Color;
        backgroundColor = XHStoreGrayColor;
    }
    self.titleLabel.textColor = textColor;
    self.titleLabel.backgroundColor = backgroundColor;
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [self initWithFrame:CGRectZero];
    if (self) { }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemSelect = NO;
        self.backgroundColor = XHWhiteColor;
        [self _setSubViews];
        [self _setSubLayouts];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self _setSubLayouts];
}

#pragma mark - Private
- (void)_setSubViews {
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.v_cornerRadius = kSizeScale(3);
    self.titleLabel.textColor = XHBlackLitColor;
    self.titleLabel.font = normalFont(13);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = XHStoreGrayColor;
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.mas_equalTo(0);
        make.height.mas_equalTo(kSizeScale(25));
    }];
}

- (void)_setSubLayouts {
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(self.width);
    }];
}

@end
