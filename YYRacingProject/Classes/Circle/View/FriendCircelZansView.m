//
//  FriendCircelZansView.m
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircelZansView.h"

#import "FriendCircelZanCell.h"
#import "FriendCircleListInfoModel.h"

@interface FriendCircelZansView()
<UICollectionViewDelegate,
UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray   *listData;

@property (nonatomic, strong) UIImageView      *backgroundView;
@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation FriendCircelZansView


#pragma mark - Setter method

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 5;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.minimumInteritemSpacing = 5;
        flowLayout.itemSize = CGSizeMake(kSizeScale(32), kSizeScale(32));
    
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = HexRGB(0xf3f3f5);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

#pragma mark - Life cycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    // 背景色
    self.backgroundView = [[UIImageView alloc] init];
    UIImage *backImage = [UIImage imageNamed:@"all_zanHeader_icon"];
    backImage = [backImage resizableImageWithCapInsets:UIEdgeInsetsMake(5, 0, 0, 10) resizingMode:UIImageResizingModeStretch];
    self.backgroundView.image = backImage;
    [self addSubview:self.backgroundView];

    // 用户赞
    Class currentCls = [FriendCircelZanCell class];
    [self.collectionView registerClass:currentCls forCellWithReuseIdentifier:strFromCls(currentCls)];
    [self.backgroundView addSubview:self.collectionView];
    
    // 布局
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backgroundView.mas_top).offset(13);
        make.bottom.equalTo(self.backgroundView).priorityHigh();
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
    }];
}

- (void)setZanArrs:(NSMutableArray *)zanArrs {
    
    CGFloat  viewHeight= 0.01;
    NSInteger sectionCount = 0;
    
    _zanArrs = zanArrs;
    self.listData = _zanArrs;
    if (kValidArray(self.listData)) {
        sectionCount = (int)ceilf(self.listData.count/7.0);
        if (sectionCount >= 3) {
            sectionCount = 3;
        }
        viewHeight = sectionCount * (kSizeScale(32) + 5) + 15;
    }
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(viewHeight);
    }];
    [UIView performWithoutAnimation:^{
        [self.collectionView reloadData];
    }];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.listData.count > 21 ? 21 : self.listData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class currentCls = [FriendCircelZanCell class];
    FriendCircelZanCell *zanCell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(currentCls) forIndexPath:indexPath];
    if (kValidArray(self.listData) && row < self.listData.count) {
        FriendCircleFavorAvatarInfoModel *model = [self.listData objectAtIndex:row];
        if (row >= 20 && self.listData.count > 21) {
            zanCell.isShowCover = YES;
        }
        else {
            zanCell.isShowCover = NO;
        }
        zanCell.headerURLStr = model.avatarUrl;
    }
    return zanCell;
}

@end
