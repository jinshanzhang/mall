//
//  FriendCircleHeaderView.h
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendCircleHeaderView : UIView

@property (nonatomic, strong) NSMutableArray *headerSources;

@end
