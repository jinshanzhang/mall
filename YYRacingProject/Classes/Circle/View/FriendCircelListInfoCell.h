//
//  FriendCircelListInfoCell.h
//  YIYanProject
//
//  Created by cjm on 2018/6/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

#import "FriendCircleListInfoModel.h"
@interface FriendCircelListInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) FriendCircleListInfoModel *listModel;

// 展开收起block
@property (nonatomic, copy) void (^foldAndShowAllClickBlock)(FriendCircelListInfoCell *currentCell,
FriendCircleListInfoModel *currentModel);

// 点赞block
@property (nonatomic, copy) void (^zanBlock)(FriendCircelListInfoCell *currentCell,
FriendCircleListInfoModel *currentModel);

// 分享block
@property (nonatomic, copy) void (^shareBlock)(FriendCircelListInfoCell *currentCell);

// 下载block
@property (nonatomic, copy) void (^downloadBlock)(FriendCircelListInfoCell *currentCell);

// 跳到商品详情block
@property (nonatomic, copy) void (^skipGoodDetailBlock)(id selectModel);

@end
