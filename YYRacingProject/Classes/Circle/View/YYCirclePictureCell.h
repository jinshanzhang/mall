//
//  YYCirclePictureCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYMultiItemScrollBaseCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYCirclePictureCell : YYMultiItemScrollBaseCell

@end

NS_ASSUME_NONNULL_END
