//
//  YYCircleSectionInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircleSectionInfoView.h"

#import "YYTopicInfoModel.h"
#import "YYCircelMenuBarItem.h"
@interface YYCircleSectionInfoView ()
<JMMenuBarDelegate,
JMMenuBarDataSource>

@property (nonatomic, strong) JMMenuBar *menuView;
@end

@implementation YYCircleSectionInfoView

#pragma mark - Setter
- (JMMenuBar *)menuView {
    if (!_menuView) {
        _menuView = [[JMMenuBar alloc] initWithFrame:CGRectZero];
        _menuView.backgroundColor = XHWhiteColor;
        _menuView.showsHorizontalScrollIndicator = NO;
        _menuView.showsVerticalScrollIndicator = NO;
        _menuView.clipsToBounds = YES;
        _menuView.scrollsToTop = NO;
        _menuView.isUseCache = NO;
        _menuView.isStartAddSpace = YES;
        _menuView.menuBarItemSpace = kSizeScale(12);
        _menuView.menuBarEdgeInsets = UIEdgeInsetsMake(0, 0, 0, kSizeScale(12));
        _menuView.selectItemStayType = JMMenuBarStayLocationCenter;
        _menuView.menuBarLayoutStyle = JMMenuBarLayoutStyleDefault;
        _menuView.menuBarDelegate = self;
        _menuView.menuBarDataSource = self;
    }
    return _menuView;
}

- (void)setContentDatas:(NSMutableArray *)contentDatas {
    _contentDatas = contentDatas;
    if (_contentDatas) {
        self.menuView.menuBarContents = _contentDatas;
        [self.menuView reloadData];
    }
}


#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _setSubViews];
        [self _setSubLayouts];
    }
    return self;
}

#pragma mark - Public
- (void)willMenuBarItemSelectAtIndex:(NSInteger)selectIndex {
    if (selectIndex >= 0) {
        self.menuView.currentItemIndex = selectIndex;
        [self.menuView updateItemSelectStateAtIndex:selectIndex];
    }
}

#pragma mark - Private
- (void)_setSubViews {
    [self addSubview:self.menuView];
}

- (void)_setSubLayouts {
    [self.menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

#pragma mark - JMMenuBarDataSource
- (JMMenuBarItem *)menuBar:(JMMenuBar *)menuBar menuBarItemAtIndex:(NSInteger)itemIndex {
    static NSString *identifier = @"circle-menu-bar-item";
    YYCircelMenuBarItem *item = [menuBar dequeueReusableItemWithIdentifier:identifier];
    if (!item) {
        item = [[YYCircelMenuBarItem alloc] init];
    }
    if (itemIndex < self.contentDatas.count) {
        YYTopicInfoModel *topicModel = [self.contentDatas objectAtIndex:itemIndex];
        item.titleLabel.text = topicModel.title;
    }
    return item;
}

#pragma mark - JMMenuBarDelegate
- (void)menuBar:(JMMenuBar *)menuBar didSelectItemAtIndex:(NSInteger)itemIndex {
    if (self.delegate && [self.delegate respondsToSelector:@selector(sectionViewItemDidSelect:didSelectItemAtIndex:)]) {
        [self.delegate sectionViewItemDidSelect:self didSelectItemAtIndex:itemIndex];
    }
}

- (CGFloat)menuBar:(JMMenuBar *)menuBar menuBarItemWidthAtIndex:(NSInteger)itemIndex {
    if (itemIndex < self.contentDatas.count) {
        YYTopicInfoModel *topicModel = [self.contentDatas objectAtIndex:itemIndex];
        CGFloat width = [YYCommonTools sizeWithText:topicModel.title
                                               font:normalFont(13)
                                            maxSize:CGSizeMake(MAXFLOAT, kSizeScale(25))].width;
        return width + kSizeScale(20);
    }
    return kScreenW/5.0;
}

@end
