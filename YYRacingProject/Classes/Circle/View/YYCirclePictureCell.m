//
//  YYCirclePictureCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCirclePictureCell.h"

#import "YYMultiItemScrollModel.h"

@interface YYCirclePictureCell ()

@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UIImageView *placeImageView;

@property (nonatomic, strong) UIView *priceView;
@property (nonatomic, strong) UILabel *priceLabel;

@end

@implementation YYCirclePictureCell

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    if (viewModel) {
        self.placeImageView.hidden = YES;
        self.priceLabel.hidden = self.priceView.hidden = YES;
        if (viewModel.type == 1) {
            //商品
            NSString *priceContent = [NSString stringWithFormat:@"¥%@", viewModel.nowPrice];
            CGSize priceSize = [YYCommonTools sizeWithText:priceContent
                                                      font:normalFont(12)
                                                   maxSize:CGSizeMake(MAXFLOAT, 16)];
            [self.priceView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(priceSize.width+5);
            }];
            self.priceLabel.text = priceContent;
            self.priceLabel.hidden = self.priceView.hidden = NO;
            
            if ([viewModel.saleStatus integerValue] == 2) {
                // 预热
                UIImage  *colorImage = [UIImage gradientColorImageFromColors:@[HexRGB(0x11BC56), HexRGB(0x2ee478)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(40, 16)];
                UIColor *bgcolor = [UIColor colorWithPatternImage:colorImage];
                [self.priceView setBackgroundColor:bgcolor];
            }
            else {
                // 热卖
                UIImage  *colorImage = [UIImage gradientColorImageFromColors:@[HexRGB(0xFF3934), HexRGB(0xFE5C37)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(40, 16)];
                UIColor *bgcolor = [UIColor colorWithPatternImage:colorImage];
                [self.priceView setBackgroundColor:bgcolor];
            }
            
        }
        if (viewModel.type == 4) {
            //视频
            self.placeImageView.hidden = NO;
            self.placeImageView.image = [UIImage imageNamed:@"video_pause_mini_icon"];
        }
        
        [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:viewModel.imageUrl]];
    }
}

- (void)createUI {
    self.contentImageView = [YYCreateTools createImageView:nil
                                            viewModel:-1];
    self.contentImageView.layer.masksToBounds = YES;
    self.contentImageView.backgroundColor = XHLightColor;
    self.contentImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.contentView addSubview:self.contentImageView];
    
    
    self.placeImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.contentView addSubview:self.placeImageView];
    
    self.priceView = [UIView new];
    [self.contentView addSubview:self.priceView];
    
    self.priceLabel = [YYCreateTools createLabel:nil
                                       font:normalFont(12)
                                  textColor:XHLightColor];
    self.priceLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.priceLabel];

    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.placeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(49), kSizeScale(49)));
    }];
    
    [self.priceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self.contentView);
        make.width.mas_equalTo(0);
        make.height.mas_equalTo(16);
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.priceView);
    }];
}

@end
