//
//  FriendTopicCollectionInfoCell.h
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendTopicCollectionInfoCell : UICollectionViewCell

@property (nonatomic, strong) NSString  *topicURLStr;
@property (nonatomic, strong) NSString  *topicTitle;

@end
