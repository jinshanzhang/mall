//
//  YYCircelMenuBarItem.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuBarItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYCircelMenuBarItem : JMMenuBarItem

@property (nonatomic, strong) UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
