//
//  YYCicleLikeCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCicleLikeCell.h"

#import "YYMultiItemScrollModel.h"
@interface YYCicleLikeCell ()

@property (nonatomic, strong) UIImageView *contentImageView;
@property (nonatomic, strong) UIView *coverView;
@property (nonatomic, strong) UILabel *coverContentLabel;

@end

@implementation YYCicleLikeCell

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    if (viewModel) {
        [self.contentImageView yy_sdWebImage:viewModel.imageUrl
                       placeholderImageType:YYPlaceholderImageListGoodType];
        self.coverView.alpha = (viewModel.type == 1 ? 0.4 : 0.0);
        self.coverContentLabel.alpha = (viewModel.type == 1 ? 1 : 0.0);
    }
}

- (void)createUI {
    self.contentImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.contentView addSubview:self.contentImageView];
    
    self.coverView = [YYCreateTools createView:XHBlackColor];
    self.coverView.alpha = 0.0;
    [self.contentView addSubview:self.coverView];
    
    self.coverContentLabel = [YYCreateTools createLabel:@"... "
                                              font:normalFont(18)
                                         textColor:XHWhiteColor];
    self.coverContentLabel.textAlignment = NSTextAlignmentCenter;
    self.coverContentLabel.alpha = 0.0;
    [self.contentView addSubview:self.coverContentLabel];
    
    [self.contentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.coverView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.coverContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.coverView);
        make.centerY.equalTo(self.coverView).offset(-4);
        make.height.mas_equalTo(15);
    }];
}

@end
