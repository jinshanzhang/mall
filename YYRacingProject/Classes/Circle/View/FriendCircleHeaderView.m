//
//  FriendCircleHeaderView.m
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleHeaderView.h"

#import "HomeBannerInfoModel.h"
#import "YYTopicInfoModel.h"

@interface FriendCircleHeaderView()
<
SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *topBannerView;

@property (nonatomic, assign) CGFloat headerHeight;
@property (nonatomic, assign) CGFloat bannerHeight;
@property (nonatomic, strong) NSMutableArray *bannerArray;


@end

@implementation FriendCircleHeaderView

#pragma mark - Setter
- (SDCycleScrollView *)topBannerView {
    if (!_topBannerView) {
        _topBannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectZero];
        _topBannerView.autoScroll = NO;
        _topBannerView.delegate = self;
        _topBannerView.backgroundColor = XHWhiteColor;
        _topBannerView.pageControlDotSize = CGSizeMake(7, 7);
        _topBannerView.pageDotImage = [UIImage imageNamed:@"home_banner_default_icon"];
        _topBannerView.placeholderImage = [UIImage imageWithColor:XHLightColor];
        _topBannerView.currentPageDotImage = [UIImage imageNamed:@"home_banner_selected_icon"];
    }
    return _topBannerView;
}

- (void)setHeaderSources:(NSMutableArray *)headerSources {
    _headerSources = headerSources;
    if (kValidArray(_headerSources)) {
        self.bannerHeight = kSizeScale(150);
        self.bannerArray = [NSMutableArray array];
        self.headerHeight = self.bannerHeight + kSizeScale(12)*2;
        [self.headerSources enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeBannerInfoModel *model = (HomeBannerInfoModel *)obj;
            [self.bannerArray addObject:model.imageUrl];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.topBannerView.imageURLStringsGroup = self.bannerArray;
        });
    }
    else {
        self.headerHeight = 0.1;
        self.bannerHeight = kSizeScale(0.1);
    }
    [self.topBannerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(self.bannerHeight);
    }];
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.headerHeight).priorityHigh();
    }];
    
    [self layoutIfNeeded];
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.headerHeight = 0.1;
        self.bannerHeight = kSizeScale(150);
        self.backgroundColor = XHWhiteColor;
        self.bannerArray = [NSMutableArray array];
        
        [self createUI];
    }
    return self;
}

- (void)createUI {
    // banner
    [self addSubview:self.topBannerView];
}


#pragma mark - SDCycleDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if (index < self.headerSources.count) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        HomeBannerInfoModel *bannerModel = [self.headerSources objectAtIndex:index];
        if (bannerModel) {
            [params setObject:@(bannerModel.linkType) forKey:@"linkType"];
            [params setObject:bannerModel.url forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    }
}

@end
