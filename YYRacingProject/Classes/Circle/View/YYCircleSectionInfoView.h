//
//  YYCircleSectionInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YYCircleSectionInfoView;
@protocol YYCircleSectionViewDelegate <NSObject>

@optional
- (void)sectionViewItemDidSelect:(YYCircleSectionInfoView *)view
                    didSelectItemAtIndex:(NSInteger)itemIndex;

@end

@interface YYCircleSectionInfoView : UIView

@property (nonatomic, strong) NSMutableArray *contentDatas;
@property (nonatomic, weak) id <YYCircleSectionViewDelegate> delegate;

- (void)willMenuBarItemSelectAtIndex:(NSInteger)selectIndex;

@end

NS_ASSUME_NONNULL_END
