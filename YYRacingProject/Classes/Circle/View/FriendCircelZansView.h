//
//  FriendCircelZansView.h
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendCircelZansView : UIView

@property (nonatomic, strong) NSMutableArray *zanArrs;

@end
