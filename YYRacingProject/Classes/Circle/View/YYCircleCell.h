//
//  YYCircleCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYCircleLayout.h"

#import "YYMultiItemScrollView.h"
#import "YYMultiItemScrollModel.h"

NS_ASSUME_NONNULL_BEGIN
@class YYCircleCell;

// 蜀黍之家圈工具栏
@interface YYCircleToolBarView : UIView

@property (nonatomic, strong) UIButton *likeButton;
@property (nonatomic, strong) UIButton *shareButton;
@property (nonatomic, strong) UIButton *downloadButton;

@property (nonatomic, strong) UIImageView *likeImageView;
@property (nonatomic, strong) UIImageView *shareImageView;
@property (nonatomic, strong) UIImageView *downloadImageView;

@property (nonatomic, strong) YYLabel *likeLabel;
@property (nonatomic, strong) YYLabel *shareLabel;
@property (nonatomic, strong) YYLabel *downloadLabel;

@property (nonatomic, weak) YYCircleCell *cell;

- (void)setLayout:(YYCircleLayout *)layout;
@end

// 蜀黍之家圈点赞视图
@interface YYCircleLikeView : UIView

@property (nonatomic, strong) UIImageView *backgroundView;
@property (nonatomic, strong) YYMultiItemScrollView *scrollView;

@property (nonatomic, weak) YYCircleCell *cell;
- (void)setLayout:(YYCircleLayout *)layout;
@end

// 蜀黍之家圈cell 视图
@interface YYCircleView : UIView

@property (nonatomic, strong) UIView *contentView;      // 容器

@property (nonatomic, strong) UIImageView *avatarView;  // 头像
@property (nonatomic, strong) YYLabel *nameLabel; // 昵称
@property (nonatomic, strong) UIImageView *tagImageView; // 置顶
@property (nonatomic, strong) YYLabel *textLabel; // 文本内容

@property (nonatomic, strong) YYMultiItemScrollView *gridView; //图片宫格
@property (nonatomic, strong) YYLabel *createTimeLabel; //时间

@property (nonatomic, strong) YYCircleToolBarView *toolBarView; //工具栏
@property (nonatomic, strong) YYCircleLikeView *likeView; //赞列表

@property (nonatomic, strong) YYCircleLayout *layout;
@property (nonatomic, weak) YYCircleCell *cell;

@end


@protocol YYCircleCellDelegate;

@interface YYCircleCell : UITableViewCell

@property (nonatomic, weak) id <YYCircleCellDelegate> delegate;
@property (nonatomic, strong) YYCircleView *circleView;

- (void)setLayout:(YYCircleLayout *)layout;

@end

@protocol YYCircleCellDelegate <NSObject>

@optional
// 图片点击事件
- (void)photoItemsView:(NSMutableArray *)allItems clickItemAtIndexPath:(NSIndexPath *)indexPath clickItemModel:(YYCircleModel *)circleModel;

// 点赞事件
- (void)cellDidClickZan:(YYCircleCell *)circleCell;

// 分享事件
- (void)cellDidClickShare:(YYCircleCell *)circleCell;

// 下载事件
- (void)cellDidClickDownload:(YYCircleCell *)circleCell;
@end



NS_ASSUME_NONNULL_END
