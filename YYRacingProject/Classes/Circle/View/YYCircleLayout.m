//
//  YYCircleLayout.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircleLayout.h"

#import "YYCircleModel.h"

@implementation YYCTextLinePositionModifier

- (instancetype)init {
    self = [super init];
    
    if (kiOS9Later) {
        _lineHeightMultiple = 1.34;   // for PingFang SC
    } else {
        _lineHeightMultiple = 1.3125; // for Heiti SC
    }
    
    return self;
}

- (void)modifyLines:(NSArray *)lines fromText:(NSAttributedString *)text inContainer:(YYTextContainer *)container {
    //CGFloat ascent = _font.ascender;
    CGFloat ascent = _font.pointSize * 0.86;
    
    CGFloat lineHeight = _font.pointSize * _lineHeightMultiple;
    for (YYTextLine *line in lines) {
        CGPoint position = line.position;
        position.y = _paddingTop + ascent + line.row  * lineHeight;
        line.position = position;
    }
}

- (id)copyWithZone:(NSZone *)zone {
    YYCTextLinePositionModifier *one = [self.class new];
    one->_font = _font;
    one->_paddingTop = _paddingTop;
    one->_paddingBottom = _paddingBottom;
    one->_lineHeightMultiple = _lineHeightMultiple;
    return one;
}

- (CGFloat)heightForLineCount:(NSUInteger)lineCount {
    if (lineCount == 0) return 0;
    //    CGFloat ascent = _font.ascender;
    //    CGFloat descent = -_font.descender;
    CGFloat ascent = _font.pointSize * 0.86;
    CGFloat descent = _font.pointSize * 0.14;
    CGFloat lineHeight = _font.pointSize * _lineHeightMultiple;
    return _paddingTop + _paddingBottom + ascent + descent + (lineCount - 1) * lineHeight;
}

@end


@implementation YYCircleLayout

- (instancetype)initWithCircleModel:(YYCircleModel *)circleModel {
    if (!circleModel) {
        return nil;
    }
    self = [super init];
    _circleModel = circleModel;
    
    [self layout];
    return self;
}

- (void)layout {
    [self _layout];
}

- (void)_layout {
    _marginTop = kYYCellTopPaddingProfile + 8;
    _avatarHeight = kYYCellProfileHeight;
    _nickNameHeight = 0;
    _textHeight = 0;
    _pictureHeight = 0;
    _createTimeHeight = 0;
    _toolBarHeight = kYYCellToolBarHeight;
    _zanHeight = 0;
    _marginBottom = kYYCellTopPaddingProfile;
    
    // 内容布局
    [self _layoutNickName];
    [self _layoutText];
    [self _layoutPicture];
    [self _layoutCreateTime];
    [self _layoutToolBar];
    
    // 高度计算
    _height = 0;
    _height += _marginTop;
    if (_nickNameHeight + _textHeight > _avatarHeight) {
        _height += (_nickNameHeight + _textHeight + kYYCellTopPaddingText);
    }
    else {
        _height += _avatarHeight;
    }
    
    if (_pictureHeight > 0) {
        _height += (_pictureHeight + kYYCellPaddingItem);
    }
    else {
        _height += _pictureHeight;
    }

    if (_createTimeHeight > 0) {
        _height += (_createTimeHeight + kYYCellPaddingItem);
    }
    else {
        _height += _createTimeHeight;
    }
    
    _height += _toolBarHeight + (_createTimeHeight <= 0 ?kYYCellPaddingItem:0);
    
    if (kValidArray(_circleModel.favorUserItems)) {
       NSInteger rowCount = (int)ceilf(_circleModel.favorUserItems.count/7.0);
        if (rowCount >= 3) {
            rowCount = 3;
        }
        _zanHeight = (rowCount * (kSizeScale(30) + 5) + 15);
    }
    
    _height += _zanHeight;
    
    _height += _marginBottom;
}


- (void)_layoutNickName {
    _nickNameHeight = 0;
    _nickNameLayout = nil;
    
    if (_circleModel.nickname.length == 0) {
        return;
    }
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:_circleModel.nickname];
    
    text.color = kYYCellNameTextColor;
    text.font = [UIFont boldSystemFontOfSize:kYYCellNameFont];
    
    YYCTextLinePositionModifier *modifier = [YYCTextLinePositionModifier new];
    modifier.font = [UIFont fontWithName:@"Heiti SC" size:kYYCellNameFont];
    modifier.paddingTop = modifier.paddingBottom = 0;
    
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(kYYCellContentWidth, HUGE);
    container.linePositionModifier = modifier;
    container.maximumNumberOfRows = 1;
    
    _nickNameLayout = [YYTextLayout layoutWithContainer:container
                                                   text:text];
    if (!_nickNameLayout) return;
    
    _nickNameHeight = [modifier heightForLineCount:_nickNameLayout.rowCount];
}


- (void)_layoutText {
    _textHeight = 0;
    _textLayout = nil;
    
    if (_circleModel.contents.length == 0) {
        return;
    }
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:_circleModel.contents];
    
    text.color = XHBlackColor;
    text.font = [UIFont systemFontOfSize:kYYCellTextFont];
    
    YYCTextLinePositionModifier *modifier = [YYCTextLinePositionModifier new];
    modifier.font = [UIFont fontWithName:@"Heiti SC" size:kYYCellTextFont];
    modifier.paddingTop = modifier.paddingBottom = 2;
    
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(kYYCellContentWidth, HUGE);
    container.linePositionModifier = modifier;
    
    _textLayout = [YYTextLayout layoutWithContainer:container
                                                   text:text];
    if (!_textLayout) return;
    
    _textHeight = [modifier heightForLineCount:_textLayout.rowCount];
}

- (void)_layoutPicture {
    _pictureHeight = 0;
    _pictureSize = CGSizeZero;
    
    if (_circleModel.pictureItems.count == 0) {
        return;
    }
    
    NSInteger pictureCount = _circleModel.pictureItems.count;
    _pictureSize = pictureCount >= 2 ? CGSizeMake(kSizeScale(78), kSizeScale(78)) : CGSizeZero;
    switch (pictureCount) {
        case 1: {
            YYPictureModel *itemModel = [_circleModel.pictureItems firstObject];
            _pictureSize = [self pictureContainerSizeHandlerAfter:CGSizeMake(itemModel.width/2.0, itemModel.height/2.0)];
            _pictureHeight = _pictureSize.height;
        }
            break;
        case 2: case 4: {
            _pictureHeight = (int)(pictureCount / 2) * (_pictureSize.height + kYYCellPaddingPic);
        }
            break;
        default: {
            _pictureHeight =  (int)ceilf(pictureCount/3.0) * (_pictureSize.height + kYYCellPaddingPic);
        }
            break;
    }
}

- (void)_layoutCreateTime {
    _createTimeHeight = 0;
    _createTimeLayout = nil;
    
    if (_circleModel.createTime.length == 0) {
        return;
    }
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:_circleModel.createTime];
    
    text.color = kYYCellTextColor;
    text.font = [UIFont systemFontOfSize:KYYCellPriceFont];
    
    YYCTextLinePositionModifier *modifier = [YYCTextLinePositionModifier new];
    modifier.font = [UIFont fontWithName:@"Heiti SC" size:KYYCellPriceFont];
    modifier.paddingTop = modifier.paddingBottom = 0;
    
    YYTextContainer *container = [YYTextContainer new];
    container.size = CGSizeMake(kYYCellContentWidth, HUGE);
    container.linePositionModifier = modifier;
    
    _createTimeLayout = [YYTextLayout layoutWithContainer:container
                                               text:text];
    if (!_createTimeLayout) return;
    
    _createTimeHeight = [modifier heightForLineCount:_createTimeLayout.rowCount];
}

- (void)_layoutToolBar {
    _toolbarLikeTextWidth = 0;
    _toolbarShareTextWidth = 0;
    _toolbarDownloadTextWidth = 0;
    
    _toolbarLikeTextLayout = nil;
    _toolbarShareTextLayout = nil;
    _toolbarDownloadTextLayout = nil;
    
    YYTextContainer *container = [YYTextContainer containerWithSize:CGSizeMake(kScreenWidth, kYYCellToolBarHeight)];
    container.maximumNumberOfRows = 1;
    //赞
    NSMutableAttributedString *likeText = [[NSMutableAttributedString alloc] initWithString:@"赞"];
    likeText.font = normalFont(11);
    likeText.color = XHBlackColor;
    
    NSMutableAttributedString *likeCount = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_circleModel.favorCnt<=0?@"":@(_circleModel.favorCnt)]];
    
    [likeCount yy_setAttributes:XHBlackLitColor
                       font:normalFont(11)
                    content:likeCount
                  alignment:NSTextAlignmentLeft];
    [likeText appendAttributedString:likeCount];
    
    _toolbarLikeTextLayout = [YYTextLayout layoutWithContainer:container text:likeText];
    _toolbarLikeTextWidth = CGFloatPixelRound(_toolbarLikeTextLayout.textBoundingRect.size.width);
    //分享
    NSMutableAttributedString *shareText = [[NSMutableAttributedString alloc] initWithString:@"分享"];
    shareText.font = normalFont(11);
    shareText.color = XHBlackColor;
    NSMutableAttributedString *shareCount = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",_circleModel.shareCnt<=0?@"":@(_circleModel.shareCnt)]];
    
    [shareCount yy_setAttributes:XHBlackLitColor
                           font:normalFont(11)
                        content:shareCount
                      alignment:NSTextAlignmentLeft];
    [shareText appendAttributedString:shareCount];
    
    _toolbarShareTextLayout = [YYTextLayout layoutWithContainer:container text:shareText];
    _toolbarShareTextWidth = CGFloatPixelRound(_toolbarShareTextLayout.textBoundingRect.size.width);
    
    //下载
    NSMutableAttributedString *downloadText = [[NSMutableAttributedString alloc] initWithString:@"一键下载"];
    downloadText.font = normalFont(11);
    downloadText.color = XHBlackColor;
    
    _toolbarDownloadTextLayout = [YYTextLayout layoutWithContainer:container text:downloadText];
    _toolbarDownloadTextWidth = CGFloatPixelRound(_toolbarDownloadTextLayout.textBoundingRect.size.width);
    _isHideShare = (_circleModel.viewType == 4) ? YES : NO;
}

- (CGSize)pictureContainerSizeHandlerAfter:(CGSize)originSize {
    //宽高比
    CGFloat  scale = 0.0;
    CGFloat  maxValue = kSizeScale(163);
    CGFloat  minValue = kSizeScale(55);
    CGFloat  originWidth = originSize.width;
    CGFloat  originHeight = originSize.height;
    CGFloat  originScale = originWidth/originHeight;
    
    if (originWidth <= 0 || originHeight <= 0) {
        return CGSizeZero;
    }
    
    if (originScale < minValue/maxValue) {
        //高同比例压缩，截取中间326
        scale = maxValue/originHeight;
        return CGSizeMake(originWidth*scale < minValue ? minValue : originWidth*scale , maxValue);
    }
    else if (originScale >= minValue/maxValue && originScale <= 1) {
        //高压缩到最大值，宽同比例压缩
        scale = maxValue/originHeight;
        return CGSizeMake(scale*originWidth > minValue ? scale*originWidth : minValue, maxValue);
    }
    else if (maxValue/minValue >= originScale && maxValue/minValue > 1) {
        //宽压缩到326， 高同比例压缩
        scale = maxValue/originWidth;
        return CGSizeMake(maxValue , originHeight*scale < minValue ? minValue : originHeight*scale);
    }
    else if (originScale > maxValue/minValue) {
        //宽同比例压缩, 截取中间326
        scale = maxValue/originWidth;
        return CGSizeMake(maxValue , originHeight*scale < minValue ? minValue : originHeight*scale);
    }
    return CGSizeZero;
}


@end

