//
//  FriendCircelZanCell.h
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendCircelZanCell : UICollectionViewCell

@property (nonatomic, strong) NSString *headerURLStr;
@property (nonatomic, assign) BOOL     isShowCover;

@end
