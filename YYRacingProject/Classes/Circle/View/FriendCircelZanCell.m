//
//  FriendCircelZanCell.m
//  YIYanProject
//
//  Created by cjm on 2018/6/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircelZanCell.h"

@interface FriendCircelZanCell()

@property (nonatomic, strong) UIImageView *headerImageView;
@property (nonatomic, strong) UIView      *topicBackgroundView;
@property (nonatomic, strong) UILabel     *topicContent;

@end

@implementation FriendCircelZanCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.headerImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self addSubview:self.headerImageView];
    [self.headerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    self.topicBackgroundView = [YYCreateTools createView:XHBlackColor];
    self.topicBackgroundView.alpha = .4;
    self.topicBackgroundView.hidden = YES;
    [self addSubview:self.topicBackgroundView];
     
    [self.topicBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    self.topicContent = [YYCreateTools createLabel:@"... "
    font:normalFont(18)
    textColor:XHWhiteColor];
    //self.topicContent.hidden = YES;
    self.topicContent.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.topicContent];
     
    [self.topicContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.headerImageView);
        make.centerY.equalTo(self.headerImageView).offset(-4);
        make.height.mas_equalTo(15);
    }];
}

- (void)setHeaderURLStr:(NSString *)headerURLStr {
    _headerURLStr = headerURLStr;
    [self.headerImageView yy_sdWebImage:_headerURLStr
                   placeholderImageType:YYPlaceholderImageListGoodType];
}

- (void)setIsShowCover:(BOOL)isShowCover {
    _isShowCover = isShowCover;
    if (_isShowCover) {
        self.topicBackgroundView.hidden = NO;
        self.topicContent.hidden = NO;
    }
    else {
        self.topicBackgroundView.hidden = YES;
        self.topicContent.hidden = YES;
    }
}

@end
