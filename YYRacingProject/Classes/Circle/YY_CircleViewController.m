//
//  YY_CircleViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_CircleViewController.h"

#import "YYCircleModel.h"
#import "YYTopicInfoModel.h"
#import "HomeBannerInfoModel.h"
#import "YYCircleCommonEventModel.h"
#import "FriendCircleTopicInfoModel.h"

#import "YYCircleCell.h"
#import "YYCircleLayout.h"
#import "FriendCircleHeaderView.h"
#import "YYCircleSectionInfoView.h"

#import "FriendCircleListInfoRequestAPI.h"
#import "FriendCircleTopicInfoRequestAPI.h"

@interface YY_CircleViewController ()
<YYCircleCellDelegate,
YYCircleSectionViewDelegate>

@property (nonatomic, strong) FriendCircleHeaderView *headerView;
@property (nonatomic, strong) YYCircleSectionInfoView *sectionView;

@property (nonatomic, assign) BOOL            isHover;     //是否悬停
@property (nonatomic, assign) BOOL            isClick;     //是否点击
@property (nonatomic, assign) BOOL            hasMore;     //是否有更多
@property (nonatomic, strong) NSString        *pageCursor; //分页游标
@property (nonatomic, strong) NSNumber        *topID;      //话题ID
@property (nonatomic, assign) NSInteger       currentPage; //当前位置
@property (nonatomic, strong) NSMutableArray  *layoutList; //布局数组

@property (nonatomic, strong) NSMutableArray  *topicList;
@property (nonatomic, strong) NSMutableArray  *bannerList;

@end

@implementation YY_CircleViewController

#pragma mark - Getter and Setter

- (FriendCircleHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[FriendCircleHeaderView alloc] initWithFrame:CGRectZero];
    }
    return _headerView;
}

- (YYCircleSectionInfoView *)sectionView {
    if (!_sectionView) {
        _sectionView = [[YYCircleSectionInfoView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(44))];
        _sectionView.delegate = self;
    }
    return _sectionView;
}

#pragma mark - Life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [MobClick event:@"UpinCilcle"];
}

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        // 子类自己刷新
        self.isSubReload = YES;
        self.NoDataType = YYEmptyViewNoDataType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isClick = NO;
    self.isHover = NO;
    self.pageCursor = @"";
    self.currentPage = 0;
    self.topID = @(-1);
    self.layoutList = [NSMutableArray array];
    
    [self xh_addTitle:@"蜀黍之家圈"];
    
    Class current = [YYCircleCell class];
    registerClass(self.m_tableView, current);
    
    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.tableHeaderView = self.headerView;
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.equalTo(self.m_tableView);
    }];

    [self loadNewer];
    // Do any additional setup after loading the view.
}

#pragma mark - Base method
- (void)loadPullDownPage {
    self.pageCursor = @"";
    [self.layoutList removeAllObjects];
    [super loadPullDownPage];
}

- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.pageCursor forKey:@"cursor"];
    if ([self.topID integerValue] >= 0) {
        [dict setObject:self.topID forKey:@"topic"];
    }
    FriendCircleListInfoRequestAPI *listAPI = [[FriendCircleListInfoRequestAPI alloc] initFriendCircleListRequest:dict];
    return listAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        NSArray *moments = [NSArray modelArrayWithClass:[YYCircleModel class] json:[responseDic objectForKey:@"moments"]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // 开启线程转化布局
            for (int i = 0; i < moments.count; i ++) {
                YYCircleLayout *layout = [[YYCircleLayout alloc] initWithCircleModel:[moments objectAtIndex:i]];
                [self.layoutList addObject:layout];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!self.isClick || !self.isHover) {
                    [self circleTopicRequest];
                    [self.m_tableView reloadData];
                }
                else {
                    [UIView animateWithDuration:0.1 animations:^{
                        [self.m_tableView reloadData];
                    } completion:^(BOOL finished) {
                        [self.m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:NO];
                    }];
                }
                self.isClick = NO;
            });
        });
        NSMutableArray *items = [NSMutableArray array];
        if (kValidArray(moments)) {
            items = [moments mutableCopy];
            [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSMutableArray *favors = [NSMutableArray array];
                YYCircleModel *itemModel = (YYCircleModel *)obj;
                if (kValidArray(itemModel.favorUserItems)) {
                    favors = [itemModel.favorUserItems mutableCopy];
                    if (itemModel.favorUserItems.count > 21) {
                        // 点赞数量：当数据大于21时，截取得到21个元素
                        NSArray *subItems = [itemModel.favorUserItems subarrayWithRange:NSMakeRange(0, 21)];
                        YYFavorUserModel *lastModel = [subItems lastObject];
                        lastModel.isAddLayer = YES;
                        favors = [[subItems subarrayWithRange:NSMakeRange(0, subItems.count - 1)] mutableCopy];
                        [favors addObject:lastModel];
                        itemModel.favorUserItems = favors;
                    }
                }
            }];
        }
        self.hasMore = [[responseDic objectForKey:@"hasMore"] boolValue];
        self.pageCursor = [responseDic objectForKey:@"cursor"];
        return items;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return self.hasMore;
}

#pragma mark - Request
// 蜀黍之家圈话题请求
- (void)circleTopicRequest {
    [self.topicList removeAllObjects];
    [self.bannerList removeAllObjects];
    
    self.topicList = [NSMutableArray array];
    self.bannerList = [NSMutableArray array];
    
    FriendCircleTopicInfoRequestAPI *topicRequest = [[FriendCircleTopicInfoRequestAPI alloc] initFriendCircleTopicRequest:[[NSDictionary dictionary] mutableCopy]];
    [topicRequest startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responObject = request.responseJSONObject;
        if (kValidDictionary(responObject)) {
            if ([responObject objectForKey:@"banners"]) {
                // 轮播列表
                NSArray *banners = [NSArray modelArrayWithClass:[HomeBannerInfoModel class] json:[responObject objectForKey:@"banners"]];
                [self.bannerList addObjectsFromArray:banners];
            }
            if ([responObject objectForKey:@"topic"]) {
                // 话题列表
                NSArray *topic = [NSArray modelArrayWithClass:[YYTopicInfoModel class] json:[responObject objectForKey:@"topic"]];
                [self.topicList addObjectsFromArray:topic];
                if (kValidArray(self.topicList)) {
                    YYTopicInfoModel *topicModel = [YYTopicInfoModel new];
                    topicModel.title = @"#全部";
                    topicModel.topicId = -1;
                    [self.topicList insertObject:topicModel atIndex:0];
                }
            }
            
            if ([responObject objectForKey:@"switchFlag"]) {
                // 控制banner
                BOOL bannerSwitch = [[responObject objectForKey:@"switchFlag"] boolValue];
                if (!bannerSwitch) {
                    self.bannerList = [NSMutableArray array];
                }
                self.sectionView.contentDatas = self.topicList;
                [self.sectionView willMenuBarItemSelectAtIndex:self.currentPage];
                
                self.headerView.headerSources = self.bannerList;
                [self.m_tableView layoutIfNeeded];
                self.m_tableView.tableHeaderView = self.headerView;
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


#pragma mark - UITableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        Class currentCls = [YYCircleCell class];
        YYCircleCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.delegate = self;
        return cell;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (kValidArray(self.layoutList) && indexPath.row < self.layoutList.count) {
        [(YYCircleCell *)cell setLayout:self.layoutList[indexPath.row]];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(44);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        if (kValidArray(self.layoutList) && indexPath.row < self.layoutList.count) {
            return ((YYCircleLayout *)self.layoutList[indexPath.row]).height;
        }
        else {
            return height;
        }
    }
    return height;
}

#pragma mark - YYCircleCellDelegate
- (void)cellDidClickZan:(YYCircleCell *)circleCell {
    [YYCircleCommonEventModel cellClickZanEvent:circleCell
                                      tableView:self.m_tableView
                                       listData:self.listData
                                        layouts:self.layoutList];
}


- (void)cellDidClickShare:(YYCircleCell *)circleCell {
    [YYCircleCommonEventModel cellClickShareEvent:circleCell
                                        tableView:self.m_tableView
                                         listData:self.listData
                                          layouts:self.layoutList
                                      resultBlock:^{
                                          [YYCircleCommonEventModel cellClickShareCountEvent:circleCell
                                                                    tableView:self.m_tableView
                                                                    listData:self.listData
                                                                    layouts:self.layoutList];
                                          }];
    
}


- (void)cellDidClickDownload:(YYCircleCell *)circleCell {
    [YYCircleCommonEventModel cellClickDownloadEvent:circleCell
                                           tableView:self.m_tableView
                                            listData:self.listData];
}

- (void)photoItemsView:(NSMutableArray *)allItems clickItemAtIndexPath:(NSIndexPath *)indexPath
        clickItemModel:(YYCircleModel *)circleModel {
    [YYCircleCommonEventModel cellClickPhotoZoomEvent:allItems
                                 clickItemAtIndexPath:indexPath
                                       clickItemModel:circleModel];
}

#pragma mark - YYCircleSectionViewDelegate
- (void)sectionViewItemDidSelect:(YYCircleSectionInfoView *)view didSelectItemAtIndex:(NSInteger)itemIndex {
    if (itemIndex < self.topicList.count) {
        self.currentPage = itemIndex;
        YYTopicInfoModel *topicModel = [self.topicList objectAtIndex:itemIndex];
        self.topID = @(topicModel.topicId);
    }
    self.isClick = YES;
    [self getHoverState];
    [self.sectionView willMenuBarItemSelectAtIndex:self.currentPage];
    [self loadPullDownPage];
}

/*#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self getHoverState];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 快速滑动，慢慢停下后调用
    [self getHoverState];
}*/

#pragma mark - Private
- (void)getHoverState {
    CGFloat currentOffsetY = self.m_tableView.contentOffset.y;
    CGFloat headerY = [self.m_tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].origin.y;
    self.isHover = (currentOffsetY > headerY ? YES : NO);
}

@end
