//
//  YY_VideoPlayViewCtrl.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_VideoPlayViewCtrl.h"
#import "YYVideoPlayControlLayerView.h"

@interface YY_VideoPlayViewCtrl ()
<YYVideoPlayControlLayerDelegate>
@property (nonatomic, strong) SJBaseVideoPlayer *player;
@property (nonatomic, strong) YYVideoPlayControlLayerView *controlLayer;

@property (nonatomic, strong) NSString    *videoURL;
@property (nonatomic, strong) NSString    *shellUri;
@property (nonatomic, strong) NSString    *thumbImageURL;
@end

@implementation YY_VideoPlayViewCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"videoURL"]) {
            self.videoURL = [self.parameter objectForKey:@"videoURL"];
        }
        if ([self.parameter objectForKey:@"shellUri"]) {
            self.shellUri = [self.parameter objectForKey:@"shellUri"];
        }
        if ([self.parameter objectForKey:@"placeholdImageURL"]) {
            self.thumbImageURL = [self.parameter objectForKey:@"placeholdImageURL"];
        }
        if (kValidString(self.videoURL)) {
             self.player.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:self.videoURL]];
        }
        if (kValidString(self.thumbImageURL)) {
            [self.player.placeholderImageView sd_setImageWithURL:[NSURL URLWithString:self.thumbImageURL]];
        }
        
        // 控制跳转到详情按钮是否出现
        if (kValidString(self.shellUri)) {
            self.controlLayer.isHideSkipDetail = NO;
        }
    }
    // Do any additional setup after loading the view.
}

#pragma mark - Private method
- (void)createUI {
    @weakify(self);
    self.controlLayer = [YYVideoPlayControlLayerView new];
    self.controlLayer.delegate = self;
    
    self.player = [SJBaseVideoPlayer player];
    self.player.autoPlayWhenPlayStatusIsReadyToPlay = YES;
    
    self.player.disableVolumeSetting = YES;
    self.player.disableAutoRotation = YES;
    self.player.disableBrightnessSetting = YES;
    self.player.controlLayerDataSource = self.controlLayer;
    self.player.controlLayerDelegate = self.controlLayer;
    self.player.gestureControl.singleTapHandler = ^(id<SJPlayerGestureControl>  _Nonnull control, CGPoint location) {
        @strongify(self);
        if (self.player.playStatus == SJVideoPlayerPlayStatusPaused) {
            [self.player play];
        }
        else {
            [self.player pause];
        }
    };
    [self.view addSubview:self.player.view];
    
    [self.player.view mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
        else make.top.offset(0);
        make.leading.trailing.offset(0);
        make.bottom.offset(0);
    }];
}


#pragma mark - YYVideoPlayControlLayerDelegate
- (void)backButtonItemClickOperator {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)downloadButtonItemClickOperator {
    [kSaveManager saveVideoReachAlbum:self.videoURL];
}

- (void)lookGoodButtonItemClickOperator {
    if (self.player.playStatus != SJVideoPlayerPlayStatusPaused) {
        [self.player pause];
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(self.shellUri)) {
        [params setObject:self.shellUri forKey:@"url"];
    }
    [params setObject:@(1) forKey:@"linkType"];
    [YYCommonTools skipMultiCombinePage:self
                                 params:params];
}
@end
