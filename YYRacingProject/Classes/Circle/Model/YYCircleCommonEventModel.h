//
//  YYCircleCommonEventModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/1.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "YYPopShare.h"
#import "YYCircleCell.h"
#import "YYCircleModel.h"

#import "FriendCircleListZanInfoRequestAPI.h"
#import "FriendCircleListShareSumInfoRequestAPI.h"

#import "FriendCircleCommodityPosterQrInfoRequestAPI.h"
#import "FriendCircleCommodityQRPhotoInfoRequestAPI.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYCircleCommonEventModel : NSObject

/**点赞计数事件**/
+ (void)cellClickZanEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData layouts:(NSMutableArray *)layouts;

/**分享事件**/
+ (void)cellClickShareEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData layouts:(NSMutableArray *)layouts resultBlock:(void(^)(void))resultBlock;

/**分享计数事件**/
+ (void)cellClickShareCountEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData layouts:(NSMutableArray *)layouts;

/**下载事件**/
+ (void)cellClickDownloadEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData;

/**图片或视频点击事件**/
+ (void)cellClickPhotoZoomEvent:(NSMutableArray *)allItems clickItemAtIndexPath:(NSIndexPath *)indexPath clickItemModel:(YYCircleModel *)circleModel;

/**单图生成二维码图片**/
+ (void)commodityQRPhotoWith:(NSMutableDictionary *)params
                    isPoster:(BOOL)isPoster
                   callBlock:(void(^)(NSString *))callBlock;
/**多图生成二维码图片**/
+ (void)commodityMultiQRPhotoWith:(NSMutableArray *)originUrls
                         indexArr:(NSMutableArray *)indexArr
                      needTranArr:(NSMutableArray *)needTranArr
                        callBlock:(void(^)(NSMutableArray *))callBlock;

/**显示分享框**/
+ (void)showShareComponent:(BOOL)isSingle
                 callBlock:(void(^)(NSIndexPath *index, YYPopShareView *view))callBlock;

/**显示打开微信框**/
+ (void)showOpenWechat;
@end

NS_ASSUME_NONNULL_END
