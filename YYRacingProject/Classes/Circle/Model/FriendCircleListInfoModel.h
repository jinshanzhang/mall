//
//  FriendCircleListInfoModel.h
//  YIYanProject
//
//  Created by cjm on 2018/6/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FriendCircleFavorAvatarInfoModel;

@interface FriendCircleListInfoModel : NSObject

@property (nonatomic, strong)  NSString  *desc;
@property (nonatomic, strong)  NSString  *image;

@property (nonatomic, strong)  NSString  *nickname;
@property (nonatomic, strong)  NSString  *personUrl;        //用户头像
@property (nonatomic, strong)  NSString  *avatarUrl;        //用户头像

@property (nonatomic, assign)  NSInteger hasTag;            //是否有tag
@property (nonatomic, assign)  NSInteger tagType;           //tag 类型
@property (nonatomic, strong)  NSString  *contents;         //内容

@property (nonatomic, strong)  NSString  *createTime;       //发布时间

@property (nonatomic, assign)  NSInteger favorCnt;          //赞的数量
@property (nonatomic, assign)  NSInteger shareCnt;          //分享的数量

@property (nonatomic, assign)  NSInteger momentID;          //动态ID
@property (nonatomic, assign)  NSInteger viewType;          //布局类型 0 单张大图 1 四宫格 2 九宫格 3 纯文本 4 视频
@property (nonatomic, assign)  BOOL      hasFavor;          //是否已赞

@property (nonatomic, strong)  NSString  *likeNum;
@property (nonatomic, strong)  NSString  *shareNum;

@property (nonatomic, assign)  BOOL      isFold;
@property (nonatomic, assign)  BOOL      isZan;

@property (nonatomic, strong)  NSMutableArray *tags;
@property (nonatomic, strong)  NSMutableArray *zanList;
@property (nonatomic, strong)  NSMutableArray *imageList;

@end

@interface  FriendCircleFavorAvatarInfoModel : NSObject

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *avatarUrl;

@end

