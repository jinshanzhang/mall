//
//  YYCircleModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// 昵称后面标签
@interface YYTagModel : NSObject

@property (nonatomic, assign) NSInteger type; // tag 类型
@property (nonatomic, copy) NSString *url; // tag url

@end


// 图片和视频资源
@interface YYPictureModel : NSObject

@property (nonatomic, assign) NSInteger type;  //0普通图片  1商品  2大促活动海报图  3粉丝邀约海报  4视频  5店铺海报图
@property (nonatomic, assign) NSInteger imageId;
@property (nonatomic, assign) NSInteger linkType;
@property (nonatomic, assign) NSInteger salesType;  //热卖状态

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *shellUri;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *bigImageUrl;
@property (nonatomic, copy) NSString *videoUrl;

@end


// 已收藏的用户
@interface YYFavorUserModel : NSObject

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *avatarUrl;  // 头像url
@property (nonatomic, assign) BOOL isAddLayer;  //是否添加...图层

@end


// 蜀黍之家圈资源
@interface YYCircleModel : NSObject

@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *avatarUrl;

@property (nonatomic, assign) BOOL hasTag;  // 是否有标签
@property (nonatomic, assign) BOOL hasFavor;  // 是否已经点赞

@property (nonatomic, assign) NSInteger circleID; // 圈ID

@property (nonatomic, assign) NSInteger tagType;  // 标签类型
@property (nonatomic, assign) NSInteger viewType;  //布局类型 0 单张大图 1 四宫格 2 九宫格 3 纯文本 4 视频
@property (nonatomic, assign) NSInteger favorCnt;
@property (nonatomic, assign) NSInteger shareCnt;

@property (nonatomic, copy) NSString *contents;
@property (nonatomic, copy) NSString *createTime;

@property (nonatomic, strong) NSArray <YYTagModel *> *tagItems;
@property (nonatomic, strong) NSMutableArray <YYPictureModel *> *pictureItems;
@property (nonatomic, strong) NSMutableArray <YYFavorUserModel *> *favorUserItems;

@end



NS_ASSUME_NONNULL_END
