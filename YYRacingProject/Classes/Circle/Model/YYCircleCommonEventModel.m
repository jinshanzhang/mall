//
//  YYCircleCommonEventModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/1.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircleCommonEventModel.h"

@interface YYCircleCommonEventModel ()

@end

@implementation YYCircleCommonEventModel

/**点赞计数事件**/
+ (void)cellClickZanEvent:(YYCircleCell *)cell
                tableView:(UITableView *)tableView listData:(NSMutableArray *)listData  layouts:(nonnull NSMutableArray *)layouts {
    
    if ((kUserInfo.useDefaultNick == 1 || kUserInfo.useDefaultAvatar == 1)) {
        NSString *message = @"您还没有设置头像昵称\n快去设置吧~";
        if (kUserInfo.useDefaultAvatar == 1) {
            message = @"您还没有设置头像\n快去设置吧~";
        }
        else if (kUserInfo.useDefaultNick == 1) {
            message = @"您还没有设置昵称\n快去设置吧~";
        }
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:message ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
            
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"确认" Click:^{
            [YYCommonTools skipMineSet:(YYBaseViewController *)[YYCommonTools getCurrentVC] isHideTabbar:YES];
        } type:JCAlertViewTypeDefault];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    __block NSIndexPath *currentPath = [tableView indexPathForCell:cell];
    if (currentPath.row < listData.count) {
        __block YYCircleModel *currentModel = [listData objectAtIndex:currentPath.row];
        __block BOOL  isFavor = !currentModel.hasFavor; //默认是没有选中， 点击一下做取反操作。
        
        [params setObject:@(isFavor) forKey:@"action"];
        [params setObject:@(currentModel.circleID) forKey:@"momentId"];
        
        [YYCenterLoading showCenterLoading];
        FriendCircleListZanInfoRequestAPI *zanRequest = [[FriendCircleListZanInfoRequestAPI  alloc] initFriendCircleAddAndCancelZanRequest:params];
        [zanRequest startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responObject = request.responseJSONObject;
            [YYCenterLoading hideCenterLoading];
            if (kValidDictionary(responObject)) {
                NSString *userHeader = [responObject objectForKey:@"avatarUrl"];
                currentModel.hasFavor = isFavor;
                if (isFavor) {
                    //添加
                    currentModel.favorCnt += 1;
                    
                    YYFavorUserModel *favorModel = [[YYFavorUserModel alloc] init];
                    favorModel.userId = kUserInfo.userId;
                    favorModel.avatarUrl = userHeader;
                    [currentModel.favorUserItems appendObject:favorModel];
                }
                else {
                    //移除
                    __block NSInteger index = -1;
                    currentModel.favorCnt -= 1;
                    
                    [currentModel.favorUserItems enumerateObjectsUsingBlock:^(YYFavorUserModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        YYFavorUserModel *favorModel = (YYFavorUserModel *)obj;
                        if ([favorModel.userId isEqualToString:kUserInfo.userId]) {
                            index = idx;
                            *stop = YES;
                        }
                    }];
                    if (index != -1) {
                        [currentModel.favorUserItems removeObjectAtIndex:index];
                    }
                }
                NSMutableArray *favors = [NSMutableArray array];
                if (kValidArray(currentModel.favorUserItems)) {
                    favors = [currentModel.favorUserItems mutableCopy];
                    if (currentModel.favorUserItems.count > 21) {
                        // 点赞数量：当数据大于21时，截取得到21个元素
                        NSArray *subItems = [currentModel.favorUserItems subarrayWithRange:NSMakeRange(0, 21)];
                        YYFavorUserModel *lastModel = [subItems lastObject];
                        lastModel.isAddLayer = YES;
                        favors = [[subItems subarrayWithRange:NSMakeRange(0, subItems.count - 1)] mutableCopy];
                        [favors addObject:lastModel];
                        currentModel.favorUserItems = favors;
                    }
                }
                // 替换数据, 替换布局，刷新列表
                if (!currentPath) {
                    currentPath = [tableView indexPathForCell:cell];
                }
                YYCircleLayout *layout = [[YYCircleLayout alloc] initWithCircleModel:currentModel];
                [layouts replaceObjectAtIndex:currentPath.row withObject:layout];
                [listData replaceObjectAtIndex:currentPath.row withObject:currentModel];
                [UIView performWithoutAnimation:^{
                    [tableView reloadRowAtIndexPath:currentPath withRowAnimation:UITableViewRowAnimationAutomatic];
                }];
            }
            
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }
}

/**下载事件**/
+ (void)cellClickDownloadEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData {
    __block NSIndexPath *currentPath = [tableView indexPathForCell:cell];
    if (currentPath.row < listData.count) {
        __block YYCircleModel *currentModel = [listData objectAtIndex:currentPath.row];
        if (kValidArray(currentModel.pictureItems)) {
            YYPictureModel *pictureModel = [currentModel.pictureItems firstObject];
            [kSaveManager saveVideoReachAlbum:pictureModel.videoUrl];
        }
    }
}

/**分享事件**/
+ (void)cellClickShareEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData layouts:(NSMutableArray *)layouts resultBlock:(void(^)(void))resultBlock {
    
    __block NSInteger type = -1;  // 0 文本 1 单图 2 多图
    __block NSInteger loadType = -1; // -1 不加载 0 正常加载 1 二维码加载
    __block NSString *shareImageUrl = nil;
    __block NSString *shareContent  = nil;
    //需要后端生成的图片位置
    __block NSMutableArray *qrIndexs = [NSMutableArray array];
    //存储图片的URL
    __block NSMutableArray *photosURL = [NSMutableArray array];
    //存储需要后端生成的图片信息
    __block NSMutableArray *commodityPhotos = [NSMutableArray array];
    __block NSMutableArray *transUrls = [NSMutableArray array];
    
    __block NSIndexPath *currentPath = [tableView indexPathForCell:cell];
    __block NSMutableDictionary *params = [NSMutableDictionary dictionary];
    __block YYCircleModel *currentModel = [listData objectAtIndex:currentPath.row];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 存储图片ID
        [currentModel.pictureItems enumerateObjectsUsingBlock:^(YYPictureModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YYPictureModel *model = (YYPictureModel *)obj;
            if (model.type != 0 && model.type != 4 ) {  //是否是商品，type = 1表示是商品
                [qrIndexs addObject:@(idx)];
                // 需要生成二维码的位置，和数据。
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                [dict setObject:@(model.imageId) forKey:@"imageId"];
                if (kValidString(model.shellUri)) {
                    [dict setObject:model.shellUri forKey:@"shellUri"];
                }
                [commodityPhotos addObject:dict];
            }
            [photosURL addObject:model.imageUrl];
        }];
        
        if (currentModel.viewType == 0 || currentModel.viewType == 3) {
            // 单图 或 文本
            if (currentModel.viewType == 0) {
                YYPictureModel *firstPicture = [currentModel.pictureItems firstObject];
                // type = 0普通图片  1商品  2二维码海报图 4视频
                if (firstPicture.type == 0) {
                    shareImageUrl = firstPicture.imageUrl;
                    loadType = 0;
                }
                else {
                    if (firstPicture.type == 4) return ;
                    
                    NSMutableArray *content = [NSMutableArray array];
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    [dict setObject:@(firstPicture.imageId) forKey:@"imageId"];
                    if (kValidString(firstPicture.shellUri)) {
                        [dict setObject:firstPicture.shellUri forKey:@"shellUri"];
                    }
                    [content addObject:dict];
                    
                    [params setObject:content forKey:@"items"];
                    
                    [YYCircleCommonEventModel commodityQRPhotoWith:params
                                                          isPoster:(firstPicture.type == 1?NO:YES)
                                                         callBlock:^(NSString * _Nonnull imageUrl) {
                                                             if (imageUrl) {
                                                                 shareImageUrl = imageUrl;
                                                             }
                                                         }];
                    loadType = 1;
                }
            }
            else {
                // 文本
                shareContent = currentModel.contents;
            }
            type = (currentModel.viewType == 3 ? 0 : 1);
        }
        else if (currentModel.viewType == 1 || currentModel.viewType == 2) {
            // 多图
            type = 2;
            if (kValidArray(commodityPhotos)) {
                loadType = 1;
                [YYCircleCommonEventModel commodityMultiQRPhotoWith:photosURL indexArr:qrIndexs needTranArr:commodityPhotos callBlock:^(NSMutableArray * _Nonnull tempArr) {
                    transUrls = tempArr;
                }];
            }
            else {
                loadType = 0;
                transUrls = photosURL;
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (kValidString(currentModel.contents)) {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = [NSString stringWithFormat:@"%@",currentModel.contents];
                [YYCommonTools showTipMessage:@"文案已复制"];
            }
            
            if (loadType == 0) {
                // 正常加载
                [YYCenterLoading showCenterLoadingInWindow];
                if (kValidString(shareImageUrl) || !kValidArray(commodityPhotos)) {
                    [YYCenterLoading hideCenterLoadingInWindow];
                }
            }
            else if (loadType == 1) {
                // 二维码加载
                [YYQRCodeCenterLoading showCenterLoading:@"二维码生成中..."
                                               superView:kAppWindow];
                if (kValidArray(commodityPhotos) && kValidArray(transUrls)) {
                    [YYQRCodeCenterLoading hideCenterLoading:nil
                                                   superView:nil];
                }
            }
            
            [YYCircleCommonEventModel showShareComponent:(type==2?NO:YES)
                                               callBlock:^(NSIndexPath * _Nonnull index, YYPopShareView *view) {
                                                   NSInteger atIndex = index.item;
                                                   resultBlock();
                                                   if (type == 2) {
                                                       if (!kValidArray(transUrls)) {
                                                           return ;
                                                       }
                                                       [view popHide];
                            
                                                       YYShareCombineInfoModel *combinModel = [[YYShareCombineInfoModel alloc] init];
                                                       combinModel.operatorType = (atIndex == 1 ? YYShareOperatorWechatSession : YYShareOperatorSavePicture);
                                                       combinModel.shareImageArray = transUrls;
                                                       [kShareManager umengShareModel:combinModel callBack:^(BOOL status) {
                                                           
                                                       }];
                                                       if (atIndex == 3) {
                                                           [YYCircleCommonEventModel showOpenWechat];
                                                       }
                                                   }
                                                   else {
                                                       if (!kValidString(shareContent) && !kValidString(shareImageUrl)) {
                                                           return ;
                                                       }
                                                       [view popHide];
                                                    
                                                       YYShareCombineInfoModel *combinModel = [[YYShareCombineInfoModel alloc] init];
                                                       combinModel.operatorType = (atIndex == 1 ? YYShareOperatorWechatSession : YYShareOperatorWechatTimeLine);
                                                       combinModel.title = shareContent;
                                                       combinModel.thumbImage = shareImageUrl;
                                                       combinModel.shareImage = shareImageUrl;
                                                       [kShareManager umengShareModel:combinModel callBack:^(BOOL status) {
                                                           
                                                       }];
                                                   }
                                                }];
        });
    });
}

/**分享计数事件**/
+ (void)cellClickShareCountEvent:(YYCircleCell *)cell tableView:(UITableView *)tableView listData:(NSMutableArray *)listData  layouts:(NSMutableArray *)layouts {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    __block NSIndexPath *currentPath = [tableView indexPathForCell:cell];
    
    if (currentPath.row < listData.count) {
        __block YYCircleModel *currentModel = [listData objectAtIndex:currentPath.row];
        
        [params setObject:@(1) forKey:@"action"];
        [params setObject:@(currentModel.circleID) forKey:@"momentId"];
        
        FriendCircleListShareSumInfoRequestAPI *shareAPI = [[FriendCircleListShareSumInfoRequestAPI alloc] initFriendCircleShareSumRequest:params];
        [shareAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responObject = request.responseJSONObject;
            if (kValidDictionary(responObject)) {
                currentModel.shareCnt ++;
                // 替换数据, 替换布局，刷新列表
                YYCircleLayout *layout = [[YYCircleLayout alloc] initWithCircleModel:currentModel];
                [layouts replaceObjectAtIndex:currentPath.row withObject:layout];
                [listData replaceObjectAtIndex:currentPath.row withObject:currentModel];
                
                if (currentPath) {
                    // 待查明，为何[tableView indexPathForCell:cell];获取的indexPath 为nil
                    [UIView performWithoutAnimation:^{
                        [tableView reloadRowAtIndexPath:currentPath withRowAnimation:UITableViewRowAnimationAutomatic];
                    }];
                }
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }
}

/**图片或视频点击事件**/
+ (void)cellClickPhotoZoomEvent:(NSMutableArray *)allItems clickItemAtIndexPath:(NSIndexPath *)indexPath clickItemModel:(YYCircleModel *)circleModel {
    BOOL isSkipAlbum = YES;
    NSInteger currentPage = indexPath.row;
    YYPictureModel *videoModel = nil;
    NSMutableArray *items = [NSMutableArray array];
    for (int i = 0; i < circleModel.pictureItems.count; i ++) {
        YYImageInfoModel *imageModel = [[YYImageInfoModel alloc] init];
        YYPictureModel *picModel = [circleModel.pictureItems objectAtIndex:i];
        if (picModel.type == 4) {
            // 是否是视频
            isSkipAlbum = NO;
            videoModel = picModel;
        }
        imageModel.type = picModel.type;
        imageModel.price = picModel.price;
        imageModel.shellUri = picModel.shellUri;
        imageModel.imageId = picModel.imageId;
        imageModel.imageUrl = picModel.imageUrl;
        imageModel.bigImageUrl = picModel.bigImageUrl;
        
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = allItems[i];
        item.imageModel = imageModel;
        item.isAlreadyReplace = NO;
        item.largeImageURL = [NSURL URLWithString:picModel.bigImageUrl];
        [items addObject:item];
    }
    if (isSkipAlbum) {
        YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:currentPage];
        [v presentFromImageView:allItems[currentPage] toContainer:kAppWindow animated:YES completion:nil];
        v.skipGoodBlock = ^(YYImageInfoModel *infoModel) {
            if (infoModel) {
                //跳到商品详情
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                [params setObject:@(1) forKey:@"linkType"];
                if (kValidString(infoModel.shellUri)) {
                    [params setObject:infoModel.shellUri forKey:@"url"];
                }
                [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
            }
        };
        v.hideViewBlock = ^(NSInteger currentPage) {
            currentPage = currentPage;
        };
    }
    else {
        if (videoModel) {
            // 跳到视频播放页
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if (kValidString(videoModel.videoUrl)) {
                [params setObject:videoModel.videoUrl forKey:@"videoURL"];
            }
            if (kValidString(videoModel.imageUrl)) {
                [params setObject:videoModel.imageUrl forKey:@"placeholdImageURL"];
            }
            if (kValidString(videoModel.shellUri)) {
                [params setObject:videoModel.shellUri forKey:@"shellUri"];
            }
            [YYCommonTools skipVideoPlayPage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
        }
    }
}


+ (void)commodityQRPhotoWith:(NSMutableDictionary *)params
                    isPoster:(BOOL)isPoster
                   callBlock:(void(^)(NSString *))callBlock {
    if (isPoster) {
        FriendCircleCommodityPosterQrInfoRequestAPI *posterQRAPI = [[FriendCircleCommodityPosterQrInfoRequestAPI alloc] initFriendCircleCommodityPosterQrRequest:params];
        [posterQRAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responObj = request.responseJSONObject;
            if ([responObj objectForKey:@"qr"]) {
                NSArray *qrArr =  [responObj objectForKey:@"qr"];
                callBlock([qrArr firstObject]);
            }
            [YYCenterLoading hideCenterLoadingInWindow];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            callBlock(nil);
            [YYCenterLoading hideCenterLoadingInWindow];
        }];
    }
    else {
        FriendCircleCommodityQRPhotoInfoRequestAPI *goodQRAPI = [[FriendCircleCommodityQRPhotoInfoRequestAPI alloc] initFriendCircleCommodityQRPhotosRequest:params];
        [goodQRAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responObj = request.responseJSONObject;
            if ([responObj objectForKey:@"qr"]) {
                NSArray *qrArr =  [responObj objectForKey:@"qr"];
                callBlock([qrArr firstObject]);
            }
            [YYCenterLoading hideCenterLoadingInWindow];
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            callBlock(nil);
            [YYCenterLoading hideCenterLoadingInWindow];
        }];
    }
}

/**多图生成二维码图片**/
+ (void)commodityMultiQRPhotoWith:(NSMutableArray *)originUrls
                                     indexArr:(NSMutableArray *)indexArr
                                  needTranArr:(NSMutableArray *)needTranArr
                        callBlock:(void (^)(NSMutableArray *))callBlock{
    __block NSMutableArray *tempOriginArr = originUrls;
    __block NSMutableArray *tempIndexArr = indexArr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:needTranArr forKey:@"items"];
    FriendCircleCommodityQRPhotoInfoRequestAPI *goodQRAPI = [[FriendCircleCommodityQRPhotoInfoRequestAPI alloc] initFriendCircleCommodityQRPhotosRequest:params];
    [goodQRAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responObj = request.responseJSONObject;
        if ([responObj objectForKey:@"qr"]) {
            BOOL isEnd = NO;
            NSArray *qrArrs = [responObj objectForKey:@"qr"];
            //对指定位置的URL进行替换
            for (int j = 0; j < tempIndexArr.count; j ++) {
                // 得到索引值
                int value = [[tempIndexArr objectAtIndex:j] intValue];
                if (j < qrArrs.count) {
                    [tempOriginArr replaceObjectAtIndex:value withObject:[qrArrs objectAtIndex:j]];
                }
                if (j == tempIndexArr.count - 1) {
                    isEnd = YES;
                }
            }
            if (isEnd) {
                callBlock(tempOriginArr);
            }
            [YYQRCodeCenterLoading hideCenterLoading:nil
                                           superView:nil];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        callBlock(nil);
        [YYQRCodeCenterLoading hideCenterLoading:nil
                                       superView:nil];
    }];
}

+ (void)showShareComponent:(BOOL)isSingle
                 callBlock:(void (^)(NSIndexPath *index, YYPopShareView *view))callBlock{
    BOOL isSinglePhoto = isSingle;
    NSInteger count = (isSinglePhoto==YES?2:3);
    NSMutableArray *titleArr = [NSMutableArray array];
    NSMutableArray *listData = [NSMutableArray array];
    NSMutableArray *imageNameArr = [NSMutableArray array];
    NSMutableDictionary *headerParams = [NSMutableDictionary dictionary];
    
    YYPopShareHeaderModel *headerModel = [[YYPopShareHeaderModel alloc] init];
    headerModel.title = @"分享到";
    [headerParams setObject:headerModel forKey:@"headerModel"];
    
    if (isSinglePhoto) {
        titleArr = [@[@"微信好友",@"朋友圈"] mutableCopy];
        imageNameArr = [@[@"wechat_icon",@"friend_circle_icon"] mutableCopy];
    }
    else {
        titleArr = [@[@"微信好友",@"一键保存图片",@"一键发圈"] mutableCopy];
        imageNameArr = [@[@"wechat_icon",@"save_icon",@"friend_circle_icon"] mutableCopy];
    }
    for (int i = 0; i < count; i ++) {
        YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] init];
        model.title = [titleArr objectAtIndex:i];
        model.imageUrl = [imageNameArr objectAtIndex:i];
        model.identifierName = @"YYPopShareItemView";
        [listData addObject:model];
    }
    
    YYPopShareView *shareView = [[YYPopShareView alloc] initWithPopShareView:headerParams listData:listData];
    @weakify(shareView);
    shareView.callBlock = ^(NSIndexPath * _Nonnull indexPath) {
        @strongify(shareView);
        NSInteger item = indexPath.item;
        if (item != 0) {
            callBlock(indexPath, shareView);
        }
        else {
            [YYCenterLoading hideCenterLoadingInWindow];
            [YYQRCodeCenterLoading hideCenterLoading:nil
                                           superView:nil];
            [shareView popHide];
        }
    };
    [shareView popShow];
}

/**显示打开微信框**/
+ (void)showOpenWechat {
    NSInteger count = 1;
    NSMutableArray *titleArr = [NSMutableArray array];
    NSMutableArray *listData = [NSMutableArray array];
    NSMutableArray *imageNameArr = [NSMutableArray array];
    NSMutableDictionary *headerParams = [NSMutableDictionary dictionary];
    
    YYPopShareHeaderModel *headerModel = [[YYPopShareHeaderModel alloc] init];
    headerModel.imageUrl = @"circle_toshare_icon";
    headerModel.height = kSizeScale(116);
    headerModel.width = kScreenW;
    [headerParams setObject:headerModel forKey:@"headerModel"];
    
    titleArr = [@[@"打开微信"] mutableCopy];
    imageNameArr = [@[@"wechat_icon"] mutableCopy];
    
    for (int i = 0; i < count; i ++) {
        YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] init];
        model.title = [titleArr objectAtIndex:i];
        model.imageUrl = [imageNameArr objectAtIndex:i];
        model.identifierName = @"YYPopShareItemView";
        [listData addObject:model];
    }
    
    YYPopShareView *shareView = [[YYPopShareView alloc] initWithPopShareView:headerParams listData:listData];
    @weakify(shareView);
    shareView.callBlock = ^(NSIndexPath * _Nonnull indexPath) {
        @strongify(shareView);
        NSInteger item = indexPath.item;
        if (item == 1) {
            [YYCommonTools openScheme:@"weixin://"];
        }
        [shareView popHide];
    };
    [shareView popShow];
}

@end

