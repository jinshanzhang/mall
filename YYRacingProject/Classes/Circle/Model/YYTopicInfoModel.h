//
//  YYTopicInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYTopicInfoModel : NSObject

@property (nonatomic, assign) int topicId; //话题ID
@property (nonatomic, strong) NSString *imageUrl; //话题背景
@property (nonatomic, strong) NSString *title;    //话题标题

@end

NS_ASSUME_NONNULL_END
