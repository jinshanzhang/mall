//
//  YYCircleModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/26.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYCircleModel.h"


@implementation YYTagModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"type" : @"tagType",
             @"url" : @"tagUrl"
             };
}

@end


@implementation YYPictureModel


@end


@implementation YYFavorUserModel


@end


@implementation YYCircleModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"circleID" : @"id", 
             @"tagItems" : @"tags",
             @"pictureItems" : @"items",
             @"favorUserItems" : @"favorAvatars"
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"tagItems" : [YYTagModel class],
             @"pictureItems" : [YYPictureModel class],
             @"favorUserItems" : [YYFavorUserModel class]
             };
}

@end
