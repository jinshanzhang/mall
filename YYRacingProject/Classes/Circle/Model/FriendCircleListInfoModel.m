//
//  FriendCircleListInfoModel.m
//  YIYanProject
//
//  Created by cjm on 2018/6/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleListInfoModel.h"

#import "YYImageInfoModel.h"
#import "HomeTagsInfoModel.h"

@implementation FriendCircleListInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"imageList":@"items",
             @"zanList":@"favorAvatars",
             @"momentID":@"id"
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             @"imageList":[YYImageInfoModel class],
             @"tags":[HomeTagsInfoModel class],
             @"zanList":[FriendCircleFavorAvatarInfoModel class]
             };
}

@end


@implementation FriendCircleFavorAvatarInfoModel

@end

