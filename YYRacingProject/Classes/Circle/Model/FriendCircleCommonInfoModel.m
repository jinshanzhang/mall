//
//  FriendCircleCommonInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleCommonInfoModel.h"

@implementation FriendCircleCommonInfoModel
/**
 *  蜀黍之家圈点赞
 */
+ (void)circleClickZanOperatorHandler:(UITableView *)tableView
                             listData:(NSMutableArray *)listData
                          currentCell:(FriendCircelListInfoCell *)cell
                         successBlock:(SuccessBlock)successBlock {
    if ((kUserInfo.useDefaultNick == 1 || kUserInfo.useDefaultAvatar == 1)) {
        NSString *message = @"您还没有设置头像昵称\n快去设置吧~";
        if (kUserInfo.useDefaultAvatar == 1) {
            message = @"您还没有设置头像\n快去设置吧~";
        }
        else if (kUserInfo.useDefaultNick == 1) {
            message = @"您还没有设置昵称\n快去设置吧~";
        }
        [JCAlertView showTwoButtonsWithTitle:@"提示"
                                     Message:message
                                  ButtonType:JCAlertViewButtonTypeCancel
                                 cancelColor:XHBlackColor
                                 ButtonTitle:@"取消"
                                       Click:^{
            
                                        }
                                  ButtonType:JCAlertViewButtonTypeDefault
                                confirmColor:XHRedColor
                                 ButtonTitle:@"确认"
                                       Click:^{
                                           [YYCommonTools skipMineSet:(YYBaseViewController *)[YYCommonTools getCurrentVC] isHideTabbar:YES];
                                       } type:JCAlertViewTypeDefault];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    __block NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    __block FriendCircleListInfoModel *model = [listData objectAtIndex:indexPath.row];
    __block BOOL  isFavor = !model.hasFavor; //默认是没有选中， 点击一下做取反操作。
    
    [params setObject:@(isFavor) forKey:@"action"];
    [params setObject:@(model.momentID) forKey:@"momentId"];
    
    [YYCenterLoading showCenterLoading];
    
    FriendCircleListZanInfoRequestAPI *zanRequest = [[FriendCircleListZanInfoRequestAPI  alloc] initFriendCircleAddAndCancelZanRequest:params];
    [zanRequest startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responObject = request.responseJSONObject;
        
        [YYCenterLoading hideCenterLoading];
        
        if (kValidDictionary(responObject)) {
            NSString *userHeader = [responObject objectForKey:@"avatarUrl"];
            model.hasFavor = isFavor;
            if (isFavor) {
                //添加
                model.favorCnt += 1;
                FriendCircleFavorAvatarInfoModel *favorModel = [[FriendCircleFavorAvatarInfoModel alloc] init];
                favorModel.userId = kUserInfo.userId;
                favorModel.avatarUrl = userHeader;
                [model.zanList appendObject:favorModel];
            }
            else {
                //移除
                model.favorCnt -= 1;
                __block NSInteger index = -1;
                [model.zanList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    FriendCircleFavorAvatarInfoModel *avatarModel = (FriendCircleFavorAvatarInfoModel *)obj;
                    if ([avatarModel.userId isEqualToString:kUserInfo.userId]) {
                        index = idx;
                        *stop = YES;
                    }
                }];
                if (index != -1) {
                    [model.zanList removeObjectAtIndex:index];
                }
            }
            if (successBlock) {
                successBlock(indexPath, model);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 *  蜀黍之家圈分享
 */
+ (void)circleClickShareOperatorHandler:(UITableView *)tableView
                               listData:(NSMutableArray *)listData
                            currentCell:(FriendCircelListInfoCell *)cell
                           successBlock:(SuccessBlock)successBlock {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    __block NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    __block FriendCircleListInfoModel *model = [listData objectAtIndex:indexPath.row];
    
    [params setObject:@(1) forKey:@"action"];
    [params setObject:@(model.momentID) forKey:@"momentId"];
    
    FriendCircleListShareSumInfoRequestAPI *shareAPI = [[FriendCircleListShareSumInfoRequestAPI alloc] initFriendCircleShareSumRequest:params];
    [shareAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responObject = request.responseJSONObject;
        if (kValidDictionary(responObject)) {
            model.shareCnt ++;
            if (successBlock) {
                successBlock(indexPath, model);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}
/**
 *  实时生成商品二维码图片
 */
+ (void)commodityGoodQROperatorHandler:(UITableView *)tableView
                              listData:(NSMutableArray *)listData
                           currentCell:(FriendCircelListInfoCell *)cell
                          successBlock:(SuccessResBlock)successBlock {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    NSMutableArray *content = [NSMutableArray array];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    __block NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    __block FriendCircleListInfoModel *model = [listData objectAtIndex:indexPath.row];
    
    YYImageInfoModel *tempModel = [model.imageList firstObject];
    [dict setObject:@(tempModel.imageId) forKey:@"imageId"];
    [dict setObject:tempModel.shellUri forKey:@"shellUri"];
    [content addObject:dict];
    
    [params setObject:content forKey:@"items"];
    
    FriendCircleCommodityQRPhotoInfoRequestAPI *qrAPI = [[FriendCircleCommodityQRPhotoInfoRequestAPI alloc] initFriendCircleCommodityQRPhotosRequest:params];
    [qrAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responObject = request.responseJSONObject;
        if (kValidDictionary(responObject)) {
            if (successBlock) {
                successBlock(indexPath, model, responObject);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**
 *  实时生成邀约二维码海报图
 */
+ (void)posterCommodityQROperatorHandler:(UITableView *)tableView
                                listData:(NSMutableArray *)listData
                             currentCell:(FriendCircelListInfoCell *)cell
                            successBlock:(SuccessResBlock)successBlock {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    __block NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    __block FriendCircleListInfoModel *model = [listData objectAtIndex:indexPath.row];
    
    YYImageInfoModel *tempModel = [model.imageList firstObject];
    
    NSMutableArray *content = [NSMutableArray array];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(tempModel.imageId) forKey:@"imageId"];
    [content addObject:dict];
    
    [params setObject:content forKey:@"items"];
    
    FriendCircleCommodityPosterQrInfoRequestAPI *qrAPI = [[FriendCircleCommodityPosterQrInfoRequestAPI alloc] initFriendCircleCommodityPosterQrRequest:params];
    [qrAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responObject = request.responseJSONObject;
        if (kValidDictionary(responObject)) {
            if (successBlock) {
                successBlock(indexPath, model, responObject);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}

/** 对图片需要合成二维码的进行操作 **/
+ (void)transformNeedCommodityQRPhotos:(NSMutableArray *)photos successBlock:(void(^)(NSMutableArray *))successBlock {
    //存储需要生成二维码的坐标。
    __block NSMutableArray *qrIndexs = [NSMutableArray array];
    //存储图片的URL
    __block NSMutableArray *photosURL = [NSMutableArray array];
    //存储需要后端生成的图片信息
    __block NSMutableArray *commodityPhotos = [NSMutableArray array];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [photos enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        YYImageInfoModel *tempModel = (YYImageInfoModel *)obj;
        if (tempModel.type == 1) {  //是否是商品，type = 1表示是商品
            [qrIndexs addObject:@(idx)];
            // 需要生成二维码的位置，和数据。
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setObject:@(tempModel.imageId) forKey:@"imageId"];
            [dict setObject:tempModel.shellUri forKey:@"shellUri"];
            [commodityPhotos addObject:dict];
        }
        [photosURL addObject:tempModel.imageUrl];
    }];
    
    if (kValidArray(commodityPhotos)) {
        [YYQRCodeCenterLoading showCenterLoading:@"二维码生成中..."
                                       superView:kAppWindow];
        [params setObject:commodityPhotos forKey:@"items"];
        FriendCircleCommodityQRPhotoInfoRequestAPI *qrAPI = [[FriendCircleCommodityQRPhotoInfoRequestAPI alloc] initFriendCircleCommodityQRPhotosRequest:params];
        [qrAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responObject = request.responseJSONObject;
            if (kValidDictionary(responObject)) {
                
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            successBlock(nil);
        }];
    }
    else {
        [YYCenterLoading showCenterLoadingInWindow];
        successBlock(photosURL);
    }
}


@end
