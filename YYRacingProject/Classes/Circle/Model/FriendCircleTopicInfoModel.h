//
//  FriendCircleTopicInfoModel.h
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendCircleTopicInfoModel : NSObject

@property (nonatomic, assign) int topicId; //话题ID
@property (nonatomic, strong) NSString *imageUrl; //话题背景
@property (nonatomic, strong) NSString *title;    //话题标题

@end
