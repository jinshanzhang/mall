//
//  FriendCircleCommonInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/16.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FriendCircelListInfoCell.h"
#import "FriendCircleListInfoModel.h"

#import "YY_MineSetViewController.h"

#import "FriendCircleListZanInfoRequestAPI.h"
#import "FriendCircleListShareSumInfoRequestAPI.h"
#import "FriendCircleCommodityQRPhotoInfoRequestAPI.h"
#import "FriendCircleCommodityPosterQrInfoRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^SuccessBlock) (NSIndexPath *selectIndex, id selectModel);
typedef void (^SuccessResBlock) (NSIndexPath *selectIndex, id selectModel, id responDict);

@interface FriendCircleCommonInfoModel : NSObject

/**
 *  蜀黍之家圈点赞
 */
+ (void)circleClickZanOperatorHandler:(UITableView *)tableView
                             listData:(NSMutableArray *)listData
                          currentCell:(FriendCircelListInfoCell *)cell
                         successBlock:(SuccessBlock)successBlock;

/**
 *  蜀黍之家圈分享
 */
+ (void)circleClickShareOperatorHandler:(UITableView *)tableView
                               listData:(NSMutableArray *)listData
                            currentCell:(FriendCircelListInfoCell *)cell
                           successBlock:(SuccessBlock)successBlock;
/**
 *  实时生成商品二维码图片
 */
+ (void)commodityGoodQROperatorHandler:(UITableView *)tableView
                                listData:(NSMutableArray *)listData
                             currentCell:(FriendCircelListInfoCell *)cell
                            successBlock:(SuccessResBlock)successBlock;


/**
 *  实时生成邀约二维码海报图
 */
+ (void)posterCommodityQROperatorHandler:(UITableView *)tableView
                                listData:(NSMutableArray *)listData
                             currentCell:(FriendCircelListInfoCell *)cell
                            successBlock:(SuccessResBlock)successBlock;


/** 对图片需要合成二维码的进行操作 **/
+ (void)transformNeedCommodityQRPhotos:(NSMutableArray *)photos
                                      successBlock:(void(^)(NSMutableArray *))successBlock;
@end

NS_ASSUME_NONNULL_END
