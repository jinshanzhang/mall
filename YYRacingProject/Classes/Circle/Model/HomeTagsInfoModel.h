//
//  HomeTagsInfoModel.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeTagsInfoModel : NSObject

//当前类别id
@property (nonatomic, assign) int categoryId;

//tab id
@property (nonatomic, assign) int tabId;

//导航栏名
@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *title;

//导航栏图片url
@property (nonatomic, copy) NSString *imageUrl;

//备注
@property (nonatomic, copy) NSString *comment;
//父目录id
@property (nonatomic, assign) int parentCategoryId;
//Tab类型 1-首页精选 2-首页商品分类 3-其他待扩展 4-9.9精选 5-9.9商品分类
@property (nonatomic, assign) int tabType;
@property (nonatomic, assign) int tagType;
@property (nonatomic, copy) NSString *tagUrl;
@end
