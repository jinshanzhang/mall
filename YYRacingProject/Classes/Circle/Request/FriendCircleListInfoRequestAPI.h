//
//  FriendCircleListInfoRequestAPI.h
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface FriendCircleListInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initFriendCircleListRequest:(NSMutableDictionary *)params;

@end
