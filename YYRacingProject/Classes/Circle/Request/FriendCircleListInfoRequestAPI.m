//
//  FriendCircleListInfoRequestAPI.m
//  YIYanProject
//
//  Created by cjm on 2018/6/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleListInfoRequestAPI.h"

@implementation FriendCircleListInfoRequestAPI

- (instancetype)initFriendCircleListRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CIRCLECONTENT];
}


@end
