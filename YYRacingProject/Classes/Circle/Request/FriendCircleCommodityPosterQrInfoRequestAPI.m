//
//  FriendCircleCommodityPosterQrInfoRequestAPI.m
//  YIYanProject
//
//  Created by cjm on 2018/7/5.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleCommodityPosterQrInfoRequestAPI.h"

@implementation FriendCircleCommodityPosterQrInfoRequestAPI

- (instancetype)initFriendCircleCommodityPosterQrRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CIRCLEQRUSER];
}
@end
