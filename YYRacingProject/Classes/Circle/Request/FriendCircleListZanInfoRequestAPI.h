//
//  FriendCircleListZanInfoRequestAPI.h
//  YIYanProject
//
//  Created by cjm on 2018/6/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface FriendCircleListZanInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initFriendCircleAddAndCancelZanRequest:(NSMutableDictionary *)params;

@end
