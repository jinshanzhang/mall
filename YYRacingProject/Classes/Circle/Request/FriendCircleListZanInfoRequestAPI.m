//
//  FriendCircleListZanInfoRequestAPI.m
//  YIYanProject
//
//  Created by cjm on 2018/6/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleListZanInfoRequestAPI.h"

@implementation FriendCircleListZanInfoRequestAPI

- (instancetype)initFriendCircleAddAndCancelZanRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CIRCLEFAVOR];
}

@end
