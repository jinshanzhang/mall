//
//  FriendCircleCommodityPosterQrInfoRequestAPI.h
//  YIYanProject
//
//  Created by cjm on 2018/7/5.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface FriendCircleCommodityPosterQrInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initFriendCircleCommodityPosterQrRequest:(NSMutableDictionary *)params;

@end
