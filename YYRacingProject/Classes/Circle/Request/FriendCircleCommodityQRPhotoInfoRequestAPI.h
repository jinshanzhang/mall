//
//  FriendCircleCommodityQRPhotoInfoRequestAPI.h
//  YIYanProject
//
//  Created by cjm on 2018/6/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"
// 实时生成二维码
@interface FriendCircleCommodityQRPhotoInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initFriendCircleCommodityQRPhotosRequest:(NSMutableDictionary *)params;

@end
