//
//  FriendCircleCommodityQRPhotoInfoRequestAPI.m
//  YIYanProject
//
//  Created by cjm on 2018/6/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "FriendCircleCommodityQRPhotoInfoRequestAPI.h"

@implementation FriendCircleCommodityQRPhotoInfoRequestAPI

- (instancetype)initFriendCircleCommodityQRPhotosRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CIRCLEQRCOMMODDITY];
}

@end
