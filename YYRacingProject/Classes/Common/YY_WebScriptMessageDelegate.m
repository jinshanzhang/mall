//
//  YY_WebScriptMessageDelegate.m
//  YIYanProject
//
//  Created by cjm on 2018/6/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_WebScriptMessageDelegate.h"
@implementation YY_WebScriptMessageDelegate

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate {
    self = [super init];
    if (self) {
        _scriptDelegate = scriptDelegate;
    }
    return self;
}

- (void)userContentController:(WKUserContentController*)userContentController didReceiveScriptMessage:(WKScriptMessage*)message{
    [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
}


@end
