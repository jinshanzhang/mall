//
//  YY_WebScriptMessageDelegate.h
//  YIYanProject
//
//  Created by cjm on 2018/6/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@interface YY_WebScriptMessageDelegate : NSObject<WKScriptMessageHandler>

@property (nonatomic, weak) id<WKScriptMessageHandler> scriptDelegate;

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;

@end
