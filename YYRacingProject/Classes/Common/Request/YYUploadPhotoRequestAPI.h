//
//  YYUploadPhotoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YTKRequest.h"
#import <AFNetworking.h>

@interface YYUploadPhotoRequestAPI : YYBaseRequestAPI

- (instancetype)initUploadPhotoInfoRequest:(NSMutableDictionary *)params;

@end
