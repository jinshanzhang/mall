//
//  YYShareGoodQRCodeInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYShareGoodQRCodeInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initShareGoodQRCodeRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
