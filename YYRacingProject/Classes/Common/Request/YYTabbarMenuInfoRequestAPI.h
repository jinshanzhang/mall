//
//  YYTabbarMenuInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYTabbarMenuInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initTabbarMenuRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
