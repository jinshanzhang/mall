//
//  YYCommonResourceAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYCommonResourceAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;


@end
