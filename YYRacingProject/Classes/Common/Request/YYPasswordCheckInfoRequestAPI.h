//
//  YYPasswordCheckInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/1.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYPasswordCheckInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initPasswordCheckRequest:(NSMutableDictionary *)params; 

@end

NS_ASSUME_NONNULL_END
