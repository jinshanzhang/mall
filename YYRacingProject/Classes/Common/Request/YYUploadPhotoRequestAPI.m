//
//  YYUploadPhotoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYUploadPhotoRequestAPI.h"

@interface YYUploadPhotoRequestAPI()

@end

@implementation YYUploadPhotoRequestAPI

- (instancetype)initUploadPhotoInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.params = params;
    }
    return self;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,UPLOADPHOTO];
}

- (AFConstructingBlock)constructingBodyBlock {
    return ^(id <AFMultipartFormData> formData) {
        if (kValidDictionary(self.params)) {
            NSMutableArray *imageArrs = [self.params objectForKey:@"photos"];
            for (int i = 0; i < imageArrs.count; i ++) {
                NSString *fromKey = @"imgs";
                NSString *type = @"image/jpeg";
                UIImage *tempImage = imageArrs[i];
                NSData *data = UIImageJPEGRepresentation(tempImage, 0.9);
                NSString *name = [NSString stringWithFormat:@"image%d.jpg", i];
                [formData appendPartWithFileData:data name:fromKey fileName:name mimeType:type];
            }
        }
    };
}
@end
