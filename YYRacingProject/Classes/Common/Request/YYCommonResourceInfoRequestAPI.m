//
//  YYCommonResourceInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCommonResourceInfoRequestAPI.h"

@implementation YYCommonResourceInfoRequestAPI

- (instancetype)initCommonResourceRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COMMONRESOURCE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
