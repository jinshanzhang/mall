//
//  YYTabbarMenuInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYTabbarMenuInfoRequestAPI.h"

@implementation YYTabbarMenuInfoRequestAPI

- (instancetype)initTabbarMenuRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,MENUTABBAR];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
