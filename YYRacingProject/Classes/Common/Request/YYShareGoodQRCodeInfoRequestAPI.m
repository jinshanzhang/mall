//
//  YYShareGoodQRCodeInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/21.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYShareGoodQRCodeInfoRequestAPI.h"

@implementation YYShareGoodQRCodeInfoRequestAPI

- (instancetype)initShareGoodQRCodeRequest:(NSMutableDictionary *)params {
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH2,PRODUCEQRCODE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
