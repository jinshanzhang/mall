//
//  YYPasswordCheckInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/1.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYPasswordCheckInfoRequestAPI.h"

@implementation YYPasswordCheckInfoRequestAPI

- (instancetype)initPasswordCheckRequest:(NSMutableDictionary *)params {
    if (self) {
        self.inParams = params;
    }
    return self;
}


- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,PASSWORDCHECK];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
