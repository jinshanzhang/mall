//
//  YYCommonResourceAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCommonResourceAPI.h"

@implementation YYCommonResourceAPI


- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.params = params;
    }
    return self;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (NSString *)requestUrl {
    NSString *type = nil;
    if (kValidDictionary(self.params)) {
        type = [self.params objectForKey:@"type"];
        [self.params removeObjectForKey:@"type"];
    }
    return [NSString stringWithFormat:@"%@%@?type=%@",INTERFACE_PATH,COMMONSOURCE,type];}
@end
