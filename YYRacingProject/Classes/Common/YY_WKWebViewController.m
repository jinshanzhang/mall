//
//  YY_WKWebViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_WKWebViewController.h"
#import <WebKit/WebKit.h>

#import "YYWebBottomShareView.h"
#import "YY_WebScriptMessageDelegate.h"
#import "WebChatPayH5View.h"

@interface YY_WKWebViewController ()
<
WKUIDelegate,
WKNavigationDelegate,
WKScriptMessageHandler>
{
    NSString *endPayRedirectURL;
}

@property (nonatomic, weak)   WKWebView             *m_webView;
@property (nonatomic, weak)   UIProgressView        *progressView;

@property (nonatomic, strong) NSString              *url;
@property (nonatomic, strong) NSNumber              *backRoot;  // 是否直接返回app
@property (nonatomic, strong) NSNumber              *pageType;  // 页面类型

@property (nonatomic, strong) YYWebBottomShareView  *shareView;

@property (nonatomic, assign) BOOL payWx;

@end

@implementation YY_WKWebViewController

#pragma mark - Setter

#pragma mark - Life cycle
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if (kValidDictionary(self.parameter)) {
        // pageType = 1, 为粉丝开店页面 , pageType = 2, 签到页面, pageType = 3, 首页web
        if ([self.parameter objectForKey:@"pageType"]) {
            self.pageType = [self.parameter objectForKey:@"pageType"];
        }
        if ([self.parameter objectForKey:@"url"]) {
            self.url = [self.parameter objectForKey:@"url"];
            self.url = [NSString removeBlankAndLine:self.url];
        }
        if ([self.parameter objectForKey:@"uri"]) {
            self.url = [self.parameter objectForKey:@"uri"];
            self.url = [NSString removeBlankAndLine:self.url];
            
        }
        // 是否返回按钮直接返回
        if ([self.parameter objectForKey:@"backRoot"]) {
            self.backRoot = [self.parameter objectForKey:@"backRoot"];
        }
    }
//    self.url = @"http://192.168.0.75:8082/#/test";

    // 偏好设置， 涉及js交互
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.preferences = [[WKPreferences alloc] init];
    config.preferences.javaScriptEnabled = YES;
    config.preferences.javaScriptCanOpenWindowsAutomatically = YES;
    config.processPool = [[WKProcessPool alloc] init];
    config.allowsInlineMediaPlayback = YES;
    config.processPool = [[WKProcessPool alloc] init];
    
    //通过JS与webview内容交互
    WKUserContentController *content = [[WKUserContentController alloc]init];
    
    YY_WebScriptMessageDelegate *scriptDelegate = [[YY_WebScriptMessageDelegate alloc] initWithDelegate:self];
    [content addScriptMessageHandler:scriptDelegate name:@"toDetail"];
    [content addScriptMessageHandler:scriptDelegate name:@"toShare"];
    [content addScriptMessageHandler:scriptDelegate name:@"toPreviewPay"];
    [content addScriptMessageHandler:scriptDelegate name:@"getToken"];
    [content addScriptMessageHandler:scriptDelegate name:@"saveCode"];
    [content addScriptMessageHandler:scriptDelegate name:@"backUpperPage"];//返回上一个页面
    [content addScriptMessageHandler:scriptDelegate name:@"goMaterial"];

    [content addScriptMessageHandler:scriptDelegate name:@"getTokenWithoutAction"];
    config.userContentController = content;
    
    WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    webView.UIDelegate = self;
    webView.scrollView.bounces = NO;
    webView.allowsBackForwardNavigationGestures = YES;
    webView.navigationDelegate = self;
    webView.backgroundColor = HexRGB(0xfff9f8);
    [self.view addSubview:webView];

    self.m_webView = webView;
    
    [webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(([self.pageType integerValue] == 2?kStatuH:([self.pageType integerValue] == 3?0:kNavigationH)));
    }];
    
    if ([self.pageType integerValue] == 1) {
        // 页面切换刷新
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]];
        [self.m_webView loadRequest:request];
    }
    
    UIProgressView *progreeView = [[UIProgressView alloc] initWithFrame:CGRectZero];
    progreeView.tintColor = XHMainColor;
    //设置进度条的高度，下面这句代码表示进度条的宽度变为原来的1倍，高度变为原来的1.5倍.
    progreeView.progress = 0.05;
    progreeView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
    [self.m_webView addSubview:progreeView];
    self.progressView = progreeView;
    
    [progreeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.m_webView);
        make.height.mas_equalTo(2);
    }];
    
    [webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    [webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL];

    if ([self.pageType integerValue] != 1) {
        [self xh_addNavigationItemWithImageName:@"detail_navbar_back"
                                         isLeft:YES
                                     clickEvent:^(UIButton *sender) {
                                         if ([self.backRoot boolValue]) {
                                             [self.navigationController popViewControllerAnimated:YES];
                                         }
                                         else {
                                             if (webView.canGoBack == YES) {
                                                 [webView goBack];
                                             }
                                             else {
                                                 [self.navigationController popViewControllerAnimated:YES];
                                             }
                                         }
                                     }];
        if ([self.pageType integerValue] == 2) {
            [self xh_hideNavigation:YES];
        }
    }
    //是否清除缓存，重新加载
    [self clearWebCache];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]];
    [webView loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private method
/**清楚缓存**/
- (void)clearWebCache {
    if (@available(iOS 9.0,*)) {
        NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
        NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
        [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes
                                                   modifiedSince:dateFrom completionHandler:^{
                                                       NSLog(@"清楚缓存");
                                                   }];
    }
    else {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        [[NSURLCache sharedURLCache] setDiskCapacity:0];
        [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"title"]) {
        if (object == self.m_webView) {
            if (kValidString(self.m_webView.title)) {
                [self xh_addTitle:self.m_webView.title];
            }
        }
    }
    else if ([keyPath isEqualToString:@"estimatedProgress"]) {
        self.progressView.progress = self.m_webView.estimatedProgress;
        if (self.progressView.progress == 1) {
            __weak typeof (self)weakSelf = self;
            [UIView animateWithDuration:0.25f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                weakSelf.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.4f);
            } completion:^(BOOL finished) {
                weakSelf.progressView.hidden = YES;
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
//    self.progressView.hidden = NO;
//    //[YYCenterLoading showCenterLoading];
//    //开始加载网页的时候将progressView的Height恢复为1.5倍
//    self.progressView.transform = CGAffineTransformMakeScale(1.0f, 1.5f);
//    //防止progressView被网页挡住
//    [self.view bringSubviewToFront:self.progressView];
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
}

// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    self.progressView.hidden = YES;
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    self.progressView.hidden = YES;
}

#define XDX_URL_TIMEOUT 30
static const NSString *CompanyFirstDomainByWeChatRegister = @"h5.10010.wiki";

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
      NSString *url = navigationAction.request.URL.absoluteString;
      if ([url containsString:@"https://wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb?"]) {
          WebChatPayH5View *h5View = [[WebChatPayH5View alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
          //url是没有拼接redirect_url微信h5支付链接
          [h5View loadingURL:url withIsWebChatURL:NO];
          [self.view addSubview:h5View];
          decisionHandler(WKNavigationActionPolicyCancel);
      }
      //加载支付宝支付
      else
      {
          if ([navigationAction.request.URL.scheme isEqualToString:@"alipay"]) {
              //  1.以？号来切割字符串
              NSArray *urlBaseArr = [navigationAction.request.URL.absoluteString componentsSeparatedByString:@"?"];
              NSString *urlBaseStr = urlBaseArr.firstObject;
              NSString *urlNeedDecode = urlBaseArr.lastObject;
              //  2.将截取以后的Str，做一下URLDecode，方便我们处理数据
              NSMutableString *afterDecodeStr = [NSMutableString stringWithString:[self decoderUrlEncodeStr:urlNeedDecode]];
              //  3.替换里面的默认Scheme为自己的Scheme
              NSString *afterHandleStr = [afterDecodeStr stringByReplacingOccurrencesOfString:@"alipays" withString:@"h5.10010.wiki"];
              
              //  4.然后把处理后的，和最开始切割的做下拼接，就得到了最终的字符串
              NSString *finalStr = [NSString stringWithFormat:@"%@?%@",urlBaseStr, [self urlEncodeStr:afterHandleStr]];
              dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                  //  判断一下，是否安装了支付宝APP（也就是看看能不能打开这个URL）
                  if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:finalStr]]) {
                      //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:finalStr]];
                      [self applicationOpenUrl:[NSURL URLWithString:finalStr]];
                  } else {
                      //未安装支付宝, 自行处理
                      NSString *url = @"itms://itunes.apple.com/cn/app/支付宝-让生活更简单/id333206289?mt=8";
                      url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                      [self applicationOpenUrl:[NSURL URLWithString:url]];
                  }
              });
              decisionHandler(WKNavigationActionPolicyCancel);
          }
          else
          {
              decisionHandler(WKNavigationActionPolicyAllow);
          }
      }
}

- (void)applicationOpenUrl:(NSURL *)url {
    UIApplication *application = [UIApplication sharedApplication];
    if([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:url options:@{}
           completionHandler:^(BOOL success) {
            NSLog(@"Open %@: %d",url,success);
        }];
    }else{
        BOOL success = [application openURL:url];
        NSLog(@"Open %@: %d",url,success);
    }
}

- (NSString *)xh_URLDecodedString:(NSString *)urlString {
    NSString *string = urlString;
    NSString *decodedString=(__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (__bridge CFStringRef)string, CFSTR(""), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    return decodedString;
}

- (NSString *)xh_URLEncodedString:(NSString *)urlString {
    NSString *string = urlString;
    NSString *encodedString = (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                                                     (CFStringRef)string,
                                                                                                     NULL,
                                                                                                     (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                     kCFStringEncodingUTF8));
    return encodedString;
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    @weakify(self);
    NSString *messageKey = message.name;
    // 返回上一层页面
    if ([messageKey isEqualToString:@"backUpperPage"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    // 保存二维码
    if ([messageKey isEqualToString:@"saveCode"]) {
        NSDictionary *messageBody = [NSDictionary dictionaryWithDictionary:message.body];
        NSString *imgUrl = [messageBody objectForKey:@"imgUrl"];
        if (kValidString(imgUrl)) {
            [kSaveManager savePhotoReachAlbum:[@[imgUrl] mutableCopy]];
        }
    }
    // 跳到商品详情。
    if ([messageKey isEqualToString:@"toDetail"]) {
        NSDictionary *messageBody = [NSDictionary dictionaryWithDictionary:message.body];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if ([messageBody objectForKey:@"url"]) {
            [params setObject:[messageBody objectForKey:@"url"] forKey:@"url"];
        }
        if ([messageBody objectForKey:@"linkType"]) {
            [params setObject:[messageBody objectForKey:@"linkType"] forKey:@"linkType"];
        }
        [YYCommonTools skipMultiCombinePage:self
                                     params:params];
    }
    // 弹出分享
    if ([messageKey isEqualToString:@"toShare"]) {
        BOOL  isSkipBorad = NO;
        NSNumber *withBorad = nil;
        //withBorad 0 不换起面板(或者null)  1 换起来
        // 0 好友   1 朋友圈    2  QQ好友     3 QQ空间     4 保存图片    5复制链接
        __block NSMutableArray *shareIcons = [NSMutableArray array];
        __block NSMutableArray *shareTitles = [NSMutableArray array];
        __block NSMutableArray *umShareArray = [NSMutableArray array];
        NSDictionary *messageBody = [NSDictionary dictionaryWithDictionary:message.body];
        if ([messageBody objectForKey:@"withBoard"]) {
            withBorad = [messageBody objectForKey:@"withBoard"];
        }
        if (withBorad) {
            if ([withBorad integerValue] == 1) {
                isSkipBorad = YES;
            }
        }
        NSDictionary *shareDict = [messageBody objectForKey:@"shareBodys"];
        NSArray *shareContents = [NSArray modelArrayWithClass:[YYShareInfoModel class] json:shareDict];
        [shareContents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger type = -1;
            BOOL  isShareLink = NO;
            NSString  *icon, *title = nil;
            YYShareInfoModel *shareModel = (YYShareInfoModel *)obj;
            type = shareModel.channel;
            if (type >= 5) { type = 5; }
            
            YYShareCombineInfoModel *umShareModel = [[YYShareCombineInfoModel alloc] init];
            umShareModel.title = kValidString(shareModel.shareTitle)?shareModel.shareTitle:nil;
            umShareModel.detailTitle = kValidString(shareModel.shareDesc)?shareModel.shareDesc:nil;
            umShareModel.webpageUrl = kValidString(shareModel.shareLinkUrl)?shareModel.shareLinkUrl:nil;
            umShareModel.thumbImage = kValidString(shareModel.shareImgUrl)?shareModel.shareImgUrl:nil;
            umShareModel.shareImage = kValidString(shareModel.shareImgUrl)?shareModel.shareImgUrl:nil;
            if (shareModel.shareType) {
                if ([shareModel.shareType integerValue] == 3) {
                    isShareLink = YES;
                }
                else if ([shareModel.shareType integerValue] == 4) {
                    isShareLink = YES;
                    umShareModel.isApplet = YES;
                    umShareModel.webpageUrl = @"https://m.ishellvip.com/#/home";
                    umShareModel.path = kValidString(shareModel.shareLinkUrl)?shareModel.shareLinkUrl:nil;
                }
            }
            umShareModel.isFriendShareLink = isShareLink;
            if (shareModel.shareImgUrl) {
                umShareModel.shareImageArray = [@[shareModel.shareImgUrl] mutableCopy];
            }
            umShareModel.operatorType = type;
            [umShareArray addObject:umShareModel];
            
            switch (shareModel.channel) {
                case 0: {
                    icon = @"wechat_icon"; title = @"微信好友";
                }
                    break;
                case 1: {
                    icon = @"friend_circle_icon"; title = @"朋友圈";
                }
                    break;
                case 4: {
                    icon = @"save_icon"; title = @"保存图片";
                }
                    break;
                case 5: {
                    icon = @"copy_link_icon"; title = @"复制链接";
                }
                    break;
                case 6: {
                    icon = @"share_password_icon"; title = @"分享口令";
                }
                    break;
                default:
                    break;
            }
            if (kValidString(icon)) {
                [shareIcons addObject:icon];
            }
            if (kValidString(title)) {
                [shareTitles addObject:title];
            }
        }];
        if (!isSkipBorad) {
            if (!kValidArray(umShareArray)) {
                [YYCommonTools showTipMessage:@"分享参数不能为空"];
                return;
            }
            YYShareCombineInfoModel *shareModel = [umShareArray firstObject];
            [kShareManager umengShareModel:shareModel
                                  callBack:^(BOOL status) {
                                      
                                  }];
        }
        else {
            self.shareView = [YYWebBottomShareView initYYPopView:[[YYWebBottomShareView alloc] initShowBottomView: shareIcons showTitles: shareTitles extend:[@{} mutableCopy]]];
            [self.shareView showViewOfAnimateType:YYPopAnimateDownUp];
            self.shareView.shareBlock = ^(NSInteger atIndex) {
                @strongify(self);
                YYLog(@"atIndex = %ld", atIndex);
                YYShareCombineInfoModel *shareModel = [umShareArray objectAtIndex:atIndex];
                //1.9.0 以后去除限制shareModel.isFriendShareLink = YES;
                [kShareManager umengShareModel:shareModel
                                      callBack:^(BOOL status) {
                                          
                                      }];
                [self.shareView showViewOfAnimateType:YYPopAnimateDownUp];
            };
        }
    }
    
    // 跳到确定订单页面
    if ([message.name isEqualToString:@"toPreviewPay"]) {
        NSDictionary *body = message.body;
        NSDictionary *dict = @{@"skuId":[body objectForKey:@"skuId"],
                               @"skuCnt":[body objectForKey:@"skuCnt"]};
        [kWholeConfig submitOrderWithSkulist:[@[dict] mutableCopy]
                                       souce:1
                                       addID:0
                               previewSwitch:1
                                       couponId:@[@(-1)]
         payment:@""
         deliveryType:@"1"
                                       block:^(PreOrderModel *model) {
                                           [YYCommonTools skipPreOrder:(YYBaseViewController *)[YYCommonTools getCurrentVC]
                                                                params:model
                                                                skuArr:[@[dict] mutableCopy]
                                                                source:1];
                                       } failBlock:^(PreOrderModel *model) {
                                           
                                       }];
    }
    // web 获取token操作处理
    if ([message.name isEqualToString:@"getToken"]) {
        NSString *jsStr = [NSString stringWithFormat:@"nativeFns('%@','%@','%@')",message.body,kUserInfo.useToken, kAPPVersion];
        [self.m_webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            
        }];
    }
    
    if ([message.name isEqualToString:@"getTokenWithoutAction"]) {
        NSString *jsStr = [NSString stringWithFormat:@"nativeFns('20000','%@')",kUserInfo.useToken];
        [self.m_webView evaluateJavaScript:jsStr completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            
        }];
    }
    if ([message.name isEqualToString:@"goMaterial"]) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:message.body forKey:@"itemId"];
        [YYCommonTools skipMaterial:self params:params];
    }
    
    
}

- (void)dealloc {
    //注意移除，否则导致11.0以下闪退
    [self.m_webView removeObserver:self forKeyPath:@"title"];
    [self.m_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"toShare"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"toDetail"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"toPreviewPay"];
    
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"getToken"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"saveCode"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"backUpperPage"];
    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"goMaterial"];

    [self.m_webView.configuration.userContentController removeScriptMessageHandlerForName:@"getTokenWithoutAction"];
    
    self.m_webView = nil;
}

#pragma mark - 用于URL编码的私有方法
//urlEncode编码
- (NSString *)urlEncodeStr:(NSString *)input {
    NSString *charactersToEscape = @"?!@#$^&%*+,:;='\"`<>()[]{}/\\| ";
    NSCharacterSet *allowedCharacters = [[NSCharacterSet characterSetWithCharactersInString:charactersToEscape] invertedSet];
    NSString *upSign = [input stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacters];
    return upSign;
}

//urlEncode解码
- (NSString *)decoderUrlEncodeStr: (NSString *) input {
    NSMutableString *outputStr = [NSMutableString stringWithString:input];
    [outputStr replaceOccurrencesOfString:@"+" withString:@"" options:NSLiteralSearch range:NSMakeRange(0,[outputStr length])];
    return [outputStr stringByRemovingPercentEncoding];
}
@end

