//
//  CourseContentDetailInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseContentDetailInfoRequestAPI : YYBaseRequestAPI

// 课程内容详情
- (instancetype)initCourseContentInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
