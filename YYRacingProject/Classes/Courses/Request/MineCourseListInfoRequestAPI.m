//
//  MineCourseListInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "MineCourseListInfoRequestAPI.h"

@implementation MineCourseListInfoRequestAPI

- (instancetype)initCourseAuthenInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,MINECOURSELIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
