//
//  CourseVideoAuthenInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "CourseVideoAuthenInfoRequestAPI.h"

@implementation CourseVideoAuthenInfoRequestAPI

// 课程播放鉴权
- (instancetype)initCourseAuthenInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        // resourceId
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COURSEAUTHENTICATION];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
