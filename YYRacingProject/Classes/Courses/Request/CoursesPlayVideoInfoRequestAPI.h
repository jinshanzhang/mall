//
//  CoursesPlayVideoInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface CoursesPlayVideoInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initCoursePlayVideoInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
