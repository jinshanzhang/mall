//
//  CourseLessonDetailInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "CourseLessonDetailInfoRequestAPI.h"

@implementation CourseLessonDetailInfoRequestAPI

// 课程详情
- (instancetype)initCourseDetailInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        // courseId
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *courseID = nil;
    if (kValidDictionary(self.inParams) && [self.inParams objectForKey:@"courseId"]) {
        courseID = [self.inParams objectForKey:@"courseId"];
        [self.inParams removeObjectForKey:@"courseId"];
    }
    if (kValidString(courseID)) {
        return [NSString stringWithFormat:@"%@%@/%@",INTERFACE_PATH,COURSEDETAIL,courseID];
    }
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COURSEDETAIL];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
