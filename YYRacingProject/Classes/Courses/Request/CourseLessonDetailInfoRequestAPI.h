//
//  CourseLessonDetailInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface CourseLessonDetailInfoRequestAPI : YYBaseRequestAPI

// 课程详情
- (instancetype)initCourseDetailInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
