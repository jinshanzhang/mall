//
//  GoodDetailCourseListInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "GoodDetailCourseListInfoRequestAPI.h"

@implementation GoodDetailCourseListInfoRequestAPI

- (instancetype)initCourseListRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *appItemID = nil;
    if (kValidDictionary(self.inParams) && [self.inParams objectForKey:@"appItemId"]) {
        appItemID = [[self.inParams objectForKey:@"appItemId"] stringValue];
        [self.inParams removeObjectForKey:@"appItemId"];
    }
    if (kValidString(appItemID)) {
        return [NSString stringWithFormat:@"%@%@/%@",INTERFACE_PATH,COURSELISTCONTENT,appItemID];
    }
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COURSELISTCONTENT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
