//
//  CoursesPlayAuthInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "CoursesPlayAuthInfoRequestAPI.h"

@implementation CoursesPlayAuthInfoRequestAPI

- (instancetype)initCoursePlayAuthInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COURSEPLAYAUTH];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
