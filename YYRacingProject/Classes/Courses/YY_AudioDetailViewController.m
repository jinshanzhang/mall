//
//  YY_AudioDetailViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/22.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YY_AudioDetailViewController.h"

#import "YYPlayer.h"
#import "YYAudioManagerClass.h"

#import "AudioDetailCell.h"
#import "LessonDetailModel.h"
#import "AudioDetailHeaderView.h"
#import "YY_ChapterViewController.h"
#import "YY_GoodDetailViewController.h"
#import "XHGestureReconginTableView.h"
#import "GoodDetailDescriInfoCell.h"
#import "AudioDetailDescriInfoCell.h"

#import "CourseVideoAuthenInfoRequestAPI.h"
#import "CourseContentDetailInfoRequestAPI.h"

@interface YY_AudioDetailViewController ()
<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) XHGestureReconginTableView  *m_baseTableView;
@property (nonatomic, strong) AudioDetailHeaderView       *headeView;
@property (nonatomic, strong) YYPlayer *player;
@property (nonatomic, assign) BOOL     isShow;

@property (nonatomic, copy)   NSString              *vid;
@property (nonatomic, copy)   NSString              *accessKeyId;
@property (nonatomic, copy)   NSString              *accessKeySecret;
@property (nonatomic, copy)   NSString              *securityToken;

@property (nonatomic, assign) BOOL                  canScroll;
@property (nonatomic, strong) LessonDetailModel     *courseModel;
@property (nonatomic, assign) BOOL                  isCanNotMoveTabView;
@property (nonatomic, assign) BOOL                  isCanNotMoveTabViewPre;

@property (nonatomic, copy)  NSString               *lessonId;
@property (nonatomic, copy)  NSString               *pageType;

@end

@implementation YY_AudioDetailViewController
#pragma mark - Setter method
- (XHGestureReconginTableView *)m_baseTableView{
    if (!_m_baseTableView) {
        _m_baseTableView = [[XHGestureReconginTableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _m_baseTableView.delegate = self;
        _m_baseTableView.dataSource = self;
        _m_baseTableView.backgroundColor = XHWhiteColor;
        _m_baseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _m_baseTableView.allowsSelection = NO;
        if (@available(iOS 11.0, *)) {
            _m_baseTableView.estimatedRowHeight = 0;
            _m_baseTableView.estimatedSectionFooterHeight = 0;
            _m_baseTableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _m_baseTableView;
}

#pragma mark - Life cycle
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.player vc_viewDidAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.player vc_viewWillDisappear];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.player.view removeFromSuperview];
    self.player = nil;
    if ([self.courseModel.contentType integerValue] == 1) {
        // 当进入音频时，页面出去判断音频组件是否显示，没有显示，将其设置为显示。
        if (![[YYAudioManagerClass shared] showStatus]) {
            [[YYAudioManagerClass shared] handlerAudioHideAndShow:YES];
        }
    }
    // 判断屏幕是否是常亮状态
    if ([UIApplication sharedApplication].idleTimerDisabled) {
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    self.canScroll = YES;
    [self xh_addTitle:@""];
    [self xh_alphaNavigation:0];
    [self xh_navBottomLine:XHClearColor];

    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"lessonID"]) {
            self.lessonId = [NSString stringWithFormat:@"%@",[self.parameter objectForKey:@"lessonID"]];
        }
        if ([self.parameter objectForKey:@"skipPageType"]) {
            self.pageType = [self.parameter objectForKey:@"skipPageType"];
        }
    }
    [self xh_addNavigationItemWithImageName:@"find_back_icon"
                                     isLeft:YES
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }];
    Class current = [AudioDetailDescriInfoCell class];
    registerClass(self.m_baseTableView, current);
    current = [AudioDetailCell class];
    registerClass(self.m_baseTableView, current);

    [self xh_addNavigationItemWithImageName:@"find_back_icon"
                                     isLeft:YES
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goodDetailNotificationHandler:) name:GoodDetailLeaveTopNotification
                                               object:nil];
    AudioDetailHeaderView *tableHeaderView = [[AudioDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(210))];
    self.headeView = tableHeaderView;
    tableHeaderView.view.clickedPlayButtonExeBlock = ^(YYPlayView * _Nonnull view) {
        @strongify(self);
        [self initPlayerView:view isNeedSeekPlay:NO];
    };
    self.m_baseTableView.tableHeaderView = tableHeaderView;
    
    self.m_baseTableView.estimatedRowHeight = 150;
    [self.view addSubview:self.m_baseTableView];
    
    [self.m_baseTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(0));
    }];
    
    [self.view insertSubview:self.navigationBar aboveSubview:self.m_baseTableView];
    
    [self getData];
    // Do any additional setup after loading the view.
}

#pragma marek API
-(void)getData {
    @weakify(self);
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.lessonId forKey:@"courseId"];
    
    [YYCenterLoading showCenterLoading];
    CourseContentDetailInfoRequestAPI *api = [[CourseContentDetailInfoRequestAPI alloc] initCourseContentInfoRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            @strongify(self);
            self.courseModel = [LessonDetailModel modelWithJSON:responDict];
            if ([self.courseModel.contentType integerValue] != 3) {
                 //如果不是图文，将屏幕设置为常亮。
                 [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
            }
            self.headeView.view.playType = [self.courseModel.contentType integerValue];
            [self.headeView.view.coverImageView sd_setImageWithURL:[NSURL URLWithString:self.courseModel.coverImage.imageUrl]
                                                  placeholderImage:[UIImage imageNamed:@"home_banner_placehold_icon"]];
            if (kValidString(self.pageType)) {
                if ([self.pageType isEqualToString:@"miniVideo"]) {
                    [self initPlayerView:self.headeView.view isNeedSeekPlay:YES];
                }
            }
            [self.m_baseTableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**获取播放的vid**/
- (void)gainPlayVid:(void(^)(BOOL))isSuccess{
    @weakify(self);
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.courseModel.resourceId forKey:@"resourceId"];
    [YYCenterLoading showCenterLoading];
    CourseVideoAuthenInfoRequestAPI *authAPI = [[CourseVideoAuthenInfoRequestAPI alloc] initCourseAuthenInfoRequest:dict];
    [authAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        @strongify(self);
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            if ([responDict objectForKey:@"videoId"]) {
                self.vid = [responDict objectForKey:@"videoId"];
                isSuccess(YES);
            }
        }
        else {
            isSuccess(NO);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}
#pragma mark - Private method
- (void)initPlayerView:(YYPlayView *)view
        isNeedSeekPlay:(BOOL)isNeedSeekPlay {
    @weakify(self);
    if ( !self ) return ;
    // 上次音频播放到的位置
    NSTimeInterval lastPlayPoint = [YYAudioManagerClass shared].lastPlayPoint;
    [self.player stopAndFadeOut];
    // create new player
    self.player = [YYPlayer player];
    [self.player.placeholderImageView sd_setImageWithURL:[NSURL URLWithString:self.courseModel.coverImage.imageUrl]
                                        placeholderImage:[UIImage imageNamed:@"home_banner_placehold_icon"]];
    self.player.hideBackButtonWhenOrientationIsPortrait = YES;
    [view.coverImageView addSubview:self.player.view];
    [self.player.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    
    if ([self.courseModel.contentType integerValue] != 3) {
        //如果是视频，检测一下音频组件是否存在。如果存在移除
        [[YYAudioManagerClass shared] setReleaseAudio];
    }
    if ([self.courseModel.contentType integerValue] == 1) {
        // 音频赋值操作
        self.player.hiddenPlaceholderImageViewWhenPlayerIsReadyForDisplay = NO;
        YYAudioInfoModel *model = [[YYAudioInfoModel alloc] init];
        model.audioHeaderUrl = self.courseModel.coverImage.imageUrl;
        model.titleStr = self.courseModel.title;
        model.timeStr = self.courseModel.duration;
        model.detailTitleStr = self.courseModel.course.title;
        model.lessonID = @([self.lessonId integerValue]);
        [[YYAudioManagerClass shared] createAudioView:self.player
                                            showModel:model];
    }
    [self gainPlayVid:^(BOOL isSuccess) {
        [kWholeConfig gainCoursePlayAuth:^(NSDictionary *responDict) {
            @strongify(self);
            AliVodModel *vod = [AliVodModel new];
            vod.vid = self.vid;
            self.accessKeyId = [responDict objectForKey:@"accessKeyId"];
            self.accessKeySecret = [responDict objectForKey:@"accessKeySecret"];
            self.securityToken = [responDict objectForKey:@"securityToken"];
            vod.accessKeyId = self.accessKeyId;
            vod.accessKeySecret = self.accessKeySecret;
            vod.securityToken = self.securityToken;
            
            self.player.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithAliVod:vod playModel:[SJPlayModel UITableViewHeaderViewPlayModelWithPlayerSuperview:view.coverImageView tableView:self.m_baseTableView]];
            if (isNeedSeekPlay) {
                self.player.URLAsset.specifyStartTime = lastPlayPoint;
            }
            //self.player.pauseWhenAppDidEnterBackground = NO;
        }];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class current = [AudioDetailCell class];
    if (indexPath.section  == 0) {
        AudioDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
        if (self.courseModel) {
            cell.model =  self.courseModel;
        }
        return cell;
    }
    else {
        current = [AudioDetailDescriInfoCell class];
        AudioDetailDescriInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
        if (self.courseModel) {
            cell.model =  self.courseModel;
        }
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return  UITableViewAutomaticDimension;
    }
    else {
        return (self.view.height);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [YYCreateTools createView:XHLightColor];
    sectionView.frame = CGRectMake(0, 0, kScreenW, 8);
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([self.pageType isEqualToString:@"miniVideo"]) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(1) forKey:@"linkType"];
        if (kValidString(self.courseModel.course.itemUrl)) {
            [params setObject:self.courseModel.course.itemUrl forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:self params:params];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)goodDetailNotificationHandler:(NSNotification *)notification {
    if ([notification.name isEqualToString:GoodDetailLeaveTopNotification]) {
        self.canScroll = YES;
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    [self judgeScrollDirection:scrollView];
    //顶部导航操作
    NSString *imageName, *shareImageName;
    if (offsetY > 200) {
        imageName = @"detail_navbar_back";
        shareImageName = @"share_small_icon";
        [self xh_addNavigationItemImageName:imageName isLeft:YES];
        [self xh_addNavigationItemImageName:shareImageName isLeft:NO];
        [self xh_addTitle:@"课程详情"];
        
        CGFloat alpha = (offsetY - 200)/100;
        
        [self xh_alphaNavigation:alpha];
        [self xh_addNavigationItemAlpha:alpha isLeft:YES];
        [self xh_addNavigationItemAlpha:alpha isLeft:NO];
    }
    else {
        imageName = @"find_back_icon";
        shareImageName = @"share_icon";
        [self xh_addNavigationItemImageName:imageName isLeft:YES];
        [self xh_addNavigationItemImageName:shareImageName isLeft:NO];
        [self xh_addTitle:@""];
        CGFloat alpha = (200 - offsetY)/100;
        [self xh_alphaNavigation:0];
        [self xh_addNavigationItemAlpha:alpha isLeft:YES];
        [self xh_addNavigationItemAlpha:alpha isLeft:NO];
    }
    // 获取停靠位置
    CGFloat pageTabOffsetY = [self.m_baseTableView rectForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:1]].origin.y - kNavigationH;
    
    self.isCanNotMoveTabViewPre = self.isCanNotMoveTabView;
    if (offsetY >= pageTabOffsetY) {
        //到达顶部，不可滚动
        [scrollView setContentOffset:CGPointMake(0, pageTabOffsetY) animated:NO];
        self.isCanNotMoveTabView = YES;
    }
    else {
        //允许滚动
        self.isCanNotMoveTabView = NO;
    }
    
    if (self.isCanNotMoveTabView != self.isCanNotMoveTabViewPre) {
        // 之前能滚动，现在不能滚动，表示进入置顶状态
        if (!self.isCanNotMoveTabViewPre && self.isCanNotMoveTabView) {
            kPostNotification(GoodDetailArriveTopNotification, nil);
            self.canScroll = NO;
        }
        // 之前不能滚动，现在能滚动，表示进入取消置顶
        if(self.isCanNotMoveTabViewPre && !self.isCanNotMoveTabView){
            if (!self.canScroll) {
                scrollView.contentOffset = CGPointMake(0, pageTabOffsetY);
                self.isCanNotMoveTabView = YES;
                kPostNotification(GoodDetailArriveTopNotification, nil);
            }
        }
    }
    if (self.isCanNotMoveTabView && self.isCanNotMoveTabViewPre) {
        kPostNotification(GoodDetailArriveTopNotification, nil);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Event
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
