//
//  CourseListInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/21.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CourseListInfoModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger status;  // 1 已完结  0 正在更新
@property (nonatomic, strong) NSMutableArray *lessons;
@property (nonatomic, assign) NSInteger totalLessonCount; //共多少期
//@property (nonatomic, assign) NSInteger
@property (nonatomic, strong) NSMutableDictionary *coverImage; //封面图

@end

@interface CourseLessonInfoModel : NSObject

@property (nonatomic, copy)   NSString *title;
@property (nonatomic, strong) NSNumber *lessionId;        //课程ID
@property (nonatomic, strong) NSMutableDictionary *coverImage; //封面图
@property (nonatomic, assign) NSInteger contentType;      //1音频  2 视频 3图文
@property (nonatomic, copy)   NSString *duration;           //时长
@property (nonatomic, strong) NSNumber *resourceId;       //资源ID， 图文为0
@property (nonatomic, assign) NSInteger tryoutFlag;       //0：非试用；1：试用
@property (nonatomic, assign) NSInteger playFlag;         //是否允许播放， 0：不允许；1：允许
@property (nonatomic, copy)   NSString *size;               //资源大小


@end

NS_ASSUME_NONNULL_END
