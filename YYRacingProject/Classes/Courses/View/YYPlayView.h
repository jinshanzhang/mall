//
//  YYPlayView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YYPlayView : UIView

@property (nonatomic, strong) UIImageView *coverImageView; //封面图
@property (nonatomic, strong, readonly) UIImageView *placeholderImageView;  //占位图
@property (nonatomic, strong, readonly) UIView *gradientsImageView;  //渐层
@property (nonatomic, strong, readonly) UIButton *playButton;

@property (nonatomic, assign) NSInteger playType; //展示类型 1 音频。2 视频 3图文
@property (nonatomic, copy, nullable) void(^clickedPlayButtonExeBlock)(YYPlayView *view);

@end

NS_ASSUME_NONNULL_END
