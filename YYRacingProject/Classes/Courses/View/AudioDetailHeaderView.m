//
//  AudioDetailHeaderView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "AudioDetailHeaderView.h"

@implementation AudioDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( !self ) return nil;
    self.backgroundColor = [UIColor blackColor];
    _view = [YYPlayView new];
    _view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _view.frame = self.bounds;
    [self addSubview:_view];
    return self;
}

@end
