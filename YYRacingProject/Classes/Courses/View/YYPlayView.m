//
//  YYPlayView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYPlayView.h"

@implementation YYPlayView
@synthesize playType = _playType;
@synthesize playButton = _playButton;
@synthesize coverImageView = _coverImageView;
@synthesize gradientsImageView = _gradientsImageView;
@synthesize placeholderImageView = _placeholderImageView;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if ( !self ) return nil;
    [self _setupViews];
    return self;
}

- (void)userClickedPlayButton {
    if ( _clickedPlayButtonExeBlock ) _clickedPlayButtonExeBlock(self);
}

- (void)_setupViews {
    [self addSubview:self.coverImageView];
    [self.coverImageView addSubview:self.gradientsImageView];
    [self.gradientsImageView addSubview:self.placeholderImageView];
    [self.coverImageView addSubview:self.playButton];
    
    [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.gradientsImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.gradientsImageView.superview);
    }];
    
    [self.placeholderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.placeholderImageView.superview);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(50)));
    }];
    
    [_playButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(64), kSizeScale(27)));
        make.right.mas_equalTo(-kSizeScale(8));
        make.bottom.mas_equalTo(-kSizeScale(12));
    }];
}

- (void)setPlayType:(NSInteger)playType {
    _playType = playType;
    switch (_playType) {
        case 1:
            self.placeholderImageView.hidden = YES;
            break;
        case 2:
            self.playButton.hidden = YES;
            break;
        case 3:
            self.playButton.hidden = YES;
            self.gradientsImageView.hidden = YES;
            self.placeholderImageView.hidden = YES;
            break;
        default:
            break;
    }
}

- (UIImageView *)coverImageView {
    if ( _coverImageView ) return _coverImageView;
    _coverImageView = [[UIImageView alloc] initWithImage:[UIImage new]];
    _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
    _coverImageView.clipsToBounds = YES;
    _coverImageView.userInteractionEnabled = YES;
    _coverImageView.tag = 101;
    return _coverImageView;
}

- (UIView *)gradientsImageView {
    if (_gradientsImageView) return _gradientsImageView;
    _gradientsImageView = [[UIView alloc] init];
    _gradientsImageView.backgroundColor = HexRGBALPHA(0x333333, 0.2);
    return _gradientsImageView;
}

- (UIImageView *)placeholderImageView {
    if (_placeholderImageView) return _placeholderImageView;
    _placeholderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"video_play_center"]];
    _placeholderImageView.userInteractionEnabled = YES;
    [_placeholderImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userClickedPlayButton)]];
    return _placeholderImageView;
}

- (UIButton *)playButton {
    if ( _playButton ) return _playButton;
    _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_playButton setImage:[UIImage imageNamed:@"video_play_btn"] forState:UIControlStateNormal];
    [_playButton addTarget:self action:@selector(userClickedPlayButton) forControlEvents:UIControlEventTouchUpInside];
    return _playButton;
}

@end
