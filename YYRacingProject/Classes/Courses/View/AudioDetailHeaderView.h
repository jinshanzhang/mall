//
//  AudioDetailHeaderView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYPlayView.h"
NS_ASSUME_NONNULL_BEGIN

@interface AudioDetailHeaderView : UIView

@property (nonatomic, strong, readonly) YYPlayView *view;

@end

NS_ASSUME_NONNULL_END
