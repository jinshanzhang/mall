//
//  YY_ChapterViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/23.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YY_ChapterViewController.h"
#import "XHGestureReconginTableView.h"

#import "ChapterCell.h"
#import "GoodDetailBasicInfoCell.h"

#import "GoodDetailHeaderView.h"
#import "CourseLessonDetailInfoRequestAPI.h"
#import "YYGetShareUrlAPI.h"

#import "MyCourceModel.h"
#import "HomeHotGoodInfoModel.h"

@interface YY_ChapterViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) XHGestureReconginTableView  *m_baseTableView;
@property (nonatomic, strong) GoodDetailHeaderView                      *headerView;
@property (nonatomic, strong) MyCourceModel               *courseModel;
@property (nonatomic, strong) HomeHotGoodInfoModel        *hotModel;

@end

@implementation YY_ChapterViewController

- (XHGestureReconginTableView *)m_baseTableView{
    if (!_m_baseTableView) {
        _m_baseTableView = [[XHGestureReconginTableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _m_baseTableView.delegate = self;
        _m_baseTableView.dataSource = self;
        _m_baseTableView.backgroundColor = XHWhiteColor;
        _m_baseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _m_baseTableView.allowsSelection = YES;
        if (@available(iOS 11.0, *)) {
            _m_baseTableView.estimatedRowHeight = 0;
            _m_baseTableView.estimatedSectionFooterHeight = 0;
            _m_baseTableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _m_baseTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.canScroll = YES;
    [self xh_addTitle:@""];
    [self xh_alphaNavigation:0];
    [self xh_navBottomLine:XHClearColor];
    // Do any additional setup after loading the view.
    @weakify(self);
    [self xh_addNavigationItemWithImageName:@"find_back_icon"
                                     isLeft:YES
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }];
    
    [self xh_addNavigationItemWithImageName:@"share_icon"
                                     isLeft:NO
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     [MobClick event:@"good_rightshare"];
                                     [self shareItemViewOperatorHander];
                                 }];
    
    [self getData];
    [self getShareURLApi];

    self.headerView = [[GoodDetailHeaderView alloc] initWithFrame:CGRectZero];
    self.headerView.backgroundColor = XHClearColor;
    self.m_baseTableView.tableHeaderView = self.headerView;

    self.m_baseTableView.estimatedRowHeight = 150;
    self.m_baseTableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.m_baseTableView];
    
    [self.m_baseTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(0));
    }];
    [self.view insertSubview:self.navigationBar aboveSubview:self.m_baseTableView];
    
    Class current = [GoodDetailBasicInfoCell class];
    registerClass(self.m_baseTableView, current);
    
    current = [ChapterCell class];
    registerClass(self.m_baseTableView , current);
}

- (void)shareItemViewOperatorHander {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(self.hotModel.commission)) {
        [params setObject:self.hotModel.commission
                   forKey:@"commission"];
    }
    YYCompositeShareInfoModel *shareModel = [[YYCompositeShareInfoModel alloc] init];
    shareModel.shareUrl = self.hotModel.shareUrl;
    shareModel.originPrice = self.hotModel.originPrice;
    shareModel.reservePrice = self.hotModel.reservePrice;
    shareModel.commission = self.hotModel.commission;
    shareModel.specialStartSellDate = @(self.hotModel.specialSellStartTime);
    shareModel.specialEndSellDate = @(self.hotModel.specialSellDate);
    shareModel.goodsTitle = self.hotModel.goodsTitle;
    shareModel.goodsShortTitle = self.hotModel.goodsShortTitle;
    shareModel.shopName = self.hotModel.shopName;
    shareModel.shopAvatorUrl = self.hotModel.shopAvatorUrl;
    shareModel.goodsShareImage = self.hotModel.goodsShareImage;
    shareModel.itemSaleType = @(self.hotModel.itemSaleType);

    [YYCommonTools goodShareOperator:shareModel
                        expandParams:params];
}


#pragma mark - API
-(void)getData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.courseId forKey:@"courseId"];
    
    [YYCenterLoading showCenterLoading];
    CourseLessonDetailInfoRequestAPI *api = [[CourseLessonDetailInfoRequestAPI alloc] initCourseDetailInfoRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.courseModel = [MyCourceModel modelWithJSON:responDict];
            self.headerView.bannerArrs = [@[self.courseModel.coverImage.imageUrl] mutableCopy];
            [self.m_baseTableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

-(void)getShareURLApi{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.courseId forKey:@"id"];
    [dict setObject:@(2) forKey:@"sourceType"];
    
    [YYCenterLoading showCenterLoading];
    YYGetShareUrlAPI *api = [[YYGetShareUrlAPI alloc] initRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.hotModel = [HomeHotGoodInfoModel modelWithJSON:responDict];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }else{
        return self.courseModel.lessons.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class current = [GoodDetailBasicInfoCell class];
    if (indexPath.section  == 0) {
        GoodDetailBasicInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
        cell.courseModel = self.courseModel;
        return cell;
    }
    else {
        current = [ChapterCell class];
        ChapterCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
        cell.model = self.courseModel.lessons[indexPath.row];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  UITableViewAutomaticDimension;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *sectionView = [YYCreateTools createView:XHLightColor];
    sectionView.frame = CGRectMake(0, 0, kScreenW, 8);
    return sectionView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 8;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LessonDetailModel *model =self.courseModel.lessons[indexPath.row];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:model.lessonId forKey:@"lessonID"];
    [YYCommonTools skipAudioWith:self params:dic];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    [self judgeScrollDirection:scrollView];
    //顶部导航操作
    NSString *imageName, *shareImageName;
    if (offsetY > 200) {
        imageName = @"detail_navbar_back";
        shareImageName = @"share_small_icon";
        [self xh_addNavigationItemImageName:imageName isLeft:YES];
        [self xh_addNavigationItemImageName:shareImageName isLeft:NO];
        [self xh_addTitle:@"课程详情"];
        
        CGFloat alpha = (offsetY - 200)/100;
        [self xh_alphaNavigation:alpha];
        [self xh_addNavigationItemAlpha:alpha isLeft:YES];
        [self xh_addNavigationItemAlpha:alpha isLeft:NO];
    }
    else {
        imageName = @"find_back_icon";
        shareImageName = @"share_icon";
        [self xh_addNavigationItemImageName:imageName isLeft:YES];
        [self xh_addNavigationItemImageName:shareImageName isLeft:NO];
        [self xh_addTitle:@""];
        CGFloat alpha = (200 - offsetY)/100;
        [self xh_alphaNavigation:0];
        [self xh_addNavigationItemAlpha:alpha isLeft:YES];
        [self xh_addNavigationItemAlpha:alpha isLeft:NO];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
