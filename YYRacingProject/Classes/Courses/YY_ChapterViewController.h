//
//  YY_ChapterViewController.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/23.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_ChapterViewController : YYBaseViewController

@property (nonatomic, copy) NSString *courseId;

@end
