//
//  YY_LifeCircleViewController.m
//  YYRacingProject
//
//  Created by cujia_1 on 2020/7/23.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "YY_LifeCircleViewController.h"

@interface YY_LifeCircleViewController ()

@end

@implementation YY_LifeCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.title = @"生活好圈";
    [self xh_addTitle:@"生活好圈"];
//    [self xh_alphaNavigation:0];
    [self xh_navBackgroundColor:XHMainColor];
    [self xh_navBottomLine:XHClearColor];
    [self xh_navTitleColor:XHWhiteColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
