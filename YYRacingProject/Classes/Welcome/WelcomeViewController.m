//
//  WelcomeViewController.m
//  Zhuan
//
//  Created by txooo on 16/1/11.
//  Copyright © 2016年 领琾. All rights reserved.
//

#import "WelcomeViewController.h"
#import "CYLTabBarController.h"
#define ImageCount 3

@interface WelcomeViewController ()<UIScrollViewDelegate>
{
    UIView *pageView;
}

@end

@implementation WelcomeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //隐藏状态栏
    [UIApplication sharedApplication].statusBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //添加UISrollView
    [self setupScrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/**
 *  添加UISrollView
 */
-(void)setupScrollView {
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.frame = self.view.bounds;
    [self.view addSubview:scrollView];
    scrollView.delegate = self;
    
    //2.添加图片
    CGFloat imageW = scrollView.frame.size.width;
    CGFloat imageH = scrollView.frame.size.height;
    for (NSInteger index = 0; index < ImageCount; index++) {
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.userInteractionEnabled = YES;
        //设置图片
        NSString *name = [NSString stringWithFormat:@"引导页-%tu",index+1];
        imageView.image = [UIImage imageNamed:name];
        //设置frame
        CGFloat imageX = index * imageW;
        imageView.frame = CGRectMake(imageX, 0, imageW, imageH);
        [scrollView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        //在最后一个图片上面添加按钮
        if (index == ImageCount-1) {
            [self setupLastImageView:imageView];
        } else{
            UIButton *jumpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//            jumpBtn.frame = CGRectMake(kScreenW - kSizeScale(12 + 60), kSizeScale(100), kSizeScale(60), kSizeScale(40));
            [jumpBtn setTitle:@"跳过" forState:UIControlStateNormal];
            [jumpBtn setTitleColor:HexRGB(0xFF583D) forState:UIControlStateNormal];
            jumpBtn.titleLabel.font = RegularFont(kSizeScale(14));
            jumpBtn.layer.cornerRadius = kSizeScale(8.0);
            jumpBtn.layer.borderWidth = kSizeScale(0.8);
            jumpBtn.layer.borderColor = HexRGB(0xFF583D).CGColor;
            [jumpBtn addTarget:self action:@selector(jumpToMainVC) forControlEvents:UIControlEventTouchUpInside];
            [imageView addSubview:jumpBtn];
            [jumpBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(imageView).offset(-kSizeScale(12));
                make.top.equalTo(imageView).offset(kNavigationH);
                make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(30)));
            }];
        }
    }
    //    3.设置滚动内容的尺寸
    scrollView.contentSize = CGSizeMake(imageW * ImageCount, 0);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    
}


//将内容添加到最后一个图片
- (void)setupLastImageView:(UIImageView *)imageView {
    //    0.imageView默认是不可点击的 将其设置为能跟用户交互
    imageView.userInteractionEnabled=YES;
    //1.添加开始按钮
    UIButton *startButton=[[UIButton alloc] init];
    startButton.backgroundColor = [UIColor clearColor];
    startButton.frame = CGRectMake(kSizeScale(50), kScreenH - kSizeScale(150), kScreenW - kSizeScale(100), kSizeScale(60));
    startButton.centerX = self.view.centerX;
    [startButton addTarget:self action:@selector(jumpToMainVC) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:startButton];
}

-(void)jumpToMainVC {
//    CYLTabBarController *tabbarVC = [[CYLTabBarController alloc]init];
//    [UIApplication sharedApplication].keyWindow.rootViewController = tabbarVC;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SetRootVCNoty" object:nil];
    NSString *key = @"CFBundleShortVersionString";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // 获得当前软件的版本号
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    [defaults setObject:currentVersion forKey:key];
    [defaults synchronize];
}

#pragma mark -scroll代理方法
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //    1.取出水平方向上滚动的距离
    CGFloat offsetX = scrollView.contentOffset.x;
    //2.求出页码
    double pageDouble=offsetX/scrollView.frame.size.width;
    int pageInt = (int)(pageDouble + 0.5);
    if (pageInt > 2) {
        pageView.hidden = YES;
    }else{
        pageView.hidden = NO;
    }
}

@end

