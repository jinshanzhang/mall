//
//  YY_MainFeaturedViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_MainFeaturedViewController.h"

#import "HomeBannerInfoModel.h"
#import "HomeTimeAxleInfoModel.h"
#import "HomeResourcesInfoModel.h"
#import "HomeListStructInfoModel.h"
#import "HomeSuperBrandLumpInfoModel.h"

#import "YYPopToastView.h"
#import "HomeHeaderInfoView.h"
#import "HomeFeatureSectionInfoView.h"

#import "HomeBrandHotInfoCell.h"
#import "HomeSpecailSellTitleInfoCell.h"
#import "HomeSpecialSellSingleInfoCell.h"
#import "HomeSpecialSellDoubleInfoCell.h"

#import "YYHomeSubListGoodsInfoRequest.h"
#import "YYHomeSubListGoodPageInfoRequest.h"
#import "YYHomeScreeningShareInfoRequestAPI.h"

@interface YY_MainFeaturedViewController ()
<
HomeHeaderViewDelegate,
HomeBrandHotInfoCellDelegate,
HomeFeatureSectionInfoViewDelegate,
HomeSpecialSellSingleInfoCellDelegate,
HomeSpecialSellDoubleInfoCellDelegate>

@property (nonatomic, strong) HomeHeaderInfoView *headerView;
@property (nonatomic, strong) HomeFeatureSectionInfoView *sectionView;
@property (nonatomic, strong) TabarItemRemindView *gifAnimateView;  //GIF 动画图
@property (nonatomic, strong) CLLocationManager *locationmanager;//定位服务

@property (nonatomic, strong) NSString *uri;
@property (nonatomic, assign) BOOL isClickSwitch;

@property (nonatomic, assign) BOOL isHover; //是否悬停
@property (nonatomic, assign) BOOL isExistBanner; //是否存在banner
@property (nonatomic, assign) BOOL hasShow; //版本检测是否执行过
@property (nonatomic, assign) BOOL isDisableAninate; //浮窗是否进行动画
@property (nonatomic, strong) NSMutableArray *niches; //资源位
@property (nonatomic, strong) NSMutableArray *timeLines; //时间轴
@property (nonatomic, assign) BOOL isAlreadAddTitleModel;//是否已经添加标题数据
@property (nonatomic, assign) NSInteger defaultSelectIndex;
@property (nonatomic, assign) NSInteger controlSkipPopTimes; //控制弹框次数
@property (nonatomic, assign) NSInteger pageIndex; //当前控制的pageIndex
@property (nonatomic, strong) NSMutableArray *lastSingleArray; //上次留下的单

@property (nonatomic, strong) HomeBannerInfoModel   *floatModel;       //浮窗资源
@property (nonatomic, strong) HomeTimeAxleInfoModel *currentTimeModel;
@property (nonatomic, strong) YYCompositeShareInfoModel *screenShareModel; //场次分享

@property (nonatomic, strong) NSMutableArray * tempMarr;
@property (nonatomic, strong) UIButton    *scrollTopBtn;   //滚动到顶部按钮

@end

@implementation YY_MainFeaturedViewController

#pragma mark - Setter

- (HomeHeaderInfoView *)headerView {
    if (!_headerView) {
        _headerView = [[HomeHeaderInfoView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 150)];
        _headerView.delegate = self;
    }
    return _headerView;
}

- (TabarItemRemindView *)gifAnimateView {
    if (!_gifAnimateView) {
        _gifAnimateView = [[TabarItemRemindView alloc] initWithFrame:CGRectZero];
        [_gifAnimateView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSkipOperator:)]];
        _gifAnimateView.hidden = YES;
    }
    return _gifAnimateView;
}

- (HomeFeatureSectionInfoView *)sectionView {
    if (!_sectionView) {
        _sectionView = [[HomeFeatureSectionInfoView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(56))];
        _sectionView.delegate = self;
    }
    return _sectionView;
}

#pragma mark - Life  cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.changeScrollTop = YES;
        self.NoDataType = YYEmptyViewAlxeNoDataType;
        self.footerBgStyleColor = XHStoreGrayColor;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    self.isHover = NO;
    self.isClickSwitch = NO;
    self.controlSkipPopTimes = 1;
    self.isAlreadAddTitleModel = NO;
    self.lastSingleArray = [NSMutableArray array];
    self.view.backgroundColor = self.m_tableView.backgroundColor = XHClearColor;
    [self xh_hideNavigation:YES];
    if (self.parameter) {
        if ([self.parameter objectForKey:@"uri"]) {
            self.uri = [self.parameter objectForKey:@"uri"];
        }
        if ([self.parameter objectForKey:@"pageIndex"]) {
            self.pageIndex = [[self.parameter objectForKey:@"pageIndex"] integerValue];
        }
    }
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        if (@available(iOS 11, *)) {
            make.top.offset(self.view.safeAreaInsets.top);
        }
        else {
            make.top.equalTo(self.view);
        }
    }];

    Class currentCls = [HomeBrandHotInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSpecialSellSingleInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSpecialSellDoubleInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSpecailSellTitleInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    self.m_tableView.tableHeaderView = self.headerView;
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.m_tableView);
        make.width.mas_equalTo(kScreenW);
    }];
    self.headerView.homeHeaderHeightUpdateBlock = ^{
        @strongify(self);
        [self.m_tableView layoutIfNeeded];
        self.m_tableView.tableHeaderView = self.headerView;
    };
    [self loadPullDownPage];
    // 右边浮窗
    [self.view addSubview:self.gifAnimateView];
    [self.gifAnimateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(93), kSizeScale(94)));
        make.right.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(85));
    }];
    // Do any additional setup after loading the view.
//    [self testNet];
    
//    [self.view addSubview:self.toTopButton];
    
    self.scrollTopBtn = [YYCreateTools createBtnImage:@"list_scrolltop_icon"];
    [self.scrollTopBtn addTarget:self action:@selector(scrollToTop) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.scrollTopBtn];
    [self.scrollTopBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(39), kSizeScale(39)));
        make.bottom.mas_equalTo(-(kTabbarH));
        make.right.mas_equalTo(-kSizeScale(10));
    }];
}

- (void)scrollToTop {
    [self.m_tableView setContentOffset:CGPointMake(0, 0) animated:NO];
}

//- (void)testNet {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"pageIndex"] = @(self.current);
//    params[@"pageSize"] = @(self.pageLength);
//    [NetWorkHelper requestWithMethod:GET path:@"/ironman/v4/index/recommdation" parameters:params block:^(NSString *errorNO, NSDictionary *result) {
//
//    }];
//}

- (void)loadPullDownPage {
    self.isAlreadAddTitleModel = NO;
    [self.lastSingleArray removeAllObjects];
    if (!self.isClickSwitch) {
        [self headerSourcePointHandler];
    }
    kPostNotification(MainHotScroll, nil);
    self.current = 1;
    self.pageLength = 10;
//    [super loadPullDownPage];
    [self.dataIds removeAllObjects];
    YYBaseRequestAPI *baseRequest = [self baseRequest];
    if (!baseRequest) {
        return;
    }
    self.loadType = YYLoadPullDownLoadType;
    baseRequest.delegate = self;
    [baseRequest start];
}

- (void)loadNextPage {
    self.current++;
    YYBaseRequestAPI *baseRequest = [self baseRequest];
    if (!baseRequest) {
        return;
    }
    self.loadType = YYLoadPullUpLoadType;
    baseRequest.delegate = self;
    [baseRequest start];
}

- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    // 上拉加载
//    if (kValidArray(self.dataIds)) {
//        [params removeAllObjects];
////        if ((self.totalLength+self.pageLength) > self.dataIds.count) {
////            self.pageLength = self.dataIds.count - self.totalLength;
////        }
////        [params setObject:[[self.dataIds subarrayWithRange:NSMakeRange(self.totalLength, self.pageLength)] componentsJoinedByString:@","] forKey:@"uris"];
////        if (self.currentTimeModel) {
////            [params setObject:self.currentTimeModel.timePoint forKey:@"timePoint"];
////        }
////        if (kValidString(self.uri)) {
////            [params setObject:self.uri forKey:@"uri"];
////        }
//        YYHomeSubListGoodPageInfoRequest *pageAPI = [[YYHomeSubListGoodPageInfoRequest alloc] initHomeSubListGoodPageInfoRequest:params];
//        return pageAPI;
//    }
//    else {
//        self.totalLength = 0;
//        self.pageLength = 1;
//        if (self.currentTimeModel) {
//            [params setObject:self.currentTimeModel.timePoint
//                       forKey:@"timePoint"];
//        }
//        if (kValidString(self.uri)) {
//            [params setObject:self.uri forKey:@"uri"];
//        }
        /*if (!self.hasShow) {
            //获取更新提示
            [YYCommonTools getVersion];
            self.hasShow = YES;
        }*/
    
        [YYCommonTools checkVersionUpdate:NO];
        
        [self handlerFloatBasket];
        if (self.controlSkipPopTimes == 1) {
            [self handlerNewCouponPopAndFloatPop];
            self.controlSkipPopTimes ++ ;
        }
        params[@"pageIndex"] = @(self.current);
        params[@"pageSize"] = @(self.pageLength);
        YYHomeSubListGoodsInfoRequest *listAPI = [[YYHomeSubListGoodsInfoRequest alloc] initHomeSubListGoodsInfoRequest:params];
        return listAPI;
//    }
    return nil;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    self.isClickSwitch = NO;
    self.tempMarr = [NSMutableArray array];
    if (kValidDictionary(responseDic)) {
        NSMutableArray *tempArr, *tempItems;
        if ([responseDic objectForKey:@"data"]) {
            NSDictionary *dataDict = [responseDic objectForKey:@"data"];
            if ([dataDict objectForKey:@"recommendationItems"]) {
                NSArray *items = [NSArray modelArrayWithClass:[HomeListItemGoodInfoModel class] json:[dataDict objectForKey:@"recommendationItems"]];
                NSMutableArray *marr = [NSMutableArray array];
                for (HomeListItemGoodInfoModel *model in items) {
                    HomeListStructInfoModel *infoModel = [[HomeListStructInfoModel alloc] init];
                    YYImageInfoModel *imgModel = [[YYImageInfoModel alloc] init];
                    imgModel.imageUrl = model.imageUrl;
                    model.coverImage = imgModel;
                    infoModel.item = model;
                    [marr addObject:infoModel];
                }
                self.tempMarr = marr;
                if (!kValidArray(marr)) {
                    return nil;
                }
                tempItems = [marr mutableCopy];
            }
        }
//        if ([responseDic objectForKey:@"itemIds"]) {
//            //处理所有的ids
//            NSArray *itemIds = [responseDic objectForKey:@"itemIds"];
//            if (kValidArray(itemIds)) {
//                tempArr = [itemIds mutableCopy];
//            }
//            if (kValidArray(tempArr)) {
//                self.dataIds = [tempArr mutableCopy];
//            }
//        }
//        if ([responseDic objectForKey:@"parseResponce"]) {
//            NSArray *items = [NSArray modelArrayWithClass:[HomeListStructInfoModel class] json:[responseDic objectForKey:@"items"]];
//            if (!kValidArray(items)) {
//                return nil;
//            }
//            tempItems = [items mutableCopy];
//        }
//        // 第一次分页需要的特殊处理。
//        if ([responseDic objectForKey:@"firstItemIds"]) {
//            NSNumber *pageNumber = [responseDic objectForKey:@"firstItemIds"];
//            if (![pageNumber isKindOfClass:[NSNull class]]) {
//                self.pageLength = [pageNumber integerValue];
//            }
//        }
//        else {
//            self.pageLength = 30;
//        }
//        self.current ++;
        return [self dataSourcePackage:tempItems];
    }
//    [self.m_tableView.ly_emptyView removeFromSuperview];
    return nil;
}

- (BOOL)canLoadMore {
//    return self.dataIds.count > self.totalLength;
    return self.tempMarr.count == self.pageLength;
}

#pragma mark - Request
/**
 首页场次分享
 **/
- (void)homeScreeningShare {
    self.screenShareModel = [[YYCompositeShareInfoModel alloc] init];
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    if (self.currentTimeModel) {
//        [params setObject:self.currentTimeModel.timePoint forKey:@"timePoint"];
//        [params setObject:self.currentTimeModel.timePointStart forKey:@"timePointStart"];
//        [params setObject:self.currentTimeModel.timePointEnd forKey:@"timePointEnd"];
//    }
//    YYHomeScreeningShareInfoRequestAPI *screenAPI = [[YYHomeScreeningShareInfoRequestAPI alloc] initHomeScreeningShareInfoRequest:params];
//    [screenAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
//        NSDictionary *responDict = request.responseJSONObject;
//        if (kValidDictionary(responDict)) {
//            self.screenShareModel.isNeedComposite = @(NO);  // 是否需要进行合成操作
//            if ([responDict objectForKey:@"skipUrl"]) {
//                self.screenShareModel.shareUrl = [responDict objectForKey:@"skipUrl"];
//            }
//            if ([responDict objectForKey:@"title"]) {
//                self.screenShareModel.goodsTitle = [responDict objectForKey:@"title"];
//            }
//            if ([responDict objectForKey:@"content"]) {
//                self.screenShareModel.goodsShortTitle = [responDict objectForKey:@"content"];
//            }
//            if ([responDict objectForKey:@"smallImageUrl"]) {
//                self.screenShareModel.goodsShareImage = [responDict objectForKey:@"smallImageUrl"];
//            }
//            if ([responDict objectForKey:@"shareImageUrl"]) {
//                self.screenShareModel.goodsShareBigImages = [responDict objectForKey:@"shareImageUrl"];
//            }
//        }
//    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
//
//    }];
}

#pragma mark - Private
/** 头部资源位处理**/
- (void)headerSourcePointHandler {
    self.niches = [NSMutableArray array];
    self.timeLines = [NSMutableArray array];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(self.uri)) {
        [params setObject:self.uri forKey:@"uri"];
    }
    [kWholeConfig homeSourcePointRequest:params
                                 success:^(NSDictionary *responDict) {
                                     if (kValidDictionary(responDict)) {
                                         if ([responDict objectForKey:@"niches"]) {
                                             NSArray *nichArray = [responDict objectForKey:@"niches"];
                                             [nichArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                 NSDictionary *singleNiches = (NSDictionary *)obj;
                                                 if ([singleNiches objectForKey:@"type"]) {
                                                     NSNumber *type = [singleNiches objectForKey:@"type"];
                                                     if([type integerValue] == 1 || [type integerValue] == 2 ) {
                                                         [self.niches addObject:[HomeResourcesInfoModel modelWithJSON:singleNiches]];
                                                     }
//                                                     if ([type integerValue] != 11) {
//                                                         [self.niches addObject:[HomeResourcesInfoModel modelWithJSON:singleNiches]];
//                                                     }
//                                                     else {
//                                                         // 超级品牌团
//                                                         [self.niches addObject:[HomeSuperBrandLumpInfoModel modelWithJSON:singleNiches]];
//                                                     }
                                                 }
                                             }];
                                             
                                             __block BOOL tempExistBanner = NO;
                                             [self.niches enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                 HomeResourcesInfoModel *infoModel = (HomeResourcesInfoModel *)obj;
                                                 if (infoModel.type == 1 && infoModel) {
                                                     tempExistBanner = YES;
                                                     *stop = YES;
                                                 }
                                             }];
                                            
                                             self.isExistBanner = tempExistBanner;
                                             if (self.navigationGradientBlock) {
                                                 self.navigationGradientBlock(self.isExistBanner, CGPointZero,self.pageIndex);
                                             }
                                             self.headerView.headerSources = self.niches;
                                             [self.m_tableView layoutIfNeeded];
                                             self.m_tableView.tableHeaderView = self.headerView;
                                         }
                                         if ([responDict objectForKey:@"timeLines"]) {
                                             NSArray *tempTimeLines = [NSArray modelArrayWithClass:[HomeTimeAxleInfoModel class] json:[responDict objectForKey:@"timeLines"]];
//                                             if (kValidArray(tempTimeLines)) {
//                                                 self.timeLines = [tempTimeLines mutableCopy];
////                                                 self.timeLines = @[];
//                                             }
                                             self.sectionView.contentDatas = self.timeLines;
                                         }
                                         [self getSelectTimeObjectOrIndex:-1];
                                         [self.sectionView willMenuBarItemSelectAtIndex:self.defaultSelectIndex];
                                         [self homeScreeningShare];
                                     }
                                 }];
}

/** 获取选中的时间轴对象 **/
- (NSInteger)getSelectTimeObjectOrIndex:(NSInteger)selectIndex {
    __block NSInteger index = 0;
    if (selectIndex == -1) {
        // 则遍历数据获取当前时间轴处于的位置
        [self.timeLines enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeTimeAxleInfoModel *model = (HomeTimeAxleInfoModel *)obj;
            if ([model.chooseFlag integerValue] == 1) {
                self.currentTimeModel = model;
                index = idx;
                self.defaultSelectIndex = index;
                *stop = YES;
            }
        }];
    }
    else {
        // 根据index 获取时间轴数据
        index = selectIndex;
        [self.timeLines enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeTimeAxleInfoModel *model = (HomeTimeAxleInfoModel *)obj;
            if (idx == index) {
                self.currentTimeModel = model;
                *stop = YES;
            }
        }];
    }
    return index;
}

/**
 处理优惠券弹框逻辑
 **/
- (void)handlerNewCouponPopAndFloatPop {
    BOOL passwordCheck = [[kUserDefault objectForKey:@"Command"] boolValue];
    BOOL newUser = [[kUserDefault objectForKey:@"NewUser"] boolValue];
    // 检查一下口令
    if (passwordCheck) {
        [kWholeConfig passwordCheckRequest];
        return;
    }
    // 检查一下新人优惠券
    if (newUser) {
        BOOL  isCanOpenNewHomePop = YES;
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        NSMutableDictionary *newCoupon = [kUserManager loadNewCouponInfo];
        if ([newCoupon objectForKey:@"linkUrl"]) {
            [params setObject:[newCoupon objectForKey:@"linkUrl"] forKey:@"url"];
        }
        if ([newCoupon objectForKey:@"type"]) {
            [params setObject:[newCoupon objectForKey:@"type"] forKey:@"linkType"];
        }
        if ([newCoupon objectForKey:@"image"]) {
            [params setObject:[[newCoupon objectForKey:@"image"] objectForKey:@"imageUrl"] forKey:@"imageUrl"];
        }
        if ([newCoupon objectForKey:@"display"]) {
            NSNumber *switchFlag = [newCoupon objectForKey:@"display"];
            if ([switchFlag integerValue] == 1) {
                // 隐藏
                isCanOpenNewHomePop = NO;
            }
            else if ([switchFlag integerValue] == 2) {
                // 展示
                isCanOpenNewHomePop = YES;
            }
        }
        HomeBannerInfoModel *model = [HomeBannerInfoModel modelWithJSON:params];
        if (isCanOpenNewHomePop) {
            [self activityPopHandler:model];
        }
        [kUserDefault setObject:@(NO) forKey:@"NewUser"];
        return;
    }
    // 普通的优惠券弹框
    NSMutableDictionary *couponDict = [YYCommonTools getAssignResourcesUseOfType:15];
    if (kValidDictionary(couponDict)) {
        HomeBannerInfoModel *bannerModel = [HomeBannerInfoModel modelWithJSON:couponDict];
        if (!bannerModel.switchFlag) {
            // 没有打开不执行
            return ;
        }
        if (bannerModel.popType == 1) {
            NSNumber *popTimes = [kUserDefault objectForKey:@"isAlreadyPop"];
            if (!popTimes || [popTimes boolValue] != YES) {
                // 如果为空，或者为NO. 则弹
                [self activityPopHandler:bannerModel];
            }
            [kUserDefault setObject:@(YES) forKey:@"isAlreadyPop"];
        }
        else if (bannerModel.popType == 2) {
            // 每次都弹
            [self activityPopHandler:bannerModel];
            [kUserDefault setObject:@(NO) forKey:@"isAlreadyPop"];
        }
    }
}

/**
 处理浮窗
 **/
- (void)handlerFloatBasket {
    NSMutableDictionary *couponDict = [YYCommonTools getAssignResourcesUseOfType:16];
    if (kValidDictionary(couponDict)) {
        HomeBannerInfoModel *bannerModel = [HomeBannerInfoModel modelWithJSON:couponDict];
        self.floatModel = bannerModel;
        self.gifAnimateView.hidden = !bannerModel.switchFlag;
        if (kValidString(bannerModel.imageUrl)) {
            self.gifAnimateView.remindURL = bannerModel.imageUrl;
        }
    }
}

/**
 首页弹框处理
 **/
- (void)activityPopHandler:(HomeBannerInfoModel *)model {
    if (!model) {
        return;
    }
    // 控制是否弹出过
    [kUserDefault setObject:@(YES) forKey:@"HomePop"];
    YYActivityPopView *popView = [YYActivityPopView initYYPopView:[[YYActivityPopView alloc] init]];
    popView.params = [@{@"activityModel":model} mutableCopy];
    @weakify(popView);
    [popView showViewOfAnimateType:YYPopAnimateScale];
    popView.operatorBlock = ^(NSInteger operatorType) {
        @strongify(popView);
        if (operatorType != 0) {
            //跳转
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:@(model.linkType) forKey:@"linkType"];
            if (kValidString(model.url)) {
                [params setObject:model.url forKey:@"url"];
            }
            [YYCommonTools skipMultiCombinePage:self params:params];
            [popView showViewOfAnimateType:YYPopAnimateScale];
        }
        [kUserDefault setObject:@(NO) forKey:@"HomePop"];
    };
    
}

/**
 分页数据源封装
 **/
- (NSMutableArray *)dataSourcePackage:(NSMutableArray *)itemArrays {
    __block NSMutableArray *dataSource = [NSMutableArray array];
    __block NSMutableArray *towAtRow = [NSMutableArray array];
    [itemArrays enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeListStructInfoModel *structModel = (HomeListStructInfoModel *)obj;
        NSInteger type = [structModel.type integerValue];
        if ([structModel.type integerValue] == 2) {
            // 增加自定义类型
            HomeListItemGoodInfoModel *emptyModel = [[HomeListItemGoodInfoModel alloc] init];
            emptyModel.scrollType = YYSingleRowScrollEmpty;
            [structModel.items addObject:emptyModel];
        }
        if (type == 1) {
            // 一排单个
            [dataSource addObject:structModel];
        }
        else if (type == 2) {
            [dataSource addObject:structModel];
        }
        else if (type == 0) {
            if (!self.isAlreadAddTitleModel) {
                HomeListStructInfoModel *tempModel = [[HomeListStructInfoModel alloc] init];
                tempModel.type = @(3);
                tempModel.title = @"热销推荐";
                [dataSource addObject:tempModel];
                self.isAlreadAddTitleModel = YES;
            }
            [towAtRow addObject:structModel];
        }
    }];
    
    if (kValidArray(self.lastSingleArray)) {
        [towAtRow insertObject:[self.lastSingleArray firstObject] atIndex:0];
    }
    if (towAtRow.count % 2 != 0) {
        [self.lastSingleArray addObject:[towAtRow lastObject]];
        [towAtRow removeLastObject];
    }
    if (kValidArray(towAtRow)) {
        [dataSource addObjectsFromArray:[YYCommonTools splitArray:towAtRow withSubSize:2]];
    }
    self.totalLength = self.totalLength + self.pageLength;
    if (self.totalLength >= self.dataIds.count && kValidArray(self.lastSingleArray)) {
        [dataSource addObject:self.lastSingleArray];
    }
    return dataSource;
}

/**
 品牌cell赋值
 **/
- (void)configBrandGoodCell:(HomeBrandHotInfoCell *)cell
                atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    if (indexPath.row < self.listData.count) {
        HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:indexPath.row];
        [cell setBrandInfoModel:itemModel];
    }
}

/**
 特卖cell赋值
 **/
- (void)configSpecialGoodCell:(HomeSpecialSellSingleInfoCell *)cell
                  atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    if (indexPath.row < self.listData.count) {
        HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:indexPath.row];
        [cell setInfoModel:itemModel];
    }
}

/**标题cell赋值**/
- (void)configTitleCell:(HomeSpecailSellTitleInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    if (indexPath.row < self.listData.count) {
        HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:indexPath.row];
        [cell setInfoModel:itemModel];
    }
}

#pragma mark - Event
/**
 点击右侧活动跳转操作
 **/
- (void)clickSkipOperator:(UITapGestureRecognizer *)tapGesture {
    if (self.floatModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(self.floatModel.linkType) forKey:@"linkType"];
        if (kValidString(self.floatModel.url)) {
            [params setObject:self.floatModel.url forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:self params:params];
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        NSInteger row = indexPath.row;
        Class current = [HomeBrandHotInfoCell class];
        if (row < self.listData.count && kValidArray(self.listData)) {
            if ([[self.listData objectAtIndex:row] isKindOfClass:[NSArray class]]) {
                current = [HomeSpecialSellDoubleInfoCell class];
                HomeSpecialSellDoubleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                cell.delegate = self;
                cell.itemModels = [self.listData objectAtIndex:row];
                return cell;
            }
            else {
                HomeListStructInfoModel *structModel = [self.listData objectAtIndex:row];
                if ([structModel.type integerValue] == 2 ) {
                    current = [HomeBrandHotInfoCell class];
                    HomeBrandHotInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                    cell.delegate = self;
                    [self configBrandGoodCell:cell atIndexPath:indexPath];
                    return cell;
                }else if ([structModel.type integerValue] == 3) {
                    current = [HomeSpecailSellTitleInfoCell class];
                    HomeSpecailSellTitleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
//                    [self configTitleCell:cell atIndexPath:indexPath];
                    return cell;
                }
                else {
                    current = [HomeSpecialSellSingleInfoCell class];
                    HomeSpecialSellSingleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                    cell.delegate = self;
                    [self configSpecialGoodCell:cell atIndexPath:indexPath];
                    return cell;
                }
            }
        }
        return cell;
    }
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        NSInteger row = indexPath.row;
        if (row < self.listData.count && kValidArray(self.listData)) {
            if ([[self.listData objectAtIndex:row] isKindOfClass:[NSArray class]]) {
                CGFloat width = (kScreenW - kSizeScale(8) - kSizeScale(12)*2)/2.0;
                return kSizeScale(80) + width;
            }
            else {
                HomeListStructInfoModel *structModel = [self.listData objectAtIndex:row];
                if ([structModel.type integerValue] == 2 ) {
                    /*height = [tableView fd_heightForCellWithIdentifier:@"HomeBrandHotInfoCell" configuration:^(id cell) {
                        [self configBrandGoodCell:cell atIndexPath:indexPath];
                     }];*/
                    height = kSizeScale(160) + kSizeScale(130) + kSizeScale(12);
                    return height;
                }
                else if ([structModel.type integerValue] == 3 ) {
//                    height = [tableView fd_heightForCellWithIdentifier:@"HomeSpecailSellTitleInfoCell" configuration:^(id cell) {
//                        [self configTitleCell:cell atIndexPath:indexPath];
//                    }];
//                    return height;
                    return 0.00001f;
                }
                else {
                    /*height = [tableView fd_heightForCellWithIdentifier:@"HomeSpecialSellSingleInfoCell" configuration:^(id cell) {
                        [self configSpecialGoodCell:cell atIndexPath:indexPath];
                    }];
                    */
                    height = kSizeScale(160) + kSizeScale(100) + kSizeScale(12);
                    return height;
                }
            }
        }
        else {
            return height;
        }
    }
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    return self.sectionView;
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return kSizeScale(56);
    return 0.000001f;
}

//NSMutableDictionary *params = [NSMutableDictionary dictionary];
//if (kValidString(self.m_detailModel.shareInfo.commission)) {
//    [params setObject:self.m_detailModel.shareInfo.commission
//               forKey:@"commission"];
//}
//if (kValidDictionary(self.m_detailModel.coupon)) {
//    NSDictionary *coupon = self.m_detailModel.coupon;
//    if ([coupon objectForKey:@"discountPrice"]) {
//        self.m_detailModel.shareInfo.discountPrice = [coupon objectForKey:@"discountPrice"];
//    }
//    if ([coupon objectForKey:@"maxDenomination"]) {
//        self.m_detailModel.shareInfo.maxDenomination = [coupon objectForKey:@"maxDenomination"];
//    }
//}
//NSMutableDictionary *qrcodeParams = [NSMutableDictionary dictionary];
//if (kValidString(self.commodityUrl)) {
//    [qrcodeParams setObject:self.commodityUrl forKey:@"id"];
//}
#pragma mark - HomeSpecialSellSingleInfoCellDelegate
- (void)homeSpecialSellSingleCellCarrayModel:(HomeListStructInfoModel *)carrayModel isShare:(BOOL)isShare {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (isShare) {
        YYCompositeShareInfoModel *shareModel = carrayModel.shareInfo;
        if (kValidDictionary(carrayModel.item.coupon)) {
            NSDictionary *coupon = carrayModel.item.coupon;
            if ([coupon objectForKey:@"discountPrice"]) {
                shareModel.discountPrice = [coupon objectForKey:@"discountPrice"];
            }
            if ([coupon objectForKey:@"maxDenomination"]) {
                shareModel.maxDenomination = [coupon objectForKey:@"maxDenomination"];
            }
        }
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (kValidString(carrayModel.item.commission)) {
//            [params setObject:shareModel.commission
//                       forKey:@"commission"];
            [params setObject:carrayModel.item.commission
            forKey:@"commission"];
        }
        NSMutableDictionary *qrcodeParams = [NSMutableDictionary dictionary];
        if (kValidString(carrayModel.item.url)) {
            [qrcodeParams setObject:carrayModel.item.url forKey:@"id"];
        }
        [kWholeConfig produceGoodQRCodeRequest:qrcodeParams
                                       success:^(NSDictionary *responDict) {
                                           if ([responDict objectForKey:@"goodDetailQRcode"]) {
                                               shareModel.qrcodePictureUrl = [responDict objectForKey:@"goodDetailQRcode"];
                                               [YYCommonTools goodShareOperator:shareModel
                                                                   expandParams:params];
                                           }
                                       }];
    }
    else {
        [params setObject:@(1) forKey:@"linkType"];
        if (kValidString(carrayModel.item.url)) {
            [params setObject:carrayModel.item.url forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:self params:params];
    }
}

#pragma mark - HomeSpecialSellDoubleInfoCellDelegate
- (void)homeSpecialSellDoubleCellCarrayModel:(id<HomeGoodItemProtocol>)carrayModel isShare:(BOOL)isShare {
    [self homeSpecialSellSingleCellCarrayModel:carrayModel isShare:isShare];
}

#pragma mark - HomeBrandHotInfoCellDelegate
- (void)homeBrandHotInfoCell:(HomeBrandHotInfoCell *)cell didSelectAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isDetail = NO;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:cell];
    HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:currentIndex.row];
    if (indexPath) {
        if (currentIndex.row < self.listData.count) {
            HomeListItemGoodInfoModel *goodModel = [itemModel.items objectAtIndex:indexPath.row];
            if (goodModel.scrollType == YYSingleRowScrollEmpty) {
                // 点击查看更多
                isDetail = NO;
            }
            else {
                // 进商品详情
                isDetail = YES;
                [params setObject:@(1) forKey:@"linkType"];
                if (kValidString(goodModel.url)) {
                    [params setObject:goodModel.url forKey:@"url"];
                }
            }
        }
    }
    if (!isDetail) {
        if ([itemModel.link objectForKey:@"linkType"]) {
            [params setObject:[itemModel.link objectForKey:@"linkType"] forKey:@"linkType"];
        }
        if ([itemModel.link objectForKey:@"uri"]) {
            [params setObject:[itemModel.link objectForKey:@"uri"] forKey:@"url"];
        }
    }
    [YYCommonTools skipMultiCombinePage:self params:params];
}

#pragma mark - HomeHeaderViewDelegate
- (void)headerViewItemDidSelect:(HomeHeaderInfoView *)view sourceModel:(HomeResourceItemInfoModel *)sourceModel {
    if (sourceModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (kValidDictionary(sourceModel.link)) {
            if ([sourceModel.link objectForKey:@"linkType"]) {
                [params setObject:[sourceModel.link objectForKey:@"linkType"] forKey:@"linkType"];
            }
            if ([sourceModel.link objectForKey:@"uri"]) {
                if ([[sourceModel.link objectForKey:@"linkType"] integerValue] == 18) {
                    NSString *content = [sourceModel.link objectForKey:@"uri"];
                    if (kValidString(content)) {
                        content = [content cutStringUseSpecialChar:@"="];
                        [params setObject:content forKey:@"url"];
                    }
                }
                else {
                    [params setObject:[sourceModel.link objectForKey:@"uri"] forKey:@"url"];
                }
            }
            [YYCommonTools skipMultiCombinePage:self params:params];
        }
    }
}

#pragma mark - HomeFeatureSectionInfoViewDelegate
- (void)homeFeatureSectionView:(HomeFeatureSectionInfoView *)sectionView didSelectItemAtIndex:(NSInteger)itemIndex {
    CGFloat currentOffsetY = self.m_tableView.contentOffset.y;
    CGFloat headerY = [self.m_tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].origin.y;
    [self.sectionView willMenuBarItemSelectAtIndex:itemIndex];
   
    [self getSelectTimeObjectOrIndex:itemIndex];
    [self homeScreeningShare];
    
    self.isClickSwitch = YES;
    if (headerY < currentOffsetY) {
        // 没有悬停不需要滚动到顶部
        if ([self.m_tableView numberOfRowsInSection:0] > 0) {
            [self.m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        }
    }
    
    [self loadPullDownPage];
    if (kIsFan||!kValidArray(self.listData)) return;
    [[YYPopToastView shared] hideToastAnimation];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    [self judgeScrollDirection:scrollView];
    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat offsetY = scrollView.contentOffset.y;
    CGRect sectionHeader = [self.m_tableView rectForHeaderInSection:0];
    if (self.sectionHeaderScrollBlock) {
        if (self.isClickSwitch) {
            return;
        }
        CGFloat diffValue = sectionHeader.origin.y - offsetY;
        self.isHover = (diffValue < 0 ? YES : NO);
//        self.sectionHeaderScrollBlock(CGPointMake(offsetX, diffValue));
    }
    if (self.navigationGradientBlock) {
        self.navigationGradientBlock(self.isExistBanner, scrollView.contentOffset, self.pageIndex);
    }
    if (offsetY <= 0) {
        self.isDisableAninate = YES;
    }
    else {
        self.isDisableAninate = NO;
    }
    if (kIsFan||!kValidArray(self.listData)) return;
    [[YYPopToastView shared] hideToastAnimation];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (!self.isDisableAninate) {
        [UIView animateWithDuration:1.0
                         animations:^{
                             self.gifAnimateView.transform = CGAffineTransformMakeTranslation(kSizeScale(93-25), 0);
                         } completion:^(BOOL finished) {
                         }];
        if (kIsFan||!kValidArray(self.listData)) return;
        [[YYPopToastView shared] hideToastAnimation];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        CGFloat offsetY = scrollView.contentOffset.y;
        if (self.scrollStopBlock) {
            self.scrollStopBlock(scrollView.contentOffset, self.pageIndex, self.isExistBanner, self.isHover);
        }
        CGRect sectionHeader = [self.m_tableView rectForHeaderInSection:0];
        [UIView animateWithDuration:1.0
                         animations:^{
                             self.gifAnimateView.transform = CGAffineTransformIdentity;
                         } completion:^(BOOL finished) {
                             
                         }];
        if (kIsFan||!kValidArray(self.listData)) return;
        [[YYPopToastView shared] showToastView:self.sectionView
                                 showCondition:(offsetY > sectionHeader.origin.y)?YES:NO clickBlock:^{
                                     [YYCommonTools goodShareOperator:self.screenShareModel
                                                         expandParams:nil];
                                 }];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 快速滑动，慢慢停下后调用
    CGFloat offsetY = scrollView.contentOffset.y;
    if (self.scrollStopBlock) {
        self.scrollStopBlock(scrollView.contentOffset, self.pageIndex, self.isExistBanner, self.isHover);
    }
    CGRect sectionHeader = [self.m_tableView rectForHeaderInSection:0];
    [UIView animateWithDuration:1.0
                     animations:^{
                         self.gifAnimateView.transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                         
                     }];
    if (kIsFan||!kValidArray(self.listData)) return;
    [[YYPopToastView shared] showToastView:self.sectionView
                             showCondition:(offsetY > sectionHeader.origin.y)?YES:NO clickBlock:^{
                                 [YYCommonTools goodShareOperator:self.screenShareModel
                                                     expandParams:nil];
                             }];
}
@end
