//
//  YY_GoodDetailDetailViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_GoodDetailDetailViewController.h"

#import "HomeGoodInfoModel.h"
#import "HomeBannerInfoModel.h"
#import "GoodDetailPhotoInfoCell.h"
#import "GoodDetailSubHeaderView.h"
@interface YY_GoodDetailDetailViewController ()

@property (nonatomic, assign) BOOL                    canScroll;
@property (nonatomic, strong) GoodDetailSubHeaderView *headerView;

@end

@implementation YY_GoodDetailDetailViewController

#pragma mark - Setter and Getter

- (void)setParameter:(NSDictionary *)parameter {
    if (!kValidDictionary(parameter)) {
        return;
    }
    if ([parameter objectForKey:@"detailImages"]) {
        self.listData = [[parameter objectForKey:@"detailImages"] mutableCopy];
    }
    if ([parameter objectForKey:@"isHideSource"]) {
        BOOL isHideSource = [[parameter objectForKey:@"isHideSource"] boolValue];
        NSMutableDictionary *couponDict = [YYCommonTools getAssignResourcesUseOfType:17];
        if (kValidDictionary(couponDict)) {
            CGFloat height = 0.0;
            HomeBannerInfoModel *model = [HomeBannerInfoModel modelWithJSON:couponDict];
            if (kValidString(model.imageUrl)) {
                height = ([UIImage getImageSizeWithURL:model.imageUrl].height)/2.0;
            }
            self.headerView = [[GoodDetailSubHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, (model.switchFlag && !isHideSource) ? height : 0.0)];
            [self.headerView setSubHeaderView:couponDict];
        }
        [self.m_tableView layoutIfNeeded];
        self.m_tableView.tableHeaderView = self.headerView;
    }
    [self reloadTableView];
}

- (GoodDetailSubHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[GoodDetailSubHeaderView alloc] initWithFrame:CGRectZero];
    }
    return _headerView;
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_hideNavigation:YES];
    
    Class currentCls = [GoodDetailPhotoInfoCell class];
    registerClass(self.m_tableView, currentCls);
    self.m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(12));
        make.left.right.bottom.equalTo(self.view);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailLeaveTopNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailArriveTopNotification object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Private method
- (void)goodDetailNotificationHandler:(NSNotification *)notification {
    NSString *notificationName = notification.name;
    if ([notificationName isEqualToString:GoodDetailArriveTopNotification]) {
        self.canScroll = YES;
        self.m_tableView.showsVerticalScrollIndicator = YES;
    }else if([notificationName isEqualToString:GoodDetailLeaveTopNotification]){
        self.m_tableView.contentOffset = CGPointZero;
        self.canScroll = NO;
        self.m_tableView.showsVerticalScrollIndicator = NO;
    }
}

/**
 *  预览保存详情图片
 */
- (void)preSaveDetailPhotos:(GoodDetailPhotoInfoCell *)photoCell {
    UIView *fromView;
    NSMutableArray *items = [NSMutableArray new];
    NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:photoCell];
    
    for (NSUInteger i = 0, max = self.listData.count; i < max; i++) {
        HomeGoodImageInfoModel *imageModel = [self.listData objectAtIndex:i];
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = photoCell;
        item.largeImageURL = [NSURL URLWithString:imageModel.url?:imageModel.imageUrl];
        
        [items addObject:item];
        if (i == currentIndex.row) {
            fromView = photoCell;
        }
    }
    YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:currentIndex.row];
    [v presentFromImageView:fromView toContainer:kAppWindow animated:YES completion:nil];
    v.hideViewBlock = ^(NSInteger currentPage) {
    };
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        Class currentCls = [GoodDetailPhotoInfoCell class];
        GoodDetailPhotoInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (row < self.listData.count && kValidArray(self.listData)) {
            HomeGoodImageInfoModel *imageModel = [self.listData objectAtIndex:row];
            cell.imageURL = imageModel.url?:imageModel.imageUrl;
        }
        cell.photoClick = ^(GoodDetailPhotoInfoCell *currentCell) {
            @strongify(self);
            [self preSaveDetailPhotos:currentCell];
        };
        return cell;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat scale = 1.0;
    NSInteger row = indexPath.row;
    CGFloat width = 0.0, height = 0.0;
    if (row < self.listData.count && kValidArray(self.listData)) {
        HomeGoodImageInfoModel *imageModel = [self.listData objectAtIndex:row];
        width = [imageModel.width floatValue]/2.0;
        height = [imageModel.height floatValue]/2.0;
        scale = width/kScreenW;   //宽度最大值为屏幕宽度，高度进行缩放。
        if (scale > 1.0) {
            scale = kScreenW/width;
            height = floor(height *scale);
        }
        else {
            scale = kScreenW/width;
            height = floor(height *scale);
        }
        return height;
    }
    return 10.0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //[super scrollViewDidScroll:scrollView];
    [self judgeScrollDirection:scrollView];
    if (!self.canScroll) {
        [scrollView setContentOffset:CGPointZero];
    }
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        //表示页面向下，则发送通知告知离开顶部。
        kPostNotification(GoodDetailLeaveTopNotification, nil);
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
