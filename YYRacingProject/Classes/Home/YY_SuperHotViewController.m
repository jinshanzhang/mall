//
//  YY_SuperHotViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_SuperHotViewController.h"

#import "HomeBrandHotInfoCell.h"
#import "HomeSuperHotInfoCell.h"

#import "RecommendHeaderView.h"
#import "MineOrderSourceVivew.h"

#import "yyCountDownManager.h"
#import "HomeListItemGoodInfoModel.h"

#import "HomeSuperHotItemGoodInfoModel.h"
#import "YYHomeSuperHotInfoRequestAPI.h"

@interface YY_SuperHotViewController ()
<
HomeSuperHotInfoCellDelegate,
HomeBrandHotInfoCellDelegate>
@property (nonatomic, strong) RecommendHeaderView *sectionHeader;
@property (nonatomic, strong) MineOrderSourceVivew  *headerView;
@property (nonatomic, strong) NSMutableArray *superListData;

@end

@implementation YY_SuperHotViewController

#pragma mark - Setter method
- (MineOrderSourceVivew *)headerView {
    if (!_headerView) {
        _headerView = [[MineOrderSourceVivew alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 0.0001f)];
    }
    return _headerView;
}


#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.tableStyle = UITableViewStyleGrouped;
        self.NoDataType = YYEmptyViewAlxeNoDataType;
        [kConfigCountDown setCountDownTimeInterval:0.1f];
        [kConfigCountDown addSourceWithIdentifier:@"superHotCtrl"];
        [kConfigCountDown start];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"超级爆款"];
    [self xh_popTopRootViewController:NO];
    
    self.superListData = [NSMutableArray array];
    
    Class currentCls = [HomeBrandHotInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSuperHotInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    self.m_tableView.tableHeaderView = self.headerView;
    [self loadNewer];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [kConfigCountDown removeSourceWithIdentifier:@"superHotCtrl"];
}

#pragma mark - Base Request
- (YYBaseRequestAPI *)baseRequest {
    YYHomeSuperHotInfoRequestAPI *superHotAPI = [[YYHomeSuperHotInfoRequestAPI alloc] initHomeSuperHotInfoRequest:[[NSDictionary dictionary] mutableCopy]];
    return superHotAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    [self.superListData removeAllObjects];
    if ([responseDic objectForKey:@"items"]) {
        NSArray *tempItems = [NSArray modelArrayWithClass:[HomeSuperHotItemGoodInfoModel class] json:[responseDic objectForKey:@"items"]];
        if (kValidArray(tempItems)) {
            __block NSMutableArray *firstSection = [NSMutableArray array];
            __block NSMutableArray *secondSection = [NSMutableArray array];
            [tempItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeSuperHotItemGoodInfoModel *model = (HomeSuperHotItemGoodInfoModel *)obj;
                __block NSNumber *saleStatus = model.saleStatus;
                [model.items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    HomeListItemGoodInfoModel *itemModel = (HomeListItemGoodInfoModel *)obj;
                    itemModel.couponLogic = @(1);
                    itemModel.saleStatus = saleStatus;
                }];
                NSInteger status = [model.saleStatus integerValue];
                model.scrollType = YYSingleRowScrollHomeBrand;
                HomeListItemGoodInfoModel *emptyModel = [[HomeListItemGoodInfoModel alloc] init];
                emptyModel.scrollType = YYSingleRowScrollEmpty;
                [model.items addObject:emptyModel];
                if (status == 3) {
                    // 马上抢
                    [firstSection addObject:model];
                }
                else if (status == 2) {
                    // 即将开始
                    [secondSection addObject:model];
                }
            }];
            if (kValidArray(firstSection)) {
                [self.superListData addObject:firstSection];
            }
            if (kValidArray(secondSection)) {
                [self.superListData addObject:secondSection];
            }
            if ([responseDic objectForKey:@"bannerUri"]) {
                CGFloat height = 0.0;
                HomeBannerInfoModel *bannerModel = nil;
                CGRect bannerFrame = CGRectMake(0, 0, kScreenW, 0.0001f);
                NSString *bannerUrl = [responseDic objectForKey:@"bannerUri"];
                if (kValidString(bannerUrl)) {
                    bannerModel = [[HomeBannerInfoModel alloc] init];
                    bannerModel.imageUrl = bannerUrl;
                    bannerModel.isOpenBottomLine = YES;
                    height = [UIImage getImageSizeWithURL:bannerUrl].height;
                    bannerModel.height = height;
                    bannerFrame = CGRectMake(0, 0, kScreenW, kSizeScale((height/2.0)+8));
                }
                self.headerView.infoModel = bannerModel;
                self.headerView.frame = bannerFrame;
                [self.m_tableView layoutIfNeeded];
                self.m_tableView.tableHeaderView = self.headerView;
            }
        }
        return tempItems;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return NO;
}

- (void)loadPullDownPage {
    [super loadPullDownPage];
    [kConfigCountDown reloadSourceWithIdentifier:@"superHotCtrl"];
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.superListData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempArray = [self.superListData objectAtIndex:section];
        return tempArray.count;
    }
    else
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger type = -1;
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    HomeSuperHotItemGoodInfoModel *itemModel = nil;
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempListData = [self.superListData objectAtIndex:section];
        if (kValidArray(tempListData) && row < tempListData.count) {
            itemModel = [tempListData objectAtIndex:row];
        }
    }
    if (itemModel) {
        type = [itemModel.superTrendyType integerValue];
    }
    Class cls = [HomeSuperHotInfoCell class];
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        if (type == 0) {
            HomeSuperHotInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            cell.delegate = self;
            [self configSingleGoodCell:cell atIndexPath:indexPath];
            return cell;
        }
        else if (type == 1) {
            cls = [HomeBrandHotInfoCell class];
            HomeBrandHotInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            cell.delegate = self;
            [self configBrandGoodCell:cell atIndexPath:indexPath];
            return cell;
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger type = -1;
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    HomeSuperHotItemGoodInfoModel *itemModel = nil;
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempListData = [self.superListData objectAtIndex:section];
        if (kValidArray(tempListData) && row < tempListData.count) {
            itemModel = [tempListData objectAtIndex:row];
        }
    }
    if (itemModel) {
        type = [itemModel.superTrendyType integerValue];
    }
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        if (type == 0) {
            height = [tableView fd_heightForCellWithIdentifier:@"HomeSuperHotInfoCell" configuration:^(HomeSuperHotInfoCell *cell) {
                [self configSingleGoodCell:cell atIndexPath:indexPath];
            }];
        }
        else if (type == 1) {
            height = [tableView fd_heightForCellWithIdentifier:@"HomeBrandHotInfoCell" configuration:^(HomeBrandHotInfoCell *cell) {
                [self configBrandGoodCell:cell atIndexPath:indexPath];
            }];
        }
        return height;
    }
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) {
        if (!self.sectionHeader) {
            self.sectionHeader = [[RecommendHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(38))];
            self.sectionHeader.showStyle = RecommendShowSuperHotStyle;
        }
        self.sectionHeader.titleLabel.text = @"即将开抢";
        return self.sectionHeader;
    }
    else
        return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0? kSizeScale(0.00001f): kSizeScale(38));
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.000001f;
}

#pragma mark - Private method
/**
 品牌cell赋值
 **/
- (void)configBrandGoodCell:(HomeBrandHotInfoCell *)cell
                atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    HomeSuperHotItemGoodInfoModel *itemModel = nil;
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempListData = [self.superListData objectAtIndex:section];
        if (kValidArray(tempListData) && row < tempListData.count) {
            itemModel = [tempListData objectAtIndex:row];
        }
    }
    //cell.brandModel = itemModel;
}

/**
 单品cell赋值
 **/
- (void)configSingleGoodCell:(HomeSuperHotInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    HomeSuperHotItemGoodInfoModel *itemModel = nil;
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempListData = [self.superListData objectAtIndex:section];
        if (kValidArray(tempListData) && row < tempListData.count) {
            itemModel = [tempListData objectAtIndex:row];
        }
    }
    cell.itemModel = itemModel;
}
#pragma mark - HomeSuperHotInfoCellDelegate
- (void)homeSuperHotClickItemCell:(HomeSuperHotInfoCell *)cell {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:cell];
    NSInteger row = currentIndex.row;
    NSInteger section = currentIndex.section;
    HomeSuperHotItemGoodInfoModel *itemModel = nil;
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempListData = [self.superListData objectAtIndex:section];
        if (kValidArray(tempListData) && row < tempListData.count) {
            itemModel = [tempListData objectAtIndex:row];
        }
    }
    if (itemModel) {
        [params setObject:itemModel.linkType forKey:@"linkType"];
        [params setObject:itemModel.uri forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:self params:params];
}

#pragma mark - HomeBrandHotInfoCellDelegate
- (void)homeBrandHotInfoCell:(HomeBrandHotInfoCell *)cell didSelectAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isDetail = YES;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    // 当前列表index
    NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:cell];
    NSInteger row = currentIndex.row;
    NSInteger section = currentIndex.section;
    HomeSuperHotItemGoodInfoModel *itemModel = nil;
    
    if (kValidArray(self.superListData) && section < self.superListData.count) {
        NSMutableArray *tempListData = [self.superListData objectAtIndex:section];
        if (kValidArray(tempListData) && row < tempListData.count) {
            itemModel = [tempListData objectAtIndex:row];
        }
    }
    if (kValidArray(itemModel.items) && indexPath.row < itemModel.items.count) {
        // 从indexPath 取数据
        HomeListItemGoodInfoModel *infoModel = [itemModel.items objectAtIndex:indexPath.row];
        if (infoModel.scrollType ==  YYSingleRowScrollEmpty) {
            isDetail = NO;
        }
        if (isDetail) {
            [params setObject:infoModel.linkType forKey:@"linkType"];
            [params setObject:infoModel.uri forKey:@"url"];
        }
    }
    if (!isDetail || !indexPath) {
        [params setObject:itemModel.linkType forKey:@"linkType"];
        [params setObject:itemModel.skipBrandTrendyUri forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:self params:params];
}

@end

