//
//  DetailColSwitchInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "DetailColSwitchInfoModel.h"

@implementation DetailColSwitchInfoModel

+ (instancetype)initDetailColClass:(NSString *)content {
    DetailColSwitchInfoModel *switchModel = [[DetailColSwitchInfoModel alloc] init];
    switchModel.content = content;
    return switchModel;
}

- (NSString *)itemContent {
    return self.content;
}

@end
