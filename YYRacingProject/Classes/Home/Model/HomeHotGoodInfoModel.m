//
//  HomeHotGoodInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeHotGoodInfoModel.h"

@implementation HomeHotGoodInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"nTagIcon" : @"newTagIcon"};
}

@end
