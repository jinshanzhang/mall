//
//  HomeListStructInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeListItemsInfoModel.h"
NS_ASSUME_NONNULL_BEGIN
// 首页列表结构
@interface HomeListStructInfoModel : NSObject
//1：商品一排一；0：商品一排二；2：品牌特卖 3：标题  4: 去掉分享
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) YYSingleRowScrollType scrollType;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableDictionary *link;
@property (nonatomic, strong) HomeListItemGoodInfoModel *item;
@property (nonatomic, strong) YYImageInfoModel *coverImage; //封面信息
@property (nonatomic, strong) YYCompositeShareInfoModel *shareInfo; //分享的数据

@end

NS_ASSUME_NONNULL_END
