//
//  HomeGoodPropSkuInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeGoodPropSkuInfoModel.h"
#import "HomeGoodInfoModel.h"

@implementation HomeGoodPropSkuInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"values":[HomeGoodValueSkuInfoModel class]};
}

@end


@implementation HomeGoodValueSkuInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"childArrs":[HomeGoodValueSkuInfoModel class],
             @"priceInfo":[HomeGoodPriceInfoModel class]};
}

@end


