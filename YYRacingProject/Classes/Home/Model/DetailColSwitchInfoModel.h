//
//  DetailColSwitchInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailColSwitchInfoModel : NSObject <JMMenuBarItemContentProtocol>

@property (nonatomic, strong) NSString *content;
+ (instancetype)initDetailColClass:(NSString *)content;


@end

NS_ASSUME_NONNULL_END
