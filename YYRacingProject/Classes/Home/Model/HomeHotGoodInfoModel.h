//
//  HomeHotGoodInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeHotGoodInfoModel : NSObject

@property (nonatomic, assign) NSInteger comId;
@property (nonatomic, copy) NSString *url;           //web 链接
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *originPrice;
@property (nonatomic, copy) NSString *reservePrice;  // 最终价
@property (nonatomic, copy) NSString *commission;
@property (nonatomic, copy) NSString *sourceIcon;    // 国家图片
@property (nonatomic, copy) NSString *salesVolume;    //搜索显示的购买人数

@property (nonatomic, copy) NSString *shareUrl;      // 分享URL
@property (nonatomic, assign) NSInteger specialSellStatus; // 1预  2 热卖
@property (nonatomic, assign) NSInteger specialSellDate;
@property (nonatomic, assign) NSInteger specialSellStartTime;

@property (nonatomic, copy) NSString *goodsTitle;
@property (nonatomic, copy) NSString *goodsShortTitle;
@property (nonatomic, copy) NSString *goodsShareImage;
@property (nonatomic, copy) NSString *shopName;
@property (nonatomic, copy) NSString *shopAvatorUrl;

@property (nonatomic, assign) NSInteger historyFlag;  //排列样式
@property (nonatomic, assign) NSInteger itemSaleType; //特卖状态
@property (nonatomic, copy) NSString *tagIcon;        //爆款图片
@property (nonatomic, copy) NSString *tommorowIcon;   //预告
@property (nonatomic, copy) NSString *nTagIcon;       //mark Icon

@property (nonatomic, assign) BOOL   isSearch; 
@end
