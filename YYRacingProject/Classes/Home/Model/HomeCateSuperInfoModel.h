//
//  HomeCateSuperInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HomeCateSubInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeCateSuperInfoModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *superID;

@property (nonatomic, strong) NSMutableArray *leve3Cats;
@property (nonatomic, assign) BOOL   isSelect; // 是否选中

@property (nonatomic, strong) NSMutableArray *childArrs;
@end

NS_ASSUME_NONNULL_END
