//
//  HomeListItemsInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeListItemGoodInfoModel.h"
NS_ASSUME_NONNULL_BEGIN
// 首页items 
@interface HomeListItemsInfoModel : NSObject<HomeBrandGoodItemProtocol>

@property (nonatomic, strong) NSMutableArray *items;


@property (nonatomic, strong) YYImageInfoModel *coverImage; //品牌特卖封面

@property (nonatomic, strong) NSNumber *linkType; //链接类型
@property (nonatomic, copy)   NSString *url;      //跳转链接

@property (nonatomic, strong) NSIndexPath           *currentIndex;
@property (nonatomic, assign) YYSingleRowScrollType scrollType;

@end

NS_ASSUME_NONNULL_END
