//
//  HomeSuperHotItemGoodInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface HomeSuperHotInfoModel : NSObject <HomeSuperHotProtocol>
@property (nonatomic, strong) NSNumber *showSwitch;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) YYSingleRowScrollType scrollType;

@end

@interface HomeSuperHotItemGoodInfoModel : NSObject <HomeSuperHotItemProtocol>

@property (nonatomic, strong) YYImageInfoModel  *coverImage; //封面信息;
@property (nonatomic, strong) NSNumber  *superTrendyType;  //0 单品  1品牌
@property (nonatomic, copy)   NSString  *sellpoint;      //卖点
@property (nonatomic, copy)   NSString  *title;
@property (nonatomic, copy)   NSString  *couponPrice;
@property (nonatomic, copy)   NSString  *originPrice;
@property (nonatomic, copy)   NSString  *reservePrice;
@property (nonatomic, copy)   NSString  *commission;
@property (nonatomic, copy)   NSString  *sellOutFlagUri; //为null或者为空表示没有售罄
@property (nonatomic, copy)   NSString  *rightIcon;
@property (nonatomic, copy)   NSString  *uri;
@property (nonatomic, copy)   NSString  *skipBrandTrendyUri; //跳转品牌uri

@property (nonatomic, strong) NSNumber  *couponFlag; //0 没有优惠券  1 有优惠券
@property (nonatomic, strong) NSNumber  *startTime;
@property (nonatomic, strong) NSNumber  *endTime;
@property (nonatomic, strong) NSNumber  *currentTime;
@property (nonatomic, strong) NSNumber  *saleStatus; // 3 马上抢   2 即将开始
@property (nonatomic, strong) NSNumber  *linkType;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) YYSingleRowScrollType scrollType;
@end

NS_ASSUME_NONNULL_END
