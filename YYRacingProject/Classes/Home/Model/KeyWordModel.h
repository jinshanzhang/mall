//
//  KeyWordModel.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyWordModel : NSObject

@property(nonatomic,copy) NSString *word;

@property (nonatomic, copy) NSString *linkUrl;
//是否高亮 1-yes 2-no
@property (nonatomic, assign) int type;

@property (nonatomic, assign) int linkType;//跳转类型1-4：和之前的跳转方式类似5：普通搜索[cw1]

@end
