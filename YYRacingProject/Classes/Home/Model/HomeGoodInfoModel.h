//
//  HomeGoodInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeGoodInfoModel : NSObject

// baseInfo
@property (nonatomic, copy) NSString   *title;  //商品名称
@property (nonatomic, copy) NSString   *volumeText;//多少人已买
@property (nonatomic, strong) NSMutableArray *auctionImages; //商品详情顶部banner
@property (nonatomic, strong) NSMutableArray *priceInfo;     //价格列表
@property (nonatomic, strong) NSMutableArray *serviceGuarantee; //服务标准列表
@property (nonatomic, copy) NSString   *sellCountTitle; //售卖文案
@property (nonatomic, copy) NSString   *storageTitle;   //库存文案
@property (nonatomic, assign) NSInteger limitTotal;     //限购件数
@property (nonatomic, strong) NSMutableArray *tags;     //商品标题标签
@property (nonatomic, strong) NSNumber  *itemStatus;    //售卖状态 0:已抢光；1：在售中；2：已下架

// other
@property (nonatomic, strong) NSMutableArray *itemCoupons;   //商品单品券
@property (nonatomic, assign) NSDictionary   *saleInfo;      //特卖信息
@property (nonatomic, assign) long currentTime;
@property (nonatomic, assign) long startTime;
@property (nonatomic, assign) long endTime;
@property (nonatomic, assign) BOOL isSuperTrendy;   // 是否是超级爆款
@property (nonatomic, assign) NSInteger specialType;  // 特卖类型 (0:普通特卖 1:新人特卖)


@property (nonatomic, assign) YYGoodSalingStatusType salingType; //商品特卖类型

// skuBase
@property (nonatomic, strong) NSNumber  *comId;
@property (nonatomic, strong) NSMutableArray *skus;          //匹配所有sku
@property (nonatomic, strong) NSMutableArray *props;         //修改过的属性
@property (nonatomic, strong) NSMutableArray *skuInfo;       //sku 对应的库存，价格信息

@property (nonatomic, strong) NSMutableArray *detailImages;  //商品详情描述
@property (nonatomic, strong) NSMutableArray *propsInfo;     //商品参数
@property (nonatomic, strong) NSMutableArray *salesImage;

@property (nonatomic, copy) NSString  *notesLink;            //商品须知链接

@property (nonatomic, assign) BOOL           isSwitchBar;    //是否开启切换bar
@property (nonatomic, assign) NSNumber       *barSwitch;     //0:不展示特殊bar；1：展示特殊bar
@property (nonatomic, strong) YYCompositeShareInfoModel *shareInfo; //分享的数据

@property (nonatomic, strong) NSMutableArray *tabNames;      //tab 标题
@property (nonatomic, strong) NSNumber      *itemType;//商品类型：1：普通商品；2：优惠券商品；3：课程商品
//是否是蜀黍专区
@property (nonatomic, assign) BOOL isMealZonePay;

// 自增
@property (nonatomic, strong) NSString       *skuDefaultPrice;    //sku默认价格
@property (nonatomic, strong) NSString       *skuCommissionPrice; //sku默认佣金展示
@property (nonatomic, strong) NSString       *skuDefaultOriginPirce;//sku默认原价
@property (nonatomic, strong) NSNumber *commissionRate; //佣金

@property (nonatomic, copy) NSString  *nowPrice;
@property (nonatomic, copy) NSString  *originPrice;
@property (nonatomic, copy) NSString  *activityPrice;
@property (nonatomic, copy) NSString  *commissionPrice;

@property (nonatomic, copy) NSString  *preTimeFormat;
@property (nonatomic, assign) NSInteger isRangeFlag;         //价格是否存在区间值
@property (nonatomic, assign) NSInteger currentValue;        //当前值

@property (nonatomic, copy) NSString  *priceMaxValue;
@property (nonatomic, copy) NSString  *priceMinValue;
@property (nonatomic, copy) NSString  *originMaxValue;
@property (nonatomic, copy) NSString  *originMinValue;
@property (nonatomic, copy) NSString  *commissionMaxValue;   //佣金最大值
@property (nonatomic, copy) NSString  *commissionMinValue;   //佣金最小值

@property (nonatomic, strong) NSMutableArray *originProps;
@property (nonatomic, strong) NSString       *propTipInfo;   //请选择属性---- 提示
@property (nonatomic, strong) NSMutableDictionary *coupon;
@property (nonatomic, strong) NSDictionary *activityInfo;

@property (nonatomic, strong) NSNumber * goodsType;

@end

// 商品价格
@interface HomeGoodPriceInfoModel : NSObject

@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *priceTag;
@property (nonatomic, assign) NSInteger priceType;  // 1是原价 2现价 3活动价
@property (nonatomic, assign) NSInteger isRangeFlag;

@property (nonatomic, strong) NSString *commissionPrice;
@property (nonatomic, strong) NSString *commissionPriceTag;

@end

// 商品参数
@interface HomeGoodArgumentInfoModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *value;

@end


// 商品图片
@interface HomeGoodImageInfoModel : NSObject

@property (nonatomic, copy) NSString *width;
@property (nonatomic, copy) NSString *height;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *imageUrl;

@end

// 基类 SKU
@interface HomeGoodBaseSkuInfoModel: NSObject
@property (nonatomic, strong) NSNumber *skuId;
@property (nonatomic, copy)   NSString *propPath;
@end

// SKU 携带的其他信息
@interface HomeGoodSkuOtherInfoModel: NSObject
@property (nonatomic, strong) NSNumber *skuId;
@property (nonatomic, strong) NSNumber *storage;
@property (nonatomic, copy)   NSString *skuUrl;
@property (nonatomic, strong) NSMutableArray *priceInfo;
@end

// coupon
@interface HomeGoodCouponInfoModel: NSObject
@property (nonatomic, copy) NSString *discountPrice;
@property (nonatomic, copy) NSString *storage;
@property (nonatomic, assign) int *type;
@property (nonatomic, strong) NSArray *couponStrList;
@property (nonatomic, strong) NSArray *itemCouponV2s;
@end
// coupon
@interface HomeGoodActivityInfoModel: NSObject

@property (nonatomic, assign) int showSwitch;
@property (nonatomic, strong) NSArray *items;

@end



