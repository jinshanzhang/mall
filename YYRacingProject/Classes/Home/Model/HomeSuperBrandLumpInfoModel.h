//
//  HomeSuperBrandLumpInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeListItemGoodInfoModel,HomeSingleSuperBrandLumpInfoModel;

@interface HomeSuperBrandLumpInfoModel : NSObject

@property (nonatomic, strong) NSNumber *showSwitch;
@property (nonatomic, strong) NSNumber *type;

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *brandIds;
@end


@interface HomeSingleSuperBrandLumpInfoModel : NSObject

@property (nonatomic, strong) YYImageInfoModel *coverImage; //图片资源
@property (nonatomic, strong) YYImageInfoModel *fullBackgroundImage; //大图资源

@property (nonatomic, strong) NSDictionary *link;
@property (nonatomic, strong) NSString *brandTime;

@property (nonatomic, strong) NSMutableArray *items;
@end


NS_ASSUME_NONNULL_END
