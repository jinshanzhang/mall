//
//  HomeBannerInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeBannerInfoModel.h"

@implementation HomeBannerInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"resourceID" : @"id"};
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.url forKey:@"url"];
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [aCoder encodeObject:self.fontColor forKey:@"fontColor"];
    
    [aCoder encodeFloat:self.width forKey:@"width"];
    [aCoder encodeFloat:self.height forKey:@"height"];
    
    [aCoder encodeInteger:self.type forKey:@"type"];
    [aCoder encodeInteger:self.popType forKey:@"popType"];
    [aCoder encodeInteger:self.resourceID forKey:@"resourceID"];
    [aCoder encodeInteger:self.linkType forKey:@"linkType"];
    
    [aCoder encodeBool:self.switchFlag forKey:@"switchFlag"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        
        self.url = [aDecoder decodeObjectForKey:@"url"];
        self.text = [aDecoder decodeObjectForKey:@"text"];
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.imageUrl = [aDecoder decodeObjectForKey:@"imageUrl"];
        self.fontColor = [aDecoder decodeObjectForKey:@"fontColor"];
        
        self.width = [aDecoder decodeFloatForKey:@"width"];
        self.height = [aDecoder decodeFloatForKey:@"height"];
        
        self.type = [aDecoder decodeIntegerForKey:@"type"];
        self.popType = [aDecoder decodeIntegerForKey:@"popType"];
        self.resourceID = [aDecoder decodeIntegerForKey:@"resourceID"];
        self.linkType = [aDecoder decodeIntegerForKey:@"linkType"];
        
        self.switchFlag = [aDecoder decodeBoolForKey:@"switchFlag"];
       
    }
    return self;
}

@end
