//
//  HomeResourcesInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/12.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeResourceItemInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeResourcesInfoModel : NSObject <ResourcesViewProtocol>

/**
 BANNER(1, "banner"),
 ICON(2, "icon"),
 CAPSULE(3, "胶囊位"),
 BOOTHS(4, "八宫格"),
 SERVICE_GUARANTEE(5, "服务保障"),
 SUPER_TRENDY(6, "超级爆款"),
 BACKGROUND_IMAGE(7, "超级背景图"),
 BANNER_BACKGROUND_IMAGE(8, "banner背景图"),
 ICON_BACKGROUND_IMAGE(9, "icon背景图"),
 SUPER_BRAND(11,"超级品牌团"),
 DEFAULT(0, "未定义");
 **/
@property (nonatomic, strong) NSNumber *showSwitch;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, strong) NSDictionary *link;
@property (nonatomic, strong) YYImageInfoModel *bgImage; //背景信息

@property (nonatomic, assign) YYSingleRowScrollType scrollType;
@property (nonatomic, strong) HomeResourceItemInfoModel *item;

@end

NS_ASSUME_NONNULL_END
