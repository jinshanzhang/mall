//
//  HomeGoodInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeGoodInfoModel.h"

@implementation HomeGoodInfoModel

@end

@implementation HomeGoodPriceInfoModel

@end


@implementation HomeGoodArgumentInfoModel

@end


@implementation HomeGoodImageInfoModel

@end

@implementation HomeGoodBaseSkuInfoModel

@end

@implementation HomeGoodCouponInfoModel

@end

@implementation HomeGoodActivityInfoModel

@end


@implementation HomeGoodSkuOtherInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"priceInfo":[HomeGoodPriceInfoModel class],
             };
}


@end




