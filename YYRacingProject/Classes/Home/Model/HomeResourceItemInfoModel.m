//
//  HomeResourceItemInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/12.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "HomeResourceItemInfoModel.h"

@implementation HomeResourceItemInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"sellpoint" : @"description"};
}


@end
