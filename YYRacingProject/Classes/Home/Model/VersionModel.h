//
//  VersionModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionModel : NSObject

@property(nonatomic,copy) NSString *latestVersion;

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *downloadUrl;

@property (nonatomic, assign) int type;//弹窗类型：0-一次，1-每次

@end
