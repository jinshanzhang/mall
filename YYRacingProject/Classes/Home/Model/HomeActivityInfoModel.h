//
//  HomeActivityInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HomeBannerInfoModel;
@interface HomeActivityInfoModel : NSObject

@property (nonatomic, copy)   NSString       *showSwitch;
@property (nonatomic, strong) NSMutableArray *bannerArray;
@property (nonatomic, strong) NSMutableArray *iconArray;    // icon
@property (nonatomic, strong) NSMutableArray *bgImageArray; //背景

@property (nonatomic, strong) HomeBannerInfoModel *capules;
@property (nonatomic, strong) NSMutableArray *boothArray;   //八宫格

@end
