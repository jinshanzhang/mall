//
//  HomeGoodPropSkuInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HomeGoodPriceInfoModel;
@interface HomeGoodPropSkuInfoModel : NSObject

@property (nonatomic, strong) NSNumber *pid;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSMutableArray *values;

@end

@interface HomeGoodValueSkuInfoModel : NSObject

@property (nonatomic, copy) NSString *pid;
@property (nonatomic, copy) NSString *vid;
@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) BOOL   isNull;
@property (nonatomic, assign) BOOL   isSelect;//是否选中
@property (nonatomic, strong) NSNumber *skuId;
@property (nonatomic, copy)   NSString *propPath;
@property (nonatomic, strong) NSNumber *storage;
@property (nonatomic, copy)   NSString *imageUrl;

@property (nonatomic, strong) NSMutableArray *priceInfo;
@property (nonatomic, strong) NSMutableArray *childArrs;

@end

