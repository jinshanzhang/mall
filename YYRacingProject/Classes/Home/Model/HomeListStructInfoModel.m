//
//  HomeListStructInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "HomeListStructInfoModel.h"

@implementation HomeListStructInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items":[HomeListItemGoodInfoModel class]};
}

@end
