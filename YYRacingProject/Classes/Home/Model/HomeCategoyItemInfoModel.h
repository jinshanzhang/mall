//
//  HomeCategoyItemInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeCategoyItemInfoModel : NSObject <JMMenuBarItemContentProtocol>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDictionary *link;
@property (nonatomic, strong) YYImageInfoModel *backgroundImage; //大背景图

@end

NS_ASSUME_NONNULL_END
