//
//  HomeCateSuperInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeCateSuperInfoModel.h"

@implementation HomeCateSuperInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"superID" : @"id"};
}

+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass {
    return @{@"leve3Cats" : [HomeCateSubInfoModel class]};
}


@end
