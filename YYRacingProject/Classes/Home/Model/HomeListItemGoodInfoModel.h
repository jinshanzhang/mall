//
//  HomeListItemGoodInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
// 首页items 下面的内容
@interface HomeListItemGoodInfoModel : NSObject <HomeGoodItemProtocol>

@property (nonatomic, strong) NSNumber *comId;   //商品或组合ID
@property (nonatomic, copy) NSString *url;       //跳转目的地址
@property (nonatomic, copy) NSString *uri;       //跳转商品
@property (nonatomic, copy) NSString *title;     //商品或组合标题
@property (nonatomic, copy) NSString *text;      //卖点
@property (nonatomic, copy) NSString *originPrice;
@property (nonatomic, copy) NSString *couponPrice;  //券后价
@property (nonatomic, copy) NSString *reservePrice; //最终价
@property (nonatomic, copy) NSString *commission; //佣金
@property (nonatomic, copy) NSString *sourceIcon; //跨境
@property (nonatomic, copy) NSString *tag;    //爆款
@property (nonatomic, copy) NSString *salesVolume;  //售卖多少件
@property (nonatomic, copy) NSString * imageUrl;
@property (nonatomic, strong) NSNumber *linkType;
@property (nonatomic, strong) NSNumber *itemSaleType; //售卖状态 1 即将开抢  2 热卖中   3 活动结束
@property (nonatomic, strong) NSNumber *saleStatus;   //特卖状态 1 未开始    2 预热    3在售中
@property (nonatomic, strong) NSNumber *couponFlag;   //0 没有券  1 有券
@property (nonatomic, strong) NSNumber *couponLogic;  //0 不开启  1 开启券逻辑

@property (nonatomic, strong) NSIndexPath           *currentIndex;
@property (nonatomic, assign) YYSingleRowScrollType scrollType;

@property (nonatomic, strong) YYImageInfoModel  *coverImage; //封面信息
@property (nonatomic, strong) YYCompositeShareInfoModel *shareInfo; //分享的数据

@property (nonatomic, strong) NSDictionary *coupon; //券信息

@end

NS_ASSUME_NONNULL_END
