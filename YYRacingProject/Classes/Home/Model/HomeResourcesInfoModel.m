//
//  HomeResourcesInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/12.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "HomeResourcesInfoModel.h"
#import "HomeResourceItemInfoModel.h"
@implementation HomeResourcesInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items" : [HomeResourceItemInfoModel class],
             @"bgImage" : [YYImageInfoModel class]
             };
}


@end
