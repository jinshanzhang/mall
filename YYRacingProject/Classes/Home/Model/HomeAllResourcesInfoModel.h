//
//  HomeAllResourcesInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/12.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HomeResourcesInfoModel.h"
NS_ASSUME_NONNULL_BEGIN
@class HomeSuperHotInfoModel;
@interface HomeAllResourcesInfoModel : NSObject<HomeAllResourcesProtocol>

@property (nonatomic, strong) HomeResourcesInfoModel *banners;
@property (nonatomic, strong) HomeResourcesInfoModel *icons;
@property (nonatomic, strong) HomeResourcesInfoModel *booths;
@property (nonatomic, strong) HomeResourcesInfoModel *bgImages;
@property (nonatomic, strong) HomeResourcesInfoModel *capsule;
@property (nonatomic, strong) HomeResourcesInfoModel *serviceGuarantee;
@property (nonatomic, strong) HomeSuperHotInfoModel *superTrendy; //超级爆款

@end

NS_ASSUME_NONNULL_END
