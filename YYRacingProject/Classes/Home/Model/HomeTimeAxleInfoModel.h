//
//  HomeTimeAxleInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeTimeAxleInfoModel : NSObject

@property (nonatomic, copy) NSString *timePointAlias;
@property (nonatomic, copy) NSString *instruction;
@property (nonatomic, strong) NSNumber *timePoint;
@property (nonatomic, strong) NSNumber *chooseFlag;  //1 抢购中  0 已开抢
@property (nonatomic, strong) NSNumber *timePointStart;
@property (nonatomic, strong) NSNumber *timePointEnd;

@end

NS_ASSUME_NONNULL_END
