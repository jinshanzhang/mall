//
//  HomeSuperBrandLumpInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSuperBrandLumpInfoModel.h"
#import "HomeListItemGoodInfoModel.h"

@implementation HomeSuperBrandLumpInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items":[HomeSingleSuperBrandLumpInfoModel class]};
}

@end


@implementation HomeSingleSuperBrandLumpInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items":[HomeListItemGoodInfoModel class]};
}

@end
