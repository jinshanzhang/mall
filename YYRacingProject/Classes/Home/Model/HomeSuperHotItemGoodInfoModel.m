//
//  HomeSuperHotItemGoodInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSuperHotItemGoodInfoModel.h"
#import "HomeListItemGoodInfoModel.h"

@implementation HomeSuperHotInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items":[HomeSuperHotItemGoodInfoModel class]};
}

@end

@implementation HomeSuperHotItemGoodInfoModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"sellpoint" : @"description",@"items": @"goods"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items":[HomeListItemGoodInfoModel class]};
}

@end
