//
//  HomeResourceItemInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/12.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeResourceItemInfoModel : NSObject <ResourcesItemViewProtocol>

@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *fontColor;

@property (nonatomic, strong) NSDictionary *link;


@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSNumber *linkType;
@property (nonatomic, strong) NSNumber *weight;
@property (nonatomic, strong) NSNumber *isVisible;
@property (nonatomic, strong) NSNumber *popType;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSNumber *height;

@property (nonatomic, strong) YYImageInfoModel *coverImage; //图片资源
@property (nonatomic, assign) YYSingleRowScrollType scrollType;

// 兼容超级爆款
@property (nonatomic, strong) NSNumber  *superTrendyType;  //0 单品  1品牌
@property (nonatomic, copy)   NSString  *sellpoint;      //卖点
@property (nonatomic, copy)   NSString  *couponPrice;
@property (nonatomic, copy)   NSString  *originPrice;
@property (nonatomic, copy)   NSString  *reservePrice;
@property (nonatomic, copy)   NSString  *commission;
@property (nonatomic, copy)   NSString  *sellOutFlagUri; //为null或者为空表示没有售罄
@property (nonatomic, copy)   NSString  *rightIcon;
@property (nonatomic, copy)   NSString  *uri;
@property (nonatomic, copy)   NSString  *skipBrandTrendyUri; //跳转品牌uri

@property (nonatomic, strong) NSNumber  *couponFlag; //0 没有优惠券  1 有优惠券
@property (nonatomic, strong) NSNumber  *couponLogic;  //0 不开启  1 开启券逻辑
@property (nonatomic, strong) NSNumber  *startTime;
@property (nonatomic, strong) NSNumber  *endTime;
@property (nonatomic, strong) NSNumber  *currentTime;
@property (nonatomic, strong) NSNumber  *saleStatus; // 3 马上抢   2 即将开始

@end

NS_ASSUME_NONNULL_END
