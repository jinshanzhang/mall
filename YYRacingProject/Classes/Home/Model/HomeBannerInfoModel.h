//
//  HomeBannerInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeBannerInfoModel : NSObject <NSCoding>

@property (nonatomic, copy) NSString *url;        //web 链接

@property (nonatomic, copy) NSString *text;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *imageUrl;   //图片链接

@property (nonatomic, copy) NSString *fontColor; //内容颜色

@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, assign) NSInteger popType; //弹出次数， 1 一次  , 2 每次  0 忽略

@property (nonatomic, assign) NSInteger resourceID; //资源ID

@property (nonatomic, assign) NSInteger linkType; //跳转类型

@property (nonatomic, assign) BOOL switchFlag; //开关

@property (nonatomic, assign) BOOL isOpenBottomLine;

@property (nonatomic, assign) BOOL HideBottom;


@end
