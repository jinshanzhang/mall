//
//  HistoryAndCategorySearchVC.m
//  LLSearchViewController
//
//  Created by 李龙 on 2017/8/22.
//  Copyright © 2017年 李龙. All rights reserved.
//

#import "YY_SearchController.h"
#import "HistoryAndCategorySearchHistroyViewP.h"
#import "HistoryAndCategorySearchCategoryViewP.h"
#import "LLSearchVCConst.h"
#import "YY_SearchResultViewController.h"

#import "LLSearchVCConst.h"
#import "YY_SearchResultViewController.h"

#import "HistoryAndCategorySearchHistroyViewP.h"
#import "HistoryAndCategorySearchCategoryViewP.h"

@interface YY_SearchController ()

@end

@implementation YY_SearchController

- (void)viewDidLoad {
    [self xh_navBottomLine:XHClearColor];
    [YYCommonTools umengEvent:@"search" attributes:nil number:@(10)];
    //告诉父类你的prestenter是什么
//
//    if (self.type == 1) {
//        [self getData];
//    }
    
    
    self.shopHistoryP = [[HistoryAndCategorySearchHistroyViewP alloc] initWithType:self.type];
    
    self.shopCategoryP = [HistoryAndCategorySearchCategoryViewP new];
    [super viewDidLoad];
    [MobClick event:@"SearchVisit"];

    self.view.backgroundColor = [UIColor whiteColor];
    [self beginSearch:^(NaviBarSearchType searchType, NBSSearchShopCategoryViewCellP *categorytagP, UILabel *historyTagLabel, UITextField *textField ,NSArray *arr)
    {
        if (categorytagP.showModel) {
            if (categorytagP.showModel.linkType == 5) {
                [self.shopHistoryP saveSearchCache:categorytagP.showModel.linkUrl result:nil];
            }
            [self pushHotKeword:categorytagP.showModel];
            self.searchTextField.text = categorytagP.showModel.word;
        }
        else if (textField.text||historyTagLabel.text)
        {
            [YYCommonTools umengEvent:@"history" attributes:nil number:@(10)];
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if (kValidString(textField.text)) {
                [params setObject:textField.text forKey:@"url"];
            }
            else {
                if (historyTagLabel.text) {
                    [params setObject:historyTagLabel.text?:@"" forKey:@"url"];
                }else{
                    if (![self.searchTextField.placeholder isEqualToString:@"请输入关键字搜索"]&&!kValidString(textField.text)) {
                        [params setObject:textField.placeholder forKey:@"url"];
                    }else{
                        [params setObject:@"" forKey:@"url"];
                    }
                }
            }
            [params setObject:@(3) forKey:@"linkType"];

            if (self.type == 1) {
                [YYCommonTools skipOrderSearchResult:self params:params];
            }else{
                [YYCommonTools skipSearchResult:self params:params];
            }
        
            [self.shopHistoryP saveSearchCache:textField.text?:historyTagLabel.text result:nil];
            self.searchTextField.text = textField.text?:historyTagLabel.text;
        }
    }];
    
//    self.searchBar.text = @"!23";
}

//-(void)getData{
//    
////    self.searchBar.placeholder = kValidString(self.maintitle)?self.maintitle:@"请输入关键字搜索";
//}


#pragma mark 判断热搜词
- (void)pushHotKeword:(KeyWordModel *)model
{
    [YYCommonTools umengEvent:@"hot" attributes:nil number:@(10)];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(model.linkType) forKey:@"linkType"];
    if (model.linkUrl) {
        [params setObject:model.linkUrl forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:self
                                 params:params];
}

-(void)dealloc
{
    YYLog(@"HistoryAndCategorySearchVC 页面销毁");
}

@end
