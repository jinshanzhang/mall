//
//  YY_GoodDetailArgumentViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_GoodDetailArgumentViewController.h"

#import "HomeGoodInfoModel.h"
#import "GoodDetailArgumentInfoCell.h"
@interface YY_GoodDetailArgumentViewController ()

@property (nonatomic, assign) BOOL                  canScroll;

@end

@implementation YY_GoodDetailArgumentViewController

- (void)setArgumentArrs:(NSMutableArray *)argumentArrs {
    _argumentArrs = argumentArrs;
    self.listData = _argumentArrs;
    [self reloadTableView];
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_hideNavigation:YES];
    
    Class currentCls = [GoodDetailArgumentInfoCell class];
    registerClass(self.m_tableView, currentCls);
    self.m_tableView.backgroundColor = XHWhiteColor;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 30.0;
    self.m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(6));
        make.left.right.bottom.equalTo(self.view);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailLeaveTopNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailArriveTopNotification object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private method
- (void)goodDetailNotificationHandler:(NSNotification *)notification {
    NSString *notificationName = notification.name;
    if ([notificationName isEqualToString:GoodDetailArriveTopNotification]) {
        self.canScroll = YES;
        self.m_tableView.showsVerticalScrollIndicator = YES;
    }else if([notificationName isEqualToString:GoodDetailLeaveTopNotification]){
        self.m_tableView.contentOffset = CGPointZero;
        self.canScroll = NO;
        self.m_tableView.showsVerticalScrollIndicator = NO;
    }
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        NSInteger row = indexPath.row;
        Class currentCls = [GoodDetailArgumentInfoCell class];
        GoodDetailArgumentInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (row < self.listData.count && kValidArray(self.listData)) {
            HomeGoodArgumentInfoModel *argument = [self.listData objectAtIndex:row];
            [cell setDetailArgument:argument.name valueName:argument.value];
        }
        return cell;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self judgeScrollDirection:scrollView];
    if (!self.canScroll) {
        [scrollView setContentOffset:CGPointZero];
    }
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        //表示页面向下，则发送通知告知离开顶部。
        kPostNotification(GoodDetailLeaveTopNotification, nil);
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
