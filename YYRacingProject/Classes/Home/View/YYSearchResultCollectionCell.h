//
//  YYSearchResultCollectionCell.h
//  YYRacingProject
//
//  Created by cujia_1 on 2020/9/2.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeListStructInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface YYSearchResultCollectionCell : UICollectionViewCell

@property (nonatomic, strong) HomeListStructInfoModel * model;

@end

NS_ASSUME_NONNULL_END
