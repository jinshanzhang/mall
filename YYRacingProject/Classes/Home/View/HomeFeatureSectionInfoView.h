//
//  HomeFeatureSectionInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/29.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeFeatureSectionInfoView;
NS_ASSUME_NONNULL_BEGIN

@protocol HomeFeatureSectionInfoViewDelegate <NSObject>

@optional
- (void)homeFeatureSectionView:(HomeFeatureSectionInfoView *)sectionView didSelectItemAtIndex:(NSInteger)itemIndex;

@end


@interface HomeFeatureSectionInfoView : UIView

@property (nonatomic, strong) NSMutableArray *contentDatas;
@property (nonatomic, weak) id <HomeFeatureSectionInfoViewDelegate> delegate;

- (void)willMenuBarItemSelectAtIndex:(NSInteger)selectIndex;

@end

NS_ASSUME_NONNULL_END
