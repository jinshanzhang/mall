//
//  HomeSuperHotInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSuperHotInfoCell.h"

#import "yyCountDownManager.h"
@interface HomeSuperHotInfoCell()

@property (nonatomic, strong) UIView      *backGroundView;
@property (nonatomic, strong) UIView      *bottomView;
@property (nonatomic, strong) UIImageView *goodImageView;
@property (nonatomic, strong) UIImageView *leftMarkImageView;
@property (nonatomic, strong) UIImageView *centerMarkImageView;

@property (nonatomic, strong) UILabel     *sellingPointLabel;    //卖点
@property (nonatomic, strong) UILabel     *goodNameLabel;
@property (nonatomic, strong) YYLabel     *countDownLabel;

@property (nonatomic, strong) UILabel     *goodPriceLabel;       //商品价格
@property (nonatomic, strong) UILabel     *couponPriceLabel;     //券后价

@property (nonatomic, strong) UIButton    *buyBtn;
@end

@implementation HomeSuperHotInfoCell

- (void)createUI {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:YYCountDownNotification object:nil];
    
    self.backGroundView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.backGroundView];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.backGroundView addSubview:self.bottomView];
    
    self.goodImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    self.goodImageView.layer.cornerRadius = 2;
    self.goodImageView.clipsToBounds = YES;
    [self.backGroundView addSubview:self.goodImageView];
    
    self.leftMarkImageView = [YYCreateTools createImageView:nil
                                                  viewModel:-1];
    self.leftMarkImageView.hidden = YES;
    [self.backGroundView addSubview:self.leftMarkImageView];
    
    self.centerMarkImageView = [YYCreateTools createImageView:@"count_down_end"
                                                    viewModel:-1];
    self.centerMarkImageView.hidden = YES;
    [self.backGroundView addSubview:self.centerMarkImageView];
    
    self.goodNameLabel = [YYCreateTools createLabel:nil
                                               font:midFont(14)
                                          textColor:XHBlackColor];
    self.goodNameLabel.numberOfLines = 1;
    [self.backGroundView addSubview:self.goodNameLabel];
    
    self.sellingPointLabel = [YYCreateTools createLabel:nil
                                                   font:normalFont(11)
                                              textColor:XHRedColor];
    self.sellingPointLabel.numberOfLines = 1;
    self.sellingPointLabel.layer.cornerRadius = 1;
    self.sellingPointLabel.clipsToBounds = YES;
    self.sellingPointLabel.layer.borderColor = XHRedColor.CGColor;
    self.sellingPointLabel.layer.borderWidth = 1.0;
    self.sellingPointLabel.textAlignment = NSTextAlignmentCenter;
    [self.backGroundView addSubview:self.sellingPointLabel];
    
    self.countDownLabel = [YYCreateTools createLabel:nil
                                                font:normalFont(11)
                                           textColor:XHRedColor
                                            maxWidth:kSizeScale(180)
                                       fixLineHeight:kSizeScale(11)];
    self.countDownLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    self.countDownLabel.textAlignment = NSTextAlignmentLeft;
    [self.backGroundView addSubview:self.countDownLabel];
    
    self.couponPriceLabel = [YYCreateTools createLabel:nil
                                                  font:midFont(11)
                                             textColor:XHBlackColor];
    [self.backGroundView addSubview:self.couponPriceLabel];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:midFont(11)
                                           textColor:XHBlackColor];
    [self.backGroundView addSubview:self.goodPriceLabel];

    self.buyBtn = [YYCreateTools createButton:@"马上抢"
                                           bgColor:XHRedColor
                                         textColor:XHWhiteColor];
    self.buyBtn.layer.cornerRadius = 2;
    self.buyBtn.clipsToBounds = YES;
    [self.buyBtn setImage:[UIImage imageNamed:@"home_super_hot_arrow_icon"] forState:UIControlStateNormal];
    [self.buyBtn addTarget:self action:@selector(cellClickOperator) forControlEvents:UIControlEventTouchUpInside];
    [self.buyBtn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextLeft imageTitleSpace:5];
    [self.backGroundView addSubview:self.buyBtn];
    
#pragma mark - layout
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(105), kSizeScale(105)));
        make.bottom.equalTo(self.backGroundView.mas_bottom).offset(-kSizeScale(20)).priorityHigh();
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.backGroundView);
        make.height.mas_equalTo(kSizeScale(8));
    }];
    
    [self.goodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodImageView.mas_right).offset(kSizeScale(12));
        make.top.equalTo(self.goodImageView.mas_top);
        make.right.equalTo(self.backGroundView.mas_right).offset(-kSizeScale(12));
    }];
    
    [self.sellingPointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(8));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(100), kSizeScale(16)));
    }];
    
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(70), kSizeScale(25)));
        make.right.mas_equalTo(-kSizeScale(12));
        make.bottom.equalTo(self.goodImageView);
    }];
    
    [self.couponPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sellingPointLabel.mas_left);
        make.centerY.equalTo(self.buyBtn);
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponPriceLabel.mas_right).offset(kSizeScale(3));
        make.bottom.equalTo(self.couponPriceLabel.mas_bottom).offset(-kSizeScale(2.8));
    }];
    
    [self.centerMarkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.goodImageView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(65), kSizeScale(65)));
    }];
    
    [self.countDownLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.buyBtn.mas_top).offset(-kSizeScale(5));
        make.left.equalTo(self.sellingPointLabel.mas_left);
    }];
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(cellClickOperator)]];
}

#pragma mark - Setter method
- (void)setItemModel:(id<HomeSuperHotItemProtocol>)itemModel {
    _itemModel = itemModel;
    if (_itemModel) {
        BOOL isWillRob = ([_itemModel.saleStatus integerValue] == 2?YES:NO);
        BOOL isSingleBrand = ([_itemModel.superTrendyType integerValue] == 0? YES : NO);
        UIColor *textColor = XHRedColor;
        UIColor *couponTextColor = XHBlackColor;
        [self.goodImageView yy_sdWebImage:_itemModel.coverImage.imageUrl placeholderImageType:YYPlaceholderImageListGoodType];
        self.goodNameLabel.text = _itemModel.title;
        self.sellingPointLabel.text = _itemModel.sellpoint;
       
        CGSize sellingPointSize = [YYCommonTools sizeWithText:itemModel.sellpoint
                                                         font:normalFont(11)
                                                      maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
        sellingPointSize.width = sellingPointSize.width + 10;
        if (sellingPointSize.width > kScreenW - (kSizeScale(105) + kSizeScale(12)*3)) {
            sellingPointSize.width = kScreenW - (kSizeScale(105) + kSizeScale(12)*3);
        }
        [self.sellingPointLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(sellingPointSize);
        }];
        
        textColor = (isWillRob? HexRGB(0x11BC56): XHRedColor);
        if (isWillRob) {
            NSString *timeFormat = [NSString getPreHotShowTime:[_itemModel.startTime longValue]
                                                   currentTime:[_itemModel.currentTime longValue]];
            UIImage *image = [UIImage imageNamed:@"home_rob_time_icon"];
            NSMutableAttributedString *postAttribute = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeScaleAspectFit attachmentSize:CGSizeMake(11.0, 11.0) alignToFont:normalFont(10) alignment:YYTextVerticalAlignmentCenter];
            [postAttribute appendAttributedString:[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@开抢",timeFormat]]];
            [postAttribute yy_setAttributes:XHBlackColor
                                       font:normalFont(11)
                                  wordSpace:@(0)
                                    content:postAttribute
                                  alignment:NSTextAlignmentLeft];
            self.countDownLabel.attributedText = postAttribute;
        }
        self.countDownLabel.textColor = textColor;
        if (isWillRob) {
            [self.buyBtn setTitle:@"即将开抢" forState:UIControlStateNormal];
            [self.buyBtn setImage:[UIImage imageWithColor:textColor] forState:UIControlStateNormal];
            [self.buyBtn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextLeft imageTitleSpace:0];
        }
        else {
            [self.buyBtn setTitle:@"马上抢" forState:UIControlStateNormal];
            [self.buyBtn setImage:[UIImage imageNamed:@"home_super_hot_arrow_icon"] forState:UIControlStateNormal];
            [self.buyBtn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextLeft imageTitleSpace:5];
        }
        [self.buyBtn setBackgroundImage:[UIImage imageWithColor:self.countDownLabel.textColor] forState:UIControlStateNormal];
        
        if (kValidString(itemModel.rightIcon) && isSingleBrand) {
            self.leftMarkImageView.hidden = NO;
            CGSize leftSize = [UIImage getImageSizeWithURL:itemModel.rightIcon];
            [self.leftMarkImageView sd_setImageWithURL:[NSURL URLWithString:itemModel.rightIcon]];
            [self.leftMarkImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(leftSize.width/2.0, leftSize.height/2.0));
            }];
        }
        else {
            self.leftMarkImageView.hidden = YES;
        }
        
        double leftTime = [itemModel.endTime longValue] - [itemModel.currentTime longValue];
        if (leftTime > 0) {
            self.centerMarkImageView.hidden = YES;
        }
    
        if (kIsFan) {
            couponTextColor = (isWillRob ? HexRGB(0x11BC56) : XHRedColor);
        }
        if ([itemModel.couponFlag integerValue] == 1) {
            //有券
            NSString *couponPrice = @"0";
            if (kValidString(_itemModel.couponPrice)) {
                couponPrice = [NSString stringWithFormat:@"%@",_itemModel.couponPrice];
            }
            NSMutableAttributedString *coupon = [YYCommonTools containSpecialSymbolHandler:@"券后¥" symbolFont:boldFont(10) symbolTextColor:couponTextColor wordSpace:1 price:couponPrice priceFont:boldFont(16) priceTextColor:couponTextColor symbolOffsetY:1.2];
            self.couponPriceLabel.attributedText = coupon;
        }
        else {
            //无券
            NSString *reservePrice = @"0";
            if (kValidString(_itemModel.reservePrice)) {
                reservePrice = [NSString stringWithFormat:@"%@",_itemModel.reservePrice];
            }
            NSMutableAttributedString *reserve = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(10) symbolTextColor:couponTextColor wordSpace:1 price:reservePrice priceFont:boldFont(16) priceTextColor:couponTextColor symbolOffsetY:1.2];
            self.couponPriceLabel.attributedText = reserve;
        }
        
        if (kIsFan) {
            NSString *originPrice = @"¥0";
            if (kValidString(originPrice)) {
                originPrice = [NSString stringWithFormat:@"¥%@",_itemModel.originPrice];
            }
            NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:originPrice];
            [origin yy_setAttributes:XHBlackLitColor
                                font:midFont(11)
                             content:origin
                           alignment:NSTextAlignmentLeft];
            [origin addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, origin.length)];
            [origin addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle)range:NSMakeRange(0,origin.length)];
            if (@available(iOS 10.3, *)) {
                [origin addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)}range:NSMakeRange(0,origin.length)];
            }
            self.goodPriceLabel.attributedText = origin;
        }
        else {
            NSString *commissionPrice = @"0";
            if (kValidString(_itemModel.commission)) {
                commissionPrice = [NSString stringWithFormat:@"%@",_itemModel.commission];
            }
            NSMutableAttributedString *commission = [YYCommonTools containSpecialSymbolHandler:@"赚¥" symbolFont:midFont(10) symbolTextColor:textColor wordSpace:1 price:commissionPrice priceFont:midFont(12) priceTextColor:textColor symbolOffsetY:1.2];
            self.goodPriceLabel.attributedText = commission;
        }
    }
}

#pragma mark - Event
- (void)cellClickOperator {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeSuperHotClickItemCell:)]) {
        [self.delegate homeSuperHotClickItemCell:self];
    }
}

- (void)countDownNotification {
    if ([_itemModel conformsToProtocol:@protocol(HomeSuperHotItemProtocol)]){
        id <HomeSuperHotItemProtocol> itemModel = self.itemModel;
        if ([itemModel.saleStatus integerValue] == 3) {
            // 抢购中
            NSString *showContent = nil;
            CGFloat timeInterval = [kConfigCountDown timeIntervalWithIdentifier:@"superHot"];
            
            double leftTime = [itemModel.endTime longValue] - [itemModel.currentTime longValue];
            double differTime = leftTime - timeInterval;
            //正在开抢
            if (differTime <= 0) {
                showContent = @"已结束00:00:00.0";
                self.centerMarkImageView.hidden = NO;
            }
            else {
                NSInteger currentHour = (NSInteger)((differTime)/1000/60/60);
                NSInteger currentMinute = (NSInteger)((differTime)/1000/60)%60;
                NSInteger currentSeconds = ((NSInteger)(differTime))/1000%60;
                CGFloat   currentMsec = ((NSInteger)((differTime)))%1000/100;
                //小时数
                NSString *hours = [NSString stringWithFormat:@"%@", (currentHour < 10 ?[NSString stringWithFormat:@"0%ld",currentHour]:@(currentHour))];
                //分钟数
                NSString *minute = [NSString stringWithFormat:@"%@", (currentMinute < 10 ?[NSString stringWithFormat:@"0%ld",currentMinute]:@(currentMinute))];
                //秒数
                NSString *second = [NSString stringWithFormat:@"%@", (currentSeconds < 10 ?[NSString stringWithFormat:@"0%ld",currentSeconds]:@(currentSeconds))];
                //毫秒数
                NSString *msec = [NSString stringWithFormat:@"%.0f", currentMsec];
                showContent = [NSString stringWithFormat:@"距结束%@:%@:%@:%@",hours,minute,second,msec];
            }
            self.countDownLabel.text = showContent;
        }
    }
}
@end
