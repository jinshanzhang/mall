//
//  HomeSuperMoldbabyInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeResourcesInfoModel;
NS_ASSUME_NONNULL_BEGIN

@interface HomeSuperMoldbabyInfoView : UIView

@property (nonatomic, strong) HomeResourcesInfoModel *resourceModel;

@end

NS_ASSUME_NONNULL_END
