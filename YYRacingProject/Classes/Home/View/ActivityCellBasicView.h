//
//  ActivityCellBasicView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivityCellBasicView : UIView

@property (nonatomic, strong) UILabel *title;
@property (nonatomic, strong) UIImageView *activityArrowImage;


@end

NS_ASSUME_NONNULL_END
