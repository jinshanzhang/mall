//
//  HomeCateItemReusableView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^itemClick)(NSInteger index, YYPriceFilterType filteType);

@interface HomeCateItemReusableView : UIView

@property (nonatomic, copy) itemClick itemBlock;
@property (nonatomic, assign) CGFloat lineHeight;

@property (nonatomic, assign) BOOL    isReset;

@end
