//
//  HomeNewVipGoodInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/24.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeNewVipGoodInfoCell.h"

#import "yyCountDownManager.h"
#import "YYMultiItemScrollModel.h"

@interface HomeNewVipGoodInfoCell()

@property (nonatomic, strong) UIView *brandBackgroundView;
@property (nonatomic, strong) UIImageView *brandImageView;
@property (nonatomic, strong) UIImageView *leftMarkImageView;
@property (nonatomic, strong) UIImageView *centerMarkImageView;
@property (nonatomic, strong) YYLabel     *superHotTitleLabel;
@property (nonatomic, strong) UILabel     *superHotPriceLabel;
@property (nonatomic, strong) UILabel     *couponPriceLabel;
@property (nonatomic, strong) UILabel     *sellingPointLabel; //卖点
@property (nonatomic, strong) UIImageView *countDownBgView;   //倒计时背景
@property (nonatomic, strong) YYLabel     *countDownTimeLabel;

@property (nonatomic, strong) id <YYMultiItemScrollViewProtocol> infoModel;

@end

@implementation HomeNewVipGoodInfoCell

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    self.infoModel = viewModel;
    [self.brandImageView yy_sdWebImage:viewModel.imageUrl placeholderImageType:YYPlaceholderImageHotGoodType];
    self.superHotTitleLabel.text = viewModel.title;
    
    self.sellingPointLabel.text = viewModel.subTitle;
    CGSize sellingPointSize = [YYCommonTools sizeWithText:viewModel.subTitle
                                                     font:normalFont(9)
                                                  maxSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    sellingPointSize.width = sellingPointSize.width + 12;
    sellingPointSize.height = kSizeScale(14);
    if (sellingPointSize.width > kSizeScale(110)) {
        sellingPointSize.width = kSizeScale(110);
    }
    self.sellingPointLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage gradientColorImageFromColors:@[HexRGB(0xCFA368), HexRGB(0xECB976)] gradientType:GradientTypeLeftToRight imgSize:sellingPointSize]];
    [self.sellingPointLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(sellingPointSize);
    }];
    
    NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"新人券后价￥" symbolFont:midFont(10) symbolTextColor:XHRedColor wordSpace:0 price:kValidString(viewModel.couponPrice)?viewModel.couponPrice:@"0" priceFont:boldFont(14) priceTextColor:XHRedColor symbolOffsetY:1.2];
    self.couponPriceLabel.attributedText = extra;
}

- (void)createUI {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:YYCountDownNotification object:nil];
    
    CGFloat cellWidth = kSizeScale(120);
    self.brandBackgroundView = [YYCreateTools createView:XHWhiteColor];
    self.brandBackgroundView.v_cornerRadius = 5;
    [self.contentView addSubview:self.brandBackgroundView];
    
    self.brandImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.brandBackgroundView addSubview:self.brandImageView];
    [self.brandImageView zy_cornerRadiusAdvance:5 rectCornerType:UIRectCornerAllCorners];
    
    self.countDownBgView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.brandBackgroundView addSubview:self.countDownBgView];
    [self.countDownBgView zy_cornerRadiusAdvance:kSizeScale(0) rectCornerType:UIRectCornerBottomLeft|UIRectCornerBottomRight];
    self.countDownBgView.image = [UIImage imageWithColor:HexRGB(0xFFEAED)];
    
    self.countDownTimeLabel = [YYCreateTools createLabel:nil
                                                    font:normalFont(9)
                                               textColor:XHRedColor
                                                maxWidth:self.width
                                           fixLineHeight:kSizeScale(11)];
    self.countDownTimeLabel.textVerticalAlignment = YYTextVerticalAlignmentCenter;
    self.countDownTimeLabel.textAlignment = NSTextAlignmentLeft;
    [self.brandBackgroundView addSubview:self.countDownTimeLabel];
    
    self.sellingPointLabel = [YYCreateTools createLabel:nil
                                                   font:normalFont(9)
                                              textColor:XHWhiteColor];
    self.sellingPointLabel.numberOfLines = 1;
    self.sellingPointLabel.v_cornerRadius = 7;
    self.sellingPointLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.sellingPointLabel];
    
    self.superHotTitleLabel = [YYCreateTools createLabel:nil
                                                    font:normalFont(12)
                                               textColor:XHBlackColor
                                                maxWidth:self.width
                                           fixLineHeight:kSizeScale(17)];
    self.superHotTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.superHotTitleLabel.numberOfLines = 1;
    [self.contentView addSubview:self.superHotTitleLabel];
    
    self.superHotPriceLabel = [YYCreateTools createLabel:nil
                                                    font:boldFont(12)
                                               textColor:XHRedColor];
    self.superHotPriceLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.superHotPriceLabel];
    
    self.leftMarkImageView = [YYCreateTools createImageView:nil
                                                  viewModel:-1];
    [self.leftMarkImageView zy_cornerRadiusAdvance:5 rectCornerType:UIRectCornerTopLeft];
    self.leftMarkImageView.hidden = YES;
    [self.brandBackgroundView addSubview:self.leftMarkImageView];
    
    self.centerMarkImageView = [YYCreateTools createImageView:@"count_down_end"
                                                    viewModel:-1];
    self.centerMarkImageView.hidden = YES;
    [self.brandBackgroundView addSubview:self.centerMarkImageView];
    
    self.couponPriceLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(12)
                                             textColor:XHRedColor];
    [self.contentView addSubview:self.couponPriceLabel];
    
#pragma mark - layout
    [self.brandBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.brandImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(cellWidth);
    }];
    
    [self.superHotTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.brandBackgroundView.mas_left).offset(10);
        make.right.equalTo(self.brandBackgroundView.mas_right).offset(-10);
        make.top.equalTo(self.brandImageView.mas_bottom).offset(kSizeScale(4));
    }];
    
    [self.sellingPointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.superHotTitleLabel);
        make.top.equalTo(self.superHotTitleLabel.mas_bottom).offset(kSizeScale(5));
        make.size.mas_equalTo(CGSizeZero);
    }];
    
    [self.couponPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.sellingPointLabel);
        make.bottom.equalTo(self.brandBackgroundView.mas_bottom).offset(-kSizeScale(3));
    }];
    
    [self.superHotPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponPriceLabel.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.couponPriceLabel);
    }];
    
    [self.leftMarkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.brandBackgroundView);
        make.size.mas_equalTo(CGSizeZero);
    }];
    
    [self.centerMarkImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.brandBackgroundView);
        make.top.mas_equalTo(kSizeScale(28));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(65), kSizeScale(65)));
    }];
    
    [self.countDownBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.brandImageView);
        make.top.mas_equalTo(self.width-15);
        make.height.mas_equalTo(15.5);
    }];
    
    [self.countDownTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(22));
        make.top.bottom.equalTo(self.countDownBgView);
    }];
}

#pragma mark - Event
/**倒计时**/
- (void)countDownNotification {
    
    NSString *showContent = nil;
    UIColor *originPriceColor = XHRedColor;
    CGFloat timeInterval = [kConfigCountDown timeIntervalWithIdentifier:@"newVip"];
    
    double leftTime = [self.infoModel.endTime longValue] - [self.infoModel.currentTime longValue];
    double differTime = leftTime - timeInterval;
    //正在开抢
    if (differTime <= 0) {
        showContent = @"已结束00:00:00.0";
        self.centerMarkImageView.hidden = NO;
    }
    else {
        NSInteger currentHour = (NSInteger)((differTime)/1000/60/60);
        NSInteger currentMinute = (NSInteger)((differTime)/1000/60)%60;
        NSInteger currentSeconds = ((NSInteger)(differTime))/1000%60;
        CGFloat   currentMsec = ((NSInteger)((differTime)))%1000/100;
        //小时数
        NSString *hours = [NSString stringWithFormat:@"%@", (currentHour < 10 ?[NSString stringWithFormat:@"0%ld",currentHour]:@(currentHour))];
        //分钟数
        NSString *minute = [NSString stringWithFormat:@"%@", (currentMinute < 10 ?[NSString stringWithFormat:@"0%ld",currentMinute]:@(currentMinute))];
        //秒数
        NSString *second = [NSString stringWithFormat:@"%@", (currentSeconds < 10 ?[NSString stringWithFormat:@"0%ld",currentSeconds]:@(currentSeconds))];
        //毫秒数
        NSString *msec = [NSString stringWithFormat:@"%.0f", currentMsec];
        showContent = [NSString stringWithFormat:@"距结束%@:%@:%@.%@",hours,minute,second,msec];
    }
    NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:showContent];
    [attribute yy_setAttributes:XHBlackColor
                           font:normalFont(9)
                      wordSpace:@(0)
                        content:attribute
                      alignment:NSTextAlignmentLeft];
    self.countDownTimeLabel.attributedText = attribute;
    self.countDownTimeLabel.textColor = originPriceColor;
}


@end

