//
//  HomeFilterConditionView.m
//  YIYanProject
//
//  Created by cjm on 2018/4/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeFilterConditionView.h"

#define btnTag  200
@interface HomeFilterConditionView()

@property (nonatomic, strong) UIButton *selectItem;
@property (nonatomic, assign) NSInteger priceIndex;
@property (nonatomic, strong) NSMutableArray *btnArrs;
@property (nonatomic, assign) BOOL  isReset;
@property (nonatomic, assign) YYPriceFilterType filterType;
@end

@implementation HomeFilterConditionView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isReset = NO;
        self.backgroundColor = XHWhiteColor;
    }
    return self;
}

- (void)setItemTitles:(NSMutableArray *)itemTitles {
    _itemTitles = itemTitles;
    if (_itemTitles) {
        self.filterType = YYPriceFilterTypeNormal;
        
        self.btnArrs = [NSMutableArray array];
        if (self.subviews.count > 0) {
            [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
        [self layoutIfNeeded]; //获取布局后的frame
        __block UIView *lastView = nil;
        CGFloat itemWidth = kScreenW / self.itemTitles.count;
        
        [_itemTitles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isKindOfClass:[NSString class]]) {
                NSString *title = obj;
                UIButton *btn = [YYCreateTools createBtn:obj];
                btn.isIgnore = YES;
                btn.titleLabel.font = normalFont(13);
                btn.titleLabel.textAlignment = NSTextAlignmentCenter;
                [btn setTitleColor:XHBlackColor forState:UIControlStateNormal];
                [btn setTitleColor:XHRedColor
                          forState:UIControlStateSelected];
                [btn addTarget:self action:@selector(btnItemClickEvent:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:btn];
                if (idx == 0) {
                    btn.selected = YES;
                    self.selectItem = btn;
                }
                if ([title isEqualToString:@"价格"]) {
                    [btn setImage:[UIImage imageNamed:@"price_filter_normal_icon"] forState:UIControlStateNormal];
                    [btn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextLeft imageTitleSpace:10];
                    self.filterType = YYPriceFilterTypeNormal;
                }
                
                btn.tag = btnTag + idx;
                
                if ([title isEqualToString:@"价格"]) {
                    [btn setImage:[UIImage imageNamed:@"price_filter_normal_icon"] forState:UIControlStateNormal];
                    [btn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextLeft imageTitleSpace:10];
                    self.priceIndex = idx;
                }
                
                if (lastView) {
                    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.top.bottom.equalTo(self);
                        make.left.equalTo(lastView.mas_right);
                        make.width.mas_equalTo(itemWidth);
                    }];
                }
                else {
                    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.mas_equalTo(0);
                        make.top.bottom.equalTo(self);
                        make.width.mas_equalTo(itemWidth);
                    }];
                }
                lastView = btn;
                [self.btnArrs addObject:btn];
            }
        }];
    }
}

- (void)btnItemClickEvent:(UIButton *)sender {
    // 1、选中的button
    __block NSString *priceImageName;
    NSInteger clickIndex = sender.tag - btnTag;
    UIButton *priceBtn = self.btnArrs[self.priceIndex];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * 0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.selectItem == nil) {
            sender.selected = YES;
            self.selectItem = sender;
        }else if (self.selectItem != nil && self.selectItem == sender){
            sender.selected = YES;
        }else if (self.selectItem != sender && self.selectItem != nil){
            self.selectItem.selected = NO;
            sender.selected = YES;
            self.selectItem = sender;
        }
        
        if (clickIndex == self.priceIndex) {
            if (self.filterType == YYPriceFilterTypeNormal) {
                self.filterType = YYPriceFilterTypeDown;
                priceImageName = @"price_filter_up_icon";
            }
            else if (self.filterType == YYPriceFilterTypeDown) {
                self.filterType = YYPriceFilterTypeUp;
                priceImageName = @"price_filter_down_icon";
            }
            else {
                self.filterType = YYPriceFilterTypeDown;
                priceImageName = @"price_filter_up_icon";
            }
            [priceBtn setImage:[UIImage imageNamed:priceImageName] forState:UIControlStateSelected];
        }
        else {
            self.filterType = YYPriceFilterTypeNormal;
            priceImageName = @"price_filter_normal_icon";
            [priceBtn setImage:[UIImage imageNamed:priceImageName] forState:UIControlStateNormal];
        }
        if (!self.isReset) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(filterSelectPoint:priceType:)]) {
                [self.delegate filterSelectPoint:clickIndex priceType:self.filterType];
            }
        }
        self.isReset = NO;
    });
}

- (void)willItemBecomeSelectAtIndex:(NSInteger)index {
    if (index >= 0 && kValidArray(self.btnArrs)) {
        UIButton *btn = self.btnArrs[index];
        self.isReset = YES;
        [self btnItemClickEvent:btn];
    }
}

@end
