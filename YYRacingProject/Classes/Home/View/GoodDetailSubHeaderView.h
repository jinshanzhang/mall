//
//  GoodDetailSubHeaderView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailSubHeaderView : UIView

- (void)setSubHeaderView:(NSMutableDictionary *)headerSources;

@end

NS_ASSUME_NONNULL_END
