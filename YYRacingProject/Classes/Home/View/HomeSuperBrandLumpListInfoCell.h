//
//  HomeSuperBrandLumpListInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class HomeSingleSuperBrandLumpInfoModel;
@interface HomeSuperBrandLumpListInfoCell : YYBaseTableViewCell

- (void)setSuperBrandViewModel:(HomeSingleSuperBrandLumpInfoModel *)lumpModel;

@end

NS_ASSUME_NONNULL_END
