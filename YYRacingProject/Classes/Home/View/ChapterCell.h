//
//  ChapterCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/24.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "LessonDetailModel.h"
#import <UIKit/UIKit.h>
@interface ChapterCell : YYBaseTableViewCell

@property (nonatomic, strong) LessonDetailModel *model;

@end
