//
//  GoodDetailServiceInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodDetailServiceInfoView : UIView

@property (nonatomic, strong) NSMutableArray *serviceLists;

@end
