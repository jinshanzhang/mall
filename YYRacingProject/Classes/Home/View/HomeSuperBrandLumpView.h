//
//  HomeSuperBrandLumpView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HomeSuperBrandLumpInfoModel;

@interface HomeSuperBrandLumpView : UIView

@property (nonatomic, copy) void (^heightUpdateBlock)(CGFloat height);
- (void)setSuperBrandViewModel:(HomeSuperBrandLumpInfoModel *)model;

@end

NS_ASSUME_NONNULL_END
