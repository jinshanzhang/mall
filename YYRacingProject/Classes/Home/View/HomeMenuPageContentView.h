//
//  HomeMenuPageContentView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HomeMenuPageContentView;

@protocol HomeMenuPageContentViewDelegate <NSObject>

@optional

/**
 HomeMenuPageContentView开始滑动
 
 @param contentView HomeMenuPageContentView
 */
- (void)HomeMenuPageViewWillBeginDragging:(HomeMenuPageContentView *)contentView;

/**
 HomeMenuPageContentView滑动调用
 
 @param contentView HomeMenuPageContentView
 @param startIndex 开始滑动页面索引
 @param endIndex 结束滑动页面索引
 @param progress 滑动进度
 */
- (void)HomeMenuPageViewDidScroll:(HomeMenuPageContentView *)contentView
                    startIndex:(NSInteger)startIndex
                      endIndex:(NSInteger)endIndex
                      progress:(CGFloat)progress;

/**
 HomeMenuPageContentView结束滑动
 
 @param contentView HomeMenuPageContentView
 @param startIndex 开始滑动索引
 @param endIndex 结束滑动索引
 */
- (void)HomeMenuPageViewDidEndDecelerating:(HomeMenuPageContentView *)contentView
                            startIndex:(NSInteger)startIndex
                              endIndex:(NSInteger)endIndex;

@end

@interface HomeMenuPageContentView : UIView

@property (nonatomic, strong) NSMutableArray *subViewCtrls;

@property (nonatomic, strong)   UIViewController *parentVC;

@property (nonatomic, weak) id<HomeMenuPageContentViewDelegate>delegate;
/**
 设置contentView当前展示的页面索引，默认为0
 */
@property (nonatomic, assign) NSInteger contentViewCurrentIndex;

/**
 设置contentView能否左右滑动，默认YES
 */
@property (nonatomic, assign) BOOL contentViewCanScroll;

@end

NS_ASSUME_NONNULL_END
