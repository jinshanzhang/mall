//
//  GoodDetailArgumentInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface GoodDetailArgumentInfoCell : YYBaseTableViewCell

- (void)setDetailArgument:(NSString *)keyName
                valueName:(NSString *)valueName;

@end
