//
//  HomeSpecailSellTitleInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/27.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

#import "HomeListStructInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface HomeSpecailSellTitleInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) HomeListStructInfoModel *infoModel;

@end

NS_ASSUME_NONNULL_END
