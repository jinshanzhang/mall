//
//  HomeActivityIconInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeActivityIconInfoCell : UICollectionViewCell

@property (nonatomic, copy) NSString  *fontColor;
@property (nonatomic, strong) NSMutableDictionary *extendParams;

- (void)setActivityView:(NSString *)iconContent
                iconUrl:(NSString *)iconUrl;

@end
