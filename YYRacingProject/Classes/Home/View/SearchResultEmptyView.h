//
//  SearchResultEmptyView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyWordModel.h"

@interface SearchResultEmptyView : UIView

@property (nonatomic,strong) UILabel *leftTitleLabel;
@property (nonatomic,strong) UIView *coverView;

@property (nonatomic, strong) NSMutableArray *keyArr;
@property (nonatomic, strong) NSMutableArray *titleArr;

@property (nonatomic, copy)   NSString *keyWord;

@property (nonatomic, copy) void (^ClickBlock)(NSString *word,NSInteger indx);



@end
