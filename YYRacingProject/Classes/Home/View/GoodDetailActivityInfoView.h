//
//  GoodDetailActivityInfoView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailActivityInfoView : UIView

@property (nonatomic, strong) NSMutableArray *activityItems;

@property (nonatomic, strong) NSMutableArray *activityViewArr;


@end




NS_ASSUME_NONNULL_END
