//
//  HomeHeaderInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HomeHeaderInfoView;
@class HomeResourceItemInfoModel;
@protocol HomeHeaderViewDelegate <NSObject>

@optional
- (void)headerViewItemDidSelect:(HomeHeaderInfoView *)view
                       sourceModel:(HomeResourceItemInfoModel *)sourceModel;

@end

@interface HomeHeaderInfoView : UIView

@property (nonatomic, weak) id <HomeHeaderViewDelegate> delegate;
@property (nonatomic, copy) void (^homeHeaderHeightUpdateBlock)(void);
@property (nonatomic, strong) NSMutableArray *headerSources;  

@end
