//
//  GoodDetailRecommendInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class GoodDetailRecommendInfoCell;
@class HomeListStructInfoModel;
@protocol GoodDetailRecommendInfoCellDelegate <NSObject>

@optional
- (void)goodDetailRecommendCell:(GoodDetailRecommendInfoCell *)cell  didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end


@interface GoodDetailRecommendInfoCell : YYBaseTableViewCell

@property (nonatomic, weak) id <GoodDetailRecommendInfoCellDelegate> delegate;
@property (nonatomic, strong) HomeListStructInfoModel  *itemModels;

@end

NS_ASSUME_NONNULL_END
