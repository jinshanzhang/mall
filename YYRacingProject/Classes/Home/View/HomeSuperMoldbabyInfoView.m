//
//  HomeSuperMoldbabyInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSuperMoldbabyInfoView.h"

@interface JMMenuBarSuperModel : NSObject //<JMMenuBarItemDataSource>

@property (nonatomic, strong) NSString *content;

@end

@implementation JMMenuBarSuperModel

@end


@interface JMMenuBarSuperItem : JMMenuBarItem

@property (nonatomic, strong) UILabel  *titleLabel;

@end

@implementation JMMenuBarSuperItem

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        self.titleLabel.font = normalFont(13);
        self.titleLabel.textColor = HexRGB(0x333333);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.backgroundColor = XHClearColor;
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
    }];
}

- (void)setItemSelect:(BOOL)itemSelect {
    [super setItemSelect:itemSelect];
    if (itemSelect) {
        self.titleLabel.textColor = HexRGB(0x222222);
        self.titleLabel.font = boldFont(13);
    }
    else {
        self.titleLabel.textColor = HexRGB(0x333333);
        self.titleLabel.font = normalFont(13);
    }
}

@end


#import "YYMultiScrollView.h"

@protocol SuperHeaderViewDelegate <NSObject>

@optional
// menuBar 切换
- (void)superHeaderMenuSwitchToIndex:(NSInteger)index;

// 超级爆款点击
- (void)superHeaderDidSelect;

@end

@interface SuperHeaderView : UIView
<
JMMenuBarDelegate,
JMMenuBarDataSource
>
@property (nonatomic, strong) UIImageView       *hotImageView;
@property (nonatomic, strong) UIImageView       *arrowImageView;
@property (nonatomic, strong) UILabel           *titleLabel;

@property (nonatomic, strong) JMMenuBar         *leftMenuBar;
@property (nonatomic, strong) UIImageView       *indicatorView;

@property (nonatomic, weak) id <SuperHeaderViewDelegate> delegate;

@property (nonatomic, strong) NSMutableArray    *leftMenuDatasource;

@end

@implementation SuperHeaderView

#pragma mark - Setter method
// 顶部分栏控件
- (JMMenuBar *)leftMenuBar {
    if (!_leftMenuBar) {
        _leftMenuBar = [[JMMenuBar alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(24))];
        _leftMenuBar.backgroundColor = XHWhiteColor;
        _leftMenuBar.showsHorizontalScrollIndicator = NO;
        _leftMenuBar.showsVerticalScrollIndicator = NO;
        _leftMenuBar.clipsToBounds = YES;
        _leftMenuBar.scrollsToTop = NO;
        _leftMenuBar.scrollEnabled = NO;
        _leftMenuBar.isUseCache = NO;
        _leftMenuBar.menuBarItemSpace = kSizeScale(10);
        _leftMenuBar.currentItemIndex = 1;
        _leftMenuBar.v_cornerRadius = 2;
        _leftMenuBar.backgroundColor = XHClearColor;
        _leftMenuBar.selectItemStayType = JMMenuBarStayLocationDefault;
        _leftMenuBar.menuBarLayoutStyle = JMMenuBarLayoutStyleDivide;
        _leftMenuBar.menuBarDelegate = self;
        _leftMenuBar.menuBarDataSource = self;
    }
    return _leftMenuBar;
}

- (UIImageView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    }
    _indicatorView.v_cornerRadius = kSizeScale(1.5);
    return _indicatorView;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.leftMenuDatasource = [NSMutableArray array];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.hotImageView = [YYCreateTools createImageView:@"home_super_hot_icon"
                                             viewModel:-1];
    self.hotImageView.hidden = YES;
    [self addSubview:self.hotImageView];
    
    self.arrowImageView = [YYCreateTools createImageView:@"home_super_more_icon"
                                             viewModel:-1];
    self.arrowImageView.hidden = YES;
    [self addSubview:self.arrowImageView];
    [self.arrowImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(superHotTitleClick:)]];

    self.titleLabel = [YYCreateTools createLabel:@"超级爆款"
                                            font:boldFont(17)
                                       textColor:XHBlackColor];
    self.titleLabel.userInteractionEnabled = YES;
    [self.titleLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(superHotTitleClick:)]];
    [self addSubview:self.titleLabel];
    
    for (int i = 0; i < 2; i ++) {
        JMMenuBarSuperModel *model = [[JMMenuBarSuperModel alloc] init];
        model.content = (i == 0 ? @"正在抢购" : @"即将开抢");
        [self.leftMenuDatasource addObject:model];
    }
    self.leftMenuBar.menuBarContents = self.leftMenuDatasource;
    [self addSubview:self.leftMenuBar];
    
    [self addSubview:self.indicatorView];
    /*self.indicatorView.image = [UIImage gradientColorImageFromColors:@[HexRGB(0xF8CF9B), HexRGB(0xF2B970)]
                                                        gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(kSizeScale(50), kSizeScale(3))];*/
    self.indicatorView.image = [UIImage imageWithColor:HexRGB(0xF2B970)];
    [self.leftMenuBar reloadData];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.mas_equalTo(kSizeScale(16));
        //make.centerY.equalTo(self.leftMenuBar);
    }];
    
    [self.leftMenuBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.right.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(126), kSizeScale(24)));
    }];
    
    JMMenuBarSuperItem *item = (JMMenuBarSuperItem *)[self.leftMenuBar gainItemSelectState];
    [self.indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(item);
        make.top.equalTo(self.leftMenuBar.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(3)));
    }];
    /*[self.hotImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(kSizeScale(12));
        make.centerY.equalTo(self.leftMenuBar);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(18), kSizeScale(18)));
    }];
    [self.arrowImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(13), kSizeScale(13)));
        make.left.equalTo(self.titleLabel.mas_right).offset(kSizeScale(2));
        make.centerY.equalTo(self.titleLabel).offset(1);
    }];
     */
}

#pragma mark - Public
- (void)indicatorViewChangeByItemIndex:(NSInteger)itemIndex {
    JMMenuBarSuperItem *item = (JMMenuBarSuperItem *)[self.leftMenuBar gainItemSelectState];
    CGRect frame = self.indicatorView.frame;
    CGFloat value = (item.width - kSizeScale(50))/2.0;
    frame.origin.x = self.leftMenuBar.originX + item.originX + value;
    
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.indicatorView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    self.indicatorView.image = [UIImage imageWithColor:(itemIndex==0?HexRGB(0xF2B970):HexRGB(0x2BB04B))];
}

#pragma mark - JMMenuBarDataSource
- (JMMenuBarItem *)menuBar:(JMMenuBar *)menuBar menuBarItemAtIndex:(NSInteger)itemIndex {
    static NSString *testIdentifier = @"JMMenuBarSuperItem";
    JMMenuBarSuperItem *item = (JMMenuBarSuperItem *)[menuBar dequeueReusableItemWithIdentifier:testIdentifier];
    if (!item) {
        item = [[JMMenuBarSuperItem alloc] init];
    }
    if (self.leftMenuDatasource.count > 0) {
        JMMenuBarSuperModel *cls = [self.leftMenuDatasource objectAtIndex:itemIndex];
        item.titleLabel.text = cls.content;
    }
    return item;
}

#pragma mark - JMMenuBarDelegate
- (void)menuBar:(JMMenuBar *)menuBar didSelectItemAtIndex:(NSInteger)itemIndex {
    if (self.delegate && [self.delegate respondsToSelector:@selector(superHeaderMenuSwitchToIndex:)]) {
        [self.delegate superHeaderMenuSwitchToIndex:itemIndex];
        //[self indicatorViewChangeByItemIndex:itemIndex];
    }
}

- (CGFloat)menuBar:(JMMenuBar *)menuBar menuBarItemWidthAtIndex:(NSInteger)itemIndex {
    return kSizeScale(58);
}

#pragma mark - Event
- (void)superHotTitleClick:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(superHeaderDidSelect)]) {
        [self.delegate superHeaderDidSelect];
    }
}

@end


// 超级爆款视图
#import "yyCountDownManager.h"
#import "HomeResourcesInfoModel.h"
#import "HomeResourceItemInfoModel.h"
@interface HomeSuperMoldbabyInfoView ()
<
SuperHeaderViewDelegate,
YYMultiScrollViewDelegate>

@property (nonatomic, strong) UIView *scrollBgView;
@property (nonatomic, strong) SuperHeaderView  *headerView;
@property (nonatomic, strong) YYMultiScrollView *scrollView;

@property (nonatomic, assign) NSInteger      firstSelectType; // 首次选中的类型
@property (nonatomic, assign) NSInteger      willRobItemCount;// 即将开抢的item数量
@property (nonatomic, assign) NSInteger      firstWillRobItemIndex; //第一个即将开抢的位置
@property (nonatomic, strong) NSMutableArray *containTypes;    // 包含的类型数组

@property (nonatomic, assign) BOOL    isMenuSelect;    //是否是菜单选择
@property (nonatomic, assign) BOOL    isClickSwitch;   //是否可以点击切换
@end

@implementation HomeSuperMoldbabyInfoView

#pragma mark - Setter method
- (YYMultiScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[YYMultiScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.delegate = self;
        _scrollView.backgroundColor = XHWhiteColor;
    }
    return _scrollView;
}

- (void)setResourceModel:(HomeResourcesInfoModel *)resourceModel {
    _resourceModel = resourceModel;
    if (_resourceModel) {
        NSInteger menuDefault = 0;
        [self.containTypes removeAllObjects];
        [kConfigCountDown reloadSourceWithIdentifier:@"superHot"];
        self.scrollView.resourceModel = resourceModel;
        [_resourceModel.items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeResourceItemInfoModel *itemModel = (HomeResourceItemInfoModel *)obj;
            if (![self.containTypes containsObject:itemModel.saleStatus]) {
                [self.containTypes addObject:itemModel.saleStatus];
            }
        }];
        if (kValidDictionary(resourceModel.link)) {
            NSString *uri = [resourceModel.link objectForKey:@"uri"];
            if (kValidString(uri)) {
                self.headerView.arrowImageView.hidden = NO;
                HomeResourceItemInfoModel *itemModel = [[HomeResourceItemInfoModel alloc] init];
                itemModel.scrollType = YYSingleRowScrollEmpty;
                [resourceModel.items addObject:itemModel];
            }
        }
        
        if (self.containTypes.count >= 2) {
            //存在抢购中和即将开抢二种样式，可以切换
            self.firstSelectType = 0;
            self.isClickSwitch = YES;
        }
        else {
            self.isClickSwitch = NO;
            NSNumber *type = [self.containTypes firstObject];
            self.firstSelectType = [type integerValue];
            if ([type integerValue] == 3) {
                // 如果是抢购中则选中抢购中
                menuDefault = 0;
            }
            else {
                // 否则则选中即将开抢
                menuDefault = 1;
            }
        }
        self.willRobItemCount = [self gainWillRobCount];
        self.firstWillRobItemIndex = [self gainWillRobAtIndex];
        self.headerView.leftMenuBar.currentItemIndex = menuDefault;
        [self.headerView.leftMenuBar updateItemSelectStateAtIndex:menuDefault];
        [self.headerView indicatorViewChangeByItemIndex:menuDefault];
        [self.scrollView scrollItemArriveAssignLocation:0
                                              robCounts:self.willRobItemCount];
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isMenuSelect = NO;
        self.isClickSwitch = NO;
        self.containTypes = [NSMutableArray array];
        self.backgroundColor = XHClearColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    [kConfigCountDown setCountDownTimeInterval:0.1f];
    [kConfigCountDown addSourceWithIdentifier:@"superHot"];
    [kConfigCountDown start];
    self.headerView = [[SuperHeaderView alloc] initWithFrame:CGRectZero];
    self.headerView.backgroundColor = XHClearColor;
    self.headerView.delegate = self;

    self.scrollBgView = [YYCreateTools createView:XHWhiteColor];
    self.scrollBgView.v_cornerRadius = kSizeScale(5);
    [self addSubview:self.scrollBgView];
    
    [self addSubview:self.headerView];
    [self.scrollBgView addSubview:self.scrollView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)||self.frame.size.height<1) {
        return;
    }
    [self.headerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(48));
    }];
    [self.scrollBgView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.headerView.mas_bottom);
        make.bottom.equalTo(self);
    }];
    [self.scrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollBgView);
        make.right.equalTo(self.scrollBgView);
        make.top.mas_equalTo(kSizeScale(12));
        make.bottom.equalTo(self.scrollBgView);
    }];
}

#pragma mark - Private
/**获取即将开抢的第一个item的位置**/
- (NSInteger)gainWillRobAtIndex {
    __block NSInteger index = -1;
    [self.resourceModel.items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeResourceItemInfoModel *itemModel = (HomeResourceItemInfoModel *)obj;
        if ([itemModel.saleStatus integerValue] == 2) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

/**获取即将开抢的数据条目**/
- (NSInteger)gainWillRobCount {
    __block NSInteger index = 0;
    [self.resourceModel.items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeResourceItemInfoModel *itemModel = (HomeResourceItemInfoModel *)obj;
        if ([itemModel.saleStatus integerValue] == 2) {
            index ++;
        }
    }];
    return index;
}

#pragma mark - SuperHeaderViewDelegate

- (void)superHeaderMenuSwitchToIndex:(NSInteger)index {
    if (self.isClickSwitch) {
        // 可以点击切换
        self.headerView.leftMenuBar.currentItemIndex = index;
        [self.headerView.leftMenuBar updateItemSelectStateAtIndex:index];
        [self.headerView indicatorViewChangeByItemIndex:index];
        [self.scrollView scrollItemArriveAssignLocation:(index==0?index:self.firstWillRobItemIndex)
                                              robCounts:self.willRobItemCount];
    }
    else {
        // 不可以点击切换
        if (self.firstSelectType == 3 && index != 0) {
            [YYCommonTools showTipMessage:@"暂无即将开抢商品"];
            return;
        }
        else if (self.firstSelectType == 2 && index == 0){
            [YYCommonTools showTipMessage:@"暂无抢购中商品"];
            return;
        }
    }
    self.isMenuSelect = YES;
}

- (void)superHeaderDidSelect {
    if (!self.headerView.arrowImageView.hidden && kValidDictionary(self.resourceModel.link)) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        NSString *uri = [self.resourceModel.link objectForKey:@"uri"];
        NSNumber *linkType = [self.resourceModel.link objectForKey:@"linkType"];
        if (linkType) {
            [params setObject:linkType forKey:@"linkType"];
        }
        if (kValidString(uri)) {
            [params setObject:uri forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    }
}

#pragma mark - YYMultiScrollViewDelegate
- (void)mulitiScrollViewDidSelect:(NSIndexPath *)indexPath {
    if (kValidArray(self.resourceModel.items) && indexPath.row < self.resourceModel.items.count) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        HomeResourceItemInfoModel *itemModel = [self.resourceModel.items objectAtIndex:indexPath.row];
        if (itemModel.scrollType != YYSingleRowScrollEmpty) {
            [params setObject:itemModel.linkType forKey:@"linkType"];
            if ([itemModel.superTrendyType integerValue] == 0) {
                // 单品
                [params setObject:itemModel.uri forKey:@"url"];
            }
            else if ([itemModel.superTrendyType integerValue] == 1) {
                // 品牌
                [params setObject:itemModel.skipBrandTrendyUri forKey:@"url"];
            }
            [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
        }
        else {
            if (kValidDictionary(self.resourceModel.link)) {
                [params setObject:[self.resourceModel.link objectForKey:@"linkType"] forKey:@"linkType"];
                if ([self.resourceModel.link objectForKey:@"uri"]) {
                    NSString *url = [self.resourceModel.link objectForKey:@"uri"];
                    if (kValidString(url)) {
                        [params setObject:url forKey:@"url"];
                    }
                }
            }
            [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
        }
    }
}

- (void)mulitiScrollViewDidScroll:(UICollectionView *)collectionView {
    CGFloat offsetX = collectionView.contentOffset.x;
    if (self.isClickSwitch && !self.isMenuSelect) {
        // 可以点击的操作
        NSInteger pointIndex = 0;
        NSInteger count = self.willRobItemCount;
        if (count <= 2) {
            pointIndex = (self.firstWillRobItemIndex-count) * (kSizeScale(120) + kSizeScale(8));
        }
        else {
            pointIndex = self.firstWillRobItemIndex * (kSizeScale(120) + kSizeScale(8));
        }
        if (offsetX >= pointIndex) {
            self.headerView.leftMenuBar.currentItemIndex = 1;
        }
        else {
            self.headerView.leftMenuBar.currentItemIndex = 0;
        }
        [self.headerView.leftMenuBar updateItemSelectStateAtIndex:self.headerView.leftMenuBar.currentItemIndex];
        [self.headerView indicatorViewChangeByItemIndex:self.headerView.leftMenuBar.currentItemIndex];
    }
}

- (void)mulitiScrollViewDidEndDecelerating:(UIScrollView *)scrollView {
     self.isMenuSelect = NO;
}

- (void)mulitiScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (decelerate) {
        self.isMenuSelect = NO;
    }
}


@end
