//
//  GoodDetailArgumentInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailArgumentInfoCell.h"

@interface GoodDetailArgumentInfoCell()

@property (nonatomic, strong) YYLabel  *nameLabel;
@property (nonatomic, strong) YYLabel  *contentLabel;

@end

@implementation GoodDetailArgumentInfoCell

- (void)createUI {
    self.nameLabel = [YYCreateTools createLabel:nil
                                           font:midFont(13)
                                      textColor:XHBlackColor
                                       maxWidth:kSizeScale(80)
                                  fixLineHeight:kSizeScale(15)];
    self.nameLabel.textVerticalAlignment = YYTextVerticalAlignmentTop;
    [self.contentView addSubview:self.nameLabel];
    
    self.contentLabel = [YYCreateTools createLabel:nil
                                           font:midFont(13)
                                      textColor:XHBlackLitColor
                                       maxWidth:kSizeScale(kScreenW-100)
                                  fixLineHeight:kSizeScale(15)];
    self.contentLabel.textVerticalAlignment = YYTextVerticalAlignmentTop;
    [self.contentView addSubview:self.contentLabel];

    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.contentView).offset(kSizeScale(5));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(5));
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(100));
        make.top.equalTo(self.contentView).offset(kSizeScale(5));
        make.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(5));
    }];
}

- (void)setDetailArgument:(NSString *)keyName
                valueName:(NSString *)valueName {
    self.nameLabel.text = keyName;
    self.contentLabel.text = valueName;
}

@end
