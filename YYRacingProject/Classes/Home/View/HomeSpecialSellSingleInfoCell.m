//
//  HomeSpecialSellSingleInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "HomeSpecialSellSingleInfoCell.h"

#import "UIColor+Extension.h"
#import "HomeListStructInfoModel.h"
#import "HomeListItemGoodInfoModel.h"
@interface  HomeSpecialSellSingleInfoCell ()

@property (nonatomic, strong) UIView      *shadowView;
@property (nonatomic, strong) UIView      *groundView;// 背景图片
@property (nonatomic, strong) UIImageView *hotImageView;
@property (nonatomic, strong) UIImageView *markImageView;
@property (nonatomic, strong) YYLabel     *hotGoodNameLabel;
@property (nonatomic, strong) UILabel     *goodPriceLabel;
@property (nonatomic, strong) UILabel     *commisionLabel;
@property (nonatomic, strong) UILabel     *sellerPointLabel;
@property (nonatomic, strong) UILabel     *couponLabel;//券提示

@property (nonatomic, strong) UIButton    *buyBtn;
@property (nonatomic, strong) UIButton    *shareBtn;

@end

@implementation HomeSpecialSellSingleInfoCell

#pragma mark - Setter
- (void)setInfoModel:(HomeListStructInfoModel *)infoModel {
    _infoModel = infoModel;
    if (_infoModel) {
        [self setCellSubViewContent];
    }
}

#pragma mark - Life cycle

- (void)createUI {
    self.backgroundColor = self.contentView.backgroundColor = XHStoreGrayColor;
    
    self.shadowView = [YYCreateTools createView:XHClearColor];
    self.shadowView.frame = CGRectMake(kSizeScale(12), kSizeScale(12), kScreenW-kSizeScale(12)*2, kSizeScale(160)+kSizeScale(100));
    [self.contentView addSubview:self.shadowView];
    
    self.groundView = [YYCreateTools createView:XHWhiteColor];
    self.groundView.frame = CGRectMake(0, 0, self.shadowView.width, self.shadowView.height - 0.5);
    self.groundView.v_cornerRadius = 5;
    [self.shadowView addSubview:self.groundView];

    self.hotImageView = [YYCreateTools createImageView:nil viewModel:-1];
    self.hotImageView.clipsToBounds = YES;
    self.hotImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.groundView addSubview:self.hotImageView];
    
    self.markImageView = [YYCreateTools createImageView:nil viewModel:-1];
    [self.groundView addSubview:self.markImageView];
    
    self.couponLabel = [YYCreateTools createLabel:nil
                                             font:normalFont(10)
                                        textColor:HexRGB(0xFED18A)];
    self.couponLabel.v_cornerRadius = 2;
    self.couponLabel.textAlignment = NSTextAlignmentCenter;
    self.couponLabel.backgroundColor = [UIColor colorWithPatternImage:[UIImage gradientColorImageFromColors:@[HexRGB(0x444444),HexRGB(0x222222)] gradientType:GradientTypeUpleftToLowright imgSize:CGSizeMake(kSizeScale(50), kSizeScale(16))]];
    [self.groundView addSubview:self.couponLabel];
    
    self.hotGoodNameLabel = [YYCreateTools createLabel:nil
                                                  font:midFont(15)
                                             textColor:XHBlackColor
                                              maxWidth:kScreenW-kSizeScale(24)
                                         fixLineHeight:kSizeScale(15)];
    self.hotGoodNameLabel.numberOfLines = 1;
    [self.groundView addSubview:self.hotGoodNameLabel];
    
    self.sellerPointLabel = [YYCreateTools createLabel:nil
                                                  font:midFont(13)
                                             textColor:XHBlackLitColor];
    [self.groundView addSubview:self.sellerPointLabel];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(18)
                                           textColor:XHBlackColor];
    self.goodPriceLabel.numberOfLines = 1;
    self.goodPriceLabel.preferredMaxLayoutWidth = kSizeScale(60);
    [self.groundView addSubview:self.goodPriceLabel];
    
    self.commisionLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(18)
                                           textColor:XHRedColor];
    [self.groundView addSubview:self.commisionLabel];

    self.buyBtn = [YYCreateTools createButton:@"立即购买"
                                      bgColor:XHRedColor
                                    textColor:XHWhiteColor];
    self.buyBtn.hidden = YES;
    self.buyBtn.v_cornerRadius = 2;
    self.buyBtn.titleLabel.font = midFont(12);
    [self.groundView addSubview:self.buyBtn];
    [self.buyBtn addTarget:self action:@selector(btnClickOperator:) forControlEvents:UIControlEventTouchUpInside];
    
    self.shareBtn = [YYCreateTools createBtn:@"分享"
                                        font:midFont(12)
                                   textColor:XHBlackColor];
    self.shareBtn.hidden = YES;
    [self.shareBtn setImage:[UIImage imageNamed:@"home_share_icon"] forState:UIControlStateNormal];
    [self.shareBtn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextRight imageTitleSpace:kSizeScale(5)];
    [self.shareBtn addTarget:self action:@selector(btnClickOperator:) forControlEvents:UIControlEventTouchUpInside];
    [self.groundView addSubview:self.shareBtn];
    
    // layout
    /*[self.groundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(kSizeScale(12));
        make.right.offset(-kSizeScale(12));
        make.bottom.offset(0);
    }];*/
    
    [self.hotImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.right.equalTo(self.groundView);
        make.height.mas_equalTo(kSizeScale(160));
    }];
    
    [self.markImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.hotImageView);
        make.left.mas_equalTo(kSizeScale(16));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(48), kSizeScale(48)));
    }];
    
    [self.couponLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.hotImageView.mas_bottom).offset(kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(0, kSizeScale(15)));
    }];
    
    [self.hotGoodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponLabel.mas_right);
        make.top.equalTo(self.couponLabel).offset(-1);
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(20)).priorityHigh();
    }];
    
    [self.sellerPointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponLabel);
        make.top.equalTo(self.hotGoodNameLabel.mas_bottom).offset(kSizeScale(2));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sellerPointLabel);
        make.top.equalTo(self.sellerPointLabel.mas_bottom).offset(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(20));
        make.bottom.equalTo(self.groundView).offset(-kSizeScale(16)).priorityHigh();
    }];
    
    [self.commisionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(10));
        make.centerY.equalTo(self.goodPriceLabel);
    }];
    
    [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.goodPriceLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(63), kSizeScale(22)));
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(6));
        make.centerY.equalTo(self.goodPriceLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(65), kSizeScale(24)));
    }];
    
    [self.groundView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                            initWithTarget:self
                                           action:@selector(cellClickOperator:)]];
    // 增加阴影
    /*[self.shadowView addShadow:ShadowDirectionBottom
                   shadowColor:HexRGBALPHA(0x000000, 0.2)
                 shadowOpacity:0.8
                  shadowRadius:0.5
               shadowPathWidth:0.3];*/
}

#pragma mark - Private
- (void)setCellSubViewContent {
    HomeListItemGoodInfoModel *itemModel = _infoModel.item;
    [self.hotImageView yy_sdWebImage:itemModel.coverImage.imageUrl
                placeholderImageType:YYPlaceholderImageHomeBannerType];
    NSMutableAttributedString *contentAttribute = [[NSMutableAttributedString alloc] init];
    if (itemModel.sourceIcon) {
        UIImage *image = [UIImage getImageFromURL:itemModel.sourceIcon];
        NSMutableAttributedString *postAttribute = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeScaleAspectFit attachmentSize:CGSizeMake(image.size.width/2.0, image.size.height/2.0) alignToFont:normalFont(12.5) alignment:YYTextVerticalAlignmentCenter];
        [contentAttribute appendAttributedString:postAttribute];
        
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:itemModel.title];
        [contentStr yy_setAttributes:XHBlackColor
                                font:midFont(15)
                           wordSpace:@(0)
                             content:contentStr
                           alignment:NSTextAlignmentLeft];
        [contentAttribute appendAttributedString:contentStr];
    }
    else {
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:itemModel.title];
        [contentStr yy_setAttributes:XHBlackColor
                                font:boldFont(15)
                             content:contentStr
                           alignment:NSTextAlignmentLeft];
        [contentAttribute appendAttributedString:contentStr];
    }
    
    // 券展示处理
    if (kValidDictionary(itemModel.coupon)) {
        if (kValidDictionary(itemModel.coupon)) {
            if ([itemModel.coupon objectForKey:@"maxDenomination"]) {
                CGSize couponSize = CGSizeZero;
                NSString *maxDenomination = [itemModel.coupon objectForKey:@"maxDenomination"];
                if (kValidString(maxDenomination)) {
                    NSString *showContent = [NSString stringWithFormat:@"领券减¥%@",maxDenomination];
                    couponSize = [YYCommonTools sizeWithText:showContent
                                                        font:normalFont(10)
                                                     maxSize:CGSizeMake(MAXFLOAT, kSizeScale(16))];
                    couponSize.width = couponSize.width + kSizeScale(8);
                    couponSize.height = kSizeScale(16);
                    self.couponLabel.text = showContent;
                }
                else {
                    self.couponLabel.text = nil;
                }
                
                [self.couponLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.width.mas_equalTo(couponSize.width);
                }];
                [self.hotGoodNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.couponLabel.mas_right).offset(couponSize.width > 0 ? kSizeScale(3) : 0);
                }];
            }
        }
    }
    
    self.hotGoodNameLabel.attributedText = contentAttribute;
    self.sellerPointLabel.text = itemModel.text;
    self.buyBtn.hidden = NO;
    self.shareBtn.hidden = YES;
    if (kIsFan) {
        // 粉丝
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:midFont(12) symbolTextColor:[itemModel.saleStatus integerValue] == 2?HexRGB(0x11BC56): XHRedColor wordSpace:2 price:itemModel.reservePrice priceFont:boldFont(15) priceTextColor:[itemModel.saleStatus integerValue] == 2?HexRGB(0x11BC56): XHRedColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
        NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",itemModel.originPrice]];
        [origin yy_setAttributes:XHBlackLitColor
                            font:midFont(13)
                         content:origin
                       alignment:NSTextAlignmentCenter];
        [origin yy_setAttributeOfDeleteLine:NSMakeRange(0, origin.length)];
        self.commisionLabel.attributedText = origin;
    }
    else {
        // 会员
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:midFont(12) symbolTextColor:XHBlackColor wordSpace:2 price:itemModel.reservePrice priceFont:boldFont(15) priceTextColor: XHBlackColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
        NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(13) symbolTextColor:[itemModel.saleStatus integerValue] == 2?HexRGB(0x11BC56): XHRedColor wordSpace:2 price:itemModel.commission priceFont:boldFont(15) priceTextColor:[itemModel.saleStatus integerValue] == 2?HexRGB(0x11BC56):XHRedColor symbolOffsetY:1.2];
        self.commisionLabel.attributedText = extra;
    }
    [self.buyBtn setTitle:(kIsFan)?([itemModel.saleStatus integerValue] == 2?@"即将开枪":@"立即抢购"):@"分享赚钱" forState:UIControlStateNormal];
    
    [self.buyBtn setBackgroundImage:[itemModel.saleStatus integerValue] == 2? [UIImage imageWithColor:HexRGB(0x11BC56)] : [UIImage gradientColorImageFromColors:@[HexRGB(0xE6B96E), HexRGB(0xE2A848)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(kSizeScale(63), kSizeScale(22))] forState:UIControlStateNormal];
    
    self.markImageView.hidden = kValidString(itemModel.tag)?NO:YES;
    if (kValidString(itemModel.tag)) {
        [self.markImageView sd_setImageWithURL:[NSURL URLWithString:itemModel.tag]];
    }
}

#pragma mark - Event

- (void)btnClickOperator:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeSpecialSellSingleCellCarrayModel:isShare:)]) {
        [self.delegate homeSpecialSellSingleCellCarrayModel:self.infoModel
                                         isShare:!kIsFan];
    }
}

- (void)cellClickOperator:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeSpecialSellSingleCellCarrayModel:isShare:)]) {
        [self.delegate homeSpecialSellSingleCellCarrayModel:self.infoModel
                                                    isShare:NO];
    }
}

@end
