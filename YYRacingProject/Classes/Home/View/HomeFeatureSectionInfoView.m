//
//  HomeFeatureSectionInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/29.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeFeatureSectionInfoView.h"
#import "HomeTimeAlexMenuBarItem.h"

#import "HomeTimeAxleInfoModel.h"
@interface HomeFeatureSectionInfoView()
<JMMenuBarDelegate,
JMMenuBarDataSource>

@property (nonatomic, strong) JMMenuBar *menuView;
@property (nonatomic, strong) UIView    *lineView;

@end

@implementation HomeFeatureSectionInfoView

#pragma mark - Setter
- (JMMenuBar *)menuView {
    if (!_menuView) {
        _menuView = [[JMMenuBar alloc] initWithFrame:CGRectZero];
        _menuView.backgroundColor = XHWhiteColor;
        _menuView.showsHorizontalScrollIndicator = NO;
        _menuView.showsVerticalScrollIndicator = NO;
        _menuView.clipsToBounds = YES;
        _menuView.scrollsToTop = NO;
        _menuView.isUseCache = NO;
//        _menuView.selectItemStayType = JMMenuBarStayLocationCenter;
//        _menuView.menuBarLayoutStyle = JMMenuBarLayoutStyleDivide;
        _menuView.menuBarDelegate = self;
        _menuView.menuBarDataSource = self;
    }
    return _menuView;
}

- (void)setContentDatas:(NSMutableArray *)contentDatas {
    _contentDatas = contentDatas;
    if (_contentDatas) {
        self.menuView.menuBarContents = _contentDatas;
        [self.menuView reloadData];
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self _setSubViews];
        [self _setSubLayouts];
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    if (![self pointInside:point withEvent:event]) {
        for (UIView *view in self.subviews) {
            NSInteger viewTag = view.tag;
            if (viewTag == 888) {// 分享场次 view frame 超出父视图事件链处理。
                CGPoint viewPoint = [self convertPoint:point toView:view];
                viewPoint = CGPointMake(viewPoint.x + view.frame.origin.x, viewPoint.y + view.frame.origin.y);
                if ([view isKindOfClass:[UIView class]] && CGRectContainsPoint(view.frame, viewPoint)) {
                    return view;
                }
            }
        }
    }
    return [super hitTest:point withEvent:event];
}

#pragma mark - Public
- (void)willMenuBarItemSelectAtIndex:(NSInteger)selectIndex {
    if (selectIndex >= 0) {
        self.menuView.currentItemIndex = selectIndex;
        [self.menuView updateItemSelectStateAtIndex:selectIndex];
    }
}

#pragma mark - Private
- (void)_setSubViews {
    self.lineView = [YYCreateTools createView:HexRGB(0xe7e7e7)];
    [self addSubview:self.menuView];
    [self addSubview:self.lineView];
}

- (void)_setSubLayouts {
    [self.menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.bottom.equalTo(self.mas_bottom).offset(-0.5);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(0.5);
    }];
}

#pragma mark - JMMenuBarDataSource
- (JMMenuBarItem *)menuBar:(JMMenuBar *)menuBar menuBarItemAtIndex:(NSInteger)itemIndex {
    static NSString *identifier = @"home-time-axle-item";
    HomeTimeAlexMenuBarItem *item = [menuBar dequeueReusableItemWithIdentifier:identifier];
    if (!item) {
        item = [[HomeTimeAlexMenuBarItem alloc] init];
    }
    if (itemIndex < self.contentDatas.count) {
        HomeTimeAxleInfoModel *axleModel = [self.contentDatas objectAtIndex:itemIndex];
        BOOL isTomorrow = [axleModel.timePoint integerValue] == 0?YES:NO ;
        item.isTomorrow = isTomorrow;
        item.titleLabel.text = axleModel.instruction;
        item.timeLabel.text = !isTomorrow ?axleModel.timePointAlias:axleModel.instruction;
    }
    return item;
}

#pragma mark - JMMenuBarDelegate
- (void)menuBar:(JMMenuBar *)menuBar didSelectItemAtIndex:(NSInteger)itemIndex {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeFeatureSectionView:didSelectItemAtIndex:)]) {
        [self.delegate homeFeatureSectionView:self didSelectItemAtIndex:itemIndex];
    }
}

- (CGFloat)menuBar:(JMMenuBar *)menuBar menuBarItemWidthAtIndex:(NSInteger)itemIndex {
    return kScreenW/5.0;
}
@end
