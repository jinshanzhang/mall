//
//  SearchCategoryContentView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "SearchCategoryContentView.h"

#import "HomeCateSubInfoModel.h"
#import "HomeCateSuperInfoModel.h"
#import "HomeBannerInfoModel.h"

#import "YYCategoryDetailInfoRequestAPI.h"
// 分类的父item
#pragma mark - 分类 Category Super Cell
@interface CategorySuperTableCell : UITableViewCell

@property (nonatomic, strong) UILabel  *itemTitleLbl;
@property (nonatomic, strong) UIView   *lineView;

@property (nonatomic, assign) BOOL      isSelect;

@property (nonatomic, copy)   void (^superItemBlock)(CategorySuperTableCell *currentCls);

@end

@implementation CategorySuperTableCell

#pragma mark - Setter method

- (void)setIsSelect:(BOOL)isSelect {
    _isSelect = isSelect;
    if (_isSelect) {
        self.contentView.backgroundColor = XHWhiteColor;
    }
    else {
        self.contentView.backgroundColor = HexRGB(0xf5f5f5);
    }
}

#pragma mark - Life cycle
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = HexRGB(0xf5f5f5);
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.itemTitleLbl = [YYCreateTools createLabel:nil
                                              font:midFont(13)
                                         textColor:XHBlackColor];
    [self.contentView addSubview:self.itemTitleLbl];
    
    self.lineView = [YYCreateTools createView:HexRGB(0xE9E9E9)];
    [self.contentView addSubview:self.lineView];
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClickEvent:)]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.itemTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(.5));
    }];
}

#pragma mark - Event
- (void)itemClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.superItemBlock) {
        self.superItemBlock(self);
    }
}

@end

#pragma mark - 分类 Category Item View
@protocol CategoryItemViewDelegate <NSObject>

@optional
- (void)cateItemMatchModel:(HomeCateSubInfoModel *)matchModel;

@end

@interface CategorySubItemView : UIView

@property (nonatomic, strong) UIImageView *itemImageView;
@property (nonatomic, strong) UILabel     *itemTitleLabel;

@property (nonatomic, copy)   NSString    *imageURL;
@property (nonatomic, copy)   NSString    *titleContent;

@property (nonatomic, strong) HomeCateSubInfoModel *currentModel;

@property (nonatomic, weak) id <CategoryItemViewDelegate> delegate;

@end


@implementation CategorySubItemView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.itemImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    self.itemImageView.image = [UIImage imageWithColor:XHRedColor];
    [self addSubview:self.itemImageView];
    self.itemTitleLabel = [YYCreateTools createLabel:nil
                                                font:midFont(11)
                                           textColor:XHBlackColor];
    [self addSubview:self.itemTitleLabel];
    
    [self.itemImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(1);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(60)));
    }];
    
    [self.itemTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.itemImageView);
        make.top.equalTo(self.itemImageView.mas_bottom).offset(kSizeScale(9));
    }];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewEventClickHandler:)]];
}

- (void)setImageURL:(NSString *)imageURL {
    _imageURL = imageURL;
    [self.itemImageView sd_setImageWithURL:[NSURL URLWithString:_imageURL]
                          placeholderImage:[UIImage imageWithColor:HexRGB(0xf5f5f5)]];
}

- (void)setTitleContent:(NSString *)titleContent {
    NSString *content = nil;
    _titleContent = titleContent;
    if ([NSString textLength:_titleContent] > 10) {
        content = [NSString stringWithFormat:@"%@...",[_titleContent changeProductNameWithProductName:_titleContent length:10]];
    }
    else {
        content = _titleContent;
    }
    self.itemTitleLabel.text = content;
}

#pragma mark - Event

- (void)viewEventClickHandler:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(cateItemMatchModel:)]) {
        [self.delegate cateItemMatchModel:self.currentModel];
    }
}

@end

#pragma mark - 分类 Category Item Cell
// 分类的子item
@interface CategorySubCollectionCell : UITableViewCell
<CategoryItemViewDelegate>

@property (nonatomic, strong)  UIView           *cateFrameView;
@property (nonatomic, strong)  NSMutableArray   *cateItemViews;

@property (nonatomic, strong)  NSMutableArray   *cateChildArrs;
@end

@implementation  CategorySubCollectionCell

#pragma mark - Setter method
// 分类中的子类目赋值
- (void)setCateChildArrs:(NSMutableArray *)cateChildArrs {
    _cateChildArrs = cateChildArrs;
    __block NSInteger cateCount = _cateChildArrs.count;
    [self.cateItemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        CategorySubItemView *itemView = (CategorySubItemView *)obj;
        if (cateCount > idx) {
            HomeCateSubInfoModel *infoModel = [self.cateChildArrs objectAtIndex:idx];
            // 显示
            itemView.hidden = NO;
            itemView.currentModel = infoModel;
            itemView.imageURL = infoModel.imageUrl;
            itemView.titleContent = infoModel.name;
        }
        else {
            // 隐藏
            itemView.hidden = YES;
        }
    }];
}

#pragma mark - Life cycle
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    CGFloat itemW = (kScreenW-kSizeScale(75))/3.0;
    self.cateItemViews = [NSMutableArray array];
    
    self.cateFrameView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.cateFrameView];
    [self.cateFrameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    for (int i = 0; i < 3; i ++) {
        CategorySubItemView *tempView = [[CategorySubItemView alloc] initWithFrame:CGRectZero];
        tempView.delegate = self;
        [self.cateFrameView addSubview:tempView];
        [tempView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo((itemW)*i);
            make.top.bottom.equalTo(self.cateFrameView);
            make.width.mas_equalTo(itemW);
         }];
        [self.cateItemViews addObject:tempView];
    }
}

#pragma mark - CategoryItemViewDelegate
- (void)cateItemMatchModel:(HomeCateSubInfoModel *)matchModel {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSDictionary *target = matchModel.target;
    if ([target objectForKey:@"linkType"]) {
        [params setObject:[target objectForKey:@"linkType"] forKey:@"linkType"];
    }
    if ([target objectForKey:@"url"]) {
        [params setObject:[target objectForKey:@"url"] forKey:@"url"];
    }
    [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
}
@end

#pragma mark - 分类 Cate TableViewHeader
@interface CategoryTableHeaderView : UIView

@property (nonatomic, assign) NSInteger isOpenBanner;
@property (nonatomic, strong) HomeBannerInfoModel *bannerModel;

@property (nonatomic, strong) UIView          *bannerBgView;
@property (nonatomic, strong) UIImageView     *bannerView;
@property (nonatomic, strong) MASConstraint   *bannerHeight;

@end

@implementation CategoryTableHeaderView

- (void)setBannerModel:(HomeBannerInfoModel *)bannerModel {
    _bannerModel = bannerModel;
    if (_bannerModel) {
        [self.bannerView yy_sdWebImage:_bannerModel.imageUrl
                  placeholderImageType:YYPlaceholderImageHomeBannerType];
    }
    else {
        self.bannerView.image = [UIImage new];
    }
    if (_bannerModel && self.isOpenBanner == 1) {
        [self.bannerHeight install];
    }
    else {
        [self.bannerHeight uninstall];
    }
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.bannerBgView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.bannerBgView];
    [self.bannerBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(kSizeScale(5));
        make.right.equalTo(self).offset(-kSizeScale(5));
        make.bottom.equalTo(self).offset(-kSizeScale(8));
        self.bannerHeight = make.height.mas_equalTo(kSizeScale(105));
    }];
    [self.bannerHeight uninstall];
    
    self.bannerView = [YYCreateTools createImageView:nil
                                           viewModel:-1];
    [self.bannerBgView addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.bannerBgView);
    }];
    [self.bannerView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerClickEvent:)]];
}

- (void)bannerClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.bannerModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(self.bannerModel.linkType) forKey:@"linkType"];
        [params setObject:self.bannerModel.url forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    }
}

@end



#pragma mark - 分类 Cate Header View
@interface CategoryItemHeaderView : UIView

@property (nonatomic, strong) UIView   *leftLineView;
@property (nonatomic, strong) UIView   *rightLineView;

@property (nonatomic, strong) UILabel  *headTitleLbl;
@property (nonatomic, copy)   NSString *title;

@end


@implementation CategoryItemHeaderView

- (void)setTitle:(NSString *)title {
    _title = title;
    self.headTitleLbl.text = _title;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        
        self.leftLineView = [YYCreateTools createView:HexRGB(0xcccccc)];
        [self addSubview:self.leftLineView];
        
        self.rightLineView = [YYCreateTools createView:HexRGB(0xcccccc)];
        [self addSubview:self.rightLineView];
        
        self.headTitleLbl = [YYCreateTools createLabel:nil
                                                  font:midFont(13)
                                             textColor:XHBlackColor];
        self.headTitleLbl.textAlignment = NSTextAlignmentCenter;
        self.headTitleLbl.backgroundColor = XHWhiteColor;
        [self addSubview:self.headTitleLbl];
 
        [self.headTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
        }];
        
        [self.leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.headTitleLbl.mas_left).offset(-12);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(28), kSizeScale(0.5)));
            make.centerY.equalTo(self.headTitleLbl);
        }];
        
        [self.rightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headTitleLbl.mas_right).offset(12);
            make.size.equalTo(self.leftLineView);
            make.centerY.equalTo(self.headTitleLbl);
        }];
    }
    return self;
}
@end

#pragma mark - 分类主布局
@interface SearchCategoryContentView()
<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, strong)   UITableView *m_leftView;
@property (nonatomic, strong)   UITableView *m_rightView;
@property (nonatomic, strong)   CategoryTableHeaderView *tableHeader;

@property (nonatomic, strong)   NSMutableArray *leftData;
@property (nonatomic, strong)   NSMutableArray *rightData;
@property (nonatomic, assign)   NSInteger      bannerSwitch;      //banner 开关 1 开
@property (nonatomic, assign)   NSInteger      selectLeftIndex;   //左边被选中的位置
@property (nonatomic, strong)   HomeBannerInfoModel  *bannerModel;

@end

@implementation SearchCategoryContentView

#pragma mark - Setter method

- (UITableView *)m_leftView {
    if (!_m_leftView) {
        _m_leftView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _m_leftView.delegate = self;
        _m_leftView.dataSource = self;
        _m_leftView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _m_leftView.allowsSelection = NO;
        _m_leftView.rowHeight = kSizeScale(50);
        if (@available(iOS 11, *)) {
            //目前如此
            _m_leftView.estimatedRowHeight = 0;
            _m_leftView.estimatedSectionHeaderHeight = 0;
            _m_leftView.estimatedSectionFooterHeight = 0;
        }
        _m_leftView.showsVerticalScrollIndicator = NO;
        _m_leftView.backgroundColor = HexRGB(0xf5f5f5);
    }
    return _m_leftView;
}

- (UITableView *)m_rightView {
    if (!_m_rightView) {
        _m_rightView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _m_rightView.delegate = self;
        _m_rightView.dataSource = self;
        _m_rightView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _m_rightView.allowsSelection = NO;
        _m_rightView.backgroundColor = XHWhiteColor;
        _m_rightView.rowHeight = kSizeScale(98);
        _m_rightView.sectionFooterHeight = 0.1f;
        if (@available(iOS 11, *)) {
            //目前如此
            _m_rightView.estimatedRowHeight = 0;
            _m_rightView.estimatedSectionHeaderHeight = 0;
            _m_rightView.estimatedSectionFooterHeight = 0;
        }
    }
    return _m_rightView;
}

- (CategoryTableHeaderView *)tableHeader {
    if (!_tableHeader) {
        _tableHeader = [[CategoryTableHeaderView alloc] initWithFrame:CGRectZero];
    }
    return _tableHeader;
}

- (void)searchCateDatas:(NSMutableArray *)cateDatas
     defaultSelectIndex:(NSInteger)selectIndex {
    self.leftData = cateDatas;
    self.selectLeftIndex = selectIndex;
}

- (void)setSelectLeftIndex:(NSInteger)selectLeftIndex {
    _selectLeftIndex = selectLeftIndex;
    [self modifyDataPointStatus];
}

#pragma mark - Life cycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.selectLeftIndex = -1;
        self.leftData = [NSMutableArray array];
        self.rightData = [NSMutableArray array];
        
        [self createUI];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.m_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self);
        make.width.mas_equalTo(kSizeScale(75));
    }];
    
    [self.m_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.m_leftView.mas_right);
        make.top.bottom.right.equalTo(self);
    }];
}

#pragma mark - Private method

- (void)createUI {
    // 左边
    [self addSubview:self.m_leftView];
    Class current = [CategorySuperTableCell class];
    registerClass(self.m_leftView, current);
    
    // 右边
    [self addSubview:self.m_rightView];
    current = [CategorySubCollectionCell class];
    registerClass(self.m_rightView, current);
    self.m_rightView.tableHeaderView = self.tableHeader;
    [self.tableHeader mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.width.equalTo(self.m_rightView);
    }];
}

/**某个位置被选中修改数据状态**/
- (void)modifyDataPointStatus {
    [self.leftData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeCateSuperInfoModel *infoModel = (HomeCateSuperInfoModel *)obj;
        infoModel.isSelect = ((idx != self.selectLeftIndex)?NO:YES);
    }];
    [self makeSimulationData:self.selectLeftIndex];
    [self.m_leftView reloadData];
}

#pragma mark - Public method


#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.m_leftView) {
        return 1;
    }
    else if (tableView == self.m_rightView) {
        return self.rightData.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.m_leftView) {
        return self.leftData.count;
    }
    else if (tableView == self.m_rightView) {
        if (kValidArray(self.rightData) && section < self.rightData.count) {
            HomeCateSuperInfoModel *subModel = [self.rightData objectAtIndex:section];
            return subModel.childArrs.count;
        }
        return 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    if (tableView == self.m_leftView) {
        NSInteger row = indexPath.row;
        Class cls = [CategorySuperTableCell class];
        CategorySuperTableCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
        if (kValidArray(self.leftData) && row < self.leftData.count) {
            HomeCateSuperInfoModel *model = [self.leftData objectAtIndex:row];
            cell.itemTitleLbl.text = model.name;
            cell.isSelect = model.isSelect;
        }
        cell.superItemBlock = ^(CategorySuperTableCell *currentCls) {
            @strongify(self);
            NSIndexPath *currentIndex = [self.m_leftView indexPathForCell:currentCls];
            self.selectLeftIndex = currentIndex.row;
        };
        return cell;
    }
    else if (tableView == self.m_rightView) {
        NSInteger row = indexPath.row;
        NSInteger section = indexPath.section;
        Class cls = [CategorySubCollectionCell class];
        CategorySubCollectionCell *cell = (CategorySubCollectionCell *)[tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
        if (kValidArray(self.rightData) && section < self.rightData.count) {
            HomeCateSuperInfoModel *subModel = [self.rightData objectAtIndex:section];
            if (row < subModel.childArrs.count) {
                cell.cateChildArrs = [subModel.childArrs objectAtIndex:row];
            }
        }
        return cell;
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == self.m_rightView) {
        CategoryItemHeaderView *headerView = [[CategoryItemHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW-self.m_leftView.width, kSizeScale(44.0))];
        if (kValidArray(self.rightData) && section < self.rightData.count) {
            HomeCateSuperInfoModel *subModel = [self.rightData objectAtIndex:section];
            headerView.title = subModel.name;
        }
        return headerView;
    }
    return [YYCreateTools createView:XHWhiteColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == self.m_rightView) {
        return kSizeScale(44.0);
    }
    return kSizeScale(0.1);
}

#pragma mark - Private method
/**
 *  制作模拟器数据
 */
- (void)makeSimulationData:(NSInteger)rowLocation {
    if (rowLocation < 0) {
        return;
    }
    [self.rightData removeAllObjects];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidArray(self.leftData) && rowLocation < self.leftData.count) {
        HomeCateSuperInfoModel *superModel = [self.leftData objectAtIndex:rowLocation];
        [params setObject:superModel.superID forKey:@"cateID"];
    }
    
    YYCategoryDetailInfoRequestAPI *secondCateAPI = [[YYCategoryDetailInfoRequestAPI alloc] initCategoryDetailInfoRequest:params];
    [secondCateAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSArray *leve2Cats = [NSArray modelArrayWithClass:[HomeCateSuperInfoModel class]
                                                         json:[responDict objectForKey:@"leve2Cats"]];
            self.rightData = [leve2Cats mutableCopy];
            [self.rightData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeCateSuperInfoModel *superModel = (HomeCateSuperInfoModel *)obj;
                superModel.childArrs = [YYCommonTools transDataToSingleRowCont:3 transDatas:superModel.leve3Cats];
            }];
        
            self.bannerSwitch = [[responDict objectForKey:@"switchFlag"] integerValue];
            self.bannerModel = [HomeBannerInfoModel modelWithJSON:[responDict objectForKey:@"banner"]];
            // 不在这里赋值，会导致header 和 section header 之间多出空隙
            self.m_rightView.tableHeaderView = self.tableHeader;
            self.tableHeader.isOpenBanner = self.bannerSwitch;
            self.tableHeader.bannerModel = self.bannerModel;
            
            [self.m_rightView layoutIfNeeded];
        }
        [self.m_rightView reloadData];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}
@end
