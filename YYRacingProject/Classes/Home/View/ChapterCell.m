//
//  ChapterCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/24.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "ChapterCell.h"

@interface ChapterCell()

@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, strong) UIImageView *typeIMG;
@property (nonatomic, strong) UILabel  *timeLabel;
@property (nonatomic, strong)  YYButton *actionBtn;
@property (nonatomic, strong) YYLabel  *detailLabel;
 

@end
@implementation ChapterCell

-(void)createUI{
    self.titleLabel = [YYCreateTools createLabel:nil
                                            font:normalFont(15)
                                       textColor:XHBlackColor];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(12);
        make.right.mas_equalTo(-80);
        make.bottom.mas_equalTo(-38);
    }];

    self.detailLabel = [YYCreateTools createLabel:nil
                                               font:boldFont(15)
                                          textColor:XHBlackColor
                                           maxWidth:(kScreenW - kSizeScale(12) - kSizeScale(22))
                                      fixLineHeight:kSizeScale(15)];
    [self addSubview:self.detailLabel];
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(-3);
        make.bottom.mas_equalTo(-12);
    }];

    self.actionBtn = [[YYButton alloc] initWithFrame:CGRectZero];
    self.actionBtn.normalTitle = @"播放";
    self.actionBtn.selectTitle = @"暂停";
    self.actionBtn.space = kSizeScale(3);
    self.actionBtn.titleFont = normalFont(12);
    self.actionBtn.titleTextColor = XHBlackLitColor;
    self.actionBtn.select = NO;
    [self addSubview:self.actionBtn];
    
    [self.actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-18);
        make.centerY.equalTo(self);
        make.width.mas_equalTo(30);
        make.height.mas_equalTo(38);
    }];
    
    self.actionBtn.userInteractionEnabled = NO;
    
    UIView *line = [YYCreateTools createView:XHNEwLineColor];
    [self addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(self);
    }];
}

-(void)setModel:(LessonDetailModel *)model{
    _model = model;
    NSMutableAttributedString *contentAttribute = [[NSMutableAttributedString alloc] init];
    UIImage *image;
    if ([model.tryoutFlag intValue] == 1) {
        if ([model.contentType intValue] == 1) {
            image = [UIImage imageNamed:@"chapter_listen_icon"];
        }else{
            image = [UIImage imageNamed:@"chapter_watch_icon"];
        }
        
        NSMutableAttributedString *postAttribute = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeScaleAspectFit attachmentSize:CGSizeMake(image.size.width, image.size.height) alignToFont:normalFont(12.5) alignment:YYTextVerticalAlignmentCenter];
        [contentAttribute appendAttributedString:postAttribute];
        
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@",model.duration]];
        [contentStr yy_setAttributes:XHBlackLitColor
                                font:normalFont(11)
                           wordSpace:@(0)
                             content:contentStr
                           alignment:NSTextAlignmentLeft];
        [contentAttribute appendAttributedString:contentStr];
    }else{
        image = [UIImage imageNamed:@""];
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:model.duration];
        [contentStr yy_setAttributes:XHBlackLitColor
                                font:normalFont(11)
                             content:contentStr
                           alignment:NSTextAlignmentLeft];
        [contentAttribute appendAttributedString:contentStr];
    }
    self.detailLabel.attributedText = contentAttribute;
    if ([model.contentType intValue] ==1) {
        [self.actionBtn setNormalImageName:@"chapter_play_icon"];
        self.actionBtn.normalTitle = @"播放";
    } else if ([model.contentType intValue] == 2){
        [self.actionBtn setNormalImageName:@"chapter_video_icon"];
        self.actionBtn.normalTitle = @"播放";
    } else {
        [self.actionBtn setNormalImageName:@"chapter_pic_icon"];
        self.actionBtn.normalTitle = @"查看";
    }
    self.actionBtn.select = NO;
    self.titleLabel.text = model.title;
    
    if ([model.tryoutFlag intValue] ==0&& !kValidString(model.duration)) {
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(20);
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-80);
            make.bottom.mas_equalTo(-20);
        }];
    }
}

@end
