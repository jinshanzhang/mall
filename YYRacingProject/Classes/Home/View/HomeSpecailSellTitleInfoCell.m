//
//  HomeSpecailSellTitleInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/27.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSpecailSellTitleInfoCell.h"

@interface HomeSpecailSellTitleInfoCell ()

@property (nonatomic, strong) UILabel *specailTitleLabel;

@end

@implementation HomeSpecailSellTitleInfoCell

#pragma mark - Setter
- (void)setInfoModel:(HomeListStructInfoModel *)infoModel {
    _infoModel = infoModel;
    if (_infoModel) {
        self.specailTitleLabel.text = _infoModel.title;
    }
}

#pragma mark -Life cycle
- (void)createUI {
    self.contentView.backgroundColor = XHStoreGrayColor;
    self.specailTitleLabel = [YYCreateTools createLabel:@"热销推荐"
                                                   font:boldFont(16)
                                              textColor:XHBlackColor];
    [self.contentView addSubview:self.specailTitleLabel];
    
    [self.specailTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(kSizeScale(12));
        make.top.offset(kSizeScale(12));
        make.bottom.offset(-kSizeScale(5));
    }];
}

@end
