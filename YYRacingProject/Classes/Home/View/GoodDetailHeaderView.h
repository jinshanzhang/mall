//
//  GoodDetailHeaderView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodDetailHeaderView : UIView

@property (nonatomic, assign) NSInteger specialMarkStatu;
@property (nonatomic, strong) NSMutableArray *bannerArrs;

@end
