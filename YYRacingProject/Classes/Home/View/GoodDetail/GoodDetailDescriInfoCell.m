//
//  GoodDetailDescriInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailDescriInfoCell.h"

#import "YY_GoodDetailNoticeViewController.h"
#import "YY_GoodDetailDetailViewController.h"
#import "YY_GoodDetailArgumentViewController.h"
#import "YY_GoodCourceDetailViewController.h"

@interface GoodDetailDescriInfoCell()
<VTMagicViewDelegate,
VTMagicViewDataSource>

@property (nonatomic, strong) NSMutableArray *menuList;
@property (nonatomic, strong) VTMagicController *magicController;

@end

@implementation GoodDetailDescriInfoCell

#pragma mark - Setter
- (VTMagicController *)magicController {
    if (!_magicController) {
        _magicController = [[VTMagicController alloc] init];
        _magicController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _magicController.magicView.navigationColor = [UIColor whiteColor];
        _magicController.magicView.sliderHidden = YES;
        _magicController.magicView.switchStyle = VTSwitchStyleUnknown;
        _magicController.magicView.layoutStyle = VTLayoutStyleCenter;
        _magicController.magicView.navigationHeight = 36.f;
        _magicController.magicView.dataSource = self;
        _magicController.magicView.delegate = self;
        _magicController.magicView.headerView.hidden = YES;
        _magicController.magicView.needPreloading = NO;  //需要重新载入
        _magicController.magicView.itemWidth = kSizeScale(117);
    
        CGFloat padding = (kScreenW - kSizeScale(117) * 3)/2.0;
        _magicController.magicView.menuBar.isAddItemSpaceLine = YES;
        _magicController.magicView.menuBar.layer.cornerRadius = 2;
        _magicController.magicView.menuBar.layer.borderColor = XHBlackColor.CGColor;
        _magicController.magicView.menuBar.layer.borderWidth = 0.5;
        _magicController.magicView.menuBar.backgroundColor = XHBlackColor;
        _magicController.magicView.leftNavigatoinItem = _magicController.magicView.rightNavigatoinItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, padding, _magicController.magicView.navigationHeight)];
    
        _magicController.magicView.separatorColor = XHWhiteColor;
    }
    return _magicController;
}

- (void)setGoodModel:(HomeGoodInfoModel *)goodModel {
    _goodModel = goodModel;
    if (kValidArray(_goodModel.tabNames)) {
        self.menuList = _goodModel.tabNames;
    }
    if (!_goodModel.isSwitchBar) {
        [self.magicController.magicView reloadData];
    }
    else {
        [self.magicController.magicView reloadDataToPage:1];
    }
}

#pragma mark - Life cycle
- (void)createUI {
    self.menuList = [@[@"商品详情",@"商品参数",@"购买须知"] mutableCopy];
    YYBaseViewController *ctrl = (YYBaseViewController *)[YYCommonTools getCurrentVC];
    [ctrl addChildViewController:self.magicController];
    [self.contentView addSubview:self.magicController.view];
    [self.magicController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.contentView.mas_top).offset(kSizeScale(12));
        make.height.mas_equalTo(kScreenH-(kBottom(50)+kNavigationH+kSizeScale(12))).priorityHigh();
        // 适配8.0 将下面语句更换为height 设置。
        //make.bottom.equalTo(self.contentView.mas_bottom).offset(-(kBottom(50)+kNavigationH));
    }];
    [self.magicController.magicView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollTopEvent:) name:CourseDetailScrollTopNotification object:nil];
}

#pragma mark - Private
- (void)scrollTopEvent:(NSNotification *)notification {
    [self.magicController.magicView reloadDataToPage:1];
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    NSMutableArray *titleList = [NSMutableArray array];
    for (NSString *title in self.menuList) {
        [titleList addObject:title];
    }
    return titleList;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [menuItem setTitleColor:XHWhiteColor forState:UIControlStateSelected];
        [menuItem setBackgroundImage:[UIImage imageWithColor:XHWhiteColor] forState:UIControlStateNormal];
        [menuItem setBackgroundImage:[UIImage imageWithColor:XHBlackColor] forState:UIControlStateSelected];
        menuItem.titleLabel.font = midFont(13);
    }
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    if (pageIndex == 0) {
        YY_GoodDetailDetailViewController *detailCtrl = [[YY_GoodDetailDetailViewController alloc] init];
        if (kValidArray(self.goodModel.detailImages)) {
//            [self.goodModel.detailImages removeFirstObject];
            detailCtrl.parameter = @{@"detailImages":self.goodModel.detailImages,
                                     @"isHideSource":@(NO)};
        }
        return detailCtrl;
    }
    else if (pageIndex == 2) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (kValidString(self.goodModel.notesLink)) {
            [params setObject:self.goodModel.notesLink forKey:@"url"];
        }
        YY_GoodDetailNoticeViewController *wekCtrl = [[YY_GoodDetailNoticeViewController alloc] init];
        wekCtrl.parameter = params;
        return wekCtrl;
    }
    else {
        
        if ([self.goodModel.itemType integerValue] == 3) {
            YY_GoodCourceDetailViewController *courceVc = [[YY_GoodCourceDetailViewController alloc] init];
            if (self.goodModel.comId != nil) {
                courceVc.parameter = @{@"comID":self.goodModel.comId};
            }
            return courceVc;
        }else{
            YY_GoodDetailArgumentViewController *argumentCtrl = [[YY_GoodDetailArgumentViewController alloc] init];
            argumentCtrl.argumentArrs = self.goodModel.propsInfo;
            return argumentCtrl;
        }
    }
}
@end
