//
//  GoodDetailServiceCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "GoodDetailServiceCell.h"
@interface GoodDetailServiceCell()

@end

@implementation GoodDetailServiceCell

-(void)createUI{
    
    self.contentView.backgroundColor = XHWhiteColor;
    
    self.iconImageView = [YYCreateTools createImageView:@"" viewModel:-1];
    [self.contentView addSubview:self.iconImageView];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(2);
        make.left.mas_equalTo(16);
        make.size.mas_equalTo(CGSizeMake(16, 16));
    }];
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(12);
        make.top.mas_equalTo(0);
    }];
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHBlackLitColor];
    self.detailLabel.numberOfLines = 0;
    [self.contentView addSubview:self.detailLabel];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconImageView.mas_right).mas_offset(12);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(5);
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(-10);
    }];
    
}



@end
