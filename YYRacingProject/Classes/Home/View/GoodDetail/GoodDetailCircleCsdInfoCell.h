//
//  GoodDetailCircleCsdInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailCircleCsdInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) id circleModel;
@property (nonatomic, copy) void (^goodDetailCsdCircleBlock)(void);

@end

NS_ASSUME_NONNULL_END
