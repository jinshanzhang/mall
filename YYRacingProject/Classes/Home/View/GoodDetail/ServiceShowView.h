//
//  ServiceShowView.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/23.
//  Copyright © 2019 cjm. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "YYBaseViewShareView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ServiceShowView : YYBaseViewShareView

- (instancetype)initServicetView;

@property (nonatomic, strong) UITableView *subTableView;
@property (nonatomic, strong) UIButton    *clickBtn;
@property (nonatomic, strong) UILabel     *title;

@property (nonatomic, strong) NSMutableDictionary *dataDic;


@end

NS_ASSUME_NONNULL_END
