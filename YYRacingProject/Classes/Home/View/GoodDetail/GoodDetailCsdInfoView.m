//
//  GoodDetailCsdInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/25.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "GoodDetailCsdInfoView.h"

#import "YYCircleCell.h"
#import "YYAskEmptyView.h"
#import "YYBaseFootTableViewCell.h"

#import "HomeBannerInfoModel.h"
#import "YYCircleCommonEventModel.h"
#import "GoodDetailSubHeaderView.h"
#import "FriendCircleListInfoModel.h"
#import "FriendCircleCommonInfoModel.h"

#import "FriendCircleListInfoRequestAPI.h"

@interface GoodDetailCsdInfoView ()
<
UITableViewDelegate,
YYCircleCellDelegate,
UITableViewDataSource
>
@property (nonatomic, strong) UITableView *m_tableView;
@property (nonatomic, strong) UIButton    *scrollTopBtn;   //滚动到顶部按钮
@property (nonatomic, strong) GoodDetailSubHeaderView *headerView;

@property (nonatomic, strong) NSMutableArray  *layoutList; //布局数组
@property (nonatomic, strong) NSMutableArray  *listData;   //数据源
@property (nonatomic, strong) NSString        *itemId;     //商品ID
@property (nonatomic, assign) BOOL            hasMore;     //是否有更多
@property (nonatomic, assign) BOOL            isCanClick;  //是否可点击
@property (nonatomic, strong) NSString        *pageCursor; //分页游标
@property (nonatomic, strong) YYAskEmptyView  *emptyView;  //空白页

@end

@implementation GoodDetailCsdInfoView

#pragma mark - Setter
- (GoodDetailSubHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [[GoodDetailSubHeaderView alloc] initWithFrame:CGRectZero];
    }
    return _headerView;
}

- (UITableView *)m_tableView {
    if (!_m_tableView) {
        _m_tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _m_tableView.allowsSelection = NO;
        _m_tableView.scrollsToTop = YES;
        _m_tableView.delegate = self;
        _m_tableView.dataSource = self;
        _m_tableView.mj_header = [YYRacingBeckRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullDownRefresh)];
        _m_tableView.mj_footer = [YYBeckRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(pullUpRefresh)];
        _m_tableView.mj_footer.hidden = YES;
        
        _m_tableView.backgroundColor = XHLightColor;
        if (@available(iOS 11, *)) {
            //目前如此
            _m_tableView.estimatedRowHeight = 0;
            _m_tableView.estimatedSectionHeaderHeight = 0;
            _m_tableView.estimatedSectionFooterHeight = 0;
        }
        _m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _m_tableView;
}

- (void)setParams:(NSMutableDictionary *)params {
    _params = params;
    if (kValidDictionary(params)) {
        if ([params objectForKey:@"itemId"]) {
            self.itemId = [params objectForKey:@"itemId"];
        }
        [self pullDownRefresh];
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.pageCursor = @"";
        self.listData = [NSMutableArray array];
        self.layoutList = [NSMutableArray array];

        self.userInteractionEnabled = YES;
        self.backgroundColor = HexRGB(0xffffff);
        
        Class cls = [YYCircleCell class];
        registerClass(self.m_tableView, cls);
        cls = [YYBaseFootTableViewCell class];
        registerClass(self.m_tableView, cls);

        self.emptyView = [YYAskEmptyView yyEmptyView:YYEmptyViewGoodDetailCsdNoDataType
                                               title:nil
                                              target:self
                                              action:@selector(askBtnClickEvent:)];
        
        self.scrollTopBtn = [YYCreateTools createBtnImage:@"list_scrolltop_icon"];
        [self.scrollTopBtn addTarget:self action:@selector(scrollTopEventOperator:) forControlEvents:UIControlEventTouchUpInside];
        self.scrollTopBtn.hidden = YES;
        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftEdgeGesture:)];
        
        [self addSubview:self.m_tableView];
        [self addSubview:self.scrollTopBtn];
        // 屏幕左侧边缘响应
        [self addGestureRecognizer:panGesture];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.scrollTopBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(39), kSizeScale(39)));
        make.bottom.mas_equalTo(-(kTabbarH));
        make.right.mas_equalTo(-kSizeScale(10));
    }];
}

#pragma mark - Private
/**素材圈请求**/
- (void)csdCircleListRequest:(BOOL)isPullDown {
    @weakify(self);
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if (self.itemId) {
        [dict setObject:self.itemId forKey:@"itemId"];
    }
    if (isPullDown) {
        self.pageCursor = @"";
        [self.listData removeAllObjects];
        [self.layoutList removeAllObjects];
    }
    [dict setObject:self.pageCursor forKey:@"cursor"];
    [kWholeConfig gainGoodCsdCircleListRequest:dict
                                       success:^(NSDictionary *responDict) {
                                           @strongify(self);
                                           if (kValidDictionary(responDict)) {
                                               NSArray *moments = [NSArray modelArrayWithClass:[YYCircleModel class] json:[responDict objectForKey:@"moments"]];
                                               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                   // 开启线程转化布局
                                                   for (int i = 0; i < moments.count; i ++) {
                                                       YYCircleLayout *layout = [[YYCircleLayout alloc] initWithCircleModel:[moments objectAtIndex:i]];
                                                       [self.layoutList addObject:layout];
                                                   }
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       [self.m_tableView reloadData];
                                                   });
                                               });
                                               NSMutableArray *items = [NSMutableArray array];
                                               if (kValidArray(moments)) {
                                                   items = [moments mutableCopy];
                                                   if (isPullDown) {
                                                       self.listData = [items mutableCopy];
                                                   }
                                                   else {
                                                       [self.listData addObjectsFromArray:items];
                                                   }
                                                   [items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                       NSMutableArray *favors = [NSMutableArray array];
                                                       YYCircleModel *itemModel = (YYCircleModel *)obj;
                                                       if (kValidArray(itemModel.favorUserItems)) {
                                                           favors = [itemModel.favorUserItems mutableCopy];
                                                           if (itemModel.favorUserItems.count > 21) {
                                                               // 点赞数量：当数据大于21时，截取得到21个元素
                                                               NSArray *subItems = [itemModel.favorUserItems subarrayWithRange:NSMakeRange(0, 21)];
                                                               YYFavorUserModel *lastModel = [subItems lastObject];
                                                               lastModel.isAddLayer = YES;
                                                               favors = [[subItems subarrayWithRange:NSMakeRange(0, subItems.count - 1)] mutableCopy];
                                                               [favors addObject:lastModel];
                                                               itemModel.favorUserItems = favors;
                                                           }
                                                       }
                                                   }];
                                               }
                                               self.pageCursor = [responDict objectForKey:@"cursor"];
                                           }
                                           
                                           NSMutableDictionary *couponDict = [YYCommonTools getAssignResourcesUseOfType:22];
                                           if (kValidDictionary(couponDict)) {
                                               CGFloat height = 0.0;
                                               HomeBannerInfoModel *model = [HomeBannerInfoModel modelWithJSON:couponDict];
                                               if (kValidString(model.imageUrl)) {
                                                   height = ([UIImage getImageSizeWithURL:model.imageUrl].height)/2.0;
                                               }
                                               self.headerView = [[GoodDetailSubHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, model.switchFlag?height:0.0)];
                                               [self.headerView setSubHeaderView:couponDict];
                                           }
                                           [self.m_tableView layoutIfNeeded];
                                           self.m_tableView.tableHeaderView = self.headerView;
                                           
                                           [self tableViewRefreshEndOperator:!isPullDown isHeaderEnd:isPullDown];
                                           if (!kValidArray(self.listData)) {
                                               self.m_tableView.ly_emptyView = self.emptyView;
                                           }
                                       }];
}

/**下拉刷新**/
- (void)pullDownRefresh {
    self.isCanClick = YES;
    self.emptyView.actionBtnBackGroundColor = XHRedColor;
    [self csdCircleListRequest:YES];
}

/**上拉加载更多**/
- (void)pullUpRefresh {
    [self csdCircleListRequest:NO];
}

/**刷新素材圈列表**/
- (void)tableViewRefreshEndOperator:(BOOL)footerEnd
                        isHeaderEnd:(BOOL)headerEnd {
    if (footerEnd) {
        [self.m_tableView.mj_footer endRefreshing];
    }
    if (headerEnd) {
        [self.m_tableView.mj_header endRefreshing];
    }
    if (self.hasMore) {
        self.m_tableView.mj_footer.hidden = NO;
    }
    else {
        self.m_tableView.mj_footer.hidden = YES;
    }
    [self.m_tableView reloadData];
}

#pragma mark - Public
- (void)showCsdView {
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self setShowOrHideCsdViewAnimateWithDuration:CsdShowDuration isShow:YES];
}

- (void)hideCsdView {
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self setShowOrHideCsdViewAnimateWithDuration:CsdShowDuration isShow:NO];
}

- (void)setShowOrHideCsdViewAnimateWithDuration:(CGFloat)duration
                                         isShow:(BOOL)isShow {
    if (isShow) {
        [UIView animateWithDuration:duration
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                                self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
                            } completion:^(BOOL finished) {
                                
                            }];
    }
    else {
        [UIView animateWithDuration:duration
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                                self.frame = CGRectMake(self.frame.size.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
                            } completion:^(BOOL finished) {
                                
                            }];
    }
    self.isOpen = isShow;
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count > 0 ? (self.hasMore != YES?self.listData.count + 1:self.listData.count): 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    NSInteger row = indexPath.row;
    Class currentCls = [YYBaseFootTableViewCell class];
    if (row == self.listData.count) {
        if (!cell) {
            cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        }
        [(YYBaseFootTableViewCell *)cell setFooterContent:@"我是有底线的"];
        return cell;
    }
    else {
        Class currentCls = [YYCircleCell class];
        YYCircleCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.delegate = self;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (kValidArray(self.layoutList) && indexPath.row < self.layoutList.count) {
        [(YYCircleCell *)cell setLayout:self.layoutList[indexPath.row]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (row == self.listData.count) {
        return 54.0;
    }
    else {
        if (kValidArray(self.layoutList) && indexPath.row < self.layoutList.count) {
            return ((YYCircleLayout *)self.layoutList[indexPath.row]).height;
        }
        else {
            return 0.0;
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY > kScreenH * 1.5) {
        self.scrollTopBtn.hidden = NO;
    }
    else {
        self.scrollTopBtn.hidden = YES;
    }
}

#pragma mark - YYCircleCellDelegate

- (void)cellDidClickZan:(YYCircleCell *)circleCell {
    [YYCircleCommonEventModel cellClickZanEvent:circleCell
                                      tableView:self.m_tableView
                                       listData:self.listData
                                        layouts:self.layoutList];
}


- (void)cellDidClickShare:(YYCircleCell *)circleCell {
    [YYCircleCommonEventModel cellClickShareEvent:circleCell
                                        tableView:self.m_tableView
                                         listData:self.listData
                                          layouts:self.layoutList resultBlock:^{
                                              [YYCircleCommonEventModel cellClickShareCountEvent:circleCell
                                                                    tableView:self.m_tableView
                                                                    listData:self.listData
                                                                    layouts:self.layoutList];
                                          }];
}


- (void)cellDidClickDownload:(YYCircleCell *)circleCell {
    [YYCircleCommonEventModel cellClickDownloadEvent:circleCell
                                           tableView:self.m_tableView
                                            listData:self.listData];
}

- (void)photoItemsView:(NSMutableArray *)allItems clickItemAtIndexPath:(NSIndexPath *)indexPath
        clickItemModel:(YYCircleModel *)circleModel {
    [YYCircleCommonEventModel cellClickPhotoZoomEvent:allItems
                                 clickItemAtIndexPath:indexPath
                                       clickItemModel:circleModel];
}

#pragma mark - Event
- (void)askBtnClickEvent:(UIButton *)sender {
    if (!self.isCanClick) {
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(self.itemId)) {
        [params setObject:self.itemId forKey:@"word"];
    }
    [kWholeConfig feedBackAboutCsdCircle:params
                                 success:^(NSDictionary *responDict) {
                                     if (kValidDictionary(responDict)) {
                                         [YYCommonTools showTipMessage:@"收到您的反馈，我们会尽快提供优质素材"];
                                         self.emptyView.actionBtnBackGroundColor = HexRGB(0xCCCCCC);
                                         self.isCanClick = NO;
                                     }
    }];
}

- (void)scrollTopEventOperator:(UIButton *)sender {
    [self.m_tableView setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (void)handleLeftEdgeGesture:(UIPanGestureRecognizer *)recognizer {
    CGFloat offsetX = [recognizer translationInView:self].x;
    if(recognizer.state == UIGestureRecognizerStateChanged)
    {
        if (self.frame.origin.x < 0) {
            return;
        }
        CGRect tempFrame = self.frame;
        tempFrame.origin.x = self.frame.origin.x + offsetX;
        if (tempFrame.origin.x < 0) {
            tempFrame.origin.x = 0.0;
        }
        self.frame = tempFrame;
        [recognizer setTranslation:CGPointZero inView:recognizer.view];
    }
    else if(recognizer.state == UIGestureRecognizerStateEnded ||recognizer.state == UIGestureRecognizerStateCancelled)
    {
        // 手势结束时，若进度大于0.5就完成pop动画，否则取消
        if(self.frame.origin.x > self.frame.size.width/2.0)
        {
            [self setShowOrHideCsdViewAnimateWithDuration:0.2 isShow:NO];
        }
        else
        {
            [self setShowOrHideCsdViewAnimateWithDuration:0.2 isShow:YES];
        }
        if (self.sideslipGestureBlock) {
            self.sideslipGestureBlock(self.isOpen);
        }
    }
}

@end
