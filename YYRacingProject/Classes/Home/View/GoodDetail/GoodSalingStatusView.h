//
//  GoodSalingStatusView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeGoodInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodSalingStatusView : UIView

@property (nonatomic, strong) HomeGoodInfoModel *goodModel;

@end

NS_ASSUME_NONNULL_END
