//
//  GoodDetailCircleCsdInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "GoodDetailCircleCsdInfoCell.h"
#import "YYanYYLabel.h"

@interface GoodDetailCircleCsdHeaderView : UIView
{
    UIView *lineView;
    UIView *markView;
    UILabel *tipLabel;
    UIImageView *arrowImage;
}
@property (nonatomic, strong) UILabel  *titleLabel;

@end


@implementation GoodDetailCircleCsdHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    markView = [YYCreateTools createView:XHClearColor];
    markView.layer.cornerRadius = kSizeScale(1.5);
    markView.layer.masksToBounds = YES;
    [self addSubview:markView];
    
    self.titleLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(14)
                                       textColor:XHBlackColor];
    self.titleLabel.numberOfLines = 1;
    [self addSubview:self.titleLabel];
    
    tipLabel = [YYCreateTools createLabel:@"查看更多素材"
                                     font:normalFont(12)
                                textColor:XHGaldLightColor];
    [self addSubview:tipLabel];
    
    arrowImage = [YYCreateTools createImageView:@"gald_arrow_icon"
                                      viewModel:-1];
    [self addSubview:arrowImage];
    
    lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:lineView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    markView.frame = CGRectMake(kSizeScale(0), 0, kSizeScale(0), kSizeScale(13));
    markView.centerY = self.centerY;
    
    self.titleLabel.frame = CGRectMake(markView.right+kSizeScale(14), 0, kSizeScale(100), kSizeScale(20));
    self.titleLabel.centerY = markView.centerY;
    
    arrowImage.frame = CGRectMake(kScreenW-(kSizeScale(12)+kSizeScale(6)), 0, kSizeScale(6), kSizeScale(10));
    arrowImage.centerY = self.titleLabel.centerY;
    
    tipLabel.frame = CGRectMake(kScreenW-(kSizeScale(75)+kSizeScale(20)), 0, kSizeScale(75), kSizeScale(20));
    tipLabel.centerY = arrowImage.centerY;
    
    lineView.frame = CGRectMake(kSizeScale(12), self.height-kSizeScale(1), self.width-(kSizeScale(12)*2), kSizeScale(1));
}

@end

@interface GoodDetailCircleCsdPictureInfoCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *coverImageView;
@property (nonatomic, strong) UIImageView *placeHolderImageView;

@end

@implementation GoodDetailCircleCsdPictureInfoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.coverImageView = [YYCreateTools createImageView:nil
                                                   viewModel:-1];
        
        self.coverImageView.clipsToBounds = YES;
        self.coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:self.coverImageView];
        [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        self.placeHolderImageView = [YYCreateTools createImageView:nil
                                                         viewModel:-1];
        
        self.placeHolderImageView.clipsToBounds = YES;
        self.placeHolderImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.coverImageView addSubview:self.placeHolderImageView];
        [self.placeHolderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(36, 36));
            make.center.equalTo(self.coverImageView);
        }];
    }
    return self;
}

@end

#import "FriendCircleListInfoModel.h"
@interface GoodDetailCircleCsdInfoCell ()
<
UICollectionViewDelegate,
UICollectionViewDataSource
>
@property (nonatomic, strong) GoodDetailCircleCsdHeaderView *headerView;
@property (nonatomic, strong) UIImageView *userHeader;
@property (nonatomic, strong) UILabel     *userName;
@property (nonatomic, strong) YYanYYLabel *publicContent;
@property (nonatomic, strong) UICollectionView *photoView;
@property (nonatomic, strong) UIView      *bottomView;

@property (nonatomic, strong) MASConstraint             *photoHeight;
@property (nonatomic, strong) NSMutableArray            *photoArrays;
@property (nonatomic, strong) FriendCircleListInfoModel *listModel;
@end

@implementation GoodDetailCircleCsdInfoCell

#pragma mark - Setter
- (UICollectionView *)photoView {
    if (!_photoView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumInteritemSpacing = 0.0f;
        flowLayout.itemSize = CGSizeMake(kSizeScale(90), kSizeScale(90));
        flowLayout.minimumLineSpacing = kSizeScale(5);
        flowLayout.sectionInset = UIEdgeInsetsZero;
        
        _photoView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                           collectionViewLayout:flowLayout];
        _photoView.delegate = self;
        _photoView.dataSource = self;
        _photoView.scrollsToTop = NO;
        _photoView.showsHorizontalScrollIndicator = NO;
        _photoView.backgroundColor = XHClearColor;
        
        Class cls = [GoodDetailCircleCsdPictureInfoCell class];
        [_photoView registerClass:cls forCellWithReuseIdentifier:strFromCls(cls)];
    }
    return _photoView;
}

- (void)setCircleModel:(id)circleModel {
    _circleModel = circleModel;
    self.photoArrays = [NSMutableArray array];
    if ([_circleModel isKindOfClass:[FriendCircleListInfoModel class]]) {
        self.listModel = (FriendCircleListInfoModel *)_circleModel;
        [self.userHeader yy_sdWebImage:self.listModel.avatarUrl
                  placeholderImageType:YYPlaceholderImageHomeBannerType];
        self.userName.text = self.listModel.nickname;
        self.publicContent.text = self.listModel.contents;
        
        [self.photoView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(kSizeScale(102));
        }];
        [self.listModel.imageList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            YYImageInfoModel *imageModel = (YYImageInfoModel *)obj;
            [self.photoArrays addObject:imageModel.imageUrl];
        }];
        [self.photoView reloadData];
    }
}

#pragma mark - Life cycle
- (void)createUI {
    // 顶部
    self.headerView = [[GoodDetailCircleCsdHeaderView alloc] initWithFrame:CGRectZero];
    self.headerView.titleLabel.text = @"发圈素材";
    [self.contentView addSubview:self.headerView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(44));
    }];
    
    // 头像
    self.userHeader = [YYCreateTools createImageView:nil
                                           viewModel:-1];
    [self.contentView addSubview:self.userHeader];
    
    [self.userHeader mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.equalTo(self.headerView.mas_bottom).offset(kSizeScale(15));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(40)));
    }];
    
    // 用户名
    self.userName = [YYCreateTools createLabel:nil
                                          font:boldFont(15)
                                     textColor:HexRGB(0x495C85)];
    [self.contentView addSubview:self.userName];
    
    [self.userName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userHeader);
        make.left.equalTo(self.userHeader.mas_right).offset(kSizeScale(10));
    }];

    // 文本内容
    self.publicContent = [YYanYYLabel new];
    self.publicContent.textColor = XHBlackColor;
    self.publicContent.font = midFont(14);
    self.publicContent.numberOfLines = 3;
    self.publicContent.displaysAsynchronously = YES;
    self.publicContent.preferredMaxLayoutWidth = kScreenW - kSizeScale(80);
    self.publicContent.textVerticalAlignment = YYTextVerticalAlignmentTop;
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 10.1f) {
        YYTextLinePositionSimpleModifier *modifiers = [YYTextLinePositionSimpleModifier new];
        modifiers.fixedLineHeight = kSizeScale(18);
        self.publicContent.linePositionModifier = modifiers;
    }
    
    [self.contentView addSubview:self.publicContent];
    
    [self.publicContent mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userName.mas_bottom).offset(kSizeScale(5));
        make.left.equalTo(self.userName);
        make.right.mas_equalTo(-kSizeScale(20));
    }];
    
    [self.contentView addSubview:self.photoView];
    
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(63));
        make.right.equalTo(self.contentView);
    make.top.equalTo(self.publicContent.mas_bottom).offset(kSizeScale(12)).priorityHigh();
        make.height.mas_equalTo(kSizeScale(0));
    }];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.photoView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(8));
        make.bottom.equalTo(self.contentView);
    }];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellClickGesture:)]];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photoArrays.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class current = [GoodDetailCircleCsdPictureInfoCell class];
    GoodDetailCircleCsdPictureInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
    if (row < self.photoArrays.count) {
        [cell.coverImageView sd_setImageWithURL:self.photoArrays[row] placeholderImage:[UIImage imageWithColor:HexRGB(0xf7f7f7)]];
        if (row < self.listModel.imageList.count) {
            YYImageInfoModel *infoModel = [self.listModel.imageList objectAtIndex:row];
            [cell.placeHolderImageView setImage:(infoModel.type == 4)? [UIImage imageNamed:@"csd_video_icon"] : [UIImage imageWithColor:XHClearColor]];
        }
    }
    return cell;
}

#pragma mark - Event
- (void)cellClickGesture:(UITapGestureRecognizer *)tapGesture {
    if (self.goodDetailCsdCircleBlock) {
        self.goodDetailCsdCircleBlock();
    }
}
@end
