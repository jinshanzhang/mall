//
//  GoodDetailDescriInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "HomeGoodInfoModel.h"

@interface GoodDetailDescriInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) HomeGoodInfoModel *goodModel;

@end
