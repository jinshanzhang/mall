//
//  GoodDetailSkuInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailSkuInfoCell.h"

#import "GoodDetailServiceInfoView.h"
@interface GoodDetailSkuInfoCell()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *topBackgroundView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UIView *bottomBackgroundView;

@property (nonatomic, strong) UILabel *skuTipLabel;
@property (nonatomic, strong) UILabel *selectTipLabel;
@property (nonatomic, strong) UIImageView *arrowImageView;

@property (nonatomic, strong) UIView  *lineView;

@property (nonatomic, strong) UILabel *serviceTipLabel;
@property (nonatomic, strong) GoodDetailServiceInfoView *serviceView; //详情&&服务介绍

@end

@implementation GoodDetailSkuInfoCell

- (void)createUI {
    self.topView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.topView];
    
    self.topBackgroundView = [YYCreateTools createView:XHWhiteColor];
    [self.topView addSubview:self.topBackgroundView];
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.lineView];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
//    [self.contentView addSubview:self.bottomView];
    
    self.bottomBackgroundView = [YYCreateTools createView:XHWhiteColor];
//    [self.bottomView addSubview:self.bottomBackgroundView];
    
    self.skuTipLabel = [YYCreateTools createLabel:@"选择规格"
                                             font:midFont(13)
                                        textColor:XHBlackColor];
    [self.topBackgroundView addSubview:self.skuTipLabel];
    
    self.selectTipLabel = [YYCreateTools createLabel:nil
                                                font:midFont(13)
                                           textColor:XHBlackColor];
    self.selectTipLabel.textAlignment = NSTextAlignmentLeft;
    [self.topBackgroundView addSubview:self.selectTipLabel];
    
    self.arrowImageView = [YYCreateTools createImageView:@"new_expand_more_icon"
                                               viewModel:-1];
    [self.topBackgroundView addSubview:self.arrowImageView];
    
    self.serviceTipLabel = [YYCreateTools createLabel:@"服务"
                                                 font:midFont(13)
                                            textColor:XHBlackLitColor];
//    [self.bottomBackgroundView addSubview:self.serviceTipLabel];
    
    self.serviceView = [[GoodDetailServiceInfoView alloc] initWithFrame:CGRectZero];
    [self.bottomBackgroundView addSubview:self.serviceView];
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(52));
        make.bottom.equalTo(self.contentView.mas_bottom).priorityHigh();
    }];
    
    [self.topBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(0));
        make.left.right.equalTo(self.topView);
        make.height.mas_equalTo(kSizeScale(44));
    }];
    
    [self.skuTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(13));
        make.centerY.equalTo(self.topBackgroundView);
        make.height.mas_equalTo(39);
    }];

    [self.selectTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.skuTipLabel);
        make.left.equalTo(self.skuTipLabel.mas_right).offset(kSizeScale(20));
        make.right.mas_equalTo(-kSizeScale(30));
    }];
    
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.skuTipLabel);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.skuTipLabel);
        make.right.mas_equalTo(-kSizeScale(13));
        make.top.equalTo(self.topView.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
//    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self.contentView);
//        make.top.equalTo(self.lineView.mas_bottom);
//        make.height.mas_equalTo(kSizeScale(52));
//        make.bottom.equalTo(self.contentView.mas_bottom).priorityHigh();
//    }];
//
//    [self.bottomBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.top.equalTo(self.bottomView);
//        make.height.mas_equalTo(kSizeScale(44));
//    }];
    
//    [self.serviceTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.skuTipLabel);
//        make.centerY.equalTo(self.bottomBackgroundView);
//        make.height.mas_equalTo(40);
//    }];
    
//    [self.serviceView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.serviceTipLabel.mas_right).offset(kSizeScale(18));
//        make.right.mas_equalTo(-kSizeScale(12));
//        make.centerY.equalTo(self.serviceTipLabel);
//        make.height.equalTo(self.serviceTipLabel);
//    }];
    [self.topBackgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickSelectSkuEvent:)]];
}


- (void)setServiceLists:(NSMutableArray *)serviceLists {
    _serviceLists = serviceLists;
    self.serviceView.serviceLists = _serviceLists;
}

- (void)setSelectTip:(NSString *)selectTip {
    _selectTip = selectTip;
    if (kValidString(_selectTip)) {
        //不为空
        self.skuTipLabel.text = @"已选";
        self.skuTipLabel.textColor = XHBlackLitColor;
        
        self.selectTipLabel.text = _selectTip;
        [self.skuTipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.centerY.equalTo(self.topBackgroundView);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(30), kSizeScale(39)));
        }];
        [self.selectTipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.skuTipLabel);
            make.left.equalTo(self.skuTipLabel.mas_right).offset(kSizeScale(15));
            make.right.mas_equalTo(-kSizeScale(30));
        }];
    }
    else {
        //为空
        self.skuTipLabel.text = @"选择规格";
        self.skuTipLabel.textColor = XHBlackColor;
        self.selectTipLabel.text = nil;
        [self.skuTipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.centerY.equalTo(self.topBackgroundView);
            make.height.mas_equalTo(kSizeScale(39));
        }];

        [self.selectTipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.skuTipLabel);
            make.left.equalTo(self.skuTipLabel.mas_right).offset(kSizeScale(20));
            make.right.mas_equalTo(-kSizeScale(30));
        }];
    }
}

#pragma mark - Event
- (void)clickSelectSkuEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.clickSelectSkuBlock) {
        self.clickSelectSkuBlock();
    }
}

@end
