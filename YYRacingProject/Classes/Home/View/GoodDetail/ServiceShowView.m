//
//  ServiceShowView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "ServiceShowView.h"
#import "GoodDetailServiceCell.h"


@interface ServiceShowView ()<UITableViewDataSource,UITableViewDelegate>


@end

@implementation ServiceShowView

- (instancetype)initServicetView {
    self = [super init];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        self.frame = CGRectMake(0, kScreenH, kScreenW, kSizeScale(480)+kSafeBottom);
        self.v_cornerRadius = 4 ;
        
        [self setUp];
        

    }
    return self;
}


-(void)setUp{
    @weakify(self);
    self.dataDic = [NSMutableDictionary dictionary];
    [self.dataDic setObject:@[@"service_keep",@"service_seven",@"service_late",@"service_slow",@"service_wrong"] forKey:@"icon"];
    [self.dataDic setObject:@[@"正品保障",@"7天无理由退换货",@"晚就赔",@"错就赔"] forKey:@"title"];
    //[self.dataDic setObject:@[@"正品保障",@"7天无理由退换货",@"晚就赔",@"慢就赔",@"错就赔"] forKey:@"title"];
    
    [self.dataDic setObject:@[@"商品由中华联合财产保险股份有限公司承担，产品质量保障，确保正品。",@"消费者在满足7天无理由退换货申请条件的前提下，可以提出“7天无理由退换货”的申请。",@"实际发货时间超过约定时间24小时或48小时(具体发货时间详见商详说明或咨询客服)，经核实无误后，即赔付1张5元无门槛优惠券。",@"收到的商品与下单商品不一致，提供相关证明且经核实为商家原因导致错发即可获得10元现金赔付（每单限赔1次)。"] forKey:@"detail"];
    //[self.dataDic setObject:@[@"商品由中华联合财产保险股份有限公司承担，产品质量保障，确保正品。",@"消费者在满足7天无理由退换货申请条件的前提下，可以提出“7天无理由退换货”的申请。",@"实际发货时间超过约定时间24小时或48小时(具体发货时间详见商详说明或咨询客服)，经核实无误后，即赔付1张5元无门槛优惠券。",@"经实物物流发生停滞，则每超过24小时赔付1张5元无门槛优惠券(以所属物流公司官网信息为准)。",@"收到的商品与下单商品不一致，提供相关证明且经核实为商家原因导致错发即可获得10元现金赔付（每单限赔1次)。"] forKey:@"detail"];

    self.title = [YYCreateTools createLabel:@"服务保障" font:boldFont(16) textColor:XHBlackColor];
    [self addSubview:self.title];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(15);
        make.centerX.equalTo(self);
    }];
    
    
    self.clickBtn =[YYCreateTools createBtn:@"我知道了" font:normalFont(17) textColor:XHWhiteColor];
    [self addSubview:self.clickBtn];
    self.clickBtn.backgroundColor = XHLoginColor;
    
    self.clickBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
//        if (self.callBack) {
//            self.callBack(self.CouponArr);
//        }
        
        [self showViewOfAnimateType:YYPopAnimateDownUp];
    };
    
    [self.clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kSafeBottom);
        make.height.mas_equalTo(kSizeScale(50));
    }];
    
    
    self.subTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 51, kScreenW, self.height-kBottom(kSizeScale(50))-50)];
    [self addSubview:_subTableView];
    self.subTableView.backgroundColor = XHWhiteColor;
    _subTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _subTableView.delegate = self;
    _subTableView.dataSource = self;
    _subTableView.estimatedRowHeight = 100;
    
    Class common = [GoodDetailServiceCell class];
    registerClass(self.subTableView, common);
    
}

#pragma mark - TableViewDatasouce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self.dataDic objectForKey:@"title"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class common = [GoodDetailServiceCell class];
    GoodDetailServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    cell.iconImageView.image = [UIImage imageNamed:[self.dataDic objectForKey:@"icon"][indexPath.row]];
    cell.titleLabel.text = [self.dataDic objectForKey:@"title"][indexPath.row];
    cell.detailLabel.text = [self.dataDic objectForKey:@"detail"][indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
