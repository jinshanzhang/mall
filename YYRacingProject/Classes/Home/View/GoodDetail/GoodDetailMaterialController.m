//
//  GoodDetailMaterialController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "GoodDetailMaterialController.h"
#import "GoodDetailCsdInfoView.h"

@interface GoodDetailMaterialController ()

@end

@implementation GoodDetailMaterialController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"素材圈"];
    [self xh_popTopRootViewController:NO];
    
   GoodDetailCsdInfoView  *csdView = [[GoodDetailCsdInfoView alloc] initWithFrame:CGRectMake(0, kNavigationH, kScreenW, kScreenH - kNavigationH)];
    
    csdView.params = [self.parameter mutableCopy];
    [self.view addSubview:csdView];
}

    // Do any additional setup after loading the view.



@end
