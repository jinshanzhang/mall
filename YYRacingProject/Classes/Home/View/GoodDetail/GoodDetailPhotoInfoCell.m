//
//  GoodDetailPhotoInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailPhotoInfoCell.h"

@interface GoodDetailPhotoInfoCell ()

@property (nonatomic, strong) YYAnimatedImageView *bgImageView;

@end

@implementation GoodDetailPhotoInfoCell

- (void)createUI {
    self.bgImageView = [[YYAnimatedImageView alloc] init];
    self.bgImageView.userInteractionEnabled = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.bgImageView];
    
    [self.bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    [self.bgImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoClickEvent:)]];
}

- (void)setImageURL:(NSString *)imageURL {
    _imageURL = imageURL;
    [self.bgImageView setImageWithURL:[NSURL URLWithString:_imageURL]
                          placeholder:[UIImage imageNamed:@"good_detail_placehold_icon"]];
    /*[self.bgImageView yy_sdWebImage:_imageURL
               placeholderImageType:YYPlaceholderImageGoodDetailType];*/
}

- (void)photoClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.photoClick) {
        self.photoClick(self);
    }
}
@end
