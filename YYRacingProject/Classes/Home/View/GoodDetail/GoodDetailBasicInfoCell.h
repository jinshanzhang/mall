//
//  GoodDetailBasicInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

#import "HomeGoodInfoModel.h"
#import "MyCourceModel.h"
@interface GoodDetailBasicInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) NSString       *goodNameStr;
@property (nonatomic, strong) NSNumber       *saleNumber;
@property (nonatomic, strong) NSNumber       *stockNumber;

@property (nonatomic, strong) HomeGoodInfoModel *detailModel;
@property (nonatomic, strong) MyCourceModel *courseModel;

@property (nonatomic, copy)  NSString        *sellCountTitle;
@property (nonatomic, copy)  NSString        *storageTitle;

@end
