//
//  GoodDetailMenuBarItem.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "GoodDetailMenuBarItem.h"
@interface GoodDetailMenuBarItem ()

@property (nonatomic, strong) UIView *bottomSlider;

@end

@implementation GoodDetailMenuBarItem

#pragma mark - Setter
- (UIView *)bottomSlider {
    if (!_bottomSlider) {
        _bottomSlider = [YYCreateTools createView:XHBlueDeepColor];
    }
    return _bottomSlider;
}


- (void)setItemSelect:(BOOL)itemSelect {
    if (itemSelect) {
        self.bottomSlider.alpha = 1.0;
        self.titleLabel.font = boldFont(17);
        self.titleLabel.textColor = XHBlueDeepColor;
    }
    else {
        self.bottomSlider.alpha = 0.0;
        self.titleLabel.font = normalFont(14);
        self.titleLabel.textColor = XHBlackColor;
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemSelect = NO;
        self.userInteractionEnabled = YES;
        self.titleLabel = [[UILabel alloc] initWithFrame:self.bounds];
        self.titleLabel.font = normalFont(14);
        self.titleLabel.textColor = XHBlackColor;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        self.bottomSlider.v_cornerRadius = kSizeScale(2);
        [self addSubview:self.bottomSlider];
        
        [self.bottomSlider mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.mas_equalTo(kSizeScale(37));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(42), kSizeScale(3)));
        }];
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(self);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(30)));
        }];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self _setSubLayouts];
}

#pragma mark - Private
- (void)_setSubLayouts {
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).offset(-kSizeScale(8));
    }];
}

@end
