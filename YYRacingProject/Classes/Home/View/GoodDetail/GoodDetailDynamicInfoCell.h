//
//  GoodDetailDynamicInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "HomeGoodInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailDynamicInfoCell : YYBaseTableViewCell

@property (nonatomic, copy) void (^couponBlock)(void);
@property (nonatomic, copy) void (^serviceBlock)(void);


- (void)setDynamicCell:(NSDictionary *)couponInfo
              activity:(NSDictionary *)activityInfo
              oldactivity:(id)oldactivityModel;

@property (nonatomic, strong) NSMutableArray *serviceLists;

@end

NS_ASSUME_NONNULL_END
