//
//  GoodDetailCsdInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/25.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CsdShowDuration 0.5

NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailCsdInfoView : UIView

@property (nonatomic, strong) NSMutableDictionary *params;
@property (nonatomic, assign) BOOL isOpen;

@property (nonatomic, copy) void (^sideslipGestureBlock)(BOOL isOpen);

- (void)showCsdView;
- (void)hideCsdView;

@end

NS_ASSUME_NONNULL_END
