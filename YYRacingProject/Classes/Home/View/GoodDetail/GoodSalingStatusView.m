//
//  GoodSalingStatusView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodSalingStatusView.h"
#import "yyCountDownManager.h"
@interface GoodSalingStatusView()
{
    CGFloat  passTime;
}
@property (nonatomic, strong) UIImageView *priceBgView;
@property (nonatomic, strong) UIImageView *timeBgView;

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *commissionLabel;
@property (nonatomic, strong) UILabel *statusLabel;

@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, assign) NSInteger diffDay;  //差的天数
@property (nonatomic, assign) NSTimeInterval   downTime;

@property (nonatomic, assign) BOOL isPreHot;
@property (nonatomic, assign) BOOL isSuperGood; //是否超级爆款商品
@property (nonatomic, strong) NSMutableArray *sliceItems;
@property (nonatomic, strong) NSMutableArray *evaluationItems;
@property (nonatomic, strong) NSMutableArray *countDownItems;

@end

@implementation GoodSalingStatusView

#pragma mark - Setter method
- (void)setGoodModel:(HomeGoodInfoModel *)goodModel {
    _goodModel = goodModel;

    UIImage *priceBgImage = [UIImage imageWithColor:XHWhiteColor];
    UIImage *timeBgImage = priceBgImage;
    
    self.statusLabel.text = goodModel.volumeText;
     if (_goodModel.salingType == YYGoodSalingStatusNoraml) {
        priceBgImage = [UIImage imageWithColor:XHWhiteColor];
        timeBgImage = priceBgImage;
    }
    else {
        if (kValidArray(_goodModel.salesImage)) {
            YYImageInfoModel *model = [_goodModel.salesImage firstObject];
            priceBgImage = [UIImage getImageFromURL:model.imageUrl];
            if (_goodModel.salesImage.count > 1) {
                YYImageInfoModel *model = [_goodModel.salesImage lastObject];
                timeBgImage = [UIImage getImageFromURL:model.imageUrl];
            }
        }
    }
    self.timeBgView.image = timeBgImage;
    self.priceBgView.image = priceBgImage;
    
    self.isSuperGood = _goodModel.isSuperTrendy;
    self.isPreHot = (_goodModel.salingType == YYGoodSalingStatusPreHot && _goodModel.salingType != YYGoodSalingStatusNoraml) ? YES : NO;
    
    UIColor *priceAndSaleColor = (self.isPreHot ? XHWhiteColor : (self.isSuperGood ? XHBlackColor : XHWhiteColor));
    UIColor *comissionColor = (self.isPreHot ? XHWhiteColor : (self.isSuperGood ? XHBlackColor : XHGaldMidColor));

    if (_goodModel.isRangeFlag == 0) {
        // 非区间
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(10));
            make.centerY.equalTo(self);
        }];

        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.priceLabel.mas_right).offset(kSizeScale(10));
            make.bottom.equalTo(self).offset(-kSizeScale(8));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(18)));
        }];
        
        [self.commissionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.priceLabel.mas_right).offset(kSizeScale(10));
            make.top.mas_equalTo(kSizeScale(10));
        }];
        
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(18) symbolTextColor:priceAndSaleColor wordSpace:2 price:kValidString(_goodModel.nowPrice)?_goodModel.nowPrice:@"0" priceFont:boldFont(32) priceTextColor: priceAndSaleColor symbolOffsetY:0];
        self.priceLabel.attributedText = attribut;
        
        if (kIsFan) {
            NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",kValidString(_goodModel.originPrice)?_goodModel.originPrice:@"0"]];
            [origin yy_setAttributes:comissionColor
                                font:midFont(13)
                             content:origin
                           alignment:NSTextAlignmentCenter];
            [origin yy_setAttributeOfDeleteLine:NSMakeRange(0, origin.length)];
            self.commissionLabel.attributedText = origin;
        }
        else {
            NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(12) symbolTextColor:comissionColor wordSpace:2 price:kValidString(_goodModel.commissionPrice)?_goodModel.commissionPrice:@"0" priceFont:boldFont(14) priceTextColor:comissionColor symbolOffsetY:1.2];
            self.commissionLabel.attributedText = extra;
        }
    }
    else {
        // 是区间
        [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(10));
            make.top.equalTo(self).offset(kSizeScale(5));
        }];
        
        [self.commissionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.priceLabel);
            make.bottom.equalTo(self).offset(-kSizeScale(5));
        }];
        
        [self.statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.commissionLabel.mas_right).offset(kSizeScale(8));
            make.centerY.equalTo(self.commissionLabel);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(18)));
        }];
        
        if (kIsFan) {
            NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",kValidString(_goodModel.originPrice)?_goodModel.originPrice:@"0"]];
            [origin yy_setAttributes:comissionColor
                                font:midFont(13)
                             content:origin
                           alignment:NSTextAlignmentCenter];
            [origin yy_setAttributeOfDeleteLine:NSMakeRange(0, origin.length)];
            self.commissionLabel.attributedText = origin;
        }
        else {
            NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(12) symbolTextColor:comissionColor wordSpace:2 price:kValidString(_goodModel.commissionPrice)?_goodModel.commissionPrice:@"0" priceFont:boldFont(14) priceTextColor:comissionColor symbolOffsetY:1.2];
            self.commissionLabel.attributedText = extra;
        }
        
        NSMutableAttributedString *attribut = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",kValidString(_goodModel.nowPrice)?_goodModel.nowPrice:@"0"]];
        [attribut yy_setAttributes:priceAndSaleColor
                              font:boldFont(20) content:attribut
                         alignment:NSTextAlignmentLeft];
        self.priceLabel.attributedText = attribut;
    }
    self.statusLabel.textColor = priceAndSaleColor;
    self.leftLabel.textColor = comissionColor;
    
    self.downTime = 0;
    if (self.isPreHot) {
        // 预热
        self.leftLabel.text = [NSString stringWithFormat:@"%@开抢",_goodModel.preTimeFormat];
    }
    else {
        if (_goodModel.salingType == YYGoodSalingStatusSpecial) {
            // 热卖
            NSString *showContent = @"距结束仅剩";
            NSDate *currentDate = [NSDate dateWithTimeIntervalSince1970:(_goodModel.currentTime/1000)];
            NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:(_goodModel.endTime/1000)];
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [calendar components:NSCalendarUnitDay
                                                       fromDate:currentDate
                                                         toDate:endDate
                                                        options:0];
            self.diffDay = (int)labs(components.day);
            if (self.diffDay >= 3) {
                showContent = [NSString stringWithFormat:@"距结束仅剩%ld天",self.diffDay];
            }
            self.leftLabel.text = showContent;
        }
    }
    [self handlerCountDownStyle];
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.diffDay = -1;
        // 增加倒计时通知
        self.sliceItems = [NSMutableArray array];
        self.countDownItems = [NSMutableArray array];
        self.evaluationItems = [NSMutableArray array];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    // 背景视图
    self.priceBgView = [YYCreateTools createImageView:nil
                                            viewModel:-1];
    [self addSubview:self.priceBgView];
    
    self.timeBgView = [YYCreateTools createImageView:nil
                                            viewModel:-1];
    [self addSubview:self.timeBgView];
    
    // 价格视图上view
    self.priceLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(30)
                                       textColor:XHWhiteColor];
    [self.priceBgView addSubview:self.priceLabel];
    
    self.commissionLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(14)
                                       textColor:XHWhiteColor];
    [self.priceBgView addSubview:self.commissionLabel];
    
    self.statusLabel = [YYCreateTools createLabel:@""
                                            font:midFont(10)
                                       textColor:XHWhiteColor];
    [self.priceBgView addSubview:self.statusLabel];

    
    self.leftLabel = [YYCreateTools createLabel:nil
                                           font:midFont(11)
                                      textColor:XHGaldMidColor];
    self.leftLabel.textAlignment = NSTextAlignmentRight;
    [self.timeBgView addSubview:self.leftLabel];
    
    for (int i = 0; i < 7; i ++) {
        BOOL isEven = ((i % 2 == 0)?YES:NO);
        UILabel *lbl = [YYCreateTools createLabel:nil
                                             font:midFont(10)
                                        textColor:XHWhiteColor];
        lbl.layer.cornerRadius = 2;
        lbl.clipsToBounds = YES;
        if (!isEven) {
            if (i < 5) {
                lbl.text = @":";
            }
            else {
                lbl.text = @".";
            }
            [self.sliceItems addObject:lbl];
        }
        else {
            [self.evaluationItems addObject:lbl];
        }
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.textColor = (!isEven?XHGaldMidColor:XHWhiteColor);
        lbl.backgroundColor = (isEven?XHGaldMidColor:XHClearColor);
        [self.timeBgView addSubview:lbl];
        [self.countDownItems addObject:lbl];
    }
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.timeBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self);
        make.width.mas_equalTo(kSizeScale(125));
    }];
    
    [self.priceBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self);
        make.right.equalTo(self.timeBgView.mas_left);
    }];
  
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.timeBgView);
        make.top.mas_equalTo(kSizeScale(8));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self countDownItemViewLayout:NO];
}


- (void)willMoveToWindow:(UIWindow*)newWindow {
    if (newWindow == nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:YYCountDownNotification object:nil];
    }
}

//添加通知方法'
- (void)didMoveToWindow {
    if (self.window) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:YYCountDownNotification object:nil];
    }
}

#pragma mark - Private method
// 处理倒计时处样式
- (void)handlerCountDownStyle {
    
    UIColor *backgColor = self.isPreHot ? XHWhiteColor : (self.isSuperGood ? XHBlackColor : XHGaldMidColor);
    UIColor *textColor = self.isPreHot ? HexRGB(0x06A144) : (self.isSuperGood ? XHWhiteColor : HexRGB(0x1C1C49));
    UIColor *sliceColor = self.isPreHot ? XHWhiteColor : (self.isSuperGood ? XHBlackColor : XHGaldMidColor);
    
    [self.evaluationItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UILabel *itemLbl = (UILabel *)obj;
        itemLbl.backgroundColor = backgColor;
        itemLbl.textColor = textColor;
    }];
    
    [self.sliceItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UILabel *itemLbl = (UILabel *)obj;
        itemLbl.textColor = sliceColor;
    }];
}

#pragma mark - Event method
// 倒计时
- (void)countDownNotification {
    double now = self.goodModel.currentTime;
    double end = (self.goodModel.salingType == YYGoodSalingStatusPreHot?self.goodModel.startTime:(self.diffDay>=3?self.goodModel.endTime-self.diffDay*24*3600*1000:self.goodModel.endTime));
    CGFloat timeInterval = [kConfigCountDown timeIntervalWithIdentifier:@"detailCtrl"];
    double leftTime = end - now;
    double differTime = leftTime - timeInterval;
    //NSLog(@"[differTime = %f]", differTime);
    
    if (differTime <=0) {
        differTime = 0;
    }

    NSInteger currentHour = (NSInteger)((differTime)/1000/60/60);
    NSInteger currentMinute = (NSInteger)((differTime)/1000/60)%60;
    NSInteger currentSeconds = ((NSInteger)(differTime))/1000%60;
    CGFloat   currentMsec = ((NSInteger)((differTime)))%1000/100;
    //小时数
    NSString *hours = [NSString stringWithFormat:@"%@", (currentHour < 10 ?[NSString stringWithFormat:@"0%ld",currentHour]:@(currentHour))];
    //分钟数
    NSString *minute = [NSString stringWithFormat:@"%@", (currentMinute < 10 ?[NSString stringWithFormat:@"0%ld",currentMinute]:@(currentMinute))];
    //秒数
    NSString *second = [NSString stringWithFormat:@"%@", (currentSeconds < 10 ?[NSString stringWithFormat:@"0%ld",currentSeconds]:@(currentSeconds))];
    //毫秒数
    NSString *msec = [NSString stringWithFormat:@"%.0f", currentMsec];
    for (int i = 0; i < self.evaluationItems.count; i ++) {
            UILabel *lbl = [self.evaluationItems objectAtIndex:i];
            if (i == 0) {
                lbl.text = hours;
            }
            else if (i == 1) {
                lbl.text = minute;
            }
            else if (i == 2) {
                lbl.text = second;
            }
            else if (i == 3) {
                lbl.text = msec;
        }
    }
}

#pragma mark - Private method
- (void)countDownItemViewLayout:(BOOL)isCenter {
    UILabel *tempLbl = nil;
    for (NSInteger i = self.countDownItems.count - 1; i >= 0 ; i --) {
        UILabel *subLbl = self.countDownItems[i];
        BOOL isEven = ((i % 2 == 0)?YES:NO);
        if (tempLbl) {
            [subLbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(tempLbl.mas_left);
                make.size.mas_equalTo(isEven? CGSizeMake(kSizeScale(16),kSizeScale(16)): CGSizeMake(kSizeScale(6), kSizeScale(18)));
                make.bottom.equalTo(tempLbl);
            }];
        }
        else {
            [subLbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(subLbl.superview.mas_right).offset(-kSizeScale(12));
                make.size.mas_equalTo(CGSizeMake(kSizeScale(16), kSizeScale(16)));
                make.bottom.equalTo(subLbl.superview.mas_bottom).offset(-kSizeScale(9));
            }];
        }
        tempLbl = subLbl;
    }
}

@end
