//
//  GoodDetailNoticeInfoCell.m
//  YYRacingProject
//


#import "GoodDetailNoticeInfoCell.h"

@interface GoodDetailNoticeInfoCell()

@property (nonatomic, strong) UIImageView *backgroundImageView;

@end

@implementation GoodDetailNoticeInfoCell

- (void)createUI {
    self.backgroundImageView = [YYCreateTools createImageView:@"good_detail_notice_icon"
                                                    viewModel:1];
    [self.contentView addSubview:self.backgroundImageView];
    
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

@end
