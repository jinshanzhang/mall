//
//  GoodDetailBasicInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailBasicInfoCell.h"

#import "HomeGoodInfoModel.h"
#import "YYBannerScrollView.h"
#import "GoodSalingStatusView.h"
@interface GoodDetailBasicInfoCell()
<YYBannerScrollViewDelegate>

@property (nonatomic, strong) YYLabel *goodNameLabel;
@property (nonatomic, strong) UIView  *goodNameLine;

@property (nonatomic, strong) UILabel *goodPriceLabel;
@property (nonatomic, strong) UILabel *extraLabel;
@property (nonatomic, strong) UILabel *saleNumLabel;
@property (nonatomic, strong) UILabel *stockNumLabel;

@property (nonatomic, strong) MASConstraint *salingHeight;

@property (nonatomic, strong) GoodSalingStatusView *salingView;
@property (nonatomic, strong) YYBannerScrollView *yyScrollView;
@property (nonatomic, strong) NSMutableArray *photoUrls;

@end

@implementation GoodDetailBasicInfoCell

- (void)createUI {
    self.contentView.backgroundColor = XHWhiteColor;
    self.salingView = [[GoodSalingStatusView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.salingView];
    
    self.goodNameLabel = [YYCreateTools createLabel:nil
                                               font:boldFont(15)
                                          textColor:XHBlackColor
                                           maxWidth:(kScreenW - kSizeScale(12) - kSizeScale(22))
                                      fixLineHeight:kSizeScale(21)];
    self.goodNameLabel.numberOfLines = 2;
    self.goodNameLabel.lineBreakMode = NSLineBreakByCharWrapping;
    [self.contentView addSubview:self.goodNameLabel];
    [self.goodNameLabel addGestureRecognizer:[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressCopyEvent:)]];
    
    self.goodNameLine = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.goodNameLine];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(18)
                                           textColor:XHBlackColor];
    [self.contentView addSubview:self.goodPriceLabel];
    
    self.extraLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(18)
                                       textColor:XHRedColor];
    [self.contentView addSubview:self.extraLabel];
    
    /*self.saleNumLabel = [YYCreateTools createLabel:nil
                                              font:normalFont(12)
                                         textColor:XHBlackLitColor];
    [self.contentView addSubview:self.saleNumLabel];
    
    self.stockNumLabel = [YYCreateTools createLabel:nil
                                              font:normalFont(12)
                                         textColor:XHBlackLitColor];
    [self.contentView addSubview:self.stockNumLabel];
    */
    [self.salingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        self.salingHeight = make.height.mas_equalTo(kSizeScale(50)).priorityHigh();
    }];
    [self.salingHeight uninstall];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.salingView.mas_bottom).offset(kSizeScale(12));
    }];
    
    [self.extraLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(12));
        make.centerY.equalTo(self.goodPriceLabel);
    }];
    
    [self.goodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.goodPriceLabel.mas_bottom);
        make.right.mas_equalTo(-kSizeScale(18));
    }];
    [self.goodNameLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(13));
        make.height.mas_equalTo(kSizeScale(8));
        make.bottom.equalTo(self.contentView.mas_bottom).priorityHigh();
    }];
    
//    [self.saleNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.goodNameLabel);
//        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(5));
//    }];

//    [self.stockNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-kSizeScale(12));
//        make.centerY.equalTo(self.saleNumLabel);
//    }];
}

#pragma mark - Setter
- (void)setGoodNameStr:(NSString *)goodNameStr {
    _goodNameStr = goodNameStr;
    NSMutableAttributedString *contentAttribute = [[NSMutableAttributedString alloc] init];
    if (kValidArray(self.detailModel.tags)) {
        for (int i = 0; i < self.detailModel.tags.count; i ++) {
            NSString *tagUrlStr = [self.detailModel.tags objectAtIndex:i];
            UIImage *image = [UIImage getImageFromURL:tagUrlStr];
            NSMutableAttributedString *postAttribute = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeScaleAspectFit attachmentSize:CGSizeMake(image.size.width/2.0, image.size.height/2.0) alignToFont:normalFont(12.5) alignment:YYTextVerticalAlignmentCenter];
            [contentAttribute appendAttributedString:postAttribute];
            if (i == self.detailModel.tags.count - 1) {
                NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:_goodNameStr];
                [contentStr yy_setAttributes:XHBlackColor
                                        font:boldFont(15)
                                   wordSpace:@(0)
                                     content:contentStr
                                   alignment:NSTextAlignmentLeft];
                [contentAttribute appendAttributedString:contentStr];
            }
        }
    }
    else {
        NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc] initWithString:_goodNameStr];
        [contentStr yy_setAttributes:XHBlackColor
                                font:boldFont(15)
                             content:contentStr
                           alignment:NSTextAlignmentLeft];
        [contentAttribute appendAttributedString:contentStr];
    }
    self.goodNameLabel.attributedText = contentAttribute;
}

- (void)setSaleNumber:(NSNumber *)saleNumber {
    _saleNumber = saleNumber;
    NSString *content = [NSString stringWithFormat:@"%@", _saleNumber];
    self.saleNumLabel.text = [NSString stringWithFormat:@"%@人已买",[content handlerMiriadeOverChar:content]];
}

-(void)setSellCountTitle:(NSString *)sellCountTitle{
    _sellCountTitle = sellCountTitle;
    self.saleNumLabel.text = sellCountTitle;
}

-(void)setStorageTitle:(NSString *)storageTitle{
    _storageTitle = storageTitle;
    self.stockNumLabel.text = storageTitle;
}

- (void)setStockNumber:(NSNumber *)stockNumber {
    _stockNumber = stockNumber;
    NSString *content = [NSString stringWithFormat:@"%@", _stockNumber];
    self.stockNumLabel.text = [NSString stringWithFormat:@"库存剩%@件", [content handlerMiriadeOverChar:content]];
}

-(void)setCourseModel:(MyCourceModel *)courseModel{
    _courseModel = courseModel;
    if (_courseModel) {
        self.goodNameLabel.text = courseModel.title;
        self.stockNumLabel.text = courseModel.lessonCountTitle;
    }
}

- (void)setDetailModel:(HomeGoodInfoModel *)detailModel {
    _detailModel = detailModel;
    if (_detailModel) {
        __block NSInteger isRangeFlag = 0;
        __block NSString *commissionPrice = nil;
        __block NSString *nowPrice = nil, *originPrice = nil;
       
        nowPrice = _detailModel.skuDefaultPrice;
        originPrice = _detailModel.skuDefaultOriginPirce;
        commissionPrice = _detailModel.skuCommissionPrice;
        
        
        [_detailModel.priceInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPriceInfoModel *priceModel = (HomeGoodPriceInfoModel *)obj;
            if (self.detailModel.salingType != YYGoodSalingStatusNoraml) {
                if (priceModel.priceType == 3) {
                    isRangeFlag = priceModel.isRangeFlag;
                }
            }
        }];
        
        _detailModel.nowPrice = nowPrice;
        _detailModel.originPrice = originPrice;
        _detailModel.commissionPrice = commissionPrice;
        _detailModel.isRangeFlag = isRangeFlag;
        
        if (_detailModel.salingType == YYGoodSalingStatusNoraml) {
            [self.salingHeight uninstall];
            self.salingView.hidden = YES;
            if (kIsFan) {
                // 现在价格
                NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(18) symbolTextColor:XHRedColor wordSpace:2 price:kValidString(nowPrice)?nowPrice:@"0" priceFont:boldFont(18) priceTextColor: XHRedColor symbolOffsetY:0];
                self.goodPriceLabel.attributedText = attribut;
                //原价
                NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",kValidString(originPrice)?originPrice:@"0"]];
                [origin yy_setAttributes:XHBlackLitColor
                                    font:midFont(13)
                                 content:origin
                               alignment:NSTextAlignmentCenter];
                [origin yy_setAttributeOfDeleteLine:NSMakeRange(0,origin.length)];
                self.extraLabel.attributedText = origin;
            }
            else {
                NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(18) symbolTextColor:XHBlackColor wordSpace:2 price:kValidString(nowPrice)?nowPrice:@"0" priceFont:boldFont(18) priceTextColor: XHBlackColor symbolOffsetY:0];
                self.goodPriceLabel.attributedText = attribut;
                
                NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(14) symbolTextColor:XHRedColor wordSpace:2 price:kValidString(commissionPrice)?commissionPrice:@"0" priceFont:boldFont(18) priceTextColor:XHRedColor symbolOffsetY:1.2];
                self.extraLabel.attributedText = extra;
            }
            [self.saleNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(-12);
                make.centerY.equalTo(self.goodPriceLabel);
            }];
            self.saleNumLabel.text = detailModel.volumeText;
        }
        else {
            [self.salingHeight install];
            self.salingView.hidden = NO;
        }
        self.salingView.goodModel = _detailModel;
    }
}
#pragma mark - Event
- (void)longPressCopyEvent:(UIGestureRecognizer *)gesture {
    [YYCommonTools pasteboardCopy:self.goodNameStr
                     appendParams:@{@"title":@"复制成功"}];
}

@end
