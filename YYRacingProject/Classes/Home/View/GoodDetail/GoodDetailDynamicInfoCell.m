//
//  GoodDetailDynamicInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailDynamicInfoCell.h"
#import "HomeBannerInfoModel.h"
#import "CouponListInfoModel.h"
#import "GoodDetailServiceInfoView.h"
#import "GoodDetailActivityInfoView.h"

@interface CouponStyleView : UIView

@property (nonatomic, strong) UIImageView *couponBgView;
@property (nonatomic, strong) UILabel     *couponTitleLbl;
@property (nonatomic, copy)   NSString    *couponContent;

@end

@implementation CouponStyleView

- (void)setCouponContent:(NSString *)couponContent {
    _couponContent = couponContent;
    self.couponTitleLbl.text = _couponContent;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.couponBgView = [YYCreateTools createImageView:@"single_coupon_icon"
                                             viewModel:-1];
    [self addSubview:self.couponBgView];
    
    self.couponTitleLbl = [YYCreateTools createLabel:nil
                                                font:midFont(11)
                                           textColor:XHRedColor];
    self.couponTitleLbl.numberOfLines = 1;
    self.couponTitleLbl.preferredMaxLayoutWidth = kSizeScale(100);
    self.couponTitleLbl.textAlignment = NSTextAlignmentCenter;
    [self.couponBgView addSubview:self.couponTitleLbl];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.couponBgView  mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.couponTitleLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(1));
        make.right.mas_equalTo(-kSizeScale(1));
        make.center.equalTo(self.couponBgView);
    }];
}

@end


@interface GoodDetailDynamicInfoCell()

@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIView *oldActivityLineView;
@property (nonatomic, strong) UIView *ActivityLineView;

@property (nonatomic, strong) UIView *bottomView;

@property (nonatomic, strong) UIView *couponView;
@property (nonatomic, strong) UILabel *couponShowView;
@property (nonatomic, strong) UILabel  *couponTipLbl;
@property (nonatomic, strong) UILabel  *couponNameLbl;
@property (nonatomic, strong) UILabel  *couponReceLbl;
@property (nonatomic, strong) UIImageView *couponArrowImage;


@property (nonatomic, strong) UIView *oldActivityView;
@property (nonatomic, strong) UILabel  *oldActivityTipLbl;
@property (nonatomic, strong) UILabel     *oldActivityContentLbl;
@property (nonatomic, strong) UIImageView *oldActivityArrowImage;
@property (nonatomic, strong) UIImageView *oldActivityContentImageView;

@property (nonatomic, strong) UIView *activityView;
@property (nonatomic, strong) UILabel  *activityTipLbl;
@property (nonatomic, strong) UILabel     *activityContentLbl;
@property (nonatomic, strong) UIImageView *activityArrowImage;

@property (nonatomic, strong) UIView *serviceView;
@property (nonatomic, strong) UILabel *serviceTipLabel;
@property (nonatomic, strong) UIImageView *serviceArrowImage;

@property (nonatomic, strong) GoodDetailServiceInfoView *serviceShowView; //详情&&服务介绍
@property (nonatomic, strong) GoodDetailActivityInfoView *activityShowView; //详情&&活动介绍

@property (nonatomic, strong) MASConstraint *couponHeight; //优惠券高度
@property (nonatomic, strong) MASConstraint *oldActivityHeight;//活动高度
@property (nonatomic, strong) MASConstraint *activityHeight;//活动高度
@property (nonatomic, strong) MASConstraint *serviceHeight;//服务高度
@property (nonatomic, strong) MASConstraint *bottomHeight;

@property (nonatomic, strong) HomeBannerInfoModel *bannerModel;
@end

@implementation GoodDetailDynamicInfoCell

- (void)createUI {
    //topview不相干的 为了灰色的view
    self.topView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.topView];
    //优惠券view
    self.couponView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.couponView];
    [self.couponView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                action:@selector(couponClickEvent:)]];
    
    self.couponTipLbl = [YYCreateTools createLabel:@"领券"
                                              font:midFont(13)
                                         textColor:XHBlackLitColor];
    [self.couponView addSubview:self.couponTipLbl];
    

    self.couponShowView = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHRedColor];
    [self.couponView addSubview:self.couponShowView];
    
    self.couponReceLbl = [YYCreateTools createLabel:@"领取"
                                              font:normalFont(13)
                                         textColor:XHRedColor];
    [self.couponView addSubview:self.couponReceLbl];
    
    self.couponArrowImage = [YYCreateTools createImageView:@"expand_more_red_icon"
                                               viewModel:-1];
    [self.couponView addSubview:self.couponArrowImage];
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.couponView addSubview:self.lineView];
    
    //旧活动view
    self.oldActivityView = [YYCreateTools createView:XHWhiteColor];
    self.oldActivityView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.oldActivityView];
    [self.oldActivityView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(oldactivitySourcesClickEvent:)]];
    
    self.oldActivityLineView = [YYCreateTools createView:XHLightColor];
    [self.oldActivityView addSubview:self.oldActivityLineView];
    
    self.oldActivityTipLbl = [YYCreateTools createLabel:@"活动"
                                                font:midFont(13)
                                           textColor:XHBlackLitColor];
    [self.oldActivityView addSubview:self.oldActivityTipLbl];
    
    self.oldActivityArrowImage = [YYCreateTools createImageView:nil
                                                         viewModel:-1];
    [self.oldActivityView addSubview:self.oldActivityArrowImage];
    
    self.oldActivityContentImageView = [YYCreateTools createImageView:nil
                                                         viewModel:-1];
    [self.oldActivityView addSubview:self.oldActivityContentImageView];
    
    self.activityContentLbl = [YYCreateTools createLabel:nil
                                                    font:midFont(13)
                                               textColor:XHBlackColor];
    self.activityContentLbl.numberOfLines = 1;
    [self.oldActivityView addSubview:self.activityContentLbl];

    self.oldActivityArrowImage = [YYCreateTools createImageView:@"new_expand_more_icon"
                                                   viewModel:-1];
    [self.oldActivityView addSubview:self.oldActivityArrowImage];
    
    //活动view
    self.activityView = [YYCreateTools createView:XHWhiteColor];
    self.activityView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.activityView];
    [self.activityView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                action:@selector(activitySourcesClickEvent:)]];
    
    self.ActivityLineView = [YYCreateTools createView:XHLightColor];
    [self.activityView addSubview:self.ActivityLineView];

    self.activityTipLbl = [YYCreateTools createLabel:@"促销"
                                              font:midFont(13)
                                         textColor:XHBlackLitColor];
    [self.activityView addSubview:self.activityTipLbl];

    self.activityShowView = [[GoodDetailActivityInfoView alloc] init];
    [self.activityView addSubview:self.activityShowView];

    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomView];
    
    self.serviceView = [YYCreateTools createView:XHWhiteColor];
    self.serviceView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.serviceView];
    [self.serviceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(serviceClickEvent:)]];
    

    self.serviceTipLabel = [YYCreateTools createLabel:@"服务"
                                                 font:midFont(13)
                                            textColor:XHBlackLitColor];
    [self.serviceView addSubview:self.serviceTipLabel];
    
    self.serviceShowView = [[GoodDetailServiceInfoView alloc] initWithFrame:CGRectZero];
    [self.serviceView addSubview:self.serviceShowView];
    
    self.serviceArrowImage = [YYCreateTools createImageView:@"new_expand_more_icon"
                                                      viewModel:-1];
    [self.serviceView addSubview:self.serviceArrowImage];
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(0));
    }];
    
    [self.couponView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.topView);
        make.top.equalTo(self.topView.mas_bottom);
        self.couponHeight = make.height.mas_equalTo(kSizeScale(42));
    }];

    /**券的内容的布局**/
    [self.couponTipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.width.mas_equalTo(kSizeScale(30));
        make.centerY.equalTo(self.couponView);
    }];
    
    [self.couponArrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponView);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self.couponReceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponView);
        make.width.mas_equalTo(kSizeScale(30));
        make.right.equalTo(self.couponArrowImage.mas_left).offset(-kSizeScale(5));
    }];
    
    [self.couponShowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.couponView);
        make.left.equalTo(self.couponTipLbl.mas_right).offset(kSizeScale(15));
        make.right.equalTo(self.couponReceLbl.mas_left);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.couponView.mas_bottom);
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(1));
    }];
    /**旧活动的内容的布局**/
    [self.oldActivityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.topView);
        make.top.equalTo(self.lineView.mas_bottom);
        self.oldActivityHeight = make.height.mas_equalTo(kSizeScale(35));
    }];

    [self.oldActivityTipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.width.mas_equalTo(kSizeScale(30));
        make.centerY.equalTo(self.oldActivityView);
    }];
    [self.oldActivityArrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.oldActivityTipLbl);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.right.mas_equalTo(-kSizeScale(12));
    }];

    [self.oldActivityLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.oldActivityView.mas_bottom);
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(1));
    }];
    
    /**活动的内容的布局**/
    [self.activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.topView);
        make.top.equalTo(self.oldActivityLineView.mas_bottom);
        self.activityHeight = make.height.mas_equalTo(kSizeScale(35));
    }];

    [self.activityTipLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.width.mas_equalTo(kSizeScale(30));
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(35);
    }];
    
    [self.ActivityLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityView.mas_bottom);
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(1));
    }];
    
    [self.activityShowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.activityView);
        make.left.equalTo(self.activityTipLbl.mas_right).offset(kSizeScale(15));
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];

    [self.serviceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ActivityLineView.mas_bottom);
        make.left.right.equalTo(self.topView);
        self.serviceHeight = make.height.mas_equalTo(kSizeScale(42));
    }];
    
    [self.serviceTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.centerY.equalTo(self.serviceView);
        make.height.mas_equalTo(40);
    }];
    
    [self.serviceArrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.serviceTipLabel);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self.serviceShowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.serviceTipLabel.mas_right).offset(kSizeScale(18));
        make.right.equalTo(self.serviceArrowImage.mas_left).offset(-kSizeScale(10));
        make.centerY.equalTo(self.serviceTipLabel);
        make.height.equalTo(self.serviceTipLabel);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.serviceView.mas_bottom);
        self.bottomHeight = make.height.mas_equalTo(kSizeScale(8));
        make.bottom.equalTo(self.contentView);
    }];
    
    [self.couponHeight install];
    [self.oldActivityHeight install];
    [self.activityHeight install];
    [self.serviceHeight install];
    [self.bottomHeight install];
}

- (void)setServiceLists:(NSMutableArray *)serviceLists {
    _serviceLists = serviceLists;
    self.serviceShowView.serviceLists = _serviceLists;
}

- (void)setDynamicCell:(NSDictionary *)couponInfo
              activity:(NSDictionary *)activityInfo
              oldactivity:(id)oldactivityModel{
    BOOL couponHide = NO,  oldActivityHide= NO ,activityHide = NO;
    self.bannerModel = (HomeBannerInfoModel *)oldactivityModel;
    HomeGoodCouponInfoModel *couponModel = [HomeGoodCouponInfoModel modelWithJSON:couponInfo];
    if (!kValidArray(couponModel.couponStrList)) {
        [self.couponHeight uninstall];
        self.couponView.hidden = YES;
        couponHide = YES;
    }
    else {
        [self.couponShowView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

        NSString *title = couponModel.couponStrList[0];
        self.couponShowView.text =  title;
        for (int i = 1; i < couponModel.couponStrList.count ; i++) {
            if (i== 3) {
                continue;
            }
            title = [title stringByAppendingString:[NSString stringWithFormat:@"   %@",couponModel.couponStrList[i]]];
            self.couponShowView.text =  title;
        }
    }
    if (!self.bannerModel || !self.bannerModel.switchFlag) {
        [self.oldActivityHeight uninstall];
        self.oldActivityView.hidden = YES;
        oldActivityHide = YES;
    }
    else {
        UIImage *image = [UIImage getImageFromURL:self.bannerModel.imageUrl];
        self.oldActivityContentImageView.image = image;
        [self.oldActivityContentImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.oldActivityTipLbl.mas_right).offset(kSizeScale(15));
            make.centerY.equalTo(self.oldActivityTipLbl);
            make.size.mas_equalTo(CGSizeMake(image.size.width/2.0, image.size.height/2.0));
        }];
        
        [self.oldActivityContentLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.oldActivityContentImageView.mas_right);
            make.centerY.equalTo(self.oldActivityTipLbl);
        }];
        
        self.oldActivityContentLbl.text = self.bannerModel.title;
    }

    HomeGoodActivityInfoModel *activityModel;
    activityModel = [HomeGoodActivityInfoModel modelWithJSON:activityInfo];

    if (activityModel.showSwitch == 0) {
        [self.activityHeight uninstall];
        self.activityView.hidden = YES;
        activityHide = YES;
    }
    else {
         [self.activityView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.topView);
            make.top.equalTo(self.oldActivityLineView.mas_bottom);
            self.activityHeight = make.height.mas_equalTo(kSizeScale(35*activityModel.items.count));
        }];
        self.activityShowView.activityItems = [activityModel.items mutableCopy];
    }
//    if (activityHide && couponHide&&oldActivityHide) {
//        [self.bottomHeight uninstall];
//    }
}

#pragma mark - Private
/**处理券的展示**/
- (void)handlerCouponShow:(NSMutableArray *)coupons {
    NSMutableArray *views = [NSMutableArray array];
    NSMutableArray *viewWidths = [NSMutableArray array];
    for (int i = 0; i < coupons.count ; i ++) {
        CGSize size = CGSizeZero;
        CouponStyleView *styleView = [[CouponStyleView alloc] initWithFrame:CGRectZero];
        styleView.couponContent = [coupons objectAtIndex:i];
        size = [YYCommonTools sizeWithText:[coupons objectAtIndex:i]
                                      font:midFont(11)
                                   maxSize:CGSizeMake(MAXFLOAT, kSizeScale(30))];
        [self.couponShowView addSubview:styleView];
        [views addObject:styleView];
        [viewWidths addObject:@(size.width+12)];
    }
    CouponStyleView *lastView = nil;
    for (int i = 0; i < views.count; i ++) {
        CGFloat width = [[viewWidths objectAtIndex:i] floatValue];
        CouponStyleView *view = [views objectAtIndex:i];
        if (lastView) {
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastView.mas_right).offset(kSizeScale(8));
                make.centerY.equalTo(view.superview);
                make.size.mas_equalTo(CGSizeMake(width, kSizeScale(15)));
            }];
        }
        else {
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(kSizeScale(8));
                make.centerY.equalTo(view.superview);
                make.size.mas_equalTo(CGSizeMake(width, kSizeScale(15)));
            }];
        }
        lastView = view;
    }
}

#pragma mark - Event

-(void)serviceClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.serviceBlock) {
        self.serviceBlock();
    }
}

- (void)couponClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.couponBlock) {
        self.couponBlock();
    }
}

- (void)activitySourcesClickEvent:(UIGestureRecognizer *)tapGesture {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.bannerModel) {
        [params setObject:@(self.bannerModel.linkType) forKey:@"linkType"];
        [params setObject:self.bannerModel.url forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC]
                                     params:params];
    }
}

- (void)oldactivitySourcesClickEvent:(UIGestureRecognizer *)tapGesture {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.bannerModel) {
        [params setObject:@(self.bannerModel.linkType) forKey:@"linkType"];
        [params setObject:self.bannerModel.url forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC]
                                     params:params];
    }
}

@end
