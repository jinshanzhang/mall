//
//  GoodDetailMenuBarItem.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "JMMenuBarItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailMenuBarItem : JMMenuBarItem
// 标题
@property (nonatomic, strong) UILabel  *titleLabel;

@end

NS_ASSUME_NONNULL_END
