//
//  GoodDetailSkuInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface GoodDetailSkuInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) NSString       *selectTip;
@property (nonatomic, strong) NSMutableArray *serviceLists;
@property (nonatomic, copy) void (^clickSelectSkuBlock)(void);

@end
