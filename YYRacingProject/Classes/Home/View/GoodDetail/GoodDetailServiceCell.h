//
//  GoodDetailServiceCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/4/23.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodDetailServiceCell : YYBaseTableViewCell

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel     *titleLabel;
@property (nonatomic, strong) UILabel     *detailLabel;

@end

NS_ASSUME_NONNULL_END
