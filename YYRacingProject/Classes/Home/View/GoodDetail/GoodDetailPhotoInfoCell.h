//
//  GoodDetailPhotoInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface GoodDetailPhotoInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) NSString  *imageURL;
@property (nonatomic, copy) void (^photoClick)(GoodDetailPhotoInfoCell *currentCell);

@end
