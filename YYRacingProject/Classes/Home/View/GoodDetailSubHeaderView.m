//
//  GoodDetailSubHeaderView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailSubHeaderView.h"
#import "HomeBannerInfoModel.h"
@interface GoodDetailSubHeaderView()

@property (nonatomic, strong) NSMutableArray    *bannerArrays;
@property (nonatomic, strong) UIImageView       *topImageView;
@property (nonatomic, strong) HomeBannerInfoModel *bannerModel;

@end

@implementation GoodDetailSubHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.bannerArrays = [NSMutableArray array];
        self.backgroundColor = XHWhiteColor;
        self.topImageView = [YYCreateTools createImageView:nil
                                                 viewModel:-1];
        [self addSubview:self.topImageView];
        
        [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        [self.topImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerClickEvent:)]];
    }
    return self;
}

- (void)setSubHeaderView:(NSMutableDictionary *)headerSources {
    self.bannerModel = [HomeBannerInfoModel modelWithJSON:headerSources];
    [self.topImageView yy_sdWebImage:self.bannerModel.imageUrl
                placeholderImageType:YYPlaceholderImageHomeBannerType];
}

#pragma mark - Event

- (void)bannerClickEvent:(UITapGestureRecognizer *)tapGesture {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.bannerModel) {
        [params setObject:@(self.bannerModel.linkType) forKey:@"linkType"];
        [params setObject:self.bannerModel.url forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    }
}

@end
