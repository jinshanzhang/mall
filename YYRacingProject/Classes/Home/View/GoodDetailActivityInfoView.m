//
//  GoodDetailActivityInfoView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "GoodDetailActivityInfoView.h"
#import "ActivityCellBasicView.h"


@implementation GoodDetailActivityInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatUI];
    }
    return self;
}

-(void)creatUI{
    self.activityViewArr = [NSMutableArray array];
}

-(void)setActivityItems:(NSMutableArray *)activityItems{
    _activityItems = activityItems;
    if (kValidArray(self.activityViewArr)) {
        return;
    }
    for (int i = 0 ; i < self.activityItems.count; i++) {
        ActivityCellBasicView *view = [[ActivityCellBasicView alloc] init];
        [self.activityViewArr addObject:view];
        [self addSubview:view];
        
        NSString *redtitle =[self.activityItems[i] objectForKey:@"title"];
        NSString *word = [NSString stringWithFormat:@"[%@] %@",redtitle,[self.activityItems[i] objectForKey:@"description"]];

        NSMutableAttributedString *titleWord = [[NSMutableAttributedString alloc]initWithString:word];
        [titleWord addAttribute:NSForegroundColorAttributeName value:XHRedColor range:NSMakeRange(0,redtitle.length+2)];
        view.title.attributedText = titleWord;


        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(activityEvent:)]];
    }
    
    if (self.activityViewArr.count == 1) {
        [self.activityViewArr[0] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.bottom.mas_equalTo(0);
        }];
    }else{
        [self.activityViewArr mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
        // 设置array的水平方向的约束
        [self.activityViewArr mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
        }];
    }

}


- (void)activityEvent:(UIGestureRecognizer *)tapGesture {
    UIView *tapView= [tapGesture view];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.activityItems[tapView.tag]) {
        NSMutableDictionary *linkDic = [self.activityItems[tapView.tag] objectForKey:@"link"];
        NSNumber *number =[linkDic objectForKey:@"linkType"] ;
        [params setObject:number forKey:@"linkType"];
        [params setObject:[linkDic objectForKey:@"uri"] forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC]
                                     params:params];
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
