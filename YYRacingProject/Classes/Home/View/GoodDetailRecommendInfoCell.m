//
//  GoodDetailRecommendInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "GoodDetailRecommendInfoCell.h"

#import "YYMultiScrollView.h"
#import "RecommendHeaderView.h"
@interface GoodDetailRecommendInfoCell()
<YYMultiScrollViewDelegate>
@property (nonatomic, strong) RecommendHeaderView *headerView;
@property (nonatomic, strong) YYMultiScrollView   *scrollView;
@property (nonatomic, strong) UIView              *bottomView;

@end

@implementation GoodDetailRecommendInfoCell

#pragma mark - Setter
- (void)setItemModels:(HomeListStructInfoModel *)itemModels {
    _itemModels = itemModels;
    if (_itemModels) {
        self.scrollView.modelType = YYMultiScrollModelGoodType;
        self.scrollView.structModel = itemModels;
    }
}

#pragma mark - Life cycle

- (void)createUI {
    self.headerView = [[RecommendHeaderView alloc] initWithFrame:CGRectZero];
    self.headerView.titleLabel.text = @"为你推荐";
    [self.contentView addSubview:self.headerView];
    
    self.scrollView = [[YYMultiScrollView alloc] initWithFrame:CGRectZero];
    self.scrollView.delegate = self;
    [self.contentView addSubview:self.scrollView];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomView];
    
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(44));
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom);
        make.left.right.equalTo(self.headerView);
        make.height.mas_equalTo(kSizeScale(175));
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView.mas_bottom).offset(kSizeScale(10));
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(8));
        make.bottom.equalTo(self.contentView);
    }];
}

#pragma mark - YYMultiScrollViewDelegate
- (void)mulitiScrollViewDidSelect:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(goodDetailRecommendCell:didSelectItemAtIndexPath:)]) {
        [self.delegate goodDetailRecommendCell:self
                      didSelectItemAtIndexPath:indexPath];
    }
}

@end
