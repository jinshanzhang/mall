//
//  RecommendHeaderView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "RecommendHeaderView.h"
@interface RecommendHeaderView()
{
    UIView *lineView;
    UIView *markView;
}
@end

@implementation RecommendHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.showStyle = RecommendShowDetailStyle;
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    markView = [YYCreateTools createView:XHRedColor];
    markView.layer.cornerRadius = kSizeScale(1.5);
    markView.layer.masksToBounds = YES;
    [self addSubview:markView];
    
    self.titleLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(14)
                                       textColor:XHBlackColor];
    self.titleLabel.numberOfLines = 1;
    [self addSubview:self.titleLabel];
    
    lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:lineView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    markView.frame = CGRectMake(kSizeScale(0), (self.height-kSizeScale(13))/2.0, kSizeScale(0), kSizeScale(13));
    
    self.titleLabel.frame = CGRectMake(markView.right+kSizeScale(14), 0, kSizeScale(100), kSizeScale(20));
    self.titleLabel.centerY = markView.centerY;
    
    lineView.frame = CGRectMake(kSizeScale(12), self.height-kSizeScale(1), self.width-(kSizeScale(12)*2), kSizeScale(1));
}


@end
