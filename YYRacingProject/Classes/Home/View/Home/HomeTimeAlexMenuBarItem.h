//
//  HomeTimeAlexMenuBarItem.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/27.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuBarItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeTimeAlexMenuBarItem : JMMenuBarItem

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, assign) BOOL isTomorrow;

@end

NS_ASSUME_NONNULL_END
