//
//  HomeCategoryMenuBarItem.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "JMMenuBarItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCategoryMenuBarItem : JMMenuBarItem
/**
 * item title.
 **/
@property (nonatomic, strong) UILabel *titleLabel;

@end

NS_ASSUME_NONNULL_END
