//
//  HomeCategoryMenuBarItem.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeCategoryMenuBarItem.h"

@implementation HomeCategoryMenuBarItem

#pragma mark - Setter
- (void)setItemSelect:(BOOL)itemSelect {
    [super setItemSelect:itemSelect];
    if (itemSelect) {
        self.titleLabel.font = boldFont(14);
    }
    else {
        self.titleLabel.font = midFont(14);
    }
    if (self.titleColorTurnBlack) {
        self.titleLabel.textColor = (itemSelect == YES ? XHBlueDeepColor : XHBlackColor);
    }
    else {
        self.titleLabel.textColor = XHWhiteColor;
    }
}

- (void)isItitemScaleTransform:(BOOL)isScale {
    [UIView animateWithDuration:0.2
                     animations:^{
                         if (isScale) {
                             self.titleLabel.transform = CGAffineTransformMakeScale(1.2, 1.2);
                             //self.titleLabel.layer.transform = CGAffineTransformMakeScale(1.25, 1.25);
                         }
                         else {
                             self.titleLabel.transform = CGAffineTransformIdentity;
                             //self.titleLabel.layer.transform = CATransform3DIdentity;
                         }
                     }];
}

- (void)setTitleColorTurnBlack:(BOOL)titleColorTurnBlack {
    [super setTitleColorTurnBlack:titleColorTurnBlack];
    if (titleColorTurnBlack) {
        self.titleLabel.textColor = (self.itemSelect == YES ? XHBlueDeepColor : XHBlackColor);
    }
    else {
        self.titleLabel.textColor = XHWhiteColor;
    }
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [self initWithFrame:CGRectZero];
    if (self) { }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemSelect = NO;
        [self _setSubViews];
        [self _setSubLayouts];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self _setSubLayouts];
}

#pragma mark - Private
- (void)_setSubViews {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textColor = XHWhiteColor;
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
}

- (void)_setSubLayouts {
    self.titleLabel.bounds = self.bounds;
    self.titleLabel.center = CGPointMake(self.frame.size.width/2.0, self.frame.size.height/2.0);
}

@end
