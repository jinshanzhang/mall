//
//  HomeTimeAlexMenuBarItem.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/27.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeTimeAlexMenuBarItem.h"

@interface HomeTimeAlexMenuBarItem()

@property (nonatomic, strong) UIView *lineView;

@end

@implementation HomeTimeAlexMenuBarItem

#pragma mark - Setter

- (void)setIsTomorrow:(BOOL)isTomorrow {
    _isTomorrow = isTomorrow;
    if (isTomorrow) {
        self.timeLabel.font = boldFont(14);
        self.titleLabel.hidden = YES;
    }
    else {
        self.titleLabel.hidden = NO;
        self.timeLabel.font = boldFont(16);
    }
}

- (void)setItemSelect:(BOOL)itemSelect {
    [super setItemSelect:itemSelect];
    if (itemSelect) {
        self.timeLabel.textColor = self.titleLabel.textColor = XHRedColor;
        self.lineView.alpha = 1.0;
    }
    else {
        self.lineView.alpha = 0.0;
        self.timeLabel.textColor = XHBlackColor;
        self.titleLabel.textColor = XHBlackLitColor;
    }
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [self initWithFrame:CGRectZero];
    if (self) { }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemSelect = NO;
        self.backgroundColor = XHWhiteColor;
        [self _setSubViews];
        [self _setSubLayouts];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self _setSubLayouts];
}

#pragma mark - Private
- (void)_setSubViews {
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.numberOfLines = 1;
    self.timeLabel.textColor = XHBlackColor;
    self.timeLabel.font = boldFont(16);
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.timeLabel];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textColor = XHBlackLitColor;
    self.titleLabel.font = midFont(11);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = XHRedColor;
    [self addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(36), kSizeScale(2)));
        make.bottom.equalTo(self);
    }];
}

- (void)_setSubLayouts {
    [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.offset(self.isTomorrow?kSizeScale(20):kSizeScale(8));
    }];
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.timeLabel.mas_bottom).offset(kSizeScale(0));
    }];
}
@end
