//
//  AudioDetailDescriInfoCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/23.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "AudioDetailDescriInfoCell.h"

#import "YY_GoodDetailDetailViewController.h"
#import "YY_GoodDetailArgumentViewController.h"


@interface AudioDetailDescriInfoCell()
<VTMagicViewDelegate,
VTMagicViewDataSource>

@property (nonatomic, strong) NSArray *menuList;
@property (nonatomic, strong) VTMagicController *magicController;

@end
@implementation AudioDetailDescriInfoCell


#pragma mark - Setter
- (VTMagicController *)magicController {
    if (!_magicController) {
        _magicController = [[VTMagicController alloc] init];
        _magicController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _magicController.magicView.navigationColor = [UIColor whiteColor];
        _magicController.magicView.sliderHidden = YES;
        _magicController.magicView.switchStyle = VTSwitchStyleUnknown;
        _magicController.magicView.layoutStyle = VTLayoutStyleCenter;
        _magicController.magicView.navigationHeight = 36.f;
        _magicController.magicView.dataSource = self;
        _magicController.magicView.delegate = self;
        _magicController.magicView.headerView.hidden = YES;
        _magicController.magicView.needPreloading = NO;  //需要重新载入
        _magicController.magicView.itemWidth = kSizeScale(176);
        
        CGFloat padding = (kScreenW - kSizeScale(176) * 2)/2.0;
        _magicController.magicView.menuBar.isAddItemSpaceLine = YES;
        _magicController.magicView.menuBar.layer.cornerRadius = 2;
        _magicController.magicView.menuBar.layer.borderColor = XHBlackColor.CGColor;
        _magicController.magicView.menuBar.layer.borderWidth = 0.5;
        _magicController.magicView.menuBar.backgroundColor = XHBlackColor;
        _magicController.magicView.leftNavigatoinItem = _magicController.magicView.rightNavigatoinItem = [[UIView alloc] initWithFrame:CGRectMake(0, 0, padding, _magicController.magicView.navigationHeight)];
        
        _magicController.magicView.separatorColor = XHWhiteColor;
    }
    return _magicController;
}

- (void)setModel:(LessonDetailModel *)model{
    _model = model;
    [self.magicController.magicView reloadData];
}


#pragma mark - Life cycle
- (void)createUI {
    self.menuList = @[@"内容详情",@"进群学习",];
    YYBaseViewController *ctrl = (YYBaseViewController *)[YYCommonTools getCurrentVC];
    [ctrl addChildViewController:self.magicController];
    [self.contentView addSubview:self.magicController.view];
    [self.magicController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.contentView.mas_top).offset(kSizeScale(12));
        make.height.mas_equalTo(kScreenH-(kNavigationH+kSizeScale(12))).priorityHigh();
        // 适配8.0 将下面语句更换为height 设置。
        //make.bottom.equalTo(self.contentView.mas_bottom).offset(-(kBottom(50)+kNavigationH));
    }];
//    [self.magicController.magicView reloadData];
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    NSMutableArray *titleList = [NSMutableArray array];
    for (NSString *title in self.menuList) {
        [titleList addObject:title];
    }
    return titleList;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [menuItem setTitleColor:XHWhiteColor forState:UIControlStateSelected];
        [menuItem setBackgroundImage:[UIImage imageWithColor:XHWhiteColor] forState:UIControlStateNormal];
        [menuItem setBackgroundImage:[UIImage imageWithColor:XHBlackColor] forState:UIControlStateSelected];
        menuItem.titleLabel.font = midFont(13);
    }
    return menuItem;
}


- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    if (pageIndex == 0) {
        YY_GoodDetailDetailViewController *detailCtrl = [[YY_GoodDetailDetailViewController alloc] init];
        if (kValidArray(self.model.detailImages)) {
            detailCtrl.parameter = @{@"detailImages":self.model.detailImages,
                                     @"isHideSource":@(YES)};
        }
        return detailCtrl;
     }
    else {
        YY_GoodDetailDetailViewController *detailCtrl = [[YY_GoodDetailDetailViewController alloc] init];
        if (self.model.course.qunImage) {
            detailCtrl.parameter = @{@"detailImages":@[self.model.course.qunImage],
                                 @"isHideSource":@(YES)};
        }

        return detailCtrl;
     
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
