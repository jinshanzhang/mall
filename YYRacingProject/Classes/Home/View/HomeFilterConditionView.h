//
//  HomeFilterConditionView.h
//  YIYanProject
//
//  Created by cjm on 2018/4/6.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterConditionDelegate <NSObject>

@optional
- (void)filterSelectPoint:(NSInteger)index
                priceType:(YYPriceFilterType)priceType;

@end

@interface HomeFilterConditionView : UIView

@property (nonatomic, strong) NSMutableArray *itemTitles;
@property (nonatomic, weak) id <FilterConditionDelegate> delegate;

- (void)willItemBecomeSelectAtIndex:(NSInteger)index;
@end
