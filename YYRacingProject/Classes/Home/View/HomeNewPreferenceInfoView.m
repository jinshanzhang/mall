//
//  HomeNewPreferenceInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/24.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeNewPreferenceInfoView.h"

#import "yyCountDownManager.h"
#import "YYMultiItemScrollView.h"


#import "HomeResourcesInfoModel.h"
#import "YYMultiItemScrollModel.h"
// 新人特惠UI
@interface HomeNewPreferenceInfoView ()
<YYMultiItemScrollViewDelegate>

@property (nonatomic, strong) UIImageView *lookMoreImageView;
@property (nonatomic, strong) UIImageView *scrollBgImageView;
@property (nonatomic, strong) YYMultiItemScrollView *scrollView;

@end

@implementation HomeNewPreferenceInfoView

#pragma mark - Setter method
- (UIImageView *)scrollBgImageView {
    if (!_scrollBgImageView) {
        _scrollBgImageView = [YYCreateTools createImageView:nil
                                                  viewModel:-1];
    }
    return _scrollBgImageView;
}

- (UIImageView *)lookMoreImageView {
    if (!_lookMoreImageView) {
        _lookMoreImageView = [YYCreateTools createImageView:@"home_new_vip_more_icon"
                                                  viewModel:-1];
    }
    [_lookMoreImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookMoreClickEvent:)]];
    return _lookMoreImageView;
}

- (YYMultiItemScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[YYMultiItemScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.miniItemSpace = 0.0;
        _scrollView.miniItemSpace = kSizeScale(8);
        _scrollView.contentBackgroundColor = XHClearColor;
        _scrollView.itemSize = CGSizeMake(kSizeScale(120), kSizeScale(190));
        _scrollView.contentEdgeInsets = UIEdgeInsetsMake(0, kSizeScale(12), kSizeScale(0), kSizeScale(12));
        _scrollView.multiScrollDelegate = self;
    }
    return _scrollView;
}

- (void)setResourceModel:(HomeResourcesInfoModel *)resourceModel {
    _resourceModel = resourceModel;
    [kConfigCountDown setCountDownTimeInterval:0.1f];
    [kConfigCountDown addSourceWithIdentifier:@"newVip"];
    [kConfigCountDown start];
    if (_resourceModel) {
        NSMutableArray *itemList = [NSMutableArray array];
        for (int i = 0; i < resourceModel.items.count; i ++) {
            YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] initWithHomeResourceItem:[resourceModel.items objectAtIndex:i]];
            /*if (i < 10) {
                [itemList addObject:model];
            }*/
            [itemList addObject:model];
        }
        /*YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] initWithHomeResourceItem:nil];
        [itemList addObject:model];*/
        
        self.scrollView.dataSource = itemList;
        [self.scrollBgImageView sd_setImageWithURL:[NSURL URLWithString:_resourceModel.bgImage.imageUrl]];
    }
    [self.scrollView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.resourceModel==nil ? 0.0 : kSizeScale(190));
    }];
    [self.lookMoreImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(self.resourceModel==nil ? CGSizeMake(kSizeScale(0), kSizeScale(0)) : CGSizeMake(kSizeScale(63), kSizeScale(18)));
    }];
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHStoreGrayColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    [self addSubview:self.scrollBgImageView];
    [self addSubview:self.scrollView];
    [self addSubview:self.lookMoreImageView];
    
    [self.scrollBgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(190));
        make.bottom.mas_equalTo(-kSizeScale(15));
    }];
    
    [self.lookMoreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(63), kSizeScale(18)));
        make.right.equalTo(self.scrollView).offset(-kSizeScale(12));
        make.bottom.equalTo(self.scrollView.mas_top).offset(-kSizeScale(28));
    }];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)||self.frame.size.height<1) {
        return;
    }
}

#pragma mark - Event
- (void)lookMoreClickEvent:(UITapGestureRecognizer *)tapGesture {
    [self lookMoreMethod];
}

#pragma mark - Private
/**查看更多**/
- (void)lookMoreMethod {
    if (kValidDictionary(self.resourceModel.link)) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if ([self.resourceModel.link objectForKey:@"linkType"]) {
            [params setObject:[self.resourceModel.link objectForKey:@"linkType"] forKey:@"linkType"];
        }
        if ([self.resourceModel.link objectForKey:@"uri"]) {
            [params setObject:[self.resourceModel.link objectForKey:@"uri"] forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    }
}


#pragma mark - YYMultiItemScrollViewDelegate
- (void)multiScrollView:(YYMultiItemScrollView *)scrollView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (row < self.resourceModel.items.count) {
        HomeResourceItemInfoModel *itemModel = [self.resourceModel.items objectAtIndex:row];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        
        [params setObject:@(1) forKey:@"linkType"];
        if (kValidString(itemModel.uri)) {
            [params setObject:itemModel.uri forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    }
    else {
        [self lookMoreMethod];
    }
}



@end
