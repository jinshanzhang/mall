//
//  AudioDetailCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/22.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "LessonDetailModel.h"

@interface AudioDetailCell : YYBaseTableViewCell

@property (nonatomic, strong) LessonDetailModel *model;

@end
