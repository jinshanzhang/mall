//
//  HomeBrandHotInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "HomeBrandHotInfoCell.h"
#import "YYSingleImageView.h"
#import "YYMultiScrollView.h"

#import "HomeListStructInfoModel.h"
@interface HomeBrandHotInfoCell ()
<
YYSingleImageViewDelegate,
YYMultiScrollViewDelegate
>

@property (nonatomic, strong) YYSingleImageView *coverImageView;
@property (nonatomic, strong) YYMultiScrollView *scrollView;
@property (nonatomic, strong) UIView            *shadowView;
@property (nonatomic, strong) UIView            *goundView;
@property (nonatomic, strong) UIImageView       *markImageView;

@property (nonatomic, strong) YYImageInfoModel  *coverModel;
@end

@implementation HomeBrandHotInfoCell

#pragma mark - Setter method
- (YYSingleImageView *)coverImageView {
    if (!_coverImageView) {
        _coverImageView = [[YYSingleImageView alloc] initWithFrame:CGRectZero];
        _coverImageView.delegate = self;
    }
    return _coverImageView;
}

- (YYMultiScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[YYMultiScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (UIImageView *)markImageView {
    if (!_markImageView) {
        _markImageView = [YYCreateTools createImageView:@"brand_banner_arrow"
                                              viewModel:-1];
    }
    return _markImageView;
}

- (void)setBrandInfoModel:(HomeListStructInfoModel *)brandInfoModel {
    _brandInfoModel = brandInfoModel;
    self.coverModel = _brandInfoModel.coverImage;
    self.scrollView.modelType = YYMultiScrollModelGoodType;
    self.scrollView.structModel = brandInfoModel;

    [self setSubViewContent];
}

- (void)Handler:(NSNotification *)notification {
    [self.scrollView.brandListView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - Life cycle
- (void)createUI {
    self.backgroundColor = self.contentView.backgroundColor = XHStoreGrayColor;
    
    self.shadowView = [YYCreateTools createView:XHClearColor];
    self.shadowView.frame = CGRectMake(kSizeScale(12), kSizeScale(12), kScreenW - kSizeScale(12)*2, kSizeScale(160)+kSizeScale(130));
    [self.contentView addSubview:self.shadowView];
    
    self.goundView = [YYCreateTools createView:XHWhiteColor];
    self.goundView.v_cornerRadius = kSizeScale(5);
    self.goundView.frame = CGRectMake(0, 0, self.shadowView.width, self.shadowView.height - 0.5);
    [self.shadowView addSubview:self.goundView];
    
    [self.goundView addSubview:self.coverImageView];
    [self.coverImageView addSubview:self.markImageView];
    [self.goundView addSubview:self.scrollView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(Handler:) name:MainHotScroll object:nil];
    // layout
    /*[self.goundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(kSizeScale(12));
        make.right.offset(-kSizeScale(12));
        make.bottom.offset(0);
    }];*/
    
    [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.mas_equalTo(kSizeScale(160)).priorityHigh();
    }];
    
    [self.markImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(17), kSizeScale(9)));
        make.centerX.equalTo(self.coverImageView);
        make.bottom.equalTo(self.coverImageView);
    }];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.coverImageView);
        make.top.equalTo(self.coverImageView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(130));
        make.bottom.equalTo(self.goundView.mas_bottom);
    }];
    
    // 增加阴影
    /*[self.shadowView addShadow:ShadowDirectionBottom
                   shadowColor:HexRGBALPHA(0x000000, 0.2)
                 shadowOpacity:0.8
                  shadowRadius:0.5
               shadowPathWidth:0.3];*/
}

#pragma mark - Private
- (void)setSubViewContent {
    if (self.coverModel) {
        /*CGFloat height = [self.coverModel.pHeight floatValue]/2;
        [self.coverImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(height);
        }];*/
        self.coverImageView.coverImage = self.coverModel;
    }
}

#pragma mark - YYMultiScrollViewDelegate
- (void)mulitiScrollViewDidSelect:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeBrandHotInfoCell:didSelectAtIndexPath:)]) {
        [self.delegate homeBrandHotInfoCell:self didSelectAtIndexPath:indexPath];
    }
}

#pragma mark - YYSingleImageViewDelegate
- (void)singleImageViewDidSelect {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeBrandHotInfoCell:didSelectAtIndexPath:)]) {
        [self.delegate homeBrandHotInfoCell:self didSelectAtIndexPath:nil];
    }
}

@end
