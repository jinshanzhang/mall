//
//  HomeGridLayoutInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HomeResourceItemInfoModel;
@protocol  HomeGridLayoutDelegate <NSObject>

@required
- (void)homeGridItemClickHandler:(HomeResourceItemInfoModel *)infoModel;
@end

@interface HomeGridLayoutInfoView : UIView

@property (nonatomic, weak)  id <HomeGridLayoutDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *itemModels;

@end
