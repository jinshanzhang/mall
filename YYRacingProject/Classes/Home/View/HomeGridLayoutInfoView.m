//
//  HomeGridLayoutInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeGridLayoutInfoView.h"
#define GRIDVIEWTAG  100
#import "HomeResourceItemInfoModel.h"
@interface HomeGridLayoutInfoView()

@property (nonatomic, strong) NSMutableArray  *gridViews;

@end

@implementation HomeGridLayoutInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.gridViews = [NSMutableArray array];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    for (int i = 0; i < 4; i ++) {
        UIImageView *gridImagView = [[UIImageView alloc] init];
        gridImagView.tag = GRIDVIEWTAG + i;
        gridImagView.userInteractionEnabled = YES;
        [gridImagView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(activityClickEvent:)]];
        [self addSubview:gridImagView];
        [self.gridViews addObject:gridImagView];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self layoutIfNeeded];
    
    __block UIImageView *lastView = nil;
    CGFloat itemWidth = self.frame.size.width/6.0;
    
    [self.gridViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIImageView *itemView = (UIImageView *)obj;
        if (lastView) {
            if (idx > 0 && idx < 3) {
                [itemView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(lastView.mas_right);
                    make.top.bottom.equalTo(lastView);
                    make.width.mas_equalTo(itemWidth);
                }];
            }
            else {
                [itemView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(lastView.mas_right);
                    make.top.bottom.equalTo(lastView);
                    make.width.mas_equalTo(itemWidth*2);
                }];
            }
        }
        else {
            [itemView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.top.bottom.equalTo(self);
                make.width.mas_equalTo(itemWidth*2);
            }];
        }
        lastView = itemView;
    }];
}

- (void)setItemModels:(NSMutableArray *)itemModels {
    _itemModels = itemModels;
    __block NSInteger tempCount = _itemModels.count;
    if (!_itemModels) {
        [_itemModels enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UIImageView *itemView = (UIImageView *)obj;
            itemView.image = [UIImage imageWithColor:XHLightColor];
        }];
        return;
    }
    @weakify(itemModels);
    [self.gridViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(itemModels);
        if (idx < tempCount) {
            UIImageView *itemView = (UIImageView *)obj;
            HomeResourceItemInfoModel *itemModel = [itemModels objectAtIndex:idx];
            [itemView yy_sdWebImage:itemModel.coverImage.imageUrl placeholderImageType:YYPlaceholderImageListGoodType];
        }
    }];
}


#pragma mark - Event
- (void)activityClickEvent:(UITapGestureRecognizer *)tapGesture {
    NSInteger tag = ((UIImageView *)tapGesture.view).tag - GRIDVIEWTAG;
    if (_delegate && [_delegate respondsToSelector:@selector(homeGridItemClickHandler:)]) {
        if (tag < self.itemModels.count) {
            if (tag<2) {
                [MobClick event:@"homeUpNew"];
            }else{
                [MobClick event:@"homeHotGood"];
            }
            [_delegate homeGridItemClickHandler:[self.itemModels objectAtIndex:tag]];
        }
    }
}
@end
