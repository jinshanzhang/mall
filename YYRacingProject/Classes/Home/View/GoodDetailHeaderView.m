//
//  GoodDetailHeaderView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailHeaderView.h"
#import "YYBannerScrollView.h"

@interface GoodDetailHeaderView()
<YYBannerScrollViewDelegate>

@property (nonatomic, strong) UIImageView *specialImageView; //新人专项图标

@property (nonatomic, strong) YYBannerScrollView *yyScrollView;
@property (nonatomic, strong) NSMutableArray *photoUrls;

@end

@implementation GoodDetailHeaderView

#pragma mark - Setter method

- (void)setBannerArrs:(NSMutableArray *)bannerArrs {
    _bannerArrs = bannerArrs;
    self.photoUrls = _bannerArrs;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.yyScrollView.imageURLs = self.photoUrls;
    });
}

- (void)setSpecialMarkStatu:(NSInteger)specialMarkStatu {
    _specialMarkStatu = specialMarkStatu;
    self.specialImageView.alpha = (_specialMarkStatu == 1 ? 1.0 : 0.0);
}

#pragma mark - Life cycle

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, kScreenW, kScreenW);
    
        self.yyScrollView = [[YYBannerScrollView alloc] init];
        self.yyScrollView.currentPageDotImage = [UIImage imageNamed:@"home_banner_selected_icon"];
        self.yyScrollView.pageDotImage = [UIImage imageNamed:@"home_banner_default_icon"];
        self.yyScrollView.pageControlDotSize = CGSizeMake(7, 7);
        self.yyScrollView.delegate = self;
        
        [self addSubview:self.yyScrollView];
        
        self.specialImageView = [YYCreateTools createImageView:@"detail_new_vip_icon"
                                                     viewModel:-1];
        self.specialImageView.alpha = 0.0;
        [self addSubview:self.specialImageView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.yyScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(kScreenW);
    }];
    
    [self.specialImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(98), kSizeScale(30)));
    }];
}

#pragma mark - YYBannerScrollViewDelegate
- (void)yyBannerScrollView:(YYBannerScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    NSString *url;
    UIView *fromView;
    NSMutableArray *items = [NSMutableArray new];
    
    for (NSUInteger i = 0, max = self.photoUrls.count; i < max; i++) {
        url = [self.photoUrls objectAtIndex:i];
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = cycleScrollView;
        item.largeImageURL = [NSURL URLWithString:url];
        
        [items addObject:item];
        if (i == index) {
            fromView = cycleScrollView;
        }
    }
    YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:index];
    [v presentFromImageView:fromView toContainer:kAppWindow animated:YES completion:nil];
    v.hideViewBlock = ^(NSInteger currentPage) {
        [self.yyScrollView makeScrollViewScrollToIndex:currentPage];
    };
}

@end
