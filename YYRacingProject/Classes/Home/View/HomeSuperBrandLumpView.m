//
//  HomeSuperBrandLumpView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSuperBrandLumpView.h"

// 超级品牌团标题
@interface HomeSuperBrandLumpTitleInfoCell: UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation HomeSuperBrandLumpTitleInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = self.contentView.backgroundColor = XHSearchGrayColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.titleLabel = [YYCreateTools createLabel:@"超级品牌团"
                                            font:boldFont(17)
                                       textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(15));
    }];
}

@end



// 更多品牌
@interface HomeSuperBrandLumpNextInfoCell: UITableViewCell

@property (nonatomic, strong) UIView  *whiteBackgroundView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView  *shadowView;
@property (nonatomic, strong) UIImageView *rightImageView;

@property (nonatomic, copy) void (^cellClickBlock)(void);
@end

@implementation HomeSuperBrandLumpNextInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = self.contentView.backgroundColor = XHSearchGrayColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    self.whiteBackgroundView = [YYCreateTools createView:XHWhiteColor];
    self.whiteBackgroundView.v_cornerRadius = 5;
    [self.contentView addSubview:self.whiteBackgroundView];
    
    [self.whiteBackgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundClickGesture:)]];
    
    self.titleLabel = [YYCreateTools createLabel:@"更多品牌团及预热"
                                            font:normalFont(12)
                                       textColor:XHBlackColor];
    [self.whiteBackgroundView addSubview:self.titleLabel];
    
    self.rightImageView = [YYCreateTools createImageView:@"home_down_more_icon"
                                               viewModel:-1];
    [self.whiteBackgroundView addSubview:self.rightImageView];
    
    
    /*self.shadowView = [YYCreateTools createView:XHClearColor];
    [self.contentView addSubview:self.shadowView];

    self.shadowView.frame = CGRectMake(kSizeScale(12), 0, kScreenW - kSizeScale(12)*2, kSizeScale(35));
    self.whiteBackgroundView.frame = CGRectMake(0, 0, self.shadowView.width, self.shadowView.height-0.5);
    [self.shadowView addShadow:ShadowDirectionBottom
                   shadowColor:HexRGBALPHA(0x000000, 0.2)
                 shadowOpacity:0.8
                  shadowRadius:0.5
               shadowPathWidth:0.3];*/

    [self.whiteBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.top.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(40));
    }];
    
    CGSize titleSize = [YYCommonTools sizeWithText:@"更多品牌团及预热"
                                              font:normalFont(12)
                                           maxSize:CGSizeMake(MAXFLOAT, 20)];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.whiteBackgroundView);
        make.size.mas_equalTo(titleSize);
    }];
    
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(11), kSizeScale(11)));
        make.centerY.equalTo(self.titleLabel).offset(-1);
        make.left.equalTo(self.titleLabel.mas_right).offset(kSizeScale(4));
    }];
}

#pragma mark - Event
- (void)backgroundClickGesture:(UITapGestureRecognizer *)gesture {
    if (self.cellClickBlock) {
        self.cellClickBlock();
    }
}

@end

#import "HomeSuperBrandLumpInfoModel.h"
#import "HomeSuperBrandLumpListInfoCell.h"
#import "YYHomeSuperBrandListGoodPageInfoRequest.h"

@interface HomeSuperBrandLumpView ()
<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, assign) BOOL canLoadMore; //是否可加载更多
@property (nonatomic, assign) NSInteger totalLength; //当前页已经加载多少条数据
@property (nonatomic, assign) NSInteger pageLength; //每页多少条
@property (nonatomic, strong) NSMutableArray *dataIds; //数据ids

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *listData;

@property (nonatomic, assign) CGFloat  normalRowHeight;  // 正常行高
@property (nonatomic, assign) CGFloat  topRowHeight;     // 顶部样式的高度
@property (nonatomic, assign) CGFloat  bottomRowHeight;  // 底部样式的高度
@property (nonatomic, assign) CGFloat  tableTotalHeight; // table总高度
@end

@implementation HomeSuperBrandLumpView

#pragma mark - Setter method
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.allowsSelection = NO;
        _tableView.scrollsToTop = YES;
        _tableView.scrollEnabled = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = XHWhiteColor;
        if (@available(iOS 11, *)) {
            //目前如此
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
        }
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return _tableView;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.canLoadMore = NO;
        self.topRowHeight = kSizeScale(48);
        self.bottomRowHeight = kSizeScale(60);
        self.normalRowHeight = kSizeScale(265) + kSizeScale(15);
        self.totalLength = 0;
        self.pageLength = 8;
        self.dataIds = [NSMutableArray array];
        self.listData = [NSMutableArray array];
        [self createUI];
    }
    return self;
}
#pragma mark - Public method
- (void)setSuperBrandViewModel:(HomeSuperBrandLumpInfoModel *)model {
    self.tableTotalHeight = 0;
    if (model) {
        self.totalLength = 0;
        self.pageLength = 8;
        [self.listData removeAllObjects];
        self.canLoadMore = (model.brandIds.count > model.items.count) ? YES : NO;
        [self.listData addObjectsFromArray:model.items];
        if (kValidArray(model.items)) {
            [self.listData insertObject:@"超级品牌团" atIndex:0];
        }
        self.totalLength = model.items.count;
        self.dataIds = model.brandIds;
        self.tableTotalHeight = self.normalRowHeight*model.items.count+(self.canLoadMore==YES?self.bottomRowHeight:0.0)+self.topRowHeight;
    }
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.tableTotalHeight);
    }];
    [self.tableView reloadData];
}

#pragma mark - Request
/**超级品牌下一页**/
- (void)pullDownRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params removeAllObjects];
    if ((self.totalLength+self.pageLength) > self.dataIds.count) {
        self.pageLength = self.dataIds.count - self.totalLength;
    }
    [params setObject:[[self.dataIds subarrayWithRange:NSMakeRange(self.totalLength, self.pageLength)] componentsJoinedByString:@","] forKey:@"uris"];
    YYHomeSuperBrandListGoodPageInfoRequest *turnPageAPI = [[YYHomeSuperBrandListGoodPageInfoRequest alloc] initHomeSuperBrandListGoodPageInfoRequest:params];
    [turnPageAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            if ([responDict objectForKey:@"items"]) {
                NSArray *items = [NSArray modelArrayWithClass:[HomeSingleSuperBrandLumpInfoModel class] json:[responDict objectForKey:@"items"]];
                if (kValidArray(items)) {
                    [self.listData addObjectsFromArray:items];
                }
                
                self.totalLength = self.totalLength + self.pageLength;
                self.canLoadMore = [self canLoadMore];
                self.tableTotalHeight = self.tableTotalHeight + self.pageLength*self.normalRowHeight;
                
                self.pageLength = 8;
                
                if (!self.canLoadMore) {
                    self.tableTotalHeight = self.tableTotalHeight - self.bottomRowHeight;
                }
                [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.height.mas_equalTo(self.tableTotalHeight);
                }];
                
                if (self.heightUpdateBlock) {
                    self.heightUpdateBlock(self.tableTotalHeight);
                }
                
                [self.tableView reloadData];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}


#pragma mark - Private method
- (void)createUI {
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(0);
    }];
    
    Class cls = [HomeSuperBrandLumpTitleInfoCell class];
    registerClass(self.tableView, cls);
    cls = [HomeSuperBrandLumpNextInfoCell class];
    registerClass(self.tableView, cls);
    cls = [HomeSuperBrandLumpListInfoCell class];
    registerClass(self.tableView, cls);
}

- (BOOL)canLoadMore {
    return self.dataIds.count > self.totalLength;
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.canLoadMore ? self.listData.count + 1 : self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    Class cls = [HomeSuperBrandLumpListInfoCell class];
    if (row < self.listData.count) {
        id dataObj = [self.listData objectAtIndex:row];
        if ([dataObj isKindOfClass:[NSString class]]) {
            cls = [HomeSuperBrandLumpTitleInfoCell class];
            HomeSuperBrandLumpTitleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            return cell;
        }
        else {
            cls = [HomeSuperBrandLumpListInfoCell class];
            HomeSuperBrandLumpListInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            [cell setSuperBrandViewModel:[self.listData objectAtIndex:row]];
            return cell;
        }
    }
    else {
        cls = [HomeSuperBrandLumpNextInfoCell class];
        HomeSuperBrandLumpNextInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
        cell.cellClickBlock = ^{
            @strongify(self);
            [self pullDownRequest];
        };
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (row < self.listData.count) {
        id dataObj = [self.listData objectAtIndex:row];
        if ([dataObj isKindOfClass:[NSString class]]) {
            return self.topRowHeight;
        }
        else
        return self.normalRowHeight;
    }
    else {
        return self.bottomRowHeight;
    }
}

@end
