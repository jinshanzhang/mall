//
//  YYSearchResultCollectionCell.m
//  YYRacingProject
//
//  Created by cujia_1 on 2020/9/2.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "YYSearchResultCollectionCell.h"
#import "YYRowSingleView.h"
@interface YYSearchResultCollectionCell()

@property (nonatomic, strong) YYRowSingleView * productView;

@end

@implementation YYSearchResultCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = UIColor.redColor;
        [self setUp];
    }
    return self;
}


- (void)setUp {
    self.productView = [[YYRowSingleView alloc] initWithFrame:CGRectZero];
    self.productView.userInteractionEnabled = NO;
//    tempView.delegate = self;
    [self addSubview:self.productView];
    [self.productView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (void)setModel:(HomeListStructInfoModel *)model {
    _model = model;
    self.productView.itemModel = model;
}

@end
