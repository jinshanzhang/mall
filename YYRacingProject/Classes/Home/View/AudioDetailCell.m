//
//  AudioDetailCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/22.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "AudioDetailCell.h"

@interface AudioDetailCell()
@property (nonatomic, strong)  UILabel       *titleLabel;
@property (nonatomic, strong)  UIImageView   *showIMG;
@property (nonatomic, strong)  UIImageView   *iconIMG;

@property (nonatomic, strong)  UILabel       *numLabel;
@property (nonatomic, strong)  UILabel       *listLabel;
@property (nonatomic, strong)  UILabel       *detailLabel;



@end
@implementation AudioDetailCell

-(void)createUI{
    
    self.showIMG = [YYCreateTools createImageView:@"" viewModel:-1];
    self.showIMG.backgroundColor = XHRedColor;
    self.showIMG.v_cornerRadius = 2;
    [self addSubview:self.showIMG];
    
    [self.showIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(12);
        make.width.mas_equalTo(kSizeScale(75));
        make.height.mas_equalTo(kSizeScale(75));
    }];
    
    self.titleLabel = [YYCreateTools createLabel:@"0-3岁的亲子运动及游戏：如何培养高智商的宝宝" font:normalFont(15) textColor:XHBlackColor];
    self.titleLabel.numberOfLines = 2;
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.showIMG.mas_right).mas_offset(13);
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-12);
    }];
    
    self.numLabel = [YYCreateTools createLabel:@"125人在学  |" font:normalFont(11) textColor:XHBlackLitColor];
    [self addSubview:self.numLabel];
    
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.showIMG.mas_right).mas_offset(13);
        make.bottom.mas_equalTo(self.showIMG.mas_bottom).mas_offset(-5);
    }];
    
    self.iconIMG = [YYCreateTools createImageView:@"chapter_icon" viewModel:-1];
    [self addSubview:self.iconIMG];

    [self.iconIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.numLabel.mas_right).mas_offset(5);
        make.bottom.mas_equalTo(self.numLabel).mas_offset(-3);
    }];
    
    self.listLabel =[YYCreateTools createLabel:@"" font:normalFont(11) textColor:XHBlackLitColor];
    [self addSubview:self.listLabel];
    
    [self.listLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.iconIMG.mas_right).mas_offset(5);
        make.bottom.mas_equalTo(self.showIMG.mas_bottom).mas_offset(-5);
    }];
    
    
    UIView *line = [YYCreateTools createView:XHSeparateLineColor];
    [self addSubview:line];

    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(self.showIMG.mas_bottom).mas_offset(10);
        make.height.mas_equalTo(1);
    }];
    
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:boldFont(16) textColor:XHBlackColor];
    self.detailLabel.numberOfLines = 0;
    [self addSubview:self.detailLabel];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(line.mas_bottom).mas_offset(10);
        make.bottom.mas_equalTo(-10);
    }];
}

-(void)setModel:(LessonDetailModel *)model{
    _model = model;
    [self.showIMG sd_setImageWithURL:[NSURL URLWithString:model.course.coverImage.imageUrl]];
    self.titleLabel.text= model.course.title;
    self.numLabel.text =model.course.sellCountTitle;
    self.detailLabel.text = model.title;
    self.listLabel.text =model.course.lessonCountTitle;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
@end
