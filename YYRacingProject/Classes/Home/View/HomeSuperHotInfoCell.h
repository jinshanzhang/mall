//
//  HomeSuperHotInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class HomeSuperHotInfoCell;
@protocol HomeSuperHotInfoCellDelegate<NSObject>

@optional
- (void)homeSuperHotClickItemCell:(HomeSuperHotInfoCell *)cell;

@end


@interface HomeSuperHotInfoCell : YYBaseTableViewCell

@property (nonatomic, weak) id <HomeSuperHotInfoCellDelegate> delegate;
@property (nonatomic, strong) id <HomeSuperHotItemProtocol> itemModel;

@end

NS_ASSUME_NONNULL_END
