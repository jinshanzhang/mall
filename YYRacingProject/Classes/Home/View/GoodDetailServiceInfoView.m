//
//  GoodDetailServiceInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GoodDetailServiceInfoView.h"

@interface GoodDetailServiceExpainView : UIView

@property (nonatomic, strong) UIView *circleView;
@property (nonatomic, strong) UILabel *titleLable;

@end

@implementation GoodDetailServiceExpainView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.circleView = [YYCreateTools createView:XHGaldMidColor];
        [self addSubview:self.circleView];
        
        self.titleLable = [YYCreateTools createLabel:nil
                                                font:midFont(12)
                                           textColor:XHBlackColor];
        [self addSubview:self.titleLable];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.circleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(3, 3));
    }];
    self.circleView.layer.cornerRadius = 1.5;
    
    [self.titleLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.circleView.mas_right).offset(kSizeScale(3));
        make.centerY.equalTo(self.circleView);
    }];
}

@end


@interface GoodDetailServiceInfoView()

@property (nonatomic, strong) NSMutableArray *addServiceViews;

@end

@implementation GoodDetailServiceInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.addServiceViews = [NSMutableArray array];
        for (int i = 0; i < 4; i ++) {
            GoodDetailServiceExpainView *serviceView = [[GoodDetailServiceExpainView alloc] initWithFrame:CGRectZero];
            [self addSubview:serviceView];
            [self.addServiceViews addObject:serviceView];
        }
    }
    return self;
}

- (void)setServiceLists:(NSMutableArray *)serviceLists {
    _serviceLists = serviceLists;
    if (kValidArray(_serviceLists)) {
        [self layoutIfNeeded];
        
        NSInteger itemWidth = 0.0;
        NSString *serviceContent = @"";
        NSInteger count = _serviceLists.count;
        
        GoodDetailServiceExpainView *lastView = nil;
        for (int i = 0; i < self.addServiceViews.count; i ++) {
            GoodDetailServiceExpainView *view = self.addServiceViews[i];
            if (i >= count) {
                view.hidden = YES;
            }
            else {
                serviceContent = [_serviceLists objectAtIndex:i];
                itemWidth = [YYCommonTools sizeWithText:serviceContent
                                                   font:midFont(12)
                                                maxSize:CGSizeMake(MAXFLOAT, self.height)].width;
                if (itemWidth < kSizeScale(70)) {
                    itemWidth = kSizeScale(70);
                }
                view.titleLable.text = serviceContent;
                if (lastView) {
                    [view mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(lastView.mas_right);
                        make.centerY.equalTo(lastView);
                        make.width.height.equalTo(lastView);
                    }];
                }
                else {
                    [view mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(view.superview);
                        make.centerY.equalTo(view.superview);
                        make.size.mas_equalTo(CGSizeMake(itemWidth, view.superview.height));
                    }];
                }
                lastView = view;
            }
        }
    }
}
@end
