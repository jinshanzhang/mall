//
//  HomeSuperBrandLumpListInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeSuperBrandLumpListInfoCell.h"

#import "YYMultiItemScrollModel.h"
#import "HomeSuperBrandLumpInfoModel.h"

#import "YYMultiItemScrollView.h"
@interface HomeSuperBrandLumpListInfoCell()

@property (nonatomic, strong) UIImageView *groundImageView;
@property (nonatomic, strong) UIImageView *brandImageView;
@property (nonatomic, strong) UILabel     *countDownLabel;
@property (nonatomic, strong) YYMultiItemScrollView *brandScrollView;

@property (nonatomic, strong) HomeSingleSuperBrandLumpInfoModel *itemModel;
@end

@implementation HomeSuperBrandLumpListInfoCell

- (void)createUI {
    self.backgroundColor = self.contentView.backgroundColor = XHSearchGrayColor;
    
    self.groundImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    self.groundImageView.v_cornerRadius = kSizeScale(5);
    [self.groundImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(superBrandLumpClickEvent:)]];
    [self.contentView addSubview:self.groundImageView];
    
    self.countDownLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(12)
                                           textColor:XHTimeBlackColor];
    self.countDownLabel.backgroundColor = XHClearColor;
    [self.contentView addSubview:self.countDownLabel];
    
    // 注意要将视图添加在cell上
    self.brandScrollView = [[YYMultiItemScrollView alloc] initWithFrame:CGRectZero];
    self.brandScrollView.miniItemSpace = kSizeScale(12);
    self.brandScrollView.backgroundColor = self.brandScrollView.contentBackgroundColor = XHClearColor;
    self.brandScrollView.scrollEnabled = NO;
    self.brandScrollView.itemSize = CGSizeMake(kSizeScale(103), kSizeScale(103) + kSizeScale(30));
    self.brandScrollView.contentEdgeInsets = UIEdgeInsetsMake(kSizeScale(1), kSizeScale(12), kSizeScale(0), kSizeScale(12));
    [self.brandScrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(superBrandLumpClickEvent:)]];
    [self.contentView addSubview:self.brandScrollView];

    [self.groundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.top.equalTo(self.contentView);
        make.bottom.mas_equalTo(-kSizeScale(15));
    }];
    
    [self.brandScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.groundImageView);
        make.bottom.equalTo(self.groundImageView.mas_bottom).offset(-kSizeScale(15));
        make.height.mas_equalTo(kSizeScale(135));
    }];

    [self.countDownLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.groundImageView.mas_left).offset(kSizeScale(12));
        make.bottom.equalTo(self.brandScrollView.mas_top).offset(-kSizeScale(10));
    }];
}

- (void)setSuperBrandViewModel:(HomeSingleSuperBrandLumpInfoModel *)lumpModel {
    if (lumpModel) {
        self.itemModel = lumpModel;
        NSMutableArray *itemList = [NSMutableArray array];
        for (int i = 0; i < lumpModel.items.count; i ++) {
            YYMultiItemScrollModel *model = [[YYMultiItemScrollModel alloc] initWithListGoodItem:[lumpModel.items objectAtIndex:i]];
            [itemList addObject:model];
        }
        self.brandScrollView.dataSource = itemList;
        [self.groundImageView sd_setImageWithURL:[NSURL URLWithString:lumpModel.fullBackgroundImage.imageUrl] placeholderImage:[UIImage imageNamed:@"home_banner_placehold_icon"]];
        
        self.countDownLabel.text = lumpModel.brandTime;
    }
}

- (void)superBrandLumpClickEvent:(UITapGestureRecognizer *)gesture {
    if (self.itemModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (self.itemModel.link) {
            if ([self.itemModel.link objectForKey:@"uri"]) {
                [params setObject:[self.itemModel.link objectForKey:@"uri"] forKey:@"url"];
            }
            if ([self.itemModel.link objectForKey:@"linkType"]) {
                [params setObject:[self.itemModel.link objectForKey:@"linkType"] forKey:@"linkType"];
            }
            [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
        }
    }
}
@end
