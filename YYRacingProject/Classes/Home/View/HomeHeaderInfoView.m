//
//  HomeHeaderInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeHeaderInfoView.h"

#import "HomeGridLayoutInfoView.h"
#import "HomeSuperMoldbabyInfoView.h"
#import "HomeSuperBrandLumpView.h"
#import "HomeNewPreferenceInfoView.h"
#import "HomeActivityIconInfoCell.h"

#import "HomeResourcesInfoModel.h"
#import "HomeSuperBrandLumpInfoModel.h"

#import "YYCollectionAlignFlowLayout.h"

@interface HomeHeaderInfoView()
<UICollectionViewDelegate,
UICollectionViewDataSource,
SDCycleScrollViewDelegate,
HomeGridLayoutDelegate>

@property (nonatomic, strong) UIView  *gridView; // 宫格布局
@property (nonatomic, strong) SDCycleScrollView *topBannerView;//banner
@property (nonatomic, strong) UICollectionView  *iconListView; //
@property (nonatomic, strong) UIImageView       *bannerFloatView;
@property (nonatomic, strong) UIImageView       *iconFloatView;
@property (nonatomic, strong) UIImageView       *serviceFloatView; // 服务资源位
@property (nonatomic, strong) UIImageView       *superFloatView;
@property (nonatomic, strong) YYAnimatedImageView *capulesView; // 胶囊图
@property (nonatomic, strong) YYAnimatedImageView *marketView; //超市图
@property (nonatomic, strong) HomeNewPreferenceInfoView *vipView; //新人特惠
@property (nonatomic, strong) HomeSuperMoldbabyInfoView *superMView; // 超级爆款
@property (nonatomic, strong) HomeSuperBrandLumpView *superBrandView; // 超级品牌团

@property (nonatomic, strong) NSMutableArray *orderList; //顺序
@property (nonatomic, strong) NSMutableArray *iconSources; // icon 资源
@property (nonatomic, strong) NSMutableArray *bannerSources; // banner 资源
@property (nonatomic, strong) NSMutableArray *gridSources; // 八宫格资源
@property (nonatomic, strong) NSMutableArray *sourceTypeList;
@property (nonatomic, strong) NSMutableArray *sourceHeightList;

@property (nonatomic, strong) HomeResourcesInfoModel *superHotSource;
@property (nonatomic, strong) HomeResourcesInfoModel *vipSources; // 新人特惠资源

@property (nonatomic, strong) HomeResourceItemInfoModel *capuleSources; // 胶囊位资源
@property (nonatomic, strong) HomeResourceItemInfoModel *marketSources; // 超市资源位
@property (nonatomic, strong) HomeResourceItemInfoModel *serviceSources; // 服务资源

@property (nonatomic, strong) HomeSuperBrandLumpInfoModel *superBrandSources; // 超级品牌团资源

@property (nonatomic, assign) CGFloat superBrandTopHeight;
@property (nonatomic, assign) NSInteger floorCount;
@end

@implementation HomeHeaderInfoView
#pragma mark - Setter method
- (SDCycleScrollView *)topBannerView {
    if (!_topBannerView) {
        _topBannerView = [[SDCycleScrollView alloc] initWithFrame:CGRectZero];
        _topBannerView.autoScroll = YES;
        _topBannerView.delegate = self;
        _topBannerView.autoScrollTimeInterval = 5.0;
        _topBannerView.backgroundColor = XHClearColor;
        _topBannerView.pageControlDotSize = CGSizeMake(7, 7);
        _topBannerView.pageDotImage = [UIImage imageNamed:@"home_banner_default_icon"];
        _topBannerView.currentPageDotImage = [UIImage imageNamed:@"home_banner_selected_icon"];
    }
    return _topBannerView;
}

- (UICollectionView *)iconListView {
    if (!_iconListView) {
        
        YYCollectionAlignFlowLayout *flowLayout = [[YYCollectionAlignFlowLayout alloc]initWithType:AlignWithLeft betweenOfCell:0];
        //flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.minimumLineSpacing = 0;

        _iconListView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                            collectionViewLayout:flowLayout];
        _iconListView.contentInset = UIEdgeInsetsMake(kSizeScale(12), 0, kSizeScale(14), 0);
        _iconListView.delegate = self;
        _iconListView.dataSource = self;
        _iconListView.scrollsToTop = NO;
        _iconListView.showsHorizontalScrollIndicator = NO;
        _iconListView.backgroundColor = XHClearColor;
    }
    return _iconListView;
}

//超级爆款
- (HomeSuperMoldbabyInfoView *)superMView {
    if (!_superMView) {
        _superMView = [[HomeSuperMoldbabyInfoView alloc] initWithFrame:CGRectZero];
        _superMView.hidden = YES;
    }
    return _superMView;
}

//超级品牌团
- (HomeSuperBrandLumpView *)superBrandView {
    if (!_superBrandView) {
        _superBrandView = [[HomeSuperBrandLumpView alloc] initWithFrame:CGRectZero];
    }
    return _superBrandView;
}

//新人特惠
- (HomeNewPreferenceInfoView *)vipView {
    if (!_vipView) {
        _vipView = [[HomeNewPreferenceInfoView alloc] initWithFrame:CGRectZero];
    }
    return _vipView;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.orderList = [NSMutableArray array];
        self.gridSources = [NSMutableArray array];
        self.iconSources = [NSMutableArray array];
        self.bannerSources = [NSMutableArray array];
        self.sourceTypeList = [NSMutableArray array];
        self.sourceHeightList = [NSMutableArray array];
        
        [self createUI];
    }
    return self;
}

- (void)createUI {
    [self addSubview:self.topBannerView];
    
    self.bannerFloatView = [[UIImageView alloc] init];
    [self.topBannerView addSubview:self.bannerFloatView];
    
    self.serviceFloatView = [[UIImageView alloc] init];
    [self addSubview:self.serviceFloatView];
    
    self.iconFloatView = [[UIImageView alloc] init];
    [self addSubview:self.iconFloatView];
    
    [self addSubview:self.iconListView];
    
    self.superFloatView = [[UIImageView alloc] init];
    [self addSubview:self.superFloatView];
    
    self.capulesView = [[YYAnimatedImageView alloc] init];
    self.capulesView.userInteractionEnabled = YES;
    [self addSubview:self.capulesView];
    [self.capulesView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(capulesViewClickEvent:)]];
    
    self.marketView = [[YYAnimatedImageView alloc] init];
    self.marketView.userInteractionEnabled = YES;
    [self addSubview:self.marketView];
    [self.marketView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(marketViewClickEvent:)]];
    
    self.gridView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.gridView];

    Class currentCls = [HomeActivityIconInfoCell class];
    [self.iconListView registerClass:currentCls
          forCellWithReuseIdentifier:strFromCls(currentCls)];
    
    [self addSubview:self.superMView];
    
    [self addSubview:self.superBrandView];
    
    [self addSubview:self.vipView];
}

- (void)setHeaderSources:(NSMutableArray *)headerSources {
    CGFloat tempHeight = 0.0;
    @weakify(self);
    /**
     * BANNER(1, "banner"), ICON(2, "icon"), CAPSULE(3, "胶囊位"), BOOTHS(4, "八宫格")
     * SERVICE_GUARANTEE(5, "服务保障"), SUPER_TRENDY(6, "超级爆款")
     * BACKGROUND_IMAGE(7, "超级背景图"), BANNER_BACKGROUND_IMAGE(8, "banner背景图"),
     * ICON_BACKGROUND_IMAGE(9, "icon背景图")
     * SUPER_BRAND(11,"超级品牌团"), 超市资源位 12, 新人优惠资源位 13
     **/
    self.floorCount = 0;
    
    self.vipSources = nil;
    self.capuleSources = nil;
    self.marketSources = nil;
    self.superHotSource = nil;
    self.serviceSources = nil;
    self.superBrandSources = nil;
    
    [self.orderList removeAllObjects];
    [self.gridSources removeAllObjects];
    [self.iconSources removeAllObjects];
    [self.bannerSources removeAllObjects];
    [self.sourceHeightList removeAllObjects];
    
    [self resetSubViewLayout];
    
    _headerSources = headerSources;
    if (kValidArray(headerSources)) {
        // 高度
        __block CGFloat bannerHeight = 0.0, serviceHeight = 0.0, iconHeight = 0.0;
        __block CGFloat capulesHeight = 0.0, gridHeight = 0.0, superHeight = 0.0;
        __block CGFloat superBrandHeight = 0.0, marketHeight = 0.0, newVIPHeight = 0.0;
        // 是否显示
        __block BOOL showBanner = NO, showService = NO, showIcon = NO;
        __block BOOL showCapules = NO, showGrid = NO, showSuper = NO;
        __block BOOL showSuperBrand = NO, showMarket = NO, showNewVip = NO;
        // 背景图
        __block NSString *bannerBgImageUrl = nil, *superBgImageUrl = nil, *iconBgImageUrl = nil;
        [headerSources enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSInteger type = -1;
            HomeResourcesInfoModel *model = nil;
            HomeSuperBrandLumpInfoModel *brandModel = nil;
            if ([obj isKindOfClass:[HomeResourcesInfoModel class]]) {
                model = (HomeResourcesInfoModel *)obj;
                type = model.type;
            }
            else if ([obj isKindOfClass:[HomeSuperBrandLumpInfoModel class]]) {
                // 超级品牌团
                brandModel = (HomeSuperBrandLumpInfoModel *)obj;
                type = [brandModel.type integerValue];
            }
            [self.orderList addObject:@(type)];
            switch (type) {
                case 1:{
                    // banner
                    showBanner = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showBanner && !kValidArray(model.items)) {
                        showBanner = NO;
                    }
                    bannerHeight = (showBanner ?kSizeScale(150):0.0);
                    self.bannerSources = model.items;
                }
                    break;
                case 2:{
                    // icon
                    showIcon = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showIcon && !kValidArray(model.items)) {
                        showIcon = NO;
                    }
                    if (showIcon) {
                        if (model.items.count <= 5) {
                            iconHeight = kSizeScale(76)+kSizeScale(12)*2 + kSizeScale(14);
                        }
                        else {
                            iconHeight = 2*kSizeScale(76)+kSizeScale(12)*2 + kSizeScale(14);
                        }
                        self.iconSources = model.items;
                    }
                }
                    break;
                case 3:{
                    // capules
                    showCapules = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showCapules && !kValidArray(model.items)) {
                        showCapules = NO;
                    }
                    if (showCapules) {
                        self.capuleSources = [model.items firstObject];
                        capulesHeight = kSizeScale([self.capuleSources.coverImage.height floatValue]/2.0);
                    }
                }
                    break;
                case 4:{
                    // 八宫格
                    showGrid = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showGrid && !kValidArray(model.items)) {
                        showGrid = NO;
                    }
                    if (showGrid) {
                        self.floorCount = (int)ceilf(model.items.count/4.0);
                        gridHeight = kSizeScale(self.floorCount * 100);
                        self.gridSources = model.items;
                    }
                }
                    break;
                case 5:{
                    // 服务保障
                    showService = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showService && !kValidArray(model.items)) {
                        showService = NO;
                    }
                    if (showService) {
                        self.serviceSources = [model.items firstObject];
                        serviceHeight = kSizeScale([self.serviceSources.coverImage.height floatValue]/2.0);
                    }
                }
                    break;
                case 6:{
                    // 超级爆款
                    showSuper = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showSuper && !kValidArray(model.items)) {
                        showSuper = NO;
                    }
                    self.superMView.hidden = !showSuper;
                    if (showSuper) {
                        self.superHotSource = model;
                        superHeight = kSizeScale(254);
                    }
                }
                    break;
                case 7:{
                    // 超级爆款bg
                    if ([model.showSwitch integerValue] == 1) {
                        HomeResourceItemInfoModel *itemModel = [model.items firstObject];
                        superBgImageUrl = itemModel.coverImage.imageUrl;
                    }
                }
                    break;
                case 8:{
                    // banner bg
                    if ([model.showSwitch integerValue] == 1) {
                        HomeResourceItemInfoModel *itemModel = [model.items firstObject];
                        bannerBgImageUrl = itemModel.coverImage.imageUrl;
                    }
                }
                    break;
                case 9:{
                    // icon bg
                    if ([model.showSwitch integerValue] == 1) {
                        HomeResourceItemInfoModel *itemModel = [model.items firstObject];
                        iconBgImageUrl = itemModel.coverImage.imageUrl;
                    }
                }
                    break;
                case 11: {
                    // 超级品牌团
                    showSuperBrand = [brandModel.showSwitch integerValue] == 1?YES:NO;
                    if (showSuperBrand && !kValidArray(brandModel.items)) {
                        showSuperBrand = NO;
                    }
                    if (showSuperBrand) {
                        CGFloat itemHeight = kSizeScale(265) + kSizeScale(15);
                        CGFloat topItemHeight = kSizeScale(48);
                        CGFloat bottomItemHeight = kSizeScale(60);
                        
                        self.superBrandSources = brandModel;
                        if (brandModel.brandIds.count > brandModel.items.count) {
                            superBrandHeight = (brandModel.items.count)*itemHeight + bottomItemHeight + topItemHeight;
                        }
                        else {
                            superBrandHeight = (brandModel.items.count)*itemHeight + topItemHeight;
                        }
                    }
                }
                    break;
                case 12: {
                    showMarket = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showMarket && !kValidArray(model.items)) {
                        showMarket = NO;
                    }
                    if (showMarket) {
                        self.marketSources = [model.items firstObject];
                        marketHeight = kSizeScale([self.marketSources.coverImage.height floatValue]/2.0);
                    }
                }
                    break;
                case 13: {
                    showNewVip = [model.showSwitch integerValue] == 1?YES:NO;
                    if (showNewVip && !kValidArray(model.items) ) {
                        showNewVip = NO;
                    }
                    if (showNewVip && kUserInfo.newFansFlag == 0) {
                        showNewVip = NO;
                    }
                    if (showNewVip) {
                        self.vipSources = model;
                        newVIPHeight = kSizeScale(290);
                    }
                }
                    break;
            }
        }];
    
        [self.sourceHeightList addObject:@(bannerHeight)];
        [self.sourceHeightList addObject:@(iconHeight)];
        [self.sourceHeightList addObject:@(capulesHeight)];
        [self.sourceHeightList addObject:@(gridHeight)];
        [self.sourceHeightList addObject:@(serviceHeight)];
        [self.sourceHeightList addObject:@(superHeight)];
        [self.sourceHeightList addObject:@(superBrandHeight)];
        [self.sourceHeightList addObject:@(marketHeight)];
        [self.sourceHeightList addObject:@(newVIPHeight)];
        
        // -- layout
        
        //banner
        __block NSMutableArray *tempBannerList = [NSMutableArray array];
        [self.bannerSources enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeResourceItemInfoModel *model = (HomeResourceItemInfoModel *)obj;
            if (kValidString(model.coverImage.imageUrl)) {
                [tempBannerList addObject:model.coverImage.imageUrl];
            }
        }];
        self.topBannerView.showPageControl = showBanner;
        self.topBannerView.imageURLStringsGroup = tempBannerList;
        if (kValidString(bannerBgImageUrl)) {
            [self.bannerFloatView sd_setImageWithURL:[NSURL URLWithString:bannerBgImageUrl]];
        }
        else {
            self.bannerFloatView.image = [UIImage imageWithColor:XHClearColor];
        }
        
        //icon
        [self.iconListView reloadData];
        if (kValidString(iconBgImageUrl)) {
            [self.iconFloatView sd_setImageWithURL:[NSURL URLWithString:iconBgImageUrl]];
        }
        else {
            self.iconFloatView.image = [UIImage imageWithColor:XHWhiteColor];
        }
        
        // 胶囊位
        if (kValidString(self.capuleSources.coverImage.imageUrl)) {
            [self.capulesView setImageWithURL:[NSURL URLWithString:self.capuleSources.coverImage.imageUrl] placeholder:[UIImage imageNamed:@"home_banner_placehold_icon"]];
        }
        else {
            self.capulesView.image = [UIImage imageWithColor:XHClearColor];
        }
        
        // 超市
        if (kValidString(self.marketSources.coverImage.imageUrl)) {
            [self.marketView setImageWithURL:[NSURL URLWithString:self.marketSources.coverImage.imageUrl] placeholder:[UIImage imageNamed:@"home_banner_placehold_icon"]];
        }
        else {
            self.marketView.image = [UIImage imageWithColor:XHClearColor];
        }
        
        // 服务
        if (kValidString(self.serviceSources.coverImage.imageUrl)) {
            [self.serviceFloatView sd_setImageWithURL:[NSURL URLWithString:self.serviceSources.coverImage.imageUrl]];
        }
        else {
            self.serviceFloatView.image = [UIImage imageWithColor:XHStoreGrayColor];
        }
        // 超级爆款
        if (kValidString(superBgImageUrl)) {
            //[self.superFloatView sd_setImageWithURL:[NSURL URLWithString:superBgImageUrl]];
            self.superFloatView.image = [UIImage imageWithColor:XHStoreGrayColor];
        }
        else {
            self.superFloatView.image = [UIImage imageWithColor:XHStoreGrayColor];
        }
        [self handlerSuperHotData];
        self.superMView.resourceModel = self.superHotSource;
        
        self.vipView.resourceModel = self.vipSources;
        // 超级品牌团
        [self.superBrandView setSuperBrandViewModel:self.superBrandSources];
        
        self.superBrandView.heightUpdateBlock = ^(CGFloat height) {
            @strongify(self);
            CGFloat tempHeight = self.superBrandTopHeight;
            [self.superBrandView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self);
                make.top.mas_equalTo(self.superBrandTopHeight);
                make.height.mas_equalTo(height);
            }];
            tempHeight += height;
            // 判断后面是否还有其他类型
            NSArray *behindArray = [self getSuperBrandLumpOrderListOfBehind];
            if (kValidArray(behindArray)) {
                tempHeight = [self getHeaderTotalHeightWithTypeList:[behindArray mutableCopy] startHeight:tempHeight];
            }
            
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(tempHeight+0.1).priorityHigh();
            }];
            [self layoutIfNeeded];
            if (self.homeHeaderHeightUpdateBlock) {
                self.homeHeaderHeightUpdateBlock();
            }
        };
        //1, 2, 3, 4, 5, 6, 11, 12
        tempHeight = [self getHeaderTotalHeightWithTypeList:self.orderList startHeight:0.0];
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(tempHeight+0.1).priorityHigh();
        }];
        
        [self layoutIfNeeded];
    }
}


- (NSArray *)getSuperBrandLumpOrderListOfBehind {
    BOOL isHaveData = NO;
    NSInteger index = -1;
    for (int i = 0; i < self.orderList.count; i ++) {
        NSInteger type = [[self.orderList objectAtIndex:i] integerValue];
        if (type == 11) {
            index = i;
        }
    }
    if (index + 1 < self.orderList.count) {
        // 后面有值
        isHaveData = YES;
    }
    else {
        // 后面无值
        isHaveData = NO;
    }
    NSArray *subList = [self.orderList subarrayWithRange:NSMakeRange(index+1, self.orderList.count-(index+1))];
    return subList;
}

- (CGFloat)getHeaderTotalHeightWithTypeList:(NSMutableArray *)list
                                startHeight:(CGFloat)startHeight {
    CGFloat tempHeight = startHeight;
    for (NSNumber *orderValue in list) {
        NSInteger type = [orderValue integerValue];
        switch (type) {
            case 1: {
                CGFloat bannerH = [[self.sourceHeightList objectAtIndex:0] floatValue];
                [self.topBannerView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.offset(0);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(bannerH);
                }];
                [self.bannerFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.offset(0);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(bannerH);
                }];
                tempHeight += bannerH;
            }
                break;
            case 2: {
                CGFloat iconH = [[self.sourceHeightList objectAtIndex:1] floatValue];
                [self.iconListView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(kSizeScale(12));
                    make.right.offset(-kSizeScale(12));
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(iconH);
                }];
                [self.iconFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.offset(0);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(iconH);
                }];
                tempHeight += iconH;
            }
                break;
            case 3: {
                CGFloat capulesH = [[self.sourceHeightList objectAtIndex:2] floatValue];
                [self.capulesView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(capulesH);
                }];
                tempHeight += capulesH;
            }
                break;
            case 4: {
                NSRange  range;
                NSInteger tempCount = 4;
                NSInteger locationIndex = 0;
                NSArray  *tempArrays = [NSArray array];
                HomeGridLayoutInfoView *lastView = nil;
                NSMutableArray *floorArrays = [NSMutableArray array];
                
                CGFloat gridHeight = [[self.sourceHeightList objectAtIndex:3] floatValue];
        
                for (int i = 0; i < self.floorCount; i ++) {
                    range = NSMakeRange(locationIndex, tempCount);
                    tempArrays = [self.gridSources subarrayWithRange:range];
                    HomeGridLayoutInfoView *floorView = [[HomeGridLayoutInfoView alloc] initWithFrame:CGRectZero];
                    floorView.delegate = self;
                    floorView.itemModels = [tempArrays mutableCopy];
                    [self.gridView addSubview:floorView];
                    [floorArrays addObject:floorView];
                    locationIndex = locationIndex + tempCount;
                }
                
                if (kValidArray(floorArrays)) {
                    for (int i = 0; i < floorArrays.count; i ++) {
                        HomeGridLayoutInfoView *infoView = floorArrays[i];
                        if (lastView) {
                            [infoView mas_makeConstraints:^(MASConstraintMaker *make) {
                                make.left.right.equalTo(infoView.superview);
                                make.top.equalTo(lastView.mas_bottom);
                                make.height.mas_equalTo(kSizeScale(100));
                            }];
                        }
                        else {
                            [infoView mas_makeConstraints:^(MASConstraintMaker *make) {
                                make.left.top.right.equalTo(infoView.superview);
                                make.height.mas_equalTo(kSizeScale(100));
                            }];
                        }
                        lastView = infoView;
                    }
                }
                [self.gridView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(gridHeight);
                }];
                tempHeight += gridHeight;
            }
                break;
            case 5: {
                // 服务
                CGFloat serviceHeight = [[self.sourceHeightList objectAtIndex:4] floatValue];
                [self.serviceFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(serviceHeight);
                }];
                tempHeight += serviceHeight;
            }
                break;
            case 6: {
                // 超级爆款
                CGFloat superHeight = [[self.sourceHeightList objectAtIndex:5] floatValue];
                [self.superMView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(kSizeScale(12));
                    make.right.offset(-kSizeScale(12));
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(superHeight<=0?0:superHeight);
                }];
                [self.superFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(superHeight);
                }];
                tempHeight += superHeight;
            }
                break;
            case 11: {
                // 超级品牌团
                self.superBrandTopHeight = tempHeight;
                CGFloat superBrandHeight = [[self.sourceHeightList objectAtIndex:6] floatValue];
                [self.superBrandView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(superBrandHeight);
                }];
                tempHeight += superBrandHeight;
            }
                break;
            case 12: {
                CGFloat marketH = [[self.sourceHeightList objectAtIndex:7] floatValue];
                [self.marketView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(marketH);
                }];
                tempHeight += marketH;
            }
                break;
            case 13: {
                CGFloat vipH = [[self.sourceHeightList objectAtIndex:8] floatValue];
                [self.vipView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.equalTo(self);
                    make.top.mas_equalTo(tempHeight);
                    make.height.mas_equalTo(vipH);
                }];
                tempHeight += vipH;
            }
                break;
        }
    }
    return tempHeight;
}

- (void)resetSubViewLayout {
    [self.topBannerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.bannerFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.iconListView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.iconFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.capulesView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.gridView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.serviceFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.superMView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.superFloatView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.superBrandView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.marketView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.vipView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.mas_equalTo(0.0);
    }];
    [self.gridView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

#pragma mark - Private method
- (void)handlerSuperHotData {
    if (self.superHotSource) {
        self.superHotSource.scrollType = YYSingleRowScrollSuperHot;
        [self.superHotSource.items enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeResourceItemInfoModel *itemModel = (HomeResourceItemInfoModel *)obj;
            itemModel.scrollType = self.superHotSource.scrollType;
        }];
    }
}

#pragma mark - SDCycleDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewItemDidSelect:sourceModel:)]) {
        if (index < self.bannerSources.count) {
            [self.delegate headerViewItemDidSelect:self sourceModel:[self.bannerSources objectAtIndex:index]];
        }
    }
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.iconSources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class current = [HomeActivityIconInfoCell class];
    HomeActivityIconInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(current) forIndexPath:indexPath];
    if (row < self.iconSources.count && kValidArray(self.iconSources)) {
        HomeResourceItemInfoModel *itemModel = [self.iconSources objectAtIndex:row];
        cell.fontColor = itemModel.fontColor;
        cell.extendParams = [@{@"isStore":@(NO)} mutableCopy];
        [cell setActivityView:itemModel.title iconUrl:itemModel.coverImage.imageUrl];
    }
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize itemSize = CGSizeZero;
    CGFloat totalWidth = (kScreenW - kSizeScale(12)*2);
    if (self.iconSources.count <= 4 || (self.iconSources.count % 2 == 0 && self.iconSources.count <= 8)) {
        itemSize = CGSizeMake((int)(totalWidth/4.0), kSizeScale(76));
    }
    else {
        itemSize = CGSizeMake((int)(totalWidth/5.0), kSizeScale(76));
    }
    return itemSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, self.iconSources.count>0?kSizeScale(12):0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewItemDidSelect:sourceModel:)]) {
        if (row < self.iconSources.count) {
            //友盟打点
            NSString *eventName = [NSString stringWithFormat:@"homeicon%ld",row+1];
            [MobClick event:eventName];
            [self.delegate headerViewItemDidSelect:self sourceModel:[self.iconSources objectAtIndex:row]];
        }
    }
}

#pragma mark - Event
- (void)capulesViewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewItemDidSelect:sourceModel:)]) {
        if (self.capuleSources) {
            [self.delegate headerViewItemDidSelect:self sourceModel:self.capuleSources];
        }
    }
}

- (void)marketViewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewItemDidSelect:sourceModel:)]) {
        if (self.marketSources) {
            [self.delegate headerViewItemDidSelect:self sourceModel:self.marketSources];
        }
    }
}

#pragma mark - HomeGridLayoutDelegate
- (void)homeGridItemClickHandler:(HomeResourceItemInfoModel *)infoModel {
    if (self.delegate && [self.delegate respondsToSelector:@selector(headerViewItemDidSelect:sourceModel:)]) {
        if (infoModel) {
            [self.delegate headerViewItemDidSelect:self sourceModel:infoModel];
        }
    }
}

@end
