//
//  HomeSpecialSellDoubleInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "HomeSpecialSellDoubleInfoCell.h"
#import "YYRowSingleView.h"
#import "HomeListStructInfoModel.h"

#define   kPaddingTop  kSizeScale(8)
@interface HomeSpecialSellDoubleInfoCell ()
<YYRowSingleViewDelegate>
@property (nonatomic, strong)  UIView           *goodFrameView;
@property (nonatomic, strong)  NSMutableArray   *goodItemViews;

@end

@implementation HomeSpecialSellDoubleInfoCell

#pragma mark - Setter method
- (void)setItemModels:(NSMutableArray *)itemModels {
    _itemModels = itemModels;
    if (_itemModels) {
        @weakify(itemModels);
        [self.goodItemViews enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            @strongify(itemModels);
            YYRowSingleView *view = (YYRowSingleView *)obj;
            if (itemModels.count < 2) {
                view.hidden = (idx != 0)?YES:NO;
                HomeListStructInfoModel *structModel = [itemModels firstObject];
                view.itemModel = structModel;
            }
            else {
                view.hidden = NO;
                HomeListStructInfoModel *structModel = [itemModels objectAtIndex:idx];
                view.itemModel = structModel;
            }
        }];
    }
}

#pragma mark - Life cycle

- (void)createUI {
    self.backgroundColor = XHClearColor;
    
    self.goodItemViews = [NSMutableArray array];
    CGFloat viewW = (kScreenW - kSizeScale(8) - kSizeScale(12)*2)/2.0;
    self.goodFrameView = [YYCreateTools createView:XHStoreGrayColor];
    [self.contentView addSubview:self.goodFrameView];
    
    [self.goodFrameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    for (int i = 0; i < 2; i ++) {
        YYRowSingleView *tempView = [[YYRowSingleView alloc] initWithFrame:CGRectZero];
        tempView.delegate = self;
        [self.goodFrameView addSubview:tempView];
        [tempView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12)+(viewW+kPaddingTop)*i);
            make.top.equalTo(self.goodFrameView.mas_top).offset(kPaddingTop);
            make.bottom.equalTo(self.goodFrameView);
            make.width.mas_equalTo(viewW);
        }];
        [self.goodItemViews addObject:tempView];
    }
}

#pragma mark - YYRowSingleViewDelegate
- (void)rowSingleDidSelect:(YYRowSingleView *)singleView isShare:(BOOL)isShare {
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeSpecialSellDoubleCellCarrayModel:isShare:)]) {
        [self.delegate homeSpecialSellDoubleCellCarrayModel:singleView.itemModel
                                                    isShare:isShare];
    }
}

@end
