//
//  HomeBrandHotInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
NS_ASSUME_NONNULL_BEGIN
@class HomeBrandHotInfoCell, HomeListStructInfoModel;
@protocol HomeBrandHotInfoCellDelegate <NSObject>

@optional
- (void)homeBrandHotInfoCell:(HomeBrandHotInfoCell *)cell
        didSelectAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface HomeBrandHotInfoCell : YYBaseTableViewCell

@property (nonatomic, weak) id <HomeBrandHotInfoCellDelegate> delegate;
@property (nonatomic, strong) HomeListStructInfoModel *brandInfoModel;

@end

NS_ASSUME_NONNULL_END
