//
//  HomeNewVipEmptyInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/24.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "HomeNewVipEmptyInfoCell.h"

@interface HomeNewVipEmptyInfoCell ()

@property (nonatomic, strong) UIView *emptyBackgroundView;
@property (nonatomic, strong) UIImageView *brandImageView;

@end

@implementation HomeNewVipEmptyInfoCell

- (void)createUI {
    CGFloat cellWidth = kSizeScale(120);
    self.emptyBackgroundView = [YYCreateTools createView:XHWhiteColor];
    self.emptyBackgroundView.v_cornerRadius = 5;
    [self.contentView addSubview:self.emptyBackgroundView];
    
    self.brandImageView = [YYCreateTools createImageView:@"home_look_more_icon"
                                               viewModel:-1];
    [self.contentView addSubview:self.brandImageView];
    [self.brandImageView zy_cornerRadiusAdvance:5 rectCornerType:UIRectCornerAllCorners];
    
    [self.emptyBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.brandImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(cellWidth);
    }];
}

@end
