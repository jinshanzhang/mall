//
//  AudioDetailDescriInfoCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/23.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "LessonDetailModel.h"

@interface AudioDetailDescriInfoCell : YYBaseTableViewCell
@property (nonatomic, strong) LessonDetailModel *model;
@end
