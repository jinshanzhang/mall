//
//  SearchCategoryContentView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchCategoryContentView : UIView

- (void)searchCateDatas:(NSMutableArray *)cateDatas
     defaultSelectIndex:(NSInteger)selectIndex;

@end

NS_ASSUME_NONNULL_END
