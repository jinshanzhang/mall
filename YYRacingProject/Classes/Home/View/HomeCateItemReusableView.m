//
//  HomeCateItemReusableView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeCateItemReusableView.h"
#import "HomeFilterConditionView.h"

@interface HomeCateItemReusableView()
<FilterConditionDelegate>

@property (nonatomic, strong) UILabel *line;
@property (nonatomic, strong) UILabel *bottom;
@property (nonatomic, strong) HomeFilterConditionView *filterView;

@end

@implementation HomeCateItemReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        
        self.line = [YYCreateTools createLabel:nil
                                          font:normalFont(12) textColor:XHLightColor];
        self.line.backgroundColor = XHLightColor;
        [self addSubview:self.line];
        
        [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self);
            make.height.mas_equalTo(1);
        }];
        
        self.filterView = [[HomeFilterConditionView alloc] initWithFrame:self.bounds];
        self.filterView.delegate = self;
        [self addSubview:self.filterView];
        
        [self.filterView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(1);
            make.left.right.bottom.equalTo(self);
        }];
        
        self.filterView.itemTitles = [@[@"综合",@"价格",@"销量"] mutableCopy];
    }
    return self;
    
}


- (void)setIsReset:(BOOL)isReset {
    _isReset = isReset;
    if (_isReset) {
        [self.filterView willItemBecomeSelectAtIndex:0];
    }
}

#pragma mark  - FilterConditionDelegate

- (void)filterSelectPoint:(NSInteger)index
                priceType:(YYPriceFilterType)priceType {
    if (self.itemBlock) {
        self.itemBlock(index, priceType);
    }
}

- (void)setLineHeight:(CGFloat)lineHeight {
    _lineHeight = lineHeight;
    @weakify(self);
    [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.right.top.equalTo(self);
        if (self.lineHeight > 205) {
            make.height.mas_equalTo(0);
        }
        else {
            make.height.mas_equalTo(1);
        }
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
