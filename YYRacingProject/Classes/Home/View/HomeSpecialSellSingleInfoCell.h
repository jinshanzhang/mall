//
//  HomeSpecialSellSingleInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@class HomeListStructInfoModel;
NS_ASSUME_NONNULL_BEGIN
@protocol HomeSpecialSellSingleInfoCellDelegate <NSObject>

@optional
- (void)homeSpecialSellSingleCellCarrayModel:(HomeListStructInfoModel *)carrayModel
                          isShare:(BOOL)isShare;

@end

@interface HomeSpecialSellSingleInfoCell : YYBaseTableViewCell

@property (nonatomic, weak) id <HomeSpecialSellSingleInfoCellDelegate> delegate;
@property (nonatomic, strong) HomeListStructInfoModel  *infoModel;

@end

NS_ASSUME_NONNULL_END
