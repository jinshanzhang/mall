//
//  HomeMenuTitleView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol HomeMenuTitleViewDelegate <NSObject>

@optional
- (void)homeMenuClickSelectItemAtIndex:(NSInteger)selectIndex;

@end

@interface HomeMenuTitleView : UIView

@property (nonatomic, assign) BOOL            isRefresh;
@property (nonatomic, weak)   id  <HomeMenuTitleViewDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *titleArrays;
@property (nonatomic, assign) NSInteger       selectIndex;  //切换选中的位置
@property (nonatomic, assign) BOOL            isCenter;     //是否居中
@property (nonatomic, copy) void (^updateRefreshBlock)(BOOL refreshStatus);

@end

NS_ASSUME_NONNULL_END
