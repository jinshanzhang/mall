//
//  ActivityCellBasicView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "ActivityCellBasicView.h"

@implementation ActivityCellBasicView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.title = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
        [self addSubview:self.title];
        
        self.activityArrowImage = [YYCreateTools createImageView:@"new_expand_more_icon"
                                                               viewModel:-1];
        [self addSubview:self.activityArrowImage];
    }
    return self;

}

-(void)layoutSubviews{
    [super layoutSubviews];

    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(-10);
        make.centerY.equalTo(self);
    }];
    
    [self.activityArrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(5, 10));
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
