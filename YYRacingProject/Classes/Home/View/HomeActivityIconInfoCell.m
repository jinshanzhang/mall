//
//  HomeActivityIconInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeActivityIconInfoCell.h"

@interface HomeActivityIconInfoCell()

@property (nonatomic, strong)  UIImageView *cateImageView;
@property (nonatomic, strong)  UILabel     *cateLabel;
@property (nonatomic, strong)  UIView      *backGroundView;
@property (nonatomic, strong)  UIView      *lineView;

@end

@implementation HomeActivityIconInfoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = XHClearColor;
        self.backGroundView = [YYCreateTools createView:XHClearColor];
        [self.contentView addSubview:self.backGroundView];

        [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.right.mas_equalTo(0);
        }];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.cateImageView = [[YYAnimatedImageView alloc] init];
    self.cateImageView.userInteractionEnabled = YES;
    [self addSubview:self.cateImageView];
    [self.contentView addSubview:self.cateImageView];

    self.cateLabel = [YYCreateTools createLabel:nil
                                           font:normalFont(13)
                                      textColor:XHBlackColor];
    self.cateLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.cateLabel];
    
    [self.cateImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(14));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(32), kSizeScale(32)));
        make.centerX.equalTo(self.contentView);
    }];
    [self.cateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.cateImageView);
        make.top.equalTo(self.cateImageView.mas_bottom).offset(kSizeScale(6));
    }];

    self.lineView = [YYCreateTools createView:XHNEwLineColor];
    self.lineView.hidden = YES;
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)setExtendParams:(NSMutableDictionary *)extendParams {
    _extendParams = extendParams;
    if (kValidDictionary(_extendParams)) {
        if ([_extendParams objectForKey:@"isStore"]) {
            BOOL isStore = [[_extendParams objectForKey:@"isStore"] boolValue];
            if (!isStore) {
                self.lineView.hidden = YES;
                [self.cateImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(12);
                    make.size.mas_equalTo(CGSizeMake(kSizeScale(41), kSizeScale(41)));
                    make.centerX.equalTo(self.contentView);
                }];
                [self.cateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self.cateImageView);
                    make.top.equalTo(self.cateImageView.mas_bottom).offset(kSizeScale(6));
                }];
                self.cateLabel.font = normalFont(12);
            }
            else {
                self.lineView.hidden = NO;
                [self.cateImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(kSizeScale(16));
                    make.size.mas_equalTo(CGSizeMake(kSizeScale(32), kSizeScale(32)));
                    make.centerX.equalTo(self.contentView);
                }];
                [self.cateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self.cateImageView);
                    make.top.equalTo(self.cateImageView.mas_bottom).offset(kSizeScale(6));
                    make.bottom.mas_equalTo(-kSizeScale(16));
                }];
                /*
                [self.cateImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(20);
                    make.size.mas_equalTo(CGSizeMake(kSizeScale(32), kSizeScale(32)));
                    make.centerX.equalTo(self.contentView);
                }];
                [self.cateLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerX.equalTo(self.cateImageView);
                    make.top.equalTo(self.cateImageView.mas_bottom).offset(kSizeScale(6));
                }];*/
            }
        }
    }
}

- (void)setFontColor:(NSString *)fontColor {
    _fontColor = fontColor;
    if (!kValidString(_fontColor)) {
        self.cateLabel.textColor = XHBlackColor;
    }
    else {
        self.cateLabel.textColor = [NSString colorWithHexString:_fontColor];
    }
}

- (void)setActivityView:(NSString *)iconContent
                iconUrl:(NSString *)iconUrl {
    self.cateLabel.text = iconContent;
    [self.cateImageView setImageWithURL:[NSURL URLWithString:iconUrl]
                            placeholder:[UIImage imageNamed:@"home_hot_good_placehold_icon"]];
}
@end
