//
//  SearchResultEmptyView.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "SearchResultEmptyView.h"
#import "LLTagViewUtils.h"
#import "YYSearchFeedbackAPI.h"

@implementation SearchResultEmptyView
{
    LLTagViewUtils *tagViewUtils;
    UIButton       *btn;
}


- (instancetype)init {
    self = [super init];
    if (self) {
        [self creatUI];
    }
    return self;
}

-(void)creatUI{

    @weakify(self);
    self.leftTitleLabel = [YYCreateTools createLabel:@"当前搜索无商品，试试以下关键词:" font:normalFont(13) textColor:XHBlackColor];
    self.leftTitleLabel.frame = CGRectMake(20, 15, 300, 10);
    self.leftTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.leftTitleLabel];
    
    self.coverView = [YYCreateTools createView:XHClearColor];
    [self addSubview:self.coverView];
    self.coverView.frame = CGRectMake(15, 40, kScreenW-30, 30);
    
    UIImageView *emptyIMG = [YYCreateTools createImageView:@"empty_search_icon" viewModel:-1];
    [self addSubview:emptyIMG];
    emptyIMG.frame = CGRectMake(0, 155, kSizeScale(164), kSizeScale(163));
    emptyIMG.centerX  = kScreenW/2;
    
    UILabel *title = [YYCreateTools createLabel:@"暂时没有该商品，可以求上架哦~" font:normalFont(12) textColor:XHBlackLitColor];
    [self addSubview:title];
    title.frame = CGRectMake(0, emptyIMG.bottom+10, 200, 20);
    title.centerX  = kScreenW/2;
    title.textAlignment = NSTextAlignmentCenter;
    
    btn = [YYCreateTools createBtn:@"求上架" font:normalFont(14) textColor:XHWhiteColor];
    [self addSubview:btn];
    [btn setBackgroundImage:[UIImage imageWithColor:XHRedColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageWithColor:XHGrayColor] forState:UIControlStateDisabled];
    btn.frame = CGRectMake(0, title.bottom+20, 92, 30);
    btn.centerX  = kScreenW/2;
    btn.v_cornerRadius = 4;
    
    btn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:self.keyWord forKey:@"word"];
        
        YYSearchFeedbackAPI *navAPI = [[YYSearchFeedbackAPI alloc] initHotSearchRequest:dic];
        [navAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                [YYCommonTools showTipMessage:@"收到您的反馈，我们会尽快上架哦~"];
                self->btn.enabled = NO;
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    };
}

#pragma mark 绘制tag
//复制数据
- (void)setupCategoryViewAndData
{
    tagViewUtils = [LLTagViewUtils new];
    
    [tagViewUtils setupCommonTagsInView:self.coverView tagTexts:self.titleArr margin:10 tagHeight:30 tagState:self.keyArr color:XHWhiteColor];
    tagViewUtils.commonTagStyle = ZYHTCommonTagStyleARCBorderTag;
    [tagViewUtils tagLabelOnClick:^(UITapGestureRecognizer *tapGes, UILabel *tagLabel, NSInteger index) {
        if (self.ClickBlock) {
            self.ClickBlock(tagLabel.text,index);
        }
    }];
    
}

-(void)setKeyArr:(NSMutableArray *)keyArr{
    _keyArr = keyArr;
    self.titleArr = [NSMutableArray array];
    for (KeyWordModel *model in keyArr) {
        [self.titleArr addObject:model.word];
    };
    [self setupCategoryViewAndData];
}

-(void)setKeyWord:(NSString *)keyWord{
    _keyWord = keyWord;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
@end
