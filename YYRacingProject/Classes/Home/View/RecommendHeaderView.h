//
//  RecommendHeaderView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, RecommendShowStyle) {
    RecommendShowDetailStyle = 0,
    RecommendShowSuperHotStyle
};

@interface RecommendHeaderView : UIView

@property (nonatomic, assign) RecommendShowStyle showStyle;
@property (nonatomic, strong) UILabel  *titleLabel;

@end

NS_ASSUME_NONNULL_END
