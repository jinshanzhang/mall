//
//  HomeMenuTitleView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HomeMenuTitleView.h"
#import "HomeTimeAxleInfoModel.h"

@interface HomemMenuItemView : UIView

@property (nonatomic, assign) BOOL      isSelect;
@property (nonatomic, assign) BOOL      isTomPre;   //是否明日预告
@property (nonatomic, strong) HomeTimeAxleInfoModel *itemModel;

@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *axleStatusLabel;
@property (nonatomic, strong) UIView  *slideView;

@end

@implementation HomemMenuItemView

- (void)setIsSelect:(BOOL)isSelect {
    _isSelect = isSelect;
    if (!_isSelect) {
        self.timeLabel.textColor = HexRGB(0x333333);
        self.axleStatusLabel.textColor = HexRGB(0x999999);
        self.slideView.backgroundColor = [UIColor whiteColor];
    }
    else {
        self.timeLabel.textColor = self.axleStatusLabel.textColor = XHLoginColor;
        self.slideView.backgroundColor = XHLoginColor;
    }
}

- (void)setItemModel:(HomeTimeAxleInfoModel *)itemModel {
    _itemModel = itemModel;
    if (_itemModel) {
        BOOL tomPre = ([_itemModel.timePoint intValue] != 0)?NO:YES;
        self.timeLabel.text = (tomPre==YES?_itemModel.instruction:_itemModel.timePointAlias);
        self.axleStatusLabel.text = _itemModel.instruction;
        self.isTomPre = tomPre;
    }
}

- (void)setIsTomPre:(BOOL)isTomPre {
    _isTomPre = isTomPre;
    if (_isTomPre) {
        self.timeLabel.font = boldFont(13);
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
        }];
        self.axleStatusLabel.text = @"";
    }
    else {
        self.timeLabel.font = boldFont(16);
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.top.mas_equalTo(kSizeScale(8));
        }];
    }
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = HexRGB(0xffffff);
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.font = boldFont(16);
        self.timeLabel.textColor = HexRGB(0x333333);
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.timeLabel];
    
        self.axleStatusLabel = [[UILabel alloc] init];
        self.axleStatusLabel.font = midFont(10);
        self.axleStatusLabel.textColor = HexRGB(0x999999);
        self.axleStatusLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.axleStatusLabel];
        
        self.slideView = [[UIView alloc] init];
        [self addSubview:self.slideView];
        
        self.isSelect = NO;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero)) {
        return;
    }
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.mas_equalTo(kSizeScale(8));
    }];
    [self.axleStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.timeLabel.mas_bottom);
    }];
    [self.slideView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(45), kSizeScale(2)));
    }];
}

@end


#define  kItemTag  120
@interface HomeMenuTitleView ()

@property (nonatomic, strong) UIScrollView   *scrollView;
@property (nonatomic, strong) NSMutableArray *itemArrays;  //存储所有显示在scrollView 上的item
@property (nonatomic, assign) NSInteger       currentIndex;//当前item位置

@end


@implementation HomeMenuTitleView

#pragma mark - Setter and Getter
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.scrollsToTop = NO;
        [self addSubview:_scrollView];
    }
    return _scrollView;
}

- (void)setTitleArrays:(NSMutableArray *)titleArrays {
    _titleArrays = titleArrays;
    if (_titleArrays.count <= 0) {
        self.backgroundColor = XHLightColor;
        return;
    }
    // 每次刷新默认的位置
    NSInteger tempIndex = -1;
    [self.itemArrays makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.itemArrays removeAllObjects];
    for (int i = 0; i < titleArrays.count; i ++) {
        HomeTimeAxleInfoModel *timeModel = [titleArrays objectAtIndex:i];
        HomemMenuItemView *itemView = [[HomemMenuItemView alloc] initWithFrame:CGRectZero];
        itemView.tag = kItemTag + i;
        itemView.itemModel = timeModel;
        if ([timeModel.chooseFlag integerValue] == 1) {
            itemView.isSelect = YES;
            tempIndex = i;
        }
        else {
            itemView.isSelect = NO;
        }
        [itemView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClickGesture:)]];
        [self.scrollView addSubview:itemView];
        [self.itemArrays addObject:itemView];
    }
    [self setNeedsLayout];
    [self layoutIfNeeded];
    if (self.isRefresh) {
        // 如果YES
        self.currentIndex = [self traverseSelectItemAtIndex];
    }
    else {
        self.currentIndex = self.currentIndex;
    }
    
    if (self.updateRefreshBlock) {
        self.updateRefreshBlock(NO);
    }
}

- (void)setCurrentIndex:(NSInteger)currentIndex {
    _currentIndex = currentIndex;
    if (_currentIndex < 0)
        return;
    [self scrollSelectItemCenter:_currentIndex animated:YES];
}

- (void)setSelectIndex:(NSInteger)selectIndex {
    _selectIndex = selectIndex;
    self.currentIndex = _selectIndex;
}

- (void)setIsCenter:(BOOL)isCenter {
    _isCenter = isCenter;
}

#pragma mark - Life cycle
-  (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.isCenter = YES;
        self.currentIndex = -1;
        self.backgroundColor = XHWhiteColor;
        self.itemArrays = [NSMutableArray array];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.scrollView.frame = self.bounds;
    if (self.itemArrays.count <= 0) {
        return;
    }
    CGFloat itemWidth = 0.0;
    CGFloat itemHeight = CGRectGetHeight(self.bounds);
    __block CGFloat totalItemWidth = 0.0;
    itemWidth = kScreenW/5.0;
    [self.itemArrays enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomemMenuItemView *item = (HomemMenuItemView *)obj;
        item.frame = CGRectMake(totalItemWidth, 0, itemWidth, itemHeight);
        totalItemWidth = totalItemWidth + itemWidth;
    }];
    self.scrollView.contentSize = CGSizeMake(totalItemWidth, CGRectGetHeight(self.scrollView.bounds));
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    if (![self pointInside:point withEvent:event]) {
        for (UIView *view in self.subviews) {
            NSInteger viewTag = view.tag;
            if (viewTag == 888) {// 分享场次 view frame 超出父视图事件链处理。
                CGPoint viewPoint = [self convertPoint:point toView:view];
                viewPoint = CGPointMake(viewPoint.x + view.frame.origin.x, viewPoint.y + view.frame.origin.y);
                if ([view isKindOfClass:[UIView class]] && CGRectContainsPoint(view.frame, viewPoint)) {
                    return view;
                }
            }
        }
    }
    return [super hitTest:point withEvent:event];
}

#pragma mark - Private
- (NSInteger)traverseSelectItemAtIndex {
    __block NSInteger index = -1;
    [self.itemArrays enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomemMenuItemView *itemView = (HomemMenuItemView *)obj;
        if (itemView.isSelect) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

- (void)scrollSelectItemCenter:(NSInteger)selectIndex
                      animated:(BOOL)animated {
    if (self.itemArrays.count <= 0) {
        return;
    }
    __block NSInteger currentIndex = selectIndex;
    [self.itemArrays enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomemMenuItemView *item = (HomemMenuItemView *)obj;
        if (currentIndex == idx) {
            item.isSelect = YES;
        }
        else {
            item.isSelect = NO;
        }
    }];
    HomemMenuItemView *selectItem = self.itemArrays[selectIndex];
    CGRect centerRect = CGRectZero;
    if (self.isCenter) {
        centerRect = CGRectMake(selectItem.center.x - CGRectGetWidth(self.scrollView.bounds)/2, 0, CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds));
    }
    else {
        centerRect = CGRectMake(selectItem.origin.x - selectItem.width, 0, CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds));
    }
    [self.scrollView scrollRectToVisible:centerRect animated:animated];
}

#pragma mark - Event
- (void)itemClickGesture:(UITapGestureRecognizer *)tapGesture {
    HomemMenuItemView *itemView = (HomemMenuItemView *)tapGesture.view;
    NSInteger currentTag = itemView.tag - kItemTag;
    self.currentIndex = currentTag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeMenuClickSelectItemAtIndex:)]) {
        [self.delegate homeMenuClickSelectItemAtIndex:_currentIndex];
    }
}

@end
