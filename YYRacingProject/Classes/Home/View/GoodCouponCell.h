//
//  GoodCouponCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "CouponListInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodCouponCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel      *couponPriceLabel;  // 优惠券折扣金额
@property (nonatomic, strong) UILabel      *conditionPriceLabel;// 优惠券使用范围

@property (nonatomic, strong) UILabel      *couponTitleLabel;
@property (nonatomic, strong) UILabel      *couponTimeLabel;


@property (nonatomic, strong) UIImageView  *dotter_ImageView;
@property (nonatomic, strong) UIImageView  *tag_ImageView;
@property (strong, nonatomic) UIButton     *receiveBtn;
@property (strong, nonatomic) UIButton     *activityBtn;

@property (nonatomic, strong) UILabel      *couponNumberLabel;


@property (nonatomic, strong) CouponListInfoModel  *model;

@property (nonatomic, copy) void(^receiveBlock)(NSString *couponKey);


@end

NS_ASSUME_NONNULL_END
