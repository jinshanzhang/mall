//
//  SuperBrandLumpInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/19.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "SuperBrandLumpInfoCell.h"

#import "YYMultiItemScrollModel.h"
@interface SuperBrandLumpInfoCell ()

@property (nonatomic, strong) UIImageView *backColorView;
@property (nonatomic, strong) UIImageView *brandImageView;
@property (nonatomic, strong) UILabel     *goodPriceLabel;

@end


@implementation SuperBrandLumpInfoCell

- (void)setViewModel:(id<YYMultiItemScrollViewProtocol>)viewModel {
    [super setViewModel:viewModel];

    UIColor *reservePriceColor = XHBlackColor;
    UIColor *originPriceColor = XHRedColor;
    NSMutableAttributedString *reservePrice = nil;
    NSMutableAttributedString *originPrice = nil;

    [self.brandImageView sd_setImageWithURL:[NSURL URLWithString:viewModel.imageUrl]];
    if (kIsFan) {
        reservePriceColor = ([viewModel.saleStatus integerValue] == 2 ? HexRGB(0x11BC56) : XHRedColor);
    }
    originPriceColor = ([viewModel.saleStatus integerValue] == 2 ? HexRGB(0x11BC56) : XHRedColor);
    
    reservePrice = [[NSMutableAttributedString alloc] init];
    reservePrice = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(10) symbolTextColor:reservePriceColor wordSpace:2 price:viewModel.nowPrice priceFont:boldFont(14) priceTextColor: reservePriceColor symbolOffsetY:0];
    if (kIsFan) {
        originPrice = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",viewModel.originPrice]];
        [originPrice yy_setAttributes:XHBlackLitColor
                                 font:midFont(12)
                              content:originPrice
                            alignment:NSTextAlignmentCenter];
        [originPrice yy_setAttributeOfDeleteLine:NSMakeRange(0, originPrice.length)];
    }
    else {
        originPrice = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(10) symbolTextColor:originPriceColor wordSpace:2 price:viewModel.commission priceFont:boldFont(12) priceTextColor:originPriceColor symbolOffsetY:1.2];
    }
    [reservePrice appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@" "]];
    [reservePrice appendAttributedString:originPrice];
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    [reservePrice yy_setAttributes:attributes
                           content:reservePrice
                         alignment:NSTextAlignmentCenter];
    self.goodPriceLabel.attributedText = reservePrice;
}

- (void)createUI {
    [super createUI];
    self.backgroundColor = self.contentView.backgroundColor = XHClearColor;
    
    self.backColorView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.contentView addSubview:self.backColorView];
    
    self.brandImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self.contentView addSubview:self.brandImageView];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(12)
                                           textColor:XHBlackColor];
    [self.contentView addSubview:self.goodPriceLabel];
    
    [self.backColorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.brandImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(self.width);
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.brandImageView);
        make.bottom.equalTo(self.mas_bottom).offset(-kSizeScale(5));
    }];

    [self.backColorView zy_cornerRadiusAdvance:kSizeScale(5) rectCornerType:UIRectCornerAllCorners];
    [self.brandImageView zy_cornerRadiusAdvance:kSizeScale(5) rectCornerType:UIRectCornerTopLeft|UIRectCornerTopRight];
    
    self.backColorView.image = [UIImage imageWithColor:XHWhiteColor];
}

@end
