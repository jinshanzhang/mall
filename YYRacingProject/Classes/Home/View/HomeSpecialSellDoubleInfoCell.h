//
//  HomeSpecialSellDoubleInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/11.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
@class HomeListStructInfoModel;
NS_ASSUME_NONNULL_BEGIN
@protocol HomeSpecialSellDoubleInfoCellDelegate <NSObject>

@optional
- (void)homeSpecialSellDoubleCellCarrayModel:(HomeListStructInfoModel *)carrayModel
                                     isShare:(BOOL)isShare;

@end

@interface HomeSpecialSellDoubleInfoCell : YYBaseTableViewCell

@property (nonatomic, weak) id <HomeSpecialSellDoubleInfoCellDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *itemModels;

@end

NS_ASSUME_NONNULL_END
