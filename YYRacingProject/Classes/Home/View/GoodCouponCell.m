//
//  GoodCouponCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "GoodCouponCell.h"

@implementation GoodCouponCell


-(void)createUI{
    
    self.contentView.backgroundColor = XHMainLightColor;
    
    UIImageView *bg_view_left = [YYCreateTools createImageView:@"goodCoupon_left" viewModel:-1];
    [self addSubview:bg_view_left];
    
    UIImageView *bg_view_right = [YYCreateTools createImageView:@"goodCoupon_right" viewModel:-1];
    [self addSubview:bg_view_right];
    
    [bg_view_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(6));
        make.bottom.mas_equalTo(0);
    }];
    
    [bg_view_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bg_view_left.mas_right);
        make.right.mas_equalTo(-kSizeScale(6));
        make.top.bottom.equalTo(bg_view_left);
        make.width.mas_equalTo(kSizeScale(90));
    }];
    
    
    self.couponPriceLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(10)
                                             textColor:XHRedColor];
    self.couponPriceLabel.textAlignment = NSTextAlignmentCenter;
    [bg_view_left addSubview:self.couponPriceLabel];
    
    
    
    self.dotter_ImageView = [YYCreateTools createImageView:@"coupon_dotted_icon"
                                                 viewModel:-1];
    [bg_view_left addSubview:self.dotter_ImageView];
    
    
    self.couponPriceLabel.attributedText = [YYCommonTools containSpecialSymbolHandler:@"¥"
                                                                           symbolFont:normalFont(14) symbolTextColor:XHRedColor
                                                                            wordSpace:2
                                                                      symbolIsAddLeft:YES
                                                                                price:@"99"
                                                                            priceFont:boldFont(30) priceTextColor:XHRedColor symbolOffsetY:0];
    
    self.conditionPriceLabel = [YYCreateTools createLabel:nil
                                                     font:normalFont(12)
                                                textColor:XHRedColor];
    self.conditionPriceLabel.textAlignment = NSTextAlignmentCenter;
    [bg_view_left addSubview:self.conditionPriceLabel];
    
    
    [self.dotter_ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(90));
        make.centerY.equalTo(bg_view_left);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(0.5), kSizeScale(42)));
    }];
    
    [self.couponPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(5));
        make.right.equalTo(self.dotter_ImageView.mas_left).offset(-kSizeScale(5)).priorityHigh();
        make.top.equalTo(self.dotter_ImageView.mas_top).mas_offset(kSizeScale(-7));
    }];
    [self.conditionPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.dotter_ImageView.mas_left).offset(-kSizeScale(5));
        make.left.mas_equalTo(kSizeScale(5));
        make.bottom.equalTo(self.dotter_ImageView.mas_bottom).mas_offset(kSizeScale(2));
    }];
    
    self.couponTitleLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(14)
                                             textColor:XHBlackColor];
    [bg_view_left addSubview:self.couponTitleLabel];
    
    [self.couponTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dotter_ImageView.mas_right).offset(kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(25));
        make.right.mas_equalTo(-10);
    }];
    
    self.couponTimeLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(11)
                                            textColor:XHBlackLitColor];
    [bg_view_left addSubview:self.couponTimeLabel];
    
    [self.couponTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.couponTitleLabel);
        make.top.mas_equalTo(self.couponTitleLabel.mas_bottom).mas_offset(kSizeScale(8));
        make.bottom.mas_equalTo(-(kSizeScale(25)));
    }];
    
    self.activityBtn = [YYCreateTools createBtn:@"逛会场 >" font:normalFont(9) textColor:XHRedColor];
    self.activityBtn.backgroundColor = HexRGB(0xFFE9ED);
    self.activityBtn.v_cornerRadius = 2;
    self.activityBtn.hidden = YES;
    [bg_view_left addSubview:self.activityBtn];
    
    
    self.tag_ImageView = [YYCreateTools createImageView:@"" viewModel:-1];
    [bg_view_left addSubview:self.tag_ImageView];
    
    [self.tag_ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(5);
    }];
    
    self.receiveBtn = [YYCreateTools createBtn:@"立即领取" font:normalFont(12) textColor:XHWhiteColor];
    self.receiveBtn.v_cornerRadius = kSizeScale(14);
    self.receiveBtn.backgroundColor = XHLoginColor;
    [bg_view_right addSubview:self.receiveBtn];

    @weakify(self);
    self.receiveBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        if (self.receiveBlock) {
            self.receiveBlock(self.model.couponKey);
        }
    };
    
    [self.receiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerX.equalTo(bg_view_right);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(64), kSizeScale(27)));
    }];
    
    self.couponNumberLabel = [YYCreateTools createLabel:@"" font:normalFont(9) textColor:XHBlackLitColor];
    [bg_view_right addSubview:self.couponNumberLabel];
    
    [self.couponNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bg_view_right);
        make.top.mas_equalTo(self.receiveBtn.mas_bottom).mas_equalTo(5);
    }];
}

-(void)setModel:(CouponListInfoModel *)model{
    _model = model;
    if (kValidString(model.denomination)) {
        self.couponPriceLabel.attributedText = [YYCommonTools containSpecialSymbolHandler:([model.type integerValue]==1?@"¥":@"折")
                                                                               symbolFont:normalFont(14) symbolTextColor:XHRedColor
                                                                                wordSpace:2
                                                                          symbolIsAddLeft:([model.type integerValue]==1?YES:NO)
                                                                                    price:model.denomination
                                                                                priceFont:boldFont(30) priceTextColor:XHRedColor
                                                                            symbolOffsetY:0];
    }
    self.couponTitleLabel.text = model.couponName;
    self.conditionPriceLabel.text = model.denominationCondition;
    self.couponTimeLabel.text = model.effectTime;
    
    if (model.roleFlag == 1) {
        [self.tag_ImageView setImage:[UIImage imageNamed:@"goodcoupon_shop"]];
    }else if (model.roleFlag == 2){
        [self.tag_ImageView setImage:[UIImage imageNamed:@"goodcoupon_fans"]];
    }
    
    NSMutableAttributedString *titleWord = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"持有%ld张",(long)_model.holdNum]];
    NSString *length = [NSString stringWithFormat:@"%ld",_model.holdNum];
    [titleWord addAttribute:NSForegroundColorAttributeName value:XHLoginColor range:NSMakeRange(2,length.length)];
    
    if (model.limitStatus == 0) {
        [self.receiveBtn setTitle:@"立即领取" forState:UIControlStateNormal];
    }
    if (model.limitStatus == 1 ) {
        [self.receiveBtn setTitle:@"继续领取" forState:UIControlStateNormal];
        [self.receiveBtn setTitleColor:XHWhiteColor forState:UIControlStateNormal];
        [self.receiveBtn setBackgroundColor:XHLoginColor];

        if (_model.holdNum>0) {
            _couponNumberLabel.attributedText = titleWord;
        }
    }if (model.limitStatus == 2 && _model.holdNum ==0) {
        [self.receiveBtn setTitle:@"已用完" forState:UIControlStateNormal];
        [self.receiveBtn setTitleColor:XHWhiteColor forState:UIControlStateNormal];
        [self.receiveBtn setBackgroundColor:XHBlackLitColor];
    }
    if (model.limitStatus == 3) {
        [self.receiveBtn setTitle:@"已领取" forState:UIControlStateNormal];
        [self.receiveBtn setTitleColor:XHWhiteColor forState:UIControlStateNormal];
        [self.receiveBtn setBackgroundColor:XHBlackLitColor];
        _couponNumberLabel.attributedText = titleWord;
    }

    if ([[model.link objectForKey:@"linkType"] floatValue] == 2&& [model.link objectForKey:@"uri"]) {
        self.activityBtn.hidden = NO;
        [self.couponTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.dotter_ImageView.mas_right).offset(kSizeScale(12));
            make.top.mas_equalTo(kSizeScale(25));
            make.height.mas_equalTo(20);
            if (self.couponTitleLabel.text.length>7) {
                make.width.mas_equalTo(kSizeScale(120));
            }
        }];

        [self.activityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.couponTitleLabel.mas_top).offset(kSizeScale(2));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(42), kSizeScale(15)));
            make.left.mas_equalTo(self.couponTitleLabel.mas_right).mas_offset(3);
        }];
    }
    self.activityBtn.actionBlock = ^(UIButton *sender) {
        NSDictionary *dict = model.link;
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[dict objectForKey:@"linkType"]?:@"" forKey:@"linkType"];
        [params setObject:[dict objectForKey:@"uri"]?:@"" forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
    };
}


@end
