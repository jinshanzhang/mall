//
//  YY_MainOtherSortViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_MainOtherSortViewController.h"

#import "HomeResourcesInfoModel.h"
#import "HomeListStructInfoModel.h"

#import "HomeHeaderInfoView.h"

#import "HomeBrandHotInfoCell.h"
#import "HomeSpecailSellTitleInfoCell.h"
#import "HomeSpecialSellSingleInfoCell.h"
#import "HomeSpecialSellDoubleInfoCell.h"

#import "YYHomeSubListGoodsInfoRequest.h"
#import "YYHomeSubListGoodPageInfoRequest.h"

@interface YY_MainOtherSortViewController ()
<HomeHeaderViewDelegate,
HomeBrandHotInfoCellDelegate,
HomeSpecialSellSingleInfoCellDelegate,
HomeSpecialSellDoubleInfoCellDelegate>

@property (nonatomic, strong) HomeHeaderInfoView *headerView;

@property (nonatomic, strong) NSString *uri;
@property (nonatomic, assign) BOOL isAddTodayTitle;
@property (nonatomic, assign) BOOL isAddMoreTitle;
@property (nonatomic, assign) BOOL isExistBanner; //是否存在banner
@property (nonatomic, assign) NSInteger pageIndex;
@property (nonatomic, strong) NSMutableArray *niches; //资源位
@property (nonatomic, strong) NSMutableArray *lastSingleArray; //上次留下的单

@end

@implementation YY_MainOtherSortViewController

#pragma mark - Setter

- (HomeHeaderInfoView *)headerView {
    if (!_headerView) {
        HomeHeaderInfoView *extractedExpr = [HomeHeaderInfoView alloc];
        _headerView = [extractedExpr initWithFrame:CGRectMake(0, 0, kScreenW, 150)];
        _headerView.delegate = self;
    }
    return _headerView;
}

#pragma mark - Life  cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.changeScrollTop = YES;
        self.NoDataType = YYEmptyViewAlxeNoDataType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isAddTodayTitle = self.isAddMoreTitle = NO;
    self.lastSingleArray = [NSMutableArray array];
    self.view.backgroundColor = self.m_tableView.backgroundColor = XHClearColor;
    [self xh_hideNavigation:YES];
    if (self.parameter) {
        if ([self.parameter objectForKey:@"uri"]) {
            self.uri = [self.parameter objectForKey:@"uri"];
        }
        if ([self.parameter objectForKey:@"pageIndex"]) {
            self.pageIndex = [[self.parameter objectForKey:@"pageIndex"] integerValue];
        }
    }
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        if (@available(iOS 11, *)) {
            make.top.offset(self.view.safeAreaInsets.top);
        }
        else {
            make.top.equalTo(self.view);
        }
    }];

    Class currentCls = [HomeBrandHotInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSpecialSellSingleInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSpecialSellDoubleInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [HomeSpecailSellTitleInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    self.m_tableView.tableHeaderView = self.headerView;
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.m_tableView);
        make.width.mas_equalTo(kScreenW);
    }];
    [self loadPullDownPage];
    // Do any additional setup after loading the view.
}

- (void)loadPullDownPage {
    [self.lastSingleArray removeAllObjects];
    self.isAddTodayTitle = self.isAddMoreTitle = NO;
    [self headerSourcePointHandler];
    kPostNotification(MainHotScroll, nil);
    [super loadPullDownPage];
}

- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    // 上拉加载
    if (kValidArray(self.dataIds)) {
        [params removeAllObjects];
        if ((self.totalLength+self.pageLength) > self.dataIds.count) {
            self.pageLength = self.dataIds.count - self.totalLength;
        }
        [params setObject:[[self.dataIds subarrayWithRange:NSMakeRange(self.totalLength, self.pageLength)] componentsJoinedByString:@","] forKey:@"uris"];
        if (kValidString(self.uri)) {
            [params setObject:self.uri forKey:@"uri"];
        }
        YYHomeSubListGoodPageInfoRequest *pageAPI = [[YYHomeSubListGoodPageInfoRequest alloc] initHomeSubListGoodPageInfoRequest:params];
        return pageAPI;
    }
    else {
        self.totalLength = 0;
        self.pageLength = 0;
        if (kValidString(self.uri)) {
            [params setObject:self.uri forKey:@"uri"];
        }
        YYHomeSubListGoodsInfoRequest *listAPI = [[YYHomeSubListGoodsInfoRequest alloc] initHomeSubListGoodsInfoRequest:params];
        return listAPI;
    }
    return nil;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        NSMutableArray *tempArr, *tempItems;
        if ([responseDic objectForKey:@"itemIds"]) {
            //处理所有的ids
            NSArray *itemIds = [responseDic objectForKey:@"itemIds"];
            if (kValidArray(itemIds)) {
                tempArr = [itemIds mutableCopy];
            }
            if (kValidArray(tempArr)) {
                self.dataIds = [tempArr mutableCopy];
            }
        }
        if ([responseDic objectForKey:@"items"]) {
            NSArray *items = [NSArray modelArrayWithClass:[HomeListStructInfoModel class] json:[responseDic objectForKey:@"items"]];
            if (!kValidArray(items)) {
                return nil;
            }
            tempItems = [items mutableCopy];
        }
        // 第一次分页需要的特殊处理。
        if ([responseDic objectForKey:@"firstItemIds"]) {
            NSNumber *pageNumber = [responseDic objectForKey:@"firstItemIds"];
            if (![pageNumber isKindOfClass:[NSNull class]]) {
                self.pageLength = [pageNumber integerValue];
            }
        }
        else {
            self.pageLength = 30;
        }
        self.current ++;
        return [self dataSourcePackage:tempItems];
    }
    return nil;
}

- (BOOL)canLoadMore {
    return self.dataIds.count > self.totalLength;
}

#pragma mark - Private
/** 头部资源位处理**/
- (void)headerSourcePointHandler {
    self.niches = [NSMutableArray array];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(self.uri)) {
        [params setObject:self.uri forKey:@"uri"];
    }
    [kWholeConfig homeSourcePointRequest:params
                                 success:^(NSDictionary *responDict) {
                                     if (kValidDictionary(responDict)) {
                                         if ([responDict objectForKey:@"niches"]) {
                                             NSArray *tempNiches = [NSArray modelArrayWithClass:[HomeResourcesInfoModel class] json:[responDict objectForKey:@"niches"]];
                                             if (kValidArray(tempNiches)) {
                                                 self.niches = [tempNiches mutableCopy];
                                             }
                                             __block BOOL tempExistBanner = NO;
                                             [self.niches enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                                 HomeResourcesInfoModel *infoModel = (HomeResourcesInfoModel *)obj;
                                                 if (infoModel.type == 1 && infoModel) {
                                                     tempExistBanner = YES;
                                                     *stop = YES;
                                                 }
                                             }];
                                             self.isExistBanner = tempExistBanner;
                                             if (self.scrollStopBlock) {
                                                 self.scrollStopBlock(CGPointZero, self.pageIndex, self.isExistBanner);
                                             }
                                             self.headerView.headerSources = self.niches;
                                             [self.m_tableView layoutIfNeeded];
                                             self.m_tableView.tableHeaderView = self.headerView;
                                         }
                                     }
                                 }];
}

/**
 分页数据源封装
 **/
- (NSMutableArray *)dataSourcePackage:(NSMutableArray *)itemArrays {
    __block NSMutableArray *dataSource = [NSMutableArray array];
    __block NSMutableArray *towAtRow = [NSMutableArray array];
    [itemArrays enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        HomeListStructInfoModel *structModel = (HomeListStructInfoModel *)obj;
        NSInteger type = [structModel.type integerValue];
        if ([structModel.type integerValue] == 2) {
            // 增加自定义类型
            HomeListItemGoodInfoModel *emptyModel = [[HomeListItemGoodInfoModel alloc] init];
            emptyModel.scrollType = YYSingleRowScrollEmpty;
            [structModel.items addObject:emptyModel];
        }
        if (type == 0) {
            if (!self.isAddMoreTitle) {
                HomeListStructInfoModel *tempModel = [[HomeListStructInfoModel alloc] init];
                tempModel.type = @(3);
                tempModel.title = @"更多推荐";
                [dataSource addObject:tempModel];
                self.isAddMoreTitle = YES;
            }
            [towAtRow addObject:structModel];
        }
        else {
            if (!self.isAddTodayTitle) {
                HomeListStructInfoModel *tempModel = [[HomeListStructInfoModel alloc] init];
                tempModel.type = @(3);
                tempModel.title = @"今日特卖";
                [dataSource addObject:tempModel];
                self.isAddTodayTitle = YES;
            }
            [dataSource addObject:structModel];
        }
    }];
    if (kValidArray(self.lastSingleArray)) {
        [towAtRow insertObject:[self.lastSingleArray firstObject] atIndex:0];
    }
    if (towAtRow.count % 2 != 0) {
        [self.lastSingleArray addObject:[towAtRow lastObject]];
        [towAtRow removeLastObject];
    }
    if (kValidArray(towAtRow)) {
        [dataSource addObjectsFromArray:[YYCommonTools splitArray:towAtRow withSubSize:2]];
    }
    self.totalLength = self.totalLength + self.pageLength;
    if (self.totalLength >= self.dataIds.count && kValidArray(self.lastSingleArray)) {
        [dataSource addObject:self.lastSingleArray];
    }
    self.pageLength = 30;
    return dataSource;
}

/**
 品牌cell赋值
 **/
- (void)configBrandGoodCell:(HomeBrandHotInfoCell *)cell
                atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    if (indexPath.row < self.listData.count) {
        HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:indexPath.row];
        [cell setBrandInfoModel:itemModel];
    }
}

/**
 特卖cell赋值
 **/
- (void)configSpecialGoodCell:(HomeSpecialSellSingleInfoCell *)cell
                  atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    if (indexPath.row < self.listData.count) {
        HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:indexPath.row];
        [cell setInfoModel:itemModel];
    }
}

/**标题cell赋值**/
- (void)configTitleCell:(HomeSpecailSellTitleInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    if (indexPath.row < self.listData.count) {
        HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:indexPath.row];
        [cell setInfoModel:itemModel];
    }
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        NSInteger row = indexPath.row;
        Class current = [HomeBrandHotInfoCell class];
        if (row < self.listData.count && kValidArray(self.listData)) {
            if ([[self.listData objectAtIndex:row] isKindOfClass:[NSArray class]]) {
                current = [HomeSpecialSellDoubleInfoCell class];
                HomeSpecialSellDoubleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                cell.delegate = self;
                cell.itemModels = [self.listData objectAtIndex:row];
                return cell;
            }
            else {
                HomeListStructInfoModel *structModel = [self.listData objectAtIndex:row];
                if ([structModel.type integerValue] == 2 ) {
                    current = [HomeBrandHotInfoCell class];
                    HomeBrandHotInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                    cell.delegate = self;
                    [self configBrandGoodCell:cell atIndexPath:indexPath];
                    return cell;
                }
                else if ([structModel.type integerValue] == 3) {
                    current = [HomeSpecailSellTitleInfoCell class];
                    HomeSpecailSellTitleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                    [self configTitleCell:cell atIndexPath:indexPath];
                    return cell;
                }
                else {
                    current = [HomeSpecialSellSingleInfoCell class];
                    HomeSpecialSellSingleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                    cell.delegate = self;
                    [self configSpecialGoodCell:cell atIndexPath:indexPath];
                    return cell;
                }
            }
        }
        return cell;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        NSInteger row = indexPath.row;
        if (row < self.listData.count && kValidArray(self.listData)) {
            if ([[self.listData objectAtIndex:row] isKindOfClass:[NSArray class]]) {
                CGFloat width = (kScreenW - kSizeScale(8) - kSizeScale(12)*2)/2.0;
                return kSizeScale(80) + width;
            }
            else {
                HomeListStructInfoModel *structModel = [self.listData objectAtIndex:row];
                if ([structModel.type integerValue] == 2 ) {
                    /*height = [tableView fd_heightForCellWithIdentifier:@"HomeBrandHotInfoCell" configuration:^(id cell) {
                        [self configBrandGoodCell:cell atIndexPath:indexPath];
                    }];*/
                    height = kSizeScale(160) + kSizeScale(130) + kSizeScale(12);
                    return height;
                }
                else if ([structModel.type integerValue] == 3 ) {
                    height = [tableView fd_heightForCellWithIdentifier:@"HomeSpecailSellTitleInfoCell" configuration:^(id cell) {
                        [self configTitleCell:cell atIndexPath:indexPath];
                    }];
                    return height;
                }
                else {
                    /*height = [tableView fd_heightForCellWithIdentifier:@"HomeSpecialSellSingleInfoCell" configuration:^(id cell) {
                        [self configSpecialGoodCell:cell atIndexPath:indexPath];
                    }];*/
                     height = kSizeScale(160) + kSizeScale(100) + kSizeScale(12);
                    return height;
                }
            }
        }
        else {
            return height;
        }
    }
    return height;
}

#pragma mark - HomeSpecialSellSingleInfoCellDelegate
- (void)homeSpecialSellSingleCellCarrayModel:(HomeListStructInfoModel *)carrayModel isShare:(BOOL)isShare {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (isShare) {
        YYCompositeShareInfoModel *shareModel = carrayModel.shareInfo;
        if (kValidDictionary(carrayModel.item.coupon)) {
            NSDictionary *coupon = carrayModel.item.coupon;
            if ([coupon objectForKey:@"discountPrice"]) {
                shareModel.discountPrice = [coupon objectForKey:@"discountPrice"];
            }
            if ([coupon objectForKey:@"maxDenomination"]) {
                shareModel.maxDenomination = [coupon objectForKey:@"maxDenomination"];
            }
        }
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (kValidString(shareModel.commission)) {
            [params setObject:shareModel.commission
                       forKey:@"commission"];
        }
        
        NSMutableDictionary *qrcodeParams = [NSMutableDictionary dictionary];
        if (kValidString(carrayModel.item.url)) {
            [qrcodeParams setObject:carrayModel.item.url forKey:@"id"];
        }
        [kWholeConfig produceGoodQRCodeRequest:qrcodeParams
                                       success:^(NSDictionary *responDict) {
                                           if ([responDict objectForKey:@"goodDetailQRcode"]) {
                                               shareModel.qrcodePictureUrl = [responDict objectForKey:@"goodDetailQRcode"];
                                               [YYCommonTools goodShareOperator:shareModel
                                                                   expandParams:params];
                                           }
                                       }];
    }
    else {
        [params setObject:@(1) forKey:@"linkType"];
        if (kValidString(carrayModel.item.url)) {
            [params setObject:carrayModel.item.url forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:self params:params];
    }
}

#pragma mark - HomeSpecialSellDoubleInfoCellDelegate
- (void)homeSpecialSellDoubleCellCarrayModel:(id<HomeGoodItemProtocol>)carrayModel isShare:(BOOL)isShare {
    [self homeSpecialSellSingleCellCarrayModel:carrayModel isShare:isShare];
}

#pragma mark - HomeBrandHotInfoCellDelegate
- (void)homeBrandHotInfoCell:(HomeBrandHotInfoCell *)cell didSelectAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isDetail = NO;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSIndexPath *currentIndex = [self.m_tableView indexPathForCell:cell];
    HomeListStructInfoModel *itemModel = [self.listData objectAtIndex:currentIndex.row];
    if (indexPath) {
        if (currentIndex.row < self.listData.count) {
            HomeListItemGoodInfoModel *goodModel = [itemModel.items objectAtIndex:indexPath.row];
            if (goodModel.scrollType == YYSingleRowScrollEmpty) {
                // 点击查看更多
                isDetail = NO;
            }
            else {
                // 进商品详情
                isDetail = YES;
                [params setObject:@(1) forKey:@"linkType"];
                if (kValidString(goodModel.url)) {
                    [params setObject:goodModel.url forKey:@"url"];
                }
            }
        }
    }
    if (!isDetail) {
        if ([itemModel.link objectForKey:@"linkType"]) {
            [params setObject:[itemModel.link objectForKey:@"linkType"] forKey:@"linkType"];
        }
        if ([itemModel.link objectForKey:@"uri"]) {
            [params setObject:[itemModel.link objectForKey:@"uri"] forKey:@"url"];
        }
    }
    [YYCommonTools skipMultiCombinePage:self params:params];
}

#pragma mark - HomeHeaderViewDelegate
- (void)headerViewItemDidSelect:(HomeHeaderInfoView *)view sourceModel:(HomeResourceItemInfoModel *)sourceModel {
    if (sourceModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (kValidDictionary(sourceModel.link)) {
            if ([sourceModel.link objectForKey:@"linkType"]) {
                [params setObject:[sourceModel.link objectForKey:@"linkType"] forKey:@"linkType"];
            }
            if ([sourceModel.link objectForKey:@"uri"]) {
                if ([[sourceModel.link objectForKey:@"linkType"] integerValue] == 18) {
                    NSString *content = [sourceModel.link objectForKey:@"uri"];
                    if (kValidString(content)) {
                        content = [content cutStringUseSpecialChar:@"="];
                        [params setObject:content forKey:@"url"];
                    }
                }
                else {
                    [params setObject:[sourceModel.link objectForKey:@"uri"] forKey:@"url"];
                }
            }
            [YYCommonTools skipMultiCombinePage:self params:params];
        }
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    [self judgeScrollDirection:scrollView];
    if (self.navigationGradientBlock) {
        self.navigationGradientBlock(self.isExistBanner, scrollView.contentOffset, self.pageIndex);
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        if (self.scrollStopBlock) {
            self.scrollStopBlock(scrollView.contentOffset, self.pageIndex, self.isExistBanner);
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 快速滑动，慢慢停下后调用
    if (self.scrollStopBlock) {
        self.scrollStopBlock(scrollView.contentOffset, self.pageIndex, self.isExistBanner);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
