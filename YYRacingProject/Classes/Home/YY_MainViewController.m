//
//  YY_MainViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_MainViewController.h"

#import "AppDelegate.h"
#import "HomeResourcesInfoModel.h"
#import "HomeCategoyItemInfoModel.h"

#import "HomeCategoryMenuBarItem.h"
#import "UINavigationController+Extension.h"

#import "YYHomeNavigationCateInfoRequestAPI.h"

#import "YY_WKWebViewController.h"
#import "YY_CategoryViewController.h"
#import "YY_MainFeaturedViewController.h"
#import "YY_MainOtherSortViewController.h"
#import "LoginUserQuitInfoRequestAPI.h"

#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>

#import "MchListViewController.h"

@interface YY_MainViewController ()
<
JMMenuPageViewDelegate,
JMMenuPageViewDataSource,
AMapLocationManagerDelegate
>

@property (nonatomic, strong) UIImageView *coverImageView; //蒙层
@property (nonatomic, strong) UIImageView *nextImageView;
@property (nonatomic, strong) UIImageView *currentImageView;

@property (nonatomic, strong) UIView *headerBackgroundBlurView;

@property (nonatomic, strong) UIView *navigationView;
@property (nonatomic, strong) UIImageView *searchIcon;
@property (nonatomic, strong) UIButton *navigatinCategoryBtn;
@property (nonatomic, strong) UIView *searchBackgroundView;

@property (nonatomic, strong) YYAnimatedImageView *leftSourceView; //左边资源位
@property (nonatomic, strong) UIView  *rightSpaceView; //右边占位图
@property (nonatomic, strong) UILabel *navigationTitleLabel;
@property (nonatomic, strong) JMMenuPageViewController *menuPageController;

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL menuBarTitleBlack; //标题是否是黑色
@property (nonatomic, assign) BOOL isHover; //首页时间轴是否悬停
@property (nonatomic, assign) UIStatusBarStyle statuStyle;
@property (nonatomic, strong) NSMutableArray *keywords;
@property (nonatomic, strong) NSMutableArray *categorys; //栏目
@property (nonatomic, strong) NSMutableDictionary *ctrlOffsetStatuDicts; //控制器导航状态
@property (nonatomic, strong) NSMutableDictionary *backgroundDicts;  //背景数据组装。
@property (nonatomic, strong) NSMutableDictionary *ctrlBannerStatuDicts; //导航器是否有banner状态
@property (nonatomic, strong) NSMutableDictionary *ctrlMenuBarStyleDicts; //是否修改item样式

@property (nonatomic, strong) HomeResourcesInfoModel *activityPlace; //左边活动位置
@property (nonatomic, strong) HomeResourceItemInfoModel *activitySkipModel; //活动跳转资源

@property (nonatomic, assign) NSInteger nowPage; //现在页数
@property (nonatomic, assign) float  lastOffsetX; //开始偏移的位置
@property (nonatomic, assign) BOOL   isTurnPage; //是否翻页

@property (nonatomic, strong) UIButton * locationButton;
@property (nonatomic, strong) AMapLocationManager *locationManager;

@end

@implementation YY_MainViewController

#pragma mark - Setter
- (UIView *)navigationView {
    if (!_navigationView) {
        _navigationView = [YYCreateTools createView:XHClearColor];
    }
    return _navigationView;
}

- (JMMenuPageViewController *)menuPageController {
    if (!_menuPageController) {
        _menuPageController = [[JMMenuPageViewController alloc] init];
        _menuPageController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _menuPageController.menuPageView.menuPageDelegate = self;
        _menuPageController.menuPageView.menuPageDataSource = self;
        _menuPageController.menuPageView.menuBar.isUseCache = NO;
        _menuPageController.menuPageView.menuBar.menuBarEdgeInsets = UIEdgeInsetsMake(0, 0, 0, kSizeScale(12));
        _menuPageController.menuPageView.menuBar.isStartAddSpace = YES;
        _menuPageController.menuPageView.navigationHeight = kSizeScale(0.0);
        _menuPageController.menuPageView.menuBar.menuBarItemSpace = kSizeScale(14);
//        _menuPageController.menuPageView.menuPageLayoutStyle = JMMenuBarLayoutStyleDivide;
//        _menuPageController.menuPageView.menuPageStayStyle = JMMenuBarStayLocationCenter;
    }
    return _menuPageController;
}


- (void)userQuitLoginRequest {
    LoginUserQuitInfoRequestAPI *quitAPI = [[LoginUserQuitInfoRequestAPI alloc] initWithQuitLoginRequest:[[NSDictionary dictionary] mutableCopy]];
    [YYCenterLoading showCenterLoading];
    [quitAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [kUserManager clearUserData];
            [[HDClient sharedClient] logout:YES];
            [RJBadgeController clearBadgeForKeyPath:kShippingRedCountPath];
            [RJBadgeController clearBadgeForKeyPath:kShippingDetailRedCountPath];
            [RJBadgeController clearBadgeForKeyPath:kShippingSubDetailRedCountPath];
            kPostNotification(LoginAndQuitSuccessNotification, nil);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (![kUserDefault boolForKey:@"isContainPrivacy"]) {
        YYAlertView *alert = [[YYAlertView alloc] initWithFrame:CGRectMake(0, 0, kScreenW-kSizeScale(58), kSizeScale(317)) title:@"欢迎使用“蜀黍之家”APP，我们深知个人信息对您的重要性，并会尽全力保护您的个人信息安全可靠。我们致力于维持您对我们的信任，恪守以下原则，保护您的个人信息，详情查看《蜀黍之家用户隐私政策》。" subTitle:@"如同意此政策，请点击“同意”并开始使用蜀黍之家，我们承诺，我们将按业界成熟的安全标准，采取相应的安全保护措施来保护您的个人信息。" alertType:YYAlertPrivacyPolicyType otherBlock:^(NSInteger index) {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:@(3) forKey:@"linkType"];
            [params setObject:PrivacyURL forKey:@"url"];
            [YYCommonTools skipMultiCombinePage:self
                                         params:params];
        } confirmBlock:^{
            [kUserDefault setBool:YES forKey:@"isContainPrivacy"];
        } cancelBlock:^{
            exit(0);
        }];
        [alert showAlertView];
    }
}

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [MobClick event:@"homeView"];
    if (kIsFan) {
        [MobClick event:@"fanHomeView"];
    }
    else {
        [MobClick event:@"memberHomeView"];
    }
    
    self.statuStyle = UIStatusBarStyleLightContent;
    [self setHeaderBackgound];
    [self setNavigationSubViews];
    [self setMenuPageView];
    
    //定位
    [self location];
    [self homeCategoryRequest];
    
    if (kIsTourist) {
        UIButton *view = [[UIButton alloc] initWithFrame:CGRectMake(0, kScreenH-kTabbarH-5, kScreenW, kTabbarH+5)];
        view.backgroundColor = XHRedColor;
        [view setTitle:@"登录/注册" forState:UIControlStateNormal];
        [view setTitleColor:XHWhiteColor forState:UIControlStateNormal];
        view.titleLabel.font = normalFont(15);
        [[UIApplication sharedApplication].keyWindow addSubview:view];
        view.actionBlock = ^(UIButton *sender) {
            [self userQuitLoginRequest];
        };
    }
    self.view.backgroundColor = XHMainLightColor;
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.statuStyle;
}

#pragma mark ******定位功能******
//定位获取经纬度和当前的位置
- (void)location {
    if([CLLocationManager locationServicesEnabled]){
        self.locationManager = [[AMapLocationManager alloc]init];
        [self.locationManager setDelegate:self];
        //是否允许后台定位。默认为NO。只在iOS 9.0及之后起作用。设置为YES的时候必须保证 Background Modes 中的 Location updates 处于选中状态，否则会抛出异常。由于iOS系统限制，需要在定位未开始之前或定位停止之后，修改该属性的值才会有效果。
        [self.locationManager setAllowsBackgroundLocationUpdates:NO];
        //指定定位是否会被系统自动暂停。默认为NO。
        [self.locationManager setPausesLocationUpdatesAutomatically:NO];
        //设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
        [self.locationManager setDistanceFilter:200];
        //设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        //设置定位超时时间
        self.locationManager.locationTimeout = 10;
        //设置反地理编码超时时间
        self.locationManager.reGeocodeTimeout = 10;
        //开始定位服务
        [self.locationManager setLocatingWithReGeocode:YES];
        [self.locationManager startUpdatingLocation];
        [self.locationButton setTitle:@"定位中" forState:UIControlStateNormal];
        [self.locationButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
    } else {
        UIAlertView *alvertView=[[UIAlertView alloc]initWithTitle:@"提示" message:@"需要开启定位服务,请到设置->隐私,打开定位服务" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alvertView show];
    }
}

- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location reGeocode:(AMapLocationReGeocode *)reGeocode {
    NSLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
      if (reGeocode) {
          [self.locationManager stopUpdatingLocation];
          [self.locationButton setTitle:reGeocode.city forState:UIControlStateNormal];
          [self.locationButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
          NSLog(@"reGeocode:%@", reGeocode);
      }
}

#pragma mark - Request
/**
 首页类目请求
 **/
- (void)homeCategoryRequest {
    self.currentPage = 0;
    self.keywords = [NSMutableArray array];
    self.categorys = [NSMutableArray array];
    self.backgroundDicts = [NSMutableDictionary dictionary];
    self.ctrlOffsetStatuDicts = [NSMutableDictionary dictionary];
    self.ctrlBannerStatuDicts = [NSMutableDictionary dictionary];
    self.ctrlMenuBarStyleDicts = [NSMutableDictionary dictionary];
    
    [YYCenterLoading showCenterLoading];

    YYHomeNavigationCateInfoRequestAPI *cateAPI = [[YYHomeNavigationCateInfoRequestAPI alloc] initHomeCategoryInfoRequest:[@{} mutableCopy]];
    [cateAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            if ([responDict objectForKey:@"keywords"]) {
                NSArray *keywordArray = [responDict objectForKey:@"keywords"];
                if (kValidArray(keywordArray)) {
                    self.keywords = [keywordArray mutableCopy];
                    self.navigationTitleLabel.text = [self.keywords firstObject];
                    self.keywords = @[];
                    
                }
            }
            if ([responDict objectForKey:@"items"]) {
                NSArray *items = [NSArray modelArrayWithClass:[HomeCategoyItemInfoModel class]
                                                         json:[responDict objectForKey:@"items"]];
                if (kValidArray(items)) {
//                    self.categorys = [items mutableCopy];
                    self.categorys = @[items.firstObject];
                    
                    [self willBackgroundPackage];
                    [self setImageViewSourceShow:0 isCurrent:YES];
                    [self setImageViewSourceShow:1 isCurrent:NO];
                }
                self.menuPageController.menuPageView.menuContents = self.categorys;
                [self.menuPageController.menuPageView reloadData];
            }
            if ([responDict objectForKey:@"cell"]) {
                self.activityPlace = [HomeResourcesInfoModel modelWithJSON:[responDict objectForKey:@"cell"]];
                CGRect leftFrame = CGRectZero;
                self.leftSourceView = [[YYAnimatedImageView alloc] init];
                self.leftSourceView.frame = leftFrame;
                self.leftSourceView.userInteractionEnabled = YES;
                self.leftSourceView.contentMode = UIViewContentModeScaleAspectFit;
                [self.leftSourceView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftNavigationSourceClick:)]];
                
            }
        }
        if (!kValidArray(self.categorys)) {
            self.menuPageController.menuPageView.contentView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewNoDataType
                                                    title:nil
                                                   target:self
                                                   action:@selector(emptyBtnOperator)];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.menuPageController.menuPageView.contentView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewNetworkErrorType
                                                      title:nil
                                                     target:self
                                                      action:@selector(emptyBtnOperator)];
    }];
}

#pragma mark - Private
- (void)setHeaderBackgound {
    
    self.nextImageView = [YYCreateTools createImageView:nil
                                                 viewModel:-1];
    self.nextImageView.alpha = 0.0;
    [self.view addSubview:self.nextImageView];
    
    self.currentImageView = [YYCreateTools createImageView:nil
                                                 viewModel:-1];
    self.currentImageView.alpha = 1.0;
    [self.view addSubview:self.currentImageView];
    
    self.coverImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    self.coverImageView.image = [UIImage imageWithColor:XHWhiteColor];
    self.coverImageView.alpha = 0.0;
    [self.view addSubview:self.coverImageView];
    
    [self.nextImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        if (@available(iOS 11, *)) {
            make.top.offset(self.view.safeAreaInsets.top);
        }
        else {
            make.top.equalTo(self.view);
        }
        make.height.mas_equalTo(kScreenW);
    }];
    
    [self.coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.top.equalTo(self.nextImageView);
    }];
    
    [self.currentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.top.equalTo(self.nextImageView);
    }];
}

- (void)setNavigationSubViews {
    [self xh_alphaNavigation:0];
    [self xh_navBottomLine:XHClearColor];
    [self xh_addTitleView:self.navigationView];
    [self setNeedsStatusBarAppearanceUpdate];
    [self.view bringSubviewToFront:self.navigationBar];
    
    [self.navigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kScreenW, kSizeScale(32)));
        make.centerX.equalTo(self.navigationView.superview);
        make.centerY.equalTo(self.navigationView.superview).offset(IS_IPHONE_X?kSizeScale(15):kSizeScale(10));
    }];
    
    //定位按钮
    self.locationButton = [UIButton creatButtonWithTitle:@"定位" textColor:[UIColor whiteColor] font:normalFont(12) target:self action:@selector(locationButtonClick)];
    [self.locationButton setImage:[UIImage imageNamed:@"location_down_white"] forState:UIControlStateNormal];
    [self.locationButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
    [self.navigationView addSubview:self.locationButton];
    
    self.navigatinCategoryBtn = [YYCreateTools createBtnImage:@"home_category_default_icon"];
    [self.navigatinCategoryBtn addTarget:self action:@selector(cateClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationView addSubview:self.navigatinCategoryBtn];

    self.searchBackgroundView = [YYCreateTools createView:HexRGBALPHA(0xeeeeee, 0.7)];
    self.searchBackgroundView.layer.cornerRadius = kSizeScale(16);
    [self.searchBackgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchClickEvent:)]];
    [self.navigationView addSubview:self.searchBackgroundView];
    
    self.searchIcon = [YYCreateTools createImageView:@"home_search_default_icon"
                                                   viewModel:-1];
    [self.searchBackgroundView addSubview:self.searchIcon];
    self.navigationTitleLabel = [YYCreateTools createLabel:@"请输入关键词搜索"
                                                      font:normalFont(12)
                                                 textColor:XHBlackMidColor];
    self.navigationTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.searchBackgroundView addSubview:self.navigationTitleLabel];
    
    [self.navigatinCategoryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-kSizeScale(12));
        make.top.bottom.offset(0);
        make.width.mas_equalTo(kSizeScale(30));
    }];
    
    [self.locationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(kSizeScale(5));
        make.top.bottom.offset(0);
        make.width.mas_equalTo(kSizeScale(45 + 8 + 5 + 20));
    }];

    [self.searchBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.locationButton.mas_right).offset(kSizeScale(10));
        make.top.bottom.offset(0);
        make.right.equalTo(self.navigatinCategoryBtn.mas_left).offset(-kSizeScale(10));
    }];
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(13), kSizeScale(13)));
        make.left.mas_equalTo(kSizeScale(10));
        make.centerY.equalTo(self.searchBackgroundView);
    }];
    
    [self.navigationTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchIcon.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.searchIcon);
    }];
    
    // 处理购物车红点，不能写在委托代理类里面。否则不释放。
    CYLTabBarController *tabVC = [(AppDelegate *)[[UIApplication sharedApplication] delegate] tabbar];
    NSArray *tabBarItems = tabVC.tabBar.items;
    UITabBarItem *cartTabItem = [tabBarItems objectAtIndex:2];
    [cartTabItem setBadgeOffset:CGPointMake(-kSizeScale(2), kSizeScale(6))];
    [cartTabItem setBadgeBgColor:XHRedColor];
    [self.badgeController observePath:kShippingRedCountPath
                            badgeView:cartTabItem
                                block:^(id  _Nullable observer, NSDictionary<NSString *,id> * _Nonnull info) {
                                    NSInteger tempCount = [[info objectForKey:@"RJBadgeCountKey"] integerValue];
                                    [cartTabItem showBadgeWithValue:tempCount];
                                }];
}

//定位按钮的点击事件
- (void)locationButtonClick {
    [self.locationButton setTitle:@"定位中" forState:UIControlStateNormal];
    [self.locationButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
    [self.locationManager setLocatingWithReGeocode:YES];
    [self.locationManager startUpdatingLocation];
    //周边商家
    MchListViewController *locationVC = [[MchListViewController alloc] init];
    locationVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:locationVC animated:YES];
}

- (void)setMenuPageView {
    [self addChildViewController:self.menuPageController];
    [self.view addSubview:self.menuPageController.view];
    
    [self.view setNeedsUpdateConstraints];
    [self.menuPageController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
}

- (void)setImageViewSourceShow:(NSInteger)page
                     isCurrent:(BOOL)isCurrent {
    NSInteger linkType = -1;
    HomeCategoyItemInfoModel *itemModel = nil;
    if (kValidArray(self.categorys) && page < self.categorys.count && page >= 0) {
        itemModel = [self.categorys objectAtIndex:page];
        linkType = [[itemModel.link objectForKey:@"linkType"] integerValue];
        if (isCurrent) {
            if (linkType == 17) {
                self.currentImageView.image = [UIImage imageWithColor:XHWhiteColor];
            }
            else {
                [self.currentImageView sd_setImageWithURL:[NSURL URLWithString:itemModel.backgroundImage.imageUrl] placeholderImage:[UIImage imageNamed:@"home_backgroud_placehold_icon"]];
            }
        }
        else {
            if (linkType == 17) {
                self.nextImageView.image = [UIImage imageWithColor:XHWhiteColor];
            }
            else {
                [self.nextImageView sd_setImageWithURL:[NSURL URLWithString:itemModel.backgroundImage.imageUrl] placeholderImage:[UIImage imageNamed:@"home_backgroud_placehold_icon"]];
            }
        }
    }
}

/**
 * 对资源进行重组
 */
- (void)willBackgroundPackage {
    if (kValidArray(self.categorys)) {
        NSInteger cateCount = self.categorys.count;
        for (int i = 0; i < cateCount; i ++) {
            [self.ctrlOffsetStatuDicts setObject:@(0.0) forKey:@(i)];
            [self.ctrlMenuBarStyleDicts setObject:@(NO) forKey:@(i)];
        }
    }
}

/**
 *  获取当前偏移的透明度
 */
- (CGFloat)gainCurrentAlphaWithOffset:(CGPoint)offset {
    CGFloat offsetY = 0.0, alpha = 0.0;
    CGFloat criticalValue = kSizeScale(150);
    if (offset.y < 0) {
        offsetY = 0.0;
    }
    if (offset.y > 0 && offset.y <= criticalValue) {
        offsetY = offset.y;
    }
    else if (offset.y > criticalValue) {
        offsetY = criticalValue;
    }
    alpha = (offsetY/criticalValue);
    return alpha;
}

#pragma 滚动的监听事件
/**
 * 根据页索引和透明度来处理导航和menuBar的样式
 **/
- (void)controlNavigationAndMenuBarStyle:(CGFloat)alpha
                               pageIndex:(NSInteger)pageIndex
                             isScrolling:(BOOL)isScrolling {
    if (alpha > 0.5) {
        // 变黑色
        if (!self.menuPageController.menuPageView.menuBar.titleColorTurnBlack) {
            self.menuPageController.menuPageView.menuBar.titleColorTurnBlack = YES;
        }
        [self.locationButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.locationButton setImage:[UIImage imageNamed:@"location_down_black"] forState:UIControlStateNormal];
        [self.locationButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
        self.searchIcon.image = [UIImage imageNamed:@"home_search_select_icon"];
        [self.navigatinCategoryBtn setImage:[UIImage imageNamed:@"home_category_select_icon"] forState:UIControlStateNormal];
        self.navigationTitleLabel.textColor = XHBlackLitColor;
        self.menuBarTitleBlack = YES;
        self.statuStyle = UIStatusBarStyleDefault;
    }
    else {
        // 变白色
        if (self.menuPageController.menuPageView.menuBar.titleColorTurnBlack) {
            self.menuPageController.menuPageView.menuBar.titleColorTurnBlack = NO;
        }
        [self.locationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.locationButton setImage:[UIImage imageNamed:@"location_down_white"] forState:UIControlStateNormal];
        [self.locationButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
        self.searchIcon.image = [UIImage imageNamed:@"home_search_default_icon"];
        [self.navigatinCategoryBtn setImage:[UIImage imageNamed:@"home_category_default_icon"] forState:UIControlStateNormal];
        self.navigationTitleLabel.textColor = HexRGB(0xEEEEEE);
        self.menuBarTitleBlack = NO;
        self.statuStyle = UIStatusBarStyleLightContent;
    }
    [self setNeedsStatusBarAppearanceUpdate];
}

/**
 * 重新计算当前页码
 */
- (void)reCalculateCurrentPage:(CGPoint)offset {
    NSInteger linkType = -1;
    float offsetX = offset.x;
    float offsetY = 0.0, alpha = 0.0;
    HomeCategoyItemInfoModel *itemModel = nil;
    
    NSInteger currentPage = (NSInteger)(offsetX/kScreenW);
    self.currentPage = currentPage;
    [self setImageViewSourceShow:self.currentPage isCurrent:YES];
    
    // 判断结束的时候当前页的状态
    if (kValidArray(self.categorys)) {
        itemModel = [self.categorys objectAtIndex:currentPage];
        linkType = [[itemModel.link objectForKey:@"linkType"] integerValue];
    }
    offsetY = [[self.ctrlOffsetStatuDicts objectForKey:@(currentPage)] floatValue];
    alpha = [self gainCurrentAlphaWithOffset:CGPointMake(offsetX, offsetY)];
    // 结束的是否偏移大于0.0
    if (alpha > 0.0) {
        self.currentImageView.alpha = 1 - alpha;
        self.coverImageView.alpha = alpha;
    }
    else {
        self.currentImageView.alpha = 1.0;
        if (linkType == 17) {
            // 是web 页面
            alpha = 1.0;
            self.coverImageView.alpha = 1.0;
        }
        else {
            self.coverImageView.alpha = 0.0;
        }
    }
    self.nextImageView.alpha = 0.0;
    // 确保首页切换回来状态不变
    if (self.isHover && self.currentPage == 0) {
        [self.menuPageController.menuPageView setNavigationHidden:YES duration:0.2];
    }
    else {
        [self.menuPageController.menuPageView setNavigationHidden:NO duration:0.2];
    }
    [self controlNavigationAndMenuBarStyle:alpha  pageIndex:currentPage isScrolling:YES];
}

#pragma mark - JMMenuPageViewDelegate
- (void)menuPageView:(JMMenuPageView *)menuPageView didSelectItemAtIndex:(NSInteger)itemIndex {
    
    NSInteger linkType = -1;
    float nowOffstY = 0.0, nowAlpha = 0.0;
    self.nowPage = itemIndex;
    HomeCategoyItemInfoModel *itemModel = nil;
    if (kValidArray(self.categorys)) {
        itemModel = [self.categorys objectAtIndex:self.nowPage];
        linkType = [[itemModel.link objectForKey:@"linkType"] integerValue];
    }
    nowOffstY = [[self.ctrlOffsetStatuDicts objectForKey:@(self.nowPage)] floatValue];
    nowAlpha = [self gainCurrentAlphaWithOffset:CGPointMake(0, nowOffstY)];
    
    NSLog(@"====%f====%f===%f===%f", self.currentImageView.alpha, self.nextImageView.alpha, self.coverImageView.alpha, nowAlpha);
    [self controlNavigationAndMenuBarStyle:(linkType == 17 ? 1.0 : nowAlpha)  pageIndex:self.nowPage isScrolling:YES];
    [self setImageViewSourceShow:itemIndex isCurrent:YES];
}

- (void)menuPageView:(JMMenuPageView *)menuPageView scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.lastOffsetX = scrollView.contentOffset.x;
    self.nowPage = scrollView.contentOffset.x/kScreenW;
    [self setImageViewSourceShow:self.nowPage isCurrent:YES];
}

- (void)menuPageView:(JMMenuPageView *)menuPageView scrollViewDidScroll:(UIScrollView *)scrollView {
   
    NSInteger page = self.nowPage;
    float offsetY = 0.0, saveAlpha = 0.0;
    float nowOffstY = 0.0, nowAlpha = 0.0;
    float offsetX = scrollView.contentOffset.x;
    float diffValue = offsetX - self.nowPage*kScreenW;
    
    nowOffstY = [[self.ctrlOffsetStatuDicts objectForKey:@(self.nowPage)] floatValue];
    nowAlpha = [self gainCurrentAlphaWithOffset:CGPointMake(0, nowOffstY)];
    // 判断左右滑动方向
    if (offsetX < self.lastOffsetX) {
        // 向右
        page = self.nowPage - 1;
        [self setImageViewSourceShow:page isCurrent:NO];
    }
    else if (offsetX > self.lastOffsetX) {
        // 向左
        page = self.nowPage + 1;
        [self setImageViewSourceShow:page isCurrent:NO];
    }
    
    // 计算上一页或者下一页的偏移状态
    offsetY = [[self.ctrlOffsetStatuDicts objectForKey:@(page)] floatValue];
    saveAlpha = [self gainCurrentAlphaWithOffset:CGPointMake(0, offsetY)];
    
    diffValue = fabsf(diffValue);
    
    float alpha = (diffValue)/kScreenW;
    alpha = fabsf(alpha);
    
    // 当前页偏移量是否大于0
    //&& alpha < nowAlpha
    if (nowOffstY > 0 ) {
        float diffAlpha = 0.0;
        self.currentImageView.alpha = 1 - nowAlpha;
        // 当前页面和前一个页面或者后一个页面偏移都大于0
        if (saveAlpha > 0) {
            if (nowAlpha < saveAlpha) {
                diffAlpha = nowAlpha + alpha;
                if (diffAlpha > saveAlpha) {
                    diffAlpha = saveAlpha;
                }
            }
            else {
                diffAlpha = nowAlpha - alpha;
                if (diffAlpha <= saveAlpha) {
                    diffAlpha = saveAlpha;
                }
            }
        }
        /*if (saveAlpha > 0 && diffAlpha < saveAlpha) {
            diffAlpha = saveAlpha;
        }*/
        self.coverImageView.alpha = diffAlpha > 0.0 ? diffAlpha : 0.0;
        self.nextImageView.alpha = alpha;
    }
    else {
        float diffAlpha = alpha < saveAlpha ? alpha : saveAlpha;
        self.currentImageView.alpha = 1 - alpha;
        self.coverImageView.alpha = diffAlpha;
        self.nextImageView.alpha = (saveAlpha > 0 ? diffAlpha : alpha);
    }
}

- (void)menuPageView:(JMMenuPageView *)menuPageView scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self reCalculateCurrentPage:scrollView.contentOffset];
    [self.ctrlMenuBarStyleDicts setObject:@(self.menuBarTitleBlack) forKey:@(self.currentPage)];
}

- (void)menuPageView:(JMMenuPageView *)menuPageView scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self reCalculateCurrentPage:scrollView.contentOffset];
        [self.ctrlMenuBarStyleDicts setObject:@(self.menuBarTitleBlack) forKey:@(self.currentPage)];
    }
}

#pragma mark - JMMenuPageViewDataSource
- (JMMenuBarItem *)menuPageView:(JMMenuPageView *)menuPageView menuBarItemAtIndex:(NSInteger)itemIndex {
    static NSString *identifier = @"home-category-title-item";
    HomeCategoryMenuBarItem *item = [menuPageView dequeueReusableItemWithIdentifier:identifier];
    if (!item) {
        item = [[HomeCategoryMenuBarItem alloc] init];
    }
    if (itemIndex < self.categorys.count) {
        HomeCategoyItemInfoModel *titleModel = [self.categorys objectAtIndex:itemIndex];
        item.titleLabel.text = titleModel.itemContent;
    }
    return item;
}

- (UIViewController *)menuPageView:(JMMenuPageView *)menuPageView viewControllerAtPage:(NSInteger)pageIndex {
    NSInteger linkType = 0;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *featureIdentifier = @"MainFeature";
    if (kValidArray(self.categorys)) {
        if (pageIndex < self.categorys.count) {
            HomeCategoyItemInfoModel *titleModel = [self.categorys objectAtIndex:pageIndex];
            if (kValidString(titleModel.title)) {
                featureIdentifier = [featureIdentifier stringByAppendingString:titleModel.title];
            }
            if (kValidDictionary(titleModel.link)) {
                params = [titleModel.link mutableCopy];
            }
            if ([titleModel.link objectForKey:@"linkType"]) {
                linkType = [[titleModel.link objectForKey:@"linkType"] integerValue];
            }
        }
        if (linkType == 15) {
            // 精选
            YY_MainFeaturedViewController *mainFeature = [menuPageView dequeueReusablePageWithIdentifier:featureIdentifier];
            if (!mainFeature) {
                mainFeature = [[YY_MainFeaturedViewController alloc] init];
            }
            [params setObject:@(pageIndex) forKey:@"pageIndex"];
            mainFeature.parameter = params;
            mainFeature.sectionHeaderScrollBlock = ^(CGPoint scrollValue) {
                if (scrollValue.y < 0) {
                    if (self.menuPageController.menuPageView.navigationHidden) {
                        return;
                    }
                    [self.menuPageController.menuPageView setNavigationHidden:YES duration:0.2];
                }
                else {
                    if (!self.menuPageController.menuPageView.navigationHidden) {
                        return;
                    }
                    [self.menuPageController.menuPageView setNavigationHidden:NO duration:0.2];
                }
            };
            mainFeature.navigationGradientBlock = ^(BOOL isExistBanner, CGPoint scrollValue, NSInteger pageIndex) {
                CGFloat alpha = [self gainCurrentAlphaWithOffset:scrollValue];
                self.coverImageView.alpha = alpha;
                self.currentImageView.alpha = 1 - alpha;
                [self controlNavigationAndMenuBarStyle:alpha
                                             pageIndex:pageIndex
                                           isScrolling:YES];
            };
            mainFeature.scrollStopBlock = ^(CGPoint scrollValue, NSInteger pageIndex, BOOL isExistBanner, BOOL isHover) {
                if ([self.ctrlOffsetStatuDicts objectForKey:@(pageIndex)]) {
                    [self.ctrlOffsetStatuDicts setObject:@(scrollValue.y) forKey:@(pageIndex)];
                }
                self.isHover = isHover;
            };
            return mainFeature;
        }
        else if (linkType == 16) {
            // 其他原生
            YY_MainOtherSortViewController *mainOther = [menuPageView dequeueReusablePageWithIdentifier:featureIdentifier];
            if (!mainOther) {
                mainOther = [[YY_MainOtherSortViewController alloc] init];
            }
            [params setObject:@(pageIndex) forKey:@"pageIndex"];
            mainOther.parameter = params;
            mainOther.navigationGradientBlock = ^(BOOL isExistBanner, CGPoint scrollValue, NSInteger pageIndex) {
                CGFloat alpha = [self gainCurrentAlphaWithOffset:scrollValue];
                self.coverImageView.alpha = alpha;
                self.currentImageView.alpha = 1 - alpha;
                [self controlNavigationAndMenuBarStyle:alpha
                                             pageIndex:pageIndex
                                           isScrolling:YES];
            };
            // 滚动停止回调
            mainOther.scrollStopBlock = ^(CGPoint scrollValue, NSInteger pageIndex, BOOL isExistBanner) {
                if ([self.ctrlOffsetStatuDicts objectForKey:@(pageIndex)]) {
                    [self.ctrlOffsetStatuDicts setObject:@(scrollValue.y) forKey:@(pageIndex)];
                }
            };
            return mainOther;
        }
        else {
            YY_WKWebViewController *wkWebView = [menuPageView dequeueReusablePageWithIdentifier:featureIdentifier];
            if (!wkWebView) {
                wkWebView = [[YY_WKWebViewController alloc] init];
            }
            if (self.menuPageController.menuPageView.navigationHidden) {
                [self.menuPageController.menuPageView setNavigationHidden:NO duration:0.2];
            }
            [params setObject:@(3) forKey:@"pageType"];
            wkWebView.parameter = params;
            return wkWebView;
        }
    }
    return [UIViewController new];
}

- (CGFloat)menuPageView:(JMMenuPageView *_Nullable)menuPageView menuBarItemWidthAtIndex:(NSInteger)itemIndex {
    CGSize itemSize = CGSizeZero;
    if (itemIndex < self.categorys.count) {
        HomeCategoyItemInfoModel *titleModel = [self.categorys objectAtIndex:itemIndex];
        itemSize = [NSString sizeWithTitle:titleModel.itemContent
                                      font:boldFont(14)];
    }
    return itemSize.width * 1.2;
}
#pragma mark - Event
/**
 * 分类跳转
 */
- (void)cateClickEvent:(UIButton *)button {
    YY_CategoryViewController *cateVC = [[YY_CategoryViewController alloc] init];
    cateVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:cateVC animated:YES];
}

/**
 * 搜索跳转
 */
- (void)searchClickEvent:(UITapGestureRecognizer *)tapGesture {
    [YYCommonTools skipSearchPage:self
                            title:self.navigationTitleLabel.text?:@"" type:0];
}

/**
 * 错误操作
 **/
- (void)emptyBtnOperator {
    [self homeCategoryRequest];
}

/**
 * 左侧资源点击
 **/
- (void)leftNavigationSourceClick:(UITapGestureRecognizer *)gesture {
    if (self.activitySkipModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (self.activitySkipModel.link) {
            if ([self.activitySkipModel.link objectForKey:@"linkType"]) {
                [params setObject:[self.activitySkipModel.link objectForKey:@"linkType"] forKey:@"linkType"];
            }
            if ([self.activitySkipModel.link objectForKey:@"uri"]) {
                [params setObject:[self.activitySkipModel.link objectForKey:@"uri"] forKey:@"url"];
            }
        }
        [YYCommonTools skipMultiCombinePage:self params:params];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
