//
//  YY_GoodDetailViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_GoodDetailViewController.h"

#import "UIColor+Extension.h"
#import "MyCourceModel.h"
#import "HomeGoodInfoModel.h"
#import "HomeBannerInfoModel.h"
#import "CouponListInfoModel.h"
#import "HomeBannerInfoModel.h"
#import "HomeListStructInfoModel.h"
#import "HomeGoodPropSkuInfoModel.h"
#import "DetailColSwitchInfoModel.h"
#import "FriendCircleListInfoModel.h"

#import "YYSkuSelectView.h"
#import "YYAddCartTipView.h"
#import "ServiceShowView.h"
#import "GoodDetailMenuBarItem.h"
#import "GoodDetailCsdInfoView.h"
#import "GoodDetailCouponView.h"
#import "GoodDetailHeaderView.h"
#import "GoodDetailSkuInfoCell.h"
#import "GoodDetailBasicInfoCell.h"
#import "GoodDetailDescriInfoCell.h"
#import "GoodDetailDynamicInfoCell.h"
#import "GoodDetailRecommendInfoCell.h"
#import "GoodDetailCircleCsdInfoCell.h"

#import "yyCountDownManager.h"

#import "XHGestureReconginTableView.h"
#import "UIColor+Extension.h"
#import "YYGoodDetailInfoRequestAPI.h"
#import "YYGoodRecommendInfoRequestAPI.h"
#import "UIImage+Extension.h"

@interface YY_GoodDetailViewController ()
<
JMMenuBarDelegate,
JMMenuBarDataSource,
UITableViewDelegate,
UIScrollViewDelegate,
UITableViewDataSource,
UIGestureRecognizerDelegate,
GoodDetailRecommendInfoCellDelegate
>

@property (nonatomic, strong) GoodDetailHeaderView        *headerView;
@property (nonatomic, strong) GoodDetailCsdInfoView       *csdView;   //素材圈
@property (nonatomic, strong) YYSkuSelectView             *skuSelectView;
@property (nonatomic, strong) XHGestureReconginTableView  *m_baseTableView;
@property (nonatomic, strong) HomeGoodInfoModel           *m_detailModel;

@property (nonatomic, assign) BOOL                  canScroll;
@property (nonatomic, assign) BOOL                  isCanNotMoveTabView;
@property (nonatomic, assign) BOOL                  isCanNotMoveTabViewPre;

@property (nonatomic, strong) UIButton              *csdBtn;         //素材
@property (nonatomic, strong) YYButton              *cartBtn;
@property (nonatomic, strong) YYButton              *tryLookBtn;     //试看按钮

@property (nonatomic, strong) YYOrderShowView       *addBtn;
@property (nonatomic, strong) YYOrderShowView       *buyBtn;
@property (nonatomic, strong) UIButton * mealZoneBuyBtn;
@property (nonatomic, strong) UIView                *m_bottomView;
@property (nonatomic, strong) JMMenuBar             *topMenuBar;
@property (nonatomic, strong) GoodDetailDescriInfoCell *descriCell;

@property (nonatomic, assign) BOOL                  isFan;
@property (nonatomic, assign) BOOL                  isFirstEnter;
@property (nonatomic, strong) NSString              *commodityUrl;     //商品URL
@property (nonatomic, strong) NSString              *selectSpec;
@property (nonatomic, strong) UIImage               *selectImage;
@property (nonatomic, assign) YYEmptyViewType       m_emptyType;
@property (nonatomic, strong) HomeBannerInfoModel   *activityModel;
@property (nonatomic, strong) HomeGoodValueSkuInfoModel  *selectSkuModel;
@property (nonatomic, strong) HomeListStructInfoModel     *recommendModel;

@property (nonatomic, strong) NSMutableArray        *detailDatas;  //详情内容
@property (nonatomic, strong) NSMutableArray        *menuDatas; //顶部分栏资源
@property (nonatomic, strong) NSMutableArray        *recommendArray;   //推荐商品

@property (nonatomic, strong) UIView  *cover;
@property (nonatomic, strong) GoodDetailCouponView *chooseCouponView;
@property (nonatomic, strong) ServiceShowView      *serviceShowView;
@property (nonatomic, strong) NSNumber * goodsType;

@end

@implementation YY_GoodDetailViewController

#pragma mark - Setter method


- (XHGestureReconginTableView *)m_baseTableView {
    if (!_m_baseTableView) {
        _m_baseTableView = [[XHGestureReconginTableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _m_baseTableView.delegate = self;
        _m_baseTableView.dataSource = self;
        _m_baseTableView.backgroundColor = XHWhiteColor;
        _m_baseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _m_baseTableView.allowsSelection = NO;
        if (@available(iOS 11.0, *)) {
            _m_baseTableView.estimatedRowHeight = 0;
            _m_baseTableView.estimatedSectionFooterHeight = 0;
            _m_baseTableView.estimatedSectionHeaderHeight = 0;
        }
    }
    return _m_baseTableView;
}

- (UIView *)m_bottomView {
    if (!_m_bottomView) {
        _m_bottomView = [YYCreateTools createView:XHWhiteColor];
        _m_bottomView.tag = 225;
    }
    return _m_bottomView;
}

// 素材view
- (GoodDetailCsdInfoView *)csdView {
    if (!_csdView) {
        _csdView = [[GoodDetailCsdInfoView alloc] initWithFrame:CGRectMake(kScreenW, kNavigationH, kScreenW, kScreenH - kNavigationH)];
    }
    return _csdView;
}

// 顶部分栏控件
- (JMMenuBar *)topMenuBar {
    if (!_topMenuBar) {
        _topMenuBar = [[JMMenuBar alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(56))];
        _topMenuBar.backgroundColor = XHClearColor;
        _topMenuBar.showsHorizontalScrollIndicator = NO;
        _topMenuBar.showsVerticalScrollIndicator = NO;
        _topMenuBar.clipsToBounds = YES;
        _topMenuBar.scrollsToTop = NO;
        _topMenuBar.alpha = 0.0;
        _topMenuBar.scrollEnabled = NO;
        _topMenuBar.menuBarItemSpace = kSizeScale(5);
        _topMenuBar.selectItemStayType = JMMenuBarStayLocationCenter;
        _topMenuBar.menuBarLayoutStyle = JMMenuBarLayoutStyleDivide;
        _topMenuBar.menuBarDelegate = self;
        _topMenuBar.menuBarDataSource = self;
    }
    return _topMenuBar;
}

#pragma mark - Life cycle

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    // 这里需要移除倒计时
}

- (void)viewDidLoad {
    [super viewDidLoad];

    @weakify(self);
    self.canScroll = YES;
    self.isFan = kIsFan;
    self.detailDatas = [NSMutableArray array];
    self.menuDatas = [NSMutableArray array];
    self.recommendArray = [NSMutableArray array];
    
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"url"]) {
            self.commodityUrl = [self.parameter objectForKey:@"url"];
        }
    }
    
    [kConfigCountDown setCountDownTimeInterval:0.1f];
    [kConfigCountDown addSourceWithIdentifier:@"detailCtrl"];
    [kConfigCountDown start];
    
    self.isFirstEnter = YES;
    [self setDetailNavigationBasicShow];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(goodDetailNotificationHandler:) name:GoodDetailLeaveTopNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                selector:@selector(refreshGoodDetailNotificationHandler:) name:RefreshGoodDetailNotification
                                               object:nil];
    
    Class current = [GoodDetailBasicInfoCell class];
    registerClass(self.m_baseTableView, current);
    
    current = [GoodDetailDynamicInfoCell class];
    registerClass(self.m_baseTableView, current);
    
    current = [GoodDetailSkuInfoCell class];
    registerClass(self.m_baseTableView, current);
    
    current = [GoodDetailDescriInfoCell class];
    registerClass(self.m_baseTableView, current);
    // 素材
    current = [GoodDetailCircleCsdInfoCell class];
    registerClass(self.m_baseTableView, current);
    // 商品推荐
    current = [GoodDetailRecommendInfoCell class];
    registerClass(self.m_baseTableView, current);
    
    self.headerView = [[GoodDetailHeaderView alloc] initWithFrame:CGRectZero];
    self.m_baseTableView.tableHeaderView = self.headerView;
    
    self.m_baseTableView.estimatedRowHeight = 44.0;
    self.m_baseTableView.rowHeight = UITableViewAutomaticDimension;
    [self.view addSubview:self.m_baseTableView];
    [self.view addSubview:self.m_bottomView];

    [self.m_baseTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(0));
    }];
    
    [self.view insertSubview:self.navigationBar aboveSubview:self.m_baseTableView];

    NSMutableDictionary *couponDict = [YYCommonTools getAssignResourcesUseOfType:18];
    if (kValidDictionary(couponDict)) {
        HomeBannerInfoModel *bannerModel = [HomeBannerInfoModel modelWithJSON:couponDict];
        self.activityModel = bannerModel;
    }
    
    
    [self callBlockGoodDetailRequest];
    //添加优惠券视图
    [self.view addSubview:self.cover];
    // 添加素材
    [self.view addSubview:self.csdView];
    
    self.csdView.sideslipGestureBlock = ^(BOOL isOpen) {
        @strongify(self);
        if (!isOpen) {
            [self csdShowBackClickEvent];
        }
    };
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [kConfigCountDown removeSourceWithIdentifier:@"detailCtrl"];
}

#pragma mark - Request method
/**获取详情信息**/
- (void)callBlockGoodDetailRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.commodityUrl forKey:@"uri"];
    YYGoodDetailInfoRequestAPI *detailAPI = [[YYGoodDetailInfoRequestAPI alloc] initGoodDetailInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [detailAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.m_detailModel = [HomeGoodInfoModel new];
//            if (kValidDictionary(self.parameter)) {
//                if ([self.parameter objectForKey:@"goodsType"]) {
//                    self.m_detailModel.goodsType = [self.parameter objectForKey:@"goodsType"];
//                }
//            }
            // 数据正常返回
            [self.m_baseTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.view);
                make.top.equalTo(self.view);
                make.bottom.mas_equalTo(-kBottom(kSizeScale(50)));
            }];
            [self.m_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.m_baseTableView);
                make.top.equalTo(self.m_baseTableView.mas_bottom);
                make.bottom.equalTo(self.view);
            }];
            self.m_detailModel.comId = [responDict objectForKey:@"comId"];
            // isSuperTrendy
            self.m_detailModel.isSuperTrendy = [[responDict objectForKey:@"isSuperTrendy"] boolValue];
            
            self.m_detailModel.goodsType = [responDict objectForKey:@"goodsType"];
            // baseInfo
            if ([responDict objectForKey:@"baseInfo"]) {
                NSDictionary *baseInfo = [responDict objectForKey:@"baseInfo"];
                self.m_detailModel.title = [baseInfo objectForKey:@"title"];
                self.m_detailModel.volumeText = [baseInfo objectForKey:@"volumeText"];

                NSMutableArray *bannerUrls = [NSMutableArray array];
                if ([baseInfo objectForKey:@"auctionImages"]) {
                    NSArray *auctionImages = [baseInfo objectForKey:@"auctionImages"];
                    [auctionImages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSDictionary *imageObj = (NSDictionary *)obj;
                        if ([imageObj objectForKey:@"url"]) {
                            [bannerUrls addObject:[imageObj objectForKey:@"url"]];
                        }
                    }];
                }
                self.m_detailModel.auctionImages = bannerUrls;
                //基础价格
                self.m_detailModel.priceInfo = [[NSArray modelArrayWithClass:[HomeGoodPriceInfoModel class] json:[baseInfo objectForKey:@"priceInfo"]] mutableCopy];
                //服务
                self.m_detailModel.serviceGuarantee = [baseInfo objectForKey:@"serviceGuarantee"];
                // 售卖量和总量
                self.m_detailModel.sellCountTitle = [baseInfo objectForKey:@"sellCountTitle"];
                self.m_detailModel.storageTitle = [baseInfo objectForKey:@"storageTitle"];
                // 限购量
                self.m_detailModel.limitTotal = [[baseInfo objectForKey:@"limitTotal"] integerValue];
                // 标题标签
                self.m_detailModel.tags = [baseInfo objectForKey:@"tags"];
                // 售卖状态
                self.m_detailModel.itemStatus = [baseInfo objectForKey:@"itemStatus"];
            }
            // 单品券
            if ([responDict objectForKey:@"itemCoupons"]) {
                NSArray *itemCoupons = [responDict objectForKey:@"itemCoupons"];
                itemCoupons = [NSArray modelArrayWithClass:[CouponListInfoModel class]
                                                      json:itemCoupons];
                if (kValidArray(itemCoupons)) {
                    self.m_detailModel.itemCoupons = [itemCoupons mutableCopy];
                }
            }
            self.m_detailModel.currentValue = 1;
            // detailImages
            if ([responDict objectForKey:@"detailImages"]) {
                NSDictionary *detail = [responDict objectForKey:@"detailImages"];
                NSArray *detailArrs = [NSArray modelArrayWithClass:[HomeGoodImageInfoModel class] json:detail];
                if (kValidArray(detailArrs)) {
                    self.m_detailModel.detailImages = [detailArrs mutableCopy];
                }
            }
            if ([responDict objectForKey:@"activityInfo"]) {
                self.m_detailModel.activityInfo = [responDict objectForKey:@"activityInfo"];
            }
            if ([responDict objectForKey:@"coupon"]) {
                self.m_detailModel.coupon = [responDict objectForKey:@"coupon"];
            }
            // saleInfo
            if ([responDict objectForKey:@"saleInfo"]) {
                NSDictionary *saleInfo = [responDict objectForKey:@"saleInfo"];
                self.m_detailModel.saleInfo = saleInfo;
                if ([saleInfo objectForKey:@"status"]) {
                    self.m_detailModel.salingType = [[saleInfo objectForKey:@"status"] integerValue];
                }
                if ([saleInfo objectForKey:@"startTime"]) {
                    self.m_detailModel.startTime = [[saleInfo objectForKey:@"startTime"] longValue];
                }
                if ([saleInfo objectForKey:@"endTime"]) {
                    self.m_detailModel.endTime = [[saleInfo objectForKey:@"endTime"] longValue];
                }
                if ([saleInfo objectForKey:@"currentTime"]) {
                    self.m_detailModel.currentTime = [[saleInfo objectForKey:@"currentTime"] longValue];
                }
                if ([saleInfo objectForKey:@"type"]) {
                    self.m_detailModel.specialType = [[saleInfo objectForKey:@"type"] integerValue];
                }
                
                self.m_detailModel.preTimeFormat = [NSString getPreHotShowTime:self.m_detailModel.startTime currentTime:self.m_detailModel.currentTime];
            }
            // propsInfo
            if ([responDict objectForKey:@"propsInfo"]) {
                NSDictionary *propsInfo = [responDict objectForKey:@"propsInfo"];
                if ([propsInfo objectForKey:@"baseProps"]) {
                    NSArray *props = [NSArray modelArrayWithClass:[HomeGoodArgumentInfoModel class] json:[propsInfo objectForKey:@"baseProps"]];
                    if (kValidArray(props)) {
                        self.m_detailModel.propsInfo = [props mutableCopy];
                    }
                }
                // 得到默认佣金，默认价格
                __block NSString *disPriceDefault = [[NSString alloc] init];
                __block NSString *comPriceDefault = [[NSString alloc] init];
                __block NSString *originPriceDefault = [[NSString alloc] init];
                [self.m_detailModel.priceInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    HomeGoodPriceInfoModel *priceModel = (HomeGoodPriceInfoModel *)obj;
                    if (priceModel.priceType == 1) {
                        originPriceDefault = priceModel.priceTag;
                    }
                    if (self.m_detailModel.salingType == YYGoodSalingStatusPreHot) {
                        if (priceModel.priceType == 3) {
                            disPriceDefault = priceModel.priceTag;
                            comPriceDefault = priceModel.commissionPriceTag;
                        }
                    }
                    else {
                        if (priceModel.priceType == 2) {
                            disPriceDefault = priceModel.priceTag;
                            comPriceDefault = priceModel.commissionPriceTag;
                        }
                    }
                }];
                self.m_detailModel.skuCommissionPrice = comPriceDefault;
                self.m_detailModel.skuDefaultPrice = disPriceDefault;
                self.m_detailModel.skuDefaultOriginPirce = originPriceDefault;
            }
            if ([responDict objectForKey:@"salesImage"]) {
                NSDictionary *salesDict = [responDict objectForKey:@"salesImage"];
                NSMutableArray *salesImage = [NSMutableArray array];
                if ([salesDict objectForKey:@"priceImage"]) {
                    YYImageInfoModel *infoModel = [YYImageInfoModel modelWithJSON:[salesDict objectForKey:@"priceImage"]];
                    if (infoModel) {
                        [salesImage addObject:infoModel];
                    }
                }
                if ([salesDict objectForKey:@"timingImage"]) {
                    YYImageInfoModel *infoModel = [YYImageInfoModel modelWithJSON:[salesDict objectForKey:@"timingImage"]];
                    if (infoModel) {
                        [salesImage addObject:infoModel];
                    }
                }
                self.m_detailModel.salesImage = salesImage;
            }
            // shareInfo
            if ([responDict objectForKey:@"shareInfo"]) {
                self.m_detailModel.shareInfo = [YYCompositeShareInfoModel modelWithJSON:[responDict objectForKey:@"shareInfo"]];
            }
            // barSwitch
            if ([responDict objectForKey:@"barSwitch"]) {
                self.m_detailModel.barSwitch = [responDict objectForKey:@"barSwitch"];
            }
            // notesLink
            if ([responDict objectForKey:@"notesLink"]) {
                self.m_detailModel.notesLink = [responDict objectForKey:@"notesLink"];
            }
            // itemType
            if ([responDict objectForKey:@"itemType"]) {
                self.m_detailModel.itemType = [responDict objectForKey:@"itemType"];
            }
            // tabNames
            if ([responDict objectForKey:@"tabNames"]) {
                self.m_detailModel.tabNames = [responDict objectForKey:@"tabNames"];
            }
            // skuBase 信息
            [self handlerGoodSKUMessage:responDict];
#pragma mark - 详情参数UI操作
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            //友盟记录商品参数
            if (self.m_detailModel.title) {
                [params setObject:self.m_detailModel.title forKey:@"商品名"];
            }
            if (self.m_detailModel.comId) {
                [params setObject:self.m_detailModel.comId forKey:@"商品ID"];
            }
            if(kValidString(kUserInfo.promotionCode)) {
                [params setObject:kUserInfo.promotionCode forKey:@"用户ID"];
            }

            [MobClick event:@"goodDetail" attributes:params];
            if (kIsFan) {
                [MobClick event:@"fanGoodDetail" attributes:params];
            }
            else {
                [MobClick event:@"memberGoodDetail" attributes:params];
            }
            
            self.headerView.specialMarkStatu = self.m_detailModel.specialType;
            self.headerView.bannerArrs = self.m_detailModel.auctionImages;
   
            [self setBottomToolView];
            [self bottomItemViewOperatorHandler];
            
            [self gainGoodCsdListRequest];
            self.detailDatas = [@[@(0),@(1),@(2)] mutableCopy];
            [self.m_baseTableView reloadData];
        }
        else {
            self.m_emptyType = YYEmptyViewUnderCarringType;
            self.m_baseTableView.scrollEnabled = NO;
            self.m_baseTableView.ly_emptyView = [YYEmptyView yyEmptyView:self.m_emptyType
                                                               title:nil
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.m_emptyType = YYEmptyViewOverTimeType;
        self.m_baseTableView.scrollEnabled = NO;
        self.m_baseTableView.ly_emptyView = [YYEmptyView yyEmptyView:self.m_emptyType
                                                           title:nil
                                                          target:self
                                                          action:@selector(emptyBtnOperator:)];
    }];
}
/**获取商品素材圈**/
- (void)gainGoodCsdListRequest {
    @weakify(self);
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if (self.commodityUrl) {
        [dict setObject:self.commodityUrl forKey:@"itemId"];
    }
    [dict setObject:@"" forKey:@"cursor"];
    [kWholeConfig gainGoodCsdCircleListRequest:dict
                                       success:^(NSDictionary *responDict) {
                                           @strongify(self);
                                           if (kValidDictionary(responDict)) {
                                               NSArray *moments = [NSArray modelArrayWithClass:[FriendCircleListInfoModel class] json:[responDict objectForKey:@"moments"]];
                                               if (kValidArray(moments) && !kIsFan) {
                                                   [self.detailDatas addObject:[moments firstObject]];
                                               }
                                           }
                                           [self gainGoodRecommendRequest];
                                       }];
}



/**获取推荐商品**/
- (void)gainGoodRecommendRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (!self.m_detailModel.comId) {
        return;
    }
//    [params setObject:self.m_detailModel.comId forKey:@"id"];
//    YYGoodRecommendInfoRequestAPI *recommendAPI = [[YYGoodRecommendInfoRequestAPI alloc] initGoodRecommendInfoRequest:params];
//    [YYCenterLoading showCenterLoading];
//    [recommendAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
//        NSDictionary *responDict = request.responseJSONObject;
//        [YYCenterLoading hideCenterLoading];
//        if (kValidDictionary(responDict)) {
//            if ([responDict objectForKey:@"items"]) {
//                NSArray *items = [NSArray modelArrayWithClass:[HomeListItemGoodInfoModel class] json:[responDict objectForKey:@"items"]];
//                if (kValidArray(items)) {
//                    self.recommendArray = [items mutableCopy];
//                    [self.recommendArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                        HomeListItemsInfoModel *model = (HomeListItemsInfoModel *)obj;
//                        model.scrollType = YYSingleRowScrollGoodRecommend;
//                    }];
//                    self.recommendModel = [[HomeListStructInfoModel alloc] init];
//                    self.recommendModel.scrollType = YYSingleRowScrollGoodRecommend;
//                    self.recommendModel.items = self.recommendArray;
//                    if (kValidArray(self.recommendArray)) {
//                        [self.detailDatas addObject:self.recommendArray];
//                        [self.m_baseTableView reloadData];
//                    }
//                }
//            }
//        }
//    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
//
//    }];
}


#pragma mark - Private method
/**
 * 处理详情导航栏基础设置
 */
- (void)setDetailNavigationBasicShow {
    @weakify(self);
    [self xh_alphaNavigation:0];
    [self xh_navBottomLine:XHClearColor];
    
    [self xh_addNavigationItemWithImageName:@"find_back_icon"
                                     isLeft:YES
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     if (kIsFan || ![self csdShowBackClickEvent]) {
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }
                                 }];
    
//    [self xh_addNavigationItemWithImageName:@"share_icon"
//                                     isLeft:NO
//                                 clickEvent:^(UIButton *sender) {
//                                     @strongify(self);
//                                     [MobClick event:@"good_rightshare"];
//                                     [self shareItemViewOperatorHander];
//                                 }];
    if (kIsFan) {
        [self xh_addTitle:@""];
    }
    else {
        self.csdBtn = [YYCreateTools createBtnImage:@"good_detail_csd_icon"];
        [self.navigationBar addSubview:self.csdBtn];
        [self.csdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.navigationBar.rightButton.mas_left).offset(-kSizeScale(10));
            make.centerY.equalTo(self.navigationBar.rightButton);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(30)));
        }];
        
        for (int i = 0; i < 2; i ++) {
            DetailColSwitchInfoModel *model = [DetailColSwitchInfoModel initDetailColClass:(i==0?@"商品":@"素材")];
            [self.menuDatas addObject:model];
        }
        self.topMenuBar.menuBarContents = self.menuDatas;
        [self xh_addTitleView:self.topMenuBar];
        [self.topMenuBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.navigationBar);
            make.centerY.equalTo(self.csdBtn);
            make.size.mas_equalTo(CGSizeMake(kSizeScale(100), kSizeScale(40)));
        }];
        
        self.csdBtn.actionBlock = ^(UIButton *btn) {
            @strongify(self);
            [self csdBtnClickEvent];
        };
        [self.topMenuBar reloadData];
        [self.topMenuBar updateItemSelectStateAtIndex:0];
    }
}

/**
 * 导航菜单项变化
 */
- (void)setDetailNavigationMenuChange:(CGFloat)offsetY {
    //顶部导航操作
    CGFloat alpha = 0.0;
    NSString *imageName, *shareImageName;
    if (offsetY > 200) {
        alpha = (offsetY - 200)/100;
        imageName = @"detail_navbar_back";
        shareImageName = @"share_small_icon";

        [self xh_addNavigationItemImageName:imageName isLeft:YES];
        [self xh_addNavigationItemImageName:shareImageName isLeft:NO];

        [self xh_alphaNavigation:alpha];
        [self xh_addNavigationItemAlpha:alpha isLeft:YES];
        [self xh_addNavigationItemAlpha:alpha isLeft:NO];
        if (kIsFan) {
            [self xh_addTitle:@"商品详情"];
        }
        else {
            self.csdBtn.alpha = 0;
            [self.csdBtn setEnabled:NO];
            self.topMenuBar.alpha = alpha;
            [self.topMenuBar setUserInteractionEnabled:YES];
        }
    }
    else {
        alpha = (200 - offsetY)/100;
        imageName = @"find_back_icon";
        shareImageName = @"share_icon";
        
        [self xh_addNavigationItemImageName:imageName isLeft:YES];
        [self xh_addNavigationItemImageName:shareImageName isLeft:NO];
        
        [self xh_alphaNavigation:0];
        [self xh_addNavigationItemAlpha:alpha isLeft:YES];
        [self xh_addNavigationItemAlpha:alpha isLeft:NO];
        if (kIsFan) {
            [self xh_addTitle:@""];
        }
        else {
            self.csdBtn.alpha = alpha;
            [self.csdBtn setEnabled:YES];
            self.topMenuBar.alpha = 0;
            [self.topMenuBar setUserInteractionEnabled:NO];
        }
    }
}
/**
 * 商品详情基础信息填充
 */
- (void)configureCell:(GoodDetailBasicInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO; // Enable to use "-sizeThatFits:"s
    cell.detailModel = self.m_detailModel;
    cell.goodNameStr = self.m_detailModel.title;
//    cell.sellCountTitle = self.m_detailModel.sellCountTitle;
    cell.storageTitle = self.m_detailModel.storageTitle;
}

/**
 处理SKU 信息
 **/
- (void)handlerGoodSKUMessage:(NSDictionary *)responDict {
    
    __block NSString  *maxValue = @"";
    __block NSString  *minValue = @"";
    __block NSString  *originMaxValue = @"";
    __block NSString  *originMinValue = @"";
    __block NSString  *maxCommissionValue = @"";
    __block NSString  *minCommissionValue = @"";
    __block NSMutableArray *allSKU = [NSMutableArray array];
    NSMutableArray *allProp = [NSMutableArray array];
    if ([responDict objectForKey:@"skuBase"]) {
        // 遍历组合skuid 和 sku其他信息
        NSDictionary *skuBase = [responDict objectForKey:@"skuBase"];
        NSArray *skuBaseArrs = [NSArray modelArrayWithClass:[HomeGoodBaseSkuInfoModel class] json:[skuBase objectForKey:@"skus"]];
        NSArray *skuInfo = [NSArray modelArrayWithClass:[HomeGoodSkuOtherInfoModel class] json:[skuBase objectForKey:@"skuInfo"]];
        // 遍历获取佣金的最大值或最小值
        [skuInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodSkuOtherInfoModel *model = (HomeGoodSkuOtherInfoModel *)obj;
            for (int i = 0; i < model.priceInfo.count; i ++) {
                HomeGoodPriceInfoModel *priceModel = [model.priceInfo objectAtIndex:i];
                if (self.m_detailModel.salingType == YYGoodSalingStatusNoraml) {
#pragma mark - 注意这里commissionPriceTag 和 commissionPrice 的区别，一个除以100，一个没有。
                    if (priceModel.priceType == 2) {
                        if ([maxCommissionValue floatValue] == 0.0 || [minCommissionValue floatValue] == 0.0) {
                            maxCommissionValue = priceModel.commissionPriceTag;
                            minCommissionValue = priceModel.commissionPriceTag;
                            continue;
                        }
                        if ([priceModel.commissionPrice floatValue] > [maxCommissionValue floatValue]) {
                            maxCommissionValue = priceModel.commissionPriceTag;
                        }
                        if ([priceModel.commissionPrice floatValue] < [minCommissionValue floatValue]) {
                            minCommissionValue = priceModel.commissionPriceTag;
                        }
                    }
                }else{
                    if (priceModel.priceType == 3) {
                        if ([maxCommissionValue floatValue] == 0.0 || [minCommissionValue floatValue] == 0.0) {
                            maxCommissionValue = priceModel.commissionPriceTag;
                            minCommissionValue = priceModel.commissionPriceTag;
                            continue;
                        }
                        if ([priceModel.commissionPrice floatValue] > [maxCommissionValue floatValue]) {
                            maxCommissionValue = priceModel.commissionPriceTag;
                        }
                        if ([priceModel.commissionPrice floatValue] < [minCommissionValue floatValue]) {
                            minCommissionValue = priceModel.commissionPriceTag;
                        }
                    }
                }
            }
        }];
        
        // 遍历价格的最大值或最小值
        [skuInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodSkuOtherInfoModel *model = (HomeGoodSkuOtherInfoModel *)obj;
            for (int i = 0; i < model.priceInfo.count; i ++) {
                HomeGoodPriceInfoModel *priceModel = [model.priceInfo objectAtIndex:i];
                
                if (priceModel.priceType == 1) {
                    if ([originMaxValue floatValue] == 0.0 || [originMinValue floatValue] == 0.0) {
                        originMaxValue = priceModel.priceTag;
                        originMinValue = priceModel.priceTag;
                        continue;
                    }
                    if ([priceModel.priceTag floatValue] > [originMaxValue floatValue]) {
                        originMaxValue = priceModel.priceTag;
                    }
                    if ([priceModel.priceTag floatValue] < [originMinValue floatValue]) {
                        originMinValue = priceModel.priceTag;
                    }
                }
                if (self.m_detailModel.salingType == YYGoodSalingStatusNoraml) {
                    if (priceModel.priceType == 2) {
                        if ([maxValue floatValue] == 0.0 || [minValue floatValue] == 0.0) {
                            maxValue = priceModel.priceTag;
                            minValue = priceModel.priceTag;
                            continue;
                        }
                        if ([priceModel.priceTag floatValue] > [maxValue floatValue]) {
                            maxValue = priceModel.priceTag;
                        }
                        if ([priceModel.priceTag floatValue] < [minValue floatValue]) {
                            minValue = priceModel.priceTag;
                        }
                    }
                }
                else {
                    if (priceModel.priceType == 3) {
                        if ([maxValue floatValue] == 0.0 || [minValue floatValue] == 0.0) {
                            maxValue = priceModel.priceTag;
                            minValue = priceModel.priceTag;
                            continue;
                        }
                        if ([priceModel.priceTag floatValue] > [maxValue floatValue]) {
                            maxValue = priceModel.priceTag;
                        }
                        if ([priceModel.priceTag floatValue] < [minValue floatValue]) {
                            minValue = priceModel.priceTag;
                        }
                    }
                }
            }
        }];
        self.m_detailModel.priceMaxValue = maxValue;
        self.m_detailModel.priceMinValue = minValue;
        
        self.m_detailModel.originMinValue = originMinValue;
        self.m_detailModel.originMaxValue = originMaxValue;
        
        self.m_detailModel.commissionMaxValue = maxCommissionValue;
        self.m_detailModel.commissionMinValue = minCommissionValue;
        
        for (int i = 0; i < skuBaseArrs.count; i ++) {
            __block HomeGoodBaseSkuInfoModel *model = [skuBaseArrs objectAtIndex:i];
            [skuInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                HomeGoodSkuOtherInfoModel *infoModel = (HomeGoodSkuOtherInfoModel *)obj;
                if ([model.skuId integerValue] == [infoModel.skuId integerValue]) {
                    HomeGoodValueSkuInfoModel *allModel = [[HomeGoodValueSkuInfoModel alloc] init];
                    allModel.skuId = infoModel.skuId;
                    allModel.propPath = model.propPath;
                    allModel.imageUrl = infoModel.skuUrl;
                    allModel.storage = infoModel.storage;
                    allModel.priceInfo = infoModel.priceInfo;
                    [allSKU addObject:allModel];
                    *stop = YES;
                }
            }];
        }
        // 赋值组合的所以SKU
        self.m_detailModel.skus = allSKU;
        // 组合最终需要的sku信息
        NSDictionary *props = [skuBase objectForKey:@"props"];
        __block NSMutableArray *propsArrs = [[NSArray modelArrayWithClass:[HomeGoodPropSkuInfoModel class] json:props] mutableCopy];
        // 组合sku 提示信息， 仅仅当sku 为1个时，默认选中时使用。
        NSString *skuPropTip = @"请选择";
        __block NSString  *skuTips = [NSString new];
        self.m_detailModel.originProps = propsArrs;
        for (int i = 0; i < propsArrs.count; i ++) {
            HomeGoodPropSkuInfoModel *skuModel = [propsArrs objectAtIndex:i];
            [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                NSInteger      skuStorage = 0;
                NSMutableArray *childArrs = [NSMutableArray array];
                HomeGoodValueSkuInfoModel *infoModel = (HomeGoodValueSkuInfoModel *)obj;
                infoModel.pid = [NSString stringWithFormat:@"%@",skuModel.pid];
                if (!kValidString(skuTips)) {
                    skuTips = infoModel.name;
                }
                else {
                    skuTips = [skuTips stringByAppendingFormat:@"、%@", infoModel.name];
                }
                //拼接一个属性子sku
                NSString *combineStr = [NSString stringWithFormat:@"%@:%@",skuModel.pid, infoModel.vid];
                //在所有sku 里查询包含当前拼接成的子sku。
                for (int j = 0; j < allSKU.count; j ++) {
                    HomeGoodValueSkuInfoModel *tempModel = allSKU[j];
                    if ([tempModel.propPath rangeOfString:combineStr].location != NSNotFound) {
                        //查询到，对sku进行字符串分离成对应数组
                        NSArray *splitArr = [tempModel.propPath componentsSeparatedByString:@";"];
                        //如果分离得到的数组不是大于1，则不做处理。 否则继续分离赋值。
                        if (splitArr.count > 1) {
                            NSString *secondSubProp = [splitArr lastObject];
                            
                            NSArray *subSplitArr = [secondSubProp componentsSeparatedByString:@":"];
                            tempModel.pid = [subSplitArr firstObject];
                            tempModel.vid = [subSplitArr lastObject];
                            tempModel.name = [self matchPidAndVidMappingName:propsArrs
                                                                        pid:tempModel.pid
                                                                        vid:tempModel.vid];
                        }
                        [childArrs addObject:tempModel];
                        skuStorage = skuStorage + [tempModel.storage integerValue];
                        continue;
                    }
                }
                // 判断是否只有一个sku
                if (kValidArray(allSKU) && allSKU.count == 1) {
                    //如果是默认选中
                    infoModel.isSelect = YES;
                }
                infoModel.storage = [NSNumber numberWithInteger:skuStorage];
                if (kValidArray(propsArrs)&&propsArrs.count <= 1) {
                    // 单属性时，提前判断一下库存是否为0. 
                    infoModel.isNull = ([infoModel.storage integerValue]<=0?YES:NO);
                }
                infoModel.childArrs = childArrs;
            }];
            skuPropTip = [skuPropTip stringByAppendingString:kValidString(skuModel.name)?skuModel.name:@""];
            [allProp addObject:skuModel];
        }
        
        // 处理一款商品多个属性，其中一个属性只有一个值的时候默认选中。
        [allProp enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPropSkuInfoModel *propModel = (HomeGoodPropSkuInfoModel *)obj;
            if (kValidArray(propModel.values) && propModel.values.count <= 1) {
                HomeGoodValueSkuInfoModel *skuModel = [propModel.values firstObject];
                skuModel.isSelect = YES;
                self.selectSkuModel = skuModel;
                *stop = YES;
            }
        }];
        
        if (self.selectSkuModel) {
            __block NSString *spliceStr = [NSString new];
            [allProp enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                 HomeGoodPropSkuInfoModel *propModel = (HomeGoodPropSkuInfoModel *)obj;
                if ([propModel.pid integerValue] != [self.selectSkuModel.pid integerValue]) {
                    spliceStr = [NSString stringWithFormat:@"请选择%@", propModel.name];
                    *stop = YES;
                }
            }];
            self.selectSpec = spliceStr;
        }
        
        // 判断是否只有一个sku
        if (kValidArray(allSKU) && allSKU.count == 1) {
            //如果是默认选中, 赋值默认选中的提示，默认选中的skuModel;
            self.selectSpec = skuTips;
            self.selectSkuModel = [allSKU firstObject];
        }
        self.m_detailModel.propTipInfo = skuPropTip;
        // 赋值组合的所以属性信息
        self.m_detailModel.props = allProp;
    }
}

/**
 匹配pid 和 vid 获取name
 **/
- (NSString *)matchPidAndVidMappingName:(NSArray *)props
                                    pid:(NSString *)pid
                                    vid:(NSString *)vid {
    __block NSString *name = nil;
    __block NSString *tempPid = pid;
    __block NSString *tempVid = vid;
    for (int i = 0; i < props.count; i ++) {
        HomeGoodPropSkuInfoModel *skuModel = [props objectAtIndex:i];
        [skuModel.values enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodValueSkuInfoModel *tempModel = (HomeGoodValueSkuInfoModel *)obj;
            if ([skuModel.pid integerValue] == [tempPid integerValue] && [tempModel.vid integerValue] == [tempVid integerValue]) {
                name = tempModel.name;
                *stop = YES;
            }
        }];
    }
    return name;
}

/**初始化底部按钮**/
- (void)setBottomToolView {
    CGFloat otherWidth = kScreenW - kSizeScale(75);
    
    UIView *backgroundView = [YYCreateTools createView:XHWhiteColor];
    [self.m_bottomView addSubview:backgroundView];
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.m_bottomView);
        make.height.mas_equalTo(kSizeScale(50));
    }];

    self.cartBtn = [[YYButton alloc] initWithFrame:CGRectZero];
    self.cartBtn.normalTitle = @"购物车";
    self.cartBtn.selectTitle = @"购物车";
    self.cartBtn.space = kSizeScale(3);
    self.cartBtn.titleFont = midFont(11);
    self.cartBtn.normalImageName = @"good_detail_cart_icon";
    self.cartBtn.selectImageName = @"good_detail_cart_icon";
    [backgroundView addSubview:self.cartBtn];
    
    self.tryLookBtn = [[YYButton alloc] initWithFrame:CGRectZero];
    self.tryLookBtn.normalTitle = @"免费试看";
    self.tryLookBtn.selectTitle = @"免费试看";
    self.tryLookBtn.titleTextColor = XHRedColor;
    self.tryLookBtn.space = kSizeScale(5);
    self.tryLookBtn.titleFont = midFont(9);
    self.tryLookBtn.normalImageName = @"good_detail_audio_icon";
    self.tryLookBtn.selectImageName = @"good_detail_audio_icon";
    [backgroundView addSubview:self.tryLookBtn];
    
    [self.cartBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(backgroundView);
        make.bottom.equalTo(backgroundView.mas_bottom);
        make.width.mas_equalTo(kSizeScale(75));
    }];
    self.cartBtn.select = YES;
    
    // 加入购物车
    self.addBtn = [[YYOrderShowView alloc] initWithFrame:CGRectZero];
    self.addBtn.topSpace = (IS_IPHONE_5 ? kSizeScale(12) : kSizeScale(9));
    self.addBtn.itemSpace = 0;
    [self.addBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addCartClickEvent:)]];
    [backgroundView addSubview:self.addBtn];
    
    // 立即购买
    self.buyBtn = [[YYOrderShowView alloc] initWithFrame:CGRectZero];
    self.buyBtn.topSpace = (IS_IPHONE_5 ? kSizeScale(12) : kSizeScale(9));
    self.buyBtn.itemSpace = 0;
    [self.buyBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buyBtnClickEvent:)]];
    [backgroundView addSubview:self.buyBtn];
    
    //蜀黍专区立即购买
    self.mealZoneBuyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.mealZoneBuyBtn.backgroundColor = XHMainColor;
    [self.mealZoneBuyBtn setTitleColor:XHWhiteColor forState:UIControlStateNormal];
    [self.mealZoneBuyBtn setTitle:@"立即购买" forState:UIControlStateNormal];
    self.mealZoneBuyBtn.titleLabel.font = MediumFont(18);
    self.mealZoneBuyBtn.hidden = YES;
    [self.mealZoneBuyBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mealZoneBuyBtnClickEvent:)]];
    [backgroundView addSubview:self.mealZoneBuyBtn];
    [self.mealZoneBuyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.bottom.equalTo(backgroundView);
    }];
    
    self.m_detailModel.isMealZonePay = (self.m_detailModel.shareInfo.payment.integerValue == 2);
//    self.m_detailModel.isMealZonePay = YES;
    self.mealZoneBuyBtn.hidden = !self.m_detailModel.isMealZonePay;
    
    if ([self.m_detailModel.itemStatus intValue] != 1 || (self.isFan && self.m_detailModel.salingType == YYGoodSalingStatusPreHot)) {  //售空，下架，粉丝的预热
        [self.addBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.cartBtn.mas_right);
            make.top.right.bottom.equalTo(backgroundView);
        }];
    }
    else {
        if (self.m_detailModel.specialType == 1 && self.isFan) {
            // 新人特卖
            [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.cartBtn.mas_right);
                make.top.right.bottom.equalTo(backgroundView);
            }];
        }
        else {
            [self.addBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.cartBtn.mas_right);
                make.top.bottom.equalTo(self.cartBtn);
                make.width.mas_equalTo(otherWidth/2.0);
            }];
            [self.buyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.addBtn.mas_right);
                make.top.right.bottom.equalTo(backgroundView);
            }];
        }
    }
    
    if ([self.m_detailModel.barSwitch integerValue] != 0) {
        if (!kIsFan) {
            //展示试看
            self.cartBtn.space = kSizeScale(3);
            self.cartBtn.titleFont = midFont(9);
            
            self.tryLookBtn.select = YES;
            CGFloat width = kSizeScale(105) - kSizeScale(1);
            CGFloat otherWidth = kScreenW - width;
            
            UIView *midLine = [YYCreateTools createView:HexRGB(0xF1F1F1)];
            [backgroundView addSubview:midLine];
            
            [self.cartBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.top.equalTo(backgroundView);
                make.bottom.equalTo(backgroundView.mas_bottom);
                make.width.mas_equalTo(width/2.0);
            }];
            
            [midLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self.cartBtn);
                make.left.equalTo(self.cartBtn.mas_right);
                make.size.mas_equalTo(CGSizeMake(kSizeScale(1), kSizeScale(29)));
            }];
            
            [self.tryLookBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.width.equalTo(self.cartBtn);
                make.left.equalTo(self.cartBtn.mas_right).offset(kSizeScale(1));
            }];
            
            [self.addBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.tryLookBtn.mas_right);
                make.top.bottom.equalTo(self.tryLookBtn);
                make.width.mas_equalTo(otherWidth/2.0);
            }];
            
            [self.buyBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.addBtn.mas_right);
                make.top.right.bottom.equalTo(backgroundView);
            }];
        }
    }
}

//蜀黍专区立即购买点击
- (void)mealZoneBuyBtnClickEvent: (UIButton *)button {
    if ([self.m_detailModel.itemStatus intValue] != 1 || (self.m_detailModel.salingType == YYGoodSalingStatusPreHot)) {
        if (self.m_detailModel.salingType == YYGoodSalingStatusPreHot) {
            [YYCommonTools showTipMessage:[NSString stringWithFormat:@"%@开抢",self.m_detailModel.preTimeFormat]];
            return;
        }
        if ([self.m_detailModel.itemStatus integerValue] != 1) {
            //0:已抢光；1：在售中；2：已下架
            if ([self.m_detailModel.itemStatus integerValue] == 0) {
                [YYCommonTools showTipMessage:@"商品已抢光"];
            }
            else {
                [YYCommonTools showTipMessage:@"商品已下架"];
            }
            return;
        }
    }
    if ([self.m_detailModel.barSwitch integerValue] != 0) {
        if (kIsFan) {
            [self virtualOperator];
            return ;
        }
    }
    [self skuSelectViewOperator];
}

/**底部操作按钮**/
- (void)bottomItemViewOperatorHandler {
    @weakify(self);
    CGFloat badgeOffx = [self.m_detailModel.barSwitch integerValue] == 0 ? kSizeScale(30) : (kIsFan?kSizeScale(30):kSizeScale(20));
    [self.cartBtn layoutIfNeeded];
    [self.cartBtn setBadgeOffset:CGPointMake(-(badgeOffx), kSizeScale(10))];
    [self.cartBtn setBadgeBgColor:XHRedColor];
    [self.badgeController observePath:kShippingDetailRedCountPath
                            badgeView:self.cartBtn
                                block:^(id  _Nullable observer, NSDictionary<NSString *,id> * _Nonnull info) {
                                    NSInteger tempCount = [[info objectForKey:@"RJBadgeCountKey"] integerValue];
                                    [self.cartBtn showBadgeWithValue:tempCount];
                                }];
    
    __block NSString *minCommissionValue = self.m_detailModel.commissionMinValue;
    if (self.selectSkuModel && [self.selectSkuModel.skuId integerValue] > 0) {
        [self.selectSkuModel.priceInfo enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HomeGoodPriceInfoModel *priceModel = (HomeGoodPriceInfoModel *)obj;
            
            if (self.m_detailModel.salingType == YYGoodSalingStatusNoraml) {
                if (priceModel.priceType == 2) {
                    minCommissionValue = priceModel.commissionPriceTag;
                    *stop = YES;
                }
            }else{
                if (priceModel.priceType == 3) {
                    minCommissionValue = priceModel.commissionPriceTag;
                    *stop = YES;
                }
            }
        }];
    }
    
    self.cartBtn.btnClickBlock = ^(BOOL status) {
        @strongify(self);
        if(!kIsLogin) {
            [YYCommonTools pushToLogin];
            return;
        }
        [YYCommonTools skipCart:self];
    };
    
    self.tryLookBtn.btnClickBlock = ^(BOOL status) {
        @strongify(self);
        if(!kIsLogin) {
            [YYCommonTools pushToLogin];
            return;
        }
        [self virtualOperator];
    };
    
    if ([self.m_detailModel.itemStatus intValue] != 1) {
        // 已抢完
        NSMutableAttributedString *topText = [[NSMutableAttributedString alloc] initWithString:[self.m_detailModel.itemStatus intValue] != 0 ?@"已下架" :@"已抢光"];
        [topText yy_setAttributes:XHWhiteColor
                             font:boldFont(17)
                          content:topText
                        alignment:NSTextAlignmentCenter];
        self.addBtn.backgroundColor = HexRGB(0xaaaaaa);
        self.addBtn.bottomAttributed = topText;
    }
    else {
        NSMutableAttributedString *topText = [[NSMutableAttributedString alloc] initWithString:self.isFan?(self.m_detailModel.salingType == YYGoodSalingStatusPreHot?[NSString stringWithFormat:@"%@开抢",self.m_detailModel.preTimeFormat]:@"加入购物车"):(self.m_detailModel.salingType == YYGoodSalingStatusPreHot?[NSString stringWithFormat:@"%@开抢",self.m_detailModel.preTimeFormat]:@"自购")];
        [topText yy_setAttributes:XHWhiteColor
                             font:self.isFan?midFont(17):midFont(16)
                          content:topText
                        alignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *bottomText = [[NSMutableAttributedString alloc] initWithString:self.isFan?((self.m_detailModel.salingType == YYGoodSalingStatusPreHot)?@"":@"立即购买"):@"分享"];
        [bottomText yy_setAttributes:XHWhiteColor
                                font:self.isFan?midFont(17):midFont(16)
                             content:bottomText
                           alignment:NSTextAlignmentCenter];
        
        NSMutableAttributedString *topCommission = [YYCommonTools containSpecialSymbolHandler:@"省" symbolFont:midFont(11) symbolTextColor:XHWhiteColor wordSpace:1 price:[NSString stringWithFormat:@"%@", minCommissionValue] priceFont:midFont(11) priceTextColor:XHWhiteColor symbolOffsetY:0];
        
        NSMutableAttributedString *bottomCommission = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(11) symbolTextColor:XHWhiteColor wordSpace:1 price:[NSString stringWithFormat:@"%@", minCommissionValue] priceFont:midFont(11) priceTextColor:XHWhiteColor symbolOffsetY:0];
        
        if (self.isFan) {
            self.addBtn.topSpace = kSizeScale(17);
            self.buyBtn.topSpace = kSizeScale(17);
            self.addBtn.bottomAttributed = topText;
            self.buyBtn.bottomAttributed = bottomText;
        }
        else {
            self.addBtn.topAttributed = topText;
            self.addBtn.bottomAttributed = topCommission;
            
            self.buyBtn.topAttributed = bottomText;
            self.buyBtn.bottomAttributed = bottomCommission;
        }
        
        [self.addBtn layoutIfNeeded];
        [self.buyBtn layoutIfNeeded];
        
//        UIColor *goldColor = [UIColor addGoldGradient:CGSizeMake(self.buyBtn.frame.size.width, self.buyBtn.frame.size.height)];
        UIColor *goldColor = XHMainColor;
        UIColor *deepBlueColor = [UIColor addDeepBlueGradient:CGSizeMake(self.addBtn.frame.size.width, self.addBtn.frame.size.height)];
        self.addBtn.backgroundColor = (self.m_detailModel.salingType == YYGoodSalingStatusPreHot?HexRGB(0x11BC56):XHMainColor);
        
        self.buyBtn.backgroundColor = HexRGB(0xE6B96E);
//         [self.buyBtn setBackgroundImage:[UIImage gradientColorImageFromColors:@[HexRGB(0xE6B96E), HexRGB(0xE2A848)] gradientType:GradientTypeLeftToRight imgSize:CGSizeMake(self.buyBtn.frame.size.width, self.buyBtn.frame.size.height)] forState:UIControlStateNormal];
        
        if ([self.m_detailModel.barSwitch integerValue] != 0) {
            // 虚拟课程，试看操作
            if (kIsFan) {
                self.addBtn.topSpace = kSizeScale(17);
                NSMutableAttributedString *contentAttribute = [[NSMutableAttributedString alloc] initWithString:@"免费试看 "];
                UIImage *image = [UIImage imageNamed:@"good_detail_fan_audio"];
                NSMutableAttributedString *postAttribute = [NSMutableAttributedString attachmentStringWithContent:image contentMode:UIViewContentModeScaleAspectFit attachmentSize:CGSizeMake(18, 14) alignToFont:normalFont(12) alignment:YYTextVerticalAlignmentTop];
                [contentAttribute appendAttributedString:postAttribute];
                [contentAttribute yy_setAttributes:XHWhiteColor
                                        font:midFont(16)
                                     content:contentAttribute
                                   alignment:NSTextAlignmentCenter];
                self.addBtn.backgroundColor = HexRGB(0xFF9701);
                self.addBtn.bottomAttributed = contentAttribute;
            }
        }
    }
}


- (void)shareItemViewOperatorHander {
    if (self.m_detailModel.shareInfo) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (kValidString(self.m_detailModel.shareInfo.commission)) {
            [params setObject:self.m_detailModel.shareInfo.commission
                       forKey:@"commission"];
        }
        if (kValidDictionary(self.m_detailModel.coupon)) {
            NSDictionary *coupon = self.m_detailModel.coupon;
            if ([coupon objectForKey:@"discountPrice"]) {
                self.m_detailModel.shareInfo.discountPrice = [coupon objectForKey:@"discountPrice"];
            }
            if ([coupon objectForKey:@"maxDenomination"]) {
                self.m_detailModel.shareInfo.maxDenomination = [coupon objectForKey:@"maxDenomination"];
            }
        }
        NSMutableDictionary *qrcodeParams = [NSMutableDictionary dictionary];
        if (kValidString(self.commodityUrl)) {
            [qrcodeParams setObject:self.commodityUrl forKey:@"id"];
        }
        [kWholeConfig produceGoodQRCodeRequest:qrcodeParams
                                       success:^(NSDictionary *responDict) {
                                           if ([responDict objectForKey:@"goodDetailQRcode"]) {
                                               self.m_detailModel.shareInfo.specialType = self.m_detailModel.specialType;
                                               self.m_detailModel.shareInfo.qrcodePictureUrl = [responDict objectForKey:@"goodDetailQRcode"];
                                               [YYCommonTools goodShareOperator:self.m_detailModel.shareInfo
                                                                   expandParams:params];
                                           }
                                       }];
    }
}

#pragma mark - Event
/**
 *  素材按钮点击事件
 */
- (void)csdBtnClickEvent {
    if (self.topMenuBar) {
        self.topMenuBar.currentItemIndex = 1;
        [self.topMenuBar updateItemSelectStateAtIndex:1];
    }
    [self setDetailNavigationMenuChange:300];
    if (!self.csdView.isOpen) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (self.commodityUrl) {
            [params setObject:self.commodityUrl forKey:@"itemId"];
        }
        self.csdView.params = params;
        [self.csdView showCsdView];
    }
}

/**
 *  素材圈覆层显示时，点击返回按钮的操作
 */
- (BOOL)csdShowBackClickEvent {
    __block BOOL isPause = NO;
    CGFloat offSetY = self.m_baseTableView.contentOffset.y;
    if (self.topMenuBar) {
        self.topMenuBar.currentItemIndex = 0;
        [self.topMenuBar updateItemSelectStateAtIndex:0];
    }
    [self setDetailNavigationMenuChange:offSetY];
    self.topMenuBar.alpha = offSetY >= 200 ? 1.0 : 0.0;
    if (self.csdView.isOpen) {
        [self.csdView hideCsdView];
        isPause = YES;
    }
    return isPause;
}

/**
 *  虚拟课程试看操作
 */
- (void)virtualOperator {
    CGFloat offsetY = self.m_baseTableView.contentOffset.y;
    CGFloat targetY = self.m_baseTableView.contentSize.height-kScreenH;
    if (offsetY < targetY && !self.isCanNotMoveTabView) {
        [self.m_baseTableView setContentOffset:CGPointMake(0, targetY) animated:YES];
        self.m_detailModel.isSwitchBar = YES;
        if (self.descriCell) {
            self.descriCell.goodModel = self.m_detailModel;
        }
    }
    else {
        kPostNotification(CourseDetailScrollTopNotification, nil);
    }
}



- (void)showService{
    self.serviceShowView = [ServiceShowView initYYPopView:[[ServiceShowView alloc] initServicetView] base:self];
    [self.serviceShowView showViewOfAnimateType:YYPopAnimateDownUp];
}

/**SKU 选择**/
- (void)skuSelectViewOperator {
    @weakify(self);
    self.skuSelectView = [YYSkuSelectView initYYPopView:[[YYSkuSelectView alloc] initSkuSelectView]];
    [self.skuSelectView showViewOfAnimateType:YYPopAnimateDownUp];
    self.skuSelectView.goodInfo = self.m_detailModel;
    self.skuSelectView.selectBlock = ^(NSString *showTip, HomeGoodValueSkuInfoModel *selectModel, NSInteger currentValue, YYSkuSelectViewDisType disType) {
        @strongify(self);
        if (disType == YYSkuSelectViewDisAddCart) {
            [MobClick event:@"good_cart"];
            [YYAddCartTipView showAddCartTipBelowSubview:self.m_bottomView
                                           tipClickBlock:^{
                                               @strongify(self);
                                               [YYCommonTools skipCart:self];
                                               return ;
                                           }];
        }
        if (selectModel) {
            self.selectSpec = showTip;
        }
        else {
            self.selectSpec = nil;
        }
        self.selectSkuModel = selectModel;
        self.m_detailModel.currentValue = currentValue;
        [self bottomItemViewOperatorHandler];
        [self.m_baseTableView reloadData];
    };
}

- (void)goodDetailNotificationHandler:(NSNotification *)notification {
    if ([notification.name isEqualToString:GoodDetailLeaveTopNotification]) {
        self.canScroll = YES;
    }
}

- (void)refreshGoodDetailNotificationHandler:(NSNotification *)notification {
    [self callBlockGoodDetailRequest];
}

- (void)addCartClickEvent:(UITapGestureRecognizer *)tapGesture {
    if(!kIsLogin) {
        [YYCommonTools pushToLogin];
        return;
    }
    if ([self.m_detailModel.itemStatus intValue] != 1 || (self.m_detailModel.salingType == YYGoodSalingStatusPreHot)) {
        if (self.m_detailModel.salingType == YYGoodSalingStatusPreHot) {
            [YYCommonTools showTipMessage:[NSString stringWithFormat:@"%@开抢",self.m_detailModel.preTimeFormat]];
            return;
        }
        if ([self.m_detailModel.itemStatus integerValue] != 1) {
            //0:已抢光；1：在售中；2：已下架
            if ([self.m_detailModel.itemStatus integerValue] == 0) {
                [YYCommonTools showTipMessage:@"商品已抢光"];
            }
            else {
                [YYCommonTools showTipMessage:@"商品已下架"];
            }
            return;
        }
    }
    if ([self.m_detailModel.barSwitch integerValue] != 0) {
        if (kIsFan) {
            [self virtualOperator];
            return ;
        }
    }
    [self skuSelectViewOperator];
}

- (void)buyBtnClickEvent:(UITapGestureRecognizer *)tapGesture {
    if(!kIsLogin) {
        [YYCommonTools pushToLogin];
        return;
    }
    [MobClick event:@"good_buy"];
    if (self.isFan) {
        if ((self.m_detailModel.salingType == YYGoodSalingStatusPreHot)) {
            return;
        }
        [self skuSelectViewOperator];
    }
    else {
        [MobClick event:@"good_bottonshare"];
        [self shareItemViewOperatorHander];
    }
}

- (void)emptyBtnOperator:(UIButton *)sender {
    if (self.m_emptyType == YYEmptyViewUnderCarringType) {
        [YYCommonTools tabbarSelectIndex:0];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else {
        [self callBlockGoodDetailRequest];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.detailDatas.count > 0 ? self.detailDatas.count + 1 : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    Class current = [GoodDetailBasicInfoCell class];
    if (kValidArray(self.detailDatas) && row < self.detailDatas.count) {
        if ([[self.detailDatas objectAtIndex:row] isKindOfClass:[NSNumber class]]) {
            if (row == 0) {
                // 商品信息
                GoodDetailBasicInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                [self configureCell:cell atIndexPath:indexPath];
                return cell;
            }
            else if (row == 1) {
                // 活动
                current = [GoodDetailDynamicInfoCell class];
                GoodDetailDynamicInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                [cell setDynamicCell:self.m_detailModel.coupon
                            activity:self.m_detailModel.activityInfo
                            oldactivity:self.activityModel];
                cell.serviceLists = self.m_detailModel.serviceGuarantee;
                cell.serviceBlock = ^{
                    @strongify(self);
                    [self showService];
                };

                cell.couponBlock = ^{
                    @strongify(self);
                    self.chooseCouponView = [GoodDetailCouponView initYYPopView:[[GoodDetailCouponView alloc] initCouponView] base:self];
                    [self.chooseCouponView showViewOfAnimateType:YYPopAnimateDownUp];
                    self.chooseCouponView.CouponArr = [[self.m_detailModel.coupon objectForKey:@"itemCouponV2s"] mutableCopy];
                    self.chooseCouponView.callBack = ^(NSMutableArray *changeArr) {
                        @strongify(self);
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.m_detailModel.coupon ];
                    [dic setObject:changeArr forKey:@"itemCouponV2s"];
                        self.m_detailModel.coupon = dic;
                    };
                    self.chooseCouponView.receiveFinish = ^(HomeBannerInfoModel *bannerModel) {
                        YYActivityPopView *popView = [YYActivityPopView initYYPopView:[[YYActivityPopView alloc] init]];
                        popView.params = [@{@"activityModel":bannerModel} mutableCopy];
                        @weakify(popView);
                        [popView showViewOfAnimateType:YYPopAnimateScale];
                        popView.operatorBlock = ^(NSInteger operatorType) {
                            @strongify(popView);
                            @strongify(self);
                            NSMutableDictionary *params = [NSMutableDictionary dictionary];
                            [params setObject:@(3) forKey:@"linkType"];
                            if (kValidString(bannerModel.url)) {
                                [params setObject:[NSString stringWithFormat:@"%@%@type=%d&promotionCode=%@",kEnvConfig.webDomin,bannerModel.url,kUserInfo.level,kUserInfo.promotionCode] forKey:@"url"];
                            }
                            [YYCommonTools skipMultiCombinePage:self params:params];
                            [popView showViewOfAnimateType:YYPopAnimateScale];
                        };
                    };
                };
                return cell;
            }
            else {
                // sku
                current = [GoodDetailSkuInfoCell class];
                GoodDetailSkuInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                cell.selectTip = self.selectSpec;
                cell.clickSelectSkuBlock = ^{
                    @strongify(self);
                    if(!kIsLogin) {
                        [YYCommonTools pushToLogin];
                        return;
                    }
                    if (kIsTourist) {
                        [YYCommonTools showTipMessage:@"请先登录/注册"];
                        return ;
                    }
                    if ([self.m_detailModel.itemStatus intValue] != 1) {
                        return;
                    }
                    [self skuSelectViewOperator];
                };
                return cell;
            }
        }
        else if ([[self.detailDatas objectAtIndex:row] isKindOfClass:[FriendCircleListInfoModel class]]) {
            current = [GoodDetailCircleCsdInfoCell class];
            GoodDetailCircleCsdInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
            if ([self.detailDatas objectAtIndex:row]) {
                FriendCircleListInfoModel *infoModel = [self.detailDatas objectAtIndex:row];
                cell.circleModel = infoModel;
            }
            cell.goodDetailCsdCircleBlock = ^{
                @strongify(self);
                [self csdBtnClickEvent];
            };
            return cell;
        }
        else {
            current = [GoodDetailRecommendInfoCell class];
            GoodDetailRecommendInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
            cell.delegate = self;
            cell.itemModels = self.recommendModel;
            //隐藏为你推荐
            cell.hidden = YES;
            return cell;
        }
    }
    else {
        current = [GoodDetailDescriInfoCell class];
        GoodDetailDescriInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
        self.descriCell = cell;
        cell.backgroundColor = XHRedColor;
        cell.goodModel = self.m_detailModel;
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (kValidArray(self.detailDatas) && row < self.detailDatas.count) {
        if ([[self.detailDatas objectAtIndex:row] isKindOfClass:[NSNumber class]]) {
            if (row == 0) {
                return [tableView fd_heightForCellWithIdentifier:@"GoodDetailBasicInfoCell" cacheByIndexPath:indexPath configuration:^(GoodDetailBasicInfoCell *cell) {
                    [self configureCell:cell atIndexPath:indexPath];
                }];
            }
            else if (row == 1) {
                return  UITableViewAutomaticDimension;
            }
            else {
                return [tableView fd_heightForCellWithIdentifier:@"GoodDetailSkuInfoCell" cacheByIndexPath:indexPath configuration:^(GoodDetailSkuInfoCell *cell) {
                }];
            }
        }
        else if ([[self.detailDatas objectAtIndex:row] isKindOfClass:[FriendCircleListInfoModel class]]) {
            return  UITableViewAutomaticDimension;
        }
        else {
            return 0.000001;
//            return  UITableViewAutomaticDimension;
        }
    }
    else {
        // 描述
        return (self.view.height);
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    [self judgeScrollDirection:scrollView];
    [self setDetailNavigationMenuChange:offsetY];
    // 获取停靠位置
    CGFloat pageTabOffsetY = [self.m_baseTableView rectForRowAtIndexPath: [NSIndexPath indexPathForRow:self.detailDatas.count<=0?0:self.detailDatas.count inSection:0]].origin.y - kNavigationH;
    self.isCanNotMoveTabViewPre = self.isCanNotMoveTabView;
    if (offsetY >= pageTabOffsetY) {
        //到达顶部，不可滚动
        [scrollView setContentOffset:CGPointMake(0, pageTabOffsetY) animated:NO];
        self.isCanNotMoveTabView = YES;
    }
    else {
        //允许滚动
        self.isCanNotMoveTabView = NO;
    }
    if (self.isCanNotMoveTabView != self.isCanNotMoveTabViewPre) {
        // 之前能滚动，现在不能滚动，表示进入置顶状态
        if (!self.isCanNotMoveTabViewPre && self.isCanNotMoveTabView) {
            kPostNotification(GoodDetailArriveTopNotification, nil);
            self.canScroll = NO;
        }
        // 之前不能滚动，现在能滚动，表示进入取消置顶
        if(self.isCanNotMoveTabViewPre && !self.isCanNotMoveTabView){
            if (!self.canScroll) {
                scrollView.contentOffset = CGPointMake(0, pageTabOffsetY);
                self.isCanNotMoveTabView = YES;
                kPostNotification(GoodDetailArriveTopNotification, nil);
            }
        }
    }
    if (self.isCanNotMoveTabView && self.isCanNotMoveTabViewPre) {
        kPostNotification(GoodDetailArriveTopNotification, nil);
    }
}

#pragma mark - JMMenuBarDelegate
- (JMMenuBarItem *)menuBar:(JMMenuBar *)menuBar menuBarItemAtIndex:(NSInteger)itemIndex {
    static NSString *testIdentifier = @"GoodDetailMenuBarItem";
    GoodDetailMenuBarItem *item = (GoodDetailMenuBarItem *)[menuBar dequeueReusableItemWithIdentifier:testIdentifier];
    if (!item) {
        item = [[GoodDetailMenuBarItem alloc] init];
    }
    if (self.menuDatas.count > 0) {
        DetailColSwitchInfoModel *cls = [self.menuDatas objectAtIndex:itemIndex];
        item.titleLabel.text = cls.itemContent;
    }
    return item;
}

- (CGFloat)menuBar:(JMMenuBar *)menuBar menuBarItemWidthAtIndex:(NSInteger)itemIndex {
    return kSizeScale(50);
}

- (void)menuBar:(JMMenuBar *)menuBar didSelectItemAtIndex:(NSInteger)itemIndex {
    if (itemIndex == 0) {
        [self csdShowBackClickEvent];
    }
    else {
        [self csdBtnClickEvent];
    }
}

#pragma mark - GoodDetailRecommendInfoCellDelegate
- (void)goodDetailRecommendCell:(GoodDetailRecommendInfoCell *)cell didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.recommendArray.count) {
        id <HomeGoodItemProtocol> itemModel = [self.recommendModel.items objectAtIndex:indexPath.row];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(1) forKey:@"linkType"];
        [params setObject:itemModel.url forKey:@"url"];
        [YYCommonTools skipMultiCombinePage:self params:params];
    }
}

@end
