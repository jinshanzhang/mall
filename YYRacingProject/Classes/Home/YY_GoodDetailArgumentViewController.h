//
//  YY_GoodDetailArgumentViewController.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewController.h"

@interface YY_GoodDetailArgumentViewController : YYBaseTableViewController

@property (nonatomic, strong) NSMutableArray *argumentArrs;

@end
