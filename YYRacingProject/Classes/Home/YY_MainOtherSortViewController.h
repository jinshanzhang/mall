//
//  YY_MainOtherSortViewController.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/28.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YY_MainOtherSortViewController : YYBaseTableViewController

@property (nonatomic, copy) void (^navigationGradientBlock)(BOOL isExistBanner, CGPoint scrollValue, NSInteger pageIndex);
// 滚动停止回调
@property (nonatomic, copy) void (^scrollStopBlock)(CGPoint scrollValue, NSInteger pageIndex, BOOL isExistBanner);

@end

NS_ASSUME_NONNULL_END
