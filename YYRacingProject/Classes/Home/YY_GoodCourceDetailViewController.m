//
//  YY_GoodCourceDetailViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YY_GoodCourceDetailViewController.h"
#import "ChapterCell.h"

@interface YY_GoodCourceDetailViewController ()

@property (nonatomic, assign)  BOOL canScroll;
@property (nonatomic, strong)  NSNumber *comID;

@end

@implementation YY_GoodCourceDetailViewController

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_hideNavigation:YES];

    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"comID"]) {
            self.comID = [self.parameter objectForKey:@"comID"];
        }
    }
    Class currentCls = [ChapterCell class];
    registerClass(self.m_tableView, currentCls);
    self.m_tableView.backgroundColor = XHWhiteColor;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 100;
    self.m_tableView.allowsSelection = YES;
    self.m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(6));
        make.left.right.bottom.equalTo(self.view);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailLeaveTopNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailArriveTopNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollTopEvent:) name:CourseDetailScrollTopNotification object:nil];
    if (self.comID != nil) {
        //如果是虚拟商品 再次请求课程内容
        [kWholeConfig getLessonWithGoodID:[self.comID intValue] block:^(NSMutableArray *courceArr) {
            self.listData = courceArr;
            [self.m_tableView reloadData];
        } failBlock:^(NSMutableArray *model) {
        }];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private method
- (void)goodDetailNotificationHandler:(NSNotification *)notification {
    NSString *notificationName = notification.name;
    if ([notificationName isEqualToString:GoodDetailArriveTopNotification]) {
        self.canScroll = YES;
        self.m_tableView.showsVerticalScrollIndicator = YES;
    }else if([notificationName isEqualToString:GoodDetailLeaveTopNotification]){
        self.m_tableView.contentOffset = CGPointZero;
        self.canScroll = NO;
        self.m_tableView.showsVerticalScrollIndicator = NO;
    }
}

- (void)scrollTopEvent:(NSNotification *)notification {
    [self.m_tableView setContentOffset:CGPointMake(0, 0) animated:NO];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        NSInteger row = indexPath.row;
        Class currentCls = [ChapterCell class];
        ChapterCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        if (row < self.listData.count && kValidArray(self.listData)) {
            cell.model = self.listData[indexPath.row];
        }
        return cell;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    if (row < self.listData.count && kValidArray(self.listData)) {
        LessonDetailModel *model = self.listData[indexPath.row];
        if ([model.playFlag integerValue] != 1) {
            [YYCommonTools showTipMessage:@"请先购买哦"];
            return;
        }
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:model.lessonId forKey:@"lessonID"];
        [YYCommonTools skipAudioWith:self params:dic];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self judgeScrollDirection:scrollView];
    if (!self.canScroll) {
        [scrollView setContentOffset:CGPointZero];
    }
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        //表示页面向下，则发送通知告知离开顶部。
        kPostNotification(GoodDetailLeaveTopNotification, nil);
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
