//
//  HomeGoodItemProtocol.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#ifndef HomeGoodItemProtocol_h
#define HomeGoodItemProtocol_h
#import <UIKit/UIKit.h>

@protocol HomeGoodItemProtocol, ShareViewProtocol, PhotoViewProtocol;
@protocol ResourcesViewProtocol,ResourcesItemViewProtocol;
@protocol HomeSuperHotItemProtocol;
// 品牌视图协议
@protocol HomeBrandGoodItemProtocol <NSObject>

@property (nonatomic, strong, readonly) NSMutableArray <id<HomeGoodItemProtocol>> *items;
@property (nonatomic, strong, readonly) id<PhotoViewProtocol> coverImage; //封面图
@property (nonatomic, strong, readonly) NSNumber  *linkType; //链接类型
@property (nonatomic, copy, readonly)   NSString  *url;      //跳转的内容

@property (nonatomic, strong, readonly) NSIndexPath *currentIndex;
@property (nonatomic, assign, readonly) YYSingleRowScrollType scrollType;
@end

// 商品视图协议
@protocol HomeGoodItemProtocol <NSObject>

@property (nonatomic, strong, readonly) NSNumber  *comId;  //id
@property (nonatomic, copy, readonly)   NSString  *url;
@property (nonatomic, copy, readonly)   NSString  *uri;
@property (nonatomic, copy, readonly)   NSString  *title;
@property (nonatomic, copy, readonly)   NSString  *couponPrice;  //券后价
@property (nonatomic, copy, readonly)   NSString  *originPrice;
@property (nonatomic, copy, readonly)   NSString  *reservePrice;
@property (nonatomic, copy, readonly)   NSString  *commission;
@property (nonatomic, copy, readonly)   NSString  *sourceIcon;//国旗
@property (nonatomic, copy, readonly)   NSString  *tagIcon;   //爆款图片标识

@property (nonatomic, strong, readonly) NSNumber *itemSaleType; //售卖状态 1 即将开抢  2 热卖中   3 活动结束
@property (nonatomic, strong, readonly) NSNumber *saleStatus;   //特卖状态 1 未开始    2 预热    3在售中
@property (nonatomic, strong, readonly) NSNumber *linkType;
@property (nonatomic, strong, readonly) NSNumber *couponFlag;   //0 没有券  1 有券
@property (nonatomic, strong, readonly) NSNumber *couponLogic;  //0 不开启  1 开启券逻辑
@property (nonatomic, strong, readonly) NSIndexPath *currentIndex;

@property (nonatomic, assign, readonly) YYSingleRowScrollType scrollType;

@property (nonatomic, strong, readonly) id<PhotoViewProtocol> coverImage; //封面图
@property (nonatomic, strong, readonly) id<ShareViewProtocol> shareInfo;  //分享

@end

// 超级爆款视图协议
@protocol HomeSuperHotProtocol <NSObject>

@optional
@property (nonatomic, strong, readonly) NSNumber *showSwitch; // 1开， 0关
@property (nonatomic, strong, readonly) NSMutableArray <id<HomeSuperHotItemProtocol>> *items;
@property (nonatomic, assign, readonly) YYSingleRowScrollType scrollType;

@end


// 超级爆款子视图协议
@protocol HomeSuperHotItemProtocol <NSObject>

@optional
@property (nonatomic, strong, readonly) id<PhotoViewProtocol> coverImage; //封面图
@property (nonatomic, strong, readonly) NSNumber  *superTrendyType;  //0 单品  1品牌
@property (nonatomic, copy, readonly)   NSString  *sellpoint;      //卖点
@property (nonatomic, copy, readonly)   NSString  *title;
@property (nonatomic, copy, readonly)   NSString  *couponPrice;
@property (nonatomic, copy, readonly)   NSString  *originPrice;
@property (nonatomic, copy, readonly)   NSString  *reservePrice;
@property (nonatomic, copy, readonly)   NSString  *commission;
@property (nonatomic, copy, readonly)   NSString  *sellOutFlagUri; //为null或者为空表示没有售罄
@property (nonatomic, copy, readonly)   NSString  *rightIcon;
@property (nonatomic, copy, readonly)   NSString  *uri;
@property (nonatomic, copy, readonly)   NSString  *skipBrandTrendyUri; //跳转品牌uri

@property (nonatomic, strong, readonly) NSNumber  *couponFlag; //0 没有优惠券  1 有优惠券
@property (nonatomic, strong, readonly) NSNumber  *startTime;
@property (nonatomic, strong, readonly) NSNumber  *endTime;
@property (nonatomic, strong, readonly) NSNumber  *currentTime;
@property (nonatomic, strong, readonly) NSNumber  *linkType;
@property (nonatomic, strong, readonly) NSNumber  *saleStatus; // 3 马上抢   2 即将开始

@property (nonatomic, strong, readonly) NSMutableArray *items;
@property (nonatomic, assign, readonly) YYSingleRowScrollType scrollType;

@end


// 分享视图协议
@protocol ShareViewProtocol <NSObject>

@property (nonatomic, copy, readonly) NSString *shopName;
@property (nonatomic, copy, readonly) NSString *shopAvatorUrl;
@property (nonatomic, copy, readonly) NSString *goodsTitle;
@property (nonatomic, copy, readonly) NSString *goodsShareImage;
@property (nonatomic, copy, readonly) NSString *goodsShareBigImages;  //分享朋友圈的服务器大图
@property (nonatomic, copy, readonly) NSString *shareUrl;
@property (nonatomic, copy, readonly) NSString *reservePrice;
@property (nonatomic, copy, readonly) NSString *goodsShortTitle;
@property (nonatomic, copy, readonly) NSString *commission;
@property (nonatomic, copy, readonly) NSString *originPrice;
@property (nonatomic, copy, readonly) NSString *qrcodePictureUrl;  //二维码图片URL

@property (nonatomic, copy, readonly) NSString *discountPrice; //券后价，折扣价
@property (nonatomic, copy, readonly) NSString *maxDenomination; //最大券的面额

@property (nonatomic, strong, readonly) NSNumber *isNeedComposite;      //是否需要合成
@property (nonatomic, assign, readonly) NSInteger specialType; //0:普通特卖 1:新人特卖
@property (nonatomic, strong, readonly) NSNumber *itemSaleType;
@property (nonatomic, strong, readonly) NSNumber *specialStartSellDate;//特卖开始
@property (nonatomic, strong, readonly) NSNumber *specialEndSellDate;  //特卖结束

@property (nonatomic, assign, readonly) BOOL isApplet;//0-h5；1-小程序
@property (nonatomic, copy, readonly) NSString *path; //小程序路径

@end



// 图片视图协议
@protocol PhotoViewProtocol <NSObject>

@property (nonatomic, copy, readonly) NSString *imageUrl;
@property (nonatomic, strong, readonly) NSNumber *pWidth;
@property (nonatomic, strong, readonly) NSNumber *pHeight;

@end


@protocol HomeAllResourcesProtocol <NSObject>

@property (nonatomic, strong, readonly) id <ResourcesViewProtocol> banners;
@property (nonatomic, strong, readonly) id <ResourcesViewProtocol> icons;
@property (nonatomic, strong, readonly) id <ResourcesViewProtocol> booths;
@property (nonatomic, strong, readonly) id <ResourcesViewProtocol> bgImages;
@property (nonatomic, strong, readonly) id <ResourcesViewProtocol> capsule;
@property (nonatomic, strong, readonly) id <ResourcesViewProtocol> serviceGuarantee;
@property (nonatomic, strong, readonly) id <HomeSuperHotProtocol> superTrendy;

@end



// 资源位协议
@protocol ResourcesViewProtocol <NSObject>

@property (nonatomic, strong, readonly) NSNumber *showSwitch; // 1开， 0关
@property (nonatomic, strong, readonly) NSMutableArray <id<ResourcesItemViewProtocol>> *items;
@property (nonatomic, strong, readonly) id <ResourcesItemViewProtocol> item;
@end


// 资源位item协议
@protocol ResourcesItemViewProtocol <NSObject>

@property (nonatomic, copy, readonly) NSString *imageUrl;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *text;
@property (nonatomic, copy, readonly) NSString *url;
@property (nonatomic, copy, readonly) NSString *fontColor;

@property (nonatomic, strong, readonly) NSNumber *type;  // 使用类型
@property (nonatomic, strong, readonly) NSNumber *linkType; //跳转类型
@property (nonatomic, strong, readonly) NSNumber *weight;
@property (nonatomic, strong, readonly) NSNumber *isVisible;
@property (nonatomic, strong, readonly) NSNumber *popType;
@property (nonatomic, strong, readonly) NSNumber *width;
@property (nonatomic, strong, readonly) NSNumber *height;

@end


#endif /* HomeGoodItemProtocol_h */
