//
//  YYBindWechatRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBindWechatRequestAPI.h"

@implementation YYBindWechatRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,BINDWECHAT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
