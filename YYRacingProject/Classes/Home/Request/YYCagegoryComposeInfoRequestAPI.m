//
//  YYCagegoryComposeInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCagegoryComposeInfoRequestAPI.h"

@implementation YYCagegoryComposeInfoRequestAPI

- (instancetype)initCategoryComposeInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
        NSLog(@"第一次请求到的数据是:%@",params);
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *url = [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CATEGORYCOMPOSE];
    NSLog(@"第一次请求到的接口是:%@",url);
    return url;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
