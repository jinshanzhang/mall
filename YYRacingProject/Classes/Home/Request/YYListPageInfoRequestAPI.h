//
//  YYListPageInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/28.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYListPageInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initListPageInfoRequest:(NSMutableDictionary *)params;

@end
