//
//  YYHomeSuperBrandListGoodPageInfoRequest.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYHomeSuperBrandListGoodPageInfoRequest : YYBaseRequestAPI

- (instancetype)initHomeSuperBrandListGoodPageInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
