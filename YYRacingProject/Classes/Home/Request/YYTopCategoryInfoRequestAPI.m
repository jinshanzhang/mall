//
//  YYTopCategoryInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYTopCategoryInfoRequestAPI.h"

@implementation YYTopCategoryInfoRequestAPI

- (instancetype)initTopCategoryInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,TOPCATEGORY];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
