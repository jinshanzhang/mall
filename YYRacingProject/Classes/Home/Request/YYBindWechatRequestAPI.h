//
//  YYBindWechatRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYBindWechatRequestAPI : YYBaseRequestAPI
- (instancetype)initRequest:(NSMutableDictionary *)params;

@end
