//
//  YYSearchResultAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYSearchResultAPI : YYBaseRequestAPI

- (instancetype)initSearchResultRequest:(NSMutableDictionary *)params;


@end
