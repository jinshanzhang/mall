//
//  YYHomeScreeningShareInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/13.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYHomeScreeningShareInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initHomeScreeningShareInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
