//
//  YYHomeNavigationCateInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHomeNavigationCateInfoRequestAPI.h"

@implementation YYHomeNavigationCateInfoRequestAPI

- (instancetype)initHomeCategoryInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH4,HOMECATEGORY];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
