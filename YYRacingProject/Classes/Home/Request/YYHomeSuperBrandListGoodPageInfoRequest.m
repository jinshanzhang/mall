//
//  YYHomeSuperBrandListGoodPageInfoRequest.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/2/18.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHomeSuperBrandListGoodPageInfoRequest.h"

@implementation YYHomeSuperBrandListGoodPageInfoRequest

- (instancetype)initHomeSuperBrandListGoodPageInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH4,HOMESUPERBRANDPAGE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
