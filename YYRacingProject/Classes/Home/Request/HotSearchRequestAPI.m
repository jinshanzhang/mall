//
//  HotSearchRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "HotSearchRequestAPI.h"

@implementation HotSearchRequestAPI

- (instancetype)initHotSearchRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}
- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,KEYWORD];
}
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
