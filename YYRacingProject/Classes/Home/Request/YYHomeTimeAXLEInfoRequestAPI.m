//
//  YYHomeTimeAXLEInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYHomeTimeAXLEInfoRequestAPI.h"

@implementation YYHomeTimeAXLEInfoRequestAPI

- (instancetype)initHomeTimeAxleInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,TIMEAXLE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
