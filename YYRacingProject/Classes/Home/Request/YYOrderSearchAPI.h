//
//  YYOrderSearchAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/19.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYOrderSearchAPI : YYBaseRequestAPI

- (instancetype)initSearchResultRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
