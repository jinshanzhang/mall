//
//  YYHomeNavigationCateInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYHomeNavigationCateInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initHomeCategoryInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
