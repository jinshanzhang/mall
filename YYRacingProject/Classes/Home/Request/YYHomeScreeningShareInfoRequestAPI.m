//
//  YYHomeScreeningShareInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/13.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYHomeScreeningShareInfoRequestAPI.h"

@implementation YYHomeScreeningShareInfoRequestAPI

- (instancetype)initHomeScreeningShareInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,HOMESHARE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
