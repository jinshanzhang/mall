//
//  YYHomeSubListGoodsInfoRequest.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/27.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHomeSubListGoodsInfoRequest.h"

@implementation YYHomeSubListGoodsInfoRequest

- (instancetype)initHomeSubListGoodsInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH4,@"index/recommdation"];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
