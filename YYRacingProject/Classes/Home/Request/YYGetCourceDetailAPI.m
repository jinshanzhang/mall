//
//  YYGetCourceAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYGetCourceDetailAPI.h"

@implementation YYGetCourceDetailAPI
- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COURSELISTCONTENT];
}
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}


@end
