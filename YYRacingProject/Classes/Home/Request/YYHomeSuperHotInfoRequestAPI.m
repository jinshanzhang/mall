//
//  YYHomeSuperHotInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/7.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYHomeSuperHotInfoRequestAPI.h"

@implementation YYHomeSuperHotInfoRequestAPI

- (instancetype)initHomeSuperHotInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,SUPERHOT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
