//
//  YYGetVertsionRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYGetVertsionRequestAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;


@end
