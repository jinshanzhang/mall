//
//  YYHomeListInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYHomeListInfoRequestAPI.h"

@implementation YYHomeListInfoRequestAPI

- (instancetype)initHomeListInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,HOMELIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
