//
//  YY_OrderSearchResultController.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/19.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YY_OrderSearchResultController : YYBaseTableViewController

@end

NS_ASSUME_NONNULL_END
