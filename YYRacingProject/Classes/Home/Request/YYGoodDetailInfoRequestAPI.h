//
//  YYGoodDetailInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYGoodDetailInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initGoodDetailInfoRequest:(NSMutableDictionary *)params;

@end
