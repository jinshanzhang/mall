//
//  YY_OrderSearchResultController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/19.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_OrderSearchResultController.h"
#import "YYOrderSearchAPI.h"
#import "OrderListInfoModel.h"

#import "OrderListGoodInfoCell.h"
#import "OrderSumCell.h"
#import "OrderListSectionFooterInfoView.h"
#import "OrderListSectionHeaderInfoView.h"

@interface YY_OrderSearchResultController ()

@property (nonatomic, strong) UILabel        *contentLbl;
@property (nonatomic, strong) NSString       *keyword;       //关键字
@property (nonatomic, strong) NSString *cursor;
@property (nonatomic, strong) NSNumber *hasMore;
@property (nonatomic, strong) NSNumber *systemTime;
@property (nonatomic, strong) NSNumber *pageSoucre;
@property (nonatomic, assign) BOOL     isShowEarn;
@property (nonatomic, strong) NSNumber *tabType;

@end

@implementation YY_OrderSearchResultController

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.emptyTitle = @"抱歉,暂无搜索结果";
        self.NoDataType = YYEmptyViewCourseNoDataType;
        self.tableStyle = UITableViewStyleGrouped;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cursor = @"";
    self.tabType = @(0);
    self.pageSoucre = @(2);
    self.isShowEarn = YES;
    self.keyword = [self.parameter objectForKey:@"url"];
    
    [self xh_navBottomLine:XHWhiteColor];
    [self xh_popTopRootViewController:NO];
    [self setNavTitleView];
    
    self.m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.m_tableView.estimatedRowHeight = 45;
    // 下面高度需要设置为5.0 否则低版本系统出现页面错乱。
    self.m_tableView.estimatedSectionHeaderHeight = 5.0;
    self.m_tableView.estimatedSectionFooterHeight = 5.0;
    //self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.m_tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    
    Class current = [OrderListGoodInfoCell class];
    registerClass(self.m_tableView, current);
    current = [OrderListSectionHeaderInfoView class];
    [self.m_tableView registerClass:current forHeaderFooterViewReuseIdentifier:strFromCls(current)];
    current = [OrderListSectionFooterInfoView class];
    [self.m_tableView registerClass:current forHeaderFooterViewReuseIdentifier:strFromCls(current)];
    current = [OrderSumCell class];
    registerClass(self.m_tableView, current);

    // Do any additional setup after loading the view.
    [self loadNewer];
}

- (void)xh_popTopRootViewController:(BOOL)isTopRoot {
    @weakify(self);
    [self.navigationBar yy_setNavigationButtonImageName:@"detail_navbar_back"
                                                 isLeft:YES
                                            buttonBlock:^(UIButton *sender) {
                                                @strongify(self);
                                                UIViewController *viewController=self.navigationController.viewControllers[self.navigationController.viewControllers.count-3];
                                                    [self.navigationController popToViewController:viewController animated:YES];
                                                kPostNotification(@"orderPop", nil);
                                            }];
}


/**
 *  搜索导航视图
 */
- (void)setNavTitleView {
    @weakify(self);
    UIButton *searchBtn = [YYCreateTools createBtnImage:nil];
    searchBtn.backgroundColor = HexRGB(0xF4F4F4);
    searchBtn.titleLabel.font = normalFont(14);
    searchBtn.layer.cornerRadius = kSizeScale(4);
    searchBtn.clipsToBounds = YES;
    
    [self xh_addTitleView:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(44);
        make.height.mas_equalTo(kSizeScale(32));
        make.right.mas_equalTo(-kSizeScale(50));
        make.bottom.mas_equalTo(-((44-kSizeScale(32))/2.0 - 2));
    }];
    
    self.contentLbl = [YYCreateTools createLabel:nil
                                            font:normalFont(14)
                                       textColor:XHBlackColor];
    [searchBtn addSubview:self.contentLbl];
    [self.contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchBtn.mas_left).offset(10);
        make.centerY.equalTo(searchBtn);
        make.right.equalTo(searchBtn.mas_right).offset(-10);
    }];
    self.contentLbl.text = self.keyword;
    
    UIButton *cleanBtn = [YYCreateTools createBtnImage:@"Search_result_clean"];
    cleanBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        self.contentLbl.text = @"";
        [self.navigationController popViewControllerAnimated:NO];
    };
    [searchBtn addSubview:cleanBtn];
    [cleanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBtn);
        make.right.equalTo(searchBtn.mas_right).offset(-kSizeScale(5));
    }];
    
    [self xh_addTitleView:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(44);
        make.height.mas_equalTo(kSizeScale(32));
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-((44-kSizeScale(32))/2.0 - 2));
    }];
    searchBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:NO];
    };
    
    
    [self xh_addNavigationItemWithImageName:@"order_show_earnmoney_icon"
                                     isLeft:NO
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     self.isShowEarn = !self.isShowEarn;
                                     if (self.isShowEarn) {
                                         [self xh_addNavigationItemImageName:@"order_show_earnmoney_icon"
                                                                      isLeft:NO];
                                     }
                                     else {
                                         [self xh_addNavigationItemImageName:@"order_hide_earnmoney_icon"
                                                                      isLeft:NO];
                                     }
                                     [self.m_tableView reloadData];
                                 }];
}

- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.keyword forKey:@"word"];
    YYOrderSearchAPI *API = [[YYOrderSearchAPI alloc] initSearchResultRequest:dict];
    return API;
}
- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.cursor = [responseDic objectForKey:@"cursor"];
        self.systemTime = [responseDic objectForKey:@"systemTime"];
        self.hasMore = [responseDic objectForKey:@"hasMore"];
        NSMutableArray *orderList = [[NSArray modelArrayWithClass:[OrderListInfoModel class] json:[responseDic objectForKey:@"orderList"]] mutableCopy];
        // 添加倒计时
        NSString *countDownIdentifier = [NSString stringWithFormat:@"YYOrderListPage%d",self.current];
        [kCountDownManager addSourceWithIdentifier:countDownIdentifier];
        [kCountDownManager reloadAllSource];
        [orderList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            OrderListInfoModel *listModel = (OrderListInfoModel *)obj;
            listModel.countDownIdentifier = countDownIdentifier;
            listModel.systemTime = self.systemTime;
            NSInteger deadTime = [listModel.payDeadlineTime integerValue]/1000;
            //NSInteger createTime = [listModel.orderTime integerValue]/1000 + 30 * 60;
            NSInteger systemTime = [listModel.systemTime integerValue]/1000;
            listModel.diffValue = deadTime - systemTime;
        }];
        return orderList;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return [self.hasMore boolValue];
}

- (void)loadPullDownPage {
    self.cursor = @"";
    [super loadPullDownPage];
}



#pragma mark - Private method
/**订单操作处理**/
- (void)orderClickOperator:(YYOrderOperatorType)type
                 listModel:(OrderListInfoModel *)listModel{
    if (type == YYOrderOperatorCancel) {
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"取消后将不能恢复，确认取消订单吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
            
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig cancelOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadPullDownPage];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorConfirm) {
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确认收货吗？收到商品后再确认收货，以免造成损失。" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig confirmOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadPullDownPage];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorDelete) {
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"删除后订单不能恢复，确认删除该订单吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig deleteOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] isStore:([self.pageSoucre integerValue] == 2?YES:NO) success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadPullDownPage];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorLookLogistic) {
        //查看物流
        [YYCommonTools skipLogisticsPage:self params:@{@"orderID": listModel.orderId}];
    }
    else if (type == YYOrderOperatorPay) {
        [YYCommonTools skipOrderPay:self
                            orderID:[listModel.orderId longValue]
                        canBackRoot:NO];
    }
}

/**修改订单状态**/
- (void)modifyOrderStatus:(NSIndexPath *)currentPath
              orderStatus:(NSNumber *)orderStatus
                operators:(NSNumber *)operators {
    
    __block BOOL  isOperator = NO;
    __block NSNumber  *tempStatus = orderStatus;
    __block NSNumber  *tempOperator = operators;
    __block NSInteger section = currentPath.section;
    
    if ([self.tabType integerValue] == 0) {
        // 全部订单列表
        [self.listData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            OrderListInfoModel *listModel = (OrderListInfoModel *)obj;
            if (idx == section) {
                listModel.orderStatus = tempStatus;
                listModel.operation = tempOperator;
                isOperator = YES;
            }
        }];
    }
    else if ([self.tabType integerValue] == 2) {
        // 待收货列表
        // 删除list 对应的数据
        [self.listData removeObjectAtIndex:section];
        isOperator = YES;
    }
    if (isOperator) {
        [UIView performWithoutAnimation:^{
            [self.m_tableView reloadData];
        }];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.listData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        return self.isShowEarn?(infoModel.skuList.count+infoModel.sumList.count):infoModel.skuList.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    Class current = [OrderListGoodInfoCell class];
    OrderListGoodInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        if (self.isShowEarn &&[self.pageSoucre integerValue] !=1) {
            if (row<infoModel.sumList.count) {
                Class current = [OrderSumCell class];
                OrderSumCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                OrderSumInfoModel *sumModel = infoModel.sumList[indexPath.row];
                cell.sumModel = sumModel;
                return cell;
            }else{
                OrderGoodInfoModel *goodModel = [infoModel.skuList objectAtIndex:row-infoModel.sumList.count];
                goodModel.source = self.pageSoucre;
                goodModel.showEarn = self.isShowEarn;
                cell.goodModel = goodModel;
            }
        }else{
            OrderGoodInfoModel *goodModel = [infoModel.skuList objectAtIndex:row];
            goodModel.source = self.pageSoucre;
            goodModel.showEarn = self.isShowEarn;
            cell.goodModel = goodModel;
        }
        
        cell.orderClick = ^(OrderGoodInfoModel *goodModel, OrderListGoodInfoCell *listCell) {
            @strongify(self);
            __block NSIndexPath *currentPath = [self.m_tableView indexPathForCell:listCell];
            [YYCommonTools skipOrderDetial:self
                                    params:@{@"orderID": goodModel.orderId,
                                             @"source": self.pageSoucre,
                                             @"tabType": self.tabType}
                                 ctrlBlock:^(NSNumber *orderStatus,NSNumber *operator,BOOL isReceive) {
                                     @strongify(self);
                                     if (isReceive) {
                                         [self modifyOrderStatus:currentPath
                                                     orderStatus:orderStatus
                                                       operators:operator];
                                     }
                                 }];
        };
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Class currentCls = [OrderListSectionHeaderInfoView class];
    OrderListSectionHeaderInfoView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:strFromCls(currentCls)];
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        infoModel.source = self.pageSoucre;
        infoModel.isShowMark = self.isShowEarn;
        sectionHeaderView.listModel = infoModel;
        sectionHeaderView.orderStatus = infoModel.orderStatus;
    }
    return sectionHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    @weakify(self);
    Class currentCls = [OrderListSectionFooterInfoView class];
    OrderListSectionFooterInfoView *sectionFooterView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:strFromCls(currentCls)];
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        sectionFooterView.listModel = infoModel;
        sectionFooterView.sectionFooterBtnOperatorBlock = ^(YYOrderOperatorType type, OrderListInfoModel *model) {
            @strongify(self);
            [self orderClickOperator:type listModel:model];
        };
    }
    return sectionFooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)dealloc {
    [kCountDownManager removeAllSource];
    [kCountDownManager invalidate];
}

#pragma mark - Event
- (void)earnShowNotification:(NSNotification *)notification {
    NSNumber *showEarn = [notification.object objectForKey:@"earnShow"];
    self.isShowEarn = [showEarn boolValue];
    [UIView performWithoutAnimation:^{
        [self.m_tableView reloadData];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
