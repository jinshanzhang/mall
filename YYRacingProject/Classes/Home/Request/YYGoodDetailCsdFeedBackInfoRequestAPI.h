//
//  YYGoodDetailCsdFeedBackInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYGoodDetailCsdFeedBackInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initGoodDetailCsdFeedBackInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
