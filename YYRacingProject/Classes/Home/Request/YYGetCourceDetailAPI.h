//
//  YYGetCourceAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYGetCourceDetailAPI : YYBaseRequestAPI
- (instancetype)initRequest:(NSMutableDictionary *)params;

@end
