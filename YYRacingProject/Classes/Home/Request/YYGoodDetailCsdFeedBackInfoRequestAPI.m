//
//  YYGoodDetailCsdFeedBackInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYGoodDetailCsdFeedBackInfoRequestAPI.h"

@implementation YYGoodDetailCsdFeedBackInfoRequestAPI

- (instancetype)initGoodDetailCsdFeedBackInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,GOODCSDFEEDBACK];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
