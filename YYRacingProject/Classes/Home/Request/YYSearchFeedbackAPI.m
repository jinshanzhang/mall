//
//  YYSearchFeedbackAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYSearchFeedbackAPI.h"

@implementation YYSearchFeedbackAPI

- (instancetype)initHotSearchRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *word = nil;
    if (kValidDictionary(self.inParams)) {
        word = [self.inParams objectForKey:@"word"];
        word = [NSString usingEncoding:word];
        [self.inParams removeObjectForKey:@"word"];
    }
    return [NSString stringWithFormat:@"%@%@?word=%@",INTERFACE_PATH,FEEDBACK,word];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
