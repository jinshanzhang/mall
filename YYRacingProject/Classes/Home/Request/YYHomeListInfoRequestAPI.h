//
//  YYHomeListInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface YYHomeListInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initHomeListInfoRequest:(NSMutableDictionary *)params;

@end
