//
//  YYCategoryDetailInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYCategoryDetailInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initCategoryDetailInfoRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
