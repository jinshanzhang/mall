//
//  YYListPageInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/28.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYListPageInfoRequestAPI.h"

@implementation YYListPageInfoRequestAPI

- (instancetype)initListPageInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
        NSLog(@"上拉加载的参数是:%@",params);
    }
    return self;
}

- (NSString *)requestUrl {
    BOOL isSearch = NO;
    if ([self.inParams objectForKey:@"isSearch"]) {
        isSearch = [[self.inParams objectForKey:@"isSearch"] boolValue];
        [self.inParams removeObjectForKey:@"isSearch"];
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",(isSearch==YES?INTERFACE_PATH:INTERFACE_PATH3),LISTPAGE];
//    NSString *url = [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LISTPAGE];
    
    NSLog(@"上拉加载的接口是:%@",url);
    return url;
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
