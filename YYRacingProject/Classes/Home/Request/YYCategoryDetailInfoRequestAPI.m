//
//  YYCategoryDetailInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/11.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYCategoryDetailInfoRequestAPI.h"

@implementation YYCategoryDetailInfoRequestAPI

- (instancetype)initCategoryDetailInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *cateID = nil;
    if ([self.inParams objectForKey:@"cateID"]) {
        cateID = [self.inParams objectForKey:@"cateID"];
        [self.inParams removeObjectForKey:@"cateID"];
    }
    if (kValidString(cateID)) {
        return [NSString stringWithFormat:@"%@%@/%@",INTERFACE_PATH,CATEGORYDETAIL,[cateID URLEncodedString]];
    }
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CATEGORYDETAIL];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
