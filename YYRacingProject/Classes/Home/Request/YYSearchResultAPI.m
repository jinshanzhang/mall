//
//  YYSearchResultAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYSearchResultAPI.h"

@implementation YYSearchResultAPI

- (instancetype)initSearchResultRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *tempUrl;
    if ([self.inParams objectForKey:@"sortType"]) {
        tempUrl = [NSString stringWithFormat:@"%@%@?sortType=%@&word=%@&sort=%@",INTERFACE_PATH,SEARCHRESULT,[self.inParams objectForKey:@"sortType"],[self.inParams objectForKey:@"word"], [self.inParams objectForKey:@"sort"]];
    } else {
        tempUrl = [NSString stringWithFormat:@"%@%@?word=%@&sort=%@",INTERFACE_PATH,SEARCHRESULT,[self.inParams objectForKey:@"word"], [self.inParams objectForKey:@"sort"]];
    }
    [self.inParams removeObjectForKey:@"word"];
    [self.inParams removeObjectForKey:@"sortType"];
    [self.inParams removeObjectForKey:@"sort"];
    NSLog(@"获取到的接口是:%@",tempUrl);
    return tempUrl;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
