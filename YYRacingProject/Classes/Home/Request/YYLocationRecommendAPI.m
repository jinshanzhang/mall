//
//  YYLocationRecommendAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/15.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYLocationRecommendAPI.h"

@implementation YYLocationRecommendAPI
- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOCATIONRECOMMEND];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
