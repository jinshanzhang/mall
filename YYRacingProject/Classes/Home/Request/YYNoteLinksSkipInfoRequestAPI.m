//
//  YYNoteLinksSkipInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/16.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYNoteLinksSkipInfoRequestAPI.h"

@implementation YYNoteLinksSkipInfoRequestAPI

- (instancetype)initNoteLinkSkipInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,NOTESKIP];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
