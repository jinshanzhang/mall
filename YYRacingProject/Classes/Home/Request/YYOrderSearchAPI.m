//
//  YYOrderSearchAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/19.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYOrderSearchAPI.h"

@implementation YYOrderSearchAPI

- (instancetype)initSearchResultRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *url = [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,ORDERRESEARCH];
    return url;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
