//
//  YYGetVertsionRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYGetVertsionRequestAPI.h"

@implementation YYGetVertsionRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,GETVERSION];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
