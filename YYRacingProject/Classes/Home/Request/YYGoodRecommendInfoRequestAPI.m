//
//  YYGoodRecommendInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/10.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYGoodRecommendInfoRequestAPI.h"

@implementation YYGoodRecommendInfoRequestAPI

- (instancetype)initGoodRecommendInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,GOODRECOMMEND];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
