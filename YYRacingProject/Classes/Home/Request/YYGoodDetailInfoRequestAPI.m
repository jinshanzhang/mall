//
//  YYGoodDetailInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/12.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYGoodDetailInfoRequestAPI.h"

@implementation YYGoodDetailInfoRequestAPI

- (instancetype)initGoodDetailInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH2,GOODDETAIL];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
