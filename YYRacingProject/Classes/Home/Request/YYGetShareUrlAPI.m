//
//  YYGetShareUrlAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/28.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYGetShareUrlAPI.h"

@implementation YYGetShareUrlAPI

- (instancetype)initRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *tempUrl;
    if ([self.inParams objectForKey:@"sourceType"]) {
        tempUrl = [NSString stringWithFormat:@"%@%@?sourceType=%@&id=%@",INTERFACE_PATH,COURSEURL,[self.inParams objectForKey:@"sourceType"],[self.inParams objectForKey:@"id"]];
    }
    [self.inParams removeObjectForKey:@"id"];
    [self.inParams removeObjectForKey:@"sourceType"];
    
    return tempUrl;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
