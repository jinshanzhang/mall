//
//  YY_GoodDetailNoticeViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/13.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_GoodDetailNoticeViewController.h"
#import "GoodDetailNoticeInfoCell.h"
@interface YY_GoodDetailNoticeViewController ()

<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, assign) BOOL                      canScroll;
@end

@implementation YY_GoodDetailNoticeViewController

#pragma mark - Setter
- (void)setParameter:(NSDictionary *)parameter {
    if (kValidDictionary(parameter)) {
        if (self.m_tableView) {
            [self.m_tableView reloadData];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_hideNavigation:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailLeaveTopNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goodDetailNotificationHandler:) name:GoodDetailArriveTopNotification object:nil];
    
    Class currentCls = [GoodDetailNoticeInfoCell class];
    registerClass(self.m_tableView, currentCls);
    self.m_tableView.backgroundColor = XHWhiteColor;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 30.0;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.m_tableView reloadData];
}

#pragma mark - Private method
- (void)goodDetailNotificationHandler:(NSNotification *)notification {
    NSString *notificationName = notification.name;
    if ([notificationName isEqualToString:GoodDetailArriveTopNotification]) {
        self.canScroll = YES;
        self.m_tableView.showsVerticalScrollIndicator = YES;
    }else if([notificationName isEqualToString:GoodDetailLeaveTopNotification]){
        self.m_tableView.contentOffset = CGPointZero;
        self.canScroll = NO;
        self.m_tableView.showsVerticalScrollIndicator = NO;
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class currentCls = [GoodDetailNoticeInfoCell class];
    GoodDetailNoticeInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    return cell;
}

#pragma mark - UIScrollDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self judgeScrollDirection:scrollView];
    if (!self.canScroll) {
        [scrollView setContentOffset:CGPointZero];
    }
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        //表示页面向下，则发送通知告知离开顶部。
        kPostNotification(GoodDetailLeaveTopNotification, nil);
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
