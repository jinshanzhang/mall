//
//  YY_CategoryViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/10/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_CategoryViewController.h"

#import "HomeCateSuperInfoModel.h"
#import "SearchCategoryContentView.h"

#import "YYTopCategoryInfoRequestAPI.h"

@interface YY_CategoryViewController ()

@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, strong) UILabel  *placeholderLbl;
@property (nonatomic, strong) SearchCategoryContentView *categoryView;


@property (nonatomic, strong) NSString *matchID;
@property (nonatomic, strong) NSMutableArray *superCates;

@end

@implementation YY_CategoryViewController

#pragma mark - Setter method
- (SearchCategoryContentView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[SearchCategoryContentView alloc] initWithFrame:CGRectZero];
        _categoryView.backgroundColor = XHWhiteColor;
    }
    return _categoryView;
}

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationUI];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"url"]) {
            self.matchID = [self.parameter objectForKey:@"url"];
        }
    }
    [MobClick event:@"homeCategory"];
    [self.view addSubview:self.categoryView];
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.bottom.right.equalTo(self.view);
    }];
    
    [self topCategoryRequest];
    // Do any additional setup after loading the view.
}

#pragma mark - Request
/**获取一级类目**/
- (void)topCategoryRequest {
    self.superCates = [NSMutableArray array];
    YYTopCategoryInfoRequestAPI *cateAPI = [[YYTopCategoryInfoRequestAPI alloc] initTopCategoryInfoRequest:[[NSDictionary dictionary] mutableCopy]];
    [cateAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            __block NSInteger defaultIndex = 0;
            NSArray *cateArrs = [NSArray modelArrayWithClass:[HomeCateSuperInfoModel class] json:[responDict objectForKey:@"cats"]];
            if (kValidString(self.matchID)) {
                [cateArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    HomeCateSuperInfoModel *infoModel = (HomeCateSuperInfoModel *)obj;
                    if ([self.matchID isEqualToString:infoModel.superID]) {
                        defaultIndex = idx;
                        *stop = YES;
                    }
                }];
            }
            self.superCates = [cateArrs mutableCopy];
            [self.categoryView searchCateDatas:self.superCates
                            defaultSelectIndex:defaultIndex];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - Private method
- (void)setNavigationUI {
    @weakify(self);
    UIView *searchView = [YYCreateTools createView:XHWhiteColor];
    [self xh_addTitleView:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kScreenW, kSizeScale(36)));
        make.centerX.equalTo(searchView.superview);
        make.centerY.equalTo(searchView.superview).offset(IS_IPHONE_X?kSizeScale(15):kSizeScale(10));
    }];
    
    UIButton *leftBtn = [YYCreateTools createBtnImage:@"detail_navbar_back"];
    [searchView addSubview:leftBtn];
    leftBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    };
    
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.equalTo(searchView);
        make.size.mas_equalTo(CGSizeMake(40, 30));
    }];
    
    UIView *searchBgView = [YYCreateTools createView:HexRGB(0xF4F4F4)];
    searchBgView.layer.cornerRadius = 2;
    searchBgView.clipsToBounds = YES;
    [searchView addSubview:searchBgView];
    [searchBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftBtn.mas_right);
        make.right.mas_equalTo(-kSizeScale(50));
        make.top.mas_equalTo(kSizeScale(4));
        make.bottom.mas_equalTo(-kSizeScale(0));
    }];
    
    [searchBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchClickEvent:)]];
    
    UIImageView *searchImgView = [YYCreateTools createImageView:@"category_search_icon"
                                                      viewModel:-1];
    [searchBgView addSubview:searchImgView];
    [searchImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBgView);
        make.left.mas_equalTo(kSizeScale(7));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(14), kSizeScale(14)));
    }];
    
    self.placeholderLbl = [YYCreateTools createLabel:@"请输入关键字搜索"
                                                font:midFont(12)
                                           textColor:XHBlackLitColor];
    [searchBgView addSubview:self.placeholderLbl];

    [self.placeholderLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchImgView.mas_right).offset(kSizeScale(7));
        make.centerY.equalTo(searchBgView);
    }];
    
    self.cancelBtn = [YYCreateTools createBtn:@"取消"
                                         font:normalFont(14)
                                    textColor:XHBlackColor];
    [self.cancelBtn addTarget:self action:@selector(cancelClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [searchView addSubview:self.cancelBtn];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(searchView).offset(-kSizeScale(5));
        make.centerY.equalTo(searchImgView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(40), kSizeScale(30)));
    }];
}

#pragma mark - Event
- (void)searchClickEvent:(UITapGestureRecognizer *)tapGesture {
    [YYCommonTools skipSearchPage:self title:@"" type:0];
}

- (void)cancelClickEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
