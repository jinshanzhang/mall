//
//  YY_SearchResultViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_SearchResultViewController.h"

#import "SearchResultEmptyView.h"
#import "HomeCateItemReusableView.h"
#import "HomeSpecialSellDoubleInfoCell.h"


#import "KeyWordModel.h"
#import "HomeHotGoodInfoModel.h"
#import "HomeListStructInfoModel.h"
#import "HomeListItemGoodInfoModel.h"

#import "YYSearchResultAPI.h"
#import "HotSearchRequestAPI.h"
#import "YYListPageInfoRequestAPI.h"
#import "YYCagegoryComposeInfoRequestAPI.h"
#import "UICollectionViewWaterFallLayout.h"
#import "YYSearchResultCollectionCell.h"

@interface YY_SearchResultViewController ()<HomeSpecialSellDoubleInfoCellDelegate,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewWaterFallLayoutDelegate>
@property (nonatomic, strong) UILabel        *contentLbl;
@property (nonatomic, strong) SearchResultEmptyView *emptyView;  //空白页
@property (nonatomic, strong) HomeCateItemReusableView *reusableView;  //筛选视图

@property (nonatomic, strong) NSString       *keyword;      //关键字
@property (nonatomic, strong) NSMutableArray *hotWordArray; //热搜词

@property (nonatomic, assign) NSInteger      sort;             // 排序方法 1综合 2销量 3价格
@property (nonatomic, assign) NSInteger      sortType;         // 0 升序  1降序
@property (nonatomic, assign) BOOL           isSort;           // 是否是否排序
@property (nonatomic, assign) BOOL           isReset;          // 下拉属性排序是否重置
@property (nonatomic, assign) BOOL           isCompose;        //是否组合页
@property (nonatomic, copy)   NSString       *url;             //多组合内容
@property (nonatomic, strong) NSMutableArray *lastSingleArray; //上次是否留单

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSMutableArray * dataArray;


@end

@implementation YY_SearchResultViewController

#pragma mark -Life cycle
//- (instancetype)init {
//    self = [super initWithTableView];
//    if (self) {
//        self.isCustomView = YES;
//        self.isAddReachBottomView = YES;
//        self.footerBgStyleColor = XHStoreGrayColor;
//    }
//    return self;
//}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewWaterFallLayout *waterFallLayout = [[UICollectionViewWaterFallLayout alloc]init];
        waterFallLayout.delegate = self;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:waterFallLayout];
        _collectionView.backgroundColor = XHLightColor;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[YYSearchResultCollectionCell class] forCellWithReuseIdentifier:@"YYSearchResultCollectionCell"];
        _collectionView.mj_header = [YYRacingBeckRefreshHeader headerWithRefreshingTarget:self refreshingAction:@selector(pullDownRefresh)];
       _collectionView.mj_footer = [YYBeckRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(pullUpRefresh)];
//        YYBeckRefreshFooter *footer = (YYBeckRefreshFooter *)_collectionView.mj_footer;
//        footer.automaticallyRefresh = YES;
//        footer.autoTriggerTimes = -1;
//        footer.triggerAutomaticallyRefreshPercent = -kScreenH;
        _collectionView.mj_footer.hidden = YES;
    }
    return _collectionView;
}

- (HomeCateItemReusableView *)reusableView {
    if(!_reusableView) {
        _reusableView = [[HomeCateItemReusableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 45)];
        @weakify(self);
        _reusableView.itemBlock = ^(NSInteger index, YYPriceFilterType filterType) {
           @strongify(self);
           [YYCenterLoading showCenterLoading];
           self.isSort = YES;
           switch (index) {
               case 0:
                   self.sort = 1;
                   self.sortType = 1;
                   self.current = 0;
                   break;
               case 1:
                   self.sort = 3;
                   self.sortType = filterType-1;
                   self.current = 0;
                   break;
               case 2:
                   self.sort = 2;
                   self.sortType = 1;
                   self.current = 0;
                   break;
               default:
                   break;
           }
           [self scrollViewTop];
           [self loadPullDownPage];
        };
    }
    return _reusableView;
}

- (NSMutableArray *)dataArray {
    if(!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.sort = 1;
    self.sortType = 1;
    self.isReset = YES;
    self.isSort = NO;
    self.isCompose = YES;
    self.lastSingleArray = [NSMutableArray array];

    [self xh_navBottomLine:XHWhiteColor];
    [self xh_popTopRootViewController:NO];
    
    if (kValidDictionary(self.parameter)) {
        // 有参数表示是类目组合页
        if ([self.parameter objectForKey:@"linkType"]) {
            NSInteger linkType = [[self.parameter objectForKey:@"linkType"] integerValue];
            if (linkType == 3 || linkType == 5) {
                self.isCompose = NO;
            }
            else if (linkType == 13) {
                self.isCompose = YES;
            }
        }
        if ([self.parameter objectForKey:@"url"]) {
            if (self.isCompose) {
                self.url = [self.parameter objectForKey:@"url"];
            }
            else {
                self.keyword = [self.parameter objectForKey:@"url"];
            }
        }
        self.isCustomView = NO;
    }
    
    if (self.isCompose) {
        [self setNavComposeTitleView];
    }
    else {
        [self setNavTitleView];
        [YYCommonTools umengEvent:@"searchResult" attributes:nil number:@(10)];
    }
    
    [self loadNewer];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.reusableView];
    [self.reusableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(45);
    }];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.reusableView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
}

#pragma mark - Request
/**
 *  热搜词请求
 */
- (void)getHotWordData {
   HotSearchRequestAPI *navAPI = [[HotSearchRequestAPI alloc] initHotSearchRequest:[[NSDictionary dictionary]mutableCopy]];
    @weakify(self);
    [navAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            @strongify(self);
            self.hotWordArray = [[NSArray modelArrayWithClass:[KeyWordModel class]
                                                         json:[responDict objectForKey:@"items"]] mutableCopy];
            [self setupShopCategory];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - Base
- (void)loadPullDownPage {
    if (self.isSort) {
        self.isReset = NO;
    }
    else {
        self.sort = 1;
        self.sortType = 1;
        self.isReset = YES;
    }
    self.current = 0;
    [self.dataArray removeAllObjects];
    self.lastSingleArray = [NSMutableArray array];
    self.collectionView.mj_footer.hidden = YES;
    [self.collectionView reloadData];
    [super loadPullDownPage];
}

- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    //上拉加载
    if (kValidArray(self.dataIds)) {
        [dict removeAllObjects];
        if ((self.totalLength+self.pageLength) > self.dataIds.count) {
            self.pageLength = self.dataIds.count - self.totalLength;
        }
        [dict setObject:[[self.dataIds subarrayWithRange:NSMakeRange(self.totalLength, self.pageLength)] componentsJoinedByString:@","] forKey:@"uris"];
        [dict setObject:@(YES) forKey:@"isSearch"];// 是否搜索功能下拉加载
        YYListPageInfoRequestAPI *pageAPI = [[YYListPageInfoRequestAPI alloc] initListPageInfoRequest:dict];
        return pageAPI;
    }
    else {
        //下拉加载
        self.totalLength = 0;
        self.pageLength = 0;
        self.current = 0;
        [self.dataIds removeAllObjects];
        
        [dict removeAllObjects];
        [dict setObject:@(self.sortType) forKey:@"sortType"];
        [dict setObject:@(self.sort) forKey:@"sort"];
        if (self.isCompose) {
            [dict setObject:self.url forKey:@"uri"];
            YYCagegoryComposeInfoRequestAPI *composeAPI = [[YYCagegoryComposeInfoRequestAPI alloc] initCategoryComposeInfoRequest:dict];
            return composeAPI;
        } else {
            [dict setObject:[self.keyword URLEncodedString] forKey:@"word"];
            YYSearchResultAPI *homeAPI = [[YYSearchResultAPI alloc] initSearchResultRequest:dict];
            return homeAPI;
        }
    }
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    self.collectionView.mj_footer.hidden = NO;
    [self.collectionView.mj_header endRefreshing];
    [self.collectionView.mj_footer endRefreshing];
    self.isSort = NO;
    if (kValidDictionary(responseDic)) {
        NSMutableArray *tempItems = [NSMutableArray array];
        //处理所有的ids
        if ([responseDic objectForKey:@"title"]) {
            NSString *navTitle = [responseDic objectForKey:@"title"];
            if (kValidString(navTitle)) {
                [self xh_addTitle:navTitle];
            }
        }
//        SLog(@"获取到的itemIds是:%@",[[responseDic objectForKey:@"itemIds"] count]);
        if ([responseDic objectForKey:@"itemIds"]) {
            NSArray *itemIds = [responseDic objectForKey:@"itemIds"];
            if (!kValidArray(itemIds) && !kValidArray(self.dataArray)) {
               self.collectionView.mj_footer.hidden = YES;
               self.emptyType = self.NoDataType;
               self.collectionView.ly_emptyView = [YYEmptyView yyEmptyView:self.NoDataType
                                                                  title:self.emptyTitle
                                                                 target:self
                                                                 action:@selector(refreshData)];
            }
            if (kValidArray(itemIds)) {
                self.dataIds = [itemIds mutableCopy];
            }
            else {
//                if (!self.isCompose) {
//                    [self getHotWordData];
//                }
                return nil;
            }
        }
//        SLog(@"获取到的items是:%@",[responseDic objectForKey:@"items"]);
        //处理返回的items
        if ([responseDic objectForKey:@"items"]) {
            NSArray *items = [responseDic objectForKey:@"items"];
            if (!kValidArray(items) && !kValidArray(self.dataArray)) {
               self.collectionView.mj_footer.hidden = YES;
               self.emptyType = self.NoDataType;
               self.collectionView.ly_emptyView = [YYEmptyView yyEmptyView:self.NoDataType
                                                                  title:self.emptyTitle
                                                                 target:self
                                                                 action:@selector(refreshData)];
           }
//            if (!kValidArray(items)) {
//                self.totalLength = self.totalLength + self.pageLength;
//                NSMutableArray *marr = [NSMutableArray array];
//                if (self.totalLength >= self.dataIds.count && kValidArray(self.lastSingleArray)) {
//                    [marr addObject:self.lastSingleArray];
//                }
//                self.pageLength = 30;
//                return marr;
//            }
//            if (kValidArray(items)) {
                tempItems = [items mutableCopy];
//            } else {
//                self.pageLength = 30;
//                self.totalLength = self.totalLength + self.pageLength;
//                [self.collectionView reloadData];
//                return nil;
//            }
        } else {
            if (!self.isCompose) {
                [self getHotWordData];
            }
        }
        if(self.current == 0) {
            self.pageLength = tempItems.count;
        }
        self.current ++;
        NSMutableArray *itemArrays = [self dataSourcePackage:tempItems];
        [self.dataArray addObjectsFromArray:itemArrays];
        [self.collectionView reloadData];
        
        SLog(@"获取到的items是:%lu",(unsigned long)tempItems.count);
        if(!self.isCompose) {
            if(tempItems.count <= 20) {
                self.totalLength = self.dataIds.count;
                YYBeckRefreshFooter *footer = (YYBeckRefreshFooter *)self.collectionView.mj_footer;
                footer.contentLbl.text = @"我是有底线的";
                [self.collectionView.mj_footer endRefreshingWithNoMoreData];
                [self.collectionView.mj_footer resetNoMoreData];
                return nil;
            }
        }
        if(self.dataArray.count < 20) {
            [self loadNextPage];
        }
        
//        return itemArrays;
        return nil;
    }
    return nil;
}

/*
 1、首先第一次请求会请求到一堆itemIds数组，和第一次请求到的数据items，比如2条数据
 2、下一次上拉加载请求从itemIds数组取【2，32】30条数据，去请求
 3、知道itemIds数组遍历完成
 **/
- (void)refreshData {
    [self.collectionView.mj_header beginRefreshing];
}

/**
 分页数据源封装
 **/
- (NSMutableArray *)dataSourcePackage:(NSMutableArray *)itemArrays {
    __block NSMutableArray *dataSource = [NSMutableArray array];
    __block NSMutableArray *towAtRow = [NSMutableArray array];
    
    [itemArrays enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *singleObj = (NSDictionary *)obj;
        HomeListStructInfoModel *structModel = [HomeListStructInfoModel new];
        structModel.type = @(4);
        if (kValidDictionary(singleObj)) {
            HomeListItemGoodInfoModel *itemModel = [HomeListItemGoodInfoModel modelWithJSON:singleObj];
            YYImageInfoModel *imageModel = [YYImageInfoModel modelWithJSON:singleObj];
            itemModel.coverImage = imageModel;
            structModel.item = itemModel;
        }
        if (kValidDictionary(singleObj)) {
            YYCompositeShareInfoModel *shareModel = [YYCompositeShareInfoModel modelWithJSON:singleObj];
            structModel.shareInfo = shareModel;
        }
        [towAtRow addObject:structModel];
    }];
    
//    if (kValidArray(self.lastSingleArray)) {
//        [towAtRow insertObject:[self.lastSingleArray firstObject] atIndex:0];
//        self.lastSingleArray = [NSMutableArray array];
//    }
//    if (towAtRow.count % 2 != 0) {
//        self.lastSingleArray = [NSMutableArray array];
//        [self.lastSingleArray addObject:[towAtRow lastObject]];
//        [towAtRow removeLastObject];
//    }
//    if (kValidArray(towAtRow)) {
//        [dataSource addObjectsFromArray:[YYCommonTools splitArray:towAtRow withSubSize:2]];
//    }
    self.totalLength = self.totalLength + self.pageLength;
//    if (self.totalLength >= self.dataIds.count && kValidArray(self.lastSingleArray)) {
//        [dataSource addObject:self.lastSingleArray];
//    }
//    10 3 2 1
    //21 2
    self.pageLength = 30;
    return towAtRow;
}


- (BOOL)canLoadMore {
    BOOL canLoad = self.dataIds.count > self.totalLength;
    YYBeckRefreshFooter *footer = (YYBeckRefreshFooter *)self.collectionView.mj_footer;
    if(!canLoad) {
        [self.collectionView.mj_footer resetNoMoreData];
        footer.contentLbl.text = @"我是有底线的";
    } else {
        footer.contentLbl.text = @"上拉加载更多";
    }
    return canLoad;
}

#pragma mark - Private
// 创建空白页面
- (void)setupShopCategory {
    @weakify(self);
    self.emptyView = [[SearchResultEmptyView alloc] init];
    self.emptyView.frame = CGRectMake(0, 50, kScreenW, kScreenH-120);
    self.emptyView.keyArr = self.hotWordArray;
    self.emptyView.keyWord = self.keyword;
    self.emptyView.ClickBlock = ^(NSString *word, NSInteger index) {
        @strongify(self);
        self.keyword = word;
        self.sort = 1;
        self.sortType = 1;
        self.isReset = YES;
        self.isSort = NO;
        KeyWordModel *model = self.hotWordArray[index];
        if (model.linkType == 5) {
            self.contentLbl.text = self.keyword;
            [self loadNewer];
        }
        else {
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:@(model.linkType) forKey:@"linkType"];
            if (kValidString(model.linkUrl)) {
                [params setObject:model.linkUrl forKey:@"url"];
            }
            [YYCommonTools skipMultiCombinePage:self
                                         params:params];
        }
    };
    self.collectionView.ly_emptyView = [LYEmptyView emptyViewWithCustomView:self.emptyView];
}

// 搜索导航视图
- (void)setNavTitleView {
    @weakify(self);
    UIButton *searchBtn = [YYCreateTools createBtnImage:nil];
    searchBtn.backgroundColor = HexRGB(0xF4F4F4);
    searchBtn.titleLabel.font = normalFont(14);
    searchBtn.layer.cornerRadius = kSizeScale(4);
    searchBtn.clipsToBounds = YES;
    
    [self xh_addTitleView:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(44);
        make.height.mas_equalTo(kSizeScale(32));
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-((44-kSizeScale(32))/2.0 - 2));
    }];
    
    self.contentLbl = [YYCreateTools createLabel:nil
                                            font:normalFont(14)
                                       textColor:XHBlackColor];
    [searchBtn addSubview:self.contentLbl];
    [self.contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchBtn.mas_left).offset(10);
        make.centerY.equalTo(searchBtn);
        make.right.equalTo(searchBtn.mas_right).offset(-10);
    }];
    self.contentLbl.text = self.keyword;
    
    UIButton *cleanBtn = [YYCreateTools createBtnImage:@"Search_result_clean"];
    cleanBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        self.contentLbl.text = @"";
        /*if (self.NavClickBlock) {
         self.NavClickBlock(self.contentLbl.text);
         }*/
        [self.navigationController popViewControllerAnimated:NO];
    };
    [searchBtn addSubview:cleanBtn];
    [cleanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBtn);
        make.right.equalTo(searchBtn.mas_right).offset(-kSizeScale(5));
    }];
    
    [self xh_addTitleView:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(44);
        make.height.mas_equalTo(kSizeScale(32));
        make.right.mas_equalTo(-15);
        make.bottom.mas_equalTo(-((44-kSizeScale(32))/2.0 - 2));
    }];
    searchBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        /*if (self.NavClickBlock) {
         self.NavClickBlock(self.contentLbl.text);
         }*/
        [self.navigationController popViewControllerAnimated:NO];
    };
    
    
}

// 类目组合导航
- (void)setNavComposeTitleView {
    @weakify(self);
    [self xh_addNavigationItemWithImageName:@"compose_search_icon"
                                     isLeft:NO
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     [YYCommonTools skipSearchPage:self title:@"" type:0];
                                 }];
}


#pragma mark - UITabelViewDelegate

#pragma mark -UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YYSearchResultCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YYSearchResultCollectionCell" forIndexPath:indexPath];
    if(indexPath.row <= self.dataArray.count) {
        cell.model = self.dataArray[indexPath.row];
    }
    return cell;
}

#pragma mark -UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row <= self.dataArray.count) {
        HomeListStructInfoModel *carrayModel = self.dataArray[indexPath.row];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:@(1) forKey:@"linkType"];
        if (kValidString(carrayModel.item.url)) {
            [params setObject:carrayModel.item.url forKey:@"url"];
        }
        [YYCommonTools skipMultiCombinePage:self params:params];
    }
}


#pragma mark -UICollectionViewWaterFallLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout heightForWidth:(CGFloat)width atIndexPath:(NSIndexPath*)indexPath {
    return kSizeScale(65) + width;
}

//区列数
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout colCountForSectionAtIndex:(NSInteger)section {
    return 2;
}

//区内边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(kSizeScale(8), kSizeScale(12),kSizeScale(8), 0);
}

//垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout lineSpacingForSectionAtIndex:(NSInteger)section {
    return kSizeScale(8);
}

//水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewWaterFallLayout*)collectionViewLayout interitemSpacingForSectionAtIndex:(NSInteger)section {
    return kSizeScale(8);
}

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (kValidArray(self.listData)) {
//        return self.canLoadMore == YES? self.listData.count : self.listData.count + 1;
//    }
//    return self.listData.count;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
//    if (!cell) {
//        NSInteger row = indexPath.row;
//        Class current = [HomeSpecialSellDoubleInfoCell class];
//        HomeSpecialSellDoubleInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
//        cell.delegate = self;
//        cell.itemModels = [self.listData objectAtIndex:row];
//        return cell;
//    }
//    return cell;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
//    if (!height) {
//        CGFloat width = (kScreenW - kSizeScale(8) - kSizeScale(12)*2)/2.0;
//        return kSizeScale(80) + width;
//    }
//    return height;
//}
//

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if (!self.reusableView) {
//        self.reusableView = [[HomeCateItemReusableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 45)];
//    }
//     @weakify(self);
//     self.reusableView.itemBlock = ^(NSInteger index, YYPriceFilterType filterType) {
//        @strongify(self);
//        [YYCenterLoading showCenterLoading];
//        self.isSort = YES;
//        switch (index) {
//            case 0:
//                self.sort = 1;
//                self.sortType = 1;
//                self.current = 0;
//                break;
//            case 1:
//                self.sort = 3;
//                self.sortType = filterType-1;
//                self.current = 0;
//                break;
//            case 2:
//                self.sort = 2;
//                self.sortType = 1;
//                self.current = 0;
//                break;
//            default:
//                break;
//        }
//        [self scrollViewTop];
//        [self loadPullDownPage];
//     };
//     if (self.isReset) {
//        self.reusableView.isReset = YES;
//     }
//     return self.reusableView;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 45;
//}

#pragma mark - HomeSpecialSellDoubleInfoCellDelegate
//- (void)homeSpecialSellDoubleCellCarrayModel:(HomeListStructInfoModel *)carrayModel isShare:(BOOL)isShare {
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setObject:@(1) forKey:@"linkType"];
//    if (kValidString(carrayModel.item.url)) {
//        [params setObject:carrayModel.item.url forKey:@"url"];
//    }
//    [YYCommonTools skipMultiCombinePage:self params:params];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

