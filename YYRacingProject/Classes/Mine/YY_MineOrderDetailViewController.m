//
//  YY_MineOrderDetailViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineOrderDetailViewController.h"

#import "OrderDetailInfoModel.h"

#import "OrderListGoodInfoCell.h"
#import "OrderDetailTimeInfoCell.h"
#import "OrderDetailKefuInfoCell.h"
#import "OrderDetailHeaderInfoCell.h"
#import "OrderDetailSectionHeaderInfoView.h"
#import "OrderDetailSectionFooterInfoView.h"

#import "MineOrderDetailInfoRequestAPI.h"
#import "YYStoreOrderDetailInfoRequestAPI.h"
#import "MineAfterSaleCancelApplyInfoRequestAPI.h"

#define kDetailOperatorTag 450
@interface YY_MineOrderDetailViewController ()
<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, strong) NSNumber  *orderID;
@property (nonatomic, strong) NSNumber  *pageSource;
@property (nonatomic, strong) NSMutableArray *operatorModel;
@property (nonatomic, assign) NSInteger tableViewSectionCount;
@property (nonatomic, strong) OrderDetailInfoModel *detailModel;

@property (nonatomic, assign) BOOL      isExitSale;     //是否存在售后
@property (nonatomic, assign) BOOL      isReceiving;    //是否点击收货
@property (nonatomic, strong) UIView    *bottomView;
@end

@implementation YY_MineOrderDetailViewController

#pragma mark - Setter method
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [YYCreateTools createView:XHWhiteColor];
    }
    return _bottomView;
}

#pragma mark - Life cycyle

- (void)viewDidLoad {
    [super viewDidLoad];
    @weakify(self);
    [self xh_addTitle:@"订单详情"];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"source"]) {
            self.pageSource = [self.parameter objectForKey:@"source"];
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOrderDetail)
                                                 name:RefreshOrderDetailNotification
                                               object:nil];
    [self xh_addNavigationItemWithImageName:@"detail_navbar_back"
                                     isLeft:YES
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     if (self.confirmReceivingBlock) {
                                         self.confirmReceivingBlock(self.detailModel.orderInfo.orderStatus, self.detailModel.orderInfo.operation ,self.isReceiving);
                                     }
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }];
    
    Class currentCls = [OrderDetailHeaderInfoCell class];
    self.tableStyle = UITableViewStyleGrouped;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    //self.m_tableView.estimatedRowHeight = 0;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.backgroundColor = XHLightColor;
    
    registerClass(self.m_tableView, currentCls);
    currentCls = [OrderListGoodInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [OrderDetailKefuInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [OrderDetailTimeInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    currentCls = [OrderDetailSectionHeaderInfoView class];
    [self.m_tableView registerClass:currentCls forHeaderFooterViewReuseIdentifier:strFromCls(currentCls)];
    self.m_tableView.backgroundColor = XHLightColor;
    
    [self.view addSubview:self.m_tableView];
    [self.view addSubview:self.bottomView];

    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"orderID"]) {
            self.orderID = [self.parameter objectForKey:@"orderID"];
        }
    }
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    
    [kCountDownManager start];
    [self gainOrderDetailRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request
/**获取订单详情**/
- (void)gainOrderDetailRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.orderID forKey:@"orderID"];
    
    [YYCenterLoading showCenterLoading];
    YYBaseRequestAPI *detailAPI = nil;
    if ([self.pageSource integerValue] == 1) {
        detailAPI =  [[MineOrderDetailInfoRequestAPI alloc] initOrderDetailInfoRequest:params];
    }
    else {
        detailAPI =  [[YYStoreOrderDetailInfoRequestAPI alloc] initOrderDetailInfoRequest:params];
    }
    [detailAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.tableViewSectionCount = 3;
            self.detailModel = [OrderDetailInfoModel modelWithJSON:responDict];
            if ([self.detailModel.orderInfo.orderStatus integerValue] == 7 || [self.detailModel.orderInfo.orderStatus integerValue] == 8) {
                self.detailModel.isShowDeliveryInfo = YES;
            }
            if (self.detailModel.expressInfo) {
                if (!self.detailModel.expressInfo.momentInfo && !self.detailModel.expressInfo.momentTime) {
                    self.detailModel.isShowDeliveryInfo = NO;
                }
            }
            [kCountDownManager addSourceWithIdentifier:@"orderDetail"];
            
            __block BOOL  isHaveSaleServiceBtn = NO;  //是否存在提交售后
            __block NSInteger salePoint = -1;
            NSInteger deadTime = [self.detailModel.orderInfo.payDeadlineTime integerValue]/1000;
            NSInteger systemTime = [self.detailModel.systemTime integerValue]/1000;
            self.detailModel.diffValue = deadTime - systemTime;
        
            self.operatorModel = [NSMutableArray array];
            self.operatorModel = [YYCommonTools getOrderShowOperatorBtns:self.detailModel.orderInfo.operation];
            [self.operatorModel enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYOperatorInfoModel *infoModel = (YYOperatorInfoModel *)obj;
                if (infoModel.operatorType == YYOrderOperatorCommit) {
                    isHaveSaleServiceBtn = YES;
                    salePoint = idx;
                    *stop = YES;
                }
            }];
            if (isHaveSaleServiceBtn && salePoint != -1) {
                [self.operatorModel removeObjectAtIndex:salePoint];
            }
            self.isExitSale = isHaveSaleServiceBtn;
            [self.bottomView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            
            
            if ([self.detailModel.orderInfo.operation integerValue] > 1 && self.operatorModel.count > 0) {
                // 存在底部操作按钮
                [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(-kBottom(0));
                    make.left.right.equalTo(self.view);
                    make.height.mas_equalTo(kSizeScale(50));
                }];
                [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make){
                    make.left.right.equalTo(self.view);
                    make.top.mas_equalTo(kNavigationH);
                    make.bottom.equalTo(self.bottomView.mas_top);
                }];
                [self operatorBtnIsShow:self.operatorModel];
            }
            else {
                // 不存在底部操作按钮
                self.bottomView.hidden = YES;
                [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make){
                    make.left.right.bottom.equalTo(self.view);
                    make.top.mas_equalTo(kNavigationH);
                }];
            }
            
            [UIView performWithoutAnimation:^{
                [self.m_tableView reloadData];
            }];
        }
        else {
            self.tableViewSectionCount = 0;
            self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                               title:nil
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.tableViewSectionCount = 0;
        self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                           title:nil
                                                          target:self
                                                          action:@selector(emptyBtnOperator:)];
    }];
}

/**取消售后申请**/
- (void)cancelAfterSaleApply:(NSMutableDictionary *)params {
    MineAfterSaleCancelApplyInfoRequestAPI *cancelAPI = [[MineAfterSaleCancelApplyInfoRequestAPI alloc] initAfterSaleCancelApplyInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [cancelAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self gainOrderDetailRequest];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - Private method
- (void)operatorBtnIsShow:(NSMutableArray *)operatorModels {
    if (kValidArray(operatorModels)) {
        CGFloat btnWidth = kSizeScale(68);
        CGFloat btnHeight = kSizeScale(25);
        UIButton *lastBtn = nil;
        for (int i = 0; i < operatorModels.count ; i ++) {
            UIButton *btn = [YYCreateTools createBtn:@""
                                                font:midFont(12)
                                           textColor:XHBlackMidColor];
            btn.tag = kDetailOperatorTag + i;
            btn.layer.cornerRadius = 2;
            btn.layer.masksToBounds = YES;
            [btn addTarget:self action:@selector(buttonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:btn];
    
            YYOperatorInfoModel *infoModel = operatorModels[i];
            if (infoModel.isWaitPay) {
                btnWidth = kSizeScale(82);
            }
            else {
                btnWidth = kSizeScale(68);
            }
            
            btn.layer.borderWidth = 0.8;
            btn.layer.borderColor = (infoModel.isSelect ? XHLoginColor.CGColor : XHLineColor.CGColor);
            [btn setTitle:infoModel.showTitle forState:UIControlStateNormal];
            [btn setTitleColor:(infoModel.isSelect?XHLoginColor:XHBlackMidColor) forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageWithColor:XHWhiteColor] forState:UIControlStateNormal];
            
            if (lastBtn) {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(lastBtn);
                    make.right.equalTo(lastBtn.mas_left).offset(-kSizeScale(12));
                    make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                }];
            }
            else {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.bottomView).offset(-kSizeScale(3));
                    make.right.equalTo(self.bottomView.mas_right).offset(-kSizeScale(12));
                    make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                }];
            }
            lastBtn = btn;
        }
    }
}

/**
 *  付款状态，地址cell
 */
- (void)configPayAndAddress:(OrderDetailHeaderInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    cell.detailModel = self.detailModel;
}

/**
 *  商品仓库， 商品信息cell
 */
- (void)configGoodAndInfo:(OrderListGoodInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    cell.fd_enforceFrameLayout = NO;
    if (kValidArray(self.detailModel.orderInfo.skuList) && row < self.detailModel.orderInfo.skuList.count) {
        OrderGoodInfoModel *goodModel = [self.detailModel.orderInfo.skuList objectAtIndex:row];
        goodModel.showSale = self.isExitSale;
        goodModel.isDetail = YES;
        cell.goodModel = goodModel;
    }
}

/**
 * 订单金额cell
 */
- (void)configOrderMoneyCell:(OrderDetailKefuInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    cell.summaryList = self.detailModel.orderInfo.summaryList;
}

/**
 * 订单时间cell
 */
- (void)configOrderTimeCell:(OrderDetailTimeInfoCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.fd_enforceFrameLayout = NO;
    cell.orderNumber = self.detailModel.orderInfo.orderId;
    cell.createTime = [NSString formatDateAndTime:[self.detailModel.orderInfo.orderTime longValue]];
    cell.remarkContent = self.detailModel.orderInfo.remark;
    if ([self.detailModel.orderInfo.payTime longValue] > 0) {
        cell.payTime = [NSString formatDateAndTime:[self.detailModel.orderInfo.payTime longValue]];
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // 1 区 显示付款状态，地址
    // 2 区 显示商家仓库，商品信息
    // 3 区 显示订单时间
    return self.tableViewSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section != 1) {
        return section == 2 ? 2 : 1;
    }
    else {
        if (self.detailModel && kValidArray(self.detailModel.orderInfo.skuList)) {
            return self.detailModel.orderInfo.skuList.count;
        }
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @weakify(self);
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    Class currentCls = [OrderDetailHeaderInfoCell class];
    if (section == 0) {
        OrderDetailHeaderInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        [self configPayAndAddress:cell
                      atIndexPath:indexPath];
        cell.lookLogisticBlock = ^{
            @strongify(self);
            [YYCommonTools skipLogisticsPage:self params:@{@"orderID": self.detailModel.orderInfo.orderId}];
        };
        return cell;
    }
    else if (section == 1) {
        currentCls = [OrderListGoodInfoCell class];
        OrderListGoodInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        [self configGoodAndInfo:cell
                    atIndexPath:indexPath];
        cell.afterSaleClick = ^(OrderGoodInfoModel *goodModel, YYGoodAfterSaleOperatorType operatorType) {
            if (operatorType == YYGoodAfterSaleOperatorApply) {
                [YYCommonTools skipAfterSaleChoose:self
                                           orderID:[goodModel.orderId longValue]
                                             skuID:[goodModel.skuId intValue]];
            }
            else if (operatorType == YYGoodAfterSaleOperatorCancel) {
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                [params setObject:goodModel.orderId forKey:@"orderId"];
                [params setObject:goodModel.skuId forKey:@"skuId"];
                [self cancelAfterSaleApply:params];
            }
            else {
                @strongify(self);
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                [params setObject:@([self.pageSource intValue] - 1) forKey:@"type"];
                [params setObject:goodModel.skuId forKey:@"spuId"];
                [params setObject:goodModel.orderId forKey:@"orderId"];
                [YYCommonTools skipAfterSaleDetailPage:self
                                                params:params];
            }
        };
        cell.orderClick = ^(OrderGoodInfoModel *goodModel, OrderListGoodInfoCell *listCell) {
            if (goodModel.itemType == 1) {
                // 为大礼包商品不可进商品详情
                return ;
            }
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if (kValidString(goodModel.itemUri)) {
                [params setObject:goodModel.itemUri forKey:@"url"];
            }
            [params setObject:@(1) forKey:@"linkType"];
            [YYCommonTools skipMultiCombinePage:self params:params];
        };
        return cell;
    }
    else {
        if (row == 0) {
            currentCls = [OrderDetailKefuInfoCell class];
            OrderDetailKefuInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
            [self configOrderMoneyCell:cell atIndexPath:indexPath];
            cell.kefuClickBlock = ^{
                @strongify(self);
//                OrderGoodInfoModel *goodModel = [self.detailModel.orderInfo.skuList firstObject];
//
//                NSMutableDictionary *params = [NSMutableDictionary dictionary];
//                HDOrderInfo *orderInfo = [[HDOrderInfo alloc] init];
//                orderInfo.imageUrl = goodModel.skuCover;
//                orderInfo.title = [NSString stringWithFormat:@"订单编号：%@",goodModel.orderId];
//                orderInfo.price = [NSString stringWithFormat:@"%@",[goodModel.price objectForKey:@"priceTag"]];
//                orderInfo.orderTitle = goodModel.skuTitle;
//                orderInfo.desc = [NSString stringWithFormat:@"%@", self.pageSource];
//                orderInfo.itemUrl = [NSString stringWithFormat:@"%@", goodModel.orderId];
//                [params setObject:orderInfo forKey:@"orderInfo"];
//
//                [YYCommonTools skipKefu:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
                NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"4000568110"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
            };
            return cell;
        }
        else {
            currentCls = [OrderDetailTimeInfoCell class];
            OrderDetailTimeInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
            [self configOrderTimeCell:cell atIndexPath:indexPath];
            return cell;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 1) {
        UIView *sectionHeaderView = [YYCreateTools createView:XHWhiteColor];
        return sectionHeaderView;
    }
    else {
        Class currentCls = [OrderDetailSectionHeaderInfoView class];
        OrderDetailSectionHeaderInfoView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:strFromCls(currentCls)];
        if (self.detailModel) {
            sectionHeaderView.businessWarehouse = self.detailModel.orderInfo.sellerAddr;
        }
        return sectionHeaderView;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *sectionFooterView = [YYCreateTools createView:XHWhiteColor];
    return sectionFooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 1 ? kSizeScale(51) : 0.00001);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.000001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    if (section == 0) {
        return [tableView fd_heightForCellWithIdentifier:@"OrderDetailHeaderInfoCell" cacheByIndexPath:indexPath configuration:^(OrderDetailHeaderInfoCell *cell) {
            [self configPayAndAddress:cell atIndexPath:indexPath];
        }];
    }
    else if (section == 1) {
        return [tableView fd_heightForCellWithIdentifier:@"OrderListGoodInfoCell" cacheByIndexPath:indexPath configuration:^(OrderListGoodInfoCell *cell) {
            [self configGoodAndInfo:cell atIndexPath:indexPath];
        }];
    }
    else if (section == 2) {
        if (row == 0) {
            return [tableView fd_heightForCellWithIdentifier:@"OrderDetailKefuInfoCell" cacheByIndexPath:indexPath configuration:^(OrderDetailKefuInfoCell *cell) {
                [self configOrderMoneyCell:cell atIndexPath:indexPath];
            }];
        }
        else {
            return [tableView fd_heightForCellWithIdentifier:@"OrderDetailTimeInfoCell" cacheByIndexPath:indexPath configuration:^(OrderDetailTimeInfoCell *cell) {
                [self configOrderTimeCell:cell atIndexPath:indexPath];
            }];
        }
    }
    return UITableViewAutomaticDimension;
}

#pragma mark - Event
- (void)buttonClickEvent:(UIButton *)sender {
    NSInteger currentTag = sender.tag - kDetailOperatorTag;
    if (currentTag < self.operatorModel.count) {
        YYOperatorInfoModel *model = self.operatorModel[currentTag];
        if (model.operatorType == YYOrderOperatorConfirm) {
            //确认收货
            self.isReceiving = YES;
            [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确认收货吗？收到商品后再确认收货，以免造成损失。" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
                
            } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
                [kWholeConfig confirmOrderRequest:[NSString stringWithFormat:@"%@", self.orderID] success:^(BOOL isSuccess) {
                    if (isSuccess) {
                        [self gainOrderDetailRequest];
                    }
                }];
            } type:JCAlertViewTypeDefault];
        }
        else if (model.operatorType == YYOrderOperatorLookLogistic) {
            //查看物流
            [YYCommonTools skipLogisticsPage:self params:@{@"orderID": self.detailModel.orderInfo.orderId}];
        }
    }
}

- (void)refreshOrderDetail {
    [self gainOrderDetailRequest];
}

- (void)emptyBtnOperator:(UIButton *)sender {
    [self gainOrderDetailRequest];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
