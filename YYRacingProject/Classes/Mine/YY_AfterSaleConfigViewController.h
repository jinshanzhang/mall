//
//  YY_AfterSaleConfigViewController.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_AfterSaleConfigViewController : YYBaseViewController

@property (nonatomic, assign) int    submitType;

@property (nonatomic, copy)   NSString *TypeDescribe;

@property (nonatomic, assign) long orderID;

@property (nonatomic, assign) int skuID;

@property (nonatomic, assign) BOOL FromChoose;

@property (nonatomic, assign) BOOL ChangeConfig;

@property (nonatomic, assign) BOOL Arbitration;

@property (nonatomic, copy) void(^receiveBlock)(NSString *result);

@end
