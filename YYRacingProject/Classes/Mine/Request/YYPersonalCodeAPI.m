//
//  YYPersonalCodeAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/16.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYPersonalCodeAPI.h"

@implementation YYPersonalCodeAPI

- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,SELLERQRCODE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
