//
//  AfterSaleServiceAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface AfterSaleServiceAPI : YYBaseRequestAPI

- (instancetype)initAfterSaleRequest:(NSMutableDictionary *)params;


@end
