//
//  MineOrderDeleteInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineOrderDeleteInfoRequestAPI.h"

@implementation MineOrderDeleteInfoRequestAPI

- (instancetype)initOrderDeleteInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,DELETEORDER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
