//
//  YYscanVsellerQrAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYscanVsellerQrAPI.h"

@implementation YYscanVsellerQrAPI

- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,SCANVSELLER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
