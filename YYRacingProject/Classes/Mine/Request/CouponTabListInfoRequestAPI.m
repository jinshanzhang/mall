//
//  CouponTabListInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CouponTabListInfoRequestAPI.h"

@implementation CouponTabListInfoRequestAPI

- (instancetype)initCouponTabListInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COUPONLIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
