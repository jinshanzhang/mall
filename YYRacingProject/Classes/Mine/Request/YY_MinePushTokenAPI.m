//
//  YY_MinePushTokenAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/8.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_MinePushTokenAPI.h"

@implementation YY_MinePushTokenAPI
- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,PUSHREGISTER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
