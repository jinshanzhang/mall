//
//  MineUpdateNickInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineUpdateNickInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initUpdateNickInfoRequest:(NSMutableDictionary *)params;

@end
