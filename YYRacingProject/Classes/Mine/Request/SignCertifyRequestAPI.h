//
//  SignCertifyRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface SignCertifyRequestAPI : YYBaseRequestAPI

- (instancetype)initCertifyRequest:(NSMutableDictionary *)params;

@end
