//
//  MineOrderConfirmInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineOrderConfirmInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initOrderConfirmInfoRequest:(NSMutableDictionary *)params;

@end
