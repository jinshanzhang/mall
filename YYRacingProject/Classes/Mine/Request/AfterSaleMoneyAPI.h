//
//  AfterSaleMoneyAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface AfterSaleMoneyAPI : YYBaseRequestAPI


- (instancetype)initAfterSaleRequest:(NSMutableDictionary *)params;

@end
