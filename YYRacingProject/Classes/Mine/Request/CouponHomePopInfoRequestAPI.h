//
//  CouponHomePopInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface CouponHomePopInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initCouponHomePopInfoRequest:(NSMutableDictionary *)params;

@end
