//
//  MineOrderLookLogisticsInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineOrderLookLogisticsInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initLookLogisticsInfoRequest:(NSMutableDictionary *)params;

@end
