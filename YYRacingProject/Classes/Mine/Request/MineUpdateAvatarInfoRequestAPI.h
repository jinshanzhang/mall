//
//  MineUpdateAvatarInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineUpdateAvatarInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initUpdateHeaderInfoRequest:(NSMutableDictionary *)params;

@end
