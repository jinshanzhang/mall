//
//  GetphoneNumberAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/22.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "GetphoneNumberAPI.h"

@implementation GetphoneNumberAPI


- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,PHONENUMBER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
