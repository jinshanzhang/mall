//
//  CouponTabListInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface CouponTabListInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initCouponTabListInfoRequest:(NSMutableDictionary *)params;

@end
