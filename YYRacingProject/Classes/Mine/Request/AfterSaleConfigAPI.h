//
//  AfterSaleConfigAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface AfterSaleConfigAPI : YYBaseRequestAPI


- (instancetype)initAfterSaleRequest:(NSMutableDictionary *)params;

@end
