//
//  MineUpdateNickInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineUpdateNickInfoRequestAPI.h"

@implementation MineUpdateNickInfoRequestAPI

- (instancetype)initUpdateNickInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,UPDATENICK];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
