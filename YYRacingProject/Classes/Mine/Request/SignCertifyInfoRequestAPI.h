//
//  SignCertifyInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface SignCertifyInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initCertifyInfoRequest:(NSMutableDictionary *)params;

@end
