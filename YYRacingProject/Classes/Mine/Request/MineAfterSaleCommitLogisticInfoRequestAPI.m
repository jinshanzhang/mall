//
//  MineAfterSaleCommitLogisticInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineAfterSaleCommitLogisticInfoRequestAPI.h"

@implementation MineAfterSaleCommitLogisticInfoRequestAPI

- (instancetype)initAfterSaleCommitLogisticsInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,APPLYLOGISTICS];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
