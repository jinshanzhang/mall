//
//  AfterSaleInsertAPi.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSaleInsertAPi.h"

@implementation AfterSaleInsertAPi



- (instancetype)initAfterSaleRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,AFRERSALEINSERT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
