//
//  MineCourceAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineCourceAPI : YYBaseRequestAPI


- (instancetype)initRequest:(NSMutableDictionary *)params;

@end
