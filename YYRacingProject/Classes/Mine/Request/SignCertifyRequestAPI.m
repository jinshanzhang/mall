//
//  SignCertifyRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "SignCertifyRequestAPI.h"

@implementation SignCertifyRequestAPI
- (instancetype)initCertifyRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CERTIFYUSER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
