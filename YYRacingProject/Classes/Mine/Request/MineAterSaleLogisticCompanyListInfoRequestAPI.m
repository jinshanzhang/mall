//
//  MineAterSaleLogisticCompanyListInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineAterSaleLogisticCompanyListInfoRequestAPI.h"

@implementation MineAterSaleLogisticCompanyListInfoRequestAPI

- (instancetype)initAfterSaleLogisticsCompanyInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,LOGISTICCOMPANY];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}


@end
