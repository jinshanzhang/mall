//
//  AfterSaleMoneyAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSaleMoneyAPI.h"

@implementation AfterSaleMoneyAPI

- (instancetype)initAfterSaleRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,SALEAFTERMONEY];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
