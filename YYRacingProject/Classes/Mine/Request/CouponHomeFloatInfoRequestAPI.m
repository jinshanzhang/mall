//
//  CouponHomeFloatInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CouponHomeFloatInfoRequestAPI.h"

@implementation CouponHomeFloatInfoRequestAPI

- (instancetype)initCouponHomeFloatInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COUPONFLOAT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
