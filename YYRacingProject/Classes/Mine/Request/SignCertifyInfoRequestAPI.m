//
//  SignCertifyInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "SignCertifyInfoRequestAPI.h"

@implementation SignCertifyInfoRequestAPI


- (instancetype)initCertifyInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CERTIFYINFO];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
