//
//  MineOrderListInfoRequest.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineOrderListInfoRequestAPI.h"

@implementation MineOrderListInfoRequestAPI

- (instancetype)initOrderListInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH3,MINEORDERLIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
