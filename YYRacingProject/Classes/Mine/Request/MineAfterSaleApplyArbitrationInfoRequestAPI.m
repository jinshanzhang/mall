//
//  MineAfterSaleApplyArbitrationInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineAfterSaleApplyArbitrationInfoRequestAPI.h"

@implementation MineAfterSaleApplyArbitrationInfoRequestAPI

- (instancetype)initAfterSaleApplyArbitrationInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,APPLYARBITRATE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}


@end
