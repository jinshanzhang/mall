//
//  MineAfterSaleListInfoRequestAPI.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineAfterSaleListInfoRequestAPI.h"

@implementation MineAfterSaleListInfoRequestAPI

- (instancetype)initOrderAfterSaleInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,SALEAFTERLIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
