//
//  AfterSaleServiceAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSaleServiceAPI.h"

@implementation AfterSaleServiceAPI

- (instancetype)initAfterSaleRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,AFRERSALETYPE];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}


@end
