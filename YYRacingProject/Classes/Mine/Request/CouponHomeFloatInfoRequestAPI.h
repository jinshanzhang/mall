//
//  CouponHomeFloatInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/30.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface CouponHomeFloatInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initCouponHomeFloatInfoRequest:(NSMutableDictionary *)params;

@end
