//
//  MineCustomerServiceInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineCustomerServiceInfoRequestAPI.h"

@implementation MineCustomerServiceInfoRequestAPI

- (instancetype)initCustomerServiceInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CUSTOMERSERVER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
