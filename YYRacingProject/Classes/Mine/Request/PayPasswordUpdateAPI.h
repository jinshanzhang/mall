//
//  PayPasswordUpdateAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/22.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface PayPasswordUpdateAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;


@end
