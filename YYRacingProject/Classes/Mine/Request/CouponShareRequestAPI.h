//
//  CouponShareRequestAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/21.
//  Copyright © 2018年 cjm. All rights reserved.
//


#import "YYBaseRequestAPI.h"

@interface CouponShareRequestAPI : YYBaseRequestAPI

- (instancetype)initCouponShareRequest:(NSMutableDictionary *)params;


@end
