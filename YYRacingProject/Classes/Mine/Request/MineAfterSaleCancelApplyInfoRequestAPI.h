//
//  MineAfterSaleCancelApplyInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineAfterSaleCancelApplyInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initAfterSaleCancelApplyInfoRequest:(NSMutableDictionary *)params;

@end
