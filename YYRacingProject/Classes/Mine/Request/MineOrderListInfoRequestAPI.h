//
//  MineOrderListInfoRequest.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineOrderListInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initOrderListInfoRequest:(NSMutableDictionary *)params;

@end
