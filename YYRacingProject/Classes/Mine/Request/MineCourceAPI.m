//
//  MineCourceAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "MineCourceAPI.h"

@implementation MineCourceAPI


- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,MINECOURSELIST];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
