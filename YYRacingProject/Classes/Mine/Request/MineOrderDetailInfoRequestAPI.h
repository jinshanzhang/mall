//
//  MineOrderDetailInfoRequestAPI.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineOrderDetailInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initOrderDetailInfoRequest:(NSMutableDictionary *)params;

@end
