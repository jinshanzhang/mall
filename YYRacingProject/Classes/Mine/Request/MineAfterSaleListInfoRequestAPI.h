//
//  MineAfterSaleListInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineAfterSaleListInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initOrderAfterSaleInfoRequest:(NSMutableDictionary *)params;

@end
