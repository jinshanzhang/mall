//
//  MineAterSaleLogisticCompanyListInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineAterSaleLogisticCompanyListInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initAfterSaleLogisticsCompanyInfoRequest:(NSMutableDictionary *)params;

@end
