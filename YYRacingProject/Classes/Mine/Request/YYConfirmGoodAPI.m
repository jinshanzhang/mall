//
//  YYConfirmGoodAPI.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/3/1.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYConfirmGoodAPI.h"

@implementation YYConfirmGoodAPI

- (instancetype)initRequest:(NSMutableDictionary *)params{
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CONFIRMEXPRESS];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}
@end
