//
//  MineAfterSaleCommitLogisticInfoRequestAPI.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

@interface MineAfterSaleCommitLogisticInfoRequestAPI : YYBaseRequestAPI

- (instancetype)initAfterSaleCommitLogisticsInfoRequest:(NSMutableDictionary *)params;

@end
