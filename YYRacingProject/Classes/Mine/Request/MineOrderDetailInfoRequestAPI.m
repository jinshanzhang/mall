//
//  MineOrderDetailInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineOrderDetailInfoRequestAPI.h"

@implementation MineOrderDetailInfoRequestAPI

- (instancetype)initOrderDetailInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    NSString *orderID = nil;
    if (kValidDictionary(self.inParams)) {
        orderID = [self.inParams objectForKey:@"orderID"];
        [self.inParams removeObjectForKey:@"orderID"];
    }
    return [NSString stringWithFormat:@"%@%@/%@",INTERFACE_PATH,ORDERDETAILS,orderID];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
