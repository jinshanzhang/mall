//
//  MineOrderCancelInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineOrderCancelInfoRequestAPI.h"

@implementation MineOrderCancelInfoRequestAPI

- (instancetype)initOrderCancelInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,CANCLEORDER];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

@end
