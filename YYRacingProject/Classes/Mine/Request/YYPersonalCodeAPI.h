//
//  YYPersonalCodeAPI.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/16.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseRequestAPI.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYPersonalCodeAPI : YYBaseRequestAPI

- (instancetype)initRequest:(NSMutableDictionary *)params;

@end

NS_ASSUME_NONNULL_END
