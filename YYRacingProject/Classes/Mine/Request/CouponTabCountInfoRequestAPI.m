//
//  CouponTabCountInfoRequestAPI.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/29.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CouponTabCountInfoRequestAPI.h"

@implementation CouponTabCountInfoRequestAPI

- (instancetype)initCouponTabCountInfoRequest:(NSMutableDictionary *)params {
    self = [super init];
    if (self) {
        self.inParams = params;
    }
    return self;
}

- (NSString *)requestUrl {
    return [NSString stringWithFormat:@"%@%@",INTERFACE_PATH,COUPONCOUNT];
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
