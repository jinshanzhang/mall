//
//  YY_AfterSaleChooseServiceController.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_AfterSaleChooseServiceController : YYBaseViewController

@property (nonatomic, assign) long orderID;

@property (nonatomic, assign) int skuID;

@property (nonatomic, copy) void (^chooseBlock)(NSInteger num);



@end
