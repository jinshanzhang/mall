//
//  YY_MineCourceViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YY_MineCourceViewController.h"
#import "YY_AudioDetailViewController.h"
#import "YY_ChapterViewController.h"

#import "MineCourceCell.h"
#import "MineCourceAPI.h"
#import "MyCourceModel.h"

@interface YY_MineCourceViewController ()

@end

@implementation YY_MineCourceViewController

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.emptyTitle = @"您还没有购买过课程~";
        self.NoDataType = YYEmptyViewCourseNoDataType;
        self.tableStyle = UITableViewStylePlain;
        self.isAddPullUpRefresh = NO;
        self.m_tableView.allowsSelection = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"我的课程"];
    [self xh_popTopRootViewController:NO];

    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 44.0;
    
    Class current = [MineCourceCell class];
    registerClass(self.m_tableView, current);
    
    self.view.backgroundColor = self.m_tableView.backgroundColor = XHLightColor;
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(50));
    }];
    
    [self loadNewer];
    // Do any additional setup after loading the view.
}

- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    MineCourceAPI *API = [[MineCourceAPI alloc] initRequest:params];
    return API;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        NSMutableArray *courceArr = [[NSArray modelArrayWithClass:[MyCourceModel class] json:[responseDic objectForKey:@"courseList"]] mutableCopy];
        return courceArr;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class currentCls = [MineCourceCell class];
    MineCourceCell *specialcell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
   if (kValidArray(self.listData) && row < self.listData.count) {
       MyCourceModel *couponModel = [self.listData objectAtIndex:row];
       specialcell.model = couponModel;
       return specialcell;
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [YYCreateTools createView:XHLightColor];
    UIView *backView = [YYCreateTools createView:XHWhiteColor];
    [view addSubview:backView];
    
    [backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(view);
        make.top.mas_equalTo(5);
    }];
    
    UIView *line = [YYCreateTools createView:XHClearColor];
    [backView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(10);
        make.width.mas_equalTo(3);
        make.height.mas_equalTo(16);
    }];
    
    UILabel *titleLabel = [YYCreateTools createLabel:@"已购课程" font:boldFont(14) textColor:XHBlackColor];
    [backView addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(13));
        make.centerY.equalTo(line);
    }];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.listData.count > 0) {
        return kSizeScale(30);
    }else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        return  UITableViewAutomaticDimension;
    }
    return height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MyCourceModel *couponModel = [self.listData objectAtIndex:indexPath.row];
    YY_ChapterViewController *vc = [[YY_ChapterViewController alloc] init];
    vc.courseId = couponModel.courseId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self judgeScrollDirection:scrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
