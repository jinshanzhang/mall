//
//  YYAddShopOwnerViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYAddShopOwnerViewController.h"

#import "YYBindVsellerAPI.h"

@interface YYAddShopOwnerViewController ()

@property (nonatomic, strong)UIView *backGoundView;
@property (nonatomic, copy) NSString *message;

@end

@implementation YYAddShopOwnerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_addTitle:@"我的专属二维码"];
    [self xh_popTopRootViewController:NO];
    [self xh_navBackgroundColor:HexRGB(0xf7f7f7)];
    self.view.backgroundColor = HexRGB(0xf7f7f7);
    NSInteger ret = [[self.OwnerDic objectForKey:@"ret"] intValue];
    if (!ret) {
        [self creatUI];
    }else{
        self.message = [self.OwnerDic objectForKey:@"msg"];
        [self creatFailueUI];
    }
    // Do any additional setup after loading the view.
}

-(void)creatFailueUI{
    
    UIImageView *iconIMG = [YYCreateTools createImageView:@"QRScanFail_icon" viewModel:-1];
    [self.view addSubview:iconIMG];
    
    [iconIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.mas_equalTo(75+kNavigationH);
        make.width.height.mas_equalTo(kSizeScale(48));
    }];
    
    UILabel *titleLabel = [YYCreateTools createLabel:self.message font:normalFont(16) textColor:XHBlackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.numberOfLines = 0;

    [self.view addSubview:titleLabel];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(iconIMG.mas_bottom).mas_offset(25);
        make.width.mas_equalTo(300);
        make.centerX.equalTo(self.view);
    }];
}


-(void)creatUI{
    self.backGoundView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:self.backGoundView];
    self.backGoundView.v_cornerRadius = 2;
    
    [self.backGoundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30+kNavigationH);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(kSizeScale(400));
    }];

    
    UIImageView *headIMG = [YYCreateTools createImageView:@"" viewModel:-1];
    [headIMG sd_setImageWithURL:[NSURL URLWithString:[self.OwnerDic objectForKey:@"vsellerAvatarUrl"]]];
    headIMG.v_cornerRadius = kSizeScale(50);
    [self.backGoundView addSubview:headIMG];
    
    [headIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backGoundView);
        make.top.mas_equalTo(kSizeScale(56));
        make.width.height.mas_equalTo(kSizeScale(100));
    }];
    
    UILabel *title = [YYCreateTools createLabel:[self.OwnerDic objectForKey:@"vsellerNickName"] font:boldFont(18) textColor:XHBlackColor];
    [self.backGoundView addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(headIMG.mas_bottom).mas_equalTo(16);
        make.centerX.equalTo(self.backGoundView);
    }];
    
    UILabel *loacation = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:HexRGB(0x0054FF)];
    loacation.textAlignment = NSTextAlignmentCenter;
    [self.backGoundView addSubview:loacation];
    
    NSMutableAttributedString * attriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@.%@",[[self.OwnerDic objectForKey:@"addr"] objectForKey:@"province"],[[self.OwnerDic objectForKey:@"addr"] objectForKey:@"city"]]];
    /**
     添加图片到指定的位置
     */
    NSTextAttachment *attchImage = [[NSTextAttachment alloc] init];
    // 表情图片
    attchImage.image = [UIImage imageNamed:@"qrparamLocation_icon"];
    // 设置图片大小
    attchImage.bounds = CGRectMake(0, -2, 12, 12);
    NSAttributedString *stringImage = [NSAttributedString attributedStringWithAttachment:attchImage];
    [attriStr insertAttributedString:stringImage atIndex:0];
    loacation.attributedText = attriStr;
    
    [loacation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backGoundView);
        make.top.mas_equalTo(title.mas_bottom).mas_offset(17);
        make.width.mas_equalTo(120);
    }];
    
    UIButton *btn =  [YYCreateTools createButton:@"添加城市店主" bgColor:XHRedColor textColor:XHWhiteColor];
    [self.backGoundView addSubview:btn];
    btn.v_cornerRadius = 4;
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backGoundView);
        make.top.mas_equalTo(loacation.mas_bottom).mas_offset(34);
        make.width.mas_equalTo(240);
        make.height.mas_equalTo(50);
    }];
    
    btn.actionBlock = ^(UIButton *sender) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:[self.OwnerDic objectForKey:@"qrToken"]?:@"" forKey:@"qrToken"];
        
        YYBindVsellerAPI *API = [[YYBindVsellerAPI alloc] initRequest:dict];
        [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                NSInteger ret = [[responDict objectForKey:@"ret"] intValue];;
                if (ret) {
                    [self.backGoundView removeAllSubviews];
                    self.message = [responDict objectForKey:@"msg"];
                    [self creatFailueUI];
                }else{
                    [YYCommonTools showTipMessage:@"绑定成功"];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        }];
    };
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
