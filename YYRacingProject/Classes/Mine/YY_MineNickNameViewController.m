//
//  YY_MineNickNameViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineNickNameViewController.h"

#import "MineUpdateNickInfoRequestAPI.h"
#import "YYStoreModifyNickNameInfoRequestAPI.h"
@interface YY_MineNickNameViewController ()
<UITextFieldDelegate>

@property (nonatomic, strong) UIView      *m_nickView;
@property (nonatomic, strong) UITextField *m_textField;
@property (nonatomic, strong) NSNumber    *userLevel;

@end

@implementation YY_MineNickNameViewController

#pragma mark - Setter
- (UITextField *)m_textField {
    if (!_m_textField) {
        _m_textField = [[UITextField alloc] init];
        _m_textField.delegate = self;
        _m_textField.font = midFont(15);
        _m_textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _m_textField;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    [self xh_addTitle:@"修改昵称"];
    if (kValidDictionary(self.parameter)) {
        self.userLevel = [self.parameter objectForKey:@"level"];
    }
    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
    [self xh_addNavigationItemWithTitle:@"保存 "
                                 isLeft:NO
                             clickEvent:^(UIButton *sender) {
                                 @strongify(self);
                                 [self modifyUserNickNameRequest];
                             }];
    [self xh_addNavigationItemFont:midFont(15)
                        titltColor:XHRedColor
                            isLeft:NO];
    
    [self createUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Private
- (void)createUI {
    self.m_nickView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:self.m_nickView];
    
    self.m_textField.text = ([self.userLevel integerValue] != 1 ?kStoreInfo.homepage.nickName : kUserInfo.userName);
    [self.m_nickView addSubview:self.m_textField];
    [self.m_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self.m_nickView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(10)+kNavigationH);
        make.left.right.equalTo(self.view);
        make.height.mas_equalTo(kSizeScale(50));
    }];
    
    [self.m_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(30));
        make.centerY.equalTo(self.m_nickView);
        make.right.mas_equalTo(-kSizeScale(20));
        make.height.mas_equalTo(kSizeScale(35));
    }];
}
#pragma mark - Request
- (void)modifyUserNickNameRequest {
    [self.m_textField resignFirstResponder];
    __block NSString *modifyContent = self.m_textField.text;
    if (!kValidString(modifyContent)) {
        [YYCommonTools showTipMessage:@"昵称不能为空"];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([self.userLevel integerValue] != 1) {
        [params setObject:modifyContent forKey:@"name"];
        YYStoreModifyNickNameInfoRequestAPI *nickAPI = [[YYStoreModifyNickNameInfoRequestAPI alloc] initStoreNickNameInfoRequest:params];
        [YYCenterLoading showCenterLoading];
        [nickAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [YYCenterLoading hideCenterLoading];
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                NSMutableDictionary *tempParams = [YYCommonTools objectTurnToDictionary:YES];
                [[tempParams objectForKey:@"homepage"] setObject:modifyContent forKey:@"nickName"];
                [kUserManager modifyStoreInfo:tempParams];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }
    else {
        [params setObject:modifyContent forKey:@"nickName"];
        MineUpdateNickInfoRequestAPI *nickAPI = [[MineUpdateNickInfoRequestAPI alloc] initUpdateNickInfoRequest:params];
        [YYCenterLoading showCenterLoading];
        [nickAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [YYCenterLoading hideCenterLoading];
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                NSMutableDictionary *tempParams = [YYCommonTools objectTurnToDictionary:NO];
                [tempParams setObject:modifyContent forKey:@"userName"];
                [tempParams setObject:@(0) forKey:@"useDefaultNick"];
                [kUserManager modifyUserInfo:tempParams];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }
}

#pragma mark - Event
- (void)textFieldDidChange:(UITextField *)textField {
    NSInteger kMaxLength = 12;
    NSString *toBeString = textField.text;
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage; //ios7之前使用[UITextInputMode currentInputMode].primaryLanguage
    if ([lang isEqualToString:@"zh-Hans"]) { //中文输入
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        if (!position) {// 没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
    }else {//中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
