//
//  YYScanViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYScanViewController.h"
#import <SGQRCode/SGQRCode.h>

#import "YYscanVsellerQrAPI.h"

#import "YYAddShopOwnerViewController.h"

@interface YYScanViewController ()<CLLocationManagerDelegate>

@property (nonatomic, strong) SGQRCodeScanView *scanView;
@property (nonatomic, strong) UILabel *promptLabel;
@property (nonatomic, assign) BOOL stop;
@property (nonatomic, copy)   NSString *blackBox;
@property (nonatomic, strong) CLLocationManager *locationmanager;//定位服务
@property (nonatomic, strong) NSString *retParam;
@property (nonatomic, strong) SGQRCodeObtain *obtain;

@end

@implementation YYScanViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_stop) {
        [self.obtain startRunningWithBefore:nil completion:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.scanView addTimer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.obtain stopRunning];
    [self.scanView removeTimer];
}

- (void)dealloc {
    NSLog(@"WBQRCodeVC - dealloc");
    [self removeScanningView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_popTopRootViewController:NO];
    [self xh_addTitle:@"扫一扫"];
    self.obtain = [SGQRCodeObtain QRCodeObtain];
//    __weak typeof(self) weakSelf = self;
    
//    [self xh_addNavigationItemWithTitle:@"相册" isLeft:NO clickEvent:^(UIButton *sender) {
//        [self.obtain establishAuthorizationQRCodeObtainAlbumWithController:self];
////        [self.obtain setBlockWithQRCodeObtainAlbumDidCancelImagePickerController:^(SGQRCodeObtain *obtain) {
////            [weakSelf.view addSubview:weakSelf.scanView];
////        }];
//        [self.obtain setBlockWithQRCodeObtainAlbumResult:^(SGQRCodeObtain *obtain, NSString *result) {
//            if (result == nil) {
//                [YYCommonTools showTipMessage:@"暂未识别出二维码"];
//            } else {
//                if ([result rangeOfString:@"starfish:"].location== NSNotFound) {
//                    [JCAlertView showOneButtonsWithTitle:@"提示" Message:@"二维码未识别" ButtonTitle:@"确定" ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor Click:^{
//                        [obtain startRunningWithBefore:nil completion:nil];
//
//                    } type:JCAlertViewTypeDefault];
//                }else{
//                    NSString *message = [result substringFromIndex:11];
//                    NSDictionary *dic = [NSString dictionaryWithJsonString:[message dencode:message]];
//                    //扫描出店主码信息
//                    if ([[dic objectForKey:@"linkType"] intValue] == 14) {
//                        weakSelf.retParam = result;
//                        [weakSelf getLocation];
//                    }
//                }
//            }
//        }];
//
//    }];
    [self setupQRCodeScan];
    [self.view addSubview:self.scanView];
    [self.view addSubview:self.promptLabel];
    
//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//    /* 获取设备指纹黑盒数据，请确保在应用开启时已经对SDK进行初始化，切勿在get的时候才初始化
//     * 如果此处获取到的blackBox特别长(超过400字节)，说明初始化尚未完成(一般需要1-3秒)，或者由于网络问题导致初始化失败，进入了降级处理
//     * 降级不影响正常设备信息的获取，只是会造成blackBox字段超长，且无法获取设备真实IP
//     * 降级数据平均长度在2KB以内,一般不超过3KB,数据的长度取决于采集到的设备信息的长度,无法100%确定最大长度
//     */
//    self.blackBox = manager->getDeviceInfo();
}

- (void)setupQRCodeScan {
    __weak typeof(self) weakSelf = self;
    SGQRCodeObtainConfigure *configure = [SGQRCodeObtainConfigure QRCodeObtainConfigure];
    configure.openLog = YES;
//    configure.rectOfInterest = CGRectMake(0.05, 0.2, 0.7, 0.6);
    // 这里只是提供了几种作为参考（共：13）；需什么类型添加什么类型即可
    NSArray *arr = @[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code];
    configure.metadataObjectTypes = arr;
    [self.obtain establishQRCodeObtainScanWithController:self configure:configure];
    // 二维码扫描回调方法
    [self.obtain setBlockWithQRCodeObtainScanResult:^(SGQRCodeObtain *obtain, NSString *result) {
        [weakSelf.obtain stopRunning];

        weakSelf.stop = YES;
        [weakSelf.self.obtain playSoundName:@"SGQRCode.bundle/sound.caf"];
        if ([result rangeOfString:@"starfish:"].location== NSNotFound) {
            
            [JCAlertView showOneButtonsWithTitle:@"提示" Message:@"二维码未识别" ButtonTitle:@"确定" ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor Click:^{
                [obtain startRunningWithBefore:nil completion:nil];
            
            } type:JCAlertViewTypeDefault];
        }else{
            NSString *message = [result substringFromIndex:11];
            NSDictionary *dic = [NSString dictionaryWithJsonString:[message dencode:message]];
            //扫描出店主码信息
            if ([[dic objectForKey:@"linkType"] intValue] == 14) {
                weakSelf.retParam = result;
                [weakSelf getLocation];
            }
        }
    }];
   //  二维码扫描开启方法: 需手动开启
    [self.obtain startRunningWithBefore:^{
        // 在此可添加 HUD
    } completion:^{
        // 在此可移除 HUD
    }];
    // 根据外界光线强弱值判断是否自动开启手电筒
    [self.obtain setBlockWithQRCodeObtainScanBrightness:^(SGQRCodeObtain *obtain, CGFloat brightness) {
        
    }];
}
- (SGQRCodeScanView *)scanView {
    if (!_scanView) {
        _scanView = [[SGQRCodeScanView alloc] initWithFrame:CGRectMake(0, kNavigationH, self.view.frame.size.width, self.view.frame.size.height)];
        // 静态库加载 bundle 里面的资源使用 SGQRCode.bundle/QRCodeScanLineGrid
        // 动态库加载直接使用 QRCodeScanLineGrid
        _scanView.backgroundColor = XHClearColor;
        _scanView.scanImageName = @"SGQRCode.bundle/QRCodeScanLineGrid";
        _scanView.scanAnimationStyle = ScanAnimationStyleDefault;
        _scanView.cornerLocation = CornerLoactionOutside;
        _scanView.cornerColor = XHWhiteColor;
    }
    return _scanView;
}


-(void)getLocation
{
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationmanager = [[CLLocationManager alloc]init];
        [_locationmanager requestAlwaysAuthorization];
        [_locationmanager requestWhenInUseAuthorization];
        _locationmanager.delegate = self;
        //设置寻址精度
        _locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationmanager.distanceFilter = 5.0;
        [_locationmanager startUpdatingLocation];
    }
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    if (error.code == kCLErrorDenied) {
        // 提示用户出错
        [JCAlertView showTwoButtonsWithTitle:@"定位服务已关闭" Message:@"您需要打开定位权限，才能获取您的 专属二维码。请到设置-隐私-定位服 务中开启【蜀黍之家】定位服务。" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"去设置" Click:^{
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        } type:JCAlertViewTypeDefault];
    }
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [self.locationmanager stopUpdatingHeading];
    _locationmanager.delegate = nil;
    //旧址
    CLLocation *currentLocation = [locations lastObject];
    NSLog(@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);

    NSString *doc = [NSString stringWithFormat:@"%f,%f",currentLocation.coordinate.longitude,currentLocation.coordinate.latitude];
    [self ScanVselleQR:[doc encode:doc]];
}

#pragma mark Netdata
-(void)ScanVselleQR:(NSString *)doc{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:doc?:@"" forKey:@"loc"];
    [dict setObject:self.blackBox?:@"" forKey:@"blackBox"];
    [dict setObject:self.retParam forKey:@"retParam"];
    
    YYscanVsellerQrAPI *API = [[YYscanVsellerQrAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.obtain startRunningWithBefore:nil completion:nil];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self.obtain stopRunning];
            YYAddShopOwnerViewController *vc = [YYAddShopOwnerViewController new];
            vc.OwnerDic = responDict;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

- (void)removeScanningView {
    [self.scanView removeTimer];
    [self.scanView removeFromSuperview];
    self.scanView = nil;
}

- (UILabel *)promptLabel {
    if (!_promptLabel) {
        _promptLabel = [[UILabel alloc] init];
        _promptLabel.backgroundColor = [UIColor clearColor];
        CGFloat promptLabelX = 0;
        CGFloat promptLabelY = 0.73 * self.view.frame.size.height;
        CGFloat promptLabelW = self.view.frame.size.width;
        CGFloat promptLabelH = 25;
        _promptLabel.frame = CGRectMake(promptLabelX, promptLabelY, promptLabelW, promptLabelH);
        _promptLabel.textAlignment = NSTextAlignmentCenter;
        _promptLabel.font = [UIFont boldSystemFontOfSize:13.0];
        _promptLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
        _promptLabel.text = @"将二维码/条码放入框内, 即可自动扫描";
    }
    return _promptLabel;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
