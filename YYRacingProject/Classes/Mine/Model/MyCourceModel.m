//
//  MyCourceModel.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "MyCourceModel.h"

@implementation MyCourceModel
+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"coverImage":[YYImageInfoModel class],
             @"qunImage":[HomeGoodImageInfoModel class],
             @"lessons":[LessonDetailModel class],
             };
}
@end
