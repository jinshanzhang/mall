//
//  AfterSaleTypeConfigs.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AfterSaleGoodStatusModel.h"

@interface AfterSaleTypeConfigsModel : NSObject

@property  (nonatomic, assign) int type;

@property  (nonatomic, copy)   NSString  *describe;

@property (nonatomic, strong)  NSArray *goodStatus;



@end
