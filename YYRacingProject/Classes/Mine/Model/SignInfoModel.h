//
//  SignInfoModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignInfoModel : NSObject

@property (nonatomic, copy) NSString *idcardNo;

@property (nonatomic, copy) NSString *accountNo;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *bankName;

@property (nonatomic, copy) NSString *provice;
@property (nonatomic, copy) NSString *city;

@property (nonatomic, assign) int  bankId;

@property (nonatomic, assign) int  proviceId;
@property (nonatomic, assign) int  cityId;
@property (nonatomic, assign) int  contractFlag;

@end
