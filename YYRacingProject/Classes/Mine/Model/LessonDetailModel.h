//
//  LessonDetailModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYImageInfoModel.h"
#import "HomeGoodInfoModel.h"
@class MyCourceModel;


@interface LessonDetailModel : NSObject

@property (nonatomic, copy) NSString  *title;

@property (nonatomic, strong) NSNumber *lessonId;//课ID

@property (nonatomic, strong) YYImageInfoModel *coverImage;//首图

@property (nonatomic, strong) NSNumber *contentType;//课内容 1 音频。2 视频 3 图文

@property (nonatomic, strong) NSNumber *resourceId;//资源ID

@property (nonatomic, strong) NSNumber *tryoutFlag;//是否试用

@property (nonatomic, strong) NSNumber *playFlag;//是否允许播放

@property (nonatomic, copy)   NSString *duration;  //时长

@property (nonatomic, copy)   NSString *size;  //大小

@property (nonatomic, strong) NSMutableArray *detailImages;

@property (nonatomic, strong) MyCourceModel *course;

@end


