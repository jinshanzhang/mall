//
//  MyCourceModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YYImageInfoModel.h"
#import "LessonDetailModel.h"
#import "HomeGoodInfoModel.h"

@interface MyCourceModel : NSObject

@property (nonatomic, copy) NSString *courseId;
@property (nonatomic, copy) NSString *title;
//课程状态：1：已完结；0：在更新
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSNumber *totalLessonCount;
@property (nonatomic, strong) NSNumber *currentLessonCount;
@property (nonatomic, strong) NSNumber *sellCount;
@property (nonatomic, strong) YYImageInfoModel *coverImage;
@property (nonatomic, strong) HomeGoodImageInfoModel *qunImage;
@property (nonatomic, strong) NSMutableArray *lessons;
@property (nonatomic, copy)   NSString *itemUrl;
@property (nonatomic, strong) NSNumber *appItemId;

@property (nonatomic, copy)   NSString *lessonCountTitle;
@property (nonatomic, copy)   NSString *sellCountTitle;

@end
