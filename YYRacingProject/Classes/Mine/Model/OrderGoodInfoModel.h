//
//  OrderGoodInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface OrderGoodInfoModel : NSObject

@property (nonatomic, strong) NSNumber *skuId;
@property (nonatomic, strong) NSNumber *skuCnt;
@property (nonatomic, strong) NSNumber *spuId;
@property (nonatomic, strong) NSNumber *orderId;
@property (nonatomic, strong) NSNumber *supId;

@property (nonatomic, copy) NSString *itemUri;
@property (nonatomic, copy) NSString *skuTitle;
@property (nonatomic, copy) NSString *skuProp;
@property (nonatomic, copy) NSString *skuCover;
@property (nonatomic, copy) NSString *commission; //佣金
@property (nonatomic, strong) NSNumber *source;

@property (nonatomic, assign) BOOL   showSale;  // 显示售后按钮
@property (nonatomic, assign) BOOL   isDetail;  // 是否详情页
@property (nonatomic, assign) BOOL   showEarn;  // 是否展示佣金
@property (nonatomic, assign) BOOL   earnFlag;  // NO赚， YES省

@property (nonatomic, assign) NSInteger itemType;  //1是大礼包商品
@property (nonatomic, assign) NSInteger operation; //售后按钮状态 0 申请退款 1 售后中 2 售后失败 3 取消售后  4 售后已取消 5售后成功

@property (nonatomic, strong) NSDictionary *price;
@property (nonatomic, strong) NSString  *priceFormat;

@end
