//
//  OrderDetailInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderDetailInfoModel.h"

@implementation OrderDetailInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"addr":[OrderDetailAddressInfoModel class],
             @"orderInfo": [OrderListInfoModel class],
             @"expressInfo": [OrderDetailExpressInfoModel class],
             @"userInfo": [OrderDetailUserInfoModel class],
             };
}

@end

@implementation OrderDetailAddressInfoModel

@end

@implementation OrderDetailExpressInfoModel

@end

@implementation OrderDetailUserInfoModel

@end
