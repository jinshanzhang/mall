//  OrderListInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderListInfoModel : NSObject

@property (nonatomic, strong) NSString *countDownIdentifier;

@property (nonatomic, strong) NSNumber *orderId;
@property (nonatomic, strong) NSNumber *shopId;
@property (nonatomic, strong) NSNumber *orderStatus;
@property (nonatomic, strong) NSNumber *operation;
@property (nonatomic, strong) NSNumber *orderTime;

@property (nonatomic, strong) NSNumber *systemTime;
@property (nonatomic, strong) NSNumber *payDeadlineTime;  //未付款截止时间
@property (nonatomic, strong) NSNumber *payTime;          //付款完成时间
@property (nonatomic, copy)   NSString *shipDeadlineTime; //发货截止时间
@property (nonatomic, assign) NSInteger diffValue;

@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *shopName;
@property (nonatomic, copy) NSString *sellerAddr;        //商家发货地
@property (nonatomic, assign) BOOL   earnFlag;           //NO赚， YES省
@property (nonatomic, assign) BOOL   isShowMark;         //是否显示标记
@property (nonatomic, strong) NSNumber *source;          //页面来源

@property (nonatomic, strong) NSMutableArray *summaryList;
@property (nonatomic, strong) NSMutableArray *skuList;
@property (nonatomic, strong) NSMutableArray *sumList;

@end

@interface OrderPriceInfoModel : NSObject

@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, copy) NSString *priceTag;

@property (nonatomic, assign) int bold;
@property (nonatomic, copy)  NSString *color;
@property (nonatomic, copy)  NSString *value;

@end

@interface OrderSumInfoModel : NSObject

@property (nonatomic, strong) NSString *fee;
@property (nonatomic, copy) NSString *homePageLogo;
@property (nonatomic, assign) int homePageRole;
@property (nonatomic, copy) NSString *homePageName;
@property (nonatomic, assign) int earnFlag;


@end
