//
//  OrderAfterSaleListInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderAfterSaleListInfoModel : NSObject

@property (nonatomic, assign) int afterSaleParentId;
@property (nonatomic, assign) long orderId;
@property (nonatomic, assign) int skuId;
@property (nonatomic, assign) int afterSaleParentStatus;
@property (nonatomic, assign) int afterSaleParentType;
@property (nonatomic, assign) long createdTime;
@property (nonatomic, assign) int skuCnt;


@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *number;
@property (nonatomic, copy) NSString *afterSaleParentStatusName;
@property (nonatomic, copy) NSString *afterSaleParentTypeName;
@property (nonatomic, copy) NSString *skuTitle;
@property (nonatomic, copy) NSString *skuProp;
@property (nonatomic, copy) NSString *skuPrice;

@end
