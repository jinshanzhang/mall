//
//  OrderLogisticsInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderLogisticsInfoModel.h"

@implementation OrderLogisticsInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"skuList":[OrderGoodInfoModel class],
             @"logisticsList":[OrderLogisticsListModel class]
             };
}

@end

@implementation OrderLogisticsListModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"expressInfoList":[OrderLogisticsPointInfoModel class],
             @"carrier":[OrderLogisticCarrierInfoModel class]
             };
}

@end


@implementation OrderLogisticCarrierInfoModel

@end


@implementation OrderLogisticsPointInfoModel

@end

