//
//  OrderLogisticsInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderGoodInfoModel.h"
@class OrderLogisticsListModel,OrderLogisticCarrierInfoModel, OrderLogisticsPointInfoModel;

@interface OrderLogisticsInfoModel : NSObject

@property (nonatomic, strong) NSMutableArray *skuList;
@property (nonatomic, strong) NSMutableArray *logisticsList;

@end

@interface OrderLogisticsListModel : NSObject

@property (nonatomic, strong) OrderLogisticCarrierInfoModel *carrier;
@property (nonatomic, strong) NSMutableArray *expressInfoList;

@end

@interface OrderLogisticCarrierInfoModel : NSObject

@property (nonatomic, copy) NSString *carrierName;
@property (nonatomic, copy) NSString *trackNo;

@end

@interface OrderLogisticsPointInfoModel : NSObject

@property (nonatomic, assign) YYLogisticsDrawType drawType;
@property (nonatomic, copy) NSString *momentInfo;
@property (nonatomic, strong) NSNumber *momentTime;

@end

