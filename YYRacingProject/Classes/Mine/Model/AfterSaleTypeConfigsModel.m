//
//  AfterSaleTypeConfigs.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSaleTypeConfigsModel.h"

@implementation AfterSaleTypeConfigsModel
+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"goodStatus":[AfterSaleGoodStatusModel class],
             };
}


@end
