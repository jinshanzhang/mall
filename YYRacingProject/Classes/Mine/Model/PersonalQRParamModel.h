//
//  PersonalQRParamModel.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/16.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PersonalQRParamModel : NSObject

@property (nonatomic, copy)   NSString *avatarUrl;  //时长
@property (nonatomic, strong)   NSNumber  *qrExpireAt;  //时长
@property (nonatomic, copy)   NSString *retParam;  //时长
@property (nonatomic, strong) NSDictionary *addr;  //时长

@end

NS_ASSUME_NONNULL_END
