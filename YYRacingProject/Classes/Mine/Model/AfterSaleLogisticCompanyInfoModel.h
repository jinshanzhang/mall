//
//  AfterSaleLogisticCompanyInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AfterSaleLogisticCompanyInfoModel : NSObject

@property (nonatomic, strong) NSNumber *companyId;
@property (nonatomic, strong) NSString *companyName;

@end
