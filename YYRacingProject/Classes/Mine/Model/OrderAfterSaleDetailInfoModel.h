//
//  OrderAfterSaleDetailInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OrderAfterSaleDetailMessageInfoModel, rderAfterSaleMessageContentInfoModel;
@interface OrderAfterSaleDetailInfoModel : NSObject

@property (nonatomic, assign) NSInteger button;
@property (nonatomic, assign) long      orderId;
@property (nonatomic, assign) NSInteger skuId;
@property (nonatomic, assign) NSInteger skuCnt;

@property (nonatomic, copy) NSString  *skuTitle;
@property (nonatomic, copy) NSString  *skuProp;
@property (nonatomic, copy) NSString  *skuPrice;
@property (nonatomic, copy) NSString  *imageUrl;

@property (nonatomic, strong) NSMutableArray *messages;

@end


@interface OrderAfterSaleDetailMessageInfoModel : NSObject

@property (nonatomic, assign) BOOL   isFirst;

@property (nonatomic, assign) NSInteger  afterSaleMessageId;
@property (nonatomic, assign) NSInteger  messageType;

@property (nonatomic, assign) long    modifiedTime;
@property (nonatomic, assign) long    createdTime;

@property (nonatomic, copy) NSString  *title;
@property (nonatomic, copy) NSString  *number;
@property (nonatomic, copy) NSString  *content;
@property (nonatomic, copy) NSString  *remainTime;

@property (nonatomic, strong) NSMutableArray *urls;
@property (nonatomic, strong) NSMutableArray *contentList;

@end


@interface OrderAfterSaleMessageContentInfoModel : NSObject

@property (nonatomic, copy) NSString  *title;
@property (nonatomic, copy) NSString  *describe;

@end



