//
//  OrderListInfoModel.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderListInfoModel.h"

#import "OrderGoodInfoModel.h"
@implementation OrderListInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"summaryList":[OrderPriceInfoModel class],
             @"skuList": [OrderGoodInfoModel class],
             @"sumList":[OrderSumInfoModel class]
             };
}
@end

@implementation OrderPriceInfoModel
@end
@implementation OrderSumInfoModel
@end

