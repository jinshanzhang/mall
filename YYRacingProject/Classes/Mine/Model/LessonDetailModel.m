//
//  LessonDetailModel.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/27.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "LessonDetailModel.h"

@implementation LessonDetailModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"coverImage":[YYImageInfoModel class],
             @"course":[MyCourceModel class],
             @"detailImages":[HomeGoodImageInfoModel class]
             };
}

@end
