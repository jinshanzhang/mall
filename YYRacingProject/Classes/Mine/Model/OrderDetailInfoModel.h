//
//  OrderDetailInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderListInfoModel.h"

@class OrderDetailAddressInfoModel,
OrderDetailExpressInfoModel,
OrderDetailUserInfoModel;
@interface OrderDetailInfoModel : NSObject

@property (nonatomic, strong) NSNumber *systemTime;
@property (nonatomic, strong) OrderDetailAddressInfoModel *addr;
@property (nonatomic, strong) OrderListInfoModel          *orderInfo;
@property (nonatomic, strong) OrderDetailExpressInfoModel *expressInfo;
@property (nonatomic, strong) OrderDetailUserInfoModel    *userInfo;

@property (nonatomic, strong) NSNumber *payTime;
@property (nonatomic, assign) NSInteger diffValue;
@property (nonatomic, assign) BOOL  isShowDeliveryInfo;   //是否显示配送信息

@end

@interface OrderDetailAddressInfoModel : NSObject

@property (nonatomic, strong) NSNumber  *addrId;
@property (nonatomic, copy)   NSString  *receiverName;
@property (nonatomic, copy)   NSString  *receiverPhone;
@property (nonatomic, copy)   NSString  *address;

@end

@interface OrderDetailExpressInfoModel : NSObject

@property (nonatomic, copy)   NSString  *momentInfo;
@property (nonatomic, strong) NSNumber  *momentTime;

@end

@interface OrderDetailUserInfoModel : NSObject

@property (nonatomic, copy)   NSString  *avatarUrl;
@property (nonatomic, copy)   NSString  *nickName;

@end



