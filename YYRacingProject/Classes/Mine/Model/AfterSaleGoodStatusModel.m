//
//  AfterSaleGoodStatusModel.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSaleGoodStatusModel.h"

@implementation AfterSaleGoodStatusModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"reasons":[AfterSaleReasonsModel class],
             };
}


@end
