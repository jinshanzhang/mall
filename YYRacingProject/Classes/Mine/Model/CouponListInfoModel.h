//
//  CouponListInfoModel.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeBannerInfoModel.h"

@interface CouponListInfoModel : NSObject

@property (nonatomic, strong) NSNumber *couponId;
@property (nonatomic, strong) NSString *couponKey;
@property (nonatomic, strong) NSNumber *tabType;
@property (nonatomic, copy) NSString  *denomination;  // 面额
@property (nonatomic, strong) NSNumber *type;         //券类型
@property (nonatomic, copy) NSString  *denominationCondition;// 面额条件
@property (nonatomic, copy) NSString  *couponName;
@property (nonatomic, copy) NSString  *effectTime;    // 有效期
@property (nonatomic, copy) NSString  *scopeTitle;    //使用范围简述
@property (nonatomic, copy) NSString  *scopeDetail;   // 使用范围
@property (nonatomic, assign) BOOL     shareAble;     //是否可分享

@property (nonatomic, strong) HomeBannerInfoModel *target;

@property (nonatomic, copy) NSString  *sharerAvatarUrl;  //头像链接
@property (nonatomic, copy) NSString  *shareDesc;        //分享描述
@property (nonatomic, copy) NSString  *stampImg;         //印戳图
@property (nonatomic, assign) NSInteger status;          //券状态
@property (nonatomic, assign) NSInteger  userCouponStatus; //用户券状态
@property (nonatomic, assign) NSInteger selected;        //券选中状态
@property (nonatomic, copy) NSString  *expireSoonImg;    //快过期图标
@property (nonatomic, assign) NSInteger roleFlag;        //角色券 0-全部可用，1-仅店主可领，2-仅粉丝可领
@property (nonatomic, assign) NSInteger limitStatus;     //限制状态 0-领取，1-继续领取，2-已用完，3-已领券
@property (nonatomic, assign) NSInteger holdNum;

@property (assign, nonatomic) BOOL    launch;//是否展开

@property (strong, nonatomic) NSDictionary *link;

@end
