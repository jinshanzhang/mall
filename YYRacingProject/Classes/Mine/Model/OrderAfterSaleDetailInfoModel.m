//
//  OrderAfterSaleDetailInfoModel.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleDetailInfoModel.h"

@implementation OrderAfterSaleDetailInfoModel
+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"messages":[OrderAfterSaleDetailMessageInfoModel class]
             };
}

@end

@implementation OrderAfterSaleDetailMessageInfoModel

+ (NSDictionary *)modelContainerPropertyGenericClass
{
    return @{
             @"contentList":[OrderAfterSaleMessageContentInfoModel class]
             };
}

@end

@implementation OrderAfterSaleMessageContentInfoModel

@end

