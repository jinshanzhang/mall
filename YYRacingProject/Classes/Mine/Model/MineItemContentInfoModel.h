//
//  MineItemContentInfoModel.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineItemContentInfoModel : NSObject

@property (nonatomic, assign) NSInteger type; // type = (0余额，1优惠券，2我的课程，3收获地址，4联系客服，5常见问题)
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *imageUrl;

@end

NS_ASSUME_NONNULL_END
