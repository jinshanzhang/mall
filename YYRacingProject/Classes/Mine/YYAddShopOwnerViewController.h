//
//  YYAddShopOwnerViewController.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/17.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YYAddShopOwnerViewController : YYBaseViewController

@property (nonatomic, strong) NSDictionary *OwnerDic;

@end

NS_ASSUME_NONNULL_END
