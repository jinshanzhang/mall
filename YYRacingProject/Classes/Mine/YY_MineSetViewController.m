//
//  YY_MineSetViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineSetViewController.h"
#import "YY_MineSignViewController.h"
#import "YYScanViewController.h"
#import "YY_MinePersonalCodeViewController.h"

#import "YYAudioManagerClass.h"

#import "MineSettingInfoCell.h"
#import "MineSetButtonInfoCell.h"

#import "YYUploadPhotoRequestAPI.h"
#import "YYWxAuthAppRequestAPI.h"
#import "YYBindWechatRequestAPI.h"
#import "LoginUserQuitInfoRequestAPI.h"
#import "MineUpdateAvatarInfoRequestAPI.h"
#import "YY_WKWebViewController.h"

@interface YY_MineSetViewController ()
<UITableViewDelegate,
UITableViewDataSource,
TZImagePickerControllerDelegate>

@property (nonatomic, assign) CGFloat         cacheSize;
@property (nonatomic, strong) NSMutableArray *cellTitles;

@property (nonatomic, copy) NSString *wxOpenId;
@property (nonatomic, copy) NSString *wxUniqueId;
@property (nonatomic, copy) NSString *iconUrl;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *oldPhone;

@end

@implementation YY_MineSetViewController


#pragma mark - Life cycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.m_tableView) {
        [kWholeConfig gainUserCenterInfo:^(BOOL isSucess) {
           // 这里9.0 设备这里不能增加动画，否则导致图片上传失败闪退。
        }isLoading:NO];
    }
    [self.m_tableView reloadData];
}

- (void)popToRootVC {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_addTitle:@"设置"];
    [self xh_popTopRootViewController:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popToRootVC) name:@"KPopToRootVCNoty" object:nil];
    Class currentCls = [MineSettingInfoCell class];
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    registerClass(self.m_tableView, currentCls);
    currentCls = [MineSetButtonInfoCell class];
    registerClass(self.m_tableView, currentCls);
    [self.view addSubview:self.m_tableView];

    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    
    if (kIsMember){
        self.cellTitles = [NSMutableArray arrayWithObjects:@[@"头像",@"昵称",@"绑定微信"],@[@"扫一扫"],@[@"清除缓存",@"蜀黍之家"],@[],nil];
    }else if (kIsFan||kUserInfo.cityPartnerFlag == 4){
        self.cellTitles = [NSMutableArray arrayWithObjects:@[@"头像",@"昵称",@"绑定微信"],@[@"扫一扫"],@[@"清除缓存",@"蜀黍之家"],@[],nil];
    }else{
        NSString *title;
        if(kUserInfo.cityPartnerFlag == 1) {
            title = @"我的城市店主";
        }
        else if (kUserInfo.cityPartnerFlag == 2) {
            title = @"我的城市合伙人";
        }
        else if (kUserInfo.cityPartnerFlag==3) {
            title = @"我的专属二维码";
        }
        self.cellTitles = [NSMutableArray arrayWithObjects:@[@"头像",@"昵称",@"绑定微信",kUserInfo.payAccountPasswordFlag==YES?@"修改支付密码":@"设置支付密码"],@[@"扫一扫",title],@[@"清除缓存",@"蜀黍之家"],@[], nil];
    }
    self.cacheSize = [kWholeConfig calculateBufferSize];
    [self.m_tableView reloadData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Request
/**更新用户头像**/
- (void)updateUserHeadRequest:(NSMutableDictionary *)params {
    MineUpdateAvatarInfoRequestAPI *updateAPI = [[MineUpdateAvatarInfoRequestAPI alloc] initUpdateHeaderInfoRequest:params];
    [updateAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSMutableDictionary *tempParams = [YYCommonTools objectTurnToDictionary:NO];
            [tempParams setObject:[params objectForKey:@"imageUrl"] forKey:@"avatarUrl"];
            [tempParams setObject:@(0) forKey:@"useDefaultAvatar"];
            [kUserManager modifyUserInfo:tempParams];
            [self.m_tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)bindWX{
    if (!self.wxUniqueId||!self.wxOpenId) {
        [YYCommonTools  showTipMessage:@"系统繁忙,请稍后再试."];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.wxOpenId forKey:@"openId"];
    [dict setObject:self.wxUniqueId forKey:@"unionId"];
    [dict setObject:self.userName forKey:@"userName"];
    [dict setObject:self.iconUrl forKey:@"avatarUrl"];
    [dict setObject:@"" forKey:@"country"];
    [dict setObject:@"" forKey:@"city"];
    [dict setObject:@"" forKey:@"province"];
    if ([self.gender isEqualToString:@"m"]) {
        self.gender = @"1";
    }
    else{
        self.gender = @"2";
    }
    [dict setObject:self.gender?:@"0" forKey:@"sex"];
    YYWxAuthAppRequestAPI *loginAPI = [[YYWxAuthAppRequestAPI alloc] initWithAuthAppRequest:dict];
    [YYCenterLoading showCenterLoading:YES];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self bindWXrequest];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

-(void)bindWXrequest{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.wxOpenId forKey:@"openId"];
    [dict setObject:self.wxUniqueId forKey:@"unionId"];
    [dict setObject:self.oldPhone?:@"" forKey:@"oldPhone"];
    
    YYBindWechatRequestAPI *API = [[YYBindWechatRequestAPI alloc] initRequest:dict];
    [YYCenterLoading showCenterLoading:YES];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request)
     {
         NSDictionary *responDict = request.responseJSONObject;
         [YYCenterLoading hideCenterLoading];
         self.oldPhone = [responDict objectForKey:@"oldPhone"];
         if (self.oldPhone) {
             [JCAlertView showTwoButtonsWithTitle:@"提示" Message:[NSString stringWithFormat:@"该微信已绑定账户%@,确认与原账户解除绑定,与当前账户绑定吗",self.oldPhone] ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
                 self.oldPhone = @"";
             } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlackColor ButtonTitle:@"确认" Click:^{
                 [self bindWXrequest];
             } type:JCAlertViewTypeDefault];
         } else {
             NSMutableDictionary *params = [YYCommonTools objectTurnToDictionary:NO];
             [params setObject:self.userName forKey:@"wxName"];
             [params setObject:@(1) forKey:@"wxBindingFlag"];
             [kUserManager modifyUserInfo:params];
             [YYCommonTools  showTipMessage:@"绑定成功"];
             [self.m_tableView reloadData];
         }
     } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
         
     }];
}

/**用户退出**/
- (void)userQuitLoginRequest {
    LoginUserQuitInfoRequestAPI *quitAPI = [[LoginUserQuitInfoRequestAPI alloc] initWithQuitLoginRequest:[[NSDictionary dictionary] mutableCopy]];
    [YYCenterLoading showCenterLoading];
    [quitAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [kUserManager clearUserData];
            [[YYAudioManagerClass shared] setReleaseAudio];
            [[HDClient sharedClient] logout:YES];
            [RJBadgeController clearBadgeForKeyPath:kShippingRedCountPath];
            [RJBadgeController clearBadgeForKeyPath:kShippingDetailRedCountPath];
            [RJBadgeController clearBadgeForKeyPath:kShippingSubDetailRedCountPath];
            kPostNotification(LoginAndQuitSuccessNotification, nil);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark - Private method
/**cell 点击操作**/
- (void)cellClickAtIndexPath:(NSIndexPath *)index {
    NSInteger row = index.row;
    NSInteger section = index.section;
    if (section == 0) {
        if (row == 0) {
            [self modifyUserHeaderOperator];
        }
        else if (row == 1) {
            [YYCommonTools skipNickNameSet:self
                                    params:@{@"level": @(1)}];
        }
        else if (row == 2) {
            [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
                if (error) {
                } else {
                    UMSocialUserInfoResponse *resp = result;
                    NSLog(@"%@",resp);
                    self.wxUniqueId = resp.unionId;
                    self.wxOpenId = resp.openid;
                    self.userName = resp.name;
                    self.iconUrl =resp.iconurl;
                    self.gender = resp.gender;
                    [self bindWX];
                }
            }];
        }else if ((row == 3&&!kIsFan)||(row==3&&kIsMember)){
            YY_MineSignViewController *vc = [YY_MineSignViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (row ==4&&!kIsFan){
            [YYCommonTools skipPaymentPassword:self];
        }
    }else if (section ==1 ){
        if (row == 0) {
            YYScanViewController *vc = [YYScanViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            if(kUserInfo.cityPartnerFlag == 1) {
                //        title = @"我的城市店主";
                YY_WKWebViewController *vc = [[YY_WKWebViewController alloc] init];
                vc.parameter = @{@"url":[NSString stringWithFormat:@"%@/#/citizen-shopkeeper",kEnvConfig.webDomin]
                                       };
                [self.navigationController pushViewController:vc animated:YES];
            }if (kUserInfo.cityPartnerFlag==2) {
            }if (kUserInfo.cityPartnerFlag==3) {
                YY_MinePersonalCodeViewController *vc = [YY_MinePersonalCodeViewController new];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }else if (section == 2){
        if (row == 0) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"清除缓存" message:[NSString stringWithFormat:@"是否要清除所有缓存(%.1fMB)",self.cacheSize] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                [YYCommonTools showTipMessage:@"清除成功"];
                [kWholeConfig clearBufferSize];
                self.cacheSize = [kWholeConfig calculateBufferSize];
                [self.m_tableView reloadData];
            }];
        }];
        [alert addAction:defaultAction];
        [alert addAction:cancelAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
      }
    }
        [self.m_tableView reloadData];
}

/**上传头像操作**/
- (void)modifyUserHeaderOperator {
    @weakify(self);
    [yyImagePickerManagerClass openImagePickerCtrl:1
                                       columnCount:4
                                       isAllowCrop:YES
                                          delegate:self
                                          viewCtrl:self
                                           myBlock:^(NSArray *array) {
                                               @strongify(self);
                                               NSMutableArray *selectArrs = [array mutableCopy];
                                               NSMutableDictionary *params = [NSMutableDictionary dictionary];
                                               [params setObject:selectArrs forKey:@"photos"];
                                               [YYCenterLoading showCenterLoading];
                                               YYUploadPhotoRequestAPI *uploadAPI = [[YYUploadPhotoRequestAPI alloc] initUploadPhotoInfoRequest:params];
                                               [uploadAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
                                                   NSDictionary *responDict = request.responseJSONObject;
                                                   if (kValidDictionary(responDict)) {
                                                       NSArray *imgs = [responDict objectForKey:@"imgs"];
                                                       if (kValidArray(imgs)) {
                                                           NSMutableDictionary *paramDicts = [[imgs firstObject] mutableCopy];
                                                           [self updateUserHeadRequest:paramDicts];
                                                       }
                                                   }
                                               } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
                                                   
                                               }];
                                           }];
}

#pragma mark -UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellTitles.count+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSMutableArray *arr;
    if (section < self.cellTitles.count-1) {
        arr = self.cellTitles[section];
    }
    return (section == self.cellTitles.count-1? 1 : arr.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSString *cellContent = @"";
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    Class currentCls = [MineSettingInfoCell class];
    MineSettingInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    if (section == 0) {
        cell.isShowHead = (row == 0 ? YES : NO);
        cell.setTitle = [self.cellTitles[section] objectAtIndex:row];
        if (row == 0) {
            cellContent = kUserInfo.avatarUrl;
        }
        else if (row == 1) {
            cellContent = kUserInfo.userName;
        }
        else if (row == 2) {
            if (kValidString(kUserInfo.wxName)) {
                cell.isHideArrowImg = NO;
                cellContent = kUserInfo.wxName;
            }
            else {
                cell.isHideArrowImg = NO;
                cellContent = @"未绑定";
            }
        }
        else if ((row == 3&&!kIsFan)||(row==3&&kIsMember)){
            if (kUserInfo.contractFlag == 1) {
                cellContent = @"未签约";
            }else{
                cellContent = @"已签约";
            }
        }else if (row == 4 && !kIsFan){
            
        }
        else if ((row == 5 && !kIsFan)||(kIsFan&&row==3)) {
            cellContent = [NSString stringWithFormat:@"%.1fMB",self.cacheSize];
        }
        else {
            if (row == 6) {
                cellContent = kAPPVersion;
            }
        }
        cell.setContent = cellContent;
        cell.cellClickBlock = ^(MineSettingInfoCell *setCell) {
            @strongify(self);
            NSIndexPath *index = [self.m_tableView indexPathForCell:setCell];
            if (index.row == 2 && kValidString(kUserInfo.wxName)) {
                return ;
            }
            [self cellClickAtIndexPath:index];
        };
        return cell;
        
    }else if (section == 1){
        cell.setTitle = [self.cellTitles[section] objectAtIndex:row];
        if (kUserInfo.cityPartnerFlag !=1 ) {
            if (indexPath.row == 0) {
                cell.hidden  = YES;
            }
        }
        if (indexPath.row == 1) {
            if (kUserInfo.cityPartnerFlag ==2) {
                cell.setContent =[kUserInfo.cityPartner objectForKey:@"nickName"];
                cell.isHideArrowImg = YES;
            }
        }
        cell.cellClickBlock = ^(MineSettingInfoCell *setCell) {
            @strongify(self);
            NSIndexPath *index = [self.m_tableView indexPathForCell:setCell];
            [self cellClickAtIndexPath:index];
        };
        return cell;
    }
    else if (section == 2){
        cell.isHideArrowImg = YES;
        cell.setTitle = [self.cellTitles[section] objectAtIndex:row];
        if (row == 0) {
            cellContent = [NSString stringWithFormat:@"%.1fMB",self.cacheSize];
        }else{
            cellContent = kAPPVersion;
        }
        cell.setContent = cellContent;
        cell.cellClickBlock = ^(MineSettingInfoCell *setCell) {
            @strongify(self);
            NSIndexPath *index = [self.m_tableView indexPathForCell:setCell];
            [self cellClickAtIndexPath:index];
        };
        return cell;
    }
    else {
        currentCls = [MineSetButtonInfoCell class];
        MineSetButtonInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.quitClickBlock = ^{
            @strongify(self);
            [self userQuitLoginRequest];
        };
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [YYCreateTools createView:XHLightColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(8);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1&&indexPath.row ==0) {
        if (kUserInfo.cityPartnerFlag !=1) {
            return 0;
        }
    }
    return kSizeScale(48);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

