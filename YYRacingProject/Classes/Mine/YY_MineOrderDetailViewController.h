//
//  YY_MineOrderDetailViewController.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseViewController.h"

@interface YY_MineOrderDetailViewController : YYBaseViewController

@property (nonatomic, copy) void (^confirmReceivingBlock)(NSNumber *orderStatus, NSNumber *operators, BOOL isReceive);

@end
