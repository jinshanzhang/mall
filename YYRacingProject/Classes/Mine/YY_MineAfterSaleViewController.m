//
//  YY_MineAfterSaleViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineAfterSaleViewController.h"

#import "OrderListGoodInfoCell.h"
#import "OrderGoodInfoModel.h"
#import "OrderAfterSaleListInfoCell.h"
#import "OrderAfterSaleListInfoModel.h"

#import "OrderAfterSaleListHeaderView.h"
#import "MineAfterSaleListInfoRequestAPI.h"
@interface YY_MineAfterSaleViewController ()

@property (nonatomic, strong) NSNumber *inType; //入口类型
@property (nonatomic, strong) NSString *cursor;
@property (nonatomic, strong) NSNumber *hasMore;

@end

@implementation YY_MineAfterSaleViewController

#pragma mark -Life cycle

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.isAddReachBottomView = YES;
        self.emptyTitle = @"您暂时还没有任何售后呢~";
        self.NoDataType = YYEmptyViewOrderNoDataType;
        self.tableStyle = UITableViewStylePlain;
        self.footerBgStyleColor = XHLightColor;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cursor = @"";
    [self xh_addTitle:@"我的售后"];
    [self xh_popTopRootViewController:NO];
    
    self.m_tableView.estimatedRowHeight = 44.0;
    
    Class current = [OrderAfterSaleListInfoCell class];
    registerClass(self.m_tableView, current);
    
    self.m_tableView.backgroundColor = XHLightColor;
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"type"]) {
            self.inType = [self.parameter objectForKey:@"type"];
        }
    }
    [self loadNewer];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Base
- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.cursor forKey:@"cursor"];
    [params setObject:self.inType forKey:@"type"];
    MineAfterSaleListInfoRequestAPI *afterListAPI = [[MineAfterSaleListInfoRequestAPI alloc] initOrderAfterSaleInfoRequest:params];
    return afterListAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.cursor = [responseDic objectForKey:@"cursor"];
        self.hasMore = [responseDic objectForKey:@"hasMore"];
        NSArray *couponList = [NSArray modelArrayWithClass:[OrderAfterSaleListInfoModel class] json:[responseDic objectForKey:@"afterSales"]];
        return couponList;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return [self.hasMore boolValue];
}

- (void)loadPullDownPage {
    self.cursor = @"";
    [super loadPullDownPage];
}

#pragma mark - UITableViewDeleagate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        Class current = [OrderAfterSaleListInfoCell class];
        OrderAfterSaleListInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
        if (row < self.listData.count && kValidArray(self.listData)) {
            OrderAfterSaleListInfoModel *infoModel = [self.listData objectAtIndex:row];
            cell.afterSaleModel = infoModel;
        }
        cell.afterSaleClick = ^(OrderAfterSaleListInfoModel * _Nonnull saleModel, OrderAfterSaleListInfoCell * _Nonnull cell) {
            @strongify(self);
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if (saleModel) {
                [params setObject:self.inType forKey:@"type"];
                [params setObject:@(saleModel.skuId) forKey:@"spuId"];
                [params setObject:@(saleModel.orderId) forKey:@"orderId"];
            }
            [YYCommonTools skipAfterSaleDetailPage:self
                                            params:params];
        };
        return cell;
    }
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    OrderAfterSaleListHeaderView *headerView = [[OrderAfterSaleListHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(40))];
    headerView.tipContent = @"如果要申请退款，可以在“全部订单”-“订单详情”中申请";
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(40);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        return UITableViewAutomaticDimension;
    }
    return height;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
