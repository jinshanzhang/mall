//
//  YY_MineOrderViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineOrderViewController.h"

#import "YYPopView.h"
#import "YYPopPullDownMenuView.h"

#import "YY_MineOrderListViewController.h"
#import "VirtualOrderListViewController.h"
@interface YY_MineOrderViewController ()
<VTMagicViewDelegate,
VTMagicViewDataSource>

@property (nonatomic, assign) BOOL   isShowMoney;
@property (nonatomic, assign) NSInteger   isShowAll;  //订单是否显示全部

@property (nonatomic, strong) NSNumber                  *currentPage;
@property (nonatomic, assign) BOOL                      popRoot;
@property (nonatomic, strong) NSNumber                  *pageSource;
@property (nonatomic, strong) NSMutableArray            *itemTitles;
@property (nonatomic, strong) VTMagicController         *magicController;
@property (nonatomic, strong) YYPopPullDownMenuView     *popMenuView;

@end

@implementation YY_MineOrderViewController

#pragma mark - Setter method



- (VTMagicController *)magicController {
    if (!_magicController) {
        _magicController = [[VTMagicController alloc] init];
        _magicController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _magicController.magicView.navigationColor = XHLightColor;
        _magicController.magicView.sliderColor = XHRedColor;
        _magicController.magicView.sliderOffset = -3;
        _magicController.magicView.sliderHeight = 2;
        _magicController.magicView.switchStyle = VTSwitchStyleDefault;
        _magicController.magicView.layoutStyle = VTLayoutStyleDivide;
        _magicController.magicView.navigationHeight = 38.f;
        _magicController.magicView.separatorHeight = 1;
        _magicController.magicView.separatorColor = XHLightColor;
        _magicController.magicView.headerHeight = 48.f;
        _magicController.magicView.dataSource = self;
        _magicController.magicView.delegate = self;
        _magicController.magicView.needPreloading = NO;
    }
    return _magicController;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    @weakify(self);
    [MobClick event:@"myOrder"];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"source"]) {
            self.pageSource = [self.parameter objectForKey:@"source"];
        }
        if ([self.parameter objectForKey:@"currentPage"]) {
            self.currentPage = [self.parameter objectForKey:@"currentPage"];
            if([self.currentPage intValue] == 5) {
                self.currentPage = @(4);
            }
        }
        if ([self.parameter objectForKey:@"from"]) {
            self.popRoot = YES;
        }
        
    }
    
    if ([self.pageSource integerValue] == 1) {
        [self xh_addTitle:@"我的订单"];
    }
    else {
        [self setNavSubViewUI];
    }
    
    [self xh_popTopRootViewController:self.popRoot?YES:NO];
    self.itemTitles = [@[@"全部",@"待付款",@"待收货",@"已完成",@"自提"] mutableCopy];
    [self addChildViewController:self.magicController];
    [self.view addSubview:self.magicController.view];
    
    if ([self.pageSource integerValue] == 2) {
        // 店铺订单
        self.isShowAll = 0;
        self.isShowMoney = YES;
        [self xh_addNavigationItemWithImageName:@"order_show_earnmoney_icon"
                                         isLeft:NO
                                     clickEvent:^(UIButton *sender) {
                                         @strongify(self);
                                         self.isShowMoney = !self.isShowMoney;
                                         if (self.isShowMoney) {
                                             [self xh_addNavigationItemImageName:@"order_show_earnmoney_icon"
                                                                          isLeft:NO];
                                         }
                                         else {
                                             [self xh_addNavigationItemImageName:@"order_hide_earnmoney_icon"
                                                                          isLeft:NO];
                                         }
                                         kPostNotification(OrderListEarnShowNotification, @{@"earnShow":@(self.isShowMoney)});
                                     }];
    }
    
    [self.magicController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(kNavigationH);
    }];
    if ([self.currentPage integerValue] > 0) {
        [self.magicController.magicView reloadDataToPage:[self.currentPage integerValue]];
    }
    else
    [self.magicController.magicView reloadData];
    // Do any additional setup after loading the view.
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(InfoNotificationAction:) name:@"orderPop" object:nil];
}

- (void)InfoNotificationAction:(NSNotification *)notification
{
    [self.magicController.magicView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private method
/**设置导航**/
- (void)setNavSubViewUI {
    UIImageView *logo = [YYCreateTools createImageView:@"store_order_icon"
                                             viewModel:-1];
    [logo addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderSwitchClickEvent:)]];
    [self xh_addTitleView:logo];
    [logo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(90), kSizeScale(17)));
        make.left.mas_equalTo(kSizeScale(40));
        make.centerY.equalTo(logo.superview).offset(IS_IPHONE_X?kSizeScale(17):kSizeScale(10));
    }];
    

    UIView *searchBackgroundView = [YYCreateTools createView:XHSearchGrayColor];
     searchBackgroundView.layer.cornerRadius = kSizeScale(2);
    [searchBackgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchClickEvent:)]];
    [self xh_addTitleView:searchBackgroundView];
    
    UIImageView *searchIcon = [YYCreateTools createImageView:@"home_search_icon"
                                                   viewModel:-1];
    [searchBackgroundView addSubview:searchIcon];

    UILabel *navigationTitleLabel = [YYCreateTools createLabel:@"搜索"
                                                      font:normalFont(12)
                                                 textColor:XHBlackMidColor];
    navigationTitleLabel.textAlignment = NSTextAlignmentLeft;
    [searchBackgroundView addSubview:navigationTitleLabel];
    
    [searchBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(logo);
        make.left.equalTo(logo.mas_right).offset(kSizeScale(10));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(180), 30));
    }];

    [searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(15), kSizeScale(15)));
        make.left.mas_equalTo(kSizeScale(15));
        make.centerY.equalTo(searchBackgroundView);
    }];
    
    [navigationTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchIcon.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(searchIcon);
    }];
}

#pragma mark - Event method
- (void)searchClickEvent:(UITapGestureRecognizer *)tapGesture {
    [YYCommonTools skipSearchPage:self
                            title:@"请输入订单编号/姓名/手机号/昵称搜索" type:1];
}

/**
 *  订单点击切换
 */
- (void)orderSwitchClickEvent:(UITapGestureRecognizer *)gesture {
    @weakify(self);
    self.popMenuView = [[YYPopPullDownMenuView alloc] initWithFrame:CGRectZero];
    self.popMenuView.menuClickBlock = ^(NSInteger menuItemIndex) {
        @strongify(self);
        self.isShowAll = menuItemIndex;
        [self.magicController.magicView reloadDataToPage:self.magicController.currentPage];
        [YYPopView hidenPopView];
    };
    [YYPopView popUpContentView:self.popMenuView onView:gesture.view];
}

#pragma mark - VTMagicViewDelegate
- (CGFloat)magicView:(VTMagicView *)magicView sliderWidthAtIndex:(NSUInteger)itemIndex {
    CGSize size = [YYCommonTools sizeWithText:self.itemTitles[itemIndex]
                                                               font:[UIFont systemFontOfSize:14]
                                                            maxSize:CGSizeMake(MAXFLOAT, 0.0)];
    return size.width;
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    return self.itemTitles;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [menuItem setTitleColor:XHRedColor forState:UIControlStateSelected];
        if (@available(iOS 8.2, *)) {
            menuItem.titleLabel.font = normalFont(14);
        }
    }
    [menuItem setTitle:self.itemTitles[itemIndex] forState:UIControlStateNormal];
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    
    NSString *identifier = [NSString stringWithFormat:@"MineOrder%ld",pageIndex];
    if(pageIndex == 4) {
        VirtualOrderListViewController *vc = [[VirtualOrderListViewController alloc] init];
        return vc;
    } else{
        YY_MineOrderListViewController *otherCtrl = [magicView dequeueReusablePageWithIdentifier:identifier];
        otherCtrl = [[YY_MineOrderListViewController alloc] init];
        otherCtrl.parameter = @{@"tabType": @(pageIndex),
                                @"source": self.pageSource,
                                @"isShowAll":@(self.isShowAll),
                                @"isShowEarn": @(self.isShowMoney)
                                };
        return otherCtrl;
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
