//
//  YY_AfterSaleChooseServiceController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_AfterSaleChooseServiceController.h"
#import "YY_AfterSaleConfigViewController.h"

#import "AfterSaleServiceAPI.h"
#import "AfterSaleTypeModel.h"

#import "AfterSalesTypeCell.h"

@interface YY_AfterSaleChooseServiceController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation YY_AfterSaleChooseServiceController


-(UITableView *)tableView
{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.backgroundColor = XHLightColor;
        self.tableView.allowsSelection = YES;
        self.tableView.estimatedRowHeight = 100;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"选择服务类型"];
    [self xh_popTopRootViewController:NO];
    
    [self getData];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.bottom.equalTo(self.view);
    }];
    Class current = [AfterSalesTypeCell class];
    registerClass(self.tableView, current);
    // Do any additional setup after loading the view.
}

-(void)getData{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.orderID) forKey:@"orderId"];
    [dict setObject:@(self.skuID) forKey:@"skuId"];
    [YYCenterLoading showCenterLoading];
    
    AfterSaleServiceAPI*api = [[AfterSaleServiceAPI alloc] initAfterSaleRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            
            self.dataArr = [[NSMutableArray modelArrayWithClass:[AfterSaleTypeModel class] json:[responDict objectForKey:@"typesInfos"]]mutableCopy];
            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}


#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 8)];
    headerView.backgroundColor = XHClearColor;
    return headerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class currentCls = [AfterSalesTypeCell class];
    AfterSalesTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    }
    
    AfterSaleTypeModel *model = self.dataArr[indexPath.row];
    cell.titleLabel.text = model.title;
    cell.detailLabel.text = model.describe;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   return 66;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AfterSaleTypeModel *model = self.dataArr[indexPath.row];
    YY_AfterSaleConfigViewController *vc = [YY_AfterSaleConfigViewController new];
    vc.submitType = model.type;
    vc.TypeDescribe = model.title;
    vc.orderID = self.orderID;
    vc.skuID = self.skuID;
    vc.FromChoose = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
