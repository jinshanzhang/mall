//
//  YY_MineCouponViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineCouponViewController.h"

#import "CouponTabCountInfoRequestAPI.h"
#import "YY_MineCouponListViewController.h"

@interface CouponListNavInfoModel()

@property (nonatomic, strong) NSNumber *tabType;
@property (nonatomic, copy)   NSString *tabName;
@property (nonatomic, copy)   NSString *tabCount;

@end

@implementation CouponListNavInfoModel

@end



@interface YY_MineCouponViewController ()
<VTMagicViewDelegate,
VTMagicViewDataSource>

@property (nonatomic, strong) NSString                  *couponRuleUrl;  //券的使用规则
@property (nonatomic, strong) NSMutableArray            *itemTitles;
@property (nonatomic, strong) NSMutableArray            *couponTabArrays;
@property (nonatomic, strong) VTMagicController         *magicController;

@end

@implementation YY_MineCouponViewController

#pragma mark - Setter method

- (VTMagicController *)magicController {
    if (!_magicController) {
        _magicController = [[VTMagicController alloc] init];
        _magicController.view.translatesAutoresizingMaskIntoConstraints = NO;
        _magicController.magicView.navigationColor = XHWhiteColor;
        _magicController.magicView.sliderColor = XHLoginColor;
        _magicController.magicView.sliderOffset = -3;
        _magicController.magicView.sliderHeight = 2;
        _magicController.magicView.switchStyle = VTSwitchStyleDefault;
        _magicController.magicView.layoutStyle = VTLayoutStyleDivide;
        _magicController.magicView.navigationHeight = 38.f;
        _magicController.magicView.separatorHeight = 1;
        _magicController.magicView.separatorColor = XHLightColor;
        _magicController.magicView.headerHeight = 48.f;
        _magicController.magicView.dataSource = self;
        _magicController.magicView.delegate = self;
        _magicController.magicView.needPreloading = NO;
    }
    return _magicController;
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"我的优惠券"];
    [MobClick event:@"mineCoupon"];
    [self xh_popTopRootViewController:NO];
    [self xh_addNavigationRightButtonRightPadding:10
                                      buttonTitle:@"使用规则"];
    [self xh_addNavigationItemWithTitle:@"使用规则"
                                 isLeft:NO
                             clickEvent:^(UIButton *sender) {
                                 NSMutableDictionary *params = [NSMutableDictionary dictionary];
                                 [params setObject:@(3) forKey:@"linkType"];
                                 if (kValidString(self.couponRuleUrl)) {
                                     [params setObject:self.couponRuleUrl forKey:@"url"];
                                 }
                                 [YYCommonTools skipOldMultiCombinePage:self params:params];
                             }];
    [self xh_addNavigationItemFont:normalFont(15)
                        titltColor:XHBlackColor
                            isLeft:NO];
    [self addChildViewController:self.magicController];
    [self.view addSubview:self.magicController.view];
    [self.magicController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(kNavigationH);
    }];
    [self gainCouponTabCountRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request
/**获取tab数量**/
- (void)gainCouponTabCountRequest {
    CouponTabCountInfoRequestAPI *tabCount = [[CouponTabCountInfoRequestAPI alloc] initCouponTabCountInfoRequest:[[NSDictionary dictionary] mutableCopy]];
    [tabCount startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            self.itemTitles = [NSMutableArray array];
            if ([responDict objectForKey:@"ruleUrl"]) {
                self.couponRuleUrl = [responDict objectForKey:@"ruleUrl"];
            }
            NSArray *couponTabs = [NSArray modelArrayWithClass:[CouponListNavInfoModel class]
                                                          json:[responDict objectForKey:@"couponTab"]];
            self.couponTabArrays = [couponTabs mutableCopy];
            for (int i = 0; i < couponTabs.count; i ++) {
                CouponListNavInfoModel *model = [couponTabs objectAtIndex:i];
                [self.itemTitles addObject:[NSString stringWithFormat:@"%@(%@)",model.tabName, model.tabCount]];
            }
            [self.magicController.magicView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}


#pragma mark - VTMagicViewDelegate
- (CGFloat)magicView:(VTMagicView *)magicView sliderWidthAtIndex:(NSUInteger)itemIndex {
    CGSize size = [YYCommonTools sizeWithText:self.itemTitles[itemIndex]
                                         font:[UIFont systemFontOfSize:14]
                                      maxSize:CGSizeMake(MAXFLOAT, 0.0)];
    return size.width;
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    return self.itemTitles;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    static NSString *itemIdentifier = @"itemIdentifier";
    UIButton *menuItem = [magicView dequeueReusableItemWithIdentifier:itemIdentifier];
    if (!menuItem) {
        menuItem = [UIButton buttonWithType:UIButtonTypeCustom];
        [menuItem setTitleColor:XHBlackColor forState:UIControlStateNormal];
        [menuItem setTitleColor:XHLoginColor forState:UIControlStateSelected];
        if (@available(iOS 8.2, *)) {
            menuItem.titleLabel.font = normalFont(14);
        }
    }
    [menuItem setTitle:self.itemTitles[itemIndex] forState:UIControlStateNormal];
    return menuItem;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageIndex {
    
    NSString *identifier = [NSString stringWithFormat:@"MineCouponPage%lu",(unsigned long)pageIndex];
    YY_MineCouponListViewController *otherCtrl = [magicView dequeueReusablePageWithIdentifier:identifier];
    if (!otherCtrl) {
        otherCtrl = [[YY_MineCouponListViewController alloc] init];
    }
    if (kValidArray(self.couponTabArrays)) {
        CouponListNavInfoModel *tabModel = self.couponTabArrays[pageIndex];
        otherCtrl.parameter = @{@"tabType": tabModel.tabType};
    }
    return otherCtrl;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
