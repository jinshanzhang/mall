//
//  YY_MineCouponListViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/24.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineCouponListViewController.h"

#import "CouponListInfoModel.h"
#import "YYShareInfoModel.h"

#import "CouponNormalTypeInfoCell.h"
#import "CouponSpecialTypeInfoCell.h"

#import "CouponTabListInfoRequestAPI.h"
#import "CouponShareRequestAPI.h"

#import "YYWebBottomShareView.h"
@interface YY_MineCouponListViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>

@property (nonatomic, strong) YYWebBottomShareView  *shareView;

@property (nonatomic, strong) NSString *cursor;
@property (nonatomic, strong) NSNumber *tabType;
@property (nonatomic, strong) NSNumber *hasMore;

@end

@implementation YY_MineCouponListViewController
#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.footerBgStyleColor = XHLightColor;
        self.NoDataType = YYEmptyViewCouponNoDataType;
        self.tableStyle = UITableViewStyleGrouped;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cursor = @"";
    [self xh_hideNavigation:YES];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"tabType"]) {
            self.tabType = [self.parameter objectForKey:@"tabType"];
        }
    }
    
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.estimatedRowHeight = 44.0;
    
    Class current = [CouponNormalTypeInfoCell class];
    registerClass(self.m_tableView, current);
    current = [CouponSpecialTypeInfoCell class];
    registerClass(self.m_tableView, current);
    self.view.backgroundColor = self.m_tableView.backgroundColor = XHLightColor;
    
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self loadNewer];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Baser Request
- (void)couponShare:(int)couponID
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(couponID) forKey:@"userCouponId"];
    
    [YYCenterLoading showCenterLoading];
    CouponShareRequestAPI *api = [[CouponShareRequestAPI alloc] initCouponShareRequest:dic];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            NSMutableArray *umShareContents = [NSMutableArray array];
            NSArray *shareContents = [NSArray modelArrayWithClass:[YYShareInfoModel class] json:[responDict objectForKey:@"shareCouponIcons"]];
            [shareContents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
#pragma mark - 注意: 这里接口返回的字段名字为shareLink。
                YYShareInfoModel *shareModel = (YYShareInfoModel *)obj;
                shareModel.shareLinkUrl = shareModel.shareLink;
                YYShareCombineInfoModel *umShareModel = [[YYShareCombineInfoModel alloc] init];
                umShareModel.isFriendShareLink = YES;
                umShareModel.operatorType = YYShareOperatorWechatSession;
                umShareModel.title = shareModel.shareTitle;
                umShareModel.detailTitle = shareModel.shareDesc;
                umShareModel.webpageUrl = shareModel.shareLinkUrl;
                umShareModel.thumbImage = shareModel.shareImage;
                umShareModel.shareImage = shareModel.shareImage;
                if (shareModel.shareImage) {
                    umShareModel.shareImageArray = [@[shareModel.shareImage] mutableCopy];
                }
                [umShareContents addObject:umShareModel];
            }];
            NSMutableArray *shareIcons = [NSMutableArray arrayWithObjects:@"allgive_icon",
                                          @"limitgive_icon",nil];
            NSMutableArray *shareTitles = [NSMutableArray arrayWithObjects:@"永久赠送",@"限制赠送", nil];
            self.shareView = [YYWebBottomShareView initYYPopView:[[YYWebBottomShareView alloc] initShowBottomView: shareIcons showTitles: shareTitles extend:[@{@"showCouponTip":@(YES)} mutableCopy]]];
            self.shareView.shareTipLable.text = @"转赠类型";
            [self.shareView showViewOfAnimateType:YYPopAnimateDownUp];
            @weakify(self);
            self.shareView.shareBlock = ^(NSInteger atIndex) {
                @strongify(self);
                /*atIndex = 0 永久赠送    1 限制赠送。 （shareContents 里）
                YYShareInfoModel type = 0 限制赠送 type = 1 永久赠送*/
                if (kValidArray(shareContents)) {
                    __block NSInteger selectType = atIndex;
                    __block YYShareCombineInfoModel *selectModel = nil;
                    [shareContents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        YYShareInfoModel *shareModel = (YYShareInfoModel *)obj;
                        if ((selectType == 0 && shareModel.type == 1) || (selectType == 1 && shareModel.type == 0)) {
                            selectModel = [umShareContents objectAtIndex:idx];
                            *stop = YES;
                        }
                    }];
                    if (selectModel) {
                        [kShareManager umengShareModel:selectModel
                                              callBack:^(BOOL status) {
                                                  @strongify(self);
                                                  [self.shareView showViewOfAnimateType:YYPopAnimateDownUp];
                                                  [self loadPullDownPage];
                                              }];
                    }
                    else {
                        [YYCommonTools showTipMessage:@"数据获取失败，请稍后重试"];
                    }
                }
            };
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.tabType forKey:@"tabType"];
    [params setObject:self.cursor forKey:@"cursor"];
    CouponTabListInfoRequestAPI *couponListAPI = [[CouponTabListInfoRequestAPI alloc] initCouponTabListInfoRequest:params];
    return couponListAPI;
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.cursor = [responseDic objectForKey:@"cursor"];
        self.hasMore = [responseDic objectForKey:@"hasMore"];
        NSArray *couponList = [NSArray modelArrayWithClass:[CouponListInfoModel class] json:[responseDic objectForKey:@"couponList"]];
        return couponList;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return [self.hasMore boolValue];
}

- (void)loadPullDownPage {
    self.cursor = @"";
    [super loadPullDownPage];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (!cell) {
        @weakify(self);
        NSInteger row = indexPath.row;
        Class currentCls = [CouponSpecialTypeInfoCell class];
        CouponSpecialTypeInfoCell *specialcell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        
        Class currentCl = [CouponNormalTypeInfoCell class];
        CouponNormalTypeInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCl)];

        if (kValidArray(self.listData) && row < self.listData.count) {
            CouponListInfoModel *couponModel = [self.listData objectAtIndex:row];
            couponModel.tabType = self.tabType;

            if (couponModel.shareAble||couponModel.shareDesc) {
                specialcell.couponModel =couponModel;
                specialcell.immediatelyBlock = ^(CouponListInfoModel *model){
                    @strongify(self);
                    NSMutableDictionary *params = [NSMutableDictionary dictionary];
                    [params setObject:@(model.target.linkType) forKey:@"linkType"];
                    [params setObject:model.target.url forKey:@"url"];
                    [YYCommonTools skipOldMultiCombinePage:self params:params];
                };
                specialcell.giveAwayBlock = ^(int couponID) {
                    [self couponShare:couponID];
                };
                return specialcell;
            }else{
                cell.couponModel = couponModel;
                cell.immediatelyBlock = ^(CouponListInfoModel *model){
                    @strongify(self);
                    NSMutableDictionary *params = [NSMutableDictionary dictionary];
                    [params setObject:@(model.target.linkType) forKey:@"linkType"];
                    [params setObject:model.target.url forKey:@"url"];
                    [YYCommonTools skipOldMultiCombinePage:self params:params];
                };
                return cell;
            }
        }
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [YYCreateTools createView:XHLightColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(12);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = [super tableView:tableView heightForRowAtIndexPath:indexPath];
    if (!height) {
        return  UITableViewAutomaticDimension;
    }
    return height;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
