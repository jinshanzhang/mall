//
//  YY_MineOrderListViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineOrderListViewController.h"

#import "OrderListInfoModel.h"
#import "HomeBannerInfoModel.h"
#import "OrderListGoodInfoCell.h"
#import "OrderSumCell.h"
#import "OrderListSectionFooterInfoView.h"
#import "OrderListSectionHeaderInfoView.h"
#import "MineOrderSourceVivew.h"

#import "MineOrderListInfoRequestAPI.h"
#import "YYStoreOrdersInfoRequestAPI.h"
#import "YYCommonResourceAPI.h"
@interface YY_MineOrderListViewController ()
<
UITableViewDelegate,
UITableViewDataSource
>
@property (nonatomic, assign) BOOL     isShowEarn;
@property (nonatomic, assign) NSInteger     isShowAll;
@property (nonatomic, strong) NSString *cursor;
@property (nonatomic, strong) NSNumber *tabType;
@property (nonatomic, strong) NSNumber *hasMore;
@property (nonatomic, strong) NSNumber *systemTime;
@property (nonatomic, strong) NSNumber *pageSoucre;
@property (nonatomic, strong) MineOrderSourceVivew  *headerView;

@end

/** 前端订单展示按照返佣商家的店铺维度进行展示**/
@implementation YY_MineOrderListViewController
#pragma mark - Setter method

- (MineOrderSourceVivew *)headerView {
    if (!_headerView) {
        _headerView = [[MineOrderSourceVivew alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 0.0001f)];
    }
    return _headerView;
}

#pragma mark - Life cycle
- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.emptyTitle = @"您目前没有相关订单";
        self.NoDataType = YYEmptyViewOrderNoDataType;
        self.tableStyle = UITableViewStyleGrouped;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    self.cursor = @"";
    [self xh_hideNavigation:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(earnShowNotification:) name:OrderListEarnShowNotification object:nil];

    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"tabType"]) {
            self.tabType = [self.parameter objectForKey:@"tabType"];
        }
        if ([self.parameter objectForKey:@"source"]) {
            self.pageSoucre = [self.parameter objectForKey:@"source"];
        }
        if ([self.parameter objectForKey:@"isShowEarn"]) {
            self.isShowEarn = [[self.parameter objectForKey:@"isShowEarn"] boolValue];
        }
        if ([self.parameter objectForKey:@"isShowAll"]) {
            self.isShowAll = [[self.parameter objectForKey:@"isShowAll"] integerValue];
        }
    }
    self.m_tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.m_tableView.estimatedRowHeight = 45;
    // 下面高度需要设置为5.0 否则低版本系统出现页面错乱。
    self.m_tableView.estimatedSectionHeaderHeight = 5.0;
    self.m_tableView.estimatedSectionFooterHeight = 5.0;
    //self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.sectionFooterHeight = UITableViewAutomaticDimension;
    self.m_tableView.sectionHeaderHeight = UITableViewAutomaticDimension;
    
    Class current = [OrderListGoodInfoCell class];
    registerClass(self.m_tableView, current);
    current = [OrderListSectionHeaderInfoView class];
    [self.m_tableView registerClass:current forHeaderFooterViewReuseIdentifier:strFromCls(current)];
    current = [OrderListSectionFooterInfoView class];
    [self.m_tableView registerClass:current forHeaderFooterViewReuseIdentifier:strFromCls(current)];
    current = [OrderSumCell class];
    registerClass(self.m_tableView, current);


    self.m_tableView.backgroundColor = XHLightColor;
    
    [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    self.m_tableView.tableHeaderView = self.headerView;
    
    [kCountDownManager start];
    [self commonResource];
    [self loadNewer];
    
    self.headerView.headerClickBlock = ^(NSMutableDictionary *params) {
        @strongify(self);
        [YYCommonTools skipMultiCombinePage:self params:params];
    };
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Baser Request
- (YYBaseRequestAPI *)baseRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.tabType forKey:@"tabType"];
    if (kValidString(self.cursor)) {
        [params setObject:self.cursor forKey:@"cursor"];
    }
    if ([self.pageSoucre integerValue] == 1) {
        MineOrderListInfoRequestAPI  *orderList = [[MineOrderListInfoRequestAPI alloc] initOrderListInfoRequest:params];
        return orderList;
    }
    else {
        [params setObject:@(self.isShowAll) forKey:@"profitType"];// 1 我的订单 0 全部订单
        YYStoreOrdersInfoRequestAPI *storeAPI = [[YYStoreOrdersInfoRequestAPI alloc] initStoreOrdersInfoRequest:params];
        return storeAPI;
    }
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    if (kValidDictionary(responseDic)) {
        self.cursor = [responseDic objectForKey:@"cursor"];
        self.systemTime = [responseDic objectForKey:@"systemTime"];
        self.hasMore = [responseDic objectForKey:@"hasMore"];
        NSMutableArray *orderList = [[NSArray modelArrayWithClass:[OrderListInfoModel class] json:[responseDic objectForKey:@"orderList"]] mutableCopy];
        // 添加倒计时
        NSString *countDownIdentifier = [NSString stringWithFormat:@"YYOrderListPage%d",self.current];
        [kCountDownManager addSourceWithIdentifier:countDownIdentifier];
        [kCountDownManager reloadAllSource];
        [orderList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            OrderListInfoModel *listModel = (OrderListInfoModel *)obj;
            listModel.countDownIdentifier = countDownIdentifier;
            listModel.systemTime = self.systemTime;
            NSInteger deadTime = [listModel.payDeadlineTime integerValue]/1000;
            //NSInteger createTime = [listModel.orderTime integerValue]/1000 + 30 * 60;
            NSInteger systemTime = [listModel.systemTime integerValue]/1000;
            listModel.diffValue = deadTime - systemTime;
        }];
        return orderList;
    }
    return nil;
}

- (BOOL)canLoadMore {
    return [self.hasMore boolValue];
}

- (void)loadPullDownPage {
    self.cursor = @"";
    [self commonResource];
    [super loadPullDownPage];
}
#pragma mark - Request
/**
 *  订单资源位
 */
- (void)commonResource {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if ([self.pageSoucre integerValue] == 1) {
        [dic setObject:@"3" forKey:@"type"];
    } else {
        [dic setObject:@"2" forKey:@"type"];
    }
    YYCommonResourceAPI *api = [[YYCommonResourceAPI alloc] initRequest:dic];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            BOOL switchFlag = NO;
            NSMutableArray *resources = [NSMutableArray array];
            if ([responDict objectForKey:@"switchFlag"]) {
                switchFlag = [[responDict objectForKey:@"switchFlag"] boolValue];
            }
            if ([responDict objectForKey:@"resources"]) {
                NSArray *tempArray = [NSArray modelArrayWithClass:[HomeBannerInfoModel class] json:[responDict objectForKey:@"resources"]];
                if (kValidArray(tempArray)) {
                    resources = [tempArray mutableCopy];
                }
                if (switchFlag) {
                    if (!kValidArray(resources)) {
                        switchFlag = NO;
                    }
                }
            }
            HomeBannerInfoModel *bannerModel = nil;
            CGRect bannerFrame = CGRectMake(0, 0, kScreenW, 0.0001f);
            if (switchFlag) {
                bannerModel = [resources objectAtIndex:[self.tabType intValue]];
                if (kValidString(bannerModel.imageUrl)) {
                    bannerModel.height = [UIImage getImageSizeWithURL:bannerModel.imageUrl].height;
                }
                bannerFrame = CGRectMake(0, 0, kScreenW, kSizeScale(bannerModel.height/2.0));
            }
            self.headerView.infoModel = bannerModel;
            self.headerView.frame = bannerFrame;
            [self.m_tableView layoutIfNeeded];
            self.m_tableView.tableHeaderView = self.headerView;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

#pragma mark - Private method
/**订单操作处理**/
- (void)orderClickOperator:(YYOrderOperatorType)type
                 listModel:(OrderListInfoModel *)listModel{
    if (type == YYOrderOperatorCancel) {
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"取消后将不能恢复，确认取消订单吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
            
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig cancelOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadPullDownPage];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorConfirm) {
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确认收货吗？收到商品后再确认收货，以免造成损失。" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig confirmOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadPullDownPage];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorDelete) {
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"删除后订单不能恢复，确认删除该订单吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig deleteOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] isStore:([self.pageSoucre integerValue] == 2?YES:NO) success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadPullDownPage];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorLookLogistic) {
        //查看物流
        [YYCommonTools skipLogisticsPage:self params:@{@"orderID": listModel.orderId}];
    }
    else if (type == YYOrderOperatorPay) {
        [YYCommonTools skipOrderPay:self
                            orderID:[listModel.orderId longValue]
                        canBackRoot:NO];
    }
}

/**修改订单状态**/
- (void)modifyOrderStatus:(NSIndexPath *)currentPath
              orderStatus:(NSNumber *)orderStatus
                operators:(NSNumber *)operators {
    
    __block BOOL  isOperator = NO;
    __block NSNumber  *tempStatus = orderStatus;
    __block NSNumber  *tempOperator = operators;
    __block NSInteger section = currentPath.section;
    
    if ([self.tabType integerValue] == 0) {
        // 全部订单列表
        [self.listData enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            OrderListInfoModel *listModel = (OrderListInfoModel *)obj;
            if (idx == section) {
                listModel.orderStatus = tempStatus;
                listModel.operation = tempOperator;
                isOperator = YES;
            }
        }];
    }
    else if ([self.tabType integerValue] == 2) {
        // 待收货列表
        // 删除list 对应的数据
        [self.listData removeObjectAtIndex:section];
        isOperator = YES;
    }
    if (isOperator) {
        [UIView performWithoutAnimation:^{
            [self.m_tableView reloadData];
        }];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.listData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        return self.isShowEarn?(infoModel.skuList.count+infoModel.sumList.count):infoModel.skuList.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    Class current = [OrderListGoodInfoCell class];
    OrderListGoodInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        if (self.isShowEarn &&[self.pageSoucre integerValue] !=1) {
            if (row<infoModel.sumList.count) {
                Class current = [OrderSumCell class];
                OrderSumCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(current)];
                OrderSumInfoModel *sumModel = infoModel.sumList[indexPath.row];
                cell.sumModel = sumModel;
                return cell;
            }else{
                OrderGoodInfoModel *goodModel = [infoModel.skuList objectAtIndex:row-infoModel.sumList.count];
                goodModel.source = self.pageSoucre;
                goodModel.showEarn = self.isShowEarn;
                cell.goodModel = goodModel;
            }
        }else{
            OrderGoodInfoModel *goodModel = [infoModel.skuList objectAtIndex:row];
                        goodModel.source = self.pageSoucre;
                        goodModel.showEarn = self.isShowEarn;
                        cell.goodModel = goodModel;
        }
        
        cell.orderClick = ^(OrderGoodInfoModel *goodModel, OrderListGoodInfoCell *listCell) {
            @strongify(self);
            __block NSIndexPath *currentPath = [self.m_tableView indexPathForCell:listCell];
            [YYCommonTools skipOrderDetial:self
                                    params:@{@"orderID": goodModel.orderId,
                                             @"source": self.pageSoucre,
                                             @"tabType": self.tabType}
                                 ctrlBlock:^(NSNumber *orderStatus,NSNumber *operator,BOOL isReceive) {
                                                 @strongify(self);
                                                 if (isReceive) {
                                                     [self modifyOrderStatus:currentPath
                                                                 orderStatus:orderStatus
                                                                   operators:operator];
                                                 }
                                    }];
        };
        
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    Class currentCls = [OrderListSectionHeaderInfoView class];
    OrderListSectionHeaderInfoView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:strFromCls(currentCls)];
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        infoModel.source = self.pageSoucre;
        infoModel.isShowMark = self.isShowEarn;
        sectionHeaderView.listModel = infoModel;
        sectionHeaderView.orderStatus = infoModel.orderStatus;
    }
    return sectionHeaderView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    @weakify(self);
    Class currentCls = [OrderListSectionFooterInfoView class];
    OrderListSectionFooterInfoView *sectionFooterView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:strFromCls(currentCls)];
    if (section < self.listData.count && kValidArray(self.listData)) {
        OrderListInfoModel *infoModel = [self.listData objectAtIndex:section];
        sectionFooterView.listModel = infoModel;
        sectionFooterView.sectionFooterBtnOperatorBlock = ^(YYOrderOperatorType type, OrderListInfoModel *model) {
            @strongify(self);
            [self orderClickOperator:type listModel:model];
        };
    }
    return sectionFooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)dealloc {
    [kCountDownManager removeAllSource];
    [kCountDownManager invalidate];
}

#pragma mark - Event
- (void)earnShowNotification:(NSNotification *)notification {
    NSNumber *showEarn = [notification.object objectForKey:@"earnShow"];
    self.isShowEarn = [showEarn boolValue];
    [UIView performWithoutAnimation:^{
        [self.m_tableView reloadData];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
