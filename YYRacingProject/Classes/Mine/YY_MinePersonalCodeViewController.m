//
//  YY_MinePersonalCodeViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/15.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_MinePersonalCodeViewController.h"
#import "YYPersonalCodeAPI.h"
#import "PersonalQRParamModel.h"

@interface YY_MinePersonalCodeViewController ()<CLLocationManagerDelegate>

@property (nonatomic, copy) NSString *blackBox;
@property (nonatomic, strong) CLLocationManager *locationmanager;//定位服务
@property (nonatomic, strong) PersonalQRParamModel *QRmodel;


@end

@implementation YY_MinePersonalCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"我的专属二维码"];
    [self xh_popTopRootViewController:NO];
    [self xh_navBackgroundColor:HexRGB(0xf7f7f7)];
    self.view.backgroundColor = HexRGB(0xf7f7f7);

//    FMDeviceManager_t *manager = [FMDeviceManager sharedManager];
//    /* 获取设备指纹黑盒数据，请确保在应用开启时已经对SDK进行初始化，切勿在get的时候才初始化
//     * 如果此处获取到的blackBox特别长(超过400字节)，说明初始化尚未完成(一般需要1-3秒)，或者由于网络问题导致初始化失败，进入了降级处理
//     * 降级不影响正常设备信息的获取，只是会造成blackBox字段超长，且无法获取设备真实IP
//     * 降级数据平均长度在2KB以内,一般不超过3KB,数据的长度取决于采集到的设备信息的长度,无法100%确定最大长度
//     */
//    self.blackBox = manager->getDeviceInfo();
    [self getLocation];
}

-(void)getLocation
{
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationmanager = [[CLLocationManager alloc]init];
        [_locationmanager requestAlwaysAuthorization];
        [_locationmanager requestWhenInUseAuthorization];
        _locationmanager.delegate = self;
        //设置寻址精度
        _locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationmanager.distanceFilter = 5.0;
        [_locationmanager startUpdatingLocation];
    }
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    if (error.code == kCLErrorDenied) {
        // 提示用户出错
        [JCAlertView showTwoButtonsWithTitle:@"定位服务已关闭" Message:@"您需要打开定位权限，才能获取您的 专属二维码。请到设置-隐私-定位服 务中开启【蜀黍之家】定位服务。" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlackColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor ButtonTitle:@"去设置" Click:^{
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        } type:JCAlertViewTypeDefault];
    }
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [self.locationmanager stopUpdatingHeading];
    _locationmanager.delegate = nil;
    //旧址
    CLLocation *currentLocation = [locations lastObject];
    NSLog(@"%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
    
    NSString *doc = [NSString stringWithFormat:@"%f,%f",currentLocation.coordinate.longitude,currentLocation.coordinate.latitude];
    [self getPersonalQRcode:[doc encode:doc]];
    
}
-(void)getPersonalQRcode:(NSString *)doc{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:doc?:@"" forKey:@"loc"];
    [dict setObject:self.blackBox?:@"" forKey:@"blackBox"];
    
    YYPersonalCodeAPI *API = [[YYPersonalCodeAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSInteger ret = [[responDict objectForKey:@"ret"] intValue];;
            if (ret) {
                [JCAlertView showOneButtonsWithTitle:@"提示" Message:[responDict objectForKey:@"msg"] ButtonTitle:@"我知道啦"ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHRedColor Click:^{
                    [self.navigationController popViewControllerAnimated:YES];
                } type:JCAlertViewTypeDefault];
            }else{
                self.QRmodel = [PersonalQRParamModel modelWithJSON:responDict];
                [self creatUI];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}


-(void)creatUI{
    UIView *backGoundView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:backGoundView];
    backGoundView.v_cornerRadius = 2;
    
    [backGoundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30+kNavigationH);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-100);
    }];
    
    UILabel *title = [YYCreateTools createLabel:@"添加城市合伙人为导师，可参与更多店主活动获得城市导师面对面指导" font:normalFont(14) textColor:XHBlackColor];
    title.numberOfLines = 0;
    [backGoundView addSubview:title];
    
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(40);
        make.left.mas_equalTo(40);
        make.right.mas_equalTo(-40);
    }];

    UIImageView *codeImg = [YYCreateTools createImageView:@"" viewModel:-1];
    [backGoundView addSubview:codeImg];
    
    [codeImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(title);
        make.top.mas_equalTo(title.mas_bottom).mas_offset(24);
        make.width.height.mas_equalTo(kSizeScale(255));
    }];
    
    UIImage *logoIMG = [UIImage getImageFromURL:self.QRmodel.avatarUrl];
    codeImg.image = [SGQRCodeObtain generateQRCodeWithData:self.QRmodel.retParam size:255 logoImage:logoIMG ratio:0.2];

    UILabel *loacation = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:HexRGB(0x0054FF)];
    loacation.textAlignment = NSTextAlignmentCenter;
    [backGoundView addSubview:loacation];
    
    NSMutableAttributedString * attriStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@.%@",[self.QRmodel.addr objectForKey:@"province"],[self.QRmodel.addr objectForKey:@"city"]]];
    /**
     添加图片到指定的位置
     */
    NSTextAttachment *attchImage = [[NSTextAttachment alloc] init];
    // 表情图片
    attchImage.image = [UIImage imageNamed:@"qrparamLocation_icon"];
    // 设置图片大小
    attchImage.bounds = CGRectMake(0, -2, 12, 12);
    NSAttributedString *stringImage = [NSAttributedString attributedStringWithAttachment:attchImage];
    [attriStr insertAttributedString:stringImage atIndex:0];
    loacation.attributedText = attriStr;

    [loacation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backGoundView);
        make.top.mas_equalTo(codeImg.mas_bottom).mas_offset(20);
        make.width.mas_equalTo(120);
    }];

    UILabel *timeLabel = [YYCreateTools createLabel:[NSString stringWithFormat:@"二维码有效期截止 %@", [NSString formatDateAndTime:[self.QRmodel.qrExpireAt doubleValue]]] font:normalFont(12) textColor:XHBlackLitColor];
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [backGoundView addSubview:timeLabel];

    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(backGoundView);
        make.top.mas_equalTo(loacation.mas_bottom).mas_offset(20);
        make.width.mas_equalTo(300);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
