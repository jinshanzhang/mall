//
//  YY_AfterSaleDetailViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_AfterSaleDetailViewController.h"

#import "OrderGoodInfoModel.h"
#import "OrderAfterSaleDetailInfoModel.h"

#import "OrderAfterSaleMessageInfoCell.h"
#import "OrderAfterSaleMessageKefuInfoCell.h"

#import "OrderAfterSaleDetailHeaderView.h"
#import "MineAfterSaleDetailInfoRequestAPI.h"
#import "YYexpressDetailAPI.h"
#import "YYConfirmGoodAPI.h"
#import "MineAfterSaleCancelApplyInfoRequestAPI.h"
#import "MineAfterSaleApplyArbitrationInfoRequestAPI.h"

#define kAfterDetailOperatorTag 460
@interface YY_AfterSaleDetailViewController ()
<UITableViewDelegate,
UITableViewDataSource>

@property (nonatomic, strong) UIView    *bottomView;
@property (nonatomic, strong) OrderAfterSaleDetailHeaderView *detailHeaderView;

@property (nonatomic, strong) OrderGoodInfoModel *goodModel;
@property (nonatomic, strong) NSMutableArray *operatorModel;
@property (nonatomic, strong) NSNumber  *spuId;
@property (nonatomic, strong) NSNumber  *orderId;
@property (nonatomic, strong) NSNumber  *inType;
@property (nonatomic, strong) OrderAfterSaleDetailInfoModel *detailModel;

@end

@implementation YY_AfterSaleDetailViewController

#pragma mark - Setter method
- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [YYCreateTools createView:XHWhiteColor];
    }
    return _bottomView;
}

#pragma mark - Life cycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self gainAfterSaleDetailRequest];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"售后详情"];
    [self xh_popTopRootViewController:NO];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"spuId"]) {
            self.spuId = [self.parameter objectForKey:@"spuId"];
        }
        if ([self.parameter objectForKey:@"orderId"]) {
            self.orderId = [self.parameter objectForKey:@"orderId"];
        }
        if ([self.parameter objectForKey:@"type"]) {
            self.inType = [self.parameter objectForKey:@"type"];
        }
    }
    [self createUI];
    [self gainAfterSaleDetailRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request
/**获取售后详情**/
- (void)gainAfterSaleDetailRequest {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.orderId forKey:@"orderId"];
    [params setObject:self.spuId forKey:@"skuId"];
    MineAfterSaleDetailInfoRequestAPI *detailAPI = [[MineAfterSaleDetailInfoRequestAPI alloc] initAfterSaleDetailInfoRequest:params];
    [detailAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            self.goodModel = [[OrderGoodInfoModel alloc] init];
            self.detailModel = [OrderAfterSaleDetailInfoModel modelWithJSON:responDict];
            self.detailHeaderView.backgroundColor = XHWhiteColor;
            if (self.detailModel) {
                self.goodModel.skuCnt = @(self.detailModel.skuCnt);
                self.goodModel.orderId =@(self.detailModel.orderId);
                self.goodModel.skuId = @(self.detailModel.skuId);
                self.goodModel.skuTitle = self.detailModel.skuTitle;
                self.goodModel.skuProp = self.detailModel.skuProp;
                self.goodModel.skuCover = self.detailModel.imageUrl;
                NSMutableDictionary *priceDict = [NSMutableDictionary dictionary];
                if (kValidString(self.detailModel.skuPrice)) {
                    [priceDict setObject:self.detailModel.skuPrice forKey:@"priceTag"];
                }
                self.goodModel.price = priceDict;
                self.detailHeaderView.goodModel = self.goodModel;
            }
            self.operatorModel = [YYCommonTools getAfterSaleShowOperatorBtns:@(self.detailModel.button)];
            if (self.detailModel.button > 1 && self.operatorModel.count > 0 && [self.inType integerValue] != 1) {
                // 存在底部操作按钮
                [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.mas_equalTo(-kBottom(0));
                    make.left.right.equalTo(self.view);
                    make.height.mas_equalTo(kSizeScale(50));
                }];
                
                [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make){
                    make.left.right.equalTo(self.view);
                    make.top.equalTo(self.detailHeaderView.mas_bottom);
                    make.bottom.equalTo(self.bottomView.mas_top);
                }];
                
                [self operatorBtnIsShow:self.operatorModel];
            }
            else {
                // 不存在底部操作按钮
                self.bottomView.hidden = YES;
                [self.m_tableView mas_remakeConstraints:^(MASConstraintMaker *make){
                    make.left.right.bottom.equalTo(self.view);
                    make.top.equalTo(self.detailHeaderView.mas_bottom);
                }];
            }
            [self.m_tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}
/**取消售后申请**/
- (void)cancelAfterSaleApply {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.orderId forKey:@"orderId"];
    [params setObject:self.spuId forKey:@"skuId"];
    MineAfterSaleCancelApplyInfoRequestAPI *cancelAPI = [[MineAfterSaleCancelApplyInfoRequestAPI alloc] initAfterSaleCancelApplyInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [cancelAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self gainAfterSaleDetailRequest];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}



#pragma mark - Private
- (void)createUI {
    self.detailHeaderView = [[OrderAfterSaleDetailHeaderView alloc] initWithFrame:CGRectMake(0, kNavigationH, kScreenW, kSizeScale(16)*2+kSizeScale(80))];
    self.detailHeaderView.backgroundColor = XHLightColor;
    [self.view addSubview:self.detailHeaderView];
    
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.estimatedRowHeight = 44.0;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.backgroundColor = XHLightColor;
    Class currentCls = [OrderAfterSaleMessageKefuInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    [self.view addSubview:self.m_tableView];
    [self.view addSubview:self.bottomView];
    
    Class cls = [OrderAfterSaleMessageInfoCell class];
    registerClass(self.m_tableView, cls);
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailHeaderView.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
}

- (void)operatorBtnIsShow:(NSMutableArray *)operatorModels {
    [self.bottomView removeAllSubviews];
    
    if (operatorModels.count == 3) {
        [operatorModels exchangeObjectAtIndex:0 withObjectAtIndex:2];
    }
    if (kValidArray(operatorModels)) {
        CGFloat btnWidth = kSizeScale(68);
        CGFloat btnHeight = kSizeScale(25);
        UIButton *lastBtn = nil;
        for (int i = 0; i < operatorModels.count ; i ++) {
            UIButton *btn = [YYCreateTools createBtn:@""
                                                font:midFont(12)
                                           textColor:XHBlackMidColor];
            btn.tag = kAfterDetailOperatorTag + i;
            btn.layer.cornerRadius = 2;
            btn.clipsToBounds = YES;
            [btn addTarget:self action:@selector(buttonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
            [self.bottomView addSubview:btn];
            
            YYOperatorInfoModel *infoModel = operatorModels[i];
            btn.layer.borderWidth = 0.8;
            btn.layer.borderColor = (infoModel.isSelect ? XHLoginColor.CGColor : XHLineColor.CGColor);
            [btn setTitle:infoModel.showTitle forState:UIControlStateNormal];
            [btn setTitleColor:(infoModel.isSelect?XHLoginColor:XHBlackMidColor) forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageWithColor:XHWhiteColor] forState:UIControlStateNormal];
            
            if (lastBtn) {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(lastBtn);
                    make.right.equalTo(lastBtn.mas_left).offset(-kSizeScale(12));
                    make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                }];
            }
            else {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.bottomView).offset(-kSizeScale(3));
                    make.right.equalTo(self.bottomView.mas_right).offset(-kSizeScale(12));
                    make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                }];
            }
            lastBtn = btn;
        }
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.detailModel.messages.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @weakify(self);
    NSInteger row = indexPath.row;
    Class cls = [OrderAfterSaleMessageKefuInfoCell class];
    if (row == self.detailModel.messages.count) {
        OrderAfterSaleMessageKefuInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
        cell.kefuClickBlock = ^{
            @strongify(self);
//            NSMutableDictionary *params = [NSMutableDictionary dictionary];
//            HDOrderInfo *orderInfo = [[HDOrderInfo alloc] init];
//            orderInfo.imageUrl = self.goodModel.skuCover;
//            orderInfo.title = [NSString stringWithFormat:@"订单编号：%@",self.goodModel.orderId];
//            orderInfo.price = [NSString stringWithFormat:@"%@",[self.goodModel.price objectForKey:@"priceTag"]];
//            orderInfo.orderTitle = self.goodModel.skuTitle;
//            orderInfo.itemUrl = [NSString stringWithFormat:@"%@=>%@", self.orderId, self.spuId];
//            orderInfo.desc = [NSString stringWithFormat:@"%@",self.inType];
//            [params setObject:orderInfo forKey:@"orderInfo"];
//            [YYCommonTools skipKefu:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"4000568110"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
        };
        return cell;
    }
    else {
        Class cls = [OrderAfterSaleMessageInfoCell class];
        OrderAfterSaleMessageInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
        if (row < self.detailModel.messages.count) {
            OrderAfterSaleDetailMessageInfoModel *model = [self.detailModel.messages objectAtIndex:row];
            model.isFirst = ((row == 0)?YES:NO);
            cell.messageModel = model;
        }
        return cell;
    }
}

#pragma mark - Event
- (void)buttonClickEvent:(UIButton *)sender {
    NSInteger currentTag = sender.tag - kAfterDetailOperatorTag;
    if (currentTag < self.operatorModel.count) {
        YYOperatorInfoModel *model = self.operatorModel[currentTag];
        if (model.afterSaleOperatorType == YYAfterSaleOperatorCancel) {
            [self cancelAfterSaleApply];
        }
        else if (model.afterSaleOperatorType == YYAfterSaleOperatorModifyApply) {
            [YYCommonTools skipAfterSaleConfig:self
                                       orderID:[self.goodModel.orderId longValue]
                                         skuID:[self.goodModel.skuId intValue]
                                        change:YES
             Arbitration:NO
                                         block:^(NSString *result) {
                                             
                                         }
             ];
        }
        else if (model.afterSaleOperatorType == YYAfterSaleOperatorApplyArbitration) {
            [YYCommonTools skipAfterSaleConfig:self
                                       orderID:[self.goodModel.orderId longValue]
                                         skuID:[self.goodModel.skuId intValue]
                                        change:YES
                                   Arbitration:YES
                                         block:^(NSString *result) {
                            [self gainAfterSaleDetailRequest];
                                         }];
        }
        else if (model.afterSaleOperatorType == YYAfterSaleOperatorReApply) {
            [YYCommonTools skipAfterSaleChoose:self
                                       orderID:[self.goodModel.orderId longValue]
                                         skuID:[self.goodModel.skuId intValue]];
        }
        else if (model.afterSaleOperatorType == YYAfterSaleOperatorUploadLogistic) {
            [YYCommonTools skipUploadLogisticPage:self
                                           params:self.parameter];
        }else if (model.afterSaleOperatorType == YYAfterSaleOperatorConfirm){
            [self confirmGood];
        }else if (model.afterSaleOperatorType == YYAfterSaleOperatorCheckLogistic){
            [YYCommonTools skipLogisticsPage:self params:@{@"orderID": self.goodModel.orderId,@"skuID":self.goodModel.skuId}];
        }
    }
}

/*确认收货*/
- (void)confirmGood {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.orderId forKey:@"orderId"];
    [params setObject:self.spuId forKey:@"skuId"];
    YYConfirmGoodAPI *api = [[YYConfirmGoodAPI alloc] initRequest:params];
    [YYCenterLoading showCenterLoading];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self gainAfterSaleDetailRequest];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
