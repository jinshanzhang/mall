//
//  YY_MineSignViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/8/28.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineSignViewController.h"
#import "OrderCommonCell.h"
#import "chooseSignSetBank.h"
#import "ChooseLocationView.h"

#import "SignBankRequestAPI.h"
#import "SignCertifyRequestAPI.h"
#import "SignCertifyInfoRequestAPI.h"


#import "BankModel.h"
#import "SignInfoModel.h"

@interface YY_MineSignViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>
@property (nonatomic, strong) NSMutableArray *cellTitles;
@property (nonatomic, strong) NSMutableArray *placeHolderArr;

@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UILabel *bankLabel;
@property (nonatomic, strong) UIView    *footerView;
@property (nonatomic, strong) UIButton  *applyBtn;
@property (nonatomic, strong) UIButton  *saveBtn;

@property (nonatomic, strong)  UIView  *cover;
@property (nonatomic, strong) chooseSignSetBank *chooseBankView;
@property (nonatomic, strong) ChooseLocationView *chooseLocationView;

//省市区ID
@property (nonatomic, assign) int provinceId;
@property (nonatomic, assign) int cityId;
//银行id
@property (nonatomic, assign) int bankId;
@property (nonatomic, copy) NSString *bankName;
@property (nonatomic, copy) NSString *address;


@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *IDnumber;
@property (copy, nonatomic) NSString *BankNumber;

@property (nonatomic, strong) SignInfoModel *signModel;


@end

@implementation YY_MineSignViewController

-(void)viewWillAppear:(BOOL)animated{
    [self getSignInfo];
}

- (UIView *)cover{
    if (!_cover) {
        _cover = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _cover.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        @weakify(self);
        [_cover addSubview:self.chooseBankView];
        _chooseBankView.chooseFinish = ^(BankModel *chooseBank) {
            @strongify(self);
        [UIView animateWithDuration:0.25 animations:^{
            self.bankId = chooseBank.bankId;
            self.bankName = chooseBank.name;
            [UIView animateWithDuration:0.25 animations:^{
                self.bankLabel.text = chooseBank.name;
                self.bankLabel.textAlignment = NSTextAlignmentLeft;
                self.view.transform = CGAffineTransformIdentity;
                self.cover.hidden = YES;
            }];
            }];
        };
        
        [_cover addSubview:self.chooseLocationView];
        __weak typeof (self) weakSelf = self;
        _chooseLocationView.chooseFinish = ^(NSString *address, NSMutableArray *idArr)
        {
            weakSelf.provinceId = [idArr[0] intValue];
            weakSelf.cityId = [idArr[1] intValue];
       
            [UIView animateWithDuration:0.25 animations:^{
                weakSelf.addressLabel.text = address;
                weakSelf.addressLabel.textAlignment = NSTextAlignmentLeft;
                weakSelf.view.transform = CGAffineTransformIdentity;
                weakSelf.cover.hidden = YES;
            }];
        };
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapCover:)];
        [_cover addGestureRecognizer:tap];
        tap.delegate = self;
        _cover.hidden = YES;
    }
    return _cover;
}



- (void)tapCover:(UITapGestureRecognizer *)tap{
    _cover.hidden = YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [gestureRecognizer locationInView:gestureRecognizer.view];
    if (CGRectContainsPoint(_chooseBankView.frame, point)){
        return NO;
    }
    return YES;
}

- (ChooseLocationView *)chooseLocationView{
    if (!_chooseLocationView) {
        @weakify(self);
        _chooseLocationView = [[ChooseLocationView alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 350, [UIScreen mainScreen].bounds.size.width, 350)];
        _chooseLocationView.signChoose = YES;
        _chooseLocationView.close = ^{
            @strongify(self);
            self.cover.hidden = YES;
        };
    }
    return _chooseLocationView;
}


-(chooseSignSetBank *)chooseBankView{
    if (!_chooseBankView) {
        @weakify(self);
        _chooseBankView = [[chooseSignSetBank alloc]initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - 370 -kSafeBottom, [UIScreen mainScreen].bounds.size.width, 370+kSafeBottom)];
        _chooseBankView.close = ^{
            @strongify(self);
            self.cover.hidden = YES;
        };
    }
    return _chooseBankView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"认证签约"];
    [self xh_popTopRootViewController:NO];
    [self creatFooter];
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.allowsSelection = YES;
    
    Class currentCls = [OrderCommonCell class];
    registerClass(self.m_tableView, currentCls);

    [self.view addSubview:self.m_tableView];
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    self.cellTitles = [@[@"真实姓名",@"身份证号",@
                         "选择银行",@"银行省市",@"银行账户"] mutableCopy];
    self.placeHolderArr = [@[@"请输入姓名",@"请输入身份证号",@"",@"",@"请输入银行卡账号"] mutableCopy];
    self.m_tableView.tableFooterView = self.footerView;
    // Do any additional setup after loading the view.
    [self.view addSubview:self.cover];
}

-(void)creatFooter{
    @weakify(self);
    self.footerView = [YYCreateTools     createView:XHClearColor];
    self.footerView.frame = CGRectMake(0, 0, kScreenW, 385);
    
    self.applyBtn = [YYCreateTools createBtn:@"完成认证" font:normalFont(17) textColor:XHWhiteColor];
    [self.footerView addSubview:self.applyBtn];
  
    [self.applyBtn setBackgroundImage:[UIImage imageWithColor:XHLoginColor] forState:UIControlStateNormal];
    [self.applyBtn setBackgroundImage:[UIImage imageWithColor:XHGrayColor] forState:UIControlStateDisabled];
    self.applyBtn.enabled = NO;
    self.applyBtn.v_cornerRadius = 4;
    
    [self.applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(50);
    }];
    self.applyBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        
        [self certifyUser];
    };
    
    self.saveBtn = [YYCreateTools createBtn:@"已签约" font:normalFont(15) textColor:XHBlackLitColor];
    [self.footerView addSubview:self.saveBtn];
    
    [self.saveBtn setBackgroundImage:[UIImage imageWithColor:XHLightColor] forState:UIControlStateNormal];
    self.saveBtn.hidden = YES;
    
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.applyBtn.mas_bottom).mas_offset(25);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.height.mas_equalTo(50);
    }];

    if (kUserInfo.contractFlag == 2) {
        [self.applyBtn setTitle:@"保存" forState:UIControlStateNormal];
        self.saveBtn.hidden = NO;
    }
}

-(void)certifyUser{
    BOOL IDjudge = [YYCommonTools isValidWithIdentityNum:self.IDnumber];
    if (IDjudge == NO) {
        [YYCommonTools showTipMessage:@"身份证号码格式不正确，请重新确认"];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.name forKey:@"name"];
    [dict setObject:@(self.bankId) forKey:@"bankId"];
    [dict setObject:@(self.provinceId) forKey:@"proviceId"];
    [dict setObject:@(self.cityId) forKey:@"cityId"];
    [dict setObject:self.IDnumber forKey:@"idcardNo"];
    [dict setObject:self.BankNumber forKey:@"accountNo"];
    [dict setObject:self.bankName forKey:@"bankName"];
    
    [YYCenterLoading showCenterLoading];
    SignCertifyRequestAPI *api = [[SignCertifyRequestAPI alloc] initCertifyRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            int flag = [[responDict objectForKey:@"contractFlag"] intValue];
            if (flag == 2) {
                [YYCommonTools  showTipMessage:@"修改成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                
                NSString *url = [responDict objectForKey:@"contractUrl"];
                if (url) {
                    NSMutableDictionary *params = [NSMutableDictionary dictionary];
                    [params setObject:@(3) forKey:@"linkType"];
                    [params setObject:[responDict objectForKey:@"contractUrl"] forKey:@"url"];
                    [params setObject:@(YES) forKey:@"backRoot"];
                    [YYCommonTools skipMultiCombinePage:self
                                                 params:params];
                }
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
    [self.m_tableView reloadData];
}


-(void)getSignInfo{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [YYCenterLoading showCenterLoading];
    SignCertifyInfoRequestAPI *api = [[SignCertifyInfoRequestAPI alloc] initCertifyInfoRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            SignInfoModel *model =[SignInfoModel modelWithJSON:responDict];
            self.signModel = model;
            self.name = model.name;
            self.IDnumber = model.idcardNo;
            self.BankNumber = model.accountNo;
            self.bankName = model.bankName;
            self.address = [NSString stringWithFormat:@"%@ %@",model.provice,model.city];
            self.provinceId = model.proviceId;
            self.cityId = model.cityId;
            self.bankId = model.bankId;
            
            if (self.provinceId) {
            NSMutableArray *codeArr = [NSMutableArray arrayWithObjects:@(self.provinceId),@(self.cityId), nil];
            NSMutableArray *titleArr = [NSMutableArray arrayWithObjects:self.signModel.provice,self.signModel.city, nil];
            self.chooseLocationView.codeArr = codeArr;
            self.chooseLocationView.titleArr = titleArr;
            }
            
            if (model.contractFlag == 2) {
                [self.applyBtn setTitle:@"保存" forState:UIControlStateNormal];
                self.saveBtn.hidden = NO;
            }
            
            [self.m_tableView reloadData];
            [self textValueChanged];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark -UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == 0? self.cellTitles.count : 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    Class currentCls = [OrderCommonCell class];
    OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    cell.titleLabel.text = self.cellTitles[indexPath.row];
    cell.titleLabel.textColor = XHBlackLitColor;
    if (row<2||row==4) {
        cell.type = CellwithOneTextFild;
        cell.showTextField.placeholder =self.placeHolderArr[indexPath.row];
        cell.showTextField.tag = row;
        switch (row) {
            case 0:
                cell.showTextField.text = self.name;
                break;
            case 1:{
                if (kValidString(self.IDnumber)) {
                    cell.showTextField.text = [self.IDnumber stringByReplacingCharactersInRange:NSMakeRange(3, self.IDnumber.length-7) withString:@"****"];
                }
            }
                break;
            case 4:{
                if (kValidString(self.BankNumber)) {
                    cell.showTextField.text = [self.BankNumber stringByReplacingCharactersInRange:NSMakeRange(3, self.BankNumber.length-7) withString:@"****"];
                }
            }
                 break;
                default:
                    break;
        }
        [cell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }else{
        cell.type = CellWithTwoLabel;
        if (row == 3) {
            if (!self.addressLabel) {
                self.addressLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
                [cell addSubview:self.addressLabel];
            }
            self.addressLabel.text = self.address?:@"请选择";
            [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(cell);
                make.left.mas_equalTo(kSizeScale(95));
                make.right.mas_equalTo(kSizeScale(-40));
            }];
            if ([self.addressLabel.text isEqualToString:@"请选择"]) {
                self.addressLabel.textAlignment = NSTextAlignmentRight;
            }else{
                self.addressLabel.textAlignment = NSTextAlignmentLeft;
            }
        }
        if (row==2){
            if (!self.bankLabel) {
                self.bankLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
                [cell addSubview:self.bankLabel];
            }
            self.bankLabel.text = self.bankName?:@"请选择";
            [self.bankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(cell);
                make.left.mas_equalTo(kSizeScale(95));
                make.right.mas_equalTo(kSizeScale(-40));
            }];
            if ([self.bankLabel.text isEqualToString:@"请选择"]) {
                self.bankLabel.textAlignment = NSTextAlignmentRight;
            }else{
                self.bankLabel.textAlignment = NSTextAlignmentLeft;
            }
        }
        UIImageView *arrowImageView = [YYCreateTools createImageView:@"cellMore_icon"
                                                           viewModel:-1];
        [cell addSubview:arrowImageView];
        [arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(kSizeScale(5),kSizeScale(9)));
            make.centerY.equalTo(cell);
            make.right.mas_equalTo(kSizeScale(-16));
        }];
    }
    cell.titleLabel.font = normalFont(15);
    if (kUserInfo.contractFlag == 2) {
        if (indexPath.row<2) {
            cell.showTextField.textColor = XHBlackLitColor;
            cell.showTextField.enabled = NO;
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(46);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2 ) {
        [self getBankList];
    }else if (indexPath.row == 3 ){
        [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
        [UIView animateWithDuration:0.25 animations:^{
        }];
        self.cover.hidden = !self.cover.hidden;
        self.chooseBankView.hidden = YES;
        self.chooseLocationView.hidden = self.cover.hidden;
    }
}

#pragma mark - UITextField delegate method
- (void)textValueChanged {
    self.applyBtn.enabled = (self.name.length >= 1 &&self.IDnumber.length ==18 && self.BankNumber.length>15&&self.bankId&&self.provinceId);
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (textField.tag == 0) {
        NSInteger kMaxLength = 12;
        NSString *toBeString = textField.text;
        NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage; //ios7之前使用[UITextInputMode currentInputMode].primaryLanguage
        if ([lang isEqualToString:@"zh-Hans"]) { //中文输入
            UITextRange *selectedRange = [textField markedTextRange];
            //获取高亮部分
            UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
            if (!position) {// 没有高亮选择的字，则对已输入的文字进行字数统计和限制
                if (toBeString.length > kMaxLength) {
                    textField.text = [toBeString substringToIndex:kMaxLength];
                }
            }
        }else {//中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
            }
        }
        self.name = textField.text;
        NSLog(@"________%@",self.name);
    }else if (textField.tag == 1){
        NSInteger kMaxLength = 18;
        NSString *toBeString = textField.text;
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
        self.IDnumber = textField.text;
    }else{
        NSInteger kMaxLength = 19;
        NSString *toBeString = textField.text;
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
        }
        self.BankNumber = textField.text;
    }
    [self textValueChanged];
}

#pragma mark requestAPI
-(void)getBankList
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [YYCenterLoading showCenterLoading];
    SignBankRequestAPI *api = [[SignBankRequestAPI alloc] initBankInfoRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
            [UIView animateWithDuration:0.25 animations:^{
            }];
            self.cover.hidden = !self.cover.hidden;
            self.chooseBankView.hidden = self.cover.hidden;
            self.chooseBankView.BankList = [[NSArray modelArrayWithClass:[BankModel class] json: [responDict objectForKey:@"items"]]mutableCopy];
            self.chooseLocationView.hidden = YES;
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
