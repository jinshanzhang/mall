//
//  YY_MyViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YY_MyViewController.h"

#import "MineItemContentInfoModel.h"

#import "MineBasicItemInfoCell.h"
#import "MineBasicBottomInfoCell.h"
#import "MineUserAndOrderInfoCell.h"

#import "MchListViewController.h"
#import "VirtualOrderDetailViewController.h"
#import "VirtualOrderListViewController.h"

@interface YY_MyViewController ()

@property (nonatomic, strong) UIView       *ctrlBackgroundView;
@property (nonatomic, strong) UIImageView  *headColorBackgroundView;
@property (nonatomic, strong) UIImageView  *headBackgroundView;

@property (nonatomic, assign) CGFloat      oldOffsetY;
@property (nonatomic, assign) CGRect       colorOriginRect;
@property (nonatomic, assign) CGRect       originHeadRect;

@property (nonatomic, strong) NSMutableArray *redCounts;
@property (nonatomic, strong) NSMutableArray *itemModels;

@end

@implementation YY_MyViewController

#pragma mark - Setter method
- (UIView *)ctrlBackgroundView {
    if (!_ctrlBackgroundView) {
        _ctrlBackgroundView = [YYCreateTools createView:XHStoreGrayColor];
        _ctrlBackgroundView.frame = CGRectMake(0, 0, kScreenW, kScreenH);
    }
    return _ctrlBackgroundView;
}

- (UIImageView *)headColorBackgroundView {
    if (!_headColorBackgroundView) {
//        _headColorBackgroundView = [YYCreateTools createImageView:@"mine_color_backgroun_icon"
//                                                        viewModel:-1];
        _headColorBackgroundView = [YYCreateTools createImageView:@"mineHeaderBack"
        viewModel:-1];
//        _headColorBackgroundView.backgroundColor = XHWhiteColor;
//        _headColorBackgroundView.backgroundColor = [UIColor blueColor];
        self.colorOriginRect = CGRectMake(0, 0, kScreenW, kSizeScale(kNavigationH+136));
    }
    return _headColorBackgroundView;
}

- (UIImageView *)headBackgroundView {
    if (!_headBackgroundView) {
        _headBackgroundView = [YYCreateTools createImageView:@"mine_heder_icon"
                                                   viewModel:-1];
        self.originHeadRect = CGRectMake(0, 0, kScreenW,  kSizeScale(kNavigationH+136));
        _headBackgroundView.frame = self.originHeadRect;
    }
    return _headBackgroundView;
}

#pragma mark - Life cycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(!kIsLogin) {
        [YYCommonTools pushToLogin];
        return;
    }
    if (self.m_tableView) {
        [self loadPullDownPage];
    }
}

- (instancetype)init {
    self = [super initWithTableView];
    if (self) {
        self.isAddPullUpRefresh = NO;
        self.isCustomView = YES;
        self.isSubReload = YES;
        self.tableStyle = UITableViewStylePlain;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    [MobClick event:@"Mine"];
    [self xh_addTitle:@"我的"];
    [self xh_alphaNavigation:0];
    [self xh_navBackgroundColor:XHMainColor];
    [self xh_navBottomLine:XHClearColor];
    [self xh_navTitleColor:XHWhiteColor];
    [self setNeedsStatusBarAppearanceUpdate];
    [self xh_addNavigationItemWithImageName:@"mine_setting_icon"
                                     isLeft:NO
                                 clickEvent:^(UIButton *sender) {
                                     @strongify(self);
                                     [YYCommonTools skipMineSet:self
                                                   isHideTabbar:YES];
                                 }];
    
    [self.view addSubview:self.ctrlBackgroundView];
    [self.view addSubview:self.headColorBackgroundView];
    [self.view addSubview:self.headBackgroundView];
    
    [self.view sendSubviewToBack:self.headBackgroundView];
    [self.view sendSubviewToBack:self.headColorBackgroundView];
    [self.view sendSubviewToBack:self.ctrlBackgroundView];
    
    Class currentCls = [MineBasicItemInfoCell class];
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    registerClass(self.m_tableView, currentCls);
    currentCls = [MineUserAndOrderInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [MineBasicBottomInfoCell class];
    registerClass(self.m_tableView, currentCls);

    self.m_tableView.backgroundColor = XHClearColor;
    
    [self.view insertSubview:self.navigationBar aboveSubview:self.m_tableView];
    
    self.itemModels = [NSMutableArray array];
    for (int i = 1; i < 4; i ++) {
        NSString *title = nil, *imageName = nil;
        MineItemContentInfoModel *item = [[MineItemContentInfoModel alloc] init];
        item.type = i;
        switch (i) {
            case 1:{
                title = @"优惠券"; imageName = @"mine_coupon_icon";
            }
                break;
//            case 2: {
//                title = @"我的课程"; imageName = @"mine_course_icon";
//            }
//                break;
            case 2: {
                title = @"收货地址"; imageName = @"mine_address_icon";
            }
                break;
            case 3: {
                title = @"联系客服"; imageName = @"mine_link_icon";
            }
                break;
//            case 5: {
//                title = @"常见问题"; imageName = @"mine_problem_icon";
//            }
//                break;
            default:
                break;
        }
        item.title = title; item.imageName = imageName;
        if (!(i == 2 && kIsTest)) {
            [self.itemModels addObject:item];
        }
    }
    [self showDataSourceOperator];
    [self loadPullDownPage];
    // Do any additional setup after loading the view.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Base Request

- (YYBaseRequestAPI *)baseRequest {
    self.redCounts = [NSMutableArray array];
    [kWholeConfig gainUserCenterInfo:^(BOOL isSucess) {
        if (isSucess) {
            if (kUserInfo) {
                [self.redCounts addObject:@{@"cnt":@(kUserInfo.orderWaitToPayCnt)}];
                [self.redCounts addObject:@{@"cnt":@(kUserInfo.orderWaitToReceiptCnt)}];
                [self.redCounts addObject:@{@"cnt":@(kUserInfo.orderFinishCnt)}];
                [self showDataSourceOperator];
            }
            [self.m_tableView reloadData];
        }
    } isLoading:NO];
    return [YYBaseRequestAPI new];
}

- (NSArray *)parseResponce:(NSDictionary *)responseDic {
    [self.m_tableView.mj_header endRefreshing];
    return nil;
}

#pragma mark - Private method
// 对展示的数据源进行操作
- (void)showDataSourceOperator {
//    __block BOOL isExistBlance = NO;
    [self.itemModels enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MineItemContentInfoModel *model = (MineItemContentInfoModel *)obj;
//        if (model.type == 0) {
//            //存在
//            model.content = kValidString(kUserInfo.balanceIncome)?[NSString stringWithFormat:@"¥%@",kUserInfo.balanceIncome]:@"";
//            isExistBlance = YES;
//        }
        if (model.type == 1) {
            model.content = kValidString(kUserInfo.couponCountText)?kUserInfo.couponCountText:@"";
            model.imageUrl = kUserInfo.couponExpireSoonUserImg;
        }
    }];
//    if (!isExistBlance && kIsMember) {
//        MineItemContentInfoModel *model = [[MineItemContentInfoModel alloc] init];
//        model.type = 0;
//        model.title = @"余额";
//        model.imageName = @"mine_balance_icon";
//        model.content = kValidString(kUserInfo.balanceIncome)?[NSString stringWithFormat:@"¥%@",kUserInfo.balanceIncome]:@"";
//        [self.itemModels insertObject:model atIndex:0];
//    }
}

- (void)cellClickOperatorHander:(MineItemContentInfoModel *)model {
//    MchListViewController *locationVC = [[MchListViewController alloc] init];
//    locationVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:locationVC animated:YES];
    
    
//    MainViewController *locationVC = [[MainViewController alloc] init];
//    locationVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:locationVC animated:YES];
//    return;
    
    NSInteger type = model.type;
    switch (type) {
//        case 0:
//        {
//            //余额
//            [YYCommonTools skipBalance:self params:nil];
//        }
//            break;
        case 1:
        {
//            VirtualOrderDetailViewController *vc = [[VirtualOrderDetailViewController alloc] init];
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
//            return;
            
//            MchListViewController *locationVC = [[MchListViewController alloc] init];
//            locationVC.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:locationVC animated:YES];
//            return;
//            
            // 将优惠券过期状态置为已经弹过。
            if (kUserInfo.isAllowMineExpireTip) {
                NSMutableDictionary *params = [YYCommonTools objectTurnToDictionary:NO];
                [params setObject:@(NO) forKey:@"isAllowMineExpireTip"];
                [params setObject:[[NSString alloc] getCurrentTimestamp] forKey:@"userImgOvertime"];
                [kUserManager modifyUserInfo:params];
            }
            [YYCommonTools skipCouponPage:self];
        }
            break;
//        case 2:
//        {
//            [YYCommonTools skipMyCourceList:self];
//        }
//            break;
        case 2:
        {
            [YYCommonTools skipAddressList:self];
        }
            break;
        case 3:
        {
//            VirtualOrderListViewController *vc = [[VirtualOrderListViewController alloc] init];
//            vc.parameter = @{@"source": @(1)};
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
//            return;
//            [YYCommonTools skipKefu:self params:nil];
            NSMutableString *str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"4000568110"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
        }
            break;
//        case 5:
//        {
//            NSMutableDictionary *params = [NSMutableDictionary dictionary];
//            [params setObject:@(3) forKey:@"linkType"];
//            [params setObject:QustionWebSite forKey:@"url"];
//            [YYCommonTools skipMultiCombinePage:self
//                                         params:params];
//
//        }
//            break;
            
        default:
            break;
    }
    
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    @weakify(self);
    NSInteger row = indexPath.row;
    Class currentCls = [MineBasicItemInfoCell class];
    if (row == 0) {
        currentCls = [MineUserAndOrderInfoCell class];
        MineUserAndOrderInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        [cell setMineOrder:[@[@"mine_stay_pay_icon",@"mine_receive_icon",@"mine_finish_icon",@"mine_after_sale_icon",@"icon_wod_tab_zit"] mutableCopy]
                    titles:[@[@"待付款",@"待收货",@"已完成",@"售后管理",@"自提"] mutableCopy]
                 redCounts:self.redCounts topSpace:-kSizeScale(kSizeScale(8)) itemSpace:kSizeScale(5)];
        cell.orderClickBlock = ^(NSInteger type) {
            if (type == 0) {
                @strongify(self);
                [YYCommonTools skipOrderList:self params:@{@"source": @(1)}];
            }
            else if (type == 4){
                [YYCommonTools skipMyAfterSalePage:self params:@{@"type": @(0)}];
            }
            else {
                [YYCommonTools skipOrderList:self params:@{@"source": @(1),
                                                           @"currentPage":@(type)}];
            }
        };
        return cell;
    }
    else {
        MineBasicItemInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        cell.itemArray = self.itemModels;
        cell.cellViewClickBlock = ^(id  _Nonnull itemModel) {
            @strongify(self);
            [self cellClickOperatorHander:itemModel];
        };
        return cell;
    }
    /*else if (row == 1) {
        
    }
    else {
        currentCls = [MineBasicBottomInfoCell class];
        MineBasicBottomInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        return cell;
    }*/
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    if (row == 0) {
//        return kSizeScale(232) + kSizeScale(50);
        return kSizeScale(213 + 39);
    }
    else {
        NSInteger count = (kIsTest ? 4 : 3);
        if (kValidArray(self.itemModels)) {
            MineItemContentInfoModel *firstModel = [self.itemModels firstObject];
            if (firstModel.type == 0) {
                count = count + 1;
            }
        }
        return kSizeScale(12) + count * kSizeScale(48);
    }
    /*else if (row == 1) {
        
    }
    else {
        return kSizeScale(150);
    }*/
    return UITableViewAutomaticDimension;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat alpha = 0.0;
    CGFloat tempValue = kSizeScale(70);
    CGFloat offsetY = scrollView.contentOffset.y;
    CGRect headerFrame = self.originHeadRect;
    CGRect colorHeaderFrame = self.colorOriginRect;
    if (offsetY > self.oldOffsetY) {
        // 上滑
        headerFrame.origin.y += (offsetY <= 0 ? offsetY : -offsetY);
        colorHeaderFrame.size.height += (offsetY <= 0 ? offsetY : -offsetY);
    }
    else{
        // 下滑
        headerFrame.origin.y += -offsetY;
        colorHeaderFrame.size.height += -offsetY;
    }
    if (offsetY > tempValue) {
        alpha = 1.0;
    }
    else {
        alpha = offsetY/tempValue;
    }
    [self xh_alphaNavigation:alpha];
    self.headBackgroundView.frame = headerFrame;
    self.headColorBackgroundView.frame = colorHeaderFrame;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    // 获取开始拖拽时tableview偏移量
    self.oldOffsetY = self.m_tableView.contentOffset.y;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
