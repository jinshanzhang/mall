//
//  YY_PaymentPasswordViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_PaymentPasswordViewController.h"

#import "OrderCommonCell.h"
#import "YY_ChangePayPasswordViewController.h"

#import "AccountIdentifyAPI.h"
#import "GetphoneNumberAPI.h"
#import "AccountSendIdentifyAPI.h"

@interface YY_PaymentPasswordViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, strong) UIButton *submitBtn;

@property (nonatomic, strong) UIButton *codeBtn;

@property (nonatomic, copy)   NSString *phone;
@property (nonatomic, copy)   NSString *code;


@end

@implementation YY_PaymentPasswordViewController
- (UIButton *)codeBtn
{
    if (!_codeBtn)
    {
        _codeBtn = [UIButton new];
        _codeBtn.frame = CGRectMake(kScreenW - 110, 0, kSizeScale(80), 46);
        [_codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        _codeBtn.titleLabel.font = normalFont(15);
        _codeBtn.layer.cornerRadius = 5;
        [_codeBtn setTitleColor:XHRedColor forState:UIControlStateNormal];
        [_codeBtn setTitleColor:XHBlackColor forState:UIControlStateDisabled];
        [_codeBtn addTarget:self action:@selector(vCodeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _codeBtn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_addTitle:kUserInfo.payAccountPasswordFlag?@"修改支付密码":@"设置支付密码"];
    [self xh_popTopRootViewController:NO];
    [self creatFooterView];

    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.estimatedRowHeight = 50;
    self.m_tableView.allowsSelection = YES;
    self.m_tableView.scrollEnabled = NO;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    Class common = [OrderCommonCell class];
    registerClass(self.m_tableView, common);
    self.m_tableView.tableFooterView = self.footerView;
    
    [self getPhoneNumber];
    // Do any additional setup after l oading the view.
}

-(void)creatFooterView{
    @weakify(self);
    self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(150))];
    self.submitBtn =[YYCreateTools createBtn:@"提交" font:boldFont(16) textColor:XHWhiteColor];
    self.submitBtn.v_cornerRadius = 4;
    self.submitBtn.enabled = NO;
    [self.footerView addSubview:self.submitBtn];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(30);
        make.right.mas_equalTo(-30).priorityHigh();
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(45);
    }];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:XHRedColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:XHLightRedColor] forState:UIControlStateDisabled];
    
    _submitBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self submitCode];
    };
}

#pragma mark -UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class common = [OrderCommonCell class];
    OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    cell.type = CellwithOneTextFild;
    if (indexPath.row == 0) {
        cell.showTextField.enabled = NO;
        cell.showTextField.text = [_phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }else{
        [cell addSubview:self.codeBtn];
        [cell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    cell.titleLabel.text = indexPath.row == 0?@"手机号":@"验证码";
    cell.showTextField.placeholder = indexPath.row==1?@"请输入验证码":@"";
    cell.showTextField.delegate = self;
    cell.showTextField.tag = indexPath.row;
    
    return cell;
}


-(void)vCodeClick
{
    [self sendCodeRequest];
}

#pragma mark request
-(void)submitCode{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.phone forKey:@"phone"];
    [dict setObject:self.code forKey:@"code"];

    AccountIdentifyAPI *API = [[AccountIdentifyAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
        YY_ChangePayPasswordViewController *vc = [YY_ChangePayPasswordViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        }
    }failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}

-(void)getPhoneNumber{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    GetphoneNumberAPI *sendCodeAPI = [[GetphoneNumberAPI alloc] initRequest:dict];
    [sendCodeAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                self.phone = [responDict objectForKey:@"phone"];
                [self.m_tableView reloadData];
            }
     }failure:^(__kindof YTKBaseRequest * _Nonnull request) {
         
    }];
}

- (void)sendCodeRequest
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.phone forKey:@"phone"];
    
    AccountSendIdentifyAPI *sendCodeAPI = [[AccountSendIdentifyAPI alloc] initRequest:dict];
    [sendCodeAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [YYCommonTools  sendCodeMethod:self.phone
                                   codeBtn:self.codeBtn
                                phoneBlock:^(NSString *phone) {
                                }];
            [YYCommonTools  showTipMessage:@"验证码已发送"];
        }
    }failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
}
#pragma mark - UITextField delegate method

- (void)textFieldDidChange:(UITextField *)textField {
    self.code = textField.text;
    self.submitBtn.enabled = (self.code.length >0);

}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
