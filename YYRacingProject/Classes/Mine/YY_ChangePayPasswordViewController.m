//
//  YY_ChangePayPasswordViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/10/22.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_ChangePayPasswordViewController.h"

#import "PayPasswordUpdateAPI.h"

#import "OrderCommonCell.h"

@interface YY_ChangePayPasswordViewController ()

@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, strong) UILabel *showLabel;

@property (nonatomic, strong) UIButton *submitBtn;

@property (nonatomic, strong) UIButton *hideBtn;

@property (nonatomic, strong) UITextField *passwordTextfield;
@property (nonatomic, copy)   NSString *originPassword;
@property (nonatomic, copy)   NSString *checkPassword;

@end

@implementation YY_ChangePayPasswordViewController


- (UIButton *)hideBtn
{
    if (!_hideBtn)
    {
        _hideBtn = [UIButton new];
        _hideBtn.frame = CGRectMake(kScreenW - 60, 0, kSizeScale(80), 46);
        [_hideBtn setImage:[UIImage imageNamed:@"order_hide_earnmoney_icon"] forState:UIControlStateNormal];
        [_hideBtn setImage:[UIImage imageNamed:@"order_show_earnmoney_icon"] forState:UIControlStateSelected];
        [_hideBtn addTarget:self action:@selector(textHide) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hideBtn;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"设置支付密码"];
    [self xh_popTopRootViewController:NO];
    
    [self creatFooterView];

    self.m_tableView.backgroundColor = XHLightColor;
    self.m_tableView.estimatedRowHeight = 50;
    self.m_tableView.allowsSelection = YES;
    self.m_tableView.scrollEnabled = NO;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    Class common = [OrderCommonCell class];
    registerClass(self.m_tableView, common);
    self.m_tableView.tableFooterView = self.footerView;
    // Do any additional setup after loading the view.
}

-(void)creatFooterView{
    @weakify(self);
    self.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(150))];
    self.submitBtn =[YYCreateTools createBtn:@"确认" font:boldFont(16) textColor:XHWhiteColor];
    self.submitBtn.v_cornerRadius = 4;
    self.submitBtn.enabled = NO;
    [self.footerView addSubview:self.submitBtn];
    
    self.showLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHRedColor];
    [self.footerView addSubview:self.showLabel];
    
    [self.showLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(10);
    }];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.right.mas_equalTo(-15).priorityHigh();
        make.top.mas_equalTo(50);
        make.height.mas_equalTo(45);
    }];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:XHRedColor] forState:UIControlStateNormal];
    [self.submitBtn setBackgroundImage:[UIImage imageWithColor:XHLightRedColor] forState:UIControlStateDisabled];
    
    _submitBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self submitPassword];
    };
}

#pragma mark -UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class common = [OrderCommonCell class];
    OrderCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    cell.type = CellwithOneTextFild;
 
    cell.titleLabel.text = indexPath.row == 0?@"新支付密码":@"确认新密码";
    cell.showTextField.placeholder = @"请输入6位数字密码";
    cell.showTextField.keyboardType = UIKeyboardTypePhonePad;
    if (indexPath.row == 0) {
        cell.showTextField.secureTextEntry = YES;
        self.passwordTextfield = cell.showTextField;
        [cell addSubview:self.hideBtn];
    }else{
        cell.showTextField.secureTextEntry = YES;
        cell.showTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    cell.showTextField.tag = indexPath.row;
    [cell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    
    return cell;
}

-(void)textHide{
    self.hideBtn.selected = !self.hideBtn.selected;
    self.passwordTextfield.secureTextEntry =!self.hideBtn.selected;
}

#pragma mark - UITextField delegate method

- (void)textFieldDidChange:(UITextField *)textField {
    NSInteger kMaxLength = 6;
    NSString *toBeString = textField.text;
    if (toBeString.length > kMaxLength) {
        textField.text = [toBeString substringToIndex:kMaxLength];
    }
    if (textField.tag == 0) {
        self.originPassword = textField.text;
    }else{
        self.checkPassword = textField.text;
    }
    [self changeFooterView];
}

-(void)changeFooterView{
    if (![self.originPassword isEqualToString:self.checkPassword]&& (self.checkPassword.length == 6)&&(self.checkPassword.length == self.originPassword.length)){
        self.showLabel.text = @"两次输入密码不一致";
    }else{
        self.showLabel.text = @"";
    }
    self.submitBtn.enabled = (self.originPassword.length==self.checkPassword.length && self.originPassword.length == 6);
}

-(void)submitPassword {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[self.originPassword encode:self.originPassword] forKey:@"password"];
    [dict setObject:[self.checkPassword encode:self.checkPassword] forKey:@"passwordTwo"];
    
    PayPasswordUpdateAPI *API = [[PayPasswordUpdateAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSMutableDictionary *params = [YYCommonTools objectTurnToDictionary:NO];
            [params setObject:@(YES) forKey:@"payAccountPasswordFlag"];
            [kUserManager modifyUserInfo:params];
            int index = (int)[[self.navigationController viewControllers]indexOfObject:self];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:index-2] animated:YES];
        }
    }failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
