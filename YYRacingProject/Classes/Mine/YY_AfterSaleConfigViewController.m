//
//  YY_AfterSaleConfigViewController.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_AfterSaleConfigViewController.h"
#import "AfterSaleConfigAPI.h"
#import "AfterSaleMoneyAPI.h"
#import "AfterSaleInsertAPi.h"
#import "YYUploadPhotoRequestAPI.h"
#import "MineAfterSaleApplyArbitrationInfoRequestAPI.h"
#import "AfterSaleChangeAPI.h"

#import "AfterSaleTypeConfigsModel.h"
#import "AfterSaleGoodStatusModel.h"
#import "AfterSaleReasonsModel.h"

#import "AfterSaleCommonCell.h"
#import "OrderAfterSaleCertificatePhotoInfoCell.h"
#import <BRStringPickerView.h>
@interface YY_AfterSaleConfigViewController ()<UITableViewDelegate,UITableViewDataSource,PhotosOperatorDelegate,
TZImagePickerControllerDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *submitBtn;
@property (nonatomic, strong) UITextView  *feedTextView;

@property (nonatomic, strong) AfterSaleTypeConfigsModel *ServiceTypeModel;
//服务类型
@property (nonatomic, strong) NSMutableArray *serviceTypeArr;

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *describeArr;

@property (nonatomic, assign) int subGoodStatusid;
@property (nonatomic, assign) int subReasonid;
@property (nonatomic, assign) int skuCnt;
@property (nonatomic, assign) int afterSaleID;


@property (nonatomic, copy) NSString *subGoodStatus;
@property (nonatomic, copy) NSString *subReason;
@property (nonatomic, copy) NSString *subMoney;

@property (nonatomic, copy) NSString *maxMoney;

@property (nonatomic, strong) NSMutableArray *allPhotos;

@end

@implementation YY_AfterSaleConfigViewController

-(UITableView *)tableView
{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] init];
        self.tableView.dataSource = self;
        self.tableView.delegate = self;
        self.tableView.backgroundColor = XHLightColor;
        self.tableView.allowsSelection = YES;
        self.tableView.estimatedRowHeight = 100;
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:self.Arbitration?@"申请仲裁":@"申请退款"];
    [self xh_popTopRootViewController:NO];
    [self creatFooterView];
    
    self.titleArr = [NSArray arrayWithObjects:@"服务类型",@"货物状态",@"售后原因",@"售后件数",@"退货金额",@"", nil];
    self.describeArr = [NSMutableArray arrayWithObjects:self.TypeDescribe?:@"请选择",self.subGoodStatus?:@"请选择",self.subReason?:@"请选择", nil];
    self.allPhotos = [NSMutableArray array];
    // Do any additional setup after loading the view.
 
    [self getData];
    
    [self.view addSubview:self.tableView];

    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigationH);
        make.left.right.equalTo(self.view);
        make.bottom.mas_equalTo(-kBottom(50));
    }];
    Class current = [AfterSaleCommonCell class];
    registerClass(self.tableView, current);
    Class photo = [OrderAfterSaleCertificatePhotoInfoCell class];
    registerClass(self.tableView, photo);
}

-(void)creatFooterView{
    self.submitBtn = [YYCreateTools createBtn:@"提交" font:normalFont(17) textColor:XHWhiteColor];
    
    [self.view addSubview:self.submitBtn];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kSafeBottom);
        make.height.mas_equalTo(kSizeScale(50));
    }];
    
//    self.submitBtn.backgroundColor = [UIColor addGoldGradient:CGSizeMake(kScreenW, kSizeScale(50))];
    self.submitBtn.backgroundColor =  XHMainColor;
    
    @weakify(self);
    self.submitBtn.actionBlock = ^(UIButton *sender) {
        @strongify(self);
        [self upPhoto];
    };
}

#pragma mark 网络请求
-(void)getOriginData{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.orderID) forKey:@"orderId"];
    [dict setObject:@(self.skuID) forKey:@"skuId"];
    [YYCenterLoading showCenterLoading];
    
    AfterSaleChangeAPI*api = [[AfterSaleChangeAPI alloc] initAfterSaleRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.afterSaleID = [[responDict objectForKey:@"afterSaleId"] intValue];
            self.submitType = [[responDict objectForKey:@"afterSaleType"] intValue];
            self.subGoodStatusid  = [[responDict objectForKey:@"goodStatus"] intValue];
            self.subReasonid  = [[responDict objectForKey:@"reasonId"] intValue];
            self.skuCnt =[[responDict objectForKey:@"skuCnt"] intValue];
            self.subMoney = [responDict objectForKey:@"fee"];
            self.feedTextView.text = [responDict objectForKey:@"describe"];
            
            NSArray *Arr = [responDict objectForKey:@"urls"];
            for (int i = 0; i < Arr.count; i++) {
                //不要连代理 会蹦
                UIImage *img = [UIImage getImageFromURL:Arr[i]];
                if (img) {
                    [self.allPhotos addObject:img];
                }
            }
            for (AfterSaleTypeConfigsModel *arr in self.serviceTypeArr) {
                if (self.submitType == arr.type) {
                    self.TypeDescribe = arr.describe;
                    for (AfterSaleGoodStatusModel *goodModel in arr.goodStatus) {
                        if (self.subGoodStatusid == goodModel.type) {
                            self.subGoodStatus = goodModel.describe;
                            for (AfterSaleReasonsModel *model  in goodModel.reasons) {
                                if (self.subReasonid == model.type) {
                                    self.subReason = model.describe;
                                    self.subReasonid = model.type;
                                }
                            }
                        }
                    }
                }
            }
            self.ServiceTypeModel = self.serviceTypeArr[self.submitType];
            self.describeArr = [NSMutableArray arrayWithObjects:self.TypeDescribe?:@"请选择",self.subGoodStatus?:@"请选择",self.subReason?:@"请选择", nil];
            [self getSaleMoney];
            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
    
}

-(void)getData{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.orderID) forKey:@"orderId"];
    [dict setObject:@(self.skuID) forKey:@"skuId"];
    [YYCenterLoading showCenterLoading];
    
    AfterSaleConfigAPI*api = [[AfterSaleConfigAPI alloc] initAfterSaleRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.serviceTypeArr = [NSMutableArray array];
            self.serviceTypeArr = [[NSMutableArray modelArrayWithClass:[AfterSaleTypeConfigsModel class] json:[responDict objectForKey:@"afterSaleTypeConfigs"]]mutableCopy];
            self.skuCnt = [[responDict objectForKey:@"skuCnt"] intValue];
            [self.tableView reloadData];

            if (self.ChangeConfig == YES && self.Arbitration == NO) {
                [self getOriginData];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

-(void)getSaleMoney{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.orderID) forKey:@"orderId"];
    [dict setObject:@(self.skuID) forKey:@"skuId"];
    [dict setObject:@(self.subReasonid) forKey:@"reasonId"];
    [dict setObject:@(self.skuCnt) forKey:@"skuCnt"];
    [YYCenterLoading showCenterLoading];
    
    AfterSaleMoneyAPI*api = [[AfterSaleMoneyAPI alloc] initAfterSaleRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            double money= [responDict[@"defaultFee"] doubleValue];
            NSString *dStr      = [NSString stringWithFormat:@"%f", money];
            NSDecimalNumber *dn = [NSDecimalNumber decimalNumberWithString:dStr];
            
            self.subMoney       = [dn stringValue];
            self.maxMoney       = self.subMoney;

            [self.tableView reloadData];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

-(void)upPhoto{
    [YYCenterLoading showCenterLoading];

    if (kValidArray(self.allPhotos)) {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.allPhotos forKey:@"photos"];
    YYUploadPhotoRequestAPI *uploadAPI = [[YYUploadPhotoRequestAPI alloc] initUploadPhotoInfoRequest:params];
    [uploadAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            NSMutableArray *imgurls = [NSMutableArray array];
            NSArray *imgs = [responDict objectForKey:@"imgs"];
            if (kValidArray(imgs)) {
                for (int i = 0; i < imgs.count; i ++) {
                    NSDictionary *imgDict = [imgs objectAtIndex:i];
                    if ([imgDict objectForKey:@"imageUrl"]) {
                        [imgurls addObject: [imgDict objectForKey:@"imageUrl"]];
                    }
                }
            }
            if (self.Arbitration == YES) {
                [self insertArbitration:imgurls];
            }else{
                [self insertAfterSale:imgurls];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
      }];
        
    }else {
        [YYCenterLoading hideCenterLoading];
        if (self.Arbitration == YES) {
            [self insertArbitration:[NSMutableArray array]];
        }else{
            [self insertAfterSale:[NSMutableArray array]];
        }
   }
}

-(void)insertArbitration:(NSMutableArray *)urlArr{
    /**申请仲裁**/
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:@(self.orderID) forKey:@"orderId"];
        [dict setObject:@(self.skuID) forKey:@"skuId"];
        [dict setObject:self.feedTextView.text forKey:@"describe"];
        [dict setObject:urlArr forKey:@"urls"];
        MineAfterSaleApplyArbitrationInfoRequestAPI *arbitrationAPI = [[MineAfterSaleApplyArbitrationInfoRequestAPI alloc] initAfterSaleApplyArbitrationInfoRequest:dict];
        [YYCenterLoading showCenterLoading];
        [arbitrationAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            [YYCenterLoading hideCenterLoading];
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                if (self.receiveBlock) {
                    self.receiveBlock(@"");
                }
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
}


-(void)insertAfterSale:(NSMutableArray *)urlArr{
    
    if (!self.subGoodStatus) {
        [YYCommonTools showTipMessage:@"请选择货物状态"];
        return;
    }
    if (!self.subReason) {
        [YYCommonTools showTipMessage:@"请选择售后原因"];
        return;
    }
    if (!self.subMoney&&self.submitType!=2) {
        [YYCommonTools showTipMessage:@"请输入退款金额"];
        return;
    }
    if (self.feedTextView.text.length<10) {
        [YYCommonTools showTipMessage:@"请输入不少于10个字的说明"];
        return;
    }

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(self.ChangeConfig == YES?self.afterSaleID:0) forKey:@"id"];
    [dict setObject:@(self.orderID) forKey:@"orderId"];
    [dict setObject:@(self.skuID) forKey:@"skuId"];
    [dict setObject:@(self.skuCnt) forKey:@"skuCnt"];
    [dict setObject:@(self.subReasonid) forKey:@"reasonId"];
    [dict setObject:@(self.submitType) forKey:@"afterSaleType"];
    [dict setObject:@(self.subGoodStatusid) forKey:@"goodStatus"];
    [dict setObject:@([self.subMoney doubleValue]) forKey:@"fee"];
    [dict setObject:self.feedTextView.text forKey:@"describe"];
    [dict setObject:urlArr forKey:@"urls"];
    
    AfterSaleInsertAPi*api = [[AfterSaleInsertAPi alloc] initAfterSaleRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            kPostNotification(RefreshOrderDetailNotification, nil);
            [YYCommonTools showTipMessage:@"提交成功"];
            
            if (self.FromChoose == YES) {
                //修改申请
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2]
                                                      animated:YES];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}


#pragma mark - UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 8;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 8)];
    headerView.backgroundColor = XHClearColor;
    return headerView;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 6;
    }else{
        return 1;
    }}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @weakify(self);
    if (indexPath.section == 0) {
    Class currentCls = [AfterSaleCommonCell class];
    AfterSaleCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
    }

       if (indexPath.row < 3) {
        cell.type = CellWithTwoLabelAndArrow;
        cell.detailLabel.text =self.describeArr[indexPath.row];
        if ([cell.detailLabel.text isEqualToString:@"请选择"]) {
            cell.detailLabel.textAlignment = NSTextAlignmentRight;
            }else{
            cell.detailLabel.textAlignment = NSTextAlignmentLeft;
            }
           cell.hidden = self.Arbitration;
        }
        if (indexPath.row == 3) {
            cell.type = CellWithNumberView;
            cell.numberView.maxValue = self.skuCnt;
            cell.numberView.currentValue = self.skuCnt;
            cell.hidden = self.Arbitration;

            cell.cellNumerBlock = ^(NSInteger number) {
            @strongify(self);
               self.skuCnt = (int)number;
               if (self.subReason) {
                 [self getSaleMoney];
               }
           };
    }
    if (indexPath.row == 4) {
        cell.type = CellwithOneTextFild;
        cell.showTextField.text = self.subMoney;
        cell.showTextField.font = normalFont(14);
        cell.showTextField.keyboardType = UIKeyboardTypeDecimalPad;
        cell.hidden = self.Arbitration;
        [cell.showTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        cell.hidden = self.submitType == 2||self.Arbitration?YES:NO;
    }
    if (indexPath.row == 5) {
        cell.type = CellWithTextView;
        if (self.feedTextView) {
            [cell textViewDidChange:self.feedTextView];
        }
        self.feedTextView = cell.showTextView;
    }
    cell.titleLabel.text = self.titleArr[indexPath.row];
    cell.titleLabel.textColor = XHBlackLitColor;
    return cell;
    }else{
        Class cls = [UITableViewCell class];
        cls = [OrderAfterSaleCertificatePhotoInfoCell class];
        OrderAfterSaleCertificatePhotoInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
        cell.delegate = self;
        cell.photoArrs = self.allPhotos;
        cell.cellTipLabel.text = @"1、照片含外包装快递面单;2、商品全貌照片;3、问题商品照片;\n注：若无法提供以上照片，您的售后可能无法支持，请你准确上传照片.";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.Arbitration == YES) {
        return indexPath.row<5&& indexPath.section == 0?0:UITableViewAutomaticDimension;
    }else{
        if (indexPath.row == 4) {
            return self.submitType == 2?0:UITableViewAutomaticDimension;
        }else{
            return  UITableViewAutomaticDimension;
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    if (indexPath.section == 1) {
        return;
    }
    if (!kValidArray(self.serviceTypeArr)) {
        return;
    }
    /********服务类型*********/
    if (indexPath.row == 0) {
        NSMutableArray *titleArr = [NSMutableArray array];
        for (int i =  0 ; i < self.serviceTypeArr.count; i++) {
            AfterSaleTypeConfigsModel *model = self.serviceTypeArr[i];
            [titleArr addObject:model.describe];
        }
        
        [BRStringPickerView showStringPickerWithTitle:@"" dataSource:titleArr defaultSelValue:self.TypeDescribe?:@"" isAutoSelect:YES themeColor:nil resultBlock:^(id selectValue) {
            //获取选择的服务类型
            self.submitType = (int)[titleArr indexOfObject:selectValue];
            self.TypeDescribe = selectValue;
            //刷新货物状态和售后原因
            self.subGoodStatus = nil;
            self.subReason = nil;
            self.subMoney = nil;
            self.maxMoney = nil;
            self.describeArr = [NSMutableArray arrayWithObjects:self.TypeDescribe,self.subGoodStatus?:@"请选择",self.subReason?:@"请选择", nil];

            [self.tableView reloadData];
            NSLog(@"%@",selectValue);
        } cancelBlock:^{
            NSLog(@"点击了背景视图或取消按钮");
        }];
    }
    /********货物状态*********/
    if (indexPath.row == 1) {
        self.ServiceTypeModel = self.serviceTypeArr[self.submitType];
        NSMutableArray *goodStatusArr = [NSMutableArray array];
        for (int i = 0; i < self.ServiceTypeModel.goodStatus.count; i++) {
            AfterSaleGoodStatusModel *goodModel = self.ServiceTypeModel.goodStatus[i];
            [goodStatusArr addObject:goodModel.describe];
        };
        
        [BRStringPickerView showStringPickerWithTitle:@"" dataSource:goodStatusArr defaultSelValue:@"" isAutoSelect:NO themeColor:nil resultBlock:^(id selectValue) {
            if (self.submitType == 1||self.submitType == 2) {
                AfterSaleGoodStatusModel *goodModel = self.ServiceTypeModel.goodStatus[0];
                self.subGoodStatusid = goodModel.type;
            }else{
                self.subGoodStatusid = (int)[goodStatusArr indexOfObject:selectValue];
            }
            self.subGoodStatus = selectValue;

            self.subReason = nil;
            self.describeArr = [NSMutableArray arrayWithObjects:self.TypeDescribe,self.subGoodStatus?:@"请选择",self.subReason?:@"请选择", nil];
            
            [self.tableView reloadData];
            NSLog(@"%@",selectValue);
        } cancelBlock:^{
            NSLog(@"点击了背景视图或取消按钮");
        }];
    }
    /********退货原因*********/
    if (indexPath.row == 2) {
        if (!self.subGoodStatus) {
            [YYCommonTools showTipMessage:@"请先选择货物状态"];
            return;
        }
        AfterSaleGoodStatusModel *reasonModel = self.ServiceTypeModel.goodStatus[self.submitType == 1||self.submitType==2?0: self.subGoodStatusid];
        NSMutableArray *reasonTitleArr = [NSMutableArray array];
        for (int i = 0; i < reasonModel.reasons.count; i++) {
            AfterSaleReasonsModel *chooseModel = reasonModel.reasons[i];
            [reasonTitleArr addObject:chooseModel.describe];
        }
        [BRStringPickerView showStringPickerWithTitle:@"" dataSource:reasonTitleArr defaultSelValue:@"" isAutoSelect:NO themeColor:nil resultBlock:^(id selectValue) {
            NSInteger index = [reasonTitleArr indexOfObject:selectValue];
            
            AfterSaleReasonsModel *Model = reasonModel.reasons[index];
            self.subReasonid = Model.type;
            self.subReason = selectValue;
            self.describeArr = [NSMutableArray arrayWithObjects:self.TypeDescribe,self.subGoodStatus?:@"请选择",self.subReason?:@"请选择", nil];
            [self getSaleMoney];
            [self.tableView reloadData];
        } cancelBlock:^{
        }];
    }
}

- (void)textFieldDidChange:(UITextField *)textField {
    
    if ([textField.text floatValue] > [self.maxMoney floatValue]) {
        textField.text = self.maxMoney;
    }
    self.subMoney = textField.text;
}

#pragma mark - PhotosOperatorDelegate
- (void)photoSelectOperator:(NSIndexPath *)index
                selectImage:(UIImage *)image {
    if (!image) {
        [yyImagePickerManagerClass openImagePickerCtrl:(5-self.allPhotos.count) columnCount:4 isAllowCrop:NO delegate:self viewCtrl:self myBlock:^(NSArray *array) {
            [self.allPhotos addObjectsFromArray:array];
            [UIView performWithoutAnimation:^{
                NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
                [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];

//                [self.tableView reloadData];
            }];
        }];
    }
}

- (void)deletePhotoOperator:(NSIndexPath *)index {
    if (kValidArray(self.allPhotos)) {
        [self.allPhotos removeObjectAtIndex:index.row];
    }
    [UIView performWithoutAnimation:^{
        NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:1];
        [self.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
//        [self.tableView reloadData];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
