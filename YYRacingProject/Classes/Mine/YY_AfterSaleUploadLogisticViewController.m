//
//  YY_AfterSaleUploadLogisticViewController.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_AfterSaleUploadLogisticViewController.h"
#import <Photos/Photos.h>
#import "TZImagePickerController.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "OrderAfterSaleLogisticNumberInfoCell.h"
#import "OrderAfterSaleLogisticCommitInfoCell.h"
#import "OrderAfterSaleLogisticCompanyInfoCell.h"
#import "OrderAfterSaleCertificatePhotoInfoCell.h"

#import "AfterSaleLogisticCompanyInfoModel.h"
#import "YYUploadPhotoRequestAPI.h"
#import "MineAfterSaleCommitLogisticInfoRequestAPI.h"
#import "MineAterSaleLogisticCompanyListInfoRequestAPI.h"
@interface YY_AfterSaleUploadLogisticViewController ()
<UITableViewDelegate,
UITableViewDataSource,
PhotosOperatorDelegate,
TZImagePickerControllerDelegate>

@property (nonatomic, assign) BOOL           isCanClick;
@property (nonatomic, strong) UITextField    *logisticNumber;

@property (nonatomic, strong) NSMutableArray *allPhotoUrls;
@property (nonatomic, strong) NSMutableArray *allPhotos;
@property (nonatomic, strong) NSMutableArray *allCompanys;
@property (nonatomic, strong) NSString       *companyTitle;

@property (nonatomic, strong) NSNumber  *spuId;
@property (nonatomic, strong) NSNumber  *orderId;
@end

@implementation YY_AfterSaleUploadLogisticViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self xh_addTitle:@"上传物流信息"];
    [self xh_popTopRootViewController:NO];
    
    self.companyTitle = nil;
    self.isCanClick = NO;
    self.allPhotos = [NSMutableArray array];
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"spuId"]) {
            self.spuId = [self.parameter objectForKey:@"spuId"];
        }
        if ([self.parameter objectForKey:@"orderId"]) {
            self.orderId = [self.parameter objectForKey:@"orderId"];
        }
    }
    
    self.tableStyle = UITableViewStyleGrouped;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.backgroundColor = XHLightColor;
    [self.view addSubview:self.m_tableView];
    self.m_tableView.estimatedRowHeight = 44.0;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.sectionFooterHeight = 0.1f;
    
    Class cls = [OrderAfterSaleLogisticCompanyInfoCell class];
    registerClass(self.m_tableView, cls);
    cls = [OrderAfterSaleLogisticNumberInfoCell class];
    registerClass(self.m_tableView, cls);
    cls = [OrderAfterSaleCertificatePhotoInfoCell class];
    registerClass(self.m_tableView, cls);
    cls = [OrderAfterSaleLogisticCommitInfoCell class];
    registerClass(self.m_tableView, cls);
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    [self gainLogisticCompanyRequest];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Request
/**获取物流公司信息**/
- (void)gainLogisticCompanyRequest {
    MineAterSaleLogisticCompanyListInfoRequestAPI *logisticCompanyAPI = [[MineAterSaleLogisticCompanyListInfoRequestAPI alloc] initAfterSaleLogisticsCompanyInfoRequest:[@{} mutableCopy]];
    [YYCenterLoading showCenterLoading];
    [logisticCompanyAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSArray *companys = [NSArray modelArrayWithClass:[AfterSaleLogisticCompanyInfoModel class] json:[responDict objectForKey:@"companys"]];
            self.allCompanys = [companys mutableCopy];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**提交物流信息**/
- (void)commitLogisticInfoRequest:(NSMutableDictionary *)params {
    MineAfterSaleCommitLogisticInfoRequestAPI *commitAPI = [[MineAfterSaleCommitLogisticInfoRequestAPI alloc] initAfterSaleCommitLogisticsInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [commitAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [YYCenterLoading hideCenterLoading];
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    
    Class cls = [UITableViewCell class];
    if (section != 0) {
        if (row == 0) {
            cls = [OrderAfterSaleCertificatePhotoInfoCell class];
            OrderAfterSaleCertificatePhotoInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            cell.delegate = self;
            cell.photoArrs = self.allPhotos;
            return cell;
        }
        else {
            cls = [OrderAfterSaleLogisticCommitInfoCell class];
            OrderAfterSaleLogisticCommitInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            [cell.commitBtn setEnabled:self.isCanClick];
            cell.commitBtn.selected = self.isCanClick;
            [cell.commitBtn addTarget:self action:@selector(commitClickEvent:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }
    else {
        if (row == 0) {
            cls = [OrderAfterSaleLogisticCompanyInfoCell class];
            OrderAfterSaleLogisticCompanyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            cell.contentLabel.text = kValidString(self.companyTitle)? self.companyTitle : @"请选择";
            cell.contentLabel.textAlignment = kValidString(self.companyTitle) ? NSTextAlignmentLeft : NSTextAlignmentRight;
            [cell.contentLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logisticSelectEvent:)]];
            return cell;
        }
        else {
            cls = [OrderAfterSaleLogisticNumberInfoCell class];
            OrderAfterSaleLogisticNumberInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(cls)];
            self.logisticNumber = cell.cellTextField;
            [self.logisticNumber addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
            return cell;
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [YYCreateTools createView:XHLightColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kSizeScale(8);
}

#pragma mark - Private
/**根据物流标题获取ID**/
- (NSNumber *)byCompanyNameGetCompanyID:(NSString *)names {
    __block NSString *tempName = names;
    __block NSNumber *companyID = nil;
    [self.allCompanys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AfterSaleLogisticCompanyInfoModel *model = (AfterSaleLogisticCompanyInfoModel *)obj;
        if ([tempName isEqualToString:model.companyName]) {
            companyID = model.companyId;
            *stop = YES;
        }
    }];
    return companyID;
}

#pragma mark - Event
- (void)textFieldChanged:(UITextField *)textField {
    NSString  *content = textField.text;
    if (content.length > 0 && kValidString(self.companyTitle)) {
        self.isCanClick = YES;
    }
    else {
        self.isCanClick = NO;
    }
    [UIView performWithoutAnimation:^{
        [self.m_tableView reloadRow:1 inSection:1 withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (void)commitClickEvent:(UIButton *)sender {
    __block NSMutableDictionary *commitParams = [NSMutableDictionary dictionary];
    [commitParams setObject:self.orderId forKey:@"orderId"];
    [commitParams setObject:self.spuId forKey:@"skuId"];
    if (!kValidString(self.companyTitle)) {
        [YYCommonTools showTipMessage:@"物流公司不能为空!"];
        return;
    }
    else {
        NSNumber *companyID = [self byCompanyNameGetCompanyID:self.companyTitle];
        [commitParams setObject:companyID forKey:@"companyId"];
    }
    NSString  *logisticNumberText = self.logisticNumber.text;
    if (!kValidString(logisticNumberText)) {
        [YYCommonTools showTipMessage:@"物流单号不能为空!"];
        return;
    }
    else {
        [commitParams setObject:logisticNumberText forKey:@"number"];
    }
    if (kValidArray(self.allPhotos)) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:self.allPhotos forKey:@"photos"];
        YYUploadPhotoRequestAPI *uploadAPI = [[YYUploadPhotoRequestAPI alloc] initUploadPhotoInfoRequest:params];
        [uploadAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
            if (kValidDictionary(responDict)) {
                NSMutableArray *imgurls = [NSMutableArray array];
                NSArray *imgs = [responDict objectForKey:@"imgs"];
                if (kValidArray(imgs)) {
                    for (int i = 0; i < imgs.count; i ++) {
                        NSDictionary *imgDict = [imgs objectAtIndex:i];
                        if ([imgDict objectForKey:@"imageUrl"]) {
                            [imgurls addObject: [imgDict objectForKey:@"imageUrl"]];
                        }
                    }
                }
                if (kValidArray(imgurls)) {
                    [commitParams setObject:imgurls forKey:@"urls"];
                }
                [self commitLogisticInfoRequest:commitParams];
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }
    else {
        [self commitLogisticInfoRequest:commitParams];
    }
}

- (void)logisticSelectEvent:(UITapGestureRecognizer *)tapGesture {
    __block NSMutableArray *companyTitle = [NSMutableArray array];
    [self.allCompanys enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        AfterSaleLogisticCompanyInfoModel *model = (AfterSaleLogisticCompanyInfoModel *)obj;
        NSMutableString *companyName = [NSMutableString stringWithString:model.companyName];
        [companyTitle addObject:companyName];
    }];
    [BRStringPickerView showStringPickerWithTitle:@"选择物流公司"
                                       dataSource:companyTitle defaultSelValue:nil
                                      resultBlock:^(id selectValue) {
                                          self.companyTitle = selectValue;
                                          if (kValidString(self.companyTitle)) {
                                              [self textFieldChanged:self.logisticNumber];
                                          }
                                          [UIView performWithoutAnimation:^{
                                              [self.m_tableView reloadRow:0 inSection:0 withRowAnimation:UITableViewRowAnimationAutomatic];
                                          }];
                                      }];
}

#pragma mark - PhotosOperatorDelegate
- (void)photoSelectOperator:(NSIndexPath *)index
                selectImage:(UIImage *)image {
    if (!image) {
        [yyImagePickerManagerClass openImagePickerCtrl:(5-self.allPhotos.count) columnCount:4 isAllowCrop:NO delegate:self viewCtrl:self myBlock:^(NSArray *array) {
            [self.allPhotos addObjectsFromArray:array];
            [UIView performWithoutAnimation:^{
                [self.m_tableView reloadData];
            }];
        }];
    }
}

- (void)deletePhotoOperator:(NSIndexPath *)index {
    if (kValidArray(self.allPhotos)) {
        [self.allPhotos removeObjectAtIndex:index.row];
    }
    [UIView performWithoutAnimation:^{
        [self.m_tableView reloadData];
    }];
}

@end
