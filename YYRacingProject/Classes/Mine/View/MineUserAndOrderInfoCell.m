//
//  MineUserAndOrderInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "MineUserAndOrderInfoCell.h"

#define MineOrderStatuBtnTag  350
#import "yyPhotoDownloadCacheManager.h"
@interface MineUserAndOrderInfoCell ()

@property (nonatomic, strong) UIImageView *cellBackgroundView;
@property (nonatomic, strong) UIImageView *whiteBackgroundView;

@property (nonatomic, strong) UIImageView *headBackgroundView;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UILabel  *userNameLabel;

//@property (nonatomic, strong) UIView   *visitView;
//@property (nonatomic, strong) UILabel  *visitLabel;
//@property (nonatomic, strong) UIButton *coppyButton;

@property (nonatomic, strong) UIView   *orderView;
@property (nonatomic, strong) UILabel  *leftTipLabel;
@property (nonatomic, strong) UILabel  *rightTipLabel;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UIView   *lineView;

@property (nonatomic, strong) UIView   *bottomView;
@property (nonatomic, strong) NSMutableArray *btnArray;

@end

@implementation MineUserAndOrderInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.btnArray = [NSMutableArray array];
        self.contentView.backgroundColor = self.backgroundColor = XHClearColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    //cell的背景
    self.cellBackgroundView = [YYCreateTools createImageView:nil
                                                   viewModel:-1];
    [self.cellBackgroundView zy_cornerRadiusAdvance:kSizeScale(5)
                                     rectCornerType:UIRectCornerAllCorners];
    self.cellBackgroundView.image = [UIImage imageWithColor:XHClearColor];
    [self.contentView addSubview:self.cellBackgroundView];
    
    //白色背景
    self.whiteBackgroundView = [YYCreateTools createImageView:nil
                                                      viewModel:-1];
    [self.whiteBackgroundView zy_cornerRadiusAdvance:kSizeScale(5)
                                     rectCornerType:UIRectCornerAllCorners];
    self.whiteBackgroundView.image = [UIImage imageWithColor:XHWhiteColor];
    [self.contentView addSubview:self.whiteBackgroundView];
    
    self.headBackgroundView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self.headBackgroundView zy_cornerRadiusAdvance:kSizeScale(kSizeScale(39))
                                rectCornerType:UIRectCornerAllCorners];
    self.headBackgroundView.image = [UIImage imageWithColor:XHWhiteColor];
    [self.contentView addSubview:self.headBackgroundView];
    
    self.headImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self.headImageView zy_cornerRadiusAdvance:kSizeScale(kSizeScale(36))
                                      rectCornerType:UIRectCornerAllCorners];
    [self.contentView addSubview:self.headImageView];
    
    self.orderView = [YYCreateTools createView:XHWhiteColor];
    [self.orderView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(orderViewClickEvent:)]];
    [self.contentView addSubview:self.orderView];
    
    //订单item背景是土
    self.bottomView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.bottomView];
    // 用户信息相关
    self.userNameLabel = [YYCreateTools createLabel:@""
                                               font:boldFont(18)
                                          textColor:XHMainColor];
    [self.contentView addSubview:self.userNameLabel];
    
    
//    self.visitView = [YYCreateTools createView:XHWhiteColor];
//    [self.contentView addSubview:self.visitView];
//
//    self.visitLabel = [YYCreateTools createLabel:@""
//                                            font:midFont(13)
//                                       textColor:XHBlueDeepColor];
//    [self.visitView addSubview:self.visitLabel];
//
//    self.coppyButton = [YYCreateTools createBtn:@"点击复制"
//                                           font:midFont(10)
//                                      textColor:XHBlueDeepColor];
//    self.coppyButton.layer.borderColor = XHBlueDeepColor.CGColor;
//    self.coppyButton.layer.borderWidth = 0.5;
//    self.coppyButton.layer.cornerRadius = kSizeScale(10);
//    [self.coppyButton addTarget:self action:@selector(codeCopyClickEvent:) forControlEvents:UIControlEventTouchUpInside];
//    [self.visitView addSubview:self.coppyButton];
//
//    self.visitLabel.hidden = YES;
//    self.coppyButton.hidden = YES;
    
    
    // 订单相关
    self.leftTipLabel = [YYCreateTools createLabel:@"我的订单"
                                              font:boldFont(14)
                                         textColor:XHBlackColor];
    [self.orderView addSubview:self.leftTipLabel];
    
    self.rightTipLabel = [YYCreateTools createLabel:@"查看全部订单"
                                               font:midFont(13)
                                          textColor:XHBlackMidColor];
    [self.orderView addSubview:self.rightTipLabel];
    
    self.rightImageView = [YYCreateTools createImageView:@"mine_arrow_icon"
                                               viewModel:-1];
    [self.orderView addSubview:self.rightImageView];
    
    self.lineView = [YYCreateTools createView:HexRGB(0xf1f1f1)];
    [self.orderView addSubview:self.lineView];
    
    
    [self.cellBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [self.whiteBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(213));
        make.bottom.equalTo(self.contentView);
    }];
    
    [self.headBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.whiteBackgroundView.mas_top);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(78), kSizeScale(78)));
        make.centerX.equalTo(self.whiteBackgroundView);
    }];
    
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.headBackgroundView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(72), kSizeScale(72)));
    }];
    // 用户布局
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headBackgroundView.mas_bottom).offset(kSizeScale(11.5));
        make.centerX.equalTo(self.contentView);
        make.height.mas_offset(kSizeScale(18));
    }];
    
//    [self.visitView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.equalTo(self.userNameLabel);
//        make.top.equalTo(self.userNameLabel.mas_bottom).offset(kSizeScale(3));
//        make.size.mas_equalTo(CGSizeMake(kSizeScale(165), kSizeScale(0)));
//    }];
//
//    [self.visitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.centerY.equalTo(self.visitView);
//    }];
//
//    [self.coppyButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.visitLabel.mas_right).offset(kSizeScale(8));
//        make.centerY.equalTo(self.visitView);
//        make.size.mas_equalTo(CGSizeMake(kSizeScale(60), kSizeScale(20)));
//    }];
    
    // 订单布局
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(76));
        make.bottom.equalTo(self.contentView).offset(-kSizeScale(2.5));
    }];
    
    [self.orderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(45));
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
    
    [self.leftTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.centerY.equalTo(self.orderView);
    }];
    
    [self.rightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.orderView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(5), kSizeScale(9)));
    }];
    
    [self.rightTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.rightImageView.mas_left).offset(-kSizeScale(8));
        make.centerY.equalTo(self.leftTipLabel);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(0.5));
        make.bottom.equalTo(self.orderView);
    }];
    
    for (int i = 0; i < 5; i ++ ) {
        YYButton *btn = [[YYButton alloc] initWithFrame:CGRectZero];
        btn.space = kSizeScale(3);
        btn.tag = MineOrderStatuBtnTag + i;  // 300.  301 . 302 . 303
        btn.titleFont = midFont(13);
        CGPoint offset = CGPointMake(IS_IPHONE_5?-kSizeScale(30) :-kSizeScale(35), IS_IPHONE_5 ?kSizeScale(20) :kSizeScale(22));
        [btn setBadgeOffset:offset];
        [btn setTitleTextColor:XHBlackMidColor];
        [btn setBadgeBgColor:XHRedColor];
        btn.btnClickEventBlock = ^(BOOL status, NSInteger btnTag) {
            NSInteger type = (btnTag - MineOrderStatuBtnTag) + 1; //1 . 2 . 3 . 4
            switch (type) {
                case 4:
                    [MobClick event:@"storeUnpaid"];
                    break;
                case 3:
                    [MobClick event:@"storeUntake"];
                    break;
                case 2:
                    [MobClick event:@"storeFinish"];
                    break;
                case 1:
                    [MobClick event:@"myAfterSale"];
                    break;
                default:
                    break;
            }
            if (self.orderClickBlock) {
                self.orderClickBlock(type);
            }
        };
        [self.bottomView addSubview:btn];
        [self.btnArray addObject:btn];
    }
    [self yy_multipleViewsAutoLayout:self.btnArray
                                      superView:self.bottomView
                                        padding:0
                                       viewSize:CGSizeMake((kScreenW-kSizeScale(12)*2)/5.0, kSizeScale(76)) startSildeLeft:YES];
}

- (void)setMineOrder:(NSMutableArray *)imageNames titles:(NSMutableArray *)titles
           redCounts:(NSMutableArray *)redCounts topSpace:(CGFloat)topSpace
           itemSpace:(CGFloat)itemSpace {
    if (kUserInfo) {
        self.userNameLabel.text = kUserInfo.userName;
//        self.visitLabel.text = [NSString stringWithFormat:@"邀请码：%@",kUserInfo.promotionCode];
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kUserInfo.avatarUrl]];
    }
    
    [self.btnArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        YYButton *btn = (YYButton *)obj;
        if (idx < imageNames.count && idx < titles.count) {
            btn.normalTitle = titles[idx];
            btn.selectTitle = titles[idx];
            btn.IMGspace = topSpace;
            btn.space = itemSpace;
            NSString *normalImageName = imageNames[idx];
            if (kValidString(normalImageName)) {
                btn.normalImageName = btn.selectImageName = normalImageName;
            }
            btn.select = YES;
        }
        if (idx < redCounts.count) {
            NSDictionary *redValue = [redCounts objectAtIndex:idx];
            [btn showBadgeWithValue:[[redValue objectForKey:@"cnt"] integerValue]];
        }
    }];
}

#pragma mark - Event
- (void)codeCopyClickEvent:(UIButton *)sender {
    [YYCommonTools pasteboardCopy:kUserInfo.promotionCode
                     appendParams:nil];
}

- (void)orderViewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.orderClickBlock) {
        self.orderClickBlock(0);
    }
}
@end
