//
//  OrderAfterSaleDetailHeaderView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleDetailHeaderView.h"

@interface OrderAfterSaleDetailHeaderView()

@property (nonatomic, strong) UIImageView  *goodImageView;
@property (nonatomic, strong) YYLabel      *goodNameLabel;
@property (nonatomic, strong) UILabel      *goodSkuLabel;
@property (nonatomic, strong) UILabel      *goodPriceLabel;
@property (nonatomic, strong) UILabel      *commissionLabel;
@property (nonatomic, strong) UILabel      *goodNumLabel;

@end

@implementation OrderAfterSaleDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.goodImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self addSubview:self.goodImageView];
    
    self.goodNameLabel = [YYCreateTools createLabel:nil
                                               font:midFont(13)
                                          textColor:XHBlackColor
                                           maxWidth:(kScreenW - kSizeScale(104) - kSizeScale(25))
                                      fixLineHeight:kSizeScale(18)];
    self.goodNameLabel.numberOfLines = 2;
    [self addSubview:self.goodNameLabel];
    
    self.goodSkuLabel = [YYCreateTools createLabel:nil
                                              font:midFont(12)
                                         textColor:XHBlackLitColor];
    self.goodSkuLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self addSubview:self.goodSkuLabel];
    
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(15)
                                           textColor:XHBlackColor];
    [self addSubview:self.goodPriceLabel];
    
    self.goodNumLabel = [YYCreateTools createLabel:nil
                                              font:midFont(12)
                                         textColor:XHBlackColor];
    [self addSubview:self.goodNumLabel];
    
    // layout
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(16));
        make.width.mas_equalTo(kSizeScale(80));
        make.bottom.mas_equalTo(-kSizeScale(16));
    }];
    
    [self.goodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImageView.mas_top).offset(-kSizeScale(5));
        make.left.equalTo(self.goodImageView.mas_right).offset(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(13));
    }];
    
    [self.goodSkuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(5));
        make.right.mas_equalTo(-kSizeScale(40));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.bottom.equalTo(self.goodImageView.mas_bottom);
    }];
    
    [self.goodNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.goodSkuLabel);
    }];
}

- (void)setGoodModel:(OrderGoodInfoModel *)goodModel {
    _goodModel = goodModel;
    self.goodNameLabel.text = _goodModel.skuTitle;
    self.goodSkuLabel.text = _goodModel.skuProp;
    [self.goodImageView yy_sdWebImage:_goodModel.skuCover
                 placeholderImageType:YYPlaceholderImageListGoodType];
    self.goodNumLabel.text = [NSString stringWithFormat:@"x%@",_goodModel.skuCnt];
    if (kValidDictionary(_goodModel.price)) {
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:midFont(9) symbolTextColor:XHBlackColor wordSpace:2 price:[NSString stringWithFormat:@"%@",[_goodModel.price objectForKey:@"priceTag"]] priceFont:boldFont(15) priceTextColor: XHBlackColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
    }
}


@end
