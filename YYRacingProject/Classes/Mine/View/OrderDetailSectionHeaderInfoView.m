//
//  OrderDetailSectionHeaderInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/21.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderDetailSectionHeaderInfoView.h"

@interface OrderDetailSectionHeaderInfoView()

@property (nonatomic, strong) UIView  *headerView;
@property (nonatomic, strong) UIView  *topView;
@property (nonatomic, strong) UIView  *marksView;
@property (nonatomic, strong) UILabel *deliverAddressLabel;

@end

@implementation OrderDetailSectionHeaderInfoView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.headerView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.headerView];
    
    self.topView = [YYCreateTools createView:XHLightColor];
    [self.headerView addSubview:self.topView];
    
    self.marksView = [YYCreateTools createView:XHRedColor];
    [self.headerView addSubview:self.marksView];
    
    self.deliverAddressLabel = [YYCreateTools createLabel:nil
                                                     font:midFont(13)
                                                textColor:XHBlackColor];
    [self.headerView addSubview:self.deliverAddressLabel];
    
    [self.headerView  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
        make.height.mas_equalTo(kSizeScale(50)).priorityHigh();
    }];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.headerView);
        make.height.mas_equalTo(kSizeScale(8));
    }];
    [self.marksView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(4), kSizeScale(4)));
        make.left.mas_equalTo(kSizeScale(14));
        make.centerY.equalTo(self.headerView).offset(kSizeScale(5));
    }];
    [self.deliverAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.marksView.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.marksView);
    }];
}

- (void)setBusinessWarehouse:(NSString *)businessWarehouse {
    _businessWarehouse = businessWarehouse;
    self.deliverAddressLabel.text = _businessWarehouse;
}

@end
