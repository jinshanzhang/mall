//
//  MineSettingInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface MineSettingInfoCell : YYBaseTableViewCell

@property (nonatomic, assign) BOOL     isShowHead;
@property (nonatomic, assign) BOOL     isHideArrowImg;
@property (nonatomic, strong) NSString *setTitle;
@property (nonatomic, strong) NSString *setContent;

@property (nonatomic, copy) void (^cellClickBlock)(MineSettingInfoCell *setCell);
@end
