//
//  OrderLogisticsSectionFooterInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderLogisticsSectionFooterInfoView.h"

@interface OrderLogisticsSectionFooterInfoView()

@property (nonatomic, strong) UIView   *topView;
@property (nonatomic, strong) UIView   *bottomView;
@property (nonatomic, strong) UILabel  *DHLLabel;
@property (nonatomic, strong) UILabel  *DHOrderNumberLabel;

@property (nonatomic, strong) UIButton *copsyBtn;

@end

@implementation OrderLogisticsSectionFooterInfoView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    self.topView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.topView];
    
    self.DHLLabel = [YYCreateTools createLabel:nil
                                          font:boldFont(15)
                                     textColor:XHBlackColor];
    [self.contentView addSubview:self.DHLLabel];
    
    self.DHOrderNumberLabel = [YYCreateTools createLabel:nil
                                                    font:midFont(13)
                                               textColor:XHBlackLitColor];
    [self.contentView addSubview:self.DHOrderNumberLabel];
    
    self.copsyBtn = [YYCreateTools createButton:@"复制"
                                       bgColor:XHWhiteColor
                                     textColor:XHBlackColor];
    self.copsyBtn.layer.cornerRadius = 11;
    self.copsyBtn.clipsToBounds = YES;
    self.copsyBtn.titleLabel.font = midFont(12);
    self.copsyBtn.layer.borderWidth = 1;
    self.copsyBtn.layer.borderColor = XHBlackColor.CGColor;
    [self.contentView addSubview:self.copsyBtn];
    [self.copsyBtn addTarget:self action:@selector(copyOrderNumberEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomView];
    
// layout
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(10)).priorityHigh();
    }];
    [self.DHLLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.topView.mas_bottom).offset(kSizeScale(12)).priorityHigh();
    }];
    [self.DHOrderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.DHLLabel);
        make.top.equalTo(self.DHLLabel.mas_bottom).offset(kSizeScale(6)).priorityHigh();
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.contentView);
        make.top.equalTo(self.DHOrderNumberLabel.mas_bottom).offset(kSizeScale(12)).priorityHigh();
        make.height.mas_equalTo(kSizeScale(10)).priorityHigh();
    }];
    
    [self.copsyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(22)));
        make.centerY.equalTo(self.DHOrderNumberLabel);
        make.right.mas_equalTo(-kSizeScale(12));
    }];
}

- (void)setCarrierModel:(OrderLogisticCarrierInfoModel *)carrierModel {
    _carrierModel = carrierModel;
    self.DHLLabel.text = _carrierModel.carrierName;
    self.DHOrderNumberLabel.text = [NSString stringWithFormat:@"运单编号：%@",_carrierModel.trackNo];
}

#pragma mark - Event
- (void)copyOrderNumberEvent:(UIButton *)sender {
    [YYCommonTools pasteboardCopy:self.carrierModel.trackNo
                     appendParams:nil];
}
@end
