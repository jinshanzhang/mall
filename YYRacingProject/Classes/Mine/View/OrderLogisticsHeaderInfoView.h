//
//  OrderLogisticsHeaderInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/14.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderLogisticsHeaderInfoView : UIView

@property (nonatomic, strong) NSString *headerTitle;

@end

NS_ASSUME_NONNULL_END
