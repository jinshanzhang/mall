//
//  OrderLogisticsCompanyInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/14.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "OrderLogisticsCompanyInfoCell.h"
@interface OrderLogisticsCompanyInfoCell()

@property (nonatomic, strong) UILabel  *logisticsCompanyNameLabel;
@property (nonatomic, strong) UILabel  *orderNumberLabel;

@property (nonatomic, strong) UIButton *copsyBtn;

@property (nonatomic, strong) UIView   *bottomView;

@property (nonatomic, strong) UILabel  *tipLabel;
@property (nonatomic, strong) UIView   *lineView;

@end

@implementation OrderLogisticsCompanyInfoCell

- (void)createUI {
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomView];
    
    self.logisticsCompanyNameLabel = [YYCreateTools createLabel:nil
                                          font:boldFont(15)
                                     textColor:XHBlackColor];
    [self.contentView addSubview:self.logisticsCompanyNameLabel];
    
    self.orderNumberLabel = [YYCreateTools createLabel:nil
                                                    font:midFont(13)
                                               textColor:XHBlackLitColor];
    [self.contentView addSubview:self.orderNumberLabel];
    
    self.copsyBtn = [YYCreateTools createButton:@"复制"
                                        bgColor:XHWhiteColor
                                      textColor:XHBlackColor];
    self.copsyBtn.layer.cornerRadius = 11;
    self.copsyBtn.clipsToBounds = YES;
    self.copsyBtn.titleLabel.font = midFont(12);
    self.copsyBtn.layer.borderWidth = 1;
    self.copsyBtn.layer.borderColor = XHBlackColor.CGColor;
    [self.contentView addSubview:self.copsyBtn];
    [self.copsyBtn addTarget:self action:@selector(copyOrderNumberEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomView];
    
    self.tipLabel = [YYCreateTools createLabel:nil
                                          font:midFont(13)
                                     textColor:XHBlackColor];
    [self.contentView addSubview:self.tipLabel];
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.lineView];
    
    // layout
    [self.logisticsCompanyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(15));
    }];
    
    [self.orderNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logisticsCompanyNameLabel);
        make.top.equalTo(self.logisticsCompanyNameLabel.mas_bottom).offset(kSizeScale(10));
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.orderNumberLabel.mas_bottom).offset(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(8));
    }];
    
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logisticsCompanyNameLabel);
        make.top.equalTo(self.bottomView.mas_bottom).offset(kSizeScale(15));
        make.width.mas_equalTo(kSizeScale(100));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.logisticsCompanyNameLabel);
        make.height.mas_equalTo(1);
        make.right.mas_equalTo(-kSizeScale(12));
        make.top.equalTo(self.tipLabel.mas_bottom).offset(kSizeScale(15));
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    [self.copsyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(22)));
        make.centerY.equalTo(self.orderNumberLabel);
        make.right.mas_equalTo(-kSizeScale(12));
    }];
}


- (void)setCarrierModel:(OrderLogisticCarrierInfoModel *)carrierModel {
    _carrierModel = carrierModel;
    if (_carrierModel) {
        self.logisticsCompanyNameLabel.text = _carrierModel.carrierName;
        self.orderNumberLabel.text = [NSString stringWithFormat:@"运单编号：%@",_carrierModel.trackNo];
        self.tipLabel.text = @"物流信息";
    }
    else {
        self.logisticsCompanyNameLabel.text = nil;
        self.orderNumberLabel.text = nil;
        self.tipLabel.text = nil;
    }
}

#pragma mark - Event
- (void)copyOrderNumberEvent:(UIButton *)sender {
    [YYCommonTools pasteboardCopy:self.carrierModel.trackNo
                     appendParams:nil];
}
@end
