//
//  MineSetButtonInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface MineSetButtonInfoCell : YYBaseTableViewCell

@property (nonatomic, copy) void (^quitClickBlock)(void);

@end
