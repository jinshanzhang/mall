//
//  AfterSaleCommonCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface AfterSaleCommonCell : YYBaseTableViewCell<UITextViewDelegate>

typedef NS_ENUM(NSInteger , UseCellType) {
    CellwithOneTextFild = 0,     //
    CellWithNumberView = 1,
    CellWithTextView = 2,
    CellWithTwoLabelAndArrow = 3,
};

@property (nonatomic, assign) UseCellType type;


@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *detailLabel;

@property (nonatomic, strong) UITextField  *showTextField;

@property (nonatomic, strong) UITextView *showTextView;

@property (nonatomic, strong) UIImageView *arrowImageView;

@property (nonatomic, strong) UIView  *lineView;

@property (nonatomic, strong) YYNumberView *numberView;

@property (nonatomic, copy) void (^cellNumerBlock)(NSInteger skuCnt);
- (void)textViewDidChange:(UITextView *)textView;



@end
