//
//  MineBasicItemView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MineItemContentInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol  MineBasicItemViewDelegate <NSObject>

@optional
- (void)basicViewClickEvent:(id)viewModel;

@end

@interface MineBasicItemView : UIView

@property (nonatomic, weak) id <MineBasicItemViewDelegate> delegate;
- (void)setViewModel:(MineItemContentInfoModel *)infoModel;

@end

NS_ASSUME_NONNULL_END
