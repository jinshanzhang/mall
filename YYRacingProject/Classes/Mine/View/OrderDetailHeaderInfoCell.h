//
//  OrderDetailHeaderInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

#import "OrderDetailInfoModel.h"
@interface OrderDetailHeaderInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) OrderDetailInfoModel  *detailModel;
@property (nonatomic, copy)   void (^lookLogisticBlock)(void);

@end
