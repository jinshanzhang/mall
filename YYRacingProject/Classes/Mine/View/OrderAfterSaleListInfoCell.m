//
//  OrderAfterSaleListInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleListInfoCell.h"

@interface OrderAfterSaleListInfoCell()

@property (nonatomic, strong) UIView   *topView;
@property (nonatomic, strong) UIView   *topLineView;

@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, strong) UILabel  *statuLabel;

@property (nonatomic, strong) UIImageView  *goodImageView;
@property (nonatomic, strong) YYLabel      *goodNameLabel;
@property (nonatomic, strong) UILabel      *goodSkuLabel;
@property (nonatomic, strong) UILabel      *goodPriceLabel;
@property (nonatomic, strong) UILabel      *goodNumLabel;

@property (nonatomic, strong) UIView  *bottomView;
@property (nonatomic, strong) UILabel *afterSaleLabel;
@property (nonatomic, strong) UIView  *bottomLineView;
@end

@implementation OrderAfterSaleListInfoCell

- (void)createUI {
    
    // 顶部
    self.topView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.topView];
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(39));
    }];
    
    self.topLineView = [YYCreateTools createView:HexRGB(0xf7f7f7)];
    [self.contentView addSubview:self.topLineView];
    
    [self.topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.topView);
        make.top.equalTo(self.topView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(1));
    }];
    
    self.titleLabel = [YYCreateTools createLabel:nil
                                            font:midFont(13)
                                       textColor:XHBlackMidColor];
    self.titleLabel.backgroundColor = XHWhiteColor;
    [self.topView addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(30));
        make.centerY.equalTo(self.topView);
    }];
    
    self.statuLabel = [YYCreateTools createLabel:nil
                                            font:midFont(13)
                                       textColor:XHRedColor];
    self.statuLabel.backgroundColor = XHWhiteColor;
    [self.topView addSubview:self.statuLabel];
    
    [self.statuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLabel);
        make.left.equalTo(self.titleLabel.mas_right).offset(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(30)).priorityHigh();
    }];
    
    // 中间商品信息
    self.goodImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self.contentView addSubview:self.goodImageView];
    
    self.goodNameLabel = [YYCreateTools createLabel:nil
                                               font:midFont(13)
                                          textColor:XHBlackColor
                                           maxWidth:(kScreenW - kSizeScale(104) - kSizeScale(25))
                                      fixLineHeight:kSizeScale(18)];
    self.goodNameLabel.numberOfLines = 2;
    [self.contentView addSubview:self.goodNameLabel];
    
    self.goodSkuLabel = [YYCreateTools createLabel:nil
                                              font:midFont(12)
                                         textColor:XHBlackLitColor];
    self.goodSkuLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:self.goodSkuLabel];
    
    // 9, 15 这里做价格区分
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(15)
                                           textColor:XHBlackColor];
    [self.contentView addSubview:self.goodPriceLabel];
    
    self.goodNumLabel = [YYCreateTools createLabel:nil
                                              font:midFont(12)
                                         textColor:XHBlackColor];
    [self.contentView addSubview:self.goodNumLabel];
    
    
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.equalTo(self.topLineView.mas_bottom).offset(kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(80), kSizeScale(80)));
    }];
    
    [self.goodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImageView.mas_top).offset(-kSizeScale(5));
        make.left.equalTo(self.goodImageView.mas_right).offset(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(13));
    }];
    
    [self.goodSkuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(5));
        make.right.mas_equalTo(-kSizeScale(40));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.bottom.equalTo(self.goodImageView.mas_bottom);
    }];
    
    [self.goodNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.goodSkuLabel);
    }];
    
    // 底部view
    self.bottomView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.bottomView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.top.equalTo(self.goodImageView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(40));
    }];
    
    self.afterSaleLabel = [YYCreateTools createLabel:nil
                                            font:midFont(13)
                                       textColor:XHBlackColor];
    [self.bottomView addSubview:self.afterSaleLabel];
    
    self.bottomLineView = [YYCreateTools createView:HexRGB(0xf7f7f7)];
    [self.contentView addSubview:self.bottomLineView];
    
    [self.afterSaleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(10));
        make.centerY.equalTo(self.bottomView);
        make.height.mas_equalTo(kSizeScale(15)).priorityHigh();
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(8));
        make.top.equalTo(self.bottomView.mas_bottom);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderClickEvent:)]];
}

- (void)setAfterSaleModel:(OrderAfterSaleListInfoModel *)afterSaleModel {
    _afterSaleModel = afterSaleModel;
    if (_afterSaleModel) {
        self.statuLabel.text = _afterSaleModel.afterSaleParentStatusName;
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
        self.titleLabel.text = [NSString stringWithFormat:@"申请时间：%@",[NSString formatDateAndTime:_afterSaleModel.createdTime formatter:formatter]];
        
        self.goodNameLabel.text = _afterSaleModel.skuTitle;
        self.goodSkuLabel.text = _afterSaleModel.skuProp;
        [self.goodImageView yy_sdWebImage:_afterSaleModel.imageUrl
                     placeholderImageType:YYPlaceholderImageListGoodType];
        self.goodNumLabel.text = [NSString stringWithFormat:@"x%d",_afterSaleModel.skuCnt];
        
        if (kValidString(_afterSaleModel.skuPrice)) {
            NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:midFont(9) symbolTextColor:XHBlackColor wordSpace:2 price:[NSString stringWithFormat:@"%@",_afterSaleModel.skuPrice] priceFont:boldFont(15) priceTextColor: XHBlackColor symbolOffsetY:0];
            self.goodPriceLabel.attributedText = attribut;
        }
        self.afterSaleLabel.text = _afterSaleModel.afterSaleParentTypeName;
    }
}

#pragma mark - Event
- (void)orderClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.afterSaleClick) {
        self.afterSaleClick(self.afterSaleModel, self);
    }
}
@end
