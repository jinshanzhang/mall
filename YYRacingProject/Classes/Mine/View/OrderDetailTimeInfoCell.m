//
//  OrderDetailTimeInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderDetailTimeInfoCell.h"

@interface OrderDetailTimeInfoCell()

@property (nonatomic, strong) UILabel  *orderNumberLbl;
@property (nonatomic, strong) UIButton *numberCopyBtn;
@property (nonatomic, strong) UILabel  *createTimeLlb;
@property (nonatomic, strong) UILabel  *payTimeLbl;
@property (nonatomic, strong) UILabel  *remarkLbl;

@end

@implementation OrderDetailTimeInfoCell

- (void)createUI {
    self.orderNumberLbl = [YYCreateTools createLabel:nil
                                                font:midFont(12)
                                           textColor:XHBlackLitColor];
    [self.contentView addSubview:self.orderNumberLbl];
    
    self.createTimeLlb = [YYCreateTools createLabel:nil
                                                font:midFont(12)
                                           textColor:XHBlackLitColor];
    [self.contentView addSubview:self.createTimeLlb];
    
    self.payTimeLbl = [YYCreateTools createLabel:nil
                                               font:midFont(12)
                                          textColor:XHBlackLitColor];
    [self.contentView addSubview:self.payTimeLbl];
    
    self.remarkLbl = [YYCreateTools createLabel:nil
                                           font:midFont(12)
                                      textColor:XHBlackLitColor];
    self.remarkLbl.numberOfLines = 0;
    self.remarkLbl.preferredMaxLayoutWidth = kScreenW - kSizeScale(24);
    [self.contentView addSubview:self.remarkLbl];
    
    self.numberCopyBtn = [YYCreateTools createBtn:@"复制"
                                             font:midFont(12)
                                        textColor:XHBlackMidColor];
    self.numberCopyBtn.backgroundColor = XHWhiteColor;
    self.numberCopyBtn.layer.borderWidth = 0.8;
    self.numberCopyBtn.layer.cornerRadius = kSizeScale(11);
    self.numberCopyBtn.layer.borderColor = XHBlackLitColor.CGColor;
    self.numberCopyBtn.isIgnore = YES;
    [self.numberCopyBtn addTarget:self action:@selector(orderNumberCopyEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.numberCopyBtn];
    
    [self.orderNumberLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(kSizeScale(12));
    }];
    
    [self.numberCopyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(10));
        make.centerY.equalTo(self.orderNumberLbl);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(22)));
    }];
    
    [self.createTimeLlb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderNumberLbl);
        make.top.equalTo(self.orderNumberLbl.mas_bottom).offset(kSizeScale(10));
    }];
    
    [self.payTimeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderNumberLbl);
        make.top.equalTo(self.createTimeLlb.mas_bottom).offset(kSizeScale(10));
    }];
    
    [self.remarkLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.createTimeLlb);
        make.right.mas_equalTo(-kSizeScale(10));
        make.top.equalTo(self.payTimeLbl.mas_bottom).offset(kSizeScale(10));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(10));
    }];
}

- (void)setOrderNumber:(NSNumber *)orderNumber {
    _orderNumber = orderNumber;
    self.orderNumberLbl.text = [NSString stringWithFormat:@"订单编号：%@",_orderNumber];
}

- (void)setCreateTime:(NSString *)createTime {
    _createTime = createTime;
    self.createTimeLlb.text = [NSString stringWithFormat:@"下单时间：%@",_createTime];
}

- (void)setPayTime:(NSString *)payTime {
    _payTime = payTime;
    self.payTimeLbl.text = [NSString stringWithFormat:@"付款时间：%@",_payTime];
}

- (void)setRemarkContent:(NSString *)remarkContent {
    _remarkContent = remarkContent;
    self.remarkLbl.text = [NSString stringWithFormat:@"备注：%@",_remarkContent];
}

#pragma mark - Event
- (void)orderNumberCopyEvent:(UIButton *)sender {
    [YYCommonTools pasteboardCopy:[NSString stringWithFormat:@"%@",self.orderNumber]
                     appendParams:nil];
}
@end
