//
//  MineSettingInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineSettingInfoCell.h"

@interface MineSettingInfoCell()

@property (nonatomic, strong) UILabel  *leftLabel;
@property (nonatomic, strong) UILabel  *rightLabel;
@property (nonatomic, strong) UIImageView *userHeadPicture;
@property (nonatomic, strong) UIImageView *arrowImageView;

@property (nonatomic, strong) UIView   *bottomLine;
@end

@implementation MineSettingInfoCell

- (void)createUI {
    
    self.leftLabel = [YYCreateTools createLabel:nil
                                           font:midFont(15)
                                      textColor:XHBlackColor];
    [self.contentView addSubview:self.leftLabel];
    
    self.rightLabel = [YYCreateTools createLabel:nil
                                           font:midFont(15)
                                      textColor:XHBlackLitColor];
    self.rightLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.rightLabel];
    
    self.userHeadPicture = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    [self.userHeadPicture zy_cornerRadiusAdvance:kSizeScale(15) rectCornerType:UIRectCornerAllCorners];
    [self.contentView addSubview:self.userHeadPicture];
    
    self.arrowImageView = [YYCreateTools createImageView:@"mine_more_icon"
                                               viewModel:-1];
    [self.contentView addSubview:self.arrowImageView];
    
    self.bottomLine = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomLine];
    
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(1));
    }];
    
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
    }];
    
    [self.userHeadPicture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left).offset(-kSizeScale(6));
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(30), kSizeScale(30)));
    }];
    
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left).offset(-kSizeScale(6));
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellClickEvent:)]];
}



- (void)setIsShowHead:(BOOL)isShowHead {
    _isShowHead = isShowHead;
    self.userHeadPicture.hidden = !_isShowHead;
    self.rightLabel.hidden = _isShowHead;
}

- (void)setIsHideArrowImg:(BOOL)isHideArrowImg {
    _isHideArrowImg = isHideArrowImg;
    self.arrowImageView.hidden = _isHideArrowImg;
}

- (void)setSetTitle:(NSString *)setTitle {
    _setTitle = setTitle;
    self.leftLabel.text = _setTitle;
}

- (void)setSetContent:(NSString *)setContent {
    _setContent = setContent;
    if (!self.isShowHead) {
        self.rightLabel.text = _setContent;
    }
    else {
        [self.userHeadPicture sd_setImageWithURL:[NSURL URLWithString:_setContent] placeholderImage:[UIImage imageNamed:@"user_default_icon"]];
    }
}

#pragma mark - Event
- (void)cellClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.cellClickBlock) {
        self.cellClickBlock(self);
    }
}

@end
