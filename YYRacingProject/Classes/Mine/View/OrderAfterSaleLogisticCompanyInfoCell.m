//
//  OrderAfterSaleLogisticCompanyInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleLogisticCompanyInfoCell.h"

@interface OrderAfterSaleLogisticCompanyInfoCell()

@end

@implementation OrderAfterSaleLogisticCompanyInfoCell

- (void)createUI {
    self.cellTipLabel = [YYCreateTools createLabel:@"物流公司"
                                              font:midFont(14)
                                         textColor:XHBlackLitColor];
    [self.contentView addSubview:self.cellTipLabel];
    
    self.lineView = [YYCreateTools createView:HexRGB(0xf1f1f1)];
    [self.contentView addSubview:self.lineView];
    
    self.contentLabel = [YYCreateTools createLabel:@"请选择"
                                              font:midFont(14)
                                         textColor:XHBlackColor];
    self.contentLabel.textAlignment = NSTextAlignmentRight;
    self.contentLabel.userInteractionEnabled = YES;
    [self.contentView addSubview:self.contentLabel];

    self.arrowImageView = [YYCreateTools createImageView:@"mine_more_icon"
                                               viewModel:-1];
    [self.contentView addSubview:self.arrowImageView];
    
    [self.cellTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(16));
        make.width.mas_equalTo(kSizeScale(60));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(16));
    }];

    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(10));
        make.centerY.equalTo(self.cellTipLabel);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(6),kSizeScale(10)));
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cellTipLabel);
        make.right.equalTo(self.arrowImageView.mas_left).offset(-kSizeScale(5));
        make.left.equalTo(self.cellTipLabel.mas_right).offset(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(30));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cellTipLabel);
        make.height.mas_equalTo(kSizeScale(0.5));
        make.right.mas_equalTo(-kSizeScale(16));
        make.bottom.equalTo(self.contentView);
    }];
}

@end
