//
//  MinePhotoLayoutInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MinePhotoLayoutInfoCell.h"

@interface MinePhotoLayoutInfoCell()

@property (nonatomic, strong) UIButton    *closeBtn;
@property (nonatomic, strong) UIImageView *showImageView;

@end


@implementation MinePhotoLayoutInfoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    self.showImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self addSubview:self.showImageView];
    
    [self.showImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addPhotoClickEvent:)]];
    
    [self.showImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self);
        make.top.mas_equalTo(kSizeScale(6));
        make.right.mas_equalTo(-kSizeScale(6));
    }];
    
    self.closeBtn = [YYCreateTools createBtnImage:@"certificate_photo_delete_icon"];
    [self.closeBtn addTarget:self action:@selector(deleteClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.closeBtn];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(13), kSizeScale(13)));
    }];
}

- (void)setShowImage:(id)showImage {
    _showImage = showImage;
    if (_showImage) {
        if ([showImage isKindOfClass:[NSString class]]) {
            [self.showImageView sd_setImageWithURL:[NSURL URLWithString:showImage]];
            self.closeBtn.hidden = NO;
        }else{
            self.showImageView.image = _showImage;
            self.closeBtn.hidden = NO;
        }
    }
    else {
        self.closeBtn.hidden = YES;
        self.showImageView.image = [UIImage imageNamed:@"certificate_photo_select_icon"];
    }
}

- (void)setCurrentIndex:(NSIndexPath *)currentIndex {
    _currentIndex = currentIndex;
}


- (void)deleteClickEvent:(UIButton *)sender {
    if (self.deletePhotoBlock) {
        self.deletePhotoBlock(self.currentIndex);
    }
}

- (void)addPhotoClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (!self.showImage) {
        if (self.modifyPhotoBlock) {
            self.modifyPhotoBlock(self.currentIndex, self.showImage);
        }
    }
}


@end
