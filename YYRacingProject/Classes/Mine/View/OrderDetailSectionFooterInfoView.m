//
//  OrderDetailSectionFooterInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/21.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderDetailSectionFooterInfoView.h"

#import "OrderListInfoModel.h"
@interface OrderDetailSectionFooterInfoView()

@property (nonatomic, strong) NSMutableArray *footerTipArrs;
@property (nonatomic, strong) NSMutableArray *footerContentArrs;

@property (nonatomic, strong) UIView   *footerView;
@property (nonatomic, strong) UIView   *tipBackgroundView;
@property (nonatomic, strong) YYLabel  *tipContentLabel;
@property (nonatomic, strong) UIView   *bottomLineView;

@property (nonatomic, strong) UIButton *linkKefuBtn;
@property (nonatomic, strong) UIView   *kefuView;

@end

@implementation OrderDetailSectionFooterInfoView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    //提前创建模型
    self.footerView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.footerView];

    [self.footerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
    }];
    //客服
    self.kefuView = [YYCreateTools createView:XHLightColor];
    [self addSubview:self.kefuView];
    [self.kefuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.footerView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(52)).priorityHigh();
    }];
    
    self.linkKefuBtn = [YYCreateTools createButton:@"联系客服"
                                           bgColor:XHWhiteColor
                                         textColor:XHBlackColor];
    [self.linkKefuBtn setImage:[UIImage imageNamed:@"link_kefu_icon"] forState:UIControlStateNormal];
    [self.linkKefuBtn addTarget:self action:@selector(linkKefuBtnClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.linkKefuBtn layoutButtonWithEdgeInsetsStyle:LLButtonStyleTextRight imageTitleSpace:5];
    [self.kefuView addSubview:self.linkKefuBtn];
    [self.linkKefuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.kefuView);
        make.height.mas_equalTo(kSizeScale(46));
        make.top.mas_equalTo(kSizeScale(1));
    }];

    //提示
    self.tipBackgroundView = [YYCreateTools createView:HexRGB(0xFDF0D5)];
    [self addSubview:self.tipBackgroundView];
    
    [self.tipBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.kefuView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(55)).priorityHigh();
    }];
    
    self.tipContentLabel = [YYCreateTools createLabel:@"请不要点击蜀黍之家平台以外的网站链接进行售后操作，或向任何人透露银行卡密码信息，谨防受骗。"
                                               font:midFont(12)
                                          textColor:HexRGB(0xFF9400)
                                           maxWidth:kScreenW-(kSizeScale(12) * 2)
                                      fixLineHeight:kSizeScale(18)];
    self.tipContentLabel.numberOfLines = 2;
    [self.tipBackgroundView addSubview:self.tipContentLabel];
    
    [self.tipContentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.tipBackgroundView);
    }];
    self.bottomLineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:self.bottomLineView];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.tipBackgroundView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(8)).priorityHigh();
        make.bottom.equalTo(self);
    }];
}

- (void)setSummaryList:(NSMutableArray *)summaryList {
    _summaryList = summaryList;
    if (kValidArray(_summaryList)) {
        self.footerTipArrs = [NSMutableArray array];
        self.footerContentArrs = [NSMutableArray array];
        __block NSMutableArray *cateTitles = [NSMutableArray array];
        __block NSMutableArray *cateContents = [NSMutableArray array];
        __block NSString       *payMoney = @"";
        [_summaryList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            OrderPriceInfoModel *priceModel = (OrderPriceInfoModel *)obj;
            NSInteger type = [priceModel.type integerValue];
            NSString  *cateTitle = nil , *cateContent = nil;
            cateContent = priceModel.priceTag;
            if (type == 3) {
                cateTitle = @"商品金额：";
            }
            else if (type == 4) {
                cateTitle = @"订单总计：";
                payMoney = cateContent;
            }
            else if (type == 5) {
                cateTitle = @"运费：";
            }
            else if (type == 6) {
                cateTitle = @"进口税：";
            }
            else if (type == 7) {
                cateTitle = @"平台券：";
            }
            else if (type == 8) {
                cateTitle = @"平台促销：";
            }
            else if (type == 9) {
                cateTitle = @"商家券：";
            }
            else if (type == 10) {
                cateTitle = @"合计优惠：";
            }
            priceModel.title = cateTitle;
            
            [cateTitles addObject:priceModel];
            [cateContents addObject:priceModel];
        }];
        if (kValidArray(_summaryList)) {
            OrderPriceInfoModel *model = [[OrderPriceInfoModel alloc] init];
            model.type = @(-1);
            model.title = @"实付金额：";
            model.price = @([payMoney floatValue]);
            model.priceTag = payMoney;
            [cateTitles addObject:model];
            [cateContents addObject:model];
        }
        [self.footerView removeAllSubviews];
        for (int i = 0; i < cateTitles.count; i ++) {
            UILabel *lbl = [YYCreateTools createLabel:nil
                                                 font:midFont(12)
                                            textColor:XHBlackLitColor];
            [self.footerView addSubview:lbl];
            [self.footerTipArrs addObject:lbl];
        }
        for (int i = 0; i < cateContents.count; i ++) {
            UILabel *lbl = [YYCreateTools createLabel:nil
                                                 font:midFont(12)
                                            textColor:XHBlackLitColor];
            lbl.textAlignment = NSTextAlignmentRight;
            [self.footerView addSubview:lbl];
            [self.footerContentArrs addObject:lbl];
        }
        [self handleOrderMoneyShow:YES];
        [self handleOrderMoneyShow:NO];
        [self.footerTipArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UILabel *contentLbl = (UILabel *)obj;
            OrderPriceInfoModel *priceModel = cateTitles[idx];
            if (idx < cateContents.count) {
                contentLbl.hidden = NO;
                contentLbl.text = priceModel.title;
            }
            else {
                contentLbl.hidden = YES;
            }
        }];
        
        [self.footerContentArrs enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            UILabel *contentLbl = (UILabel *)obj;
            OrderPriceInfoModel *priceModel = cateContents[idx];
            if (idx < cateContents.count) {
                contentLbl.hidden = NO;
                NSInteger type = [priceModel.type integerValue];
                if ((type > 6 && type <= 10)) {
                    contentLbl.text = [NSString stringWithFormat:@"-¥ %@",priceModel.priceTag];
                }
                else {
                    contentLbl.text = [NSString stringWithFormat:@"¥ %@",priceModel.priceTag];
                }
                
                if (type == -1 || (type > 6 && type <= 10)) {
                    contentLbl.font = boldFont(14);
                    contentLbl.textColor = XHRedColor;
                }
                else {
                    contentLbl.font = midFont(12);
                    contentLbl.textColor = XHBlackLitColor;
                }
            }
            else {
                contentLbl.hidden = YES;
            }
        }];
    }
}

- (void)handleOrderMoneyShow:(BOOL)isLeft {
    UILabel *tempLabel = nil;
    NSMutableArray *subViews = (isLeft == YES ? self.footerTipArrs : self.footerContentArrs);
    for (int i = 0; i < subViews.count; i ++) {
        UILabel *lbl = [subViews objectAtIndex:i];
        if (tempLabel) {
            if (i == subViews.count - 1) {
                [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                    if (isLeft) {
                         make.left.equalTo(tempLabel);
                    }
                    else {
                         make.right.equalTo(tempLabel);
                    }
                    make.top.equalTo(tempLabel.mas_bottom).offset(kSizeScale(10));
                    make.bottom.equalTo(lbl.superview.mas_bottom).offset(-kSizeScale(10)).priorityHigh();
                }];
            }
            else {
                [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                    if (isLeft) {
                        make.left.equalTo(tempLabel);
                    }
                    else {
                        make.right.equalTo(tempLabel);
                    }
                    make.top.equalTo(tempLabel.mas_bottom).offset(kSizeScale(10));
                }];
            }
        }
        else {
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                if (isLeft) {
                    make.left.mas_equalTo(kSizeScale(12));
                }
                else {
                    make.right.mas_equalTo(-kSizeScale(12));
                }
                make.top.mas_equalTo(kSizeScale(10));
            }];
        }
        tempLabel = lbl;
    }
}

- (NSString *)gainCorrespondingMoneyShow:(NSInteger)index {
    __block NSInteger type = index;
    __block NSString *price = [NSString string];
    [self.summaryList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        OrderPriceInfoModel *model = (OrderPriceInfoModel *)obj;
        if ([model.type integerValue] == type) {
            price = [NSString stringWithFormat:@"¥ %@",model.priceTag];
            *stop = YES;
        }
    }];
    return price;
}

#pragma mark - Event
- (void)linkKefuBtnClickEvent:(UIButton *)sender {
    if (self.kefuClickBlock) {
        self.kefuClickBlock();
    }
}
@end
