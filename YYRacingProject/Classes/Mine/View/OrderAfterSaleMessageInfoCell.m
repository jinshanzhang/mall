//
//  OrderAfterSaleMessageInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleMessageInfoCell.h"

#import "OrderAfterSaleCertificateInfoView.h"
@interface OrderAfterSaleMessageInfoCell()

@property (nonatomic, strong) UIView   *afterSaleBackgroundView;

@property (nonatomic, strong) UILabel  *afterStatuLabel;
@property (nonatomic, strong) UILabel  *afterTimeLabel;
@property (nonatomic, strong) UILabel  *afterDescriLabel; //描述
@property (nonatomic, strong) UIView   *lineView;

@property (nonatomic, strong) UIView   *otherView;
@property (nonatomic, strong) OrderAfterSaleMessageTimeInfoView *timeView;
@property (nonatomic, copy)   NSString *address;

@end

@implementation OrderAfterSaleMessageInfoCell

- (void)createUI {
    self.contentView.backgroundColor = XHLightColor;
    
    self.timeView = [[OrderAfterSaleMessageTimeInfoView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.timeView];
    
    [self.timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(56));
    }];
    
    self.afterSaleBackgroundView = [YYCreateTools createView:XHWhiteColor];
    self.afterSaleBackgroundView.layer.cornerRadius = 4;
    self.afterSaleBackgroundView.clipsToBounds = YES;
    [self.contentView addSubview:self.afterSaleBackgroundView];
    
    [self.afterSaleBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.top.equalTo(self.timeView.mas_bottom).offset(kSizeScale(10));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(10));
    }];
    
    self.afterStatuLabel = [YYCreateTools createLabel:nil
                                                 font:midFont(14)
                                            textColor:XHWhiteColor];
    [self.afterSaleBackgroundView addSubview:self.afterStatuLabel];
    
    self.afterTimeLabel = [YYCreateTools createLabel:nil
                                                 font:midFont(14)
                                            textColor:XHWhiteColor];
    [self.afterSaleBackgroundView addSubview:self.afterTimeLabel];
    
    self.lineView = [YYCreateTools createView:HexRGB(0xf1f1f1)];
    [self.afterSaleBackgroundView addSubview:self.lineView];
    
    self.otherView = [YYCreateTools createView:XHWhiteColor];
    [self.afterSaleBackgroundView addSubview:self.otherView];
    
    [self.afterStatuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    make.left.equalTo(self.afterSaleBackgroundView.mas_left).offset(kSizeScale(10));
    make.top.equalTo(self.afterSaleBackgroundView.mas_top).offset(kSizeScale(10));
    }];
    
    [self.afterTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.afterSaleBackgroundView.mas_right).offset(-kSizeScale(10));
        make.centerY.equalTo(self.afterStatuLabel);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.afterStatuLabel);
        make.right.equalTo(self.afterTimeLabel);
        make.top.equalTo(self.afterStatuLabel.mas_bottom).offset(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(0.5));
    }];
    
    [self.otherView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.lineView);
        make.top.equalTo(self.lineView.mas_bottom);
        make.bottom.equalTo(self.afterSaleBackgroundView.mas_bottom);
    }];
}


- (void)setMessageModel:(OrderAfterSaleDetailMessageInfoModel *)messageModel {
    _messageModel = messageModel;
    BOOL isAddCertificate = NO;
    BOOL isFirst = _messageModel.isFirst;
    NSMutableArray *labelKeyArrays = [NSMutableArray array];
    NSMutableArray *labelValueArrays = [NSMutableArray array];
    OrderAfterSaleCertificateInfoView *certificationView = nil;
    
    [self.otherView removeAllSubviews];
    // 是否追加凭证
    if (kValidArray(_messageModel.urls)) {
        isAddCertificate = YES;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM月dd HH:mm"];
    self.timeView.messageTime = [NSString formatDateAndTime:_messageModel.createdTime
                                                  formatter:formatter];
    self.afterTimeLabel.text = kValidString(_messageModel.remainTime)?_messageModel.remainTime:@"";
    self.afterStatuLabel.text = _messageModel.title;
    // 复合视图
    NSMutableArray *tempArray = [self addItemToAfterSaleBackgroundView:self.otherView messageModel:_messageModel];
    if (kValidArray(tempArray) && tempArray.count == 2) {
        labelKeyArrays = [tempArray firstObject];
        labelValueArrays = [tempArray lastObject];
    }
    // 复合布局
    UIView *lastLbl = [self addTOAfterSaleBackgroundSubViewLayout:labelKeyArrays valueLables:labelValueArrays isAppendCertificate:isAddCertificate];
    if (lastLbl && isAddCertificate) {
        certificationView = [[OrderAfterSaleCertificateInfoView alloc] initWithFrame:CGRectZero];
        [self.otherView addSubview:certificationView];
        certificationView.imageUrls = _messageModel.urls;
        [certificationView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.otherView.mas_left).offset(kSizeScale(10));
            make.top.equalTo(lastLbl.mas_bottom).offset(kSizeScale(8));
            make.height.mas_equalTo(kSizeScale(45));
            make.right.equalTo(self.otherView.mas_right).offset(-kSizeScale(10));
            make.bottom.equalTo(self.otherView.mas_bottom).offset(-kSizeScale(10)).priorityHigh();
        }];
    }
    if (isFirst) {
        self.lineView.backgroundColor = XHWhiteColor;
        self.afterSaleBackgroundView.backgroundColor = self.otherView.backgroundColor = XHLoginColor;
        self.afterStatuLabel.textColor = self.afterTimeLabel.textColor = XHWhiteColor;
        [self changeOtherViewSubViewTextColor:XHWhiteColor subViews:labelKeyArrays];
        [self changeOtherViewSubViewTextColor:XHWhiteColor subViews:labelValueArrays];
        if (certificationView) {
            certificationView.contentColor = XHWhiteColor;
        }
    }
    else {
        self.lineView.backgroundColor = HexRGB(0xF1F1F1);
        self.afterSaleBackgroundView.backgroundColor = self.otherView.backgroundColor = XHWhiteColor;
        self.afterStatuLabel.textColor = self.afterTimeLabel.textColor = XHBlackColor;
        [self changeOtherViewSubViewTextColor:XHBlackColor subViews:labelKeyArrays];
        [self changeOtherViewSubViewTextColor:XHBlackColor subViews:labelValueArrays];
        if (certificationView) {
            certificationView.contentColor = XHBlackColor;
        }
    }
}
#pragma mark - Private method
- (NSMutableArray *)addItemToAfterSaleBackgroundView:(UIView *)superView
                                        messageModel:(OrderAfterSaleDetailMessageInfoModel *)messageModel {
    NSMutableArray *tempLbl = [NSMutableArray array];
    __block UIView  *tempSupView = superView;
    __block NSMutableArray *keyLabels = [NSMutableArray array];
    __block NSMutableArray *valueLabels = [NSMutableArray array];
    [messageModel.contentList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        OrderAfterSaleMessageContentInfoModel *model = (OrderAfterSaleMessageContentInfoModel *)obj;
        // key
        UILabel *keyLabel = [YYCreateTools createLabel:kValidString(model.title)?[NSString stringWithFormat:@"%@：",model.title]:nil
                                                  font:midFont(14)
                                             textColor:XHBlackColor];
        [keyLabels addObject:keyLabel];
        [tempSupView addSubview:keyLabel];
        
        // value
        UILabel *valueLabel = [YYCreateTools createLabel:kValidString(model.describe)?model.describe:nil
                                                  font:midFont(14)
                                             textColor:XHBlackColor];
      
        valueLabel.numberOfLines = 0;
        [valueLabels addObject:valueLabel];
        valueLabel.userInteractionEnabled = YES;
        [tempSupView addSubview:valueLabel];
        if ([model.title isEqualToString:@"收件地址"]) {
            self.address = model.describe;
            [valueLabel addGestureRecognizer:[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longPressCopyEvent:)]];
        }
    }];
    [tempLbl addObject:keyLabels];
    [tempLbl addObject:valueLabels];
    return tempLbl;
}

- (void)longPressCopyEvent:(UIGestureRecognizer *)gesture {
    [YYCommonTools pasteboardCopy:self.address
                     appendParams:@{@"title":@"复制成功"}];
}

- (UIView *)addTOAfterSaleBackgroundSubViewLayout:(NSMutableArray *)keyLabels
                                  valueLables:(NSMutableArray *)valueLabels
                          isAppendCertificate:(BOOL)isAppendCertificate {
    
    CGFloat   width = 0.0;
    UILabel   *lastLbl = nil;
    UILabel   *valueLastLbl = nil;
    // 内容布局
    for (int i = 0; i < valueLabels.count; i ++) {
        UILabel *keylbl = [keyLabels objectAtIndex:i];
        UILabel *lbl = [valueLabels objectAtIndex:i];
        width = [YYCommonTools sizeWithText:keylbl.text
                                       font:midFont(14)
                                    maxSize:CGSizeMake(MAXFLOAT, kSizeScale(25))].width + kSizeScale(1);
        if (lastLbl) {
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.otherView.mas_left).offset(width);
                make.top.equalTo(lastLbl.mas_bottom).offset(kSizeScale(8));
                make.right.equalTo(self.otherView);
            }];
        }
        else {
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.otherView.mas_left).offset(width);
                make.top.equalTo(self.otherView.mas_top).offset(kSizeScale(10));
                make.right.equalTo(self.otherView);
            }];
        }
        lastLbl = lbl;
        if (!isAppendCertificate) {
            // 不需要追加凭证
            if (i == valueLabels.count - 1) {
                [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.otherView.mas_bottom).offset(-kSizeScale(10));
                }];
            }
        }
    }
    // key 布局
    for (int i = 0; i < keyLabels.count ; i ++) {
        UILabel *valueLbl = [valueLabels objectAtIndex:i];
        UILabel *lbl = [keyLabels objectAtIndex:i];
        if (valueLastLbl) {
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.otherView.mas_left);
                make.top.equalTo(valueLbl);
            }];
        }
        else {
            [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.otherView.mas_left);
                make.top.equalTo(valueLbl);
            }];
        }
        valueLastLbl = lbl;
    }
    return lastLbl;
}


- (void)changeOtherViewSubViewTextColor:(UIColor *)textColor
                               subViews:(NSMutableArray *)subViews {
    for (int i = 0; i < subViews.count; i ++) {
        UILabel *lbl = [subViews objectAtIndex:i];
        lbl.textColor = textColor;
    }
}


@end
