//
//  AfterSaleCommonCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSaleCommonCell.h"
#import "UITextView+Placeholder.h"

@implementation AfterSaleCommonCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

-(void)setType:(UseCellType)type{
    _type = type;
    
    for(UIView *view in self.contentView.subviews){
        if(view){
            [view removeFromSuperview];
        }
    }
    if (self.type == CellwithOneTextFild) {
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.showTextField];
        self.titleLabel.font = normalFont(14);
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(16));
            make.top.mas_equalTo(15);
            make.bottom.mas_equalTo(-15);
        }];
        [self.showTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(85));
            make.centerY.equalTo(self.titleLabel);
            make.right.mas_equalTo(kSizeScale(-12));
        }];
    }else if (self.type == CellWithNumberView){
        
        [self.contentView addSubview:self.titleLabel];
        
        self.titleLabel.font = normalFont(14);
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(16));
            make.top.mas_equalTo(10);
            make.bottom.mas_equalTo(-10);
        }];
        
        [self addSubview:self.numberView];
        [self.numberView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(85));
            make.centerY.equalTo(self);
            make.width.mas_equalTo(kSizeScale(85));
            make.height.mas_equalTo(kSizeScale(25));
        }];
        @weakify(self);
        self.numberView.addClick = ^(NSInteger number) {
            @strongify(self)
            if (self.cellNumerBlock) {
                self.cellNumerBlock(number);
            }
        };
        self.numberView.reduceClick = ^(NSInteger number) {
            @strongify(self);
            if (self.cellNumerBlock) {
                self.cellNumerBlock(number);
            }
        };
  
    }else if (self.type == CellWithTwoLabelAndArrow){
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.detailLabel];
        
        self.titleLabel.font = normalFont(14);
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(16));
            make.top.mas_equalTo(15);
            make.bottom.mas_equalTo(-15);
        }];
        
        self.arrowImageView = [YYCreateTools createImageView:@"cellMore_icon"
                                                   viewModel:-1];
        [self.contentView addSubview:self.arrowImageView];
        [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.mas_equalTo(-kSizeScale(9));
            make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
        }];
        self.detailLabel.font = normalFont(14);
        self.detailLabel.textAlignment = NSTextAlignmentRight;

        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(85));
            make.right.mas_equalTo(kSizeScale(-20));
            make.bottom.mas_equalTo(-15);
        }];
    }
    else if (self.type == CellWithTextView){
 
        [self.contentView addSubview:self.showTextView];
        [self.showTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(5);
            make.left.mas_equalTo(kSizeScale(9));
            make.right.mas_equalTo(-kSizeScale(16));
            make.bottom.mas_equalTo(-10);
            make.height.mas_equalTo(120);
        }];
        
        
        [self.contentView addSubview:self.detailLabel];
        
        [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-kSizeScale(16));
            make.bottom.mas_equalTo(-16);
        }];
        self.detailLabel.text = @"0/100";
        
    }
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(kSizeScale(-16));
        make.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
}


-(void)createUI{
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints =NO;
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
    [self.contentView addSubview:self.detailLabel];
    
    self.showTextField = [YYCreateTools createTextField:@"" font:normalFont(13) texColor:XHBlackColor];
    [self.contentView addSubview:_showTextField];
    
    
    self.showTextField.leftViewMode = UITextFieldViewModeAlways;
    UIImageView *loginImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftmoney_icon"]];
    UIView *lv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    loginImgV.center = lv.center;
    [lv addSubview:loginImgV];
    _showTextField.leftView = lv;

    

    self.showTextView = [[UITextView alloc] init];
    self.showTextView.placeholder = @"请输入不少于10个字的说明";
    self.showTextView.placeholderAttributes = @{NSForegroundColorAttributeName:XHBlackLitColor,NSFontAttributeName:normalFont(14)};
    self.showTextView.textColor = XHBlackColor;
    self.showTextView.font = normalFont(14);
    self.showTextView.delegate = self;
    self.showTextView.backgroundColor = XHWhiteColor;
    
    
    self.numberView = [[YYNumberView alloc] initWithFrame:CGRectZero];
    self.numberView.afterSaleTip = @"申请数量已达上限";
    self.numberView.minValue = 1;
    self.numberView.isOpenTipMsg = YES;
    self.numberView.isOpenTenLimit = YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    self.detailLabel.text = [NSString stringWithFormat:@"%lu/100", (unsigned long)textView.text.length];
    
    //字数限制操作
    if (textView.text.length >= 100) {
        
        textView.text = [textView.text substringToIndex:100];
        
        self.detailLabel.text = @"100/100";
    }
}




@end
