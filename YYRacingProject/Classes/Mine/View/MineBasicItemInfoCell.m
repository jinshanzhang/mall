//
//  MineBasicItemInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "MineBasicItemInfoCell.h"

#import "MineBasicItemView.h"
#import "MineItemContentInfoModel.h"
@interface MineBasicItemInfoCell ()
<MineBasicItemViewDelegate>
@property (nonatomic, strong) UIImageView *basicBackgroundView;
@property (nonatomic, strong) MineBasicItemView *couponView;

@property (nonatomic, strong) NSMutableArray  *itemViews;
@property (nonatomic, strong) MASConstraint   *couponHeight;

@end


@implementation MineBasicItemInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = XHStoreGrayColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    NSInteger count = 0;
    MineBasicItemView *tempItemView = nil;
    self.itemViews = [NSMutableArray array];
    
    self.basicBackgroundView = [YYCreateTools createImageView:nil
                                                    viewModel:-1];
    [self.basicBackgroundView zy_cornerRadiusAdvance:kSizeScale(5)
                                      rectCornerType:UIRectCornerAllCorners];
    self.basicBackgroundView.image = [UIImage imageWithColor:XHWhiteColor];

    [self.contentView addSubview:self.basicBackgroundView];
    
    self.couponView = [[MineBasicItemView alloc] initWithFrame:CGRectZero];
    self.couponView.delegate = self;
    [self.contentView addSubview:self.couponView];
    
    if (kIsTest) {
        count = 4;
    }
    else {
        count = 3;
    }
    for (int i = 0; i < count; i ++) {
        MineBasicItemView *itemView = [[MineBasicItemView alloc] initWithFrame:CGRectZero];
        itemView.delegate = self;
        [self.contentView addSubview:itemView];
        [self.itemViews addObject:itemView];
    }
    
    [self.basicBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.bottom.equalTo(self.contentView);
    }];
    
    [self.couponView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.basicBackgroundView);
        self.couponHeight = make.height.mas_equalTo(kSizeScale(48)).priorityHigh();
    }];
    
    for (int i = 0; i < self.itemViews.count ; i ++) {
        MineBasicItemView *subView = self.itemViews[i];
        if (tempItemView) {
            [subView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.height.equalTo(tempItemView);
                make.top.equalTo(tempItemView.mas_bottom);
                if (i == self.itemViews.count - 1) {
                    make.bottom.equalTo(self.basicBackgroundView).offset(0);
                }
            }];
        }
        else {
            [subView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(self.couponView);
                make.top.equalTo(self.couponView.mas_bottom);
                make.height.mas_equalTo(kSizeScale(48));
                if (i == self.itemViews.count - 1) {
                    make.bottom.equalTo(self.basicBackgroundView).offset(0);
                }
            }];
        }
        tempItemView = subView;
    }
}

- (void)setItemArray:(NSMutableArray *)itemArray {
    _itemArray = itemArray;
    if (kValidArray(itemArray)) {
        NSArray *subArray = [NSArray array];
        MineItemContentInfoModel *firstModel = [itemArray firstObject];
        if (firstModel.type == 0) {
            [self.couponHeight install];
            [self.couponView setViewModel:firstModel];
            subArray = [itemArray subarrayWithRange:NSMakeRange(1, itemArray.count-1)];
        }
        else {
            [self.couponHeight uninstall];
            subArray = itemArray;
        }
        
        if (kValidArray(subArray)) {
            NSMutableArray *tempArray = [subArray mutableCopy];
            [tempArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {\
                if (idx < self.itemViews.count) {
                    MineBasicItemView *view = [self.itemViews objectAtIndex:idx];
                    [view setViewModel:obj];
                }
            }];
        }
    }
}

#pragma mark - MineBasicItemViewDelegate
- (void)basicViewClickEvent:(id)viewModel {
    if (viewModel && self.cellViewClickBlock) {
        self.cellViewClickBlock(viewModel);
    }
}
@end
