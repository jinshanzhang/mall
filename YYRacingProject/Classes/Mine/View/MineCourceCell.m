//
//  MineCourceCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "MineCourceCell.h"
#import "UIColor+Extension.h"
@interface MineCourceCell()

@property (nonatomic, strong) UIImageView *showIMG;
@property (nonatomic, strong) UILabel     *chapterLabel;
@property (nonatomic, strong) UILabel    *actionLabel;
@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, strong) UIView   *lineView;

@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *commissionLabel;

@end
@implementation MineCourceCell

-(void)createUI{
    
    self.showIMG = [YYCreateTools createImageView:@"" viewModel:-1];
    self.showIMG.v_cornerRadius = 2;
    [self addSubview:self.showIMG];
    
    [self.showIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(15);
        make.width.mas_equalTo(105);
        make.height.mas_equalTo(105);
    }];
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(15) textColor:XHBlackColor];
    self.titleLabel.numberOfLines = 2;
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(13);
        make.left.mas_equalTo(self.showIMG.mas_right).mas_offset(12);
    }];

    self.chapterLabel = [YYCreateTools createLabel:@"" font:normalFont(11) textColor:XHBlackLitColor];
    [self addSubview:self.chapterLabel];
    
    [self.chapterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.showIMG.mas_right).mas_offset(12);
        make.bottom.mas_equalTo(self.showIMG.mas_bottom).mas_offset(-5);
    }];
    
    // 价格视图上view
    self.priceLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(15)
                                       textColor:XHWhiteColor];
    self.priceLabel.hidden = YES;
    [self addSubview:self.priceLabel];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.showIMG.mas_right).mas_offset(12);
        make.bottom.mas_equalTo(self.showIMG.mas_bottom).mas_offset(-5);
        make.height.mas_equalTo(kSizeScale(20));
    }];
    
    
    self.commissionLabel = [YYCreateTools createLabel:nil
                                            font:boldFont(18)
                                       textColor:XHWhiteColor];
    [self addSubview:self.commissionLabel];
    self.commissionLabel.hidden  = YES;
    [self.commissionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLabel.mas_right).offset(kSizeScale(12));
        make.centerY.equalTo(self.priceLabel);
        make.height.mas_equalTo(kSizeScale(20));
    }];
    
    
    self.actionLabel = [YYCreateTools createLabel:@"去学习" font:boldFont(11) textColor:XHWhiteColor];
//    self.actionLabel.backgroundColor = [UIColor addGoldGradient:CGSizeMake(kSizeScale(70), kSizeScale(24))];
    self.actionLabel.backgroundColor =  XHMainColor;
    self.actionLabel.textAlignment = NSTextAlignmentCenter;
    self.actionLabel.v_cornerRadius =2;
    
    [self addSubview:self.actionLabel];
    [self.actionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-16);
        make.bottom.mas_equalTo(self.showIMG.mas_bottom).mas_offset(-5);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(24);
    }];
    
    self.lineView =[YYCreateTools createView:XHLightColor];
    [self addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
        make.top.mas_equalTo(self.showIMG.mas_bottom).mas_offset(16);
        make.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

-(void)setModel:(MyCourceModel *)model{
    _model = model;
    [self.showIMG sd_setImageWithURL:[NSURL URLWithString:model.coverImage.imageUrl]];
    self.titleLabel.text = model.title;
    self.chapterLabel.text = model.lessonCountTitle;
}

-(void)setGroomModel:(groomGoodsModel *)groomModel{
    _groomModel = groomModel;
    
    [self.showIMG sd_setImageWithURL:[NSURL URLWithString:groomModel.imageUrl]];
    
    self.titleLabel.text = groomModel.title;
    
    self.actionLabel.text = @"立即购买";
    self.priceLabel.hidden =NO;
    self.commissionLabel.hidden = NO;
    
    if (kIsFan) {
        // 现在价格
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(13) symbolTextColor:XHRedColor wordSpace:2 price:kValidString(groomModel.price)?groomModel.price:@"0" priceFont:boldFont(18) priceTextColor: XHRedColor symbolOffsetY:0];
        self.priceLabel.attributedText = attribut;
        //原价
        NSMutableAttributedString *origin = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",kValidString(groomModel.originPrice)?groomModel.originPrice:@"0"]];
        [origin yy_setAttributes:XHBlackLitColor
                            font:midFont(18)
                         content:origin
                       alignment:NSTextAlignmentCenter];
        [origin addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, origin.length)];
        [origin addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle)range:NSMakeRange(0,origin.length)];
        if (@available(iOS 10.3, *)) {
            [origin addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)}range:NSMakeRange(0,origin.length)];
        }
        self.commissionLabel.attributedText = origin;
    }else{
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(13) symbolTextColor:XHBlackColor wordSpace:2 price:groomModel.price priceFont:boldFont(18) priceTextColor: XHBlackColor symbolOffsetY:0];
        self.priceLabel.attributedText = attribut;
        NSMutableAttributedString *extra = [YYCommonTools containSpecialSymbolHandler:@"赚" symbolFont:midFont(13) symbolTextColor: XHRedColor wordSpace:4 price:groomModel.commission priceFont:boldFont(18) priceTextColor:XHRedColor symbolOffsetY:1.2];
        self.commissionLabel.attributedText = extra;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
