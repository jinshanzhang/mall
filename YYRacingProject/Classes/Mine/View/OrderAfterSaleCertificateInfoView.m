//
//  OrderAfterSaleCertificateInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleCertificateInfoView.h"

#define  KBGIMAGETAG    400

@interface OrderAfterSaleCertificateInfoView()

@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) NSMutableArray *totalImageView;

@end

@implementation OrderAfterSaleCertificateInfoView

#pragma mark - Setter method
- (void)setContentColor:(UIColor *)contentColor {
    _contentColor = contentColor;
    self.tipLabel.textColor = _contentColor;
}

- (void)setImageUrls:(NSMutableArray *)imageUrls {
    _imageUrls = imageUrls;
    for (int i = 0; i < self.totalImageView.count; i ++) {
        UIImageView *bgImageView = [self.totalImageView objectAtIndex:i];
        if (i >= imageUrls.count) {
            bgImageView.hidden = YES;
        }
        else {
            [bgImageView sd_setImageWithURL:[NSURL URLWithString:[_imageUrls objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"aftersale_icon"]];
        }
    }
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.totalImageView = [NSMutableArray arrayWithCapacity:5];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.tipLabel = [YYCreateTools createLabel:@"凭证："
                                          font:midFont(14)
                                     textColor:XHBlackColor];
    [self addSubview:self.tipLabel];
    for (int i = 0; i < 5; i ++) {
        UIImageView *bg = [YYCreateTools createImageView:nil
                                               viewModel:-1];
        bg.tag = KBGIMAGETAG + i;
        [self addSubview:bg];
        bg.userInteractionEnabled = YES;
        [bg addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bgImageViewClickEvent:)]];
        [self.totalImageView addObject:bg];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIImageView *lastView = nil;
    CGFloat padding = kSizeScale(8);
    CGFloat imageW = kSizeScale(45);
    
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(45), kSizeScale(25)));
    }];
    
    for (int i = 0; i < self.totalImageView.count; i ++) {
        UIImageView *imageView = self.totalImageView[i];
        if (lastView) {
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastView.mas_right).offset(padding);
                make.size.equalTo(lastView);
                make.centerY.equalTo(lastView);
            }];
        }
        else {
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.tipLabel.mas_right);
                make.size.mas_equalTo(CGSizeMake(kSizeScale(imageW), kSizeScale(imageW)));
                make.top.equalTo(self);
            }];
        }
        lastView = imageView;
    }
}

#pragma mark - Private method
- (void)bgImageViewClickEvent:(UITapGestureRecognizer *)tapGesture {

    NSString *url;
    UIView *fromView;
    NSMutableArray *items = [NSMutableArray new];
    
    UIImageView *bg = (UIImageView *)tapGesture.view;
    NSInteger tag = bg.tag - KBGIMAGETAG;
    
    for (NSUInteger i = 0, max = self.imageUrls.count; i < max; i++) {
        url = [self.imageUrls objectAtIndex:i];
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = bg;
        item.largeImageURL = [NSURL URLWithString:url];
        
        [items addObject:item];
        if (i == tag) {
            fromView = bg;
        }
    }
    YYPhotoGroupView *v = [[YYPhotoGroupView alloc] initWithGroupItems:items currentPage:tag];
    [v presentFromImageView:fromView toContainer:kAppWindow animated:YES completion:nil];
    v.hideViewBlock = ^(NSInteger currentPage) {
        
    };
}
@end
