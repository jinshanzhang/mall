//
//  MinePhotoLayoutInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MinePhotoLayoutInfoCell : UICollectionViewCell

@property (nonatomic, copy) void (^modifyPhotoBlock)(NSIndexPath *index, UIImage *image);
@property (nonatomic, copy) void (^deletePhotoBlock)(NSIndexPath *index);

@property (nonatomic, strong) NSIndexPath *currentIndex;
@property (nonatomic, strong) id showImage;

@end
