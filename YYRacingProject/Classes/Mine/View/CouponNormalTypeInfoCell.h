//
//  CouponNormalTypeInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

#import "CouponListInfoModel.h"
@interface CouponNormalTypeInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) CouponListInfoModel *couponModel;
@property (nonatomic, copy) void (^immediatelyBlock)(CouponListInfoModel *couponModel);

@end
