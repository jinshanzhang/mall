//
//  MineBasicBottomInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/1.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "MineBasicBottomInfoCell.h"

@interface MineBasicBottomInfoCell()

@property (nonatomic, strong) UIImageView *bottomImageView;

@end

@implementation MineBasicBottomInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = self.backgroundColor = XHClearColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.bottomImageView = [YYCreateTools createImageView:nil
                                                viewModel:-1];
    self.bottomImageView.image = [UIImage imageWithColor:XHStoreGrayColor];
    [self.contentView addSubview:self.bottomImageView];
    
    [self.bottomImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(150));
    }];
}

@end
