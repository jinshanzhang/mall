//
//  OrderListGoodInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderGoodInfoModel.h"

@interface OrderListGoodInfoCell : YYBaseTableViewCell

@property (nonatomic, assign) BOOL  isHideGoodBottomLine;
@property (nonatomic, strong) OrderGoodInfoModel *goodModel;

@property (nonatomic, copy)   void (^afterSaleClick)(OrderGoodInfoModel *goodModel, YYGoodAfterSaleOperatorType operatorType);
@property (nonatomic, copy)   void (^orderClick)(OrderGoodInfoModel *goodModel,OrderListGoodInfoCell *cell);

@end
