//
//  OrderAfterSaleListInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderAfterSaleListInfoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface OrderAfterSaleListInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) OrderAfterSaleListInfoModel *afterSaleModel;
@property (nonatomic, copy)   void (^afterSaleClick)(OrderAfterSaleListInfoModel *saleModel,OrderAfterSaleListInfoCell *cell);

@end

NS_ASSUME_NONNULL_END
