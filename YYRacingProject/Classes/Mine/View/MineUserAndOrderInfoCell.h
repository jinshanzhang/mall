//
//  MineUserAndOrderInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineUserAndOrderInfoCell : UITableViewCell

@property (nonatomic, copy) void (^orderClickBlock)(NSInteger type);

- (void)setMineOrder:(NSMutableArray *)imageNames
              titles:(NSMutableArray *)titles
           redCounts:(NSMutableArray *)redCounts
            topSpace:(CGFloat)topSpace
           itemSpace:(CGFloat)itemSpace;

@end

NS_ASSUME_NONNULL_END
