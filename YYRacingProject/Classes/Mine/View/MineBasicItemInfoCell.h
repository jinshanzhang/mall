//
//  MineBasicItemInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MineBasicItemInfoCell : UITableViewCell

@property (nonatomic, copy) void (^cellViewClickBlock)(id itemModel);
@property (nonatomic, strong) NSMutableArray *itemArray;

@end

NS_ASSUME_NONNULL_END
