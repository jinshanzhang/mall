//
//  OrderAfterSaleCertificateInfoView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderAfterSaleCertificateInfoView : UIView

@property (nonatomic, strong) UIColor *contentColor;
@property (nonatomic, strong) NSMutableArray *imageUrls;

@end
