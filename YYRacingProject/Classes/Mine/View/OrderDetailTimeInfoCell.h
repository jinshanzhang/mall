//
//  OrderDetailTimeInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/23.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface OrderDetailTimeInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) NSNumber *orderNumber;
@property (nonatomic, strong) NSString *createTime;
@property (nonatomic, strong) NSString *payTime;
@property (nonatomic, strong) NSString *remarkContent;

@end
