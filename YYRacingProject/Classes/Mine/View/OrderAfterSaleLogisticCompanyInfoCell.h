//
//  OrderAfterSaleLogisticCompanyInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface OrderAfterSaleLogisticCompanyInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel  *cellTipLabel;
@property (nonatomic, strong) UILabel  *contentLabel;
@property (nonatomic, strong) UILabel  *defaultLabel;
@property (nonatomic, strong) UIImageView  *arrowImageView;
@property (nonatomic, strong) UIView   *lineView;

@end
