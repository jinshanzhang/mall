//
//  OrderListSectionHeaderInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OrderListInfoModel.h"
@interface OrderListSectionHeaderInfoView : UITableViewHeaderFooterView

@property (nonatomic, strong) OrderListInfoModel *listModel;
@property (nonatomic, strong) NSNumber *orderStatus;

@end
