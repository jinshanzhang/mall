//
//  OrderAfterSaleMessageKefuInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleMessageKefuInfoCell.h"

@interface OrderAfterSaleMessageKefuInfoCell()

@property (nonatomic, strong) UIView *backGroundView;
@property (nonatomic, strong) YYLabel *linkKefuLbl;

@end

@implementation OrderAfterSaleMessageKefuInfoCell

- (void)createUI {
    self.backGroundView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.backGroundView];
    [self.backGroundView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(linkKefuClickEvent:)]];
    
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(50));
    }];
    
    self.linkKefuLbl = [[YYLabel alloc] init];
    self.linkKefuLbl.font = midFont(14);
    self.linkKefuLbl.textColor = XHBlackLitColor;
    self.linkKefuLbl.textAlignment = NSTextAlignmentCenter;
    [self.backGroundView addSubview:self.linkKefuLbl];
    
    [self.linkKefuLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.backGroundView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(250), kSizeScale(25)));
    }];

    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"如需帮助，你可以向客服小贝进行咨询"];
    string.alignment = NSTextAlignmentCenter;
    //修改颜色
    [string addAttribute:NSForegroundColorAttributeName value:XHLoginColor range:NSMakeRange(9, 4)];
    self.linkKefuLbl.attributedText = string;
}

- (void)linkKefuClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.kefuClickBlock) {
        self.kefuClickBlock();
    }
}

@end
