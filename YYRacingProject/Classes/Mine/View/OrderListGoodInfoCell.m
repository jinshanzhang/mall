//
//  OrderListGoodInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderListGoodInfoCell.h"

#define kGoodOperatorTag  220
@interface OrderListGoodInfoCell()

@property (nonatomic, strong) UIImageView  *goodImageView;
@property (nonatomic, strong) YYLabel      *goodNameLabel;
@property (nonatomic, strong) UILabel      *goodSkuLabel;
@property (nonatomic, strong) UILabel      *goodPriceLabel;
@property (nonatomic, strong) UILabel      *commissionLabel;
@property (nonatomic, strong) UILabel      *goodNumLabel;

@property (nonatomic, strong) UIView       *bottomLine;
@property (nonatomic, strong) UIView       *afterSaleView;

@property (nonatomic, strong) MASConstraint *listTopPadding;
@property (nonatomic, strong) MASConstraint *detailTopPadding;
@property (nonatomic, strong) NSMutableArray *operatorModel;

@end


@implementation OrderListGoodInfoCell

-  (void)createUI {
    self.goodImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self.contentView addSubview:self.goodImageView];
    
    self.bottomLine = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.bottomLine];
    
    self.goodNameLabel = [YYCreateTools createLabel:nil
                                               font:midFont(13)
                                          textColor:XHBlackColor
                                           maxWidth:(kScreenW - kSizeScale(104) - kSizeScale(25))
                                      fixLineHeight:kSizeScale(18)];
    self.goodNameLabel.numberOfLines = 2;
    [self.contentView addSubview:self.goodNameLabel];
    
    self.afterSaleView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.afterSaleView];
    
    self.goodSkuLabel = [YYCreateTools createLabel:nil
                                              font:midFont(12)
                                         textColor:XHBlackLitColor];
    self.goodSkuLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:self.goodSkuLabel];
    
    // 9, 15 这里做价格区分
    self.goodPriceLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(15)
                                           textColor:XHBlackColor];
    [self.contentView addSubview:self.goodPriceLabel];
    
    self.commissionLabel = [YYCreateTools createLabel:nil
                                                 font:midFont(12)
                                            textColor:XHBlackColor];
    self.commissionLabel.hidden = YES;
    self.commissionLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.commissionLabel];
    
    self.goodNumLabel = [YYCreateTools createLabel:nil
                                              font:midFont(12)
                                         textColor:XHBlackColor];
    [self.contentView addSubview:self.goodNumLabel];
    
    [self.goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(80), kSizeScale(80)));
    }];

    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodImageView);
        make.bottom.equalTo(self.contentView).priorityHigh();
        make.right.mas_equalTo(-kSizeScale(12));
        make.height.mas_equalTo(0.5);
        
        self.listTopPadding = make.top.equalTo(self.goodImageView.mas_bottom).offset(kSizeScale(12)).priorityHigh();
        self.detailTopPadding = make.top.equalTo(self.goodImageView.mas_bottom).offset(kSizeScale(19)).priorityHigh();
    }];
    [self.listTopPadding install];
    [self.detailTopPadding uninstall];
    
    [self.goodNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImageView.mas_top).offset(-kSizeScale(5));
        make.left.equalTo(self.goodImageView.mas_right).offset(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(13));
    }];
    
    [self.goodSkuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.top.equalTo(self.goodNameLabel.mas_bottom).offset(kSizeScale(5));
        make.right.mas_equalTo(-kSizeScale(40));
    }];
    
    [self.goodPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodNameLabel);
        make.bottom.equalTo(self.goodImageView.mas_bottom);
    }];
    
    [self.afterSaleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodPriceLabel.mas_right).offset(kSizeScale(5));
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.goodPriceLabel);
        make.height.mas_equalTo(kSizeScale(30));
    }];
    
    [self.commissionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodPriceLabel);
        make.right.mas_equalTo(-kSizeScale(12));
    }];
    
    [self.goodNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.goodSkuLabel);
    }];
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderClickEvent:)]];
}

- (void)setGoodModel:(OrderGoodInfoModel *)goodModel {
    _goodModel = goodModel;
    self.operatorModel = [NSMutableArray array];
    NSMutableAttributedString  *comissionText = [NSMutableAttributedString new];
    
    self.goodNameLabel.text = _goodModel.skuTitle;
    self.goodSkuLabel.text = _goodModel.skuProp;
    [self.goodImageView yy_sdWebImage:_goodModel.skuCover
                 placeholderImageType:YYPlaceholderImageListGoodType];
    self.goodNumLabel.text = [NSString stringWithFormat:@"x%@",_goodModel.skuCnt];
    if (kValidDictionary(_goodModel.price)) {
        NSMutableAttributedString *attribut = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:midFont(9) symbolTextColor:XHBlackColor wordSpace:2 price:[NSString stringWithFormat:@"%@",[_goodModel.price objectForKey:@"priceTag"]] priceFont:boldFont(15) priceTextColor: XHBlackColor symbolOffsetY:0];
        self.goodPriceLabel.attributedText = attribut;
    }

    if ([_goodModel.source integerValue] == 2) {
        if (_goodModel.showEarn) {
            self.commissionLabel.hidden = NO;
            comissionText = [YYCommonTools containSpecialSymbolHandler:(_goodModel.earnFlag ?@"省":@"赚")
                                                            symbolFont:midFont(12)
                                                       symbolTextColor:(_goodModel.earnFlag ? HexRGB(0xE6B96E): HexRGB(0xE6000F)) wordSpace:2
                                                                 price:(kValidString(_goodModel.commission)?_goodModel.commission:@"20.5")
                                                             priceFont:midFont(12)
                                                        priceTextColor:XHBlackColor
                                                         symbolOffsetY:0];
            self.commissionLabel.attributedText = comissionText;
        }
        else {
            self.commissionLabel.hidden = YES;
        }
    }
    else {
        self.commissionLabel.hidden = YES;
    }
    self.operatorModel = [YYCommonTools getGoodAfterSaleShowOperatorBtns:@(_goodModel.operation)];
    [self operatorBtnIsShow:self.operatorModel];
}

- (void)setIsHideGoodBottomLine:(BOOL)isHideGoodBottomLine {
    _isHideGoodBottomLine = isHideGoodBottomLine;
    self.bottomLine.hidden = _isHideGoodBottomLine;
}

#pragma mark - Private
- (void)operatorBtnIsShow:(NSMutableArray *)operatorModels {
    [self.afterSaleView removeAllSubviews];
    if (kValidArray(operatorModels)) {
        CGFloat btnWidth = kSizeScale(68);
        CGFloat btnHeight = kSizeScale(22);
        UIButton *lastBtn = nil;
        for (int i = 0; i < operatorModels.count ; i ++) {
            
            YYOperatorInfoModel *infoModel = operatorModels[i];
            UIButton *btn = [YYCreateTools createBtn:nil
                                                       font:midFont(12)
                                                  textColor:XHBlackMidColor];
            btn.tag = kGoodOperatorTag + i;
            btn.clipsToBounds = YES;
            btn.backgroundColor = XHWhiteColor;
            btn.layer.borderWidth = 0.8;
            btn.layer.cornerRadius = kSizeScale(11);
            btn.layer.borderColor = XHBlackLitColor.CGColor;
            [btn addTarget:self action:@selector(buttonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
            [self.afterSaleView addSubview:btn];
            [btn setTitle:infoModel.showTitle forState:UIControlStateNormal];
        
            if (lastBtn) {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(lastBtn);
                    make.right.equalTo(lastBtn.mas_left).offset(-kSizeScale(12));
                    make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                }];
            }
            else {
                [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.equalTo(self.afterSaleView);
                    make.right.equalTo(self.afterSaleView.mas_right);
                    make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                }];
            }
            [btn enlargeTouchAreaWithTop:0 right:0 bottom:50 left:0];
            lastBtn = btn;
        }
    }
}

#pragma mark - Event
- (void)buttonClickEvent:(UIButton *)sender {
    NSInteger tag = sender.tag - kGoodOperatorTag;
    if (tag < self.operatorModel.count) {
        YYOperatorInfoModel *model = [self.operatorModel objectAtIndex:tag];
        if (self.afterSaleClick) {
            self.afterSaleClick(self.goodModel, model.goodOperatorType);
        }
    }
}

- (void)orderClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.orderClick) {
        self.orderClick(self.goodModel, self);
    }
}

@end
