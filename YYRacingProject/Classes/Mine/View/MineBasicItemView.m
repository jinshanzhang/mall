//
//  MineBasicItemView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/3/30.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "MineBasicItemView.h"

@interface MineBasicItemView ()

@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) UIImageView *markImageView;

@property (nonatomic, strong) UILabel     *leftTitleLabel;
@property (nonatomic, strong) UILabel     *rightTitleLabel;

@property (nonatomic, strong) UIView      *lineView;

@property (nonatomic, strong) MineItemContentInfoModel *itemModel;
@end

@implementation MineBasicItemView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.v_cornerRadius = kSizeScale(5);
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.leftImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self addSubview:self.leftImageView];
    
    self.leftTitleLabel = [YYCreateTools createLabel:nil
                                                font:boldFont(14)
                                           textColor:XHBlackColor];
    [self addSubview:self.leftTitleLabel];
    
    self.lineView = [YYCreateTools createView:HexRGB(0xf1f1f1)];
    [self addSubview:self.lineView];
    
    self.arrowImageView = [YYCreateTools createImageView:@"mine_arrow_icon"
                                              viewModel:-1];
    [self addSubview:self.arrowImageView];
    
    self.rightTitleLabel = [YYCreateTools createLabel:nil
                                                font:normalFont(14)
                                           textColor:XHBlackMidColor];
    [self addSubview:self.rightTitleLabel];
    
    self.markImageView = [YYCreateTools createImageView:nil
                                               viewModel:-1];
    [self addSubview:self.markImageView];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewClickEvent:)]];
    
    [self setSubViewLayout];
}

- (void)setSubViewLayout {
    [self.leftImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.mas_equalTo(kSizeScale(14));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(18), kSizeScale(18)));
    }];
    
    [self.leftTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.leftImageView);
        make.left.equalTo(self.leftImageView.mas_right).offset(kSizeScale(6));
    }];
    
    [self.arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.mas_equalTo(-kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(5), kSizeScale(9)));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(12));
        make.bottom.equalTo(self);
        make.height.mas_equalTo(kSizeScale(0.5));
    }];
    
    [self.rightTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left).offset(-kSizeScale(5));
        make.centerY.equalTo(self);
    }];
    
    [self.markImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.rightTitleLabel.mas_left).offset(-kSizeScale(5));
        make.centerY.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(104), kSizeScale(18)));
    }];
}

- (void)setViewModel:(MineItemContentInfoModel *)infoModel {
    if (infoModel) {
        self.itemModel = infoModel;
        self.leftImageView.image = [UIImage imageNamed:infoModel.imageName];
        self.leftTitleLabel.text = infoModel.title;
        
        self.rightTitleLabel.text = infoModel.content;
        if (kValidString(infoModel.imageUrl)) {
            [self.markImageView sd_setImageWithURL:[NSURL URLWithString:infoModel.imageUrl]];
        }
        else {
            self.markImageView.image = [UIImage imageWithColor:XHWhiteColor];
        }
    }
}

#pragma mark - Event
- (void)viewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.delegate && [self.delegate respondsToSelector:@selector(basicViewClickEvent:)]) {
        if (self.itemModel) {
            [self.delegate basicViewClickEvent:self.itemModel];
        }
    }
}
@end
