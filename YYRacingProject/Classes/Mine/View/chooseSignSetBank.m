//
//  chooseSignSetBank.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "chooseSignSetBank.h"
#import "SignBankCell.h"

static  CGFloat  const  kHYTopViewHeight = 48; //顶部视图的高度

@interface chooseSignSetBank ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong) UIScrollView * contentView;
@property (nonatomic,strong) UIView * underLine;

@property (nonatomic,strong) UITableView * tableView;

@property (nonatomic,assign)  BOOL  selectFirst;

@end

@implementation chooseSignSetBank
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
        self.backgroundColor = XHWhiteColor;
    }
    return self;
}

-(void)setUp{
    
    UIView * topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 45)];
    [self addSubview:topView];
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"选择银行";
    titleLabel.font = normalFont(15);
    titleLabel.textColor = XHBlackColor;
    [titleLabel sizeToFit];
    [topView addSubview:titleLabel];
    titleLabel.centerY = topView.height * 0.5;
    titleLabel.centerX = topView.width * 0.5;
    
    
    UIButton *closebtn = [YYCreateTools createBtnImage:@"goodCoupon_close_icon"];
    [topView addSubview:closebtn];
    [closebtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(titleLabel);
        make.right.mas_equalTo(-20);
    }];
    
    closebtn.actionBlock = ^(UIButton *sender) {
        if (self.close) {
            self.close();
        }
    };
    UIView * separateLine = [self separateLine];
    [topView addSubview: separateLine];
    separateLine.top = topView.height - separateLine.height;



    UIScrollView * contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, kHYTopViewHeight, self.frame.size.width, self.height - kHYTopViewHeight)];
    contentView.contentSize = CGSizeMake(kScreenW*2, 0);
    [self addSubview:contentView];
    _contentView = contentView;
    _contentView.pagingEnabled = YES;
    _contentView.delegate = self;
    
     self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenW, _contentView.height)];
    [_contentView addSubview:self.tableView];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0);
    Class common = [SignBankCell class];
    registerClass(_tableView, common);}

//分割线
- (UIView *)separateLine{
    
    UIView * separateLine = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1 / [UIScreen mainScreen].scale)];
    separateLine.backgroundColor =  XHLightColor;
    return separateLine;
}


-(void)setBankList:(NSMutableArray *)BankList{
    if (!_BankList) {
        _BankList = BankList;
    }
    [self.tableView reloadData];
}

#pragma mark - TableViewDatasouce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _BankList.count;;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Class common = [SignBankCell class];
    SignBankCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    if (!cell) {
        cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(common)];
    }
    BankModel *model = self.BankList[indexPath.row];
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    for (BankModel *item  in self.BankList) {
        if (item == _BankList[indexPath.row]) {
            item.select = YES;
            if (self.chooseFinish) {
                self.chooseFinish(item);
            }
        }else{
            item.select = NO;
        }
    }
    [tableView reloadData];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
