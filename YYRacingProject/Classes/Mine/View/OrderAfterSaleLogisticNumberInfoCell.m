//
//  OrderAfterSaleLogisticNumberInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleLogisticNumberInfoCell.h"

@interface OrderAfterSaleLogisticNumberInfoCell()

@end

@implementation OrderAfterSaleLogisticNumberInfoCell

- (void)createUI {
    self.cellTipLabel = [YYCreateTools createLabel:@"物流单号"
                                              font:midFont(14)
                                         textColor:XHBlackLitColor];
    [self.contentView addSubview:self.cellTipLabel];
    
    [self.cellTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(16));
        make.width.mas_equalTo(kSizeScale(60));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(16));
    }];
    
    self.cellTextField = [YYCreateTools createTextField:nil
                                                   font:midFont(14)
                                               texColor:XHBlackColor];
    self.cellTextField.textAlignment = NSTextAlignmentLeft;
    self.cellTextField.placeholder = @"请输入物流单号";
    self.cellTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.contentView addSubview:self.cellTextField];
    
    [self.cellTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cellTipLabel.mas_right).offset(kSizeScale(10));
        make.centerY.equalTo(self.cellTipLabel);
        make.right.mas_equalTo(-kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(30));
    }];
}

@end
