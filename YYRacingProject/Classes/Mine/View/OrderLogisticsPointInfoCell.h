//
//  OrderLogisticsPointInfoCell.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderLogisticsInfoModel.h"
@interface OrderLogisticsPointInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) OrderLogisticsPointInfoModel *pointModel;

@end
