//
//  SignBankCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "SignBankCell.h"

@implementation SignBankCell

-(void)createUI{
    
    self.nameLabel = [YYCreateTools createLabel:@"" font:normalFont(14) textColor:XHBlackColor];
    [self.contentView addSubview:self.nameLabel];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(1);
    }];
    
}

-(void)setModel:(BankModel *)model{
    self.nameLabel.text = model.name;
    self.nameLabel.textColor = model.select == YES?XHLoginColor:XHBlackColor;
}



@end
