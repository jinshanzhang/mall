//
//  OrderLogisticsPointInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderLogisticsPointInfoCell.h"
#import "OrderLogisticsPointInfoView.h"
@interface OrderLogisticsPointInfoCell()

@property (nonatomic, strong) UILabel  *pointLabel;
@property (nonatomic, strong) UILabel  *timeLabel;
@property (nonatomic, strong) UIView   *lineView;
@property (nonatomic, strong) OrderLogisticsPointInfoView *pointView;
@property (nonatomic, strong)  NSAttributedString *phoneNumber;


@end

@implementation OrderLogisticsPointInfoCell

- (void)createUI {
    self.pointLabel = [YYCreateTools createLabel:nil
                                            font:midFont(13)
                                       textColor:XHBlackColor];
    self.pointLabel.numberOfLines = 0;
    [self.contentView addSubview:self.pointLabel];
    
    self.timeLabel = [YYCreateTools createLabel:nil
                                            font:midFont(12)
                                       textColor:XHBlackMidColor];
    [self.contentView addSubview:self.timeLabel];
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.lineView];
    
    [self.pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(40));
        make.top.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(-kSizeScale(30));
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pointLabel);
        make.top.equalTo(self.pointLabel.mas_bottom).offset(kSizeScale(5));
        make.bottom.equalTo(self.contentView).offset(-kSizeScale(12));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pointLabel);
        make.right.mas_equalTo(-kSizeScale(10));
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(1));
    }];
}

- (void)setPointModel:(OrderLogisticsPointInfoModel *)pointModel {
    _pointModel = pointModel;
    self.pointLabel.text = _pointModel.momentInfo;
    self.pointLabel.textColor = (pointModel.drawType == YYLogisticsDrawTopType ? HexRGB(0x4F9443):XHBlackColor);
    self.timeLabel.text = [NSString formatDateAndTime:[_pointModel.momentTime longLongValue]];
    
    self.pointView = [[OrderLogisticsPointInfoView alloc] init];
    [self.contentView addSubview:self.pointView];
    [self.pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.bottom.top.equalTo(self.contentView);
        make.width.mas_equalTo(kSizeScale(20));
    }];
    self.pointView.drawType = pointModel.drawType;
    
    [self distinguishPhoneNumLabel:self.pointLabel labelStr:_pointModel.momentInfo];
}


-(void)distinguishPhoneNumLabel:(UILabel *)label labelStr:(NSString *)labelStr{
    
    //获取字符串中的电话号码
    NSString *regulaStr = @"\\d{3,4}[- ]?\\d{7,8}";
    NSRange stringRange = NSMakeRange(0, labelStr.length);
    //正则匹配
    NSError *error;
    
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:labelStr];
    
    NSRegularExpression *regexps = [NSRegularExpression regularExpressionWithPattern:regulaStr options:0 error:&error];
    if (!error && regexps != nil) {
        [regexps enumerateMatchesInString:labelStr options:0 range:stringRange usingBlock:^(NSTextCheckingResult * _Nullable result, NSMatchingFlags flags, BOOL * _Nonnull stop) {
            NSRange phoneRange = result.range;
            //定义一个NSAttributedstring接受电话号码字符串
           self.phoneNumber = [str attributedSubstringFromRange:phoneRange];
            //添加下划线
            NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
            [str addAttributes:attribtDic range:phoneRange];
            //设置文本中的电话号码显示为黄色
//            [str addAttribute:NSForegroundColorAttributeName value:[LFWImage colorWithHexString:@"FF8200"] range:phoneRange];
            label.attributedText = str;
            label.userInteractionEnabled = YES;
            //添加手势，可以点击号码拨打电话
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
            [label addGestureRecognizer:tap];
        }];
    }
    
}

-(void)tapGesture:(UITapGestureRecognizer *)sender{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType  isEqualToString:@"iPod touch"]||[deviceType  isEqualToString:@"iPad"]||[deviceType  isEqualToString:@"iPhone Simulator"]){
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:@"您的设备不能打电话" delegate:nil cancelButtonTitle:@"好的,知道了" otherButtonTitles:nil,nil];
        
        [alert show];
        
    }else{
        //NSAttributedstring转换为NSString
        NSString *stringNum = [self.phoneNumber string];
        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",stringNum];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }
    
}

@end
