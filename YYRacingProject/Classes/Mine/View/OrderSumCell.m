//
//  OrderSumCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "OrderSumCell.h"

@implementation OrderSumCell

-(void)createUI{
    self.icon = [YYCreateTools createImageView:nil viewModel:-1];
    [self.icon zy_cornerRadiusAdvance:kSizeScale(15) rectCornerType:UIRectCornerAllCorners];
    [self.icon zy_attachBorderWidth:1 color:XHWhiteColor];
    [self addSubview:self.icon];
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.height.width.mas_equalTo(kSizeScale(24));
        make.top.mas_equalTo(2);
        make.bottom.mas_equalTo(-4);
    }];
    
    self.identityImageView = [YYCreateTools createImageView:@"Identity_icon"
                                                  viewModel:-1];
    [self addSubview:self.identityImageView];
    [self.identityImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.icon);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(27), kSizeScale(27)));
    }];
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(13) textColor:XHBlackLitColor];
    [self addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.icon.mas_right).mas_offset(6);
        make.centerY.equalTo(self);
    }];
    
    self.commissionLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackColor];
    [self addSubview:self.commissionLabel];
    
    [self.commissionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.equalTo(self);
    }];

    self.bottomLine = [YYCreateTools createView:XHLightColor];
    [self addSubview:self.bottomLine];
    
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.right.mas_equalTo(kSizeScale(-12));
        make.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

-(void)setSumModel:(OrderSumInfoModel *)sumModel{
    _sumModel = sumModel;
    [self.icon sd_setImageWithURL:[NSURL URLWithString:sumModel.homePageLogo]];
    self.titleLabel.text = sumModel.homePageName;
    NSMutableAttributedString  *comissionText = [NSMutableAttributedString new];
    comissionText = [YYCommonTools containSpecialSymbolHandler:(sumModel.earnFlag == 1 ?@"省":@"赚")
                                                    symbolFont:midFont(12)
                                               symbolTextColor:(sumModel.earnFlag == 1 ? HexRGB(0xE6B96E): HexRGB(0xE6000F)) wordSpace:2
                                                         price:(kValidString(sumModel.fee)?sumModel.fee:@"20.5")
                                                     priceFont:midFont(12)
                                                priceTextColor:XHBlackColor
                                                 symbolOffsetY:0];
    self.commissionLabel.attributedText = comissionText;
    
    self.identityImageView.hidden = (sumModel.homePageRole<2)?YES:NO;
    if (sumModel.homePageRole >= 2) {
        NSString *identityPhotoName = nil;
        
        if (sumModel.homePageRole == 2) {
            identityPhotoName = @"owner_header_icon";
        }
        if (sumModel.homePageRole == 3) {
            identityPhotoName = @"agent_header_icon";
        }
        if (sumModel.homePageRole == 4) {
            identityPhotoName = @"partner_header_icon";
        }
        if (kValidString(identityPhotoName)) {
            self.identityImageView.image = [UIImage imageNamed:identityPhotoName];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
