//
//  OrderLogisticsHeaderInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/14.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "OrderLogisticsHeaderInfoView.h"

@interface OrderLogisticsHeaderInfoView()

@property (nonatomic, strong) UILabel *headerTitleLbl;

@end

@implementation OrderLogisticsHeaderInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.headerTitleLbl = [YYCreateTools createLabel:nil
                                                font:midFont(13)
                                           textColor:XHBlackColor];
    self.headerTitleLbl.numberOfLines = 1;
    self.headerTitleLbl.preferredMaxLayoutWidth = kScreenW-kSizeScale(30);
    [self addSubview:self.headerTitleLbl];
}

- (void)setHeaderTitle:(NSString *)headerTitle {
    _headerTitle = headerTitle;
    if (kValidString(_headerTitle)) {
        self.headerTitleLbl.text = _headerTitle;
        [self.headerTitleLbl mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kSizeScale(12));
            make.top.mas_equalTo(kSizeScale(15));
            make.width.mas_equalTo(kSizeScale(80));
            make.bottom.equalTo(self.mas_bottom).offset(-kSizeScale(15)).priorityHigh();
        }];
    }
    [self layoutIfNeeded];
}

@end
