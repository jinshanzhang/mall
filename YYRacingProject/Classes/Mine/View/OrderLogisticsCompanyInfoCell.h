//
//  OrderLogisticsCompanyInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/14.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderLogisticsInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderLogisticsCompanyInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) OrderLogisticCarrierInfoModel *carrierModel;

@end

NS_ASSUME_NONNULL_END
