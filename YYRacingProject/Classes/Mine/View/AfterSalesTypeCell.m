//
//  AfterSalesTypeCell.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AfterSalesTypeCell.h"

@implementation AfterSalesTypeCell

-(void)createUI{
    
    self.titleLabel = [YYCreateTools createLabel:@"" font:normalFont(15) textColor:XHBlackColor];
    [self.contentView addSubview:self.titleLabel];
    
    self.detailLabel = [YYCreateTools createLabel:@"" font:normalFont(12) textColor:XHBlackLitColor];
    [self.contentView addSubview:self.detailLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(16);
        make.height.mas_equalTo(14);
    }];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_equalTo(8);
    }];
    
    
    self.iconIMG = [YYCreateTools createImageView:@"cellMore_icon"
                                               viewModel:-1];
    [self.contentView addSubview:self.iconIMG];
    
    [self.iconIMG mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(12));
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
    }];
    
    UIView *lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:lineView];
    
    [lineView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
    }];
    
}


@end
