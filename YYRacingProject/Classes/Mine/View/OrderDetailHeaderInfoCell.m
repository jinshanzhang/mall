//
//  OrderDetailHeaderInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderDetailHeaderInfoCell.h"

@interface OrderDetailHeaderInfoCell()

@property (nonatomic, strong) UIImageView  *orderStatusView;  // 订单状态View
@property (nonatomic, strong) UILabel *orderStatusLabel; // 订单状态
@property (nonatomic, strong) UILabel *orderCoutDownLabel; //订单倒计时

@property (nonatomic, strong) UIView  *logisticsView;    // 物流信息View
@property (nonatomic, strong) YYLabel *deliveryLabel;    // 配送位置
@property (nonatomic, strong) UILabel *deliveryTimeLabel;// 配送时间
@property (nonatomic, strong) UIImageView *deliveryMoreImageView;
@property (nonatomic, strong) UIImageView *deliveryImageView;
@property (nonatomic, strong) UIView  *deliveryBottomView;
@property (nonatomic, strong) MASConstraint *loginsticsHeight;

@property (nonatomic, strong) UIView  *userInfoView;     // 用户信息View
@property (nonatomic, strong) UIImageView *userMaskImageView;
@property (nonatomic, strong) UIImageView *userHeaderImageView;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UIView  *userBottomLine;

@property (nonatomic, strong) UIView  *addressView;      // 地址信息View
@property (nonatomic, strong) UIImageView *addressMaskImageView;
@property (nonatomic, strong) UILabel  *addressUserInfoLabel;
@property (nonatomic, strong) YYLabel  *addressLabel;

@end


@implementation OrderDetailHeaderInfoCell

- (void)createUI {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:OYCountDownNotification object:nil];
    //订单状态View
    self.orderStatusView = [YYCreateTools createImageView:@"" viewModel:-1];
    self.orderStatusView.backgroundColor = XHMainColor;
    [self.contentView addSubview:self.orderStatusView];
    
    self.orderStatusLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(17)
                                             textColor:XHWhiteColor];
    self.orderStatusLabel.textAlignment = NSTextAlignmentLeft;
    [self.orderStatusView addSubview:self.orderStatusLabel];
    
    self.orderCoutDownLabel = [YYCreateTools createLabel:nil
                                                  font:midFont(12)
                                             textColor:XHWhiteColor];
    [self.orderStatusView addSubview:self.orderCoutDownLabel];
    
    //物流view
    self.logisticsView = [YYCreateTools createView:XHWhiteColor];
    [self.logisticsView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookLogisticsClickEvent:)]];
    [self.contentView addSubview:self.logisticsView];
    self.deliveryLabel = [YYCreateTools createLabel:nil
                                               font:midFont(13)
                                          textColor:HexRGB(0x4F9443)
                                           maxWidth:kSizeScale(245)
                                      fixLineHeight:kSizeScale(18)];
    self.deliveryLabel.numberOfLines = 2;
    [self.logisticsView addSubview:self.deliveryLabel];
    
    self.deliveryTimeLabel = [YYCreateTools createLabel:nil
                                                   font:midFont(12)
                                              textColor:XHBlackLitColor];
    [self.logisticsView addSubview:self.deliveryTimeLabel];
    
    self.deliveryBottomView = [YYCreateTools createView:XHLightColor];
    [self.logisticsView addSubview:self.deliveryBottomView];
    
    self.deliveryImageView = [YYCreateTools createImageView:@"order_logistics_icon"
                                                  viewModel:-1];
    [self.logisticsView addSubview:self.deliveryImageView];
    
    self.deliveryMoreImageView = [YYCreateTools createImageView:@"expand_more_icon"
                                                  viewModel:-1];
    [self.logisticsView addSubview:self.deliveryMoreImageView];
    
    // 用户view
    self.userInfoView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.userInfoView];
    
    self.userMaskImageView = [YYCreateTools createImageView:@"order_userwechat_icon"
                                                  viewModel:-1];
    [self.userInfoView addSubview:self.userMaskImageView];
    
    self.userHeaderImageView = [YYCreateTools createImageView:nil
                                                    viewModel:-1];
    [self.userHeaderImageView zy_cornerRadiusAdvance:13
                                      rectCornerType:UIRectCornerAllCorners];
    self.userHeaderImageView.image = [UIImage imageWithColor:XHRedColor];
    [self.userInfoView addSubview:self.userHeaderImageView];
    
    self.userNameLabel = [YYCreateTools createLabel:nil
                                               font:boldFont(13)
                                          textColor:XHBlackColor];
    [self.userInfoView addSubview:self.userNameLabel];
    
    self.userBottomLine = [YYCreateTools createView:XHLightColor];
    [self.userInfoView addSubview:self.userBottomLine];
    
    // 地址View
    self.addressView = [YYCreateTools createView:XHWhiteColor];
    [self.contentView addSubview:self.addressView];
    
    self.addressUserInfoLabel = [YYCreateTools createLabel:nil
                                                      font:boldFont(13)
                                                 textColor:XHBlackColor];
    [self.contentView addSubview:self.addressUserInfoLabel];
    
    self.addressLabel = [YYCreateTools createLabel:nil
                                              font:midFont(13)
                                         textColor:XHBlackLitColor
                                          maxWidth:kScreenW-kSizeScale(80)
                                     fixLineHeight:kSizeScale(18)];
    self.addressLabel.numberOfLines = 2;
    [self.contentView addSubview:self.addressLabel];
    
    self.addressMaskImageView = [YYCreateTools createImageView:@"order_address_icon"
                                                     viewModel:-1];
    [self.contentView addSubview:self.addressMaskImageView];
    
    /** 订单状态布局 **/
    [self.orderStatusView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(90));
    }];
    
    [self.orderStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.centerY.equalTo(self.orderStatusView).offset(-kSizeScale(8));
    }];
    
    [self.orderCoutDownLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderStatusLabel);
        make.top.equalTo(self.orderStatusLabel.mas_bottom);
    }];

    /** 物流布局 **/
    [self.logisticsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.orderStatusView);
        make.top.equalTo(self.orderStatusView.mas_bottom);
        self.loginsticsHeight = make.height.mas_equalTo(0);
    }];
    [self.loginsticsHeight uninstall];
    
    [self.deliveryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logisticsView.mas_top).offset(kSizeScale(12)).priorityHigh();
        make.left.equalTo(self.logisticsView.mas_left).offset(kSizeScale(48));
        make.right.equalTo(self.logisticsView.mas_right).offset(-kSizeScale(30));
    }];
    
    [self.deliveryTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.deliveryLabel);
        make.top.equalTo(self.deliveryLabel.mas_bottom).offset(kSizeScale(5));
    }];
    
    [self.deliveryBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.deliveryTimeLabel.mas_bottom).offset(kSizeScale(12)).priorityHigh();
        make.left.right.equalTo(self.logisticsView);
        make.height.mas_equalTo(kSizeScale(8)).priorityHigh();
        make.bottom.equalTo(self.logisticsView.mas_bottom);
    }];
    
    [self.deliveryImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(14));
        make.centerY.equalTo(self.logisticsView).offset(-kSizeScale(5));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(24), kSizeScale(24)));
    }];
    
    [self.deliveryMoreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(kSizeScale(-14));
        make.centerY.equalTo(self.deliveryImageView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(6), kSizeScale(10)));
    }];
    
    /** 用户布局 **/
    [self.userInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.logisticsView);
        make.top.equalTo(self.logisticsView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(51));
    }];
    
    [self.userMaskImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(14));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(24), kSizeScale(24)));
        make.centerY.equalTo(self.userInfoView);
    }];
    
    [self.userHeaderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userMaskImageView.mas_right).offset(kSizeScale(10));
        make.centerY.equalTo(self.userInfoView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(27), kSizeScale(27)));
    }];
    
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userHeaderImageView.mas_right).offset(kSizeScale(10));
        make.centerY.equalTo(self.userInfoView);
    }];
    
    [self.userBottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userMaskImageView);
        make.bottom.equalTo(self.userInfoView.mas_bottom);
        make.height.mas_equalTo(1);
        make.right.equalTo(self.userInfoView.mas_right).offset(-kSizeScale(14));
    }];
    
    /** 地址布局 **/
    [self.addressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.orderStatusView);
        make.right.mas_equalTo(-kSizeScale(30));
        make.top.equalTo(self.userInfoView.mas_bottom);
        make.bottom.equalTo(self.contentView.mas_bottom);
    }];
    
    [self.addressUserInfoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.addressView.mas_left).offset(kSizeScale(48));
        make.top.equalTo(self.addressView.mas_top).offset(kSizeScale(10));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.addressUserInfoLabel);
        make.top.equalTo(self.addressUserInfoLabel.mas_bottom);
        make.right.equalTo(self.addressView).offset(-kSizeScale(10));
        make.bottom.equalTo(self.addressView.mas_bottom).offset(-kSizeScale(10));
    }];
    
    [self.addressMaskImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(14));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(24), kSizeScale(24)));
        make.centerY.equalTo(self.addressView);
    }];
}

- (void)setDetailModel:(OrderDetailInfoModel *)detailModel {
    _detailModel = detailModel;
    BOOL isDelivery = _detailModel.isShowDeliveryInfo;
    NSInteger orderStatus = [self.detailModel.orderInfo.orderStatus integerValue];
    if (isDelivery) {
        [self.loginsticsHeight uninstall];
        self.deliveryImageView.hidden = NO;
        self.deliveryMoreImageView.hidden = NO;
        
        self.deliveryLabel.text = _detailModel.expressInfo.momentInfo;
        self.deliveryTimeLabel.text = [NSString formatDateAndTime:[_detailModel.expressInfo.momentTime longValue]];
    }
    else {
        [self.loginsticsHeight install];
        self.deliveryImageView.hidden = YES;
        self.deliveryMoreImageView.hidden = YES;
    }
    
    self.userNameLabel.text = _detailModel.userInfo.nickName;
    [self.userHeaderImageView sd_setImageWithURL:[NSURL URLWithString:_detailModel.userInfo.avatarUrl] placeholderImage:[UIImage imageWithColor:XHRedColor]];
    
    self.addressUserInfoLabel.text = [NSString stringWithFormat:@"%@  %@", _detailModel.addr.receiverName, _detailModel.addr.receiverPhone];
    self.addressLabel.text = _detailModel.addr.address;
    
    self.orderStatusLabel.text = [YYCommonTools showQueryOrderStatus:self.detailModel.orderInfo.orderStatus];
    if (orderStatus > 2) {
        NSString *countDownContent = @"";
        if(orderStatus == 6 || orderStatus == 7) {
            countDownContent = _detailModel.orderInfo.shipDeadlineTime;
        }
        if (kValidString(countDownContent)) {
            [self.orderStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(kSizeScale(16));
                make.centerY.equalTo(self.orderStatusView).offset(-kSizeScale(8));
            }];
        }else {
            [self.orderStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(kSizeScale(16));
                make.centerY.equalTo(self.orderStatusView);
            }];
        }
        self.orderCoutDownLabel.text = countDownContent;
        [kCountDownManager invalidate];
    }
    else {
        [self countDownNotification];
    }
}
#pragma mark - Event
- (void)countDownNotification {
    NSInteger timeInterval = [kCountDownManager timeIntervalWithIdentifier:@"orderDetail"];
    if (self.detailModel.diffValue > 0) {
        NSString *timeFormat;
        NSInteger count =  self.detailModel.diffValue - timeInterval;
        if (count > 0) {
            int seconds = count % 60;
            int minutes = (count / 60) % 60;
            timeFormat = [NSString stringWithFormat:@"%02d分%02d秒以后自动取消", minutes, seconds];
        }
        else {
            timeFormat = @"交易关闭";
        }
        self.orderCoutDownLabel.text = timeFormat;
    }
    else {
        self.orderCoutDownLabel.text = @"交易关闭";
    }
}

- (void)lookLogisticsClickEvent:(UITapGestureRecognizer *)gesture {
    if (self.lookLogisticBlock) {
        self.lookLogisticBlock();
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
