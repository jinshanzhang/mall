//
//  OrderLogisticsPointInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderLogisticsPointInfoView.h"

@interface OrderLogisticsPointInfoView()

@end

@implementation OrderLogisticsPointInfoView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = XHWhiteColor;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGFloat height = self.height;
    
    CGFloat lineLeft = kSizeScale(8), lineHeight = 0.0, lineTop = 0.0;
    CGFloat spaceToTopAndBottom = kSizeScale(26);
    CGRect circleRect = CGRectZero;
    CGFloat radius = (self.drawType == YYLogisticsDrawTopType ? kSizeScale(12) : kSizeScale(6));
    
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    linePath.lineWidth = 1.0;
    UIColor *lineColor = HexRGB(0xcccccc);
    [lineColor set];
    
    if (self.drawType == YYLogisticsDrawTopType) {
        lineTop = spaceToTopAndBottom;
        lineHeight = height;
        circleRect = CGRectMake(lineLeft-radius/2.0, lineTop, radius, radius);
    }
    else if (self.drawType == YYLogisticsDrawMidType) {
        lineTop = 0.0;
        lineHeight = height - lineTop;
        circleRect = CGRectMake(lineLeft-radius/2.0, spaceToTopAndBottom, radius, radius);
    }
    else {
        lineTop = 0.0;
        lineHeight = height - spaceToTopAndBottom;
        circleRect = CGRectMake(lineLeft-radius/2.0, lineHeight, radius, radius);
    }
    [linePath moveToPoint:CGPointMake(lineLeft, lineTop)];
    [linePath addLineToPoint:CGPointMake(lineLeft, lineHeight)];
    [linePath stroke];
    
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithOvalInRect:circleRect];
    if (self.drawType == YYLogisticsDrawTopType) {
        circlePath.lineWidth = radius/3;
        UIColor *circleColor = HexRGB(0x4f9443);
        [circleColor set];
        [circlePath fill];
        
        UIColor *shadowColor = HexRGB(0xDCEAD9);
        [shadowColor set];
    }
    else {
        UIColor *circleColor = HexRGB(0xcccccc);
        [circleColor set];
        [circlePath fill];
    }
    [circlePath stroke];
}
@end
