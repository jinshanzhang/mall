//
//  OrderAfterSaleCertificatePhotoInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleCertificatePhotoInfoCell.h"

#import "MinePhotoLayoutInfoCell.h"
@interface OrderAfterSaleCertificatePhotoInfoCell()
<UICollectionViewDelegate,
UICollectionViewDataSource>

@property (nonatomic, strong) UILabel   *cellTitleLabel;

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation OrderAfterSaleCertificatePhotoInfoCell
- (void)createUI {
    self.contentView.backgroundColor = XHWhiteColor;
    self.cellTitleLabel = [YYCreateTools createLabel:@"上传凭证"
                                                font:midFont(14)
                                           textColor:XHBlackLitColor];
    [self.contentView addSubview:self.cellTitleLabel];
    
    [self.cellTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_equalTo(kSizeScale(16));
    }];
    
    self.cellTipLabel = [YYCreateTools createLabel:@"建议上传物流照片，以便商家尽快确认收货"
                                              font:midFont(10)
                                         textColor:XHBlackLitColor];
    self.cellTipLabel.numberOfLines = 0;
    [self.contentView addSubview:self.cellTipLabel];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    CGFloat k320ItemW = (kScreenW-32)/5;
    if (kScreenW > 320.0) {
        layout.itemSize = CGSizeMake(kSizeScale(66), kSizeScale(66));
        layout.minimumInteritemSpacing = kSizeScale(0);
    }
    else {
        layout.itemSize = CGSizeMake(k320ItemW, k320ItemW);
        layout.minimumInteritemSpacing = kSizeScale(0);
    }
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                             collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.allowsSelection = NO;
    self.collectionView.backgroundColor = self.contentView.backgroundColor;
    [self.contentView addSubview:self.collectionView];
    
    Class currentCls = [MinePhotoLayoutInfoCell class];
    [self.collectionView registerClass:currentCls
            forCellWithReuseIdentifier:strFromCls(currentCls)];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cellTitleLabel);
        make.right.mas_equalTo(-kSizeScale(16));
        make.top.mas_equalTo(self.cellTitleLabel.mas_bottom).offset(kSizeScale(12));
        make.height.mas_equalTo(kScreenW > 320.0 ? kSizeScale(66) :k320ItemW);
    }];
    
    [self.cellTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.collectionView);
        make.right.mas_equalTo(-kSizeScale(16));
        make.top.equalTo(self.collectionView.mas_bottom).offset(kSizeScale(12));
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-kSizeScale(10));
    }];
}

- (void)setPhotoArrs:(NSMutableArray *)photoArrs {
    _photoArrs = photoArrs;
    [UIView performWithoutAnimation:^{
        [self.collectionView reloadData];
    }];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photoArrs.count < 5 ? self.photoArrs.count + 1 : self.photoArrs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    @weakify(self);
    NSInteger row = indexPath.row;
    Class currentCls = [MinePhotoLayoutInfoCell class];
    MinePhotoLayoutInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:strFromCls(currentCls) forIndexPath:indexPath];
    cell.currentIndex = indexPath;
    if (kValidArray(self.photoArrs) && row < self.photoArrs.count) {
        cell.showImage = self.photoArrs[row];
    }
    else {
        cell.showImage = nil;
    }
    cell.modifyPhotoBlock = ^(NSIndexPath *index, UIImage *image) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(photoSelectOperator:selectImage:)]) {
            [self.delegate photoSelectOperator:index selectImage:image];
        }
    };
    cell.deletePhotoBlock = ^(NSIndexPath *index) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(deletePhotoOperator:)]) {
            [self.delegate deletePhotoOperator:index];
        }
    };
    return cell;
}

@end
