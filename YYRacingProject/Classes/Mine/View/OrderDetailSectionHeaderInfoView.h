//
//  OrderDetailSectionHeaderInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/21.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailSectionHeaderInfoView : UITableViewHeaderFooterView

@property (nonatomic, strong) NSString *businessWarehouse;

@end
