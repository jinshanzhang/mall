//
//  OrderAfterSaleMessageKefuInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderAfterSaleMessageKefuInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) void (^kefuClickBlock)(void);

@end

NS_ASSUME_NONNULL_END
