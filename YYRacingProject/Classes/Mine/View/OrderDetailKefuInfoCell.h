//
//  OrderDetailKefuInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderDetailKefuInfoCell : YYBaseTableViewCell

@property (nonatomic, copy) void (^kefuClickBlock)(void);
@property (nonatomic, strong) NSMutableArray *summaryList;

@end

NS_ASSUME_NONNULL_END
