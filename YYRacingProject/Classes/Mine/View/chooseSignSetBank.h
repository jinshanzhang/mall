//
//  chooseSignSetBank.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankModel.h"

@interface chooseSignSetBank : UIView

@property (nonatomic, copy) void(^close)(void);

@property (nonatomic, strong) NSMutableArray *BankList;

@property (nonatomic, copy) void(^chooseFinish)(BankModel *chooseModel);

@end
