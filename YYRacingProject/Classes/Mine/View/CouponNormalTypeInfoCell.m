//
//  CouponNormalTypeInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/27.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "CouponNormalTypeInfoCell.h"

@interface CouponNormalTypeInfoCell()

@property (nonatomic, strong) UIImageView  *coupon_bgImageView;
@property (nonatomic, strong) UIImageView  *dotter_ImageView;
@property (nonatomic, strong) UIImageView  *overtime_ImageView;
@property (nonatomic, strong) UIImageView  *coupon_StatuImageView;

@property (nonatomic, strong) UILabel      *couponPriceLabel;  // 优惠券折扣金额
@property (nonatomic, strong) UILabel      *conditionPriceLabel;// 优惠券使用范围

@property (nonatomic, strong) UILabel      *couponTitleLabel;
@property (nonatomic, strong) UILabel      *couponTimeLabel;
@property (nonatomic, strong) UILabel      *couponScopeLabel;  // 优惠券使用范围

@property (nonatomic, strong) UIButton     *immediatelyBtn;
@end

@implementation CouponNormalTypeInfoCell

- (void)createUI {
    self.backgroundColor = self.contentView.backgroundColor = XHLightColor;
    
    self.coupon_bgImageView = [YYCreateTools createImageView:@"coupon_normal_bg_icon"
                                                   viewModel:-1];
    self.coupon_bgImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.coupon_bgImageView];
    
    self.couponPriceLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(10)
                                             textColor:XHRedColor];
    self.couponPriceLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.couponPriceLabel];
    
    self.overtime_ImageView = [YYCreateTools createImageView:nil
                                                   viewModel:-1];
    [self.contentView addSubview:self.overtime_ImageView];
    
    self.coupon_StatuImageView = [YYCreateTools createImageView:nil
                                                      viewModel:-1];
    [self.contentView addSubview:self.coupon_StatuImageView];
    
    self.conditionPriceLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(12)
                                             textColor:XHRedColor];
    self.conditionPriceLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.conditionPriceLabel];
    
    self.dotter_ImageView = [YYCreateTools createImageView:@"coupon_dotted_icon"
                                                 viewModel:-1];
    [self.contentView addSubview:self.dotter_ImageView];
    
    self.couponTitleLabel = [YYCreateTools createLabel:nil
                                                  font:boldFont(16)
                                             textColor:XHBlackColor];
    [self.contentView addSubview:self.couponTitleLabel];
    
    self.couponTimeLabel = [YYCreateTools createLabel:nil
                                                  font:normalFont(11)
                                             textColor:XHBlackLitColor];
    self.couponTimeLabel.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.couponTimeLabel];
    
    self.couponScopeLabel = [YYCreateTools createLabel:nil
                                                 font:normalFont(11)
                                            textColor:XHBlackLitColor];
    [self.contentView addSubview:self.couponScopeLabel];
    
    self.immediatelyBtn = [YYCreateTools createButton:@"立即使用"
                                              bgColor:XHLoginColor
                                            textColor:XHWhiteColor];
    self.immediatelyBtn.titleLabel.font = normalFont(11);
    self.immediatelyBtn.layer.cornerRadius = kSizeScale(14);
    self.immediatelyBtn.clipsToBounds = YES;
    [self.immediatelyBtn addTarget:self action:@selector(immediatelyClickEvent:) forControlEvents:UIControlEventTouchUpInside];
    [self.coupon_bgImageView addSubview:self.immediatelyBtn];
    
    [self.coupon_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(2));
        make.right.equalTo(self.contentView).offset(-kSizeScale(2));
        make.height.mas_equalTo(kSizeScale(102));
        make.top.bottom.equalTo(self.contentView);
    }];
    
    [self.overtime_ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(6));
        make.top.mas_equalTo(kSizeScale(4));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(50), kSizeScale(18)));
    }];
    
    [self.dotter_ImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(100));
        make.centerY.equalTo(self.coupon_bgImageView.mas_centerY).offset(-kSizeScale(5));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(1), kSizeScale(57)));
    }];
    
    [self.couponPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(17));
        make.right.equalTo(self.dotter_ImageView.mas_left).offset(-kSizeScale(5)).priorityHigh();
        make.top.equalTo(self.dotter_ImageView.mas_top).offset(-kSizeScale(2));
    }];

    [self.conditionPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(17));
        make.right.equalTo(self.dotter_ImageView.mas_left).offset(-kSizeScale(5)).priorityHigh();
        make.bottom.equalTo(self.dotter_ImageView.mas_bottom).offset(-kSizeScale(2));
    }];

    [self.immediatelyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kSizeScale(28));
        make.centerY.equalTo(self.coupon_bgImageView.mas_centerY).offset(-kSizeScale(5));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(66), kSizeScale(28)));
    }];
    
    [self.couponTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dotter_ImageView.mas_right).offset(kSizeScale(10));
        make.top.equalTo(self.dotter_ImageView.mas_top).offset(-kSizeScale(4));
        make.right.equalTo(self.immediatelyBtn.mas_left).offset(-kSizeScale(10));
    }];
    
    [self.couponTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.couponTitleLabel);
        make.centerY.equalTo(self.dotter_ImageView.mas_centerY).offset(kSizeScale(1));
    }];
    
    [self.couponScopeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.couponTitleLabel);
        make.bottom.equalTo(self.dotter_ImageView.mas_bottom).offset(kSizeScale(1));
    }];
    
    [self.coupon_StatuImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.equalTo(self.coupon_bgImageView).offset(-kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(70), kSizeScale(70)));
    }];
}



- (void)setCouponModel:(CouponListInfoModel *)couponModel {
    _couponModel = couponModel;
    if (_couponModel) {
        // 是否未使用
        BOOL  isNotused = (_couponModel.status == 1 ? YES : NO);
        
        if (kValidString(_couponModel.expireSoonImg) && isNotused) {
            [self.overtime_ImageView sd_setImageWithURL:[NSURL URLWithString:_couponModel.expireSoonImg]];
        }
        else {
            self.overtime_ImageView.image = [UIImage imageWithColor:XHClearColor];
        }
        
        if (kValidString(_couponModel.denomination)) {
            self.couponPriceLabel.attributedText = [YYCommonTools containSpecialSymbolHandler:([_couponModel.type integerValue]==1?@"¥":@"折")
                  symbolFont:normalFont(14) symbolTextColor:(isNotused==YES?XHRedColor:XHBlackLitColor)
                                                            wordSpace:2
                                                        symbolIsAddLeft:([_couponModel.type integerValue]==1?YES:NO)
                                                        price:_couponModel.denomination
                                                        priceFont:boldFont(30) priceTextColor:(isNotused==YES?XHRedColor:XHBlackLitColor)
                                                                                symbolOffsetY:0];
        }
        self.immediatelyBtn.hidden = !isNotused;
        self.coupon_StatuImageView.hidden = !self.immediatelyBtn.hidden;
        if (self.immediatelyBtn.hidden) {
            if (kValidString(_couponModel.stampImg)) {
                [self.coupon_StatuImageView sd_setImageWithURL:[NSURL URLWithString:_couponModel.stampImg]];
            }
            else {
                self.coupon_StatuImageView.image = [UIImage imageWithColor:XHClearColor];
            }
        }
        self.couponTitleLabel.text = _couponModel.couponName;
        self.conditionPriceLabel.text = _couponModel.denominationCondition;
        self.couponTimeLabel.text = _couponModel.effectTime;
        self.couponScopeLabel.text = _couponModel.scopeTitle;
        
        self.couponTitleLabel.textColor = (isNotused==YES?XHBlackColor:XHBlackLitColor);
        self.conditionPriceLabel.textColor = (isNotused==YES?XHRedColor:XHBlackLitColor);
        
        
        
    }
}

#pragma mark - Event
- (void)immediatelyClickEvent:(UIButton *)sender {
    if (self.immediatelyBlock) {
        self.immediatelyBlock(self.couponModel);
    }
}

@end
