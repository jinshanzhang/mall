//
//  MineCourceCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/11/26.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "MyCourceModel.h"
#import "groomGoodsModel.h"

@interface MineCourceCell : YYBaseTableViewCell

@property (nonatomic, strong) MyCourceModel * model;

@property (nonatomic, strong) groomGoodsModel * groomModel;

@end
