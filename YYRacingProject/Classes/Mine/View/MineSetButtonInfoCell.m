//
//  MineSetButtonInfoCell.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/25.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineSetButtonInfoCell.h"

@interface MineSetButtonInfoCell()

@property (nonatomic, strong) UIButton  *quitBtn;

@end

@implementation MineSetButtonInfoCell

- (void)createUI {
    self.quitBtn = [YYCreateTools createBtn:@"退出登录"
                                       font:boldFont(15)
                                  textColor:XHRedColor];
    self.quitBtn.backgroundColor = XHWhiteColor;
    [self.contentView addSubview:self.quitBtn];
    
    [self.quitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.quitBtn addTarget:self action:@selector(quitBtnClicEvent:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Event
- (void)quitBtnClicEvent:(UIButton *)sender {
    if (self.quitClickBlock) {
        self.quitClickBlock();
    }
}
@end
