//
//  MineOrderSourceVivew.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeBannerInfoModel.h"

@interface MineOrderSourceVivew : UIView

@property (nonatomic, strong) HomeBannerInfoModel *infoModel;
@property (nonatomic, copy) void (^headerClickBlock)(NSMutableDictionary *params);

@end
