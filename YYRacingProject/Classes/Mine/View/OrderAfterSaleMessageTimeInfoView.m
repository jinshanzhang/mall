//
//  OrderAfterSaleMessageTimeInfoView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleMessageTimeInfoView.h"

@interface OrderAfterSaleMessageTimeInfoView()

@property (nonatomic, strong) UILabel *messageTimeLabel;

@end

@implementation OrderAfterSaleMessageTimeInfoView

#pragma mark - Setter method

- (void)setMessageTime:(NSString *)messageTime {
    _messageTime = messageTime;
    self.messageTimeLabel.text = _messageTime;
}

#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHLightColor;
        
        self.messageTimeLabel = [YYCreateTools createLabel:nil
                                                      font:midFont(12)
                                                 textColor:XHWhiteColor];
        self.messageTimeLabel.textAlignment = NSTextAlignmentCenter;
        self.messageTimeLabel.backgroundColor = HexRGB(0xcccccc);
        self.messageTimeLabel.layer.cornerRadius = 10;
        self.messageTimeLabel.clipsToBounds = YES;
        [self addSubview:self.messageTimeLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.messageTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(100), 20));
        make.bottom.mas_equalTo(-kSizeScale(12));
    }];
}

@end
