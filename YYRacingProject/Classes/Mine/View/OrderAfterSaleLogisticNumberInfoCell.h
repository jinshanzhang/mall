//
//  OrderAfterSaleLogisticNumberInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface OrderAfterSaleLogisticNumberInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel  *cellTipLabel;
@property (nonatomic, strong) UITextField *cellTextField;

@end
