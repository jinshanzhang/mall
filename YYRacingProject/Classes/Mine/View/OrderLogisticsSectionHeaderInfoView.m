//
//  OrderLogisticsSectionHeaderInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderLogisticsSectionHeaderInfoView.h"

@interface OrderLogisticsSectionHeaderInfoView()

@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, strong) UIView   *lineView;

@end

@implementation OrderLogisticsSectionHeaderInfoView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.contentView.backgroundColor = XHWhiteColor;
    self.titleLabel = [YYCreateTools createLabel:nil
                                            font:midFont(13)
                                       textColor:XHBlackColor];
    self.titleLabel.backgroundColor = XHWhiteColor;
    [self.contentView addSubview:self.titleLabel];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(5));
        make.height.mas_equalTo(kSizeScale(30)).priorityHigh();
        make.bottom.mas_equalTo(-kSizeScale(5));
    }];
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self.contentView addSubview:self.lineView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(10));
        make.right.mas_equalTo(-kSizeScale(10));
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(1));
    }];
}

- (void)setSectionTitle:(NSString *)sectionTitle {
    _sectionTitle = sectionTitle;
    self.titleLabel.text = _sectionTitle;
}

- (void)setIsShowLine:(BOOL)isShowLine {
    _isShowLine = isShowLine;
    self.lineView.hidden = !_isShowLine;
}
@end
