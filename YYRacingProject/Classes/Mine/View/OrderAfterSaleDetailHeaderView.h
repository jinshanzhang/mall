//
//  OrderAfterSaleDetailHeaderView.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderGoodInfoModel.h"

@interface OrderAfterSaleDetailHeaderView : UIView

@property (nonatomic, strong) OrderGoodInfoModel  *goodModel;

@end
