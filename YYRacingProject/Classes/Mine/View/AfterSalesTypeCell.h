//
//  AfterSalesTypeCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"

@interface AfterSalesTypeCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel;


@property (nonatomic, strong) UIImageView *iconIMG;


@end
