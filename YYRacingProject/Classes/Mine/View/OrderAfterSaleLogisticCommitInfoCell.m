//
//  OrderAfterSaleLogisticCommitInfoCell.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleLogisticCommitInfoCell.h"

@interface OrderAfterSaleLogisticCommitInfoCell()

@end

@implementation OrderAfterSaleLogisticCommitInfoCell

- (void)createUI {
    self.contentView.backgroundColor = XHLightColor;
    
    self.commitBtn = [YYCreateTools createButton:@"确认"
                                         bgColor:XHRedColor
                                       textColor:XHWhiteColor];
    self.commitBtn.layer.cornerRadius = kSizeScale(2);
    self.commitBtn.clipsToBounds = YES;
    [self.commitBtn setBackgroundImage:[UIImage imageWithColor:XHRedColor] forState:UIControlStateSelected];
    [self.commitBtn setBackgroundImage:[UIImage imageWithColor:HexRGB(0xcccccc)] forState:UIControlStateNormal];
    [self.contentView addSubview:self.commitBtn];
    
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(16));
        make.right.mas_equalTo(-kSizeScale(16));
        make.top.mas_equalTo(kSizeScale(30));
        make.height.mas_equalTo(kSizeScale(50));
        make.bottom.equalTo(self.contentView);
    }];
}

@end
