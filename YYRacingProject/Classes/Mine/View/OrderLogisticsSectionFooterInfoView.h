//
//  OrderLogisticsSectionFooterInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderLogisticsInfoModel.h"

@interface OrderLogisticsSectionFooterInfoView : UITableViewHeaderFooterView

@property (nonatomic, strong) OrderLogisticCarrierInfoModel *carrierModel;

@end
