//
//  OrderAfterSaleListHeaderView.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderAfterSaleListHeaderView.h"

@interface OrderAfterSaleListHeaderView()

@property (nonatomic, strong) UILabel *headerTipLabel;

@end

@implementation OrderAfterSaleListHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, kScreenW, kSizeScale(40));
        self.backgroundColor = HexRGB(0xFDF0D5);
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.headerTipLabel = [YYCreateTools createLabel:nil
                                                font:midFont(12)
                                           textColor:HexRGB(0xFF9400)];
    [self addSubview:self.headerTipLabel];
    
    [self.headerTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kSizeScale(12));
        make.centerY.equalTo(self);
        make.right.equalTo(self.mas_right).offset(-kSizeScale(10)).priorityHigh();
    }];
}

- (void)setTipContent:(NSString *)tipContent {
    _tipContent = tipContent;
    if (kValidString(_tipContent)) {
        self.backgroundColor = HexRGB(0xFDF0D5);
    }
    else {
        self.backgroundColor = XHLightColor;
    }
    self.headerTipLabel.text = _tipContent;
}

@end
