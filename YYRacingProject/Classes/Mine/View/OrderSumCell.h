//
//  OrderSumCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2019/1/25.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderListInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderSumCell : YYBaseTableViewCell

@property (nonatomic, strong) UIImageView *icon;

@property (nonatomic, strong) UIImageView *identityImageView;  //身份标识


@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView  *bottomLine;

@property (nonatomic, strong) UILabel      *commissionLabel;


@property (nonatomic, strong) OrderSumInfoModel *sumModel;

@end

NS_ASSUME_NONNULL_END
