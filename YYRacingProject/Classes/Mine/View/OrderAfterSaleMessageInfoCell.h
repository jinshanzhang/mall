//
//  OrderAfterSaleMessageInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/17.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "OrderAfterSaleDetailInfoModel.h"
#import "OrderAfterSaleMessageTimeInfoView.h"
@interface OrderAfterSaleMessageInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) OrderAfterSaleDetailMessageInfoModel *messageModel;

@end
