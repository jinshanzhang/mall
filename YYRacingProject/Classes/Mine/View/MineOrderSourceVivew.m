//
//  MineOrderSourceVivew.m
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/26.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "MineOrderSourceVivew.h"

@interface MineOrderSourceVivew ()

@property (nonatomic, strong) YYAnimatedImageView *headerImageView;
@property (nonatomic, strong) UIView              *bottomView;

@end


@implementation MineOrderSourceVivew
#pragma mark - Life cycle
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = XHWhiteColor;
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.headerImageView = [[YYAnimatedImageView alloc] init];
    self.headerImageView.userInteractionEnabled = YES;
    [self addSubview:self.headerImageView];
    [self.headerImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(capulesViewClickEvent:)]];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self addSubview:self.bottomView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (CGRectEqualToRect(self.frame, CGRectZero) || self.frame.size.height < 0.5) {
        return;
    }
    [self.headerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(!self.infoModel?kSizeScale(0.00001):kSizeScale(self.infoModel.height/2.0));
    }];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.headerImageView.mas_bottom).priorityHigh();
        make.bottom.equalTo(self);
    }];
}

- (void)setInfoModel:(HomeBannerInfoModel *)infoModel {
    _infoModel = infoModel;
    if (self.infoModel) {
        [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:infoModel.imageUrl]];
    }
}

#pragma mark - Event
- (void)capulesViewClickEvent:(UITapGestureRecognizer *)tapGesture {
    if (self.headerClickBlock) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if (self.infoModel) {
            [params setObject:@(self.infoModel.linkType) forKey:@"linkType"];
            [params setObject:self.infoModel.url forKey:@"url"];
            self.headerClickBlock(params);
        }
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
