//
//  OrderAfterSaleCertificatePhotoInfoCell.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/9/18.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
@protocol PhotosOperatorDelegate <NSObject>

@optional
- (void)photoSelectOperator:(NSIndexPath *)index selectImage:(UIImage *)image;
- (void)deletePhotoOperator:(NSIndexPath *)index;
@end


@interface OrderAfterSaleCertificatePhotoInfoCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel   *cellTipLabel;
@property (nonatomic, strong) NSMutableArray *photoArrs;
@property (nonatomic, weak) id <PhotosOperatorDelegate>delegate;

@end
