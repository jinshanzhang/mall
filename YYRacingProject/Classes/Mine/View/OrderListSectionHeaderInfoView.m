//
//  OrderListSectionHeaderInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderListSectionHeaderInfoView.h"

@interface OrderListSectionHeaderInfoView()

@property (nonatomic, strong) UILabel  *businessTitleLabel;
@property (nonatomic, strong) UIImageView  *markImageView;
@property (nonatomic, strong) UILabel  *orderStatuLabel;

@property (nonatomic, strong) UIView   *backGroundView;
@property (nonatomic, strong) UIView   *bottomLine;

@end

@implementation OrderListSectionHeaderInfoView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI {
    self.backGroundView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.backGroundView];
    
    self.markImageView = [YYCreateTools createImageView:nil
                                              viewModel:-1];
    [self addSubview:self.markImageView];
    
    self.businessTitleLabel = [YYCreateTools createLabel:nil
                                                    font:midFont(13) textColor:XHBlackColor];
    [self.backGroundView addSubview:self.businessTitleLabel];
    
    self.orderStatuLabel = [YYCreateTools createLabel:nil
                                                    font:midFont(13) textColor:XHRedColor];
    [self.backGroundView addSubview:self.orderStatuLabel];
    
    self.bottomLine = [YYCreateTools createView:XHLightColor];
    [self.backGroundView addSubview:self.bottomLine];
    
    [self.backGroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self);
        make.height.mas_equalTo(kSizeScale(40)).priorityHigh();
    }];
    [self.markImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backGroundView.mas_left).offset(kSizeScale(12));
        make.top.equalTo(self.backGroundView.mas_top).offset(kSizeScale(12));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(15), kSizeScale(15)));
    }];
    [self.businessTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.markImageView.mas_right).offset(kSizeScale(6));
        make.top.equalTo(self.backGroundView.mas_top).offset(kSizeScale(12));
    }];
    [self.orderStatuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backGroundView.mas_top).offset(kSizeScale(12));
        make.right.equalTo(self.backGroundView.mas_right).offset(-kSizeScale(12));
    }];
    [self.bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backGroundView.mas_left).offset(kSizeScale(12));
        make.right.equalTo(self.orderStatuLabel);
        make.bottom.equalTo(self.backGroundView);
        make.height.mas_equalTo(1);
    }];
}

- (void)setListModel:(OrderListInfoModel *)listModel {
    _listModel = listModel;
    if (_listModel) {
        NSInteger pageSource = [_listModel.source integerValue];
        if (pageSource == 1) {
            // 个人中心订单
            self.markImageView.hidden = YES;
            [self.businessTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.backGroundView.mas_left).offset(kSizeScale(12));
                make.top.equalTo(self.backGroundView.mas_top).offset(kSizeScale(12));
            }];
            self.businessTitleLabel.text = _listModel.shopName;
        }
        else if (pageSource == 2) {
            // 店铺订单
            BOOL isShowMark = _listModel.isShowMark;
            if (isShowMark) {
                self.markImageView.hidden = NO;
                self.markImageView.image = [UIImage imageNamed:_listModel.earnFlag?@"order_province_icon":@"order_earn_icon"];
                [self.businessTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.markImageView.mas_right).offset(kSizeScale(6));
                    make.top.equalTo(self.backGroundView.mas_top).offset(kSizeScale(12));
                }];
            }
            else {
                self.markImageView.hidden = YES;
                [self.businessTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.backGroundView.mas_left).offset(kSizeScale(12));
                    make.top.equalTo(self.backGroundView.mas_top).offset(kSizeScale(12));
                }];
            }
            self.businessTitleLabel.text = [NSString formatDateAndTime:[_listModel.orderTime longValue]];
        }
    }
}

- (void)setOrderStatus:(NSNumber *)orderStatus {
    _orderStatus = orderStatus;
    self.orderStatuLabel.text = [YYCommonTools showQueryOrderStatus:orderStatus];
}

@end
