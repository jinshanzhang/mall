//
//  SignBankCell.h
//  YYRacingProject
//
//  Created by 侯剑儒 on 2018/9/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YYBaseTableViewCell.h"
#import "BankModel.h"

@interface SignBankCell : YYBaseTableViewCell

@property (nonatomic, strong) UILabel  *nameLabel;

@property (nonatomic, strong) BankModel *model;

@end
