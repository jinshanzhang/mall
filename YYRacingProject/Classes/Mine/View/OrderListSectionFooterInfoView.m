//
//  OrderListSectionFooterInfoView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "OrderListSectionFooterInfoView.h"

#define  kOperatorTag 400
@interface OrderListSectionFooterInfoView()

@property (nonatomic, strong) UIView  *lineView;
@property (nonatomic, strong) UIView  *balanceView;
@property (nonatomic, strong) UIView  *operateView;
@property (nonatomic, strong) UIView  *bottomView;

@property (nonatomic, strong) UILabel *balanceLabel;
@property (nonatomic, strong) NSMutableArray *operatorBtnArrs;  //操作按钮数组
@property (nonatomic, strong) NSMutableArray *operatorModels;

@property (nonatomic, strong) MASConstraint *operateHeight;
@property (nonatomic, strong) UIButton *tempBtn;

@end

@implementation OrderListSectionFooterInfoView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        // 监听通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDownNotification) name:OYCountDownNotification object:nil];
        self.operatorBtnArrs = [NSMutableArray array];
        self.operatorModels = [NSMutableArray array];
        [self createUI];
    }
    return self;
}

- (void)createUI {
    
    self.lineView = [YYCreateTools createView:XHLightColor];
    [self addSubview:self.lineView];
    
    self.balanceView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.balanceView];
    
    self.balanceLabel = [YYCreateTools createLabel:nil
                                              font:midFont(13)
                                         textColor:XHBlackColor];
    [self.balanceView addSubview:self.balanceLabel];

    self.operateView = [YYCreateTools createView:XHWhiteColor];
    [self addSubview:self.operateView];
    
    self.bottomView = [YYCreateTools createView:XHLightColor];
    [self addSubview:self.bottomView];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(1));
    }];
    
    [self.balanceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.lineView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(40)).priorityHigh();
    }];
    
    [self.balanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.balanceView.mas_right).offset(-kSizeScale(13));
        make.centerY.equalTo(self.balanceView);
    }];
    
    [self.operateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.balanceView.mas_bottom);
        self.operateHeight = make.height.mas_equalTo(kSizeScale(40)).priorityHigh();
    }];
    
    [self.operateHeight install];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.operateView.mas_bottom);
        make.height.mas_equalTo(kSizeScale(8)).priorityHigh();
        make.bottom.equalTo(self);
    }];
    for (int i = 0; i < 4; i ++) {
        UIButton *btn = [YYCreateTools createBtn:@""
                                            font:midFont(12)
                                       textColor:XHBlackMidColor];
        [btn setTitleColor:XHBlackMidColor forState:UIControlStateNormal];
        [btn setTitleColor:XHLoginColor forState:UIControlStateSelected];
        [btn setBackgroundImage:[UIImage imageWithColor:XHWhiteColor] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageWithColor:XHWhiteColor] forState:UIControlStateSelected];
        btn.tag = kOperatorTag + i;
        btn.layer.cornerRadius = 2;
        btn.layer.masksToBounds = YES;
        [btn addTarget:self action:@selector(buttonClickEvent:) forControlEvents:UIControlEventTouchUpInside];
        [self.operateView addSubview:btn];
        [self.operatorBtnArrs addObject:btn];
    }
}

- (void)setListModel:(OrderListInfoModel *)listModel {
    _listModel = listModel;
    if (_listModel) {
        __block NSInteger removeAtIndex = -1;
        __block BOOL  storeOrderDeleteBtnIsRemove = NO;
        NSMutableAttributedString *attribut = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"共%ld件商品   总计：",_listModel.skuList.count]];
        if (kValidArray(_listModel.summaryList)) {
            __block NSString *totalString = @"";
            __block NSString *freightString = @"";
            __block NSString *couponString = @"";
            
            [_listModel.summaryList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                 OrderPriceInfoModel *priceModel = (OrderPriceInfoModel *)obj;
                if ([priceModel.type integerValue] == 4) {
                    totalString = priceModel.priceTag;
                }
                if ([priceModel.type integerValue] == 5) {
                    freightString = priceModel.priceTag;
                }
                if ([priceModel.type integerValue] == 10) {
                    couponString = priceModel.priceTag;
                }
            }];
            NSMutableAttributedString *moneyAtti = [YYCommonTools containSpecialSymbolHandler:@"¥" symbolFont:boldFont(13) symbolTextColor:XHBlackColor wordSpace:2 price:[NSString stringWithFormat:@"%@",totalString] priceFont:boldFont(13) priceTextColor: XHBlackColor symbolOffsetY:0];
            [attribut appendAttributedString:moneyAtti];
//            if ([_listModel.source integerValue] == 2) {
//                NSMutableAttributedString *freightAtti = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"（含运费¥%@)",freightString]];
//                [attribut appendAttributedString:freightAtti];
//            }
//            else {
                if (kValidString(couponString)) {
                    NSMutableAttributedString *couponAtti = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"（优惠-¥%@)",couponString]];
                    [attribut appendAttributedString:couponAtti];
                }
//            }
        }
        self.operatorModels = [YYCommonTools getOrderShowOperatorBtns:_listModel.operation];
        
        if ([_listModel.source integerValue] == 2) {
            [self.operatorModels enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                YYOperatorInfoModel *model = (YYOperatorInfoModel *)obj;
                if (model.operatorType == YYOrderOperatorDelete) {
                    storeOrderDeleteBtnIsRemove = YES;
                    removeAtIndex = idx;
                }
            }];
            if (storeOrderDeleteBtnIsRemove && removeAtIndex != -1) {
                [self.operatorModels removeObjectAtIndex:removeAtIndex];
            }
        }
        [self operatorBtnIsShow];
        [self countDownNotification];
        self.balanceLabel.attributedText = attribut;
    }
}

- (void)operatorBtnIsShow {
    if (kValidArray(self.operatorModels)) {
        CGFloat btnWidth = kSizeScale(68);
        CGFloat btnHeight = kSizeScale(25);
        [self.operateHeight install];
        UIButton *lastBtn = nil;
        for (int i = 0; i < self.operatorBtnArrs.count ; i ++) {
            UIButton *btn = [self.operatorBtnArrs objectAtIndex:i];
            if (i < self.operatorModels.count) {
                YYOperatorInfoModel *infoModel = self.operatorModels[i];
                if (infoModel.isWaitPay) {
                    btnWidth = kSizeScale(82);
                    self.tempBtn = btn;
                }
                else {
                    btnWidth = kSizeScale(68);
                }
                btn.layer.borderWidth = 0.8;
                btn.layer.borderColor = (infoModel.isSelect ? XHLoginColor.CGColor : XHLineColor.CGColor);
                [btn setTitle:infoModel.showTitle forState:UIControlStateNormal];
                [btn setTitle:infoModel.showTitle forState:UIControlStateSelected];
                btn.selected = infoModel.isSelect;
                btn.hidden = NO;
                if (lastBtn) {
                    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.centerY.equalTo(lastBtn);
                        make.right.equalTo(lastBtn.mas_left).offset(-kSizeScale(12));
                        make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                    }];
                }
                else {
                    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                        make.centerY.equalTo(self.operateView).offset(-kSizeScale(3));
                        make.right.equalTo(self.operateView.mas_right).offset(-kSizeScale(12));
                        make.size.mas_equalTo(CGSizeMake(btnWidth, btnHeight)).priorityHigh();
                    }];
                }
                lastBtn = btn;
            }
            else {
                btn.hidden = YES;
            }
        }
    }
    else {
        for (int i = 0; i < self.operatorBtnArrs.count ; i ++) {
            UIButton *btn = [self.operatorBtnArrs objectAtIndex:i];
            btn.hidden = YES;
        }
        [self.operateHeight uninstall];
    }
}
#pragma mark -Event
- (void)countDownNotification {
    if ([self.listModel.orderStatus integerValue] == 2) {
        NSInteger timeInterval;
        if (kValidString(self.listModel.countDownIdentifier)) {
            timeInterval = [kCountDownManager timeIntervalWithIdentifier:self.listModel.countDownIdentifier];
        }else {
            timeInterval = kCountDownManager.timeInterval;
        }
        if (self.listModel.diffValue > 0) {
            NSString *timeFormat = nil;
            NSInteger count =  self.listModel.diffValue - timeInterval;
            int seconds = count % 60;
            int minutes = (count / 60) % 60;
            if (count > 0) {
                timeFormat = [NSString stringWithFormat:@"付款%02d:%02d", minutes, seconds];
            }
            else {
                timeFormat = @"付款";
            }
            [self.tempBtn setTitle:timeFormat forState:UIControlStateNormal];
            [self.tempBtn setTitle:timeFormat forState:UIControlStateSelected];
        }
    }
}

- (void)buttonClickEvent:(UIButton *)sender {
    NSInteger currentTag = sender.tag - kOperatorTag;
    if (currentTag < self.operatorModels.count) {
        YYOperatorInfoModel *model = self.operatorModels[currentTag];
        if (self.sectionFooterBtnOperatorBlock) {
            self.sectionFooterBtnOperatorBlock(model.operatorType, self.listModel);
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
