//
//  OrderLogisticsPointInfoView.h
//  YYRacingProject
//
//  Created by cjm on 2018/8/4.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderLogisticsPointInfoView : UIView

@property (nonatomic, assign) YYLogisticsDrawType drawType;

@end
