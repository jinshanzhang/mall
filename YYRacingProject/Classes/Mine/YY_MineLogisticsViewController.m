//
//  YY_MineLogisticsViewController.m
//  YYRacingProject
//
//  Created by cjm on 2018/8/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "YY_MineLogisticsViewController.h"

#import "HomeMenuTitleView.h"
#import "HomeTimeAxleInfoModel.h"
#import "OrderLogisticsInfoModel.h"

#import "OrderListGoodInfoCell.h"
#import "OrderLogisticsPointInfoCell.h"
#import "OrderLogisticsCompanyInfoCell.h"

#import "OrderLogisticsHeaderInfoView.h"

#import "MineOrderLookLogisticsInfoRequestAPI.h"
#import "YYexpressDetailAPI.h"

@interface YY_MineLogisticsViewController ()
<UITableViewDelegate,
UITableViewDataSource,
HomeMenuTitleViewDelegate>

@property (nonatomic, strong) NSNumber *orderID;
@property (nonatomic, strong) NSNumber *skuID;

@property (nonatomic, strong) NSMutableArray *packageArrays;
@property (nonatomic, strong) NSMutableArray *logisticsArrays;


@property (nonatomic, assign) NSInteger tableViewSectionCount;
@property (nonatomic, strong) OrderLogisticsInfoModel *logisticsModel;
@property (nonatomic, strong) OrderLogisticsListModel *selectLogisticModel;

@property (nonatomic, strong) HomeMenuTitleView   *menuView;
@property (nonatomic, strong) OrderLogisticsHeaderInfoView *headerView;

@end

@implementation YY_MineLogisticsViewController

#pragma mark - Setter
- (OrderLogisticsHeaderInfoView *)headerView {
    if (!_headerView) {
        _headerView = [[OrderLogisticsHeaderInfoView alloc] initWithFrame:CGRectZero];
    }
    return _headerView;
}

#pragma mark - Life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self xh_addTitle:@"物流详情"];
    [self xh_navBottomLine:XHLightColor];
    [self xh_popTopRootViewController:NO];
    
    self.packageArrays = [NSMutableArray array];
    self.logisticsArrays = [NSMutableArray array];
    
    self.tableStyle = UITableViewStylePlain;
    self.m_tableView.delegate = self;
    self.m_tableView.dataSource = self;
    self.m_tableView.estimatedRowHeight = 44.0;
    self.m_tableView.rowHeight = UITableViewAutomaticDimension;
    self.m_tableView.backgroundColor = XHLightColor;
    
    Class currentCls = [OrderListGoodInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [OrderLogisticsPointInfoCell class];
    registerClass(self.m_tableView, currentCls);
    currentCls = [OrderLogisticsCompanyInfoCell class];
    registerClass(self.m_tableView, currentCls);
    
    [self.view addSubview:self.m_tableView];
    
    if (kValidDictionary(self.parameter)) {
        if ([self.parameter objectForKey:@"orderID"]) {
            self.orderID = [self.parameter objectForKey:@"orderID"];
        }
        if ([self.parameter objectForKey:@"skuID"]) {
            self.skuID = [self.parameter objectForKey:@"skuID"];
        }
    }
    
    [self.m_tableView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.right.bottom.equalTo(self.view);
        make.top.mas_equalTo(kNavigationH);
    }];
    
    self.m_tableView.tableHeaderView = self.headerView;
    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.m_tableView);
        make.width.mas_equalTo(kScreenW);
    }];
    
    if (self.skuID) {
        [self afterExpressLogisticsRequestOperator];
    }else{
        [self lookLogisticsRequestOperator];
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Request
/**获取物流信息**/
- (void)afterExpressLogisticsRequestOperator {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.orderID) {
        [params setObject:self.orderID forKey:@"orderId"];
        [params setObject:self.skuID forKey:@"skuId"];
    }
    YYexpressDetailAPI *logisticsAPI = [[YYexpressDetailAPI alloc] initRequest:params];
    [YYCenterLoading showCenterLoading];
    [logisticsAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.headerView.headerTitle = @"商品信息";
            [self.m_tableView layoutIfNeeded];
            self.m_tableView.tableHeaderView = self.headerView;
            
            self.tableViewSectionCount = 2;
            self.logisticsModel = [OrderLogisticsInfoModel modelWithJSON:responDict];
            [self.logisticsModel.logisticsList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                OrderLogisticsListModel *model = (OrderLogisticsListModel *)obj;
                if (model.carrier) {
                    [model.expressInfoList insertObject:model.carrier
                                                atIndex:0];
                }
            }];
            if (kValidArray(self.logisticsModel.logisticsList)) {
                self.selectLogisticModel = [self.logisticsModel.logisticsList firstObject];
            }
            for (int i = 0; i < self.logisticsModel.logisticsList.count; i ++) {
                HomeTimeAxleInfoModel *axleModel = [[HomeTimeAxleInfoModel alloc] init];
                axleModel.chooseFlag = (i==0?@(1):@(0));
                /*NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                 formatter.numberStyle = kCFNumberFormatterRoundHalfDown;
                 NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans"];
                 formatter.locale = locale;
                 NSString *string = [formatter stringFromNumber:[NSNumber numberWithInt:i+1]];
                 */
                axleModel.instruction = axleModel.timePointAlias = [NSString stringWithFormat:@"包裹%d",i+1];
                [self.packageArrays addObject:axleModel];
            }
        }
        else {
            self.tableViewSectionCount = 0;
            self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                               title:nil
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
        }
        [self.m_tableView reloadData];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.tableViewSectionCount = 0;
        self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                           title:nil
                                                          target:self
                                                          action:@selector(emptyBtnOperator:)];
    }];
}

/**获取物流信息**/
- (void)lookLogisticsRequestOperator {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (self.orderID) {
        [params setObject:self.orderID forKey:@"orderId"];
    }
    MineOrderLookLogisticsInfoRequestAPI *logisticsAPI = [[MineOrderLookLogisticsInfoRequestAPI alloc] initLookLogisticsInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [logisticsAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            self.headerView.headerTitle = @"商品信息";
            [self.m_tableView layoutIfNeeded];
            self.m_tableView.tableHeaderView = self.headerView;
    
            self.tableViewSectionCount = 2;
            self.logisticsModel = [OrderLogisticsInfoModel modelWithJSON:responDict];
            [self.logisticsModel.logisticsList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                OrderLogisticsListModel *model = (OrderLogisticsListModel *)obj;
                if (model.carrier) {
                    [model.expressInfoList insertObject:model.carrier
                                                atIndex:0];
                }
            }];
            if (kValidArray(self.logisticsModel.logisticsList)) {
                self.selectLogisticModel = [self.logisticsModel.logisticsList firstObject];
            }
            for (int i = 0; i < self.logisticsModel.logisticsList.count; i ++) {
                HomeTimeAxleInfoModel *axleModel = [[HomeTimeAxleInfoModel alloc] init];
                axleModel.chooseFlag = (i==0?@(1):@(0));
                /*NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                formatter.numberStyle = kCFNumberFormatterRoundHalfDown;
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans"];
                formatter.locale = locale;
                NSString *string = [formatter stringFromNumber:[NSNumber numberWithInt:i+1]];
                */
                axleModel.instruction = axleModel.timePointAlias = [NSString stringWithFormat:@"包裹%d",i+1];
                [self.packageArrays addObject:axleModel];
             }
        }
        else {
            self.tableViewSectionCount = 0;
            self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                               title:nil
                                                              target:self
                                                              action:@selector(emptyBtnOperator:)];
        }
        [self.m_tableView reloadData];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        self.tableViewSectionCount = 0;
        self.m_tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOverTimeType
                                                           title:nil
                                                          target:self
                                                          action:@selector(emptyBtnOperator:)];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.tableViewSectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.logisticsModel.skuList.count;
    }
    else {
        return self.selectLogisticModel.expressInfoList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    NSInteger row = indexPath.row;
    NSInteger section = indexPath.section;
    Class currentCls = [OrderListGoodInfoCell class];
    if (section == 0) {
        OrderListGoodInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
        
        cell.goodModel = [self.logisticsModel.skuList objectAtIndex:row];
         return cell;
    }
    else {
        id logisticsModel = nil;
        currentCls = [OrderLogisticsPointInfoCell class];
        if (row < self.selectLogisticModel.expressInfoList.count) {
            logisticsModel = [self.selectLogisticModel.expressInfoList objectAtIndex:row];
            if ([logisticsModel isKindOfClass:[OrderLogisticCarrierInfoModel class]]) {
                OrderLogisticCarrierInfoModel *carrierModel = (OrderLogisticCarrierInfoModel *)logisticsModel;
                currentCls = [OrderLogisticsCompanyInfoCell class];
                OrderLogisticsCompanyInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
                cell.carrierModel = carrierModel;
                return cell;
            }
            else if ([logisticsModel isKindOfClass:[OrderLogisticsPointInfoModel class]])
            {
                OrderLogisticsPointInfoModel *pointModel = (OrderLogisticsPointInfoModel *)logisticsModel;
                OrderLogisticsPointInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:strFromCls(currentCls)];
                if (row == 1) {
                    pointModel.drawType = YYLogisticsDrawTopType;
                }
                else if (row == self.selectLogisticModel.expressInfoList.count-1) {
                    pointModel.drawType = YYLogisticsDrawBottomType;
                }
                else {
                    pointModel.drawType = YYLogisticsDrawMidType;
                }
                cell.pointModel = pointModel;
                return cell;
            }
        }
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) {
        if (!self.menuView) {
            self.menuView = [[HomeMenuTitleView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(36))];
            self.menuView.delegate = self;
        }
        self.menuView.isCenter = NO;
        self.menuView.titleArrays = self.packageArrays;
        return self.menuView;
    }
    else {
        return [YYCreateTools createView:XHWhiteColor];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [YYCreateTools createView:XHLightColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return (section == 0 ? 0.000001f : kSizeScale(36));
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return (section == 0 ? 8.f : 0.00001f);
}

#pragma mark - HomeMenuTitleViewDelegate
- (void)homeMenuClickSelectItemAtIndex:(NSInteger)selectIndex {
    CGFloat currentOffsetY = self.m_tableView.contentOffset.y;
    CGFloat headerY = [self.m_tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]].origin.y;
    if (kValidArray(self.logisticsModel.logisticsList) && selectIndex < self.logisticsModel.logisticsList.count) {
        self.selectLogisticModel = [self.logisticsModel.logisticsList objectAtIndex:selectIndex];
    }
    if (headerY  < currentOffsetY) {
        // 没有悬停不需要滚动到顶部
        [self.m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    [UIView performWithoutAnimation:^{
        [self.m_tableView reloadData];
    }];
}

#pragma mark - Event
- (void)emptyBtnOperator:(UIButton *)sender {
    [self lookLogisticsRequestOperator];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
