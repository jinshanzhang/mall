//
//  MchListModel.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/22.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchListModel.h"

@implementation BusinessAddressVo

@end

@implementation FictitiousGoodsVoList

//+ (NSDictionary *)modelContainerPropertyGenericClass {
//    return @{
//             @"businessAddressVo":[BusinessAddressVo class]
//    };
//}

@end

@implementation MchListModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
             @"sellerId" : @"id"
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"items":[MchListModel class],
             @"goodsItems":[FictitiousGoodsVoList class]
    };
}

@end
