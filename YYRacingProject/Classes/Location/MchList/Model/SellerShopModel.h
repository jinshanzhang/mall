//
//  SellerShopModel.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/7.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SellerShopModel : NSObject

@property (nonatomic, copy) NSString * sellerId;
@property (nonatomic , assign) NSInteger              companyId;
@property (nonatomic , assign) BOOL              depositStatusFlag;
@property (nonatomic , assign) NSInteger              deposit;
@property (nonatomic , assign) NSInteger              userId;
//@property (nonatomic , assign) NSInteger              sellerType;
@property (nonatomic , assign) NSInteger              sellerScore;
@property (nonatomic , assign) NSInteger              signedPlatform;
@property (nonatomic , copy) NSString              * email;
@property (nonatomic , assign) NSInteger              status;
@property (nonatomic, copy) NSString * shopName;
@property (nonatomic, copy) NSString * sellerType;
@property (nonatomic, copy) NSString * saleTime;
@property (nonatomic, copy) NSString * phone;
@property (nonatomic, copy) NSString * describe;
@property (nonatomic, copy) NSString * shopLogo;
@property (nonatomic, copy) NSString * address;

@property (nonatomic, copy) NSString * latitude;
@property (nonatomic, copy) NSString * longitude;

@property (nonatomic, copy) NSString * province;
@property (nonatomic, copy) NSString * district;
@property (nonatomic, copy) NSString * street;
@property (nonatomic, copy) NSString * city;

@end

NS_ASSUME_NONNULL_END
