//
//  MchListModel.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/22.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessAddressVo :NSObject
@property (nonatomic , copy) NSString              * district;
@property (nonatomic , copy) NSString              * city;
@property (nonatomic , copy) NSString              * street;
@property (nonatomic , copy) NSString              * streetNum;
@property (nonatomic , copy) NSString              * cityCode;
@property (nonatomic , copy) NSString              * adCode;
@property (nonatomic , copy) NSString              * address;
@property (nonatomic , assign) CGFloat              longitude;
@property (nonatomic , assign) CGFloat              latitude;
@property (nonatomic , assign) CGFloat              distance;
@property (nonatomic , copy) NSString              * province;



@end

@interface FictitiousGoodsVoList :NSObject

@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * url;
@property (nonatomic , copy) NSString              * goodsTitle;
@property (nonatomic , copy) NSString              * shopName;
@property (nonatomic , assign) NSInteger              specialSellStatus;
@property (nonatomic , copy) NSString              * shareUrl;
@property (nonatomic , copy) NSString              * sourceIcon;
@property (nonatomic , copy) NSString              * goodsShortTitle;
@property (nonatomic , copy) NSString              * goodsShareImage;
@property (nonatomic , copy) NSString              * reservePrice;
@property (nonatomic , copy) NSString              * imageUrl;
@property (nonatomic , copy) NSString              * shopAvatorUrl;
@property (nonatomic , copy) NSString              * payment;
@property (nonatomic , copy) NSString              * commission;
//@property (nonatomic , copy) NSString              * newTagIcon;
@property (nonatomic , assign) NSInteger              comId;
@property (nonatomic , copy) NSString              * originPrice;
@property (nonatomic , copy) NSString              * specialSellDate;
@property (nonatomic , copy) NSString              * salesVolume;
@property (nonatomic , assign) NSInteger              goodsType;

@end

@interface MchListModel : NSObject

@property (nonatomic, copy) NSString * sellerId;
@property (nonatomic , assign) NSInteger              companyId;
@property (nonatomic , assign) BOOL              depositStatusFlag;
@property (nonatomic , assign) NSInteger              deposit;
@property (nonatomic , assign) NSInteger              userId;
//@property (nonatomic , assign) NSInteger              sellerType;
@property (nonatomic , assign) NSInteger              sellerScore;
@property (nonatomic , assign) NSInteger              signedPlatform;
@property (nonatomic , copy) NSString              * email;
@property (nonatomic , assign) NSInteger              status;
@property (nonatomic, copy) NSString * shopImgUrl;
@property (nonatomic, copy) NSString * shopName;
@property (nonatomic, copy) NSString * sellerType;
@property (nonatomic, copy) NSString * saleTime;
@property (nonatomic, copy) NSString * phone;
@property (nonatomic , strong) BusinessAddressVo             * businessAddressVo;
@property (nonatomic , strong) NSArray<FictitiousGoodsVoList *>              * goodsItems;
@property (nonatomic, copy) NSString * describe;

@end

NS_ASSUME_NONNULL_END
