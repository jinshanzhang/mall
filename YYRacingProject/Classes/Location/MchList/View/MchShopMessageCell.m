//
//  MchShopHeaderView.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchShopMessageCell.h"

@interface  MchShopMessageCell ()

@property (nonatomic, strong) UIImageView * productImageView;
@property (nonatomic, strong) UILabel * productNameLabel;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UILabel * roundLabel;
@property (nonatomic, strong) UIView * lineView;


@end

@implementation MchShopMessageCell

//- (instancetype)init {
//    if(self = [super init]) {
//        [self setUpView];
//    }
//    return self;
//}
//
//- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
//    if(self = [super initWithReuseIdentifier:reuseIdentifier]) {
//        [self setUpView];
//    }
//    return self;
//}

-(void)setFrame:(CGRect)frame{
    CGFloat margin = kSizeScale(12);
    frame.origin.x = margin;
    frame.size.width = kScreenW - margin * 2;
    [super setFrame:frame];
}

- (void)js_createSubViews {
    [self setUpView];
}

- (void)setUpView {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.productImageView];
    [self addSubview:self.productNameLabel];
    [self addSubview:self.roundLabel];
    [self addSubview:self.addressLabel];
    [self addSubview:self.lineView];
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(12));
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kSizeScale(60));
    }];
    
    [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView).offset(kSizeScale(4));
        make.left.equalTo(self.productImageView.mas_right).offset(kSizeScale(12));
        make.right.equalTo(self).offset(-kSizeScale(12));
    }];
    
    [self.roundLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-kSizeScale(10));
//        make.centerY.equalTo(self.addressLabel);
        make.bottom.equalTo(self.productImageView).offset(-kSizeScale(4));
        make.width.mas_lessThanOrEqualTo(kSizeScale(60));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.productImageView.mas_right).offset(kSizeScale(12));
//        make.bottom.equalTo(self.productImageView).offset(-kSizeScale(4));
        make.centerY.equalTo(self.roundLabel);
   make.right.lessThanOrEqualTo(self.roundLabel.mas_left).offset(-kSizeScale(12));
    }];
    
    
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.productNameLabel);
        make.right.bottom.equalTo(self);
        make.size.mas_equalTo(kSizeScale(0.5));
    }];
    
}

#pragma mark
- (UIImageView *)productImageView {
    if(!_productImageView) {
        _productImageView = [[UIImageView alloc] init];
        _productImageView.backgroundColor = XHLightColor;
        _productImageView.layer.cornerRadius = kSizeScale(8.0);
        _productImageView.clipsToBounds = YES;
    }
    return _productImageView;
}

- (UILabel *)productNameLabel {
    if(!_productNameLabel) {
        _productNameLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(16)];
    }
    return _productNameLabel;
}

- (UILabel *)addressLabel {
    if(!_addressLabel) {
        _addressLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentLeft font:midFont(13)];
        _addressLabel.textAlignment = NSTextAlignmentLeft;
//        _addressLabel.backgroundColor = [UIColor redColor];
        _addressLabel.numberOfLines = 2;
    }
    return _addressLabel;
}

- (UILabel *)roundLabel {
    if(!_roundLabel) {
        _roundLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentRight font:normalFont(12)];
    }
    return _roundLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}

-(void)setModel:(MchListModel *)model {
    _model = model;
    if(model) {
        if(kValidString(model.shopImgUrl)) {
            [self.productImageView sd_setImageWithURL:[NSURL URLWithString:model.shopImgUrl]];
        }
        if(kValidString(model.shopName)) {
            self.productNameLabel.text = model.shopName;
        }
        self.addressLabel.text = [self removeForeAndBehindSpaceWithString:[NSString stringWithFormat:@"   %@%@%@%@%@",model.businessAddressVo.province,model.businessAddressVo.city,model.businessAddressVo.district,model.businessAddressVo.street,model.businessAddressVo.address]];
        if(model.businessAddressVo.distance < 1000) {
            self.roundLabel.text = [NSString stringWithFormat:@"%.1fm",model.businessAddressVo.distance];
        } else {
            self.roundLabel.text = [NSString stringWithFormat:@"%.1fkm",model.businessAddressVo.distance / 1000.0];
        }
    }
}

- (NSString *)removeForeAndBehindSpaceWithString:(NSString *)str {
    NSString *tmp = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return [tmp stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
