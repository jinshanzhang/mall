//
//  SelectSortTypeListCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "SelectSortTypeListCell.h"

@implementation SelectSortTypeListCell

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.itemNameLabel];
    [self addSubview:self.selectImageView];
    [self addSubview:self.lineView];
}

- (void)js_layoutSubViews {
    [self.itemNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(12));
        make.centerY.equalTo(self);
    }];
    
    [self.selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-kSizeScale(12));
        make.centerY.equalTo(self);
        make.width.mas_equalTo(kSizeScale(14));
        make.height.mas_equalTo(kSizeScale(10));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.itemNameLabel);
        make.right.bottom.equalTo(self);
        make.height.mas_equalTo(kSizeScale(0.5));
    }];
}

- (UIImageView *)selectImageView {
    if(!_selectImageView) {
        _selectImageView = [[UIImageView alloc] init];
        _selectImageView.image = [UIImage imageNamed:@"icon_shangjia_xuanze"];;
    }
    return _selectImageView;
}

- (UILabel *)itemNameLabel {
    if(!_itemNameLabel) {
        _itemNameLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(13)];
    }
    return _itemNameLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}


@end
