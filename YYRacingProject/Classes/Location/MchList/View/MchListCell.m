//
//  MchListCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchListCell.h"

@interface MchListCell()

@property (nonatomic, strong) UIImageView * productImageView;
@property (nonatomic, strong) UILabel * productNameLabel;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UILabel * originPriceLabel;
@property (nonatomic, strong) UILabel * saleCountLabel;
@property (nonatomic, strong) UIView * lineView;

@end

@implementation MchListCell

-(void)setFrame:(CGRect)frame{
    CGFloat margin = kSizeScale(12);
    frame.origin.x = margin;
    frame.size.width = kScreenW - margin * 2;
    [super setFrame:frame];
}

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.productImageView];
    [self addSubview:self.productNameLabel];
    [self addSubview:self.saleCountLabel];
    [self addSubview:self.priceLabel];
    [self addSubview:self.originPriceLabel];
    [self addSubview:self.lineView];
}

- (void)js_layoutSubViews {
   [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(84));
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kSizeScale(50));
    }];
    
    [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView).offset(kSizeScale(4));
        make.left.equalTo(self.productImageView.mas_right).offset(kSizeScale(12));
        make.right.equalTo(self).offset(-kSizeScale(80));
    }];
    
    [self.saleCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.productNameLabel);
        make.right.equalTo(self).offset(-kSizeScale(10));
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.productNameLabel);
        make.bottom.equalTo(self.productImageView).offset(-kSizeScale(4));
    }];
    
    [self.originPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLabel.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.priceLabel);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.centerY.equalTo(self.originPriceLabel);
        make.height.mas_equalTo(kSizeScale(0.5));
    }];
}

- (UIImageView *)productImageView {
    if(!_productImageView) {
        _productImageView = [[UIImageView alloc] init];
        _productImageView.backgroundColor = XHLightColor;
        _productImageView.layer.cornerRadius = kSizeScale(4.0);
        _productImageView.clipsToBounds = YES;
    }
    return _productImageView;
}

- (UILabel *)productNameLabel {
    if(!_productNameLabel) {
        _productNameLabel = [UILabel creatLabelWithTitle:@" " textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(11)];
    }
    return _productNameLabel;
}

- (UILabel *)priceLabel {
    if(!_priceLabel) {
        _priceLabel = [UILabel creatLabelWithTitle:@"--" textColor:HexRGB(0xFF583D) textAlignment:NSTextAlignmentLeft font:boldFont(14)];
    }
    return _priceLabel;
}

- (UILabel *)originPriceLabel {
    if(!_originPriceLabel) {
        _originPriceLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentLeft font:midFont(10)];
    }
    return _originPriceLabel;
}

- (UILabel *)saleCountLabel {
    if(!_saleCountLabel) {
        _saleCountLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentRight font:midFont(10)];
    }
    return _saleCountLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = XHBlackLitColor;
    }
    return _lineView;
}

-(void)setModel:(FictitiousGoodsVoList *)model {
    _model = model;
    if(model) {
        if(kValidString(model.imageUrl)) {
            [self.productImageView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl]];
        }
        if(kValidString(model.reservePrice)) {
            self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",model.reservePrice.floatValue];
        }
        if(kValidString(model.originPrice) && model.originPrice.floatValue > 0) {
            self.originPriceLabel.hidden = NO;
            self.lineView.hidden = NO;
            self.originPriceLabel.text = [NSString stringWithFormat:@"¥%.2f",model.originPrice.floatValue];
        } else {
            self.originPriceLabel.hidden = YES;
            self.lineView.hidden = YES;
        }
        self.productNameLabel.text = model.title;
        self.saleCountLabel.text = [NSString stringWithFormat:@"已售%ld份",(long)model.salesVolume.intValue];
    }
}

@end
