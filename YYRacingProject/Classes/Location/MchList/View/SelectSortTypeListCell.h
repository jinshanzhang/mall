//
//  SelectSortTypeListCell.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MchListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectSortTypeListCell : BaseTableViewCell

@property (nonatomic, strong) UIImageView * selectImageView;
@property (nonatomic, strong) UILabel * itemNameLabel;
@property (nonatomic, strong) UIView * lineView;

@end

NS_ASSUME_NONNULL_END
