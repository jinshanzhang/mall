//
//  MchListCell.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MchListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MchListCell : BaseTableViewCell

@property (nonatomic, strong) FictitiousGoodsVoList * model;

@end

NS_ASSUME_NONNULL_END
