//
//  SelectSortTypeView.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SelectSortResultDelegate <NSObject>

- (void)selectSortResult:(NSString *)selectSortTitle index:(NSInteger)index;

@end

@interface SelectSortTypeView : UIButton

@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, copy) NSString * selectSortTitle;

@property (nonatomic, weak) id<SelectSortResultDelegate> delegate;

- (void)show;

@end

NS_ASSUME_NONNULL_END
