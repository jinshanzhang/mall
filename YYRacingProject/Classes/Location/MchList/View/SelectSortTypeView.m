//
//  SelectSortTypeView.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "SelectSortTypeView.h"
#import "SelectSortTypeListCell.h"
@interface SelectSortTypeView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView * tableView;

@end

@implementation SelectSortTypeView

- (instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        [self setUpView];
    }
    return self;
}

- (void)setUpView {
    self.selectSortTitle = @"";
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
//    [self addGestureRecognizer:tap];
    [self addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    self.dataArray = [NSMutableArray array];
    self.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[SelectSortTypeListCell class] forCellReuseIdentifier:@"SelectSortTypeListCell"];
    [self addSubview:self.tableView];
}

- (void)dismiss {
    [self removeFromSuperview];
}

- (void)show {
    [self.tableView reloadData];
    [UIView animateWithDuration:0.2 animations:^{
        self.tableView.frame = CGRectMake(0, 0, kScreenW, self.dataArray.count * kSizeScale(57));
        self.backgroundColor = HexRGBALPHA(0x000000, 0.6);
    } completion:^(BOOL finished) {
        
    }];
    
}

- (UITableView *)tableView {
    if(!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, 0) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = XHLightColor;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        if (@available(iOS 11, *)) {
            //目前如此
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
        }
        if (@available(iOS 11.0, *)) {
           _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _tableView;
}

#pragma mark----- UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    SelectSortTypeListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectSortTypeListCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row < self.dataArray.count) {
        NSString *title = self.dataArray[indexPath.row];
        cell.itemNameLabel.text = title;
        if([title isEqualToString:self.selectSortTitle]) {
            cell.itemNameLabel.textColor = HexRGB(0x418CF6);
            cell.selectImageView.hidden = NO;
        } else {
            cell.itemNameLabel.textColor = HexRGB(0x333333);
            cell.selectImageView.hidden = YES;
        }
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

#pragma mark----- UITableViewDelagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(57);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  0.0001f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.0001f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   [tableView deselectRowAtIndexPath:indexPath animated:YES];
   if ([self.delegate respondsToSelector:@selector(selectSortResult:index:)]) {
       [self.delegate selectSortResult:self.dataArray[indexPath.row] index:indexPath.row];
   }
   [self removeFromSuperview];
}

@end
