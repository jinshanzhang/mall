//
//  MahListViewController.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchListViewController.h"
#import "MchListCell.h"
#import "MchShopMessageCell.h"
#import "LocationViewController.h"
#import "MchDetailViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "SelectSortTypeView.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import "MchListModel.h"

#import "CommonNoDataView.h"

@interface MchListViewController ()<UITableViewDelegate,UITableViewDataSource,SelectAddressResultDelegate,SelectSortResultDelegate,AMapLocationManagerDelegate>

@property (nonatomic, strong) UIView * navView;
@property (nonatomic, strong) UIButton * addressButton;
@property (nonatomic, strong) UIButton * sortButton;

@property (nonatomic, strong) SelectSortTypeView *selectSortTypeView;
@property (nonatomic, copy) NSString * selectSortTitle;
@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;

@property (nonatomic, copy) NSString * sortKey;

@property (nonatomic, strong) CommonNoDataView * noDataView;


@end

@implementation MchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageNum = 1;
    self.selectSortTitle = @"默认排序";
    self.sortKey = @"1";
    [self setUpNav];
    [self location];
    [self setTableView];
//    [self loadData];
}

/**
 下拉加载数据请求
 */
- (void)loadNewData {
    self.pageNum = 1;
    [self loadData];
}

/**
上拉加载数据请求
*/
- (void)loadMoreData {
    [self loadData];
}

- (void)loadData {
    if(self.pageNum == 1) {
        [YYCenterLoading showCenterLoading];
    }
    self.pageSize = 20;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if(kValidString(kUserInfo.userId)) {
        params[@"userId"] = kUserInfo.userId;
    }
    if(kValidString(kUserInfo.useToken)) {
        params[@"token"] = kUserInfo.useToken;
    }
    params[@"pageNum"] = @(self.pageNum);
    params[@"pageSize"] = @(self.pageSize);
    params[@"longitude"] = @(self.longitude);
    params[@"latitude"] = @(self.latitude);
    params[@"distance"] = @(1000000);
    if(self.sortKey) {
       params[@"sortKey"] = self.sortKey;
    } else {
        self.sortKey = @"1";
        params[@"sortKey"] = self.sortKey;
    }
    @weakify(self);
    [NetWorkHelper requestWithMethod:GET path:@"/ironman/v5/business/nearbyBusinesses" parameters:params block:^(NSString *errorNO, NSDictionary *result) {
        @strongify(self);
        [YYCenterLoading hideCenterLoading];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        NSString *code = result[@"ret"];
        if([code intValue] == 0) {
            NSDictionary *dataDict = [result objectForKey:@"data"];
            NSArray *items = [NSArray modelArrayWithClass:[MchListModel class] json:[dataDict objectForKey:@"items"]];
            NSMutableArray *marr = [NSMutableArray array];
            marr = [NSMutableArray arrayWithArray:items];
             if(self.pageNum == 1) {
                self.dataArray = marr;
             } else {
                [self.dataArray addObjectsFromArray:marr];
             }
             self.noDataView.hidden = !(self.dataArray.count == 0);
             if(marr.count < self.pageSize) {
                 [self.tableView.mj_footer endRefreshingWithNoMoreData];
             } else {
                 self.pageNum++;
             }
            self.tableView.mj_footer.hidden = (self.dataArray.count == 0);
            self.tableView.mj_footer.hidden = NO;
            [self.tableView reloadData];
        } else {
            [self.dataArray removeAllObjects];
            [self.tableView reloadData];
            self.tableView.mj_footer.hidden = YES;
            if(self.dataArray.count > 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                self.noDataView.hidden = NO;
                self.noDataView.noDataLabel.text = @"当前位置附近暂无相关店铺";
            }
        }
    }];
}

- (void)setUpNav {
    [self xh_hideNavigation:YES];
    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
    [self setNavUI];
}

- (void)setNavUI {
    @weakify(self);
    UIView *navView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:navView];
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(navView.superview);
        make.height.mas_equalTo(kNavigationH);
    }];
    self.navView = navView;
    
    UIButton *leftBtn = [YYCreateTools createBtnImage:@"detail_navbar_back"];
    [navView addSubview:leftBtn];
    leftBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    };
    
    UIView *searchBgView = [YYCreateTools createView:nil];
    [navView addSubview:searchBgView];
    [searchBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kSizeScale(9) + kStatuH);
        make.width.mas_equalTo(kSizeScale(155));
        make.height.mas_equalTo(kSizeScale(25));
        make.centerX.equalTo(navView);
    }];
    
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.equalTo(searchBgView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(36), kSizeScale(36)));
    }];
    
    UIImageView *locationImageView = [[UIImageView alloc] init];
//    locationImageView.backgroundColor = XHLightColor;
    locationImageView.image = [UIImage imageNamed:@"icon_shouye_dingwe"];
    [searchBgView addSubview:locationImageView];
    
    UIButton *addressButton = [UIButton creatButtonWithTitle:@" " textColor:XHTimeBlackColor font:boldFont(18) target:self action:@selector(changeAddressOperation)];
//    addressButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [searchBgView addSubview:addressButton];
    [addressButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBgView);
        make.centerX.equalTo(searchBgView).offset(kSizeScale(7));
//        make.right.equalTo(searchBgView);
        make.width.mas_lessThanOrEqualTo(145);
        make.height.equalTo(searchBgView);
    }];
    self.addressButton = addressButton;
    
    [locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBgView);
        make.right.equalTo(addressButton.mas_left).offset(kSizeScale(-7));
        make.width.mas_equalTo(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(12));
    }];
    
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(navView.mas_bottom);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view);
    }];
    
    UIButton *sortButton = [UIButton creatButtonWithTitle:@"默认排序" textColor:XHTimeBlackColor font:midFont(13) target:self action:@selector(sortButtonOperation)];
    [sortButton setImage:[UIImage imageNamed:@"icon_shangjia_paixu"] forState:UIControlStateNormal];
    [sortButton layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(4)];
    sortButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [navView addSubview:sortButton];
    [sortButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBgView);
        make.right.equalTo(navView).offset(-kSizeScale(12));
        make.width.mas_equalTo(kSizeScale(65));
        make.height.mas_equalTo(kSizeScale(18));
    }];
    self.sortButton = sortButton;
    
    [self.view addSubview:self.noDataView];
    [self.noDataView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(197);
        make.center.equalTo(self.tableView);
        make.left.right.equalTo(self.view);
    }];
}

//定位获取经纬度和当前的位置
- (void)location {
    if([CLLocationManager locationServicesEnabled]){
        self.locationManager = [[AMapLocationManager alloc]init];
        [self.locationManager setDelegate:self];
        //是否允许后台定位。默认为NO。只在iOS 9.0及之后起作用。设置为YES的时候必须保证 Background Modes 中的 Location updates 处于选中状态，否则会抛出异常。由于iOS系统限制，需要在定位未开始之前或定位停止之后，修改该属性的值才会有效果。
        [self.locationManager setAllowsBackgroundLocationUpdates:NO];
        //指定定位是否会被系统自动暂停。默认为NO。
        [self.locationManager setPausesLocationUpdatesAutomatically:NO];
        //设定定位的最小更新距离。单位米，默认为 kCLDistanceFilterNone，表示只要检测到设备位置发生变化就会更新位置信息
        [self.locationManager setDistanceFilter:200];
        //设定期望的定位精度。单位米，默认为 kCLLocationAccuracyBest
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        //设置定位超时时间
        self.locationManager.locationTimeout = 10;
        //设置反地理编码超时时间
        self.locationManager.reGeocodeTimeout = 10;
        //开始定位服务
        [self.locationManager setLocatingWithReGeocode:YES];
        [self.locationManager startUpdatingLocation];
    } else {
        UIAlertView *alvertView=[[UIAlertView alloc]initWithTitle:@"提示" message:@"需要开启定位服务,请到设置->隐私,打开定位服务" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alvertView show];
    }
}

- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location reGeocode:(AMapLocationReGeocode *)reGeocode {
    NSLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
    self.latitude = location.coordinate.latitude;
    self.longitude = location.coordinate.longitude;
    kUserManager.latitude = self.latitude;
    kUserManager.longitude = self.longitude;
    [self loadData];
    if (reGeocode) {
       [self.locationManager stopUpdatingLocation];
       [self.addressButton setTitle:reGeocode.POIName forState:UIControlStateNormal];
       NSLog(@"reGeocode:%@", reGeocode);
   }
}

- (void)changeAddressOperation {
    LocationViewController *locationVC = [[LocationViewController alloc] init];
    locationVC.delegate = self;
    [self.navigationController pushViewController:locationVC animated:YES];
}

- (void)selectAddressPoi:(AMapPOI *)poi {
    self.pageNum = 1;
    self.latitude = poi.location.latitude;
    self.longitude = poi.location.longitude;
    kUserManager.latitude = self.latitude;
    kUserManager.longitude = self.longitude;
    [self loadData];
    [self.addressButton setTitle:poi.name forState:UIControlStateNormal];
}

- (void)sortButtonOperation {
    if(self.selectSortTypeView) {
        [self.selectSortTypeView removeFromSuperview];
    }
    SelectSortTypeView *selectSortTypeView = [[SelectSortTypeView alloc] initWithFrame:CGRectMake(0, kNavigationH, kScreenW, kScreenH - kNavigationH)];
    selectSortTypeView.dataArray = [NSMutableArray arrayWithArray:@[@"默认排序",@"距离最近"]];
    selectSortTypeView.selectSortTitle = self.selectSortTitle;
    selectSortTypeView.delegate = self;
    [self.view addSubview:selectSortTypeView];
    [selectSortTypeView show];
    self.selectSortTypeView = selectSortTypeView;
}

- (void)selectSortResult:(NSString *)selectSortTitle index:(NSInteger)index {
    self.selectSortTitle = selectSortTitle;
    self.sortKey = [NSString stringWithFormat:@"%ld",(long)index + 1];
    [self loadData];
    [self.sortButton setTitle:selectSortTitle forState:UIControlStateNormal];
}

- (void)setTableView {
    self.tableView.separatorColor = [UIColor whiteColor];
    self.needPullDownRefresh = self.needPullUpRefresh = YES;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[MchListCell class] forCellReuseIdentifier:@"MchListCell"];
    [self.tableView registerClass:[MchShopMessageCell class] forCellReuseIdentifier:@"MchShopMessageCell"];
    [self.tableView reloadData];
}

#pragma mark----- UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if(indexPath.row == 0) {
        MchShopMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[MchShopMessageCell description]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(indexPath.section < self.dataArray.count) {
            MchListModel *model = self.dataArray[indexPath.section];
            cell.model = model;
        }
        return cell;
    }
    MchListCell *cell = [tableView dequeueReusableCellWithIdentifier:[MchListCell description]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.section < self.dataArray.count) {
        MchListModel *model = self.dataArray[indexPath.section];
        cell.model = model.goodsItems[indexPath.row - 1];
    }
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    MchListModel *model = self.dataArray[section];
    return model.goodsItems.count + 1;
}

#pragma mark----- UITableViewDelagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(74);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        UIView *headerView = [[UIView alloc] init];
        return headerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return kSizeScale(16);
    }
    return  0.0001f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *lineView = [[UIView alloc] init];
    return lineView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  kSizeScale(8);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MchDetailViewController *detailVC = [[MchDetailViewController alloc] init];
    detailVC.hidesBottomBarWhenPushed = YES;
    if(indexPath.section < self.dataArray.count) {
        MchListModel *model = self.dataArray[indexPath.section];
//        detailVC.model = model;
        detailVC.sellerId = model.sellerId;
        detailVC.userId = [NSString stringWithFormat:@"%ld",model.userId];
    }
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(tintColor)]) {
        if (tableView == self.tableView) {
            // 圆角弧度半径
            CGFloat cornerRadius = kSizeScale(12);
            // 设置cell的背景色为透明，如果不设置这个的话，则原来的背景色不会被覆盖
            cell.backgroundColor = UIColor.clearColor;
            
            // 创建一个shapeLayer
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CAShapeLayer *backgroundLayer = [[CAShapeLayer alloc] init]; //显示选中
            // 创建一个可变的图像Path句柄，该路径用于保存绘图信息
            CGMutablePathRef pathRef = CGPathCreateMutable();
            // 获取cell的size
            CGRect bounds = CGRectInset(cell.bounds, 0, 0);
            
            // CGRectGetMinY：返回对象顶点坐标
            // CGRectGetMaxY：返回对象底点坐标
            // CGRectGetMinX：返回对象左边缘坐标
            // CGRectGetMaxX：返回对象右边缘坐标
            
            // 这里要判断分组列表中的第一行，每组section的第一行，每组section的中间行
            BOOL addLine = NO;
            // CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            if (indexPath.row == 0) {
                // 初始起点为cell的左下角坐标
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                // 起始坐标为左下角，设为p1，（CGRectGetMinX(bounds), CGRectGetMinY(bounds)）为左上角的点，设为p1(x1,y1)，(CGRectGetMidX(bounds), CGRectGetMinY(bounds))为顶部中点的点，设为p2(x2,y2)。然后连接p1和p2为一条直线l1，连接初始点p到p1成一条直线l，则在两条直线相交处绘制弧度为r的圆角。
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                // 终点坐标为右下角坐标点，把绘图信息都放到路径中去,根据这些路径就构成了一块区域了
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                // 初始起点为cell的左上角坐标
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                // 添加一条直线，终点坐标为右下角坐标点并放到路径中去
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                // 添加cell的rectangle信息到path中（不包括圆角）
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            // 把已经绘制好的可变图像路径赋值给图层，然后图层根据这图像path进行图像渲染render
            layer.path = pathRef;
            backgroundLayer.path = pathRef;
            // 注意：但凡通过Quartz2D中带有creat/copy/retain方法创建出来的值都必须要释放
            CFRelease(pathRef);
            // 按照shape layer的path填充颜色，类似于渲染render
            // layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            layer.fillColor = [UIColor whiteColor].CGColor;
            // 添加分隔线图层
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                // 分隔线颜色取自于原来tableview的分隔线颜色
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            
            // view大小与cell一致
            UIView *roundView = [[UIView alloc] initWithFrame:bounds];
            // 添加自定义圆角后的图层到roundView中
            [roundView.layer insertSublayer:layer atIndex:0];
            roundView.backgroundColor = UIColor.clearColor;
            //cell的背景view
            //cell.selectedBackgroundView = roundView;
            cell.backgroundView = roundView;
            
            //以上方法存在缺陷当点击cell时还是出现cell方形效果，因此还需要添加以下方法
            UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            backgroundLayer.fillColor = tableView.separatorColor.CGColor;
            [selectedBackgroundView.layer insertSublayer:backgroundLayer atIndex:0];
            selectedBackgroundView.backgroundColor = UIColor.clearColor;
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

- (CommonNoDataView *)noDataView {
    if(!_noDataView) {
        _noDataView = [[CommonNoDataView alloc] initWithFrame:CGRectMake(0, kNavigationH, kScreenW, kScreenH - kNavigationH - kTabbarH)];
//        _noDataView.backgroundColor = [UIColor redColor];
        _noDataView.hidden = YES;
        @weakify(self);
        _noDataView.refreshBlock = ^{
            @strongify(self);
            [self loadNewData];
        };
    }
    return _noDataView;
}

@end
