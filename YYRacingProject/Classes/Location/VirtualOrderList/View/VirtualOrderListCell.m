//
//  VirtualOrderListCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/6.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderListCell.h"
#import "MchDetailViewController.h"

@interface VirtualOrderListCell()

@property (nonatomic, strong) UIView * whiteBgView;
@property (nonatomic, strong) UIImageView * shopImgView;
@property (nonatomic, strong) UIButton * shopNameBtn;
@property (nonatomic, strong) UILabel * orderStateLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIImageView * productImageView;
@property (nonatomic, strong) UILabel * productNameLabel;
@property (nonatomic, strong) UILabel * totalPriceLabel;
@property (nonatomic, strong) UIButton * payBtn;
@property (nonatomic, strong) UIButton * lookCodeBtn;
@property (nonatomic, strong) UIButton * deleteOrderBtn;


@property (nonatomic, assign) NSInteger orderStatus;

@end

@implementation VirtualOrderListCell

/*
 全部:根据订单状态显示对应订单操作

 待付款:删除订单,去支付

 待核销:去核销,退款

 已核销:删除订单

 已过期:删除订单,退款
 **/

- (void)js_createSubViews {
    self.contentView.backgroundColor = XHLightColor;
    [self.contentView addSubview:self.whiteBgView];
    [self.whiteBgView addSubview:self.shopImgView];
    [self.whiteBgView addSubview:self.shopNameBtn];
    [self.whiteBgView addSubview:self.orderStateLabel];
    [self.whiteBgView addSubview:self.lineView];
    
    [self.whiteBgView addSubview:self.productImageView];
    [self.whiteBgView addSubview:self.productNameLabel];
    [self.whiteBgView addSubview:self.totalPriceLabel];
    [self.whiteBgView addSubview:self.lookCodeBtn];
    [self.whiteBgView addSubview:self.deleteOrderBtn];
}

- (void)js_layoutSubViews {
    [self.whiteBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(kSizeScale(12));
        make.right.equalTo(self.contentView).offset(-kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(188.0));
    }];
    
    [self.shopImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.whiteBgView).offset(kSizeScale(12));
        make.width.height.mas_equalTo(kSizeScale(24));
    }];
    
    [self.shopNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.shopImgView);
        make.left.equalTo(self.shopImgView.mas_right).offset(kSizeScale(6));
        make.width.mas_lessThanOrEqualTo(kSizeScale(230));
        make.height.mas_equalTo(kSizeScale(22));
    }];
    
    [self.orderStateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.shopImgView);
        make.right.equalTo(self.whiteBgView).offset(-kSizeScale(12));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopImgView);
        make.height.mas_equalTo(kSizeScale(1.0));
        make.top.equalTo(self.shopImgView.mas_bottom).offset(kSizeScale(9.0));
        make.right.equalTo(self.whiteBgView).offset(-kSizeScale(12.0));
    }];
    
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopImgView);
        make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(18));
        make.width.height.mas_equalTo(kSizeScale(64.0));
    }];
    
    [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView);
        make.left.equalTo(self.productImageView.mas_right).offset(kSizeScale(8));
        make.right.equalTo(self.whiteBgView).offset(-kSizeScale(12.0));
    }];
    
    [self.totalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productNameLabel.mas_bottom).offset(kSizeScale(9));
        make.right.equalTo(self.productNameLabel);
    }];
    
    [self.lookCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
        make.right.equalTo(self.productNameLabel);
        make.width.mas_equalTo(kSizeScale(99.0));
        make.height.mas_equalTo(kSizeScale(32.0));
    }];
    
    [self.deleteOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.lookCodeBtn);
        make.right.equalTo(self.lookCodeBtn.mas_left).offset(-kSizeScale(12));
        make.width.mas_equalTo(kSizeScale(90.0));
        make.height.mas_equalTo(kSizeScale(32.0));
    }];
}


//去支付
- (void)payOperation {
    if(self.btnOperationBlock) {
        self.btnOperationBlock(YYOrderOperatorPay);
    }
}

//删除订单
- (void)deleteOrder {
    if(self.btnOperationBlock) {
        self.btnOperationBlock(YYOrderOperatorDelete);
    }
}

//退款
- (void)refundOperation {
   if(self.btnOperationBlock) {
        self.btnOperationBlock(YYOrderOperatorRefund);
    }
}

//去核销
- (void)toConfirmSold {
    //进入详情
    if(self.btnOperationBlock) {
        self.btnOperationBlock(YYOrderOperatorDetail);
    }
}

//查看提货码
- (void)lookCodeBtnOperation {
    //进入详情
    if(self.btnOperationBlock) {
        self.btnOperationBlock(YYOrderOperatorDetail);
    }
}

- (void)cancelOrderOperation {
    if(self.btnOperationBlock) {
        self.btnOperationBlock(YYOrderOperatorCancel);
    }
}

- (UIView *)whiteBgView {
    if(!_whiteBgView) {
        _whiteBgView = [[UIView alloc] init];
        _whiteBgView.backgroundColor = [UIColor whiteColor];
        _whiteBgView.layer.cornerRadius = kSizeScale(10.0);
        _whiteBgView.clipsToBounds = YES;
    }
    return _whiteBgView;
}

- (UIImageView *)shopImgView {
    if(!_shopImgView) {
        _shopImgView = [[UIImageView alloc] init];
        _shopImgView.backgroundColor = XHLightColor;
        [_shopImgView setImage:[UIImage imageNamed:@"icon_liebiao_dianpu"]];
    }
    return _shopImgView;
}

- (UIButton *)shopNameBtn {
    if(!_shopNameBtn) {
        _shopNameBtn = [UIButton creatButtonWithTitle:@"--" textColor:XHTimeBlackColor font:boldFont(16) target:self action:@selector(shopNameBtnOperation)];
        [_shopNameBtn setImage:[UIImage imageNamed:@"icon_liebiao_taiozhuan"] forState:UIControlStateNormal];
        _shopNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_shopNameBtn layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(7.0)];
    }
    return _shopNameBtn;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}

- (void)shopNameBtnOperation {
    MchDetailViewController *detailVC = [[MchDetailViewController alloc] init];
    detailVC.hidesBottomBarWhenPushed = YES;
    detailVC.sellerId = self.model.sellerId;
    detailVC.userId = [NSString stringWithFormat:@"%@",self.model.sellerUserId];
    [[YYCommonTools getCurrentVC].navigationController pushViewController:detailVC animated:YES];
}

- (UILabel *)orderStateLabel {
    if(!_orderStateLabel) {
        _orderStateLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentRight font:midFont(14)];
        _orderStateLabel.numberOfLines = 1;
    }
    return _orderStateLabel;
}

- (UIImageView *)productImageView {
    if(!_productImageView) {
        _productImageView = [[UIImageView alloc] init];
        _productImageView.backgroundColor = XHLightColor;
        _productImageView.layer.cornerRadius = kSizeScale(8.0);
        _productImageView.clipsToBounds = YES;
//        _productImageView.backgroundColor = [UIColor redColor];
    }
    return _productImageView;
}

- (UILabel *)productNameLabel {
    if(!_productNameLabel) {
        _productNameLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(13)];
        _productNameLabel.numberOfLines = 2;
    }
    return _productNameLabel;
}

- (UILabel *)totalPriceLabel {
    if(!_totalPriceLabel) {
        _totalPriceLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentRight font:boldFont(14)];
        _totalPriceLabel.numberOfLines = 1;
    }
    return _totalPriceLabel;
}

- (UIButton *)lookCodeBtn {
    if(!_lookCodeBtn) {
        _lookCodeBtn = [UIButton creatButtonWithTitle:@"查看提货码" textColor:XHTimeBlackColor font:boldFont(14) target:self action:nil];
        _lookCodeBtn.clipsToBounds = YES;
        _lookCodeBtn.layer.cornerRadius = kSizeScale(16.0);
        _lookCodeBtn.layer.borderColor = [HexRGB(0xE0E0E0) CGColor];
        _lookCodeBtn.layer.borderWidth = kSizeScale(0.5);
    }
    return _lookCodeBtn;
}

- (UIButton *)deleteOrderBtn {
    if(!_deleteOrderBtn) {
        _deleteOrderBtn = [UIButton creatButtonWithTitle:@"删除订单" textColor:XHTimeBlackColor font:boldFont(14) target:self action:nil];
        _deleteOrderBtn.clipsToBounds = YES;
        _deleteOrderBtn.layer.cornerRadius = kSizeScale(16.0);
        _deleteOrderBtn.layer.borderColor = [HexRGB(0xE0E0E0) CGColor];
        _deleteOrderBtn.layer.borderWidth = kSizeScale(0.5);
    }
    return _deleteOrderBtn;
}


- (void)setModel:(VirtualOrderListModel *)model {
    _model = model;
    [self.shopNameBtn setTitle:model.shopName forState:UIControlStateNormal];
    [self.shopNameBtn layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(7.0)];
    if(kValidString(model.shopLogo)) {
        [self.productImageView sd_setImageWithURL:[NSURL URLWithString:model.shopLogo]];
    }
    NSMutableString *skuString = [NSMutableString string];
    for (int i = 0;i < model.skuList.count;i++) {
        SkuList *skuModel = model.skuList[i];
        if(i == 0) {
            [skuString appendString:[NSString stringWithFormat:@"%@*%ld",skuModel.skuTitle,(long)skuModel.skuCnt]];
        } else {
            [skuString appendString:[NSString stringWithFormat:@"%@%@%ld",@",",skuModel.skuTitle,(long)skuModel.skuCnt]];
        }
    }
    self.productNameLabel.text = skuString;
    __block NSString *totalString = @"";
    [model.summaryList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         SummaryList *priceModel = (SummaryList *)obj;
        if (priceModel.type == 4) {
            totalString = priceModel.priceTag;
        }
    }];
    
    self.totalPriceLabel.text = [NSString stringWithFormat:@"总价 ¥%@",totalString];
//    subOrderStatus - 2-待付款；3-支付中；5-支付成功；6-可发货（待发货）；7-待收货；8-确认收货（已完成）；9-已完结（用户可结算）；10-用户已结算；20-商户已结算
    if(model.orderStatus == 2 || model.orderStatus == 3) {
        // 待付款:删除订单,去支付
        self.lookCodeBtn.hidden = NO;
        self.deleteOrderBtn.hidden = NO;
        
        self.orderStateLabel.text = @"待支付";
        self.orderStateLabel.textColor = XHBlackLitColor;
        [self.lookCodeBtn removeAllTargets];
        [self.lookCodeBtn setTitle:@"去支付" forState:UIControlStateNormal];
        [self.lookCodeBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
            make.right.equalTo(self.productNameLabel);
            make.width.mas_equalTo(kSizeScale(80.0));
            make.height.mas_equalTo(kSizeScale(32.0));
        }];
        [self.lookCodeBtn addTarget:self action:@selector(payOperation) forControlEvents:UIControlEventTouchUpInside];
        
        [self.deleteOrderBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        [self.deleteOrderBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.lookCodeBtn);
            make.right.equalTo(self.lookCodeBtn.mas_left).offset(-kSizeScale(12));
            make.width.mas_equalTo(kSizeScale(90.0));
            make.height.mas_equalTo(kSizeScale(32.0));
        }];
        [self.deleteOrderBtn removeAllTargets];
        [self.deleteOrderBtn addTarget:self action:@selector(deleteOrder) forControlEvents:UIControlEventTouchUpInside];
    } else if(model.orderStatus == 6 || model.orderStatus == 7) {
        // 待核销
        self.lookCodeBtn.hidden = NO;
        self.deleteOrderBtn.hidden = YES;
        
        self.orderStateLabel.text = @"待核销";
        self.orderStateLabel.textColor = HexRGB(0xFF583D);
    
        [self.lookCodeBtn setTitle:@"去核销" forState:UIControlStateNormal];
        [self.lookCodeBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
            make.right.equalTo(self.productNameLabel);
            make.width.mas_equalTo(kSizeScale(80.0));
            make.height.mas_equalTo(kSizeScale(32.0));
        }];
        [self.lookCodeBtn removeAllTargets];
        [self.deleteOrderBtn removeAllTargets];
        [self.lookCodeBtn addTarget:self action:@selector(toConfirmSold) forControlEvents:UIControlEventTouchUpInside];
//        [self.lookCodeBtn setTitle:@"取消订单" forState:UIControlStateNormal];
//        [self.lookCodeBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
//            make.right.equalTo(self.productNameLabel);
//            make.width.mas_equalTo(kSizeScale(85.0));
//            make.height.mas_equalTo(kSizeScale(32.0));
//        }];
//        [self.lookCodeBtn addTarget:self action:@selector(cancelOrderOperation) forControlEvents:UIControlEventTouchUpInside];
//
//        [self.deleteOrderBtn setTitle:@"去核销" forState:UIControlStateNormal];
//        [self.deleteOrderBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(self.lookCodeBtn);
//            make.right.equalTo(self.lookCodeBtn.mas_left).offset(-kSizeScale(12));
//            make.width.mas_equalTo(kSizeScale(80.0));
//            make.height.mas_equalTo(kSizeScale(32.0));
//        }];
//        [self.deleteOrderBtn addTarget:self action:@selector(toConfirmSold) forControlEvents:UIControlEventTouchUpInside];
    } else if(model.orderStatus == 8) {
        // 已核销:删除订单
        self.lookCodeBtn.hidden = NO;
        self.deleteOrderBtn.hidden = YES;
        
        self.orderStateLabel.text = @"已核销";
        self.orderStateLabel.textColor = XHBlackLitColor;
        [self.lookCodeBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        [self.lookCodeBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
            make.right.equalTo(self.productNameLabel);
            make.width.mas_equalTo(kSizeScale(90.0));
            make.height.mas_equalTo(kSizeScale(32.0));
        }];
        [self.lookCodeBtn removeAllTargets];
        [self.deleteOrderBtn removeAllTargets];
        [self.lookCodeBtn addTarget:self action:@selector(deleteOrder) forControlEvents:UIControlEventTouchUpInside];
    } else if(model.orderStatus == 20) {
        //已过期  删除订单,退款
//        self.lookCodeBtn.hidden = NO;
//        self.deleteOrderBtn.hidden = NO;
//        self.orderStateLabel.text = @"已过期";
//        self.orderStateLabel.textColor = XHBlackLitColor;
//        [self.lookCodeBtn setTitle:@"退款" forState:UIControlStateNormal];
//        [self.lookCodeBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
//            make.right.equalTo(self.productNameLabel);
//            make.width.mas_equalTo(kSizeScale(70.0));
//            make.height.mas_equalTo(kSizeScale(32.0));
//        }];
//        [self.lookCodeBtn addTarget:self action:@selector(refundOperation) forControlEvents:UIControlEventTouchUpInside];
//
//        [self.deleteOrderBtn setTitle:@"删除订单" forState:UIControlStateNormal];
//        [self.deleteOrderBtn mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.centerY.equalTo(self.lookCodeBtn);
//            make.right.equalTo(self.lookCodeBtn.mas_left).offset(-kSizeScale(12));
//            make.width.mas_equalTo(kSizeScale(90.0));
//            make.height.mas_equalTo(kSizeScale(32.0));
//        }];
//        [self.deleteOrderBtn addTarget:self action:@selector(deleteOrder) forControlEvents:UIControlEventTouchUpInside];
        
        self.lookCodeBtn.hidden = NO;
        self.deleteOrderBtn.hidden = YES;
        
        self.orderStateLabel.text = @"已过期";
        self.orderStateLabel.textColor = XHBlackLitColor;
        [self.lookCodeBtn setTitle:@"删除订单" forState:UIControlStateNormal];
        [self.lookCodeBtn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.productImageView.mas_bottom).offset(kSizeScale(10));
            make.right.equalTo(self.productNameLabel);
            make.width.mas_equalTo(kSizeScale(90.0));
            make.height.mas_equalTo(kSizeScale(32.0));
        }];
        [self.lookCodeBtn removeAllTargets];
        [self.deleteOrderBtn removeAllTargets];
        [self.lookCodeBtn addTarget:self action:@selector(deleteOrder) forControlEvents:UIControlEventTouchUpInside];
    }
}

@end
