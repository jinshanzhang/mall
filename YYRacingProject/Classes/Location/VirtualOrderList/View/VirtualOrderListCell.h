//
//  VirtualOrderListCell.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/6.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "VirtualOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VirtualOrderListCell : BaseTableViewCell

@property (nonatomic, copy) void (^btnOperationBlock)(YYOrderOperatorType type);

@property (nonatomic, strong) VirtualOrderListModel * model;

@end

NS_ASSUME_NONNULL_END
