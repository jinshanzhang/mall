//
//  VirtualOrderListViewController.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/4.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderListViewController.h"

#import "VirtualOrderListCell.h"
#import "VirtualOrderListModel.h"

#import "VirtualOrderDetailViewController.h"

@interface VirtualOrderListViewController ()<UITableViewDelegate,UITableViewDataSource>


@end

@implementation VirtualOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    [self setTableView];
    [self loadData];
}

- (void)setUpNav {
    [self xh_hideNavigation:YES];
//    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
}

- (void)setTableView {
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[VirtualOrderListCell class] forCellReuseIdentifier:@"VirtualOrderListCell"];
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view);
    }];
    self.needPullUpRefresh = YES;
    self.needPullDownRefresh = YES;
}

- (void)loadNewData {
    self.pageNum = 1;
    [self loadData];
}

- (void)loadMoreData {
    [self loadData];
}

- (void)loadData {
    self.pageSize = 20;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"pageNum"] = @(self.pageNum);
    params[@"pageSize"] = @(self.pageSize);
    @weakify(self);
    [NetWorkHelper requestWithMethod:GET path:@"/ironman/v3/buyer/selfPickOrders" parameters:params block:^(NSString *errorNO, NSDictionary *result) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        NSString *code = result[@"ret"];
        NSDictionary *dataDict = result[@"data"];
        if([code intValue] == 0) {
            NSDictionary *dataDict = [result objectForKey:@"data"];
            NSArray *items = [NSArray modelArrayWithClass:[VirtualOrderListModel class] json:[dataDict objectForKey:@"orderList"]];
            NSMutableArray *marr = [NSMutableArray array];
            marr = [NSMutableArray arrayWithArray:items];
             if(self.pageNum == 1) {
                self.dataArray = marr;
             } else {
                [self.dataArray addObjectsFromArray:marr];
             }
            if(self.dataArray.count == 0) {
                 self.tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewOrderNoDataType title:@"您目前没有相关订单"
                 target:self
                 action:@selector(loadNewData)];
//                 self.tableView.ly_emptyView.contentViewY = kSizeScale(143+270);
             }
             if(marr.count < self.pageSize) {
                 [self.tableView.mj_footer endRefreshingWithNoMoreData];
             } else {
                 self.pageNum++;
             }
            self.tableView.mj_footer.hidden = (self.dataArray.count == 0);
            [self.tableView reloadData];
        } else {
            if(self.dataArray.count > 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        NSLog(@"获取到的请求的数据是:%@",result);
    }];
}

/**订单操作处理**/
- (void)orderClickOperator:(YYOrderOperatorType)type
                 listModel:(VirtualOrderListModel *)listModel{
    if (type == YYOrderOperatorCancel) {
        //取消订单
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"取消后将不能恢复，确认取消订单吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
            
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig cancelOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadNewData];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorConfirm) {
        //确认收货
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"确认收货吗？收到商品后再确认收货，以免造成损失。" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig confirmOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadNewData];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorDelete) {
        //删除订单
        [JCAlertView showTwoButtonsWithTitle:@"提示" Message:@"删除后订单不能恢复，确认删除该订单吗？" ButtonType:JCAlertViewButtonTypeCancel cancelColor:XHBlueColor ButtonTitle:@"取消" Click:^{
        } ButtonType:JCAlertViewButtonTypeDefault confirmColor:XHBlueColor ButtonTitle:@"确认" Click:^{
            [kWholeConfig deleteOrderRequest:[NSString stringWithFormat:@"%@", listModel.orderId] isStore:(NO) success:^(BOOL isSuccess) {
                if (isSuccess) {
                    [UIView performWithoutAnimation:^{
                        [self.m_tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                    }];
                    [self loadNewData];
                }
            }];
        } type:JCAlertViewTypeDefault];
    }
    else if (type == YYOrderOperatorLookLogistic) {
        //查看物流
        [YYCommonTools skipLogisticsPage:self params:@{@"orderID": listModel.orderId}];
    }
    else if (type == YYOrderOperatorPay) {
        //去支付
        [YYCommonTools skipOrderPay:self
                            orderID:[listModel.orderId longValue]
                        canBackRoot:NO];
    }
//    else if(type == YYOrderOperatorRefund) {
//        [YYCommonTools skipAfterSaleChoose:self
//        orderID:[listModel.orderId longValue]
//          skuID:[listModel.skuId intValue]];
//    }
}

#pragma mark----- UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    VirtualOrderListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VirtualOrderListCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row < self.dataArray.count) {
        VirtualOrderListModel *model = self.dataArray[indexPath.row];
        cell.model = model;
        @weakify(self);
        cell.btnOperationBlock = ^(YYOrderOperatorType type) {
            @strongify(self);
            if(type == YYOrderOperatorDetail) {
                [self toDetailVCWithModel:model];
            } else {
                [self orderClickOperator:type listModel:model];
            }
        };
    }
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

#pragma mark----- UITableViewDelagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(200.0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  0.00001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.00001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self toDetailVCWithModel:self.dataArray[indexPath.row]];
}


- (void)toDetailVCWithModel:(VirtualOrderListModel *)model {
    VirtualOrderDetailViewController *detailVC = [[VirtualOrderDetailViewController alloc] init];
    detailVC.model = model;
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end
