//
//  VirtualOrderListModel.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/8.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderListModel.h"

@implementation SummaryList

@end
@implementation Price

@end
@implementation SkuList

@end
@implementation SumList

@end

@implementation VirtualOrderListModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"summaryList":[SummaryList class],
             @"skuList":[SkuList class],
             @"sumList":[SumList class]
    };
}

@end
