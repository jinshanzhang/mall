//
//  VirtualOrderListModel.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/8.
//  Copyright © 2020 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SummaryList :NSObject
@property (nonatomic , copy) NSString              * color;
@property (nonatomic , assign) NSInteger              price;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * value;
@property (nonatomic , assign) NSInteger              type;
@property (nonatomic , copy) NSString              * priceTag;
@property (nonatomic , assign) NSInteger              bold;

@end

@interface Price :NSObject
@property (nonatomic , assign) NSInteger              price;
@property (nonatomic , copy) NSString              * priceTag;

@end

@interface SkuList :NSObject

@property (nonatomic , assign) NSInteger              skuCnt;
@property (nonatomic , copy) NSString              * skuCover;
@property (nonatomic , assign) NSInteger              itemType;
@property (nonatomic , copy) NSString              * skuProp;
@property (nonatomic , assign) NSInteger              operation;
@property (nonatomic , assign) NSInteger              skuId;
@property (nonatomic , strong) Price              * price;
@property (nonatomic , assign) NSInteger              earnFlag;
@property (nonatomic , assign) NSInteger              spuId;
@property (nonatomic , assign) NSInteger              orderId;
@property (nonatomic , copy) NSString              * skuTitle;
@property (nonatomic , copy) NSString              * commission;
@property (nonatomic , copy) NSString              * itemUri;

@end

@interface SumList :NSObject

@end

@interface VirtualOrderListModel :NSObject
@property (nonatomic, copy) NSString * sellerUserId;
@property (nonatomic, copy) NSString * sellerId;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , copy) NSString              * payTime;
@property (nonatomic , assign) NSInteger              orderStatus;
@property (nonatomic , copy) NSString              * province;
@property (nonatomic , copy) NSString              * shipDeadlineTime;
@property (nonatomic , copy) NSString              * district;
@property (nonatomic , strong) NSArray<SummaryList *>              * summaryList;
@property (nonatomic , strong) NSArray<SkuList *>              * skuList;
@property (nonatomic , copy) NSString              * saleTime;
@property (nonatomic , assign) NSInteger              shopId;
@property (nonatomic , copy) NSString              * sort;
@property (nonatomic , copy) NSString              * shopName;
@property (nonatomic , copy) NSString              * street;
@property (nonatomic , assign) NSInteger              operation;
@property (nonatomic , copy) NSString              * city;
@property (nonatomic , copy) NSString              * sellerAddr;
@property (nonatomic , assign) NSInteger              earnFlag;
@property (nonatomic , assign) CGFloat              latitude;
@property (nonatomic , copy) NSString              * pickNumber;
@property (nonatomic , copy) NSString              * deliveryType;
@property (nonatomic , copy) NSString              * shopLogo;
@property (nonatomic , assign) CGFloat              longitude;
@property (nonatomic , copy) NSString              * phone;
@property (nonatomic , copy) NSString              * describe;
@property (nonatomic , assign) NSInteger              orderTime;
@property (nonatomic , assign) NSInteger              payDeadlineTime;
@property (nonatomic , strong) NSArray<SumList *>              * sumList;
@property (nonatomic , copy) NSString              * address;
@property (nonatomic , copy) NSString              * orderId;

@end

NS_ASSUME_NONNULL_END
