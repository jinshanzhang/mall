//
//  MchDetailListCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchDetailListCell.h"

@interface MchDetailListCell()


@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIView * bottomLineView;

@end

@implementation MchDetailListCell

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.productImageView];
    [self addSubview:self.productNameLabel];
    [self addSubview:self.saleCountLabel];
    [self addSubview:self.priceLabel];
    [self addSubview:self.originPriceLabel];
    [self addSubview:self.lineView];
    [self addSubview:self.bottomLineView];
}

- (void)js_layoutSubViews {
   [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(12));
        make.centerY.equalTo(self);
        make.width.height.mas_equalTo(kSizeScale(86));
    }];
    
    [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView).offset(kSizeScale(0));
        make.left.equalTo(self.productImageView.mas_right).offset(kSizeScale(10));
        make.right.equalTo(self).offset(-kSizeScale(83));
    }];
    
    [self.saleCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.productNameLabel);
        make.right.equalTo(self).offset(-kSizeScale(10));
    }];
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.productNameLabel);
        make.bottom.equalTo(self.productImageView).offset(-kSizeScale(4));
    }];
    
    [self.originPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLabel.mas_right).offset(kSizeScale(5));
        make.centerY.equalTo(self.priceLabel);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.centerY.equalTo(self.originPriceLabel);
        make.height.mas_equalTo(kSizeScale(0.5));
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.productNameLabel);
        make.right.bottom.equalTo(self);
        make.size.mas_equalTo(kSizeScale(0.5));
    }];
}

- (UIImageView *)productImageView {
    if(!_productImageView) {
        _productImageView = [[UIImageView alloc] init];
        _productImageView.backgroundColor = XHLightColor;
        _productImageView.layer.cornerRadius = kSizeScale(8.0);
        _productImageView.clipsToBounds = YES;
    }
    return _productImageView;
}

- (UILabel *)productNameLabel {
    if(!_productNameLabel) {
        _productNameLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(16)];
        _productNameLabel.numberOfLines = 2;
    }
    return _productNameLabel;
}

- (UILabel *)priceLabel {
    if(!_priceLabel) {
        _priceLabel = [UILabel creatLabelWithTitle:@"--" textColor:HexRGB(0xFF583D) textAlignment:NSTextAlignmentLeft font:boldFont(16)];
    }
    return _priceLabel;
}

- (UILabel *)originPriceLabel {
    if(!_originPriceLabel) {
        _originPriceLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentLeft font:boldFont(13)];
    }
    return _originPriceLabel;
}

- (UILabel *)saleCountLabel {
    if(!_saleCountLabel) {
        _saleCountLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentRight font:midFont(12)];
    }
    return _saleCountLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = XHBlackLitColor;
    }
    return _lineView;
}

- (UIView *)bottomLineView {
    if(!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _bottomLineView;
}

- (void)setModel:(FictitiousGoodsVoList *)model {
    _model = model;
    if(model) {
        if(kValidString(model.imageUrl)) {
            [self.productImageView sd_setImageWithURL:[NSURL URLWithString:model.imageUrl]];
        }
        if(kValidString(model.reservePrice)) {
            self.priceLabel.text = [NSString stringWithFormat:@"¥%.2f",model.reservePrice.floatValue];
        }
        if(kValidString(model.originPrice) && model.originPrice.floatValue > 0) {
            self.originPriceLabel.hidden = NO;
            self.lineView.hidden = NO;
            self.originPriceLabel.text = [NSString stringWithFormat:@"¥%.2f",model.originPrice.floatValue];
        } else {
            self.originPriceLabel.hidden = YES;
            self.lineView.hidden = YES;
        }
        self.productNameLabel.text = model.title;
        self.saleCountLabel.text = [NSString stringWithFormat:@"已售%ld份",(long)model.salesVolume.intValue];
    }
}

@end
