//
//  MchDetailListCell.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MchListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MchDetailListCell : BaseTableViewCell

@property (nonatomic, strong) UIImageView * productImageView;
@property (nonatomic, strong) UILabel * productNameLabel;
@property (nonatomic, strong) UILabel * priceLabel;
@property (nonatomic, strong) UILabel * originPriceLabel;
@property (nonatomic, strong) UILabel * saleCountLabel;

@property (nonatomic, strong) FictitiousGoodsVoList * model;

@end

NS_ASSUME_NONNULL_END
