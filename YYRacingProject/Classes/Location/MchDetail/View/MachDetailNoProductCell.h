//
//  MachDetailNoProductCell.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/23.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MachDetailNoProductCell : BaseTableViewCell

@property (nonatomic, strong) UIImageView * noDataImageView;

@property (nonatomic, strong) UILabel * noDataLabel;

@property (nonatomic, strong) UIButton * noDataButton;

@property (nonatomic, copy) void (^refreshBlock) (void);


@end

NS_ASSUME_NONNULL_END
