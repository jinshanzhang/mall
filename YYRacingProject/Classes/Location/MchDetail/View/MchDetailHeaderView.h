//
//  MchDetailHeaderView.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseView.h"
#import "MchListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MchDetailHeaderView : BaseView

@property (nonatomic, strong) MchListModel * model;

@end

NS_ASSUME_NONNULL_END
