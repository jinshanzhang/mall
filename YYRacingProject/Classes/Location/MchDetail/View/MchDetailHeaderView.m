
//
//  MchDetailHeaderView.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchDetailHeaderView.h"
#import "JXMapNavigationView.h"

@interface MchDetailHeaderView()

@property (nonatomic, strong) UIImageView * productImageView;

@property (nonatomic, strong) UIView * backView;
@property (nonatomic, strong) UILabel * shopNameLabel;
@property (nonatomic, strong) UILabel * shopTypeLabel;
@property (nonatomic, strong) UILabel * shopOpenTimeLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIButton * phoneButton;
@property (nonatomic, strong) UIButton * locationButton;
@property (nonatomic, strong) UIImageView * locationImageView;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UILabel * roundLabel;

@property (nonatomic, strong)JXMapNavigationView *mapNavigationView;

@end

@implementation MchDetailHeaderView

- (void)js_createSubViews {
    [self addSubview:self.productImageView];
    [self addSubview:self.backView];
    [self.backView addSubview:self.shopNameLabel];
    [self.backView addSubview:self.shopTypeLabel];
    [self.backView addSubview:self.shopOpenTimeLabel];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.locationImageView];
    [self.backView addSubview:self.addressLabel];
    [self.backView addSubview:self.locationButton];
    [self.backView addSubview:self.phoneButton];
    [self.backView addSubview:self.roundLabel];
}

- (void)js_layoutSubViews {
    [self.productImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.mas_equalTo(kSizeScale(278));
    }];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.productImageView.mas_bottom).offset(-kSizeScale(76));
        make.left.equalTo(self).offset(kSizeScale(16));
        make.right.equalTo(self).offset(-kSizeScale(16));
        make.height.mas_equalTo(kSizeScale(207));
    }];
    
    [self.shopNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.backView).offset(kSizeScale(16));
       make.left.equalTo(self.backView).offset(kSizeScale(14));
       make.right.equalTo(self.backView).offset(-kSizeScale(14));
       make.height.mas_equalTo(kSizeScale(28));
    }];
    
    [self.shopTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.shopNameLabel.mas_bottom).offset(kSizeScale(14));
       make.left.equalTo(self.backView).offset(kSizeScale(14));
       make.right.equalTo(self.backView).offset(-kSizeScale(14));
       make.height.mas_equalTo(kSizeScale(20));
    }];
    
    [self.shopOpenTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.shopTypeLabel.mas_bottom).offset(kSizeScale(14));
       make.left.equalTo(self.backView).offset(kSizeScale(14));
       make.right.equalTo(self.backView).offset(-kSizeScale(14));
       make.height.mas_equalTo(kSizeScale(20));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopNameLabel);
        make.top.equalTo(self.shopOpenTimeLabel.mas_bottom).offset(kSizeScale(14));
        make.right.equalTo(self);
        make.height.mas_equalTo(kSizeScale(0.5));
    }];
    
    [self.phoneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView).offset(-kSizeScale(18));
        make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(18));
        make.width.mas_equalTo(kSizeScale(24));
        make.height.mas_equalTo(kSizeScale(24));
    }];
    
    [self.locationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.phoneButton.mas_left).offset(-kSizeScale(32));
        make.centerY.equalTo(self.phoneButton);
        make.width.mas_equalTo(kSizeScale(24));
        make.height.mas_equalTo(kSizeScale(24));
    }];
    
    [self.locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopNameLabel);
        make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(16));
        make.width.mas_equalTo(kSizeScale(11));
        make.height.mas_equalTo(kSizeScale(14));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(13));
       make.left.equalTo(self.shopNameLabel);
       make.right.equalTo(self.locationButton.mas_left).offset(-kSizeScale(22));
    }];
    
    [self.roundLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.addressLabel.mas_bottom).offset(kSizeScale(2));
       make.left.equalTo(self.locationImageView);
       make.height.mas_equalTo(kSizeScale(17));
    }];
}

#pragma mark LazyLoad
- (UIImageView *)productImageView {
    if(!_productImageView) {
        _productImageView = [[UIImageView alloc] init];
        _productImageView.backgroundColor = XHLightColor;
//        _productImageView.backgroundColor = [UIColor redColor];
    }
    return _productImageView;
}

- (UIView *)backView {
    if(!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
        _backView.layer.cornerRadius = kSizeScale(8);
    }
    return _backView;
}

- (UILabel *)shopNameLabel {
    if(!_shopNameLabel) {
        _shopNameLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(20)];
        _shopNameLabel.numberOfLines = 1;
    }
    return _shopNameLabel;
}

- (UILabel *)shopTypeLabel {
    if(!_shopTypeLabel) {
        _shopTypeLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(14)];
        _shopTypeLabel.numberOfLines = 1;
    }
    return _shopTypeLabel;
}

- (UILabel *)shopOpenTimeLabel {
    if(!_shopOpenTimeLabel) {
        _shopOpenTimeLabel = [UILabel creatLabelWithTitle:@"营业时间  --" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(14)];
        _shopOpenTimeLabel.numberOfLines = 1;
    }
    return _shopOpenTimeLabel;
}

- (UILabel *)roundLabel {
    if(!_roundLabel) {
        _roundLabel = [UILabel creatLabelWithTitle:@" " textColor:XHBlackLitColor textAlignment:NSTextAlignmentLeft font:midFont(12)];
        _roundLabel.numberOfLines = 1;
    }
    return _roundLabel;
}

- (UILabel *)addressLabel {
    if(!_addressLabel) {
        _addressLabel = [UILabel creatLabelWithTitle:@"" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(14)];
        _addressLabel.numberOfLines = 2;
    }
    return _addressLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}

- (UIButton *)phoneButton {
    if(!_phoneButton) {
        _phoneButton = [UIButton creatButtonWithTitle:@"" textColor:nil font:nil target:self action:@selector(phoneOperation)];
        [_phoneButton setImage:[UIImage imageNamed:@"btn_xiangqing_dianhua"] forState:UIControlStateNormal];
    }
    return _phoneButton;
}

- (UIButton *)locationButton {
    if(!_locationButton) {
        _locationButton = [UIButton creatButtonWithTitle:@"" textColor:nil font:nil target:self action:@selector(locationOperation)];
        [_locationButton setImage:[UIImage imageNamed:@"btn_xiangqing_dizhi"] forState:UIControlStateNormal];
    }
    return _locationButton;
}

- (UIImageView *)locationImageView {
    if(!_locationImageView) {
        _locationImageView = [[UIImageView alloc] init];
        _locationImageView.image = [UIImage imageNamed:@"icon_shangjia_dingwe"];
    }
    return _locationImageView;
}

- (JXMapNavigationView *)mapNavigationView{
    if (_mapNavigationView == nil) {
        _mapNavigationView = [[JXMapNavigationView alloc]init];
    }
    return _mapNavigationView;
}

- (void)locationOperation {
    if(self.model.businessAddressVo.latitude > 0) {
        NSString *address = [NSString stringWithFormat:@"%@%@%@%@%@",self.model.businessAddressVo.province,self.model.businessAddressVo.city,self.model.businessAddressVo.district,self.model.businessAddressVo.street,self.model.businessAddressVo.address];
        [self.mapNavigationView showMapNavigationViewWithtargetLatitude:self.model.businessAddressVo.latitude targetLongitute:self.model.businessAddressVo.longitude toName:address];
        [[YYCommonTools getCurrentVC].view addSubview:_mapNavigationView];
    }
}

- (void)phoneOperation {
    if(kValidString(self.model.phone)) {
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.model.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
    }
}

- (void)setModel:(MchListModel *)model {
    _model = model;
    if(model) {
        if(kValidString(model.shopImgUrl)) {
            [self.productImageView sd_setImageWithURL:[NSURL URLWithString:model.shopImgUrl]];
        }
        if(kValidString(model.shopName)) {
            self.shopNameLabel.text = model.shopName;
        }
        if(kValidString(model.describe)) {
            self.shopTypeLabel.text = model.describe;
        }
        if(kValidString(model.saleTime)) {
            self.shopOpenTimeLabel.text = [NSString stringWithFormat:@"营业时间  %@",model.saleTime];
        }
        if(model.businessAddressVo.distance < 1000) {
            self.roundLabel.text = [NSString stringWithFormat:@"距您%.1fm",model.businessAddressVo.distance];
        } else {
            self.roundLabel.text = [NSString stringWithFormat:@"距您%.1fkm",model.businessAddressVo.distance / 1000.0];
        }
        self.addressLabel.text = [NSString stringWithFormat:@"   %@%@%@%@%@",model.businessAddressVo.province,model.businessAddressVo.city,model.businessAddressVo.district,model.businessAddressVo.street,model.businessAddressVo.address];
    }
}

@end
