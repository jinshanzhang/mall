//
//  MachDetailNoProductCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/23.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MachDetailNoProductCell.h"

@implementation MachDetailNoProductCell

- (void)js_createSubViews {
    [super js_createSubViews];
    self.backgroundColor = HexRGB(0xF5F5F5);
    [self.contentView addSubview:self.noDataImageView];
    [self.contentView addSubview:self.noDataLabel];
    [self.contentView addSubview:self.noDataButton];
}

- (void)js_layoutSubViews {
    [super js_layoutSubViews];
    [self.noDataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(20));
    }];
    
    [self.noDataImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(self.noDataLabel.mas_top).offset(-kSizeScale(8));
        make.width.height.mas_equalTo(kSizeScale(109));
    }];
    
    [self.noDataButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.noDataLabel.mas_bottom).offset(kSizeScale(20));
        make.width.mas_equalTo(kSizeScale(120));
        make.height.mas_equalTo(kSizeScale(40));
    }];
}

- (UIImageView *)noDataImageView {
    if(!_noDataImageView) {
        _noDataImageView = [[UIImageView alloc] init];
        _noDataImageView.image = [UIImage imageNamed:@"img_shangjia_quesun"];
    }
    return _noDataImageView;
}

- (UILabel *)noDataLabel {
    if(!_noDataLabel) {
        _noDataLabel = [UILabel creatLabelWithTitle:@"获取当前位置失败" textColor:XHBlackLitColor textAlignment:NSTextAlignmentCenter font:midFont(14)];
    }
    return _noDataLabel;
}

- (UIButton *)noDataButton {
    if(!_noDataButton) {
        _noDataButton = [[UIButton alloc] init];
        [_noDataButton setImage:[UIImage imageNamed:@"btn_sousuo_quedinng"] forState:UIControlStateNormal];
        [_noDataButton addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
    }
    return _noDataButton;
}

- (void)refreshData {
    if(self.refreshBlock) {
        self.refreshBlock();
    }
}

@end
