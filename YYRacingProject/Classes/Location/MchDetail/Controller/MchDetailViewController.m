//
//  MchDetailViewController.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MchDetailViewController.h"
#import "MchDetailHeaderView.h"
#import "MchDetailListCell.h"

#import "VirtualOrderDetailViewController.h"

#import "MchListModel.h"

//#import "CommonNoDataView.h"
#import "MachDetailNoProductCell.h"

#import "YY_GoodDetailViewController.h"

@interface MchDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) MchDetailHeaderView * headerView;
//@property (nonatomic, strong) CommonNoDataView * noDataView;
@property (nonatomic, strong) MchListModel * shopMessageModel;

@end

@implementation MchDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    [self setTableView];
}

- (void)setSellerId:(NSString *)sellerId {
    _sellerId = sellerId;
    self.pageNum = 1;
    if(kValidString(sellerId)) {
        [self getShopMessageData];
    }
}

- (void)setUserId:(NSString *)userId {
    _userId = userId;
    [self lodaData];
}

/**
 下拉加载数据请求
 */
- (void)loadNewData {
    self.pageNum = 1;
    [self lodaData];
}

/**
上拉加载数据请求
*/
- (void)loadMoreData {
    [self lodaData];
}

- (void)getShopMessageData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"sellerId"] = self.sellerId;
    params[@"latitude"] = @(kUserManager.latitude);
    params[@"longitude"] = @(kUserManager.longitude);
    @weakify(self);
    [NetWorkHelper requestWithMethod:GET path:@"/ironman/v5/business/shopInfo" parameters:params block:^(NSString *errorNO, NSDictionary *result) {
        @strongify(self);
        NSString *code = result[@"ret"];
        if([code intValue] == 0) {
            NSDictionary *dataDict = [result objectForKey:@"data"];
            NSDictionary *shopDict = @{@"shop" : @[dataDict]};
            NSArray *items = [NSArray modelArrayWithClass:[MchListModel class] json:[shopDict objectForKey:@"shop"]];
            self.shopMessageModel = [items firstObject];
            self.headerView.model = self.shopMessageModel;
            [self.tableView reloadData];
        } else {
            if(self.dataArray.count > 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
        NSLog(@"获取到的商品信息请求的数据是:%@",result);
    }];
}

- (void)lodaData {
    self.pageSize = 20;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"pageNum"] = @(self.pageNum);
    params[@"pageSize"] = @(self.pageSize);
    if(kValidString(kUserInfo.userId)) {
        params[@"userId"] = kUserInfo.userId;
    }
    params[@"sellerUserId"] = self.userId;
    @weakify(self);
    [NetWorkHelper requestWithMethod:GET path:@"/ironman/v5/business/items" parameters:params block:^(NSString *errorNO, NSDictionary *result) {
        @strongify(self);
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        NSString *code = result[@"ret"];
        if([code intValue] == 0) {
             NSDictionary *dataDict = [result objectForKey:@"data"];
             NSArray *items = [NSArray modelArrayWithClass:[FictitiousGoodsVoList class] json:[dataDict objectForKey:@"items"]];
             NSMutableArray *marr = [NSMutableArray array];
             marr = [NSMutableArray arrayWithArray:items];
              if(self.pageNum == 1) {
                 self.dataArray = marr;
              } else {
                 [self.dataArray addObjectsFromArray:marr];
              }
             if(marr.count < self.pageSize) {
                 [self.tableView.mj_footer endRefreshingWithNoMoreData];
             } else {
                 self.pageNum++;
             }
            self.tableView.mj_footer.hidden = (self.dataArray.count == 0);
            if(self.dataArray.count == 0) {
                self.tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewNoDataType title:@"暂无商品"
                target:self
                action:@selector(refreshData)];
                self.tableView.ly_emptyView.contentViewY = kSizeScale(143+270);
            }
            [self.tableView reloadData];
        } else {
            if(self.dataArray.count > 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }
//        NSLog(@"获取到的请求的数据是:%@",result);
    }];
}

- (void)refreshData {
    [self.tableView.mj_header beginRefreshing];
}

- (void)setUpNav {
    [self xh_hideNavigation:YES];
    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
}

- (void)setBackButton {
   @weakify(self);
   UIButton *leftBtn = [YYCreateTools createBtnImage:@"video_navbar_back"];
    
   [self.view addSubview:leftBtn];
   leftBtn.actionBlock = ^(UIButton *btn) {
       @strongify(self);
       [self.navigationController popViewControllerAnimated:YES];
   };
   [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(kStatuH + kSizeScale(20));
       make.left.equalTo(self.view).offset(kSizeScale(20));
       make.size.mas_equalTo(CGSizeMake(kSizeScale(30), kSizeScale(30)));
   }];
   leftBtn.layer.cornerRadius = kSizeScale(15);
   leftBtn.clipsToBounds = YES;
   leftBtn.backgroundColor = HexRGBALPHA(0x000000, 0.6);
}

- (MchDetailHeaderView *)headerView {
    if(!_headerView) {
        _headerView = [[MchDetailHeaderView alloc] initWithFrame:CGRectMake(0, kNavigationH, kScreenW, kSizeScale(143+278))];
    }
    return _headerView;
}

- (void)setTableView {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[MchDetailListCell class] forCellReuseIdentifier:@"MchDetailListCell"];
    [self.tableView registerClass:[MachDetailNoProductCell class] forCellReuseIdentifier:@"MachDetailNoProductCell"];
    self.tableView.tableHeaderView = self.headerView;
    self.needPullUpRefresh = YES;
    self.needPullDownRefresh = YES;
    [self setBackButton];
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view);
    }];
    [self.tableView reloadData];
}

#pragma mark----- UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
//    if(self.dataArray.count <= 0) {
//        MachDetailNoProductCell *cell = [tableView dequeueReusableCellWithIdentifier:[MachDetailNoProductCell description]];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
//        return cell;
//    }
    MchDetailListCell *cell = [tableView dequeueReusableCellWithIdentifier:[MchDetailListCell description]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row < self.dataArray.count) {
        FictitiousGoodsVoList *model = self.dataArray[indexPath.row];
        cell.model = model;
    }
    
//    [cell.productImageView sd_setImageWithURL:[NSURL URrLWithString:model.imageUrl]];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if(self.dataArray.count <= 0) {
//        return 1;
//    }
    return self.dataArray.count;
}

#pragma mark----- UITableViewDelagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(self.dataArray.count <= 0) {
//        return kSizeScale(260);
//    }
    return kSizeScale(110);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  0.00001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    VirtualOrderDetailViewController *detailVC = [[VirtualOrderDetailViewController alloc] init];
//    detailVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:detailVC animated:YES];
    if(indexPath.row < self.dataArray.count) {
        FictitiousGoodsVoList *model = self.dataArray[indexPath.row];
        YY_GoodDetailViewController *goodDetail = [[YY_GoodDetailViewController alloc] init];
        goodDetail.parameter = @{@"linkType" : @"1",@"url" : model.url,@"goodsType":@(model.goodsType)};
        goodDetail.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:goodDetail animated:YES];
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.00001;
}

//- (CommonNoDataView *)noDataView {
//    if(!_noDataView) {
//        _noDataView = [[CommonNoDataView alloc] init];
//        _noDataView.hidden = YES;
//        [self.view addSubview:_noDataView];
//        [_noDataView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.right.centerX.equalTo(self.tableView);
//            make.height.mas_equalTo(kScreenH - kSizeScale(143+278));
//            make.bottom.equalTo(self.view);
//        }];
//        @weakify(self);
//        _noDataView.refreshBlock = ^{
//            @strongify(self);
//            [self loadNewData];
//        };
//    }
//    return _noDataView;
//}

@end
