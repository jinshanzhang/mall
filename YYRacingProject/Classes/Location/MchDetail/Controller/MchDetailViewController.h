//
//  MchDetailViewController.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/13.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewController.h"
//#import "MchListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MchDetailViewController : BaseTableViewController

@property (nonatomic, strong) NSString * sellerId;
@property (nonatomic, copy) NSString * userId;

@end

NS_ASSUME_NONNULL_END
