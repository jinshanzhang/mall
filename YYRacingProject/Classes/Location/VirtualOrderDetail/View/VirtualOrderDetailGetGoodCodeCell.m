//
//  VirtualOrderDetailGetGoodCodeCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/4.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderDetailGetGoodCodeCell.h"
#import "QiCodeManager.h"

@interface VirtualOrderDetailGetGoodCodeCell()

@property (nonatomic, strong) UILabel * getCodeTextLabel;
@property (nonatomic, strong) UIImageView * codeImageView;
@property (nonatomic, strong) UIImageView * useTypeImageView;
@property (nonatomic, strong) UILabel * codeLabel;
@property (nonatomic, strong) UILabel * warnLabel;
@property (nonatomic, strong) UIView * lineView;


@end

@implementation VirtualOrderDetailGetGoodCodeCell

-(void)setFrame:(CGRect)frame{
    CGFloat margin = kSizeScale(12);
    frame.origin.x = margin;
    frame.size.width = kScreenW - margin * 2;
    [super setFrame:frame];
}

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    
    [self.contentView addSubview:self.getCodeTextLabel];
    [self.contentView addSubview:self.codeImageView];
    [self.contentView addSubview:self.useTypeImageView];
    [self.contentView addSubview:self.codeLabel];
    [self.contentView addSubview:self.warnLabel];
    [self.contentView addSubview:self.lineView];
}

- (void)js_layoutSubViews {
    [self.getCodeTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(kSizeScale(20));
        make.height.mas_equalTo(kSizeScale(20));
    }];
    
    [self.codeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.getCodeTextLabel.mas_bottom).offset(kSizeScale(4.0));
        make.width.height.mas_equalTo(kSizeScale(176));
    }];
    
    [self.useTypeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(kSizeScale(18.0));
        make.width.height.mas_equalTo(kSizeScale(80));
        make.centerX.equalTo(self.codeImageView.mas_right);
    }];
    
    [self.codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.codeImageView.mas_bottom).offset(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(18));
    }];
    
    [self.warnLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.codeLabel.mas_bottom).offset(kSizeScale(2.0));
        make.height.mas_equalTo(kSizeScale(18));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(1.0));
        make.bottom.equalTo(self.contentView);
        make.right.equalTo(self.contentView).offset(-kSizeScale(12.0));
    }];
}

- (UILabel *)getCodeTextLabel {
    if(!_getCodeTextLabel) {
        _getCodeTextLabel = [UILabel creatLabelWithTitle:@"您的提货码" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentCenter font:boldFont(14)];
    }
    return _getCodeTextLabel;
}

- (UIImageView *)codeImageView {
    if(!_codeImageView) {
        _codeImageView = [[UIImageView alloc] init];
        _codeImageView.backgroundColor = XHLightColor;
    }
    return _codeImageView;
}

- (UIImageView *)useTypeImageView {
    if(!_useTypeImageView) {
        _useTypeImageView = [[UIImageView alloc] init];
        _useTypeImageView.image = [UIImage imageNamed:@"icon_dingdan_wancheng"];
    }
    return _useTypeImageView;
}

- (UILabel *)codeLabel {
    if(!_codeLabel) {
        _codeLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentCenter font:midFont(13)];
    }
    return _codeLabel;
}

- (UILabel *)warnLabel {
    if(!_warnLabel) {
        _warnLabel = [UILabel creatLabelWithTitle:@"如未显示使用码，请下拉刷新" textColor:XHBlackLitColor textAlignment:NSTextAlignmentCenter font:midFont(13)];
    }
    return _warnLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}

- (void)setModel:(VirtualOrderListModel *)model {
    _model = model;
    self.codeLabel.text = model.pickNumber;
     if(model.orderStatus == 8) {
        // 已核销:删除订单
        self.useTypeImageView.hidden = NO;
        self.useTypeImageView.image = [UIImage imageNamed:@"icon_dingdan_wancheng"];
        self.codeImageView.image = [QiCodeManager generateQRCode:model.pickNumber size:CGSizeMake(kSizeScale(176), kSizeScale(176))];
         self.codeImageView.alpha = 0.3;
    } else if(model.orderStatus == 20) {
        //已过期  删除订单,退款
        self.useTypeImageView.hidden = NO;
        self.useTypeImageView.image = [UIImage imageNamed:@"icon_dingdan_shixiao"];
        self.codeImageView.image = [QiCodeManager generateQRCode:model.pickNumber size:CGSizeMake(kSizeScale(176), kSizeScale(176))];
        self.codeImageView.alpha = 0.3;
    } else if(model.orderStatus == 6 || model.orderStatus == 7) {
        // 待核销
        self.useTypeImageView.hidden = YES;
        self.useTypeImageView.image = [UIImage imageNamed:@"icon_dingdan_wancheng"];
        self.codeImageView.image = [QiCodeManager generateQRCode:model.pickNumber size:CGSizeMake(kSizeScale(176), kSizeScale(176))];
    } else {
        self.useTypeImageView.hidden = YES;
    }
}


@end
