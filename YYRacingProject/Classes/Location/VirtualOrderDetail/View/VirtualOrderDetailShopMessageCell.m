//
//  VirtualOrderDetailShopMessageCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/4.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderDetailShopMessageCell.h"
#import "JXMapNavigationView.h"
#import "MchDetailViewController.h"
@interface VirtualOrderDetailShopMessageCell()

@property (nonatomic, strong) UIImageView * shopImgView;
@property (nonatomic, strong) UIButton * shopNameBtn;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UIImageView * locationImageView;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UIButton * phoneButton;
@property (nonatomic, strong) UIButton * locationButton;

@property (nonatomic, strong)JXMapNavigationView *mapNavigationView;

@end

@implementation VirtualOrderDetailShopMessageCell

-(void)setFrame:(CGRect)frame{
    CGFloat margin = kSizeScale(12);
    frame.origin.x = margin;
    frame.size.width = kScreenW - margin * 2;
    [super setFrame:frame];
}

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.shopImgView];
    [self addSubview:self.shopNameBtn];
    [self addSubview:self.lineView];
    [self addSubview:self.locationImageView];
    [self addSubview:self.addressLabel];
    [self addSubview:self.phoneButton];
    [self addSubview:self.locationButton];
}

- (void)js_layoutSubViews {
    [self.shopImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self).offset(kSizeScale(12));
        make.width.height.mas_equalTo(kSizeScale(50));
    }];
    
    [self.shopNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.shopImgView).offset(kSizeScale(4));
        make.left.equalTo(self.shopImgView.mas_right).offset(kSizeScale(10));
        make.width.mas_lessThanOrEqualTo(kSizeScale(240));
        make.height.mas_equalTo(kSizeScale(22));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopImgView);
        make.height.mas_equalTo(kSizeScale(1.0));
        make.top.equalTo(self.shopImgView.mas_bottom).offset(kSizeScale(20));
        make.right.equalTo(self).offset(-kSizeScale(5.0));
    }];
    
    [self.phoneButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(kSizeScale(24));
        make.right.equalTo(self).offset(-kSizeScale(22));
        make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(19));
    }];
    
    [self.locationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(kSizeScale(24));
        make.right.equalTo(self.phoneButton.mas_left).offset(-kSizeScale(32));
        make.centerY.equalTo(self.phoneButton);
    }];
    
    [self.locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.shopImgView);
        make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(16));
        make.width.mas_equalTo(kSizeScale(11));
        make.height.mas_equalTo(kSizeScale(14));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(13));
       make.left.equalTo(self.shopImgView);
       make.right.equalTo(self.locationButton.mas_left).offset(-kSizeScale(22));
    }];
}

- (UIImageView *)shopImgView {
    if(!_shopImgView) {
        _shopImgView = [[UIImageView alloc] init];
        _shopImgView.backgroundColor = XHLightColor;
        _shopImgView.layer.cornerRadius = kSizeScale(8.0);
        _shopImgView.clipsToBounds = YES;
    }
    return _shopImgView;
}

- (UIButton *)shopNameBtn {
    if(!_shopNameBtn) {
        _shopNameBtn = [UIButton creatButtonWithTitle:@"--" textColor:XHTimeBlackColor font:boldFont(16) target:self action:@selector(shopNameBtnOperation)];
        [_shopNameBtn setImage:[UIImage imageNamed:@"icon_liebiao_taiozhuan"] forState:UIControlStateNormal];
        _shopNameBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_shopNameBtn layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(7.0)];
    }
    return _shopNameBtn;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}

- (void)shopNameBtnOperation {
   MchDetailViewController *detailVC = [[MchDetailViewController alloc] init];
   detailVC.hidesBottomBarWhenPushed = YES;
   detailVC.sellerId = self.model.sellerId;
   detailVC.userId = [NSString stringWithFormat:@"%@",self.model.sellerUserId];
   [[YYCommonTools getCurrentVC].navigationController pushViewController:detailVC animated:YES];
}

- (UIImageView *)locationImageView {
    if(!_locationImageView) {
        _locationImageView = [[UIImageView alloc] init];
        _locationImageView.image = [UIImage imageNamed:@"icon_shangjia_dingwe"];
    }
    return _locationImageView;
}

- (UILabel *)addressLabel {
    if(!_addressLabel) {
        _addressLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(14)];
        _addressLabel.numberOfLines = 2;
    }
    return _addressLabel;
}

- (UIButton *)phoneButton {
    if(!_phoneButton) {
        _phoneButton = [UIButton creatButtonWithTitle:@"" textColor:nil font:nil target:self action:@selector(phoneOperation)];
        [_phoneButton setImage:[UIImage imageNamed:@"btn_xiangqing_dianhua"] forState:UIControlStateNormal];
    }
    return _phoneButton;
}

- (UIButton *)locationButton {
    if(!_locationButton) {
        _locationButton = [UIButton creatButtonWithTitle:@"" textColor:nil font:nil target:self action:@selector(locationOperation)];
        [_locationButton setImage:[UIImage imageNamed:@"btn_xiangqing_dizhi"] forState:UIControlStateNormal];
    }
    return _locationButton;
}

- (JXMapNavigationView *)mapNavigationView{
    if (_mapNavigationView == nil) {
        _mapNavigationView = [[JXMapNavigationView alloc]init];
    }
    return _mapNavigationView;
}

- (void)locationOperation {
    if(self.model.latitude > 0) {
        NSString *address = [NSString stringWithFormat:@"%@%@%@%@%@",self.model.province,self.model.city,self.model.district,self.model.street,self.model.address];
        [self.mapNavigationView showMapNavigationViewWithtargetLatitude:self.model.latitude targetLongitute:self.model.longitude toName:address];
        [[YYCommonTools getCurrentVC].view addSubview:_mapNavigationView];
    }
}

- (void)phoneOperation {
    if(kValidString(self.model.phone)) {
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.model.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
    }
}

- (void)setModel:(VirtualOrderListModel *)model {
    _model = model;
    [self.shopNameBtn setTitle:model.shopName forState:UIControlStateNormal];
    [self.shopNameBtn layoutWithStyle:ButtonLayoutStyleImagePositionRight space:kSizeScale(7.0)];
    if(kValidString(model.shopLogo)) {
        [self.shopImgView sd_setImageWithURL:[NSURL URLWithString:model.shopLogo]];
    }
    NSString *address = [NSString stringWithFormat:@"    %@%@%@%@%@",self.model.province,self.model.city,self.model.district,self.model.street,self.model.address];
    self.addressLabel.text = address;
}

@end
