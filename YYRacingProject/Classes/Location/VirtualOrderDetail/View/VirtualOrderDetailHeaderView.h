//
//  VirtualOrderDetailHeaderView.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseView.h"
#import "VirtualOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VirtualOrderDetailHeaderView : BaseView

@property (nonatomic, strong) VirtualOrderListModel * model;

@end

NS_ASSUME_NONNULL_END
