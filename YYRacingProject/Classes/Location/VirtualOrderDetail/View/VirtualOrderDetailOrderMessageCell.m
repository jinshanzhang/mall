//
//  VirtualOrderDetailOrderMessageCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/10/4.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderDetailOrderMessageCell.h"

@interface VirtualOrderDetailOrderMessageCell()

@property (nonatomic, strong) UILabel * orderMessageTextLabel;
@property (nonatomic, strong) UIView * lineView;
@property (nonatomic, strong) UILabel * orderNumTextLabel;
@property (nonatomic, strong) UILabel * orderNumLabel;

@property (nonatomic, strong) UILabel * orderTimeTextLabel;
@property (nonatomic, strong) UILabel * orderTimeLabel;

@property (nonatomic, strong) UILabel * orderTotalPriceTextLabel;
@property (nonatomic, strong) UILabel * orderTotalPriceLabel;

@property (nonatomic, strong) UILabel * orderActualPriceTextLabel;
@property (nonatomic, strong) UILabel * orderActualPriceLabel;

@property (nonatomic, strong) UIView * bottomLineView;
@property (nonatomic, strong) UIButton * contactBtn;

@end

@implementation VirtualOrderDetailOrderMessageCell

-(void)setFrame:(CGRect)frame{
    CGFloat margin = kSizeScale(12);
    frame.origin.x = margin;
    frame.size.width = kScreenW - margin * 2;
    [super setFrame:frame];
}

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.orderMessageTextLabel];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.orderNumTextLabel];
    [self.contentView addSubview:self.orderNumLabel];
    [self.contentView addSubview:self.orderTimeTextLabel];
    [self.contentView addSubview:self.orderTimeLabel];
    [self.contentView addSubview:self.orderTotalPriceTextLabel];
    [self.contentView addSubview:self.orderTotalPriceLabel];
    [self.contentView addSubview:self.orderActualPriceTextLabel];
    [self.contentView addSubview:self.orderActualPriceLabel];
    [self.contentView addSubview:self.bottomLineView];
    [self.contentView addSubview:self.contactBtn];
}

- (void)js_layoutSubViews {
    [self.orderMessageTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.contentView).offset(kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(22.0));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(1.0));
        make.top.equalTo(self.orderMessageTextLabel.mas_bottom).offset(kSizeScale(12));
        make.right.equalTo(self.contentView).offset(-kSizeScale(12.0));
    }];
    
    [self.orderNumTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.top.equalTo(self.lineView.mas_bottom).offset(kSizeScale(17));
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-kSizeScale(12));
        make.centerY.equalTo(self.orderNumTextLabel);
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderTimeTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.top.equalTo(self.orderNumTextLabel.mas_bottom).offset(kSizeScale(18));
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-kSizeScale(12));
        make.centerY.equalTo(self.orderTimeTextLabel);
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderTotalPriceTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.top.equalTo(self.orderTimeTextLabel.mas_bottom).offset(kSizeScale(18));
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderTotalPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-kSizeScale(12));
        make.centerY.equalTo(self.orderTotalPriceTextLabel);
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderActualPriceTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.top.equalTo(self.orderTotalPriceTextLabel.mas_bottom).offset(kSizeScale(18));
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.orderActualPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-kSizeScale(12));
        make.centerY.equalTo(self.orderActualPriceTextLabel);
        make.height.mas_equalTo(kSizeScale(20.0));
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(kSizeScale(12));
        make.height.mas_equalTo(kSizeScale(1.0));
        make.top.equalTo(self.orderActualPriceTextLabel.mas_bottom).offset(kSizeScale(20));
        make.right.equalTo(self.contentView).offset(-kSizeScale(12.0));
    }];
    
    [self.contactBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomLineView.mas_bottom).offset(kSizeScale(13));
        make.centerX.equalTo(self.contentView);
        make.height.mas_equalTo(kSizeScale(20));
        make.width.mas_equalTo(kSizeScale(82));
    }];
}

#pragma mark LazyLoad

- (UILabel *)orderMessageTextLabel {
    if(!_orderMessageTextLabel) {
        _orderMessageTextLabel = [UILabel creatLabelWithTitle:@"订单信息" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:boldFont(16)];
    }
    return _orderMessageTextLabel;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _lineView;
}

- (UILabel *)orderNumTextLabel {
    if(!_orderNumTextLabel) {
        _orderNumTextLabel = [UILabel creatLabelWithTitle:@"订单编号" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(14)];
    }
    return _orderNumTextLabel;
}

- (UILabel *)orderNumLabel {
    if(!_orderNumLabel) {
        _orderNumLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackMidColor textAlignment:NSTextAlignmentRight font:midFont(14)];
    }
    return _orderNumLabel;
}

- (UILabel *)orderTimeTextLabel {
    if(!_orderTimeTextLabel) {
        _orderTimeTextLabel = [UILabel creatLabelWithTitle:@"下单时间" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(14)];
    }
    return _orderTimeTextLabel;
}

- (UILabel *)orderTimeLabel {
    if(!_orderTimeLabel) {
        _orderTimeLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackMidColor textAlignment:NSTextAlignmentRight font:midFont(14)];
    }
    return _orderTimeLabel;
}

- (UILabel *)orderTotalPriceTextLabel {
    if(!_orderTotalPriceTextLabel) {
        _orderTotalPriceTextLabel = [UILabel creatLabelWithTitle:@"订单总价" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(14)];
    }
    return _orderTotalPriceTextLabel;
}

- (UILabel *)orderTotalPriceLabel {
    if(!_orderTotalPriceLabel) {
        _orderTotalPriceLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackMidColor textAlignment:NSTextAlignmentRight font:midFont(14)];
    }
    return _orderTotalPriceLabel;
}

- (UILabel *)orderActualPriceTextLabel {
    if(!_orderActualPriceTextLabel) {
        _orderActualPriceTextLabel = [UILabel creatLabelWithTitle:@"实际付款" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(14)];
    }
    return _orderActualPriceTextLabel;
}

- (UILabel *)orderActualPriceLabel {
    if(!_orderActualPriceLabel) {
        _orderActualPriceLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackMidColor textAlignment:NSTextAlignmentRight font:midFont(14)];
    }
    return _orderActualPriceLabel;
}

- (UIView *)bottomLineView {
    if(!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = HexRGB(0xf0f0f0);
    }
    return _bottomLineView;
}

- (UIButton *)contactBtn {
    if(!_contactBtn) {
        _contactBtn = [UIButton creatButtonWithTitle:@"联系售后" textColor:XHTimeBlackColor font:boldFont(14) target:self action:@selector(contactBtnOperation)];
        [_contactBtn setImage:[UIImage imageNamed:@"icon_dingdan_shouhou"] forState:UIControlStateNormal];
        [_contactBtn layoutWithStyle:ButtonLayoutStyleImagePositionLeft space:kSizeScale(5.0)];
    }
    return _contactBtn;
}

- (void)contactBtnOperation {
    if(kValidString(self.model.phone)) {
        NSMutableString *str = [[NSMutableString alloc] initWithFormat:@"telprompt://%@",self.model.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str] options:nil completionHandler:nil];
    }
}

- (void)setModel:(VirtualOrderListModel *)model {
    _model = model;
    self.orderNumLabel.text = model.orderId;
    self.orderTimeLabel.text = [NSString formatDateAndTime:[model.payTime longValue]];
    __block NSString *totalString = @"";
    __block NSString *actualString = @"";
    [model.summaryList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
         SummaryList *priceModel = (SummaryList *)obj;
        if (priceModel.type == 3) {
            totalString = priceModel.priceTag;
        }
        if (priceModel.type == 4) {
            actualString = priceModel.priceTag;
        }
    }];
    self.orderTotalPriceLabel.text = [NSString stringWithFormat:@"¥ %.2f",totalString.floatValue];
    self.orderActualPriceLabel.text = [NSString stringWithFormat:@"¥ %.2f",actualString.floatValue];
}

@end
