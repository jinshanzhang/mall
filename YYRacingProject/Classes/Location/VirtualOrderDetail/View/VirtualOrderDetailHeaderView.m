//
//  VirtualOrderDetailHeaderView.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderDetailHeaderView.h"

@interface VirtualOrderDetailHeaderView()

@property (nonatomic, strong) UILabel * orderTypeLabel;


@end

@implementation VirtualOrderDetailHeaderView

- (void)js_createSubViews {
    [self addSubview:self.orderTypeLabel];
    
}

- (void)js_layoutSubViews {
    [self.orderTypeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(kSizeScale(24));
        make.height.mas_equalTo(kSizeScale(33));
    }];
}

#pragma mark LazyLoad


- (UILabel *)orderTypeLabel {
    if(!_orderTypeLabel) {
        _orderTypeLabel = [UILabel creatLabelWithTitle:@"订单待提取" textColor:HexRGB(0xFF583D) textAlignment:NSTextAlignmentLeft font:boldFont(24)];
    }
    return _orderTypeLabel;
}

- (void)setModel:(VirtualOrderListModel *)model {
    _model = model;
    if(model.orderStatus == 2 || model.orderStatus == 3) {
        // 待付款:删除订单,去支付
        self.orderTypeLabel.text = @"订单待支付";
    } else if(model.orderStatus == 6 || model.orderStatus == 7) {
        // 待核销
        self.orderTypeLabel.text = @"订单待核销";
    } else if(model.orderStatus == 8) {
        // 已核销:删除订单
        self.orderTypeLabel.text = @"订单已核销";
    } else if(model.orderStatus == 20) {
        //已过期  删除订单,退款
        self.orderTypeLabel.text = @"订单已过期";
    }
}

@end
