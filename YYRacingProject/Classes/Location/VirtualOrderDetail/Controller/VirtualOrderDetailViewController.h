//
//  VirtualOrderDetailViewController.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewController.h"
#import "VirtualOrderListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VirtualOrderDetailViewController : BaseTableViewController

@property (nonatomic, strong) VirtualOrderListModel * model;

@end

NS_ASSUME_NONNULL_END
