//
//  VirtualOrderDetailViewController.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/19.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "VirtualOrderDetailViewController.h"
#import "VirtualOrderDetailShopMessageCell.h"
#import "VirtualOrderDetailGetGoodCodeCell.h"
#import "VirtualOrderDetailProductListCell.h"
#import "VirtualOrderDetailOrderMessageCell.h"
#import "VirtualOrderDetailHeaderView.h"
@interface VirtualOrderDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) VirtualOrderDetailHeaderView * headerView;
@property (nonatomic, strong) UIButton * leftBtn;

@end

@implementation VirtualOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpNav];
    [self setTableView];
}

- (void)setModel:(VirtualOrderListModel *)model {
    _model = model;
    self.headerView.model = model;
    [self loadData];
    [self.tableView reloadData];
}

- (void)setUpNav {
    [self xh_hideNavigation:YES];
    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
}

- (void)loadData {
    @weakify(self);
    [NetWorkHelper requestWithMethod:GET path:[NSString stringWithFormat:@"/ironman/v3/buyer/orders/%@",self.model.orderId] parameters:@{} block:^(NSString *errorNO, NSDictionary *result) {
            @strongify(self);
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            NSString *code = result[@"ret"];
            if([code intValue] == 0) {
//                 NSDictionary *dataDict = [result objectForKey:@"data"];
//                 NSArray *items = [NSArray modelArrayWithClass:[FictitiousGoodsVoList class] json:[dataDict objectForKey:@"items"]];
//                 NSMutableArray *marr = [NSMutableArray array];
//                 marr = [NSMutableArray arrayWithArray:items];
//                  if(self.pageNum == 1) {
//                     self.dataArray = marr;
//                  } else {
//                     [self.dataArray addObjectsFromArray:marr];
//                  }
//                 if(marr.count < self.pageSize) {
//                     [self.tableView.mj_footer endRefreshingWithNoMoreData];
//                 } else {
//                     self.pageNum++;
//                 }
//                self.tableView.mj_footer.hidden = (self.dataArray.count == 0);
//                if(self.dataArray.count == 0) {
//                    self.tableView.ly_emptyView = [YYEmptyView yyEmptyView:YYEmptyViewNoDataType title:@"暂无商品"
//                    target:self
//                    action:@selector(refreshData)];
//                    self.tableView.ly_emptyView.contentViewY = kSizeScale(143+270);
//                }
//                [self.tableView reloadData];
            } else {
                if(self.dataArray.count > 0) {
                    [self.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
            NSLog(@"获取到的请求的数据是:%@",result);
        }];
}

- (void)setBackButton {
   @weakify(self);
   UIButton *leftBtn = [YYCreateTools createBtnImage:@"icon_fanhui_hei"];
    
   [self.view addSubview:leftBtn];
   leftBtn.actionBlock = ^(UIButton *btn) {
       @strongify(self);
       [self.navigationController popViewControllerAnimated:YES];
   };
   [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(kStatuH + kSizeScale(5));
       make.left.equalTo(self.view).offset(kSizeScale(10));
       make.size.mas_equalTo(CGSizeMake(kSizeScale(30), kSizeScale(30)));
   }];
    self.leftBtn = leftBtn;
}

- (VirtualOrderDetailHeaderView *)headerView {
    if(!_headerView) {
        _headerView = [[VirtualOrderDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, kSizeScale(41))];
    }
    return _headerView;
}

- (void)setTableView {
    self.tableView.separatorColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerClass:[VirtualOrderDetailShopMessageCell class] forCellReuseIdentifier:@"VirtualOrderDetailShopMessageCell"];
    [self.tableView registerClass:[VirtualOrderDetailGetGoodCodeCell class] forCellReuseIdentifier:@"VirtualOrderDetailGetGoodCodeCell"];
    [self.tableView registerClass:[VirtualOrderDetailProductListCell class] forCellReuseIdentifier:@"VirtualOrderDetailProductListCell"];
    [self.tableView registerClass:[VirtualOrderDetailOrderMessageCell class] forCellReuseIdentifier:@"VirtualOrderDetailOrderMessageCell"];
    self.tableView.tableHeaderView = self.headerView;
    [self setBackButton];
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.leftBtn.mas_bottom);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view);
    }];
    self.needPullDownRefresh = YES;
}

- (void)loadNewData {
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}

#pragma mark----- UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if(indexPath.section == 0) {
        VirtualOrderDetailShopMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[VirtualOrderDetailShopMessageCell description]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = self.model;
        return cell;
    }
    if(indexPath.section == 1) {
        if(indexPath.row == 0) {
            VirtualOrderDetailGetGoodCodeCell *cell = [tableView dequeueReusableCellWithIdentifier:[VirtualOrderDetailGetGoodCodeCell description]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if(self.model.orderStatus == 2 || self.model.orderStatus == 3) {
                cell.hidden = YES;
            } else {
                cell.hidden = NO;
                cell.model = self.model;
            }
            return cell;
        }
        VirtualOrderDetailProductListCell *cell = [tableView dequeueReusableCellWithIdentifier:[VirtualOrderDetailProductListCell description]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        if(self.model.orderStatus == 2 || self.model.orderStatus == 3) {
//            cell.model = self.model.skuList[indexPath.row];
//        } else {
            cell.model = self.model.skuList[indexPath.row - 1];
//        }
        return cell;
    }
    VirtualOrderDetailOrderMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[VirtualOrderDetailOrderMessageCell description]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.model;
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1) {
        return self.model.skuList.count + 1;
    }
    return 1;
}

#pragma mark----- UITableViewDelagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        return kSizeScale(148);
    }
    if(indexPath.section == 1) {
        if(indexPath.row == 0) {
            if(self.model.orderStatus == 2 || self.model.orderStatus == 3) {
                return 0.000001f;
            } else {
                return kSizeScale(286);
            }
        }
        return kSizeScale(75);
    }
    if(indexPath.section == 2) {
        return kSizeScale(265);
    }
    return 0.0000001;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(section == 1 || section == 2) {
        return [[UIView alloc] init];
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 1 || section == 2) {
        return kSizeScale(8.0);
    }
    return  0.00001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.00001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell respondsToSelector:@selector(tintColor)]) {
        if (tableView == self.tableView) {
            // 圆角弧度半径
            CGFloat cornerRadius = kSizeScale(8.0);
            // 设置cell的背景色为透明，如果不设置这个的话，则原来的背景色不会被覆盖
            cell.backgroundColor = UIColor.clearColor;
            
            // 创建一个shapeLayer
            CAShapeLayer *layer = [[CAShapeLayer alloc] init];
            CAShapeLayer *backgroundLayer = [[CAShapeLayer alloc] init]; //显示选中
            // 创建一个可变的图像Path句柄，该路径用于保存绘图信息
            CGMutablePathRef pathRef = CGPathCreateMutable();
            // 获取cell的size
            CGRect bounds = CGRectInset(cell.bounds, 0, 0);
            
            // CGRectGetMinY：返回对象顶点坐标
            // CGRectGetMaxY：返回对象底点坐标
            // CGRectGetMinX：返回对象左边缘坐标
            // CGRectGetMaxX：返回对象右边缘坐标
            
            // 这里要判断分组列表中的第一行，每组section的第一行，每组section的中间行
            BOOL addLine = NO;
            // CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
            if (indexPath.row == 0) {
                // 初始起点为cell的左下角坐标
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
                // 起始坐标为左下角，设为p1，（CGRectGetMinX(bounds), CGRectGetMinY(bounds)）为左上角的点，设为p1(x1,y1)，(CGRectGetMidX(bounds), CGRectGetMinY(bounds))为顶部中点的点，设为p2(x2,y2)。然后连接p1和p2为一条直线l1，连接初始点p到p1成一条直线l，则在两条直线相交处绘制弧度为r的圆角。
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                // 终点坐标为右下角坐标点，把绘图信息都放到路径中去,根据这些路径就构成了一块区域了
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
                addLine = YES;
            } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
                // 初始起点为cell的左上角坐标
                CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
                CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
                // 添加一条直线，终点坐标为右下角坐标点并放到路径中去
                CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
            } else {
                // 添加cell的rectangle信息到path中（不包括圆角）
                CGPathAddRect(pathRef, nil, bounds);
                addLine = YES;
            }
            // 把已经绘制好的可变图像路径赋值给图层，然后图层根据这图像path进行图像渲染render
            layer.path = pathRef;
            backgroundLayer.path = pathRef;
            // 注意：但凡通过Quartz2D中带有creat/copy/retain方法创建出来的值都必须要释放
            CFRelease(pathRef);
            // 按照shape layer的path填充颜色，类似于渲染render
            // layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
            layer.fillColor = [UIColor whiteColor].CGColor;
            // 添加分隔线图层
            if (addLine == YES) {
                CALayer *lineLayer = [[CALayer alloc] init];
                CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
                lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
                // 分隔线颜色取自于原来tableview的分隔线颜色
                lineLayer.backgroundColor = tableView.separatorColor.CGColor;
                [layer addSublayer:lineLayer];
            }
            
            // view大小与cell一致
            UIView *roundView = [[UIView alloc] initWithFrame:bounds];
            // 添加自定义圆角后的图层到roundView中
            [roundView.layer insertSublayer:layer atIndex:0];
            roundView.backgroundColor = UIColor.clearColor;
            //cell的背景view
            //cell.selectedBackgroundView = roundView;
            cell.backgroundView = roundView;
            
            //以上方法存在缺陷当点击cell时还是出现cell方形效果，因此还需要添加以下方法
            UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
            backgroundLayer.fillColor = tableView.separatorColor.CGColor;
            [selectedBackgroundView.layer insertSublayer:backgroundLayer atIndex:0];
            selectedBackgroundView.backgroundColor = UIColor.clearColor;
            cell.selectedBackgroundView = selectedBackgroundView;
        }
    }
}

@end
