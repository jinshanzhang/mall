//
//  MainViewController.m
//  GDMapPlaceAroundDemo
//
//  Created by Mr.JJ on 16/6/14.
//  Copyright © 2016年 Mr.JJ. All rights reserved.
//

#import "LocationViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "MapPoiTableView.h"
#import "Reachability.h"
#import "UIView+CJCorner.h"
#import "LocationSearchResultViewController.h"

#define KSearchBarHeight (kSizeScale(50))
#define KCellHeight (kSizeScale(74))

@interface LocationViewController () <MAMapViewDelegate,MapPoiTableViewDelegate,AMapSearchDelegate,UISearchBarDelegate,SearchResultTableVCDelegate>

@property (nonatomic, strong) UIButton * sureButton;
@property (nonatomic, strong) UIView *searchView;

@end

@implementation LocationViewController
{
    MAMapView *_mapView;
    // 地图中心点的标记
    UIImageView *_centerMaker;
    // 地图中心点POI列表
    MapPoiTableView *_tableView;
    // 高德API不支持定位开关，需要自己设置
    UIButton *_locationBtn;
    UIImage *_imageLocated;
    UIImage *_imageNotLocate;
    // 搜索API
    AMapSearchAPI *_searchAPI;
    
    // 第一次定位标记
    BOOL isFirstLocated;
    // 搜索页数
    NSInteger searchPage;

    // 禁止连续点击两次
    BOOL _isMapViewRegionChangedFromTableView;
    
    UISearchController *_searchController;
    UITableView *_searchTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setNav];
    [self setSearchUI];
    [self netNotifier];
    [self initMapView];
    [self initCenterMarker];
    [self initLocationButton];
    [self initTableView];
    [self initSearch];
}

- (void)setNav {
//    [self xh_addTitle:@"定位"];
    [self xh_hideNavigation:YES];
    [self xh_popTopRootViewController:NO];
    self.view.backgroundColor = XHLightColor;
}

- (void)netNotifier {
    // 使用通知中心监听kReachabilityChangedNotification通知
   [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(reachabilityChanged:)
                                                name:kReachabilityChangedNotification object:nil];
   // 获取访问指定站点的Reachability对象
   Reachability *reach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
   // 让Reachability对象开启被监听状态
   [reach startNotifier];
}

#pragma mark - 初始化
- (void)initMapView {
    _mapView = [[MAMapView alloc] init];
    _mapView.delegate = self;
    // 不显示罗盘
    _mapView.showsCompass = NO;
    // 不显示比例尺
    _mapView.showsScale = NO;
    // 地图缩放等级
    _mapView.zoomLevel = 16;
    // 开启定位
    _mapView.showsUserLocation = YES;
    _mapView.userTrackingMode = MAUserTrackingModeFollow;
    [self.view addSubview:_mapView];
    
    CGFloat mapViewHeight = kScreenH - kNavigationH - kSizeScale(282);
    [_mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.searchView.mas_bottom).offset(kSizeScale(0));
        make.height.mas_equalTo(mapViewHeight);
    }];
}

- (void)initCenterMarker {
    UIImage *image = [UIImage imageNamed:@"centerMarker"];
    _centerMaker = [[UIImageView alloc] initWithImage:image];
    [self.view addSubview:_centerMaker];
    [_centerMaker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_mapView);
        make.width.mas_equalTo(image.size.width);
        make.height.mas_equalTo(image.size.height);
    }];
}

- (void)initTableView {
    _tableView = [[MapPoiTableView alloc] init];
    _tableView.delegate = self;
    _tableView.tableView.mj_footer.hidden = YES;
    [self.view addSubview:_tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(_mapView.mas_bottom).offset(-kSizeScale(10));
//        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.height.mas_equalTo(3 * KCellHeight);
    }];
    [_tableView addRoundedCorners:UIRectCornerTopLeft | UIRectCornerTopRight radius:CGSizeMake(kSizeScale(20), kSizeScale(20)) rect:CGRectMake(0, 0, kScreenW, 3 * KCellHeight)];
}

- (void)initLocationButton {
    _imageLocated = [UIImage imageNamed:@"gpsselected"];
    _imageNotLocate = [UIImage imageNamed:@"gpsnormal"];
    _locationBtn = [[UIButton alloc] init];
    _locationBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _locationBtn.backgroundColor = [UIColor colorWithRed:239.0/255 green:239.0/255 blue:239.0/255 alpha:1];
    _locationBtn.layer.cornerRadius = 3;
    [_locationBtn addTarget:self action:@selector(actionLocation) forControlEvents:UIControlEventTouchUpInside];
    [_locationBtn setImage:_imageNotLocate forState:UIControlStateNormal];
    [self.view addSubview:_locationBtn];
    
    [_locationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.right.equalTo(self.view).offset(-kSizeScale(10));
        make.bottom.equalTo(_mapView.mas_bottom).offset(-kSizeScale(10));
        make.width.mas_equalTo(kSizeScale(40));
        make.height.mas_equalTo(kSizeScale(40));
    }];
}

- (void)initSearch {
    searchPage = 1;
    _searchAPI = [[AMapSearchAPI alloc] init];
    _searchAPI.delegate = _tableView;
//    _searchResultTableVC = [[SearchResultTableVC alloc] init];
//    _searchResultTableVC.delegate = self;
//    _searchController = [[UISearchController alloc] initWithSearchResultsController:_searchResultTableVC];
//    _searchController.searchResultsUpdater = _searchResultTableVC;
//    _searchController.dimsBackgroundDuringPresentation = YES;
//    _searchController.searchBar.delegate = self;
//    _searchController.searchBar.frame = CGRectMake(0, kNavigationH, kScreenW, kSizeScale(50));
//    [self.view addSubview:_searchController.searchBar];
    
    [self.view addSubview:self.sureButton];
    [self.sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tableView.mas_bottom).offset(kSizeScale(5));
        make.left.equalTo(self.view).offset(kSizeScale(14));
        make.right.equalTo(self.view).offset(-kSizeScale(14));
        make.height.mas_equalTo(kSizeScale(44));
//        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-kSizeScale(16));
    }];
}

#pragma mark - Private method
- (void)setSearchUI {
    @weakify(self);
    UIView *searchView = [YYCreateTools createView:XHWhiteColor];
    [self.view addSubview:searchView];
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(searchView.superview);
        make.height.mas_equalTo(kNavigationH);
    }];
    self.searchView = searchView;
    
    UIButton *leftBtn = [YYCreateTools createBtnImage:@"detail_navbar_back"];
    [searchView addSubview:leftBtn];
    leftBtn.actionBlock = ^(UIButton *btn) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    };
    
    UIView *searchBgView = [YYCreateTools createView:HexRGB(0xF4F4F4)];
    searchBgView.layer.cornerRadius = kSizeScale(16);
    searchBgView.clipsToBounds = YES;
    [searchView addSubview:searchBgView];
    [searchBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftBtn.mas_right);
        make.right.mas_equalTo(-kSizeScale(12));
        make.top.mas_equalTo(kSizeScale(6) + kStatuH);
//        make.bottom.mas_equalTo(-kSizeScale(0));
        make.height.height.mas_equalTo(kSizeScale(32));
    }];
    
    [searchBgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchClickEvent)]];
    
    [leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.equalTo(searchBgView);
        make.size.mas_equalTo(CGSizeMake(kSizeScale(36), kSizeScale(36)));
    }];
    
    UIImageView *searchImgView = [YYCreateTools createImageView:@"category_search_icon"
                                                      viewModel:-1];
    [searchBgView addSubview:searchImgView];
    [searchImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchBgView);
        make.left.mas_equalTo(kSizeScale(20));
        make.size.mas_equalTo(CGSizeMake(kSizeScale(14), kSizeScale(14)));
    }];
    
    UILabel *placeholderLbl = [YYCreateTools createLabel:@"搜索您的位置"
                                                font:midFont(14)
                                           textColor:XHBlackLitColor];
    [searchBgView addSubview:placeholderLbl];

    [placeholderLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(searchImgView.mas_right).offset(kSizeScale(8));
        make.centerY.equalTo(searchBgView);
    }];
}

- (void)searchClickEvent {
    LocationSearchResultViewController *searchVC = [[LocationSearchResultViewController alloc] init];
    searchVC.type = 99;
    searchVC.delegate = self;
    searchVC.searchBarText = @"搜索您的位置";
    searchVC.maintitle = @"搜索您的位置";
    [searchVC setSearchCity:_tableView.selectedPoi.city];
    [self.navigationController pushViewController:searchVC animated:YES];
}

#pragma mark - Event
- (void)searchClickEvent:(UITapGestureRecognizer *)tapGesture {
    [YYCommonTools skipSearchPage:self title:@"" type:0];
}

- (void)cancelClickEvent:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MAMapViewDelegate
- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation {
    // 首次定位
    if (updatingLocation && !isFirstLocated) {
        [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude)];
        isFirstLocated = YES;
    }
}

- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    if (!_isMapViewRegionChangedFromTableView && isFirstLocated) {
        AMapGeoPoint *point = [AMapGeoPoint locationWithLatitude:_mapView.centerCoordinate.latitude longitude:_mapView.centerCoordinate.longitude];
        [self searchReGeocodeWithAMapGeoPoint:point];
        [self searchPoiByAMapGeoPoint:point];
        // 范围移动时当前页面数重置
        searchPage = 1;
    }
    _isMapViewRegionChangedFromTableView = NO;
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation {
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {
        static NSString *reuseIndetifier = @"anntationReuseIndetifier";
        MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIndetifier];
        if (!annotationView) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIndetifier];
            annotationView.image = [UIImage imageNamed:@"msg_location"];
            annotationView.centerOffset = CGPointMake(0, -18);
        }
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MAMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    MAAnnotationView *view = views[0];
    // 放到该方法中用以保证userlocation的annotationView已经添加到地图上了。
    if ([view.annotation isKindOfClass:[MAUserLocation class]]) {
        MAUserLocationRepresentation *pre = [[MAUserLocationRepresentation alloc] init];
        pre.showsAccuracyRing = NO;
        [_mapView updateUserLocationRepresentation:pre];
        view.calloutOffset = CGPointMake(0, 0);
    }
}

#pragma mark - MapPoiTableViewDelegate
- (void)loadMorePOI {
    searchPage++;
    AMapGeoPoint *point = [AMapGeoPoint locationWithLatitude:_mapView.centerCoordinate.latitude longitude:_mapView.centerCoordinate.longitude];
    [self searchPoiByAMapGeoPoint:point];
}

- (void)setMapCenterWithPOI:(AMapPOI *)point isLocateImageShouldChange:(BOOL)isLocateImageShouldChange {
    // 切换定位图标
    if (isLocateImageShouldChange) {
        [_locationBtn setImage:_imageNotLocate forState:UIControlStateNormal];
    }
    _isMapViewRegionChangedFromTableView = YES;
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(point.location.latitude, point.location.longitude);
    [_mapView setCenterCoordinate:location animated:YES];
}

- (void)setSendButtonEnabledAfterLoadFinished {
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

#pragma mark - SearchResultTableVCDelegate
- (void)setSelectedLocationWithLocation:(AMapPOI *)poi {
    [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(poi.location.latitude,poi.location.longitude) animated:NO];
    _searchController.searchBar.text = @"";
}

#pragma mark - Action
- (void)actionLocation {
    [_mapView setCenterCoordinate:_mapView.userLocation.coordinate animated:YES];
}

- (void)sendLocation {
//    //添加Marker标记
//    LocationDetailVC *locationDetailVC = [[LocationDetailVC alloc] initWithLatitude:_tableView.selectedPoi.location.latitude longitude:_tableView.selectedPoi.location.longitude title:_tableView.selectedPoi.name position:_tableView.selectedPoi.address];
//    [self.navigationController pushViewController:locationDetailVC animated:YES];
}

// 搜索中心点坐标周围的POI-AMapGeoPoint
- (void)searchPoiByAMapGeoPoint:(AMapGeoPoint *)location {
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    request.location = location;
    // 搜索半径
    request.radius = 1000;
    // 搜索结果排序
    request.sortrule = 1;
    // 当前页数
    request.page = searchPage;
    [_searchAPI AMapPOIAroundSearch:request];
}

// 搜索逆向地理编码-AMapGeoPoint
- (void)searchReGeocodeWithAMapGeoPoint:(AMapGeoPoint *)location {
    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    regeo.location = location;
    // 返回扩展信息
    regeo.requireExtension = YES;
    [_searchAPI AMapReGoecodeSearch:regeo];
}

#pragma mark - 网络环境监听
- (void)reachabilityChanged:(NSNotification *)note{
    // 通过通知对象获取被监听的Reachability对象
    Reachability *curReach = [note object];
    // 获取Reachability对象的网络状态
    NetworkStatus status = [curReach currentReachabilityStatus];
    if (status == ReachableViaWWAN || status == ReachableViaWiFi){
        NSLog(@"Reachable");
        if (isFirstLocated) {
            AMapGeoPoint *point = [AMapGeoPoint locationWithLatitude:_mapView.centerCoordinate.latitude longitude:_mapView.centerCoordinate.longitude];
            [self searchReGeocodeWithAMapGeoPoint:point];
            [self searchPoiByAMapGeoPoint:point];
            searchPage = 1;
        }
    }
    else if (status == NotReachable){
        NSLog(@"notReachable");
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
}

- (void)searchAction {
//    _searchController.searchBar.frame = CGRectMake(0, kNavigationH, kScreenW, KSearchBarHeight);
//    [self.navigationController.navigationBar addSubview:_searchController.searchBar];
//    _searchController.searchBar.showsCancelButton = YES;
//    _searchController.hidesNavigationBarDuringPresentation = NO;
}

- (UIButton *)sureButton {
    if(!_sureButton) {
        _sureButton = [UIButton creatButtonWithTitle:@"确定" textColor:[UIColor whiteColor] font:midFont(16) target:self action:@selector(sureButtonOperation)];
        _sureButton.backgroundColor = HexRGB(0x418CF6);
        _sureButton.layer.cornerRadius = kSizeScale(22);
        _sureButton.clipsToBounds = YES;
    }
    return _sureButton;
}

- (void)sureButtonOperation {
    if ([self.delegate respondsToSelector:@selector(selectAddressPoi:)]) {
        [self.delegate selectAddressPoi:_tableView.selectedPoi];
    }
   [self.navigationController popViewControllerAnimated:YES];
}

@end
