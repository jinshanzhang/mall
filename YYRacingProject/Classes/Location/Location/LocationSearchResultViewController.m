//
//  LocationSearchResultViewController.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/18.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "LocationSearchResultViewController.h"
#import "HistoryAndCategorySearchHistroyViewP.h"
#import "HistoryAndCategorySearchCategoryViewP.h"
#import "LLSearchVCConst.h"
#import "YY_SearchResultViewController.h"

#import "LLSearchVCConst.h"
#import "YY_SearchResultViewController.h"

#import "HistoryAndCategorySearchHistroyViewP.h"
#import "HistoryAndCategorySearchCategoryViewP.h"

#import "MapPoiListCell.h"

#import <AMapSearchKit/AMapSearchKit.h>
#import "MJRefresh.h"

@interface LocationSearchResultViewController ()<UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate>
{
    NSString *_city;
    // 搜索key
    NSString *_searchString;
    // 搜索页数
    NSInteger searchPage;
    // 搜索API
    AMapSearchAPI *_searchAPI;
    // 搜索结果数组
    NSMutableArray *_searchResultArray;
    // 下拉更多请求数据的标记
    BOOL isFromMoreLoadRequest;
}

@property (nonatomic, strong) UITableView * tableView;

@end

@implementation LocationSearchResultViewController

- (void)viewDidLoad {
    [self xh_navBottomLine:XHClearColor];
    self.isOnlyShowHistoryView = YES;
    self.shopHistoryP = [[HistoryAndCategorySearchHistroyViewP alloc] initWithType:self.type];
    [super viewDidLoad];
    
    _searchAPI = [[AMapSearchAPI alloc] init];
    _searchAPI.delegate = self;
    
    _searchResultArray = [NSMutableArray array];
    self.view.backgroundColor = [UIColor whiteColor];
    [self beginSearch:^(NaviBarSearchType searchType, NBSSearchShopCategoryViewCellP *categorytagP, UILabel *historyTagLabel, UITextField *textField ,NSArray *arr)
    {
        if (textField.text||historyTagLabel.text){
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            if (kValidString(textField.text)) {
                [params setObject:textField.text forKey:@"url"];
            } else {
                if (historyTagLabel.text) {
                    [params setObject:historyTagLabel.text?:@"" forKey:@"url"];
                } else {
                    if (![self.searchTextField.placeholder isEqualToString:@"请输入关键字搜索"]&&!kValidString(textField.text)) {
                        [params setObject:textField.placeholder forKey:@"url"];
                    }else{
                        [params setObject:@"" forKey:@"url"];
                    }
                }
            }
            [params setObject:@(3) forKey:@"linkType"];
            [self.shopHistoryP saveSearchCache:textField.text?:historyTagLabel.text result:nil];
            self.searchTextField.text = textField.text?:historyTagLabel.text;
            
            _searchString = self.searchTextField.text;
            searchPage = 1;
            [self searchPoiBySearchString:_searchString];
        }
    }];
    
    self.setUpCustomViewBlock = ^{
        [self setUpTableView];
    };
}

- (void)setUpTableView {
    // 解决tableview无法正常显示的问题
    self.edgesForExtendedLayout = UIRectEdgeBottom;
//    self.tableView.frame = CGRectMake(0, kNavigationH, kScreenW, kScreenH - kNavigationH);
    self.tableView.hidden = YES;
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_footer.hidden = YES;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(kNavigationH);
        make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        make.left.right.equalTo(self.view);
    }];
    [self.tableView reloadData];
}


#pragma mark - AMapSearchDelegate
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response {
    // 判断是否从更多拉取
    if (isFromMoreLoadRequest) {
        isFromMoreLoadRequest = NO;
    } else {
        [_searchResultArray removeAllObjects];
        // 刷新后TableView返回顶部
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
    // 刷新完成,没有数据时不显示footer
    if (response.pois.count == 0) {
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
    else {
        self.tableView.mj_footer.state = MJRefreshStateIdle;
        // 添加数据并刷新TableView
        [response.pois enumerateObjectsUsingBlock:^(AMapPOI *obj, NSUInteger idx, BOOL *stop) {
            [_searchResultArray addObject:obj];
        }];
    }
    if(_searchResultArray.count <= 0) {
        [YYCommonTools showTipMessage:@"没有查询到相关地址信息"];
    }
    self.tableView.mj_footer.hidden = !(_searchResultArray.count > 0);
    self.tableView.hidden = !(_searchResultArray.count > 0);
    [self.tableView reloadData];
}

#pragma mark - Action
- (void)searchPoiBySearchString:(NSString *)searchString {
    //POI关键字搜索
    AMapPOIKeywordsSearchRequest *request = [[AMapPOIKeywordsSearchRequest alloc] init];
    request.keywords = searchString;
    request.city = _city;
    request.cityLimit = YES;
    request.page = searchPage;
    [_searchAPI AMapPOIKeywordsSearch:request];
}

- (void)loadMoreData
{
    searchPage++;
    isFromMoreLoadRequest = YES;
    [self searchPoiBySearchString:_searchString];
}

-(void)dealloc {
    YYLog(@"HistoryAndCategorySearchVC 页面销毁");
}


- (UITableView *)tableView {
    if(!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = XHLightColor;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        if (@available(iOS 11, *)) {
            //目前如此
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
        }else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        if (@available(iOS 11.0, *)) {
           _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
        [_tableView registerClass:[MapPoiListCell class] forCellReuseIdentifier:@"MapPoiListCell"];
    }
    return _tableView;
}

#pragma mark----- UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    MapPoiListCell *cell = [tableView dequeueReusableCellWithIdentifier:[MapPoiListCell description]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    AMapPOI *point = [_searchResultArray objectAtIndex:indexPath.row];
    cell.addressLabel.text = point.name;
    cell.detailAddressLabel.text = point.address;
    cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _searchResultArray.count;
}

#pragma mark----- UITableViewDelagate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kSizeScale(74);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return  0.00001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return  0.00001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(setSelectedLocationWithLocation:)]) {
        [self.delegate setSelectedLocationWithLocation:_searchResultArray[indexPath.row]];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setSearchCity:(NSString *)city {
    _city = city;
}

@end
