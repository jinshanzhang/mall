//
//  LocationSearchResultViewController.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/18.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "LLNaviSearchBaseVC.h"
#import "KeyWordModel.h"
NS_ASSUME_NONNULL_BEGIN

@class AMapPOI;

@protocol SearchResultTableVCDelegate <NSObject>

- (void)setSelectedLocationWithLocation:(AMapPOI *)poi;

@end

@interface LocationSearchResultViewController : LLNaviSearchBaseVC

- (void)setSearchCity:(NSString *)city;
@property (nonatomic, weak) id<SearchResultTableVCDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
