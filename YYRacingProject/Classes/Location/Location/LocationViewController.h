//
//  MainViewController.h
//  GDMapPlaceAroundDemo
//
//  Created by Mr.JJ on 16/6/14.
//  Copyright © 2016年 Mr.JJ. All rights reserved.
//

#import "YYBaseViewController.h"
@class AMapPOI;

@protocol SelectAddressResultDelegate <NSObject>

- (void)selectAddressPoi:(AMapPOI *)poi;

@end

@interface LocationViewController : YYBaseViewController

@property (nonatomic, weak) id<SelectAddressResultDelegate> delegate;

@end
