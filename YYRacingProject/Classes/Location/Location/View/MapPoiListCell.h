//
//  MapPoiListCell.h
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/18.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MapPoiListCell : BaseTableViewCell

@property (nonatomic, strong) UIImageView * locationImageView;
@property (nonatomic, strong) UILabel * addressLabel;
@property (nonatomic, strong) UIImageView * selectImageView;
@property (nonatomic, strong) UILabel * detailAddressLabel;

@end

NS_ASSUME_NONNULL_END
