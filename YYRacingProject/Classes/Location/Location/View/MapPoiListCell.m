//
//  MapPoiListCell.m
//  YYRacingProject
//
//  Created by 张金山 on 2020/9/18.
//  Copyright © 2020 cjm. All rights reserved.
//

#import "MapPoiListCell.h"

@interface MapPoiListCell()

@property (nonatomic, strong) UIView * lineView;

@end

@implementation MapPoiListCell

- (void)js_createSubViews {
    self.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.locationImageView];
    [self addSubview:self.addressLabel];
    [self addSubview:self.selectImageView];
    [self addSubview:self.detailAddressLabel];
    [self addSubview:self.lineView];
}

- (void)js_layoutSubViews {
    [self.locationImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(20));
        make.top.equalTo(self).offset(kSizeScale(20));
        make.width.mas_equalTo(kSizeScale(10));
        make.height.mas_equalTo(kSizeScale(12));
    }];
    
    [self.addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(20));
        make.right.equalTo(self).offset(-kSizeScale(50));
        make.centerY.equalTo(self.locationImageView);
        make.height.mas_equalTo(kSizeScale(20));
    }];
    
    [self.detailAddressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(kSizeScale(20));
        make.right.equalTo(self).offset(-kSizeScale(50));
        make.top.equalTo(self.addressLabel.mas_bottom).offset(kSizeScale(2));
//        make.height.mas_equalTo(kSizeScale(20));
    }];
    
    [self.selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-kSizeScale(12));
        make.centerY.equalTo(self);
        make.width.mas_equalTo(kSizeScale(18));
        make.height.mas_equalTo(kSizeScale(18));
   }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.detailAddressLabel.mas_bottom).offset(kSizeScale(16));
        make.bottom.equalTo(self);
        make.left.equalTo(self.addressLabel);
        make.right.equalTo(self).offset(- kSizeScale(10));
        make.size.mas_equalTo(kSizeScale(0.5));
    }];
}

- (UIImageView *)locationImageView {
    if(!_locationImageView) {
        _locationImageView = [[UIImageView alloc] init];
        _locationImageView.image = [UIImage imageNamed:@"icon_shouye_dingwe"];
        _locationImageView.hidden = YES;
    }
    return _locationImageView;
}

- (UILabel *)addressLabel {
    if(!_addressLabel) {
        _addressLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHTimeBlackColor textAlignment:NSTextAlignmentLeft font:midFont(14)];
    }
    return _addressLabel;
}

- (UILabel *)detailAddressLabel {
    if(!_detailAddressLabel) {
        _detailAddressLabel = [UILabel creatLabelWithTitle:@"--" textColor:XHBlackLitColor textAlignment:NSTextAlignmentLeft font:midFont(14)];
        _detailAddressLabel.numberOfLines = 1;
    }
    return _detailAddressLabel;
}

- (UIImageView *)selectImageView {
    if(!_selectImageView) {
        _selectImageView = [[UIImageView alloc] init];
        _selectImageView.image = [UIImage imageNamed:@"icon_sousuo_yixuan"];
        _selectImageView.hidden = YES;
    }
    return _selectImageView;
}

- (UIView *)lineView {
    if(!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = HexRGB(0xF0F0F0);
    }
    return _lineView;
}


@end

