//
//  AppDelegate+Monitor.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/9.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Monitor)

- (void)monitorApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
