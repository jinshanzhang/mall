//
//  yyShareManagerClass.h
//  YIYanProject
//
//  Created by cjm on 2018/4/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <UMShare/UMShare.h>

#define kShareManager    [yyShareManagerClass sharedyyShareManagerClass]
typedef void(^ShareCallBack)(BOOL status);

#pragma mark - Protocol (分享协议)
@protocol YYShareProtocol <NSObject>

@property (nonatomic, assign, readonly) YYShareOperatorType operatorType; //操作类型
@property (nonatomic, assign, readonly) BOOL   isFriendShareLink; // 是否开启好友分享链接
@property (nonatomic, assign, readonly) BOOL   isApply; //是否是应用内
@property (nonatomic, assign, readonly) BOOL   isApplet; //是否是小程序
@property (nonatomic, copy, readonly) NSString *title; //标题
@property (nonatomic, copy, readonly) NSString *detailTitle; //内容
@property (nonatomic, copy, readonly) id thumbImage;   //缩略图， 可UIImage, NSData,image_url
@property (nonatomic, copy, readonly) id shareImage;   //分享图
@property (nonatomic, copy, readonly) NSString *path; //小程序页面
@property (nonatomic, copy, readonly) NSString *webpageUrl; //链接
@property (nonatomic, strong, readonly) NSMutableArray *shareImageArray; //分享多图 可UIImage、NSData, 单图可image_url NSString集合
//@property (nonatomic, strong, readonly) NSMutableArray *saveImageArray; //保存图片 用UIImage  暂时废弃

@end

#pragma mark - Model (分享model)
@interface YYShareCombineInfoModel : NSObject <YYShareProtocol>
// 好友分享图片，好友分享URL， 朋友圈分享图片， 朋友圈分享链接
@property (nonatomic, assign) YYShareOperatorType operatorType; //操作类型
@property (nonatomic, assign) BOOL isFriendShareLink; // 是否开启好友分享链接
@property (nonatomic, assign) BOOL   isApplet; //是否是小程序
@property (nonatomic, assign) BOOL   isApply; //是否是应用内
@property (nonatomic, copy) NSString *title; //标题
@property (nonatomic, copy) NSString *detailTitle; //内容
@property (nonatomic, copy) id thumbImage;   //缩略图， 可UIImage, NSData,image_url
@property (nonatomic, copy) id shareImage;   //分享图
@property (nonatomic, copy) NSString *path; //小程序页面
@property (nonatomic, copy) NSString *webpageUrl; //链接
@property (nonatomic, strong) NSMutableArray *shareImageArray; //分享多图 可UIImage、NSData, 单图可image_url NSString集合

@end

@interface yyShareManagerClass : NSObject

WMSingletonH(yyShareManagerClass);

/**
 * 分享链接
 */
- (void)umengShareModel:(id <YYShareProtocol>)shareModel  callBack:(ShareCallBack)callBack;

/**
 * 分享链接
 * thumImage 可以是image , url , nsdata
 */
- (void)shareWebUrl:(NSString *)webUrl
               desc:(NSString *)desc
              title:(NSString *)title
          thumImage:(id)thumImage
           platType:(YYSharePlatType)platType
          isSuccess:(void(^)(BOOL))isSuccess;

@end
