//
//  yyEnvConfigManager.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define kEnvConfig  [yyEnvConfigManager sharedyyEnvConfigManager]
@interface yyEnvConfigManager : NSObject
WMSingletonH(yyEnvConfigManager);

- (NSString *)baserUrl;
- (NSString *)webDomin;
- (NSString *)signKey;

- (NSString *)inviteFans;       //邀请粉丝
- (NSString *)fanShop;          //粉丝版店主邀请粉丝开店
- (NSString *)hostShop;         //店主版店主邀请粉丝开店

@end

NS_ASSUME_NONNULL_END
