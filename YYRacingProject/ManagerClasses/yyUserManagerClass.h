//
//  yyUserManagerClass.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kUserManager    [yyUserManagerClass sharedyyUserManagerClass]
#define kUserInfo       kUserManager.currentUserInfo
#define kStoreInfo      kUserManager.currentStoreInfo
#define kIsLogin        kUserManager.isLogin
#define kIsTest         kUserManager.isTest
#define kIsTourist      kUserManager.isTourist
#define kIsFan          kUserManager.isFan
#define kIsMember       kUserManager.isMember


@interface yyUserManagerClass : NSObject

WMSingletonH(yyUserManagerClass);

@property (nonatomic, strong) YYUserInfoModel *currentUserInfo;   //当前用户信息
@property (nonatomic, strong) StoreInfoModel  *currentStoreInfo;  //当前店铺信息
@property (nonatomic, assign) BOOL   isLogin;   //是否登录
@property (nonatomic, assign) BOOL   isFan;     //是粉丝
@property (nonatomic, assign) BOOL   isMember;  //是会员
@property (nonatomic, assign) BOOL   isTest;    //是否测试账号
@property (nonatomic, assign) BOOL   isTourist;    //是否测试账号


- (BOOL)modifyUserInfo:(NSMutableDictionary *)userDict;
- (BOOL)modifyStoreInfo:(NSMutableDictionary *)userDict;

- (BOOL)modifyNewCouponInfo:(NSMutableDictionary *)couponInfo;
- (BOOL)modifyTabbarMenuInfo:(NSMutableArray *)tabbarMenus;
- (BOOL)modifyCommonSources:(NSMutableArray *)commonSources;

- (YYUserInfoModel *)loadUserInfo;
- (StoreInfoModel *)loadStoreInfo;

- (NSMutableArray *)loadTabbarMenuInfo;
- (NSMutableArray *)loadCommonSources;
- (NSMutableDictionary *)loadNewCouponInfo;

- (void)clearUserData;

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double longitude;

@end
