//
//  YYPhotoDownloadCacheManager.h
//  OCTheme
//
//  Created by 蔡金明 on 2019/4/2.
//  Copyright © 2019 蔡金明. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kPhotoCacheConfig          yyPhotoDownloadCacheManager

NS_ASSUME_NONNULL_BEGIN

@interface yyPhotoDownloadCacheManager : NSObject
/**
 *  创建目录文件
 */
+ (NSString *)createDirectoryFileExistsAtPath:(NSString *)path
                                      isClear:(BOOL)isClear;

/**
 *  创建文件路径
 */
+ (NSString *)createIconPath:(NSString *)iconName iconIndex:(NSInteger)iconIndex
                      status:(NSString *)status isAddSuffix:(BOOL)isAddSuffix;

/**
 *  资源内容下载
 */
+ (void)downloadPhotoSources:(NSMutableArray *)photoSources directory:(NSString *)directory
                    iconName:(NSString *)iconName  isAddSuffix:(BOOL)isAddSuffix
                   callBlock:(void(^)(BOOL isSuccess))callBlock;

/**
 *  图片资源读取
 */
+ (UIImage *)readImageSource:(NSString *)imageName
                   directory:(NSString *)directory;
@end

NS_ASSUME_NONNULL_END
