//
//  CountDownManager.m
//  CellCountDown
//
//  Created by herobin on 16/9/11.
//  Copyright © 2016年 herobin. All rights reserved.
//

#import "yyCountDownManager.h"
#import <UIKit/UIKit.h>

@interface YYTimeInterval ()

@property (nonatomic, assign) double timeInterval;

+ (instancetype)timeInterval:(double)timeInterval;

@end



@interface yyCountDownManager ()

@property (nonatomic, strong) NSTimer *timer;

/// 时间差字典(单位:毫秒)(使用字典来存放, 支持多列表或多页面使用)
@property (nonatomic, strong) NSMutableDictionary<NSString *, YYTimeInterval *> *timeIntervalDict;

/// 后台模式使用, 记录进入后台的绝对时间
@property (nonatomic, assign) double intervalTime;   //时间间隔
@property (nonatomic, assign) BOOL backgroudRecord;
@property (nonatomic, assign) CFAbsoluteTime lastTime;

@end

@implementation yyCountDownManager

+ (instancetype)manager {
    static yyCountDownManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[yyCountDownManager alloc]init];
    });
    return manager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.intervalTime = 1.0;
        // 将要进入后台
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackgroundNotification) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        // 将要进入前台
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForegroundNotification) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    return self;
}

- (void)start {
    // 启动定时器
    [self timer];
}

- (void)reload {
    // 刷新只要让时间差为0即可
    _timeInterval = 0.0f;
}

- (void)setCountDownTimeInterval:(float)time {
    self.intervalTime = time;
}

- (void)invalidate {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)timerAction {
    // 定时器每次加1
    [self timerActionWithTimeInterval:self.intervalTime];
}

- (void)timerActionWithTimeInterval:(double)timeInterval {
    // 时间差+
    self.timeInterval += timeInterval;
    [self.timeIntervalDict enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, YYTimeInterval * _Nonnull obj, BOOL * _Nonnull stop) {
        YYTimeInterval *y_timer = (YYTimeInterval *)obj;
        if (timeInterval < 1) {
            y_timer.timeInterval += timeInterval*1000;
        }
        else {
            y_timer.timeInterval += timeInterval;
        }
    }];
    // 发出通知
    [[NSNotificationCenter defaultCenter] postNotificationName:YYCountDownNotification object:nil userInfo:nil];
}

- (void)addSourceWithIdentifier:(NSString *)identifier {
    YYTimeInterval *timeInterval = self.timeIntervalDict[identifier];
    if (timeInterval) {
        timeInterval.timeInterval = 0.0f;
    }else {
        [self.timeIntervalDict setObject:[YYTimeInterval timeInterval:0.0f] forKey:identifier];
    }
}

- (double)timeIntervalWithIdentifier:(NSString *)identifier {
    return self.timeIntervalDict[identifier].timeInterval;
}

- (void)reloadSourceWithIdentifier:(NSString *)identifier {
    self.timeIntervalDict[identifier].timeInterval = 0.0f;
}

- (void)reloadAllSource {
    [self.timeIntervalDict enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, YYTimeInterval * _Nonnull obj, BOOL * _Nonnull stop) {
        obj.timeInterval = 0.0f;
    }];
}

- (void)removeSourceWithIdentifier:(NSString *)identifier {
    [self.timeIntervalDict removeObjectForKey:identifier];
}

- (void)removeAllSource {
    [self.timeIntervalDict removeAllObjects];
}

- (void)applicationDidEnterBackgroundNotification {
    self.backgroudRecord = (_timer != nil);
    if (self.backgroudRecord) {
        self.lastTime = CFAbsoluteTimeGetCurrent();
        [self invalidate];
    }
}

- (void)applicationWillEnterForegroundNotification {
    if (self.backgroudRecord) {
        CFAbsoluteTime timeInterval = CFAbsoluteTimeGetCurrent() - self.lastTime;
        // 取整
        double intervalValue = 0.0;
        if (self.intervalTime < 1) {
            intervalValue = timeInterval * 1000;
        }
        [self timerActionWithTimeInterval:(NSInteger)intervalValue];
        [self start];
    }
}

- (NSTimer *)timer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:self.intervalTime target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return _timer;
}

- (NSMutableDictionary *)timeIntervalDict {
    if (!_timeIntervalDict) {
        _timeIntervalDict = [NSMutableDictionary dictionary];
    }
    return _timeIntervalDict;
}

NSString *const YYCountDownNotification = @"YYCountDownNotification";

@end


@implementation YYTimeInterval

+ (instancetype)timeInterval:(double)timeInterval {
    YYTimeInterval *object = [YYTimeInterval new];
    object.timeInterval = timeInterval;
    return object;
}

@end
