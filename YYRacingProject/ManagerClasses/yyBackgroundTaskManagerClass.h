//
//  yyCountDownManagerClass.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YYCountDownTask;
NS_ASSUME_NONNULL_BEGIN
@interface yyBackgroundTaskManagerClass : NSObject

+ (instancetype)sharedCountDownManager;

/**创建任务**/
- (void)scheduledCountDownWithKey:(NSString *)key
                     timeInterval:(NSTimeInterval)timeInterval
                      coutingDown:(void (^) (NSTimeInterval))countingDown
                  countDownFinish:(void (^) (NSTimeInterval))countDownFinish;
/**检测任务key 是否存储**/
- (BOOL)countDownTaskIsExistWithKey:(NSString *)key
                               task:(YYCountDownTask *__autoreleasing  _Nullable *)task;
@end

NS_ASSUME_NONNULL_END
