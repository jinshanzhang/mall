//
//  yyEnvConfigManager.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/1/4.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "yyEnvConfigManager.h"

@implementation yyEnvConfigManager
WMSingletonM(yyEnvConfigManager);

- (NSString *)baserUrl {
    NSString *url = @"";
#ifdef DEBUG
    //debug
    // 线上环境
    url = @"http://api.sszj888.com/";
#else
   //release
   // 线上环境
   url = @"http://api.sszj888.com/";
#endif
    return url;
}

- (NSString *)webDomin {
    NSString *webMomin = @"";
    webMomin = @"http://m.sszj888.com/";
    return webMomin;
}

- (NSString *)signKey {
    return @"Ni4uPUvd9N3wEYXSD5OZq5qeyAjRFYIQa4QwqiKp0dodfg9N9q0B3jm46neyTgL6";
}

//邀请粉丝
- (NSString *)inviteFans {
    return [NSString stringWithFormat:@"%@#/inviteFans?promotionCode=%@",kEnvConfig.webDomin,kUserInfo.promotionCode];
}

//粉丝版店主邀请粉丝开店
- (NSString *)fanShop {
    return [NSString stringWithFormat:@"%@#/setupshop/shopDetail?type=1&promotionCode=%@",kEnvConfig.webDomin,kUserInfo.promotionCode];
}

//店主版店主邀请粉丝开店
- (NSString *)hostShop {
    return [NSString stringWithFormat:@"%@#/setupshop/record?type=0&promotionCode=%@",kEnvConfig.webDomin,kUserInfo.promotionCode];
}

@end
