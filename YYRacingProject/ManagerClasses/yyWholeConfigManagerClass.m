//
//  yyWholeConfigManagerClass.m
//  YIYanProject
//
//  Created by cjm on 2018/4/21.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "yyWholeConfigManagerClass.h"

#import <SDWebImage/SDImageCache.h>
#import "YYTabbarItemInfoModel.h"
#import "HomeBannerInfoModel.h"
#import "LessonDetailModel.h"

#import "PreOrderRequestAPI.h"
#import "UpdateCartRequestAPI.h"

#import "YYBindPromotionRequestAPI.h"
#import "MineCenterInfoRequestAPI.h"
#import "YYStorePageInfoRequestAPI.h"
#import "CartSkuTotalCountRequestAPI.h"
#import "CouponHomePopInfoRequestAPI.h"
#import "YYLocationRecommendAPI.h"
#import "YYShareGoodQRCodeInfoRequestAPI.h"

#import "MineOrderDeleteInfoRequestAPI.h"
#import "MineOrderCancelInfoRequestAPI.h"
#import "MineOrderConfirmInfoRequestAPI.h"
#import "YYStoreOrderDeleteInfoRequestAPI.h"

#import "YYGoodDetailCsdInfoRequestAPI.h"
#import "YYGoodDetailCsdFeedBackInfoRequestAPI.h"

#import "YYTabbarMenuInfoRequestAPI.h"
#import "YY_MinePushTokenAPI.h"
#import "YYNoteLinksSkipInfoRequestAPI.h"
#import "YYPasswordCheckInfoRequestAPI.h"
#import "YYCommonResourceInfoRequestAPI.h"
#import "CoursesPlayAuthInfoRequestAPI.h"
#import "CoursesPlayVideoInfoRequestAPI.h"
#import "GoodDetailCourseListInfoRequestAPI.h"

#import "YYHomeSubListSourcePointInfoRequestAPI.h"

@implementation yyWholeConfigManagerClass

WMSingletonM(yyWholeConfigManagerClass);

#pragma mark - Request
/**获取购物车数量**/
- (void)gainGoodSkuTotalCountRequest:(void(^)(NSInteger ))skuTotalCount {
    CartSkuTotalCountRequestAPI *totalAPI = [[CartSkuTotalCountRequestAPI alloc] initGoodSkuTotalCountRequest:[[NSDictionary dictionary] mutableCopy]];
    [totalAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSInteger totalCount = [[responDict objectForKey:@"cartSkuCnt"] integerValue];
            if (skuTotalCount) {
                skuTotalCount(totalCount);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**加sku到购物车**/
- (void)addGoodSkuToShippingCart:(NSDictionary *)params
                   success:(void(^)(BOOL))success {
    UpdateCartRequestAPI *addCartAPI = [[UpdateCartRequestAPI alloc] initUpdateCarRequest:[params mutableCopy]];
    [addCartAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSInteger totalCount = [[responDict objectForKey:@"cartSkuCnt"] integerValue];
            [RJBadgeController setBadgeForKeyPath:kShippingSubDetailRedCountPath count:totalCount];
            if (success) {
                success(YES);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (success) {
            success(NO);
        }
    }];
}


/**根据商品ID获取课程内容**/
- (void)getLessonWithGoodID:(int)appItemId
                         block:(void (^)(NSMutableArray *))courceModelBlock
                     failBlock:(void (^)(NSMutableArray *))failBlock
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(appItemId) forKey:@"appItemId"];
    [YYCenterLoading showCenterLoading];
    GoodDetailCourseListInfoRequestAPI *api = [[GoodDetailCourseListInfoRequestAPI alloc] initCourseListRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            NSMutableArray *courceArr = [[NSArray modelArrayWithClass:[LessonDetailModel class] json:[responDict objectForKey:@"lessons"]] mutableCopy];
            courceModelBlock(courceArr);
        }else
        {
            failBlock(nil);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

/**sku结算操作**/
- (void)submitOrderWithSkulist:(NSMutableArray *)skulist
                         souce:(int)source
                         addID:(int)addID
                 previewSwitch:(int)previewSwitch
                      couponId:(NSArray *)couponId
                       payment:(NSString *)payment
                  deliveryType:(NSString *)deliveryType
                         block:(void (^)(PreOrderModel *))preModelBlock
                         failBlock:(void (^)(PreOrderModel *))failBlock
 {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:skulist forKey:@"skuList"];
    [dict setObject:@(source) forKey:@"source"];
     if(payment) {
        [dict setObject:payment forKey:@"payment"];
     }
    [dict setObject:@(addID) forKey:@"addrId"];
    [dict setObject:@"" forKey:@"promotionCode"];
    [dict setObject:@(previewSwitch) forKey:@"previewSwitch"];
    [dict setObject:couponId forKey:@"couponId"];
//     if(deliveryType) {
//         [dict setObject:deliveryType forKey:@"deliveryType"];
//     }
    [YYCenterLoading showCenterLoading];
    PreOrderRequestAPI *api = [[PreOrderRequestAPI alloc] initPreOrderRequest:dict];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            PreOrderModel *model = [PreOrderModel modelWithJSON:responDict];
            preModelBlock(model);
        }else
        {
            NSLog(@"获取到的错误码是:%ld %@",api.responseStatusCode,api.responseString);
            NSDictionary *dataDict = api.responseObject;
            [YYCommonTools showTipMessage:[dataDict objectForKey:@"msg"]];
            failBlock(nil);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

/**获取用户信息**/
- (void)gainUserCenterInfo:(void(^)(BOOL))success
                 isLoading:(BOOL)isLoading {
    MineCenterInfoRequestAPI *centerInfoAPI = [[MineCenterInfoRequestAPI alloc] initMineCenterInfoRequest:[[NSDictionary dictionary] mutableCopy]];
    if (isLoading) {
        [YYCenterLoading showCenterLoading];
    }
    [centerInfoAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (isLoading) {
            [YYCenterLoading hideCenterLoading];
        }
        if (kValidDictionary(responDict)) {
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:responDict];
            [params setObject:kUserInfo.useToken?:@"" forKey:@"useToken"];
            // 处理过期提醒操作。
            if ([params objectForKey:@"couponExpireSoonIconImg"]) {
                BOOL isTip = [YYCommonTools handlerCouponOvertimeTipOperator:kUserInfo.homeImgOvertime lastTipStatus:kUserInfo.isAllowHomeExpireTip];
                [params setObject:@(isTip) forKey:@"isAllowHomeExpireTip"];
            }
            else {
                [params setObject:@(NO) forKey:@"isAllowHomeExpireTip"];
            }
            if (kUserInfo.homeImgOvertime) {
                [params setObject:kUserInfo.homeImgOvertime forKey:@"homeImgOvertime"];
            }
            //测试个人中心过期
            if ([params objectForKey:@"couponExpireSoonUserImg"]) {
                BOOL isTip = [YYCommonTools handlerCouponOvertimeTipOperator:kUserInfo.userImgOvertime lastTipStatus:kUserInfo.isAllowMineExpireTip];
                [params setObject:@(isTip) forKey:@"isAllowMineExpireTip"];
            }
            else {
                [params setObject:@(NO) forKey:@"isAllowMineExpireTip"];
            }
            if (kUserInfo.userImgOvertime) {
                [params setObject:kUserInfo.userImgOvertime forKey:@"userImgOvertime"];
            }
            // 对13067836121 账号做特殊处理
            if ([params objectForKey:@"promotionCode"]) {
                NSString *code = [params objectForKey:@"promotionCode"];
                if ([code isEqualToString:@"858795"]) {
                    [params setObject:@(YES) forKey:@"isTestAccount"];
                }
                else {
                    [params setObject:@(NO) forKey:@"isTestAccount"];
                }
            // 对13067836121 账号做特殊游客处理
                if ([code isEqualToString:@"558691"]) {
                    [params setObject:@(YES) forKey:@"isTouristAccount"];
                }
            }
            [kUserManager modifyUserInfo:params];
            if (success) {
                success(YES);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (success) {
            success(NO);
        }
    }];
}
/**获取推送token**/
- (void)gainUserPushToken{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@(2) forKey:@"pipeline"];
    NSString *token = [kUserDefault objectForKey:@"deviceToken"];
    [dict setObject:token?:@"" forKey:@"deviceToken"];
    [dict setObject:@(0) forKey:@"appId"];
    YY_MinePushTokenAPI *API = [[YY_MinePushTokenAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

/**主动获取用户位置信息**/
- (void)locationRecommend:(NSString *)loc{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:loc?:@"" forKey:@"loc"];
    [dict setObject:kDeviceUUID forKey:@"deviceId"];
   
    YYLocationRecommendAPI *API = [[YYLocationRecommendAPI alloc] initRequest:dict];
    [API startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    }];
}

/**新人优惠券信息**/
- (void)newUserCouponRequest {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    CouponHomePopInfoRequestAPI *homePopAPI = [[CouponHomePopInfoRequestAPI alloc] initCouponHomePopInfoRequest:dict];
    [homePopAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            [kUserManager modifyNewCouponInfo:[responDict mutableCopy]];
        }
        [kUserDefault setObject:@(YES)
                        forKey:@"NewUser"];
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [kUserManager modifyNewCouponInfo:nil];
        [kUserDefault setObject:@(NO)
                         forKey:@"NewUser"];
    }];
}

/**双十一口令弹框**/
- (void)passwordCheckRequest {
    // 口令弹框
    [kUserDefault setObject:@(NO)
                     forKey:@"Command"];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    UIPasteboard *paste = [UIPasteboard generalPasteboard];
    NSString  *pasteContent = paste.string;
    [params setObject:@(1) forKey:@"activityId"];
    [kUserDefault setObject:@(NO)
                     forKey:@"Command"];
    if (!kValidString(pasteContent)) {
        return;
    }
    else {
        if ([pasteContent rangeOfString:@"bk"].location == NSNotFound) {
            return;
        }
        else {
            [kUserDefault setObject:@(YES)
                            forKey:@"Command"];
            [params setObject:pasteContent forKey:@"command"];
            YYPasswordCheckInfoRequestAPI *checkAPI = [[YYPasswordCheckInfoRequestAPI alloc] initPasswordCheckRequest:params];
            [checkAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
                NSDictionary *responDict = request.responseJSONObject;
                if (kValidDictionary(responDict)) {
                    // app 口令弹框
                    if ([responDict objectForKey:@"app"]) {
                        NSDictionary *appDict = [responDict objectForKey:@"app"];
                        if (kValidDictionary(appDict)) {
                            HomeBannerInfoModel *bannerModel = [[HomeBannerInfoModel alloc] init];
                            bannerModel.linkType = [[appDict objectForKey:@"linkType"] integerValue];
                            bannerModel.imageUrl = [appDict objectForKey:@"imageUrl"];
                            bannerModel.url = [appDict objectForKey:@"url"];
                            YYActivityPopView *popView = [YYActivityPopView initYYPopView:[[YYActivityPopView alloc] init]];
                            popView.params = [@{@"activityModel":bannerModel} mutableCopy];
                            @weakify(popView);
                            [popView showViewOfAnimateType:YYPopAnimateScale];
                            popView.operatorBlock = ^(NSInteger operatorType) {
                                @strongify(popView);
                                if (operatorType != 0) {
                                    //跳转
                                    NSMutableDictionary *params = [NSMutableDictionary dictionary];
                                    [params setObject:@(bannerModel.linkType) forKey:@"linkType"];
                                    if (kValidString(bannerModel.url)) {
                                        [params setObject:bannerModel.url forKey:@"url"];
                                    }
                                    [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];
                                    [popView showViewOfAnimateType:YYPopAnimateScale];
                                }
                                paste.string = @"";
                                [[NSUserDefaults standardUserDefaults] setObject:@(NO)
                                        forKey:@"Command"];
                            };
                        }
                    }
                }
            } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            }];
        }
    }
}

/**短信链接通用链跳转**/
- (void)noteLinkUniversalSkip:(NSMutableDictionary *)params {
    YYNoteLinksSkipInfoRequestAPI *linkAPI = [[YYNoteLinksSkipInfoRequestAPI alloc] initNoteLinkSkipInfoRequest:params];
    [linkAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            if ([responDict objectForKey:@"urlValue"]) {
                NSMutableDictionary *params = [NSMutableDictionary dictionary];
                NSDictionary *strTransJson = [NSString dictionaryWithJsonString:[responDict objectForKey:@"urlValue"]];
                if (kValidDictionary(strTransJson)) {
                    [params setObject:[strTransJson objectForKey:@"linkType"]?:@"" forKey:@"linkType"];
                    [params setObject:[strTransJson objectForKey:@"linkUrl"]?:@"" forKey:@"url"];
                    [YYCommonTools skipMultiCombinePage:(YYBaseViewController *)[YYCommonTools getCurrentVC] params:params];

                }
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**获取通用资源位**/
- (void)commonResourceRequest {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"15,16,17,18,22" forKey:@"types"];
    YYCommonResourceInfoRequestAPI *resourceAPI = [[YYCommonResourceInfoRequestAPI alloc] initCommonResourceRequest:dict];
    [resourceAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict) && [responDict objectForKey:@"items"]) {
            NSArray *tempItems = [responDict objectForKey:@"items"];
            if (kValidArray(tempItems)) {
                [kUserManager modifyCommonSources:[tempItems mutableCopy]];
            }
        }
        else {
            [kUserManager modifyCommonSources:nil];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**获取底部tabbar资源位**/
- (void)tabbarMenuIconRequest{
    __block BOOL isExistContents = YES;
    __block BOOL isDownLoadFinish = NO;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    //__block NSMutableArray *array = [kUserManager loadTabbarMenuInfo];
    // 创建文件的全路径
    NSString *tabbarPath = [YYCommonTools documentCustomPath:@"/tabbar"];
    isExistContents = [YYCommonTools fileBelowIsExistFiles:tabbarPath];
    YYTabbarMenuInfoRequestAPI *menuAPI = [[YYTabbarMenuInfoRequestAPI alloc] initTabbarMenuRequest:dict];
    [menuAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        int scale = 2;
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            NSNumber *currentVersion = nil;
            //NSNumber *lastVersion = [kUserDefault objectForKey:@"tabbarVersion"];
            if ([responDict objectForKey:@"version"]) {
                currentVersion = [responDict objectForKey:@"version"];
            }
            if ([responDict objectForKey:@"data"]) {
                // 接口正常，数据返回为空; 数据为空。
                NSString *msg = [responDict objectForKey:@"data"];
                if (kValidString(msg) && [msg isEqualToString:@"ok"]) {
                    [kUserManager modifyTabbarMenuInfo:nil];
                }
            }
            NSMutableArray *tabbarSources = [NSMutableArray array];
            if ([responDict objectForKey:@"bgImage"]) {
                //背景资源
                YYTabbarItemInfoModel *model= [[YYTabbarItemInfoModel alloc] init];
                model.index = @(-1);
                NSString *bgImageURL = [responDict objectForKey:@"bgImage"];
                if (kValidString(bgImageURL)) {
                    isDownLoadFinish = [YYCommonTools downLoadImages:[@[bgImageURL] mutableCopy]
                                                        photoPreName:@"bgImage"];
                }
                model.normalIconPath = [NSString stringWithFormat:@"bgImage-0@%dx.png",scale];
                [tabbarSources addObject:model];
            }
            if ([responDict objectForKey:@"buttons"]) {
                // tabbar menu选中，未选中图片
                NSArray *buttons = [responDict objectForKey:@"buttons"];
                NSMutableArray *selectArray = [NSMutableArray array];
                NSMutableArray *normalArray = [NSMutableArray array];
                for (int i = 0; i < buttons.count; i ++) {
                    YYTabbarItemInfoModel *model= [[YYTabbarItemInfoModel alloc] init];
                    NSDictionary *btnItem = [buttons objectAtIndex:i];
                    if ([btnItem objectForKey:@"index"]) {
                        model.index = [btnItem objectForKey:@"index"];
                    }
                    if ([btnItem objectForKey:@"click"]) {
                        model.title = [[btnItem objectForKey:@"click"] objectForKey:@"title"];
                        model.selectColor = [[btnItem objectForKey:@"click"] objectForKey:@"fontColor"];

                        NSString *iconImageURL = [[btnItem objectForKey:@"click"] objectForKey:@"iconImage"];
                        model.selectIcon = [[btnItem objectForKey:@"click"] objectForKey:@"iconImage"];
                        model.selectIconPath = [NSString stringWithFormat:@"select-%d@%dx.png", i, scale];
                        [selectArray addObject:iconImageURL];
                    }
                    if ([btnItem objectForKey:@"normal"]) {
                        model.title = [[btnItem objectForKey:@"normal"] objectForKey:@"title"];
                        model.normalColor = [[btnItem objectForKey:@"normal"] objectForKey:@"fontColor"];
                        
                        NSString *iconImageURL = [[btnItem objectForKey:@"normal"] objectForKey:@"iconImage"];
                        model.normalIcon = [[btnItem objectForKey:@"normal"] objectForKey:@"iconImage"];
                        model.normalIconPath = [NSString stringWithFormat:@"normal-%d@%dx.png", i, scale];
                        [normalArray addObject:iconImageURL];
                    }
                    [tabbarSources addObject:model];
                }
                isDownLoadFinish = [YYCommonTools downLoadImages:selectArray
                                                    photoPreName:@"select"];
                isDownLoadFinish = [YYCommonTools downLoadImages:normalArray
                                                    photoPreName:@"normal"];
            }
            [kUserManager modifyTabbarMenuInfo:nil];
            [kUserManager modifyTabbarMenuInfo:tabbarSources];
        }
        else {
            [kUserManager modifyTabbarMenuInfo:nil];
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**用户绑定请求**/
- (void)bindPromotionRequset:(NSString *)code
                       token:(NSString *)token {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:code forKey:@"promotionCode"];
    [dict setObject:token forKey:@"token"];
    
    [YYCenterLoading showCenterLoading];
    YYBindPromotionRequestAPI *loginAPI = [[YYBindPromotionRequestAPI alloc] initWithBindCodeRequest:dict];
    [loginAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request)
     {
         NSDictionary *responDict = request.responseJSONObject;
         [YYCenterLoading hideCenterLoading];
         if (kValidDictionary(responDict)) {
             NSMutableDictionary *params = [NSMutableDictionary dictionary];
             [params setObject:@(1) forKey:@"source"];
             if (kValidString(token)) {
                 [params setObject:token forKey:@"useToken"];
             }
             [YYCommonTools loginWithToken:params];
         }
     } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
     }];
}

/**获取店铺信息**/
- (void)storeDetailInfoRequest:(void(^)(BOOL))success isLoading:(BOOL)isLoading {
    YYStorePageInfoRequestAPI *storeAPI = [[YYStorePageInfoRequestAPI alloc] initStorePageInfoRequest:[[NSDictionary dictionary]mutableCopy]];
    if (isLoading) {
        [YYCenterLoading showCenterLoading];
    }
    [storeAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (isLoading) {
            [YYCenterLoading hideCenterLoading];
        }
        if (kValidDictionary(responDict)) {
            NSMutableArray *incomeArr = [NSMutableArray array];
            NSMutableArray *totalArr = [NSMutableArray array];
            
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:responDict];
            NSDictionary *income = [params objectForKey:@"income"];
            NSString  *todayOrderCnt = [income objectForKey:@"todayOrderCnt"]?:@"";
            NSString  *todayIncome = [income objectForKey:@"todayIncome"]?:@"";
            NSString  *currentMonthIncome = [income objectForKey:@"currentMonthIncome"]?:@"";
            
            NSString  *unBalanceIncome = [income objectForKey:@"unBalanceIncome"]?:@"";
            NSString  *balanceIncome = [income objectForKey:@"balanceIncome"]?:@"";
            NSString  *totalIncome = [income objectForKey:@"totalIncome"]?:@"";
            
            [incomeArr addObject:todayOrderCnt];
            [incomeArr addObject:todayIncome];
            [incomeArr addObject:currentMonthIncome];
            
            [totalArr addObject:unBalanceIncome];
            [totalArr addObject:balanceIncome];
            [totalArr addObject:totalIncome];
            
            [params setObject:incomeArr forKey:@"curEarnArrs"];
            [params setObject:totalArr forKey:@"totalEarnArrs"];

            // 将店铺店主信息写入本地
            [kUserManager modifyStoreInfo:params];
            if (success) {
                success(YES);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (success) {
            success(NO);
        }
    }];
}

/**订单取消操作**/
- (void)cancelOrderRequest:(NSString *)orderID
                   success:(void(^)(BOOL))success {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(orderID)) {
        [params setObject:orderID forKey:@"orderId"];
    }
    MineOrderCancelInfoRequestAPI *cancelAPI = [[MineOrderCancelInfoRequestAPI alloc] initOrderCancelInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [cancelAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            if (success) {
                success(YES);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (success) {
            success(NO);
        }
    }];
}


/**订单确认收货操作**/
- (void)confirmOrderRequest:(NSString *)orderID
                    success:(void(^)(BOOL))success {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(orderID)) {
        [params setObject:orderID forKey:@"orderId"];
    }
    MineOrderConfirmInfoRequestAPI *confirmAPI = [[MineOrderConfirmInfoRequestAPI alloc] initOrderConfirmInfoRequest:params];
    [YYCenterLoading showCenterLoading];
    [confirmAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        [YYCenterLoading hideCenterLoading];
        if (kValidDictionary(responDict)) {
            if (success) {
                success(YES);
            }
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        if (success) {
            success(NO);
        }
    }];
}

/**删除订单操作**/
- (void)deleteOrderRequest:(NSString *)orderID
                   isStore:(BOOL)isStore
                   success:(void(^)(BOOL))success {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (kValidString(orderID)) {
        [params setObject:orderID forKey:@"orderId"];
    }
    if (isStore) {
        YYStoreOrderDeleteInfoRequestAPI *deleteAPI = [[YYStoreOrderDeleteInfoRequestAPI alloc] initOrderDeleteInfoRequest:params];
        [YYCenterLoading showCenterLoading];
        [deleteAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
            [YYCenterLoading hideCenterLoading];
            if (kValidDictionary(responDict)) {
                if (success) {
                    success(YES);
                }
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            if (success) {
                success(NO);
            }
        }];
    }
    else {
        MineOrderDeleteInfoRequestAPI *deleteAPI = [[MineOrderDeleteInfoRequestAPI alloc] initOrderDeleteInfoRequest:params];
        [YYCenterLoading showCenterLoading];
        [deleteAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            NSDictionary *responDict = request.responseJSONObject;
            [YYCenterLoading hideCenterLoading];
            if (kValidDictionary(responDict)) {
                if (success) {
                    success(YES);
                }
            }
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            if (success) {
                success(NO);
            }
        }];
    }
}
/**生成商品二维码**/
- (void)produceGoodQRCodeRequest:(NSMutableDictionary *)params
                         success:(void(^)(NSDictionary *))success {
    YYShareGoodQRCodeInfoRequestAPI *qrCodeAPI = [[YYShareGoodQRCodeInfoRequestAPI alloc] initShareGoodQRCodeRequest:params];
    [qrCodeAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        success(responDict);
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        success(nil);
    }];
}


#pragma mark - Home
/**获取首页资源位**/
- (void)homeSourcePointRequest:(NSMutableDictionary *)params
                       success:(void(^)(NSDictionary *))success {
    YYHomeSubListSourcePointInfoRequestAPI *soureAPI = [[YYHomeSubListSourcePointInfoRequestAPI alloc] initHomeSubListSourceInfoRequest:params];
    [soureAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        success(responDict);
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        success(nil);
    }];
}

#pragma mark - CsdCircle
/**素材圈接口**/
- (void)gainGoodCsdCircleListRequest:(NSMutableDictionary *)params
                             success:(void(^)(NSDictionary *))success {
    YYGoodDetailCsdInfoRequestAPI *listAPI = [[YYGoodDetailCsdInfoRequestAPI alloc] initGoodDetailCsdInfoRequest:params];
    [listAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        success(responDict);
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        success(nil);
    }];
}

/**素材圈反馈接口**/
- (void)feedBackAboutCsdCircle:(NSMutableDictionary *)params
                       success:(void(^)(NSDictionary *))success {
    YYGoodDetailCsdFeedBackInfoRequestAPI *feedBacAPI = [[YYGoodDetailCsdFeedBackInfoRequestAPI alloc] initGoodDetailCsdFeedBackInfoRequest:params];
    [feedBacAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        success(responDict);
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        success(nil);
    }];
}


#pragma mark - Course
/**获取播放授权**/
- (void)gainCoursePlayAuth:(void(^)(NSDictionary *))success {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //视频
    [params setObject:@"56ee2609d7db4c63a76a51bdec806993" forKey:@"videoId"];
    CoursesPlayAuthInfoRequestAPI *authAPI = [[CoursesPlayAuthInfoRequestAPI alloc] initCoursePlayAuthInfoRequest:params];
    [authAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            success(responDict);
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        success(nil);
    }];
}

/****/
- (void)gainVideoPlayInfo {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"3d2efec6485d4f6f918de78757f4c2bd" forKey:@"videoId"];
    CoursesPlayVideoInfoRequestAPI *videoAPI = [[CoursesPlayVideoInfoRequestAPI alloc] initCoursePlayVideoInfoRequest:params];
    [videoAPI startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSDictionary *responDict = request.responseJSONObject;
        if (kValidDictionary(responDict)) {
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}


#pragma mark - Common Tool

/**计算缓存文件大小**/
- (CGFloat)calculateBufferSize {
    CGFloat folderSize = 0.0;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString  *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    // 查看路径是否存在
    if ([fileManager fileExistsAtPath:cachePath]) {
        NSArray *childFiles = [fileManager subpathsAtPath:cachePath];
        for(NSString *fileName in childFiles) {
            NSString *absolutePath = [cachePath stringByAppendingString:fileName];
            if ([fileManager fileExistsAtPath:absolutePath]) {
                NSDictionary *attr = [fileManager attributesOfItemAtPath:absolutePath
                                                                   error:nil];
                long long cacheSize = attr.fileSize;
                folderSize = folderSize + (cacheSize/1024.0/1024.0);
            }
        }
        folderSize += [[SDImageCache sharedImageCache] getSize]/1024.0/1024.0;
    }
    return folderSize;
}

/**清除成功**/
- (void)clearBufferSize {
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSArray *files = [[NSFileManager defaultManager ]subpathsAtPath:cachePath];
    for ( NSString * p in files) {
        NSError * error = nil ;
        //获取文件全路径
        NSString * fileAbsolutePath = [cachePath stringByAppendingPathComponent:p];
        if ([[NSFileManager defaultManager ] fileExistsAtPath :fileAbsolutePath]) {
            [[NSFileManager defaultManager ] removeItemAtPath :fileAbsolutePath error :&error];
        }
    }
}

@end
