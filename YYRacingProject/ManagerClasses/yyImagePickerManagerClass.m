//
//  yyImagePickerManagerClass.m
//  YIYanProject
//
//  Created by cjm on 2018/4/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "yyImagePickerManagerClass.h"
#import <AFNetworking/AFNetworking.h>

@implementation yyImagePickerManagerClass

+ (void)openImagePickerCtrl:(NSInteger)maxCount
                columnCount:(NSInteger)columnCount
                isAllowCrop:(BOOL)isAllowCrop
                   delegate:(id<TZImagePickerControllerDelegate>)delegate
                   viewCtrl:(YYBaseViewController *)viewCtrl
                    myBlock:(imageBlock)myBlock {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:maxCount columnNumber:4 delegate:delegate pushPhotoPickerVc:YES];
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    // 4. 照片排列按修改时间升序
    imagePickerVc.allowCrop = isAllowCrop;
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.alwaysEnableDoneBtn = YES;
    imagePickerVc.sortAscendingByModificationDate = NO;

    NSInteger left = 15;
    NSInteger widthHeight = kScreenW - 2 * left;
    NSInteger top = (kScreenH - widthHeight) / 2;
    imagePickerVc.cropRect = CGRectMake(left, top, widthHeight, widthHeight);
    
#pragma mark - 到这里为止
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        myBlock(photos);
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewCtrl presentViewController:imagePickerVc animated:YES completion:nil];
    });
}

@end
