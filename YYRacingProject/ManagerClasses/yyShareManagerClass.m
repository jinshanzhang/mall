//
//  yyShareManagerClass.m
//  YIYanProject
//
//  Created by cjm on 2018/4/20.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "yyShareManagerClass.h"

@interface YYShareCombineInfoModel()

@end

@implementation YYShareCombineInfoModel

@end

@interface yyShareManagerClass()

@end

@implementation yyShareManagerClass

WMSingletonM(yyShareManagerClass);

#pragma mark - Public
- (void)umengShareModel:(id<YYShareProtocol>)shareModel callBack:(ShareCallBack)callBack {
    if (!shareModel) {
        [YYCommonTools showTipMessage:@"分享参数不能为空"];
        return;
    }
    // 先消失加载框
    [YYCenterLoading hideCenterLoading];
    [YYCenterLoading hideCenterLoadingInWindow];
    [YYQRCodeCenterLoading hideCenterLoading:nil
                                   superView:nil];
    //shareModel.operatorType 0 微信好友 1 朋友圈 2 QQ好友  3 QQ空间  4 保存图片 5 复制链接
    UMSocialPlatformType tempPlatType;
    NSUInteger operatorType = shareModel.operatorType;
    if (operatorType <= 3) {
        if (operatorType == 0 || operatorType == 1) {
            // 微信
            if (operatorType == 0) {
                tempPlatType = UMSocialPlatformType_WechatSession;
            }
            else {
                tempPlatType = UMSocialPlatformType_WechatTimeLine;
            }
            if (![[UMSocialManager defaultManager] isInstall:tempPlatType]) {
                [YYCommonTools showTipMessage:@"微信未安装"];
                return;
            }
        }
        else {
            // QQ
            if (operatorType == 2) {
                tempPlatType = UMSocialPlatformType_QQ;
            }
            else {
                tempPlatType = UMSocialPlatformType_Qzone;
            }
            if (![[UMSocialManager defaultManager] isInstall:tempPlatType]) {
                [YYCommonTools  showTipMessage:@"QQ未安装"];
                return;
            }
        }
        if (shareModel.shareImageArray.count > 1 && operatorType == 0) {
            /**
             * 1. 如果存在多图， 且是分享给好友
             * 2. 需要先进行图片本地下载， 接着调用系统的分享
             */
            [self callBackSystemMethodShareMulti:shareModel.shareImageArray];
        }
        else {
            [self shareConfigureAndCarrayOperatore:tempPlatType
                                    configureModel:shareModel
                                          callBack:callBack];
        }
    }
    else {
        if (operatorType == 4) {
            // 保存图片
            if (kValidArray(shareModel.shareImageArray)) {
                [kSaveManager savePhotoReachAlbum:shareModel.shareImageArray];
            }
        }
        else if (operatorType == 5) {
            // 复制链接
            if (shareModel.webpageUrl) {
                [YYCommonTools pasteboardCopy:shareModel.webpageUrl
                                 appendParams:nil];
            }
        }
    }
}

#pragma mark - Private method
/**分享数据配置和执行操作**/
- (void)shareConfigureAndCarrayOperatore:(UMSocialPlatformType)platform
                          configureModel:(id <YYShareProtocol>)configureModel
                                callBack:(ShareCallBack)callBack {
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    if (configureModel.isFriendShareLink) {
        // 分享链接
        if (configureModel.isApply && platform == UMSocialPlatformType_WechatTimeLine) {
            UMShareImageObject *imageObject = [UMShareImageObject
                                               shareObjectWithTitle:configureModel.title
                                               descr:configureModel.detailTitle
                                               thumImage:configureModel.thumbImage];
            if (configureModel.shareImage) {
                imageObject.shareImage = configureModel.shareImage;
            }
            messageObject.shareObject = imageObject;
        }
        else {
            if (configureModel.isApplet && configureModel.path) {
                UMShareMiniProgramObject *appletObject = [UMShareMiniProgramObject
                                                          shareObjectWithTitle:configureModel.title descr:configureModel.detailTitle thumImage:configureModel.thumbImage];
                appletObject.webpageUrl = configureModel.webpageUrl;
                appletObject.path = configureModel.path;
                if (configureModel.thumbImage) {
                    appletObject.hdImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:configureModel.thumbImage]];
                }
                appletObject.userName = kApplet;
                
#ifdef DEBUG
// debug
#if DevelopMent == 1
                appletObject.miniProgramType =  UShareWXMiniProgramTypeRelease;
#elif DevelopMent == 2
                appletObject.miniProgramType =  UShareWXMiniProgramTypePreview;
#elif DevelopMent == 3
                appletObject.miniProgramType =  UShareWXMiniProgramTypePreview;
#else
                appletObject.miniProgramType =  UShareWXMiniProgramTypePreview;
#endif
                                
#else
// release
                appletObject.miniProgramType =  UShareWXMiniProgramTypeRelease;
#endif

                messageObject.shareObject = appletObject;
                
            }
            else {
                UMShareWebpageObject *webObject = [UMShareWebpageObject
                                                   shareObjectWithTitle:configureModel.title
                                                   descr:configureModel.detailTitle
                                                   thumImage:configureModel.thumbImage];
                webObject.webpageUrl = configureModel.webpageUrl;
                messageObject.shareObject = webObject;
            }
        }
    }
    else {
        // 分享图片
        if (configureModel.shareImage) {
            UMShareImageObject *imageObject = [UMShareImageObject
                                               shareObjectWithTitle:configureModel.title
                                               descr:configureModel.detailTitle
                                               thumImage:configureModel.thumbImage];
            if (configureModel.shareImage) {
                imageObject.shareImage = configureModel.shareImage;
            }
            messageObject.shareObject = imageObject;
        }
        else {
            // 文本
            messageObject.text = configureModel.title;
        }
    }

    
    [[UMSocialManager defaultManager] shareToPlatform:platform
                                        messageObject:messageObject
                                currentViewController:[YYCommonTools getCurrentVC]
                                           completion:^(id result, NSError *error) {
                                               if (error) {
                                                   YYLog(@"error=%@",error);
                                                   [YYCommonTools showTipMessage:@"分享失败"];
                                                   callBack(NO);
                                               }
                                               else {
                                                   [YYCommonTools showTipMessage:@"分享成功"];
                                                   callBack(YES);
                                               }
                                           }];
}


/**调用系统方法对微信好友分享多图**/
- (void)callBackSystemMethodShareMulti:(NSMutableArray *)imageArray {
    NSMutableArray *tempArray = imageArray;
    __block NSMutableArray *downPhotos = [NSMutableArray array];
    
    dispatch_group_t group =  dispatch_group_create();
    // 创建串行队列
    dispatch_queue_t queue = dispatch_queue_create("com.beike.shareDownPhotoQueue", DISPATCH_QUEUE_SERIAL);
    // 异步任务
    //tempArray = [[[tempArray reverseObjectEnumerator] allObjects] mutableCopy];
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        for (int i = 0; i < tempArray.count; i ++) {
            SDWebImageDownloader *manager = [SDWebImageDownloader sharedDownloader];
            manager.downloadTimeout = 10;
            [manager downloadImageWithURL:[NSURL URLWithString:tempArray[i]] options:SDWebImageDownloaderUseNSURLCache|SDWebImageDownloaderScaleDownLargeImages progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                if (image) {
                    [downPhotos addObject:image];
                }
                if (downPhotos.count >= tempArray.count) {
                    dispatch_group_leave(group);
                }
            }];
        }
    });
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:downPhotos applicationActivities:nil];
        if (@available(iOS 9.0, *)) {
            activityVC.excludedActivityTypes = @[UIActivityTypePostToFacebook,
                                                 UIActivityTypePostToTwitter,
                                                 UIActivityTypePostToWeibo,
                                                 UIActivityTypeMessage,
                                                 UIActivityTypeMail,
                                                 UIActivityTypePrint,
                                                 UIActivityTypeCopyToPasteboard,
                                                 UIActivityTypeAssignToContact,
                                                 UIActivityTypeSaveToCameraRoll,
                                                 UIActivityTypeAddToReadingList,
                                                 UIActivityTypePostToFlickr,
                                                 UIActivityTypePostToVimeo,
                                                 UIActivityTypePostToTencentWeibo,
                                                 UIActivityTypeAirDrop,
                                                 UIActivityTypeOpenInIBooks];
        } else {
            // Fallback on earlier versions
            activityVC.excludedActivityTypes = @[UIActivityTypePostToFacebook,
                                                 UIActivityTypePostToTwitter,
                                                 UIActivityTypePostToWeibo,
                                                 UIActivityTypeMessage,
                                                 UIActivityTypeMail,
                                                 UIActivityTypePrint,
                                                 UIActivityTypeCopyToPasteboard,
                                                 UIActivityTypeAssignToContact,
                                                 UIActivityTypeSaveToCameraRoll,
                                                 UIActivityTypeAddToReadingList,
                                                 UIActivityTypePostToFlickr,
                                                 UIActivityTypePostToVimeo,
                                                 UIActivityTypePostToTencentWeibo,
                                                 UIActivityTypeAirDrop];
        }
        [[YYCommonTools getCurrentVC] presentViewController:activityVC animated:YES completion:^{
            
        }];
    });
}


- (void)shareWebUrl:(NSString *)webUrl
               desc:(NSString *)desc
              title:(NSString *)title
          thumImage:(id)thumImage
           platType:(YYSharePlatType)platType isSuccess:(void(^)(BOOL))isSuccess {
    
    UMSocialPlatformType tempPlatType;
    
    if (platType == YYSharePlatWeChat || platType == YYSharePlatFriend) {
        if (platType == YYSharePlatWeChat) {
            tempPlatType = UMSocialPlatformType_WechatSession;
        }
        else {
            tempPlatType = UMSocialPlatformType_WechatTimeLine;
        }
        if (![[UMSocialManager defaultManager] isInstall:tempPlatType]) {
            [YYCommonTools showTipMessage:@"微信未安装"];
            return;
        }
    }
    else {
        if (platType == YYSharePlatQQ) {
            tempPlatType = UMSocialPlatformType_QQ;
        }
        else {
            tempPlatType = UMSocialPlatformType_Qzone;
        }
        if (![[UMSocialManager defaultManager] isInstall:tempPlatType]) {
            [YYCommonTools  showTipMessage:@"QQ未安装"];
            return;
        }
    }
    
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    if (kValidString(webUrl) && thumImage) {
        UMShareWebpageObject *webObject = [UMShareWebpageObject shareObjectWithTitle:title
                                                                               descr:desc
                                                                           thumImage:thumImage];
        webObject.webpageUrl = webUrl;
        messageObject.shareObject = webObject;
    }
    else if (thumImage){
        UMShareImageObject *imageObject = [UMShareImageObject shareObjectWithTitle:title
                                                                               descr:desc
                                                                           thumImage:thumImage];
        imageObject.shareImage = thumImage;
        messageObject.shareObject = imageObject;
    }
    else {
        UMSocialMessageObject *object = [UMSocialMessageObject messageObject];
        //设置文本
        object.text = title;
        messageObject = object;
    }
    //获取一下当前viewController
    [[UMSocialManager defaultManager] shareToPlatform:tempPlatType
                                        messageObject:messageObject
                                currentViewController:[YYCommonTools getCurrentVC]
                                           completion:^(id result, NSError *error) {
                                               if (error) {
                                                   isSuccess(NO);
                                                   [YYCommonTools showTipMessage:@"分享失败"];
                                               }
                                               else {
                                                    isSuccess(YES);
                                                    [YYCommonTools showTipMessage:@"分享成功"];
                                               }
                                           }];
}

@end
