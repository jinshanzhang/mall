//
//  yyCountDownManagerClass.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "yyBackgroundTaskManagerClass.h"
#import <UIKit/UIKit.h>

@interface YYCountDownTask: NSOperation

/** 计时进行回调 **/
@property (nonatomic, copy) void (^countingDownBlock)(NSTimeInterval timeInterval);

/** 计时结束回调 **/
@property (nonatomic, copy) void (^coutDownFinishBlock)(NSTimeInterval timeInterval);

/** 计时剩余时间 **/
@property (nonatomic, assign) NSTimeInterval  leftTimeInterval;

/** 后台任务标识，确保程序进入后台依然可以倒计时 **/
@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTaskIdentifier;

/** 线程名称 (NSOperation 的name 属性只在iOS 8+中存在) **/
@property (nonatomic, copy) NSString *operationName;

@end

@implementation  YYCountDownTask

- (void)main {
    self.backgroundTaskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    
    while (-- self.leftTimeInterval > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.countingDownBlock) {
                self.countingDownBlock(self.leftTimeInterval);
            }
        });
        // 一秒一秒进行倒计时
        [NSThread sleepForTimeInterval:1];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.coutDownFinishBlock) {
            self.coutDownFinishBlock(0);
        }
    });
    if (self.backgroundTaskIdentifier != UIBackgroundTaskInvalid) {
        // 判断任务是否结束,如果不等于；将任务结束了。
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTaskIdentifier];
        self.backgroundTaskIdentifier = UIBackgroundTaskInvalid;
    }
}

@end

@interface yyBackgroundTaskManagerClass()

/**创建线程池**/
@property (nonatomic, strong) NSOperationQueue  *pool;

@end

@implementation yyBackgroundTaskManagerClass

+ (instancetype)sharedCountDownManager {
    static yyBackgroundTaskManagerClass *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[yyBackgroundTaskManagerClass alloc] init];
        instance.pool = [[NSOperationQueue alloc] init];
    });
    return instance;
}

- (void)scheduledCountDownWithKey:(NSString *)key
                     timeInterval:(NSTimeInterval)timeInterval
                      coutingDown:(void (^) (NSTimeInterval))countingDown
                  countDownFinish:(void (^) (NSTimeInterval))countDownFinish {
    if (timeInterval > 180) {
        NSCAssert(NO, @"iOS 系统后台时间限制不能超过180秒");
    }
    if (self.pool.operations.count >= 20) {
        // 最多 允许 20 个并发线程
        return;
    }
    YYCountDownTask *downTask = nil;
    // 判断现场是否存在，存在的话继续执行倒计时。
    if ([self countDownTaskIsExistWithKey:key task:&downTask]) {
        downTask.countingDownBlock = countingDown;
        downTask.coutDownFinishBlock = countDownFinish;
        if (countingDown) {
            countingDown(downTask.leftTimeInterval);
        }
    }
    else {
        downTask        =      [[YYCountDownTask alloc] init];
        downTask.leftTimeInterval   = timeInterval;
        downTask.countingDownBlock = countingDown;
        downTask.coutDownFinishBlock = countDownFinish;
        if ([@([UIDevice currentDevice].systemVersion.doubleValue) compare:@(8)] == NSOrderedAscending) {
            downTask.operationName = key;
        }
        else {
            downTask.name = key;
        }
        [self.pool addOperation:downTask];
    }
}


- (BOOL)countDownTaskIsExistWithKey:(NSString *)key
                               task:(YYCountDownTask *__autoreleasing  _Nullable *)task {
    __block BOOL taskExist = NO;
    if ([@([UIDevice currentDevice].systemVersion.doubleValue) compare:@(8)] == NSOrderedAscending) {
        // 是否8.0 系统以上
        [self.pool.operations enumerateObjectsUsingBlock:^(__kindof YYCountDownTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.operationName isEqualToString:key]) {
                taskExist   = YES;
                *stop       = YES;
            }
        }];
    }
    else {
        [self.pool.operations enumerateObjectsUsingBlock:^(__kindof YYCountDownTask * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj.name isEqualToString:key]) {
                taskExist   = YES;
                *stop       = YES;
            }
        }];
    }
    return taskExist;
}

@end
