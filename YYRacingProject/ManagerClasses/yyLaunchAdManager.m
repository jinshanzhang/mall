//
//  yyLaunchAdManager.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/2.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "yyLaunchAdManager.h"

#import "XHLaunchAd.h"
@interface yyLaunchAdManager ()
<XHLaunchAdDelegate>

@end

@implementation yyLaunchAdManager

+ (void)load {
    [self shareManager];
}

+ (yyLaunchAdManager *)shareManager{
    static yyLaunchAdManager *instance = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        instance = [[yyLaunchAdManager alloc] init];
    });
    return instance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        //在UIApplicationDidFinishLaunching时初始化开屏广告,做到对业务层无干扰,当然你也可以直接在AppDelegate didFinishLaunchingWithOptions方法中初始化
        /*[[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
            //初始化开屏广告
            NSNumber *status = [kUserDefault objectForKey:@"isAdAlreadyShow"];
            if (!status || ![status boolValue]) {
                [self setupXHLaunchAd];
            }
        }];*/
    }
    return self;
}

#pragma mark - Private

- (void)setupXHLaunchAd{
    [self createCoopenVideo];
}


/**
 *  创建开屏视频
 **/
- (void)createCoopenVideo {
    //设置你工程的启动页使用的是:LaunchImage 还是 LaunchScreen.storyboard(不设置默认:LaunchImage)
    [XHLaunchAd setLaunchSourceType:SourceTypeLaunchImage];
    
    //配置广告数据
    XHLaunchVideoAdConfiguration *videoAdconfiguration = [XHLaunchVideoAdConfiguration new];
    //广告停留时间
    videoAdconfiguration.duration = 15;
    //广告frame
    videoAdconfiguration.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    //广告视频URLString/或本地视频名(请带上后缀)
    videoAdconfiguration.videoNameOrURLString = @"video_ad.mp4";
    //是否关闭音频
    videoAdconfiguration.muted = NO;
    //视频填充模式
    videoAdconfiguration.videoGravity = AVLayerVideoGravityResizeAspectFill;
    //是否只循环播放一次
    videoAdconfiguration.videoCycleOnce = NO;
    //广告点击打开页面参数(openModel可为NSString,模型,字典等任意类型)
    //videoAdconfiguration.openModel =  @"http://www.it7090.com";
    //跳过按钮类型
    //videoAdconfiguration.skipButtonType = SkipTypeRoundText;
    //广告显示完成动画
    videoAdconfiguration.showFinishAnimate = ShowFinishAnimateNone;
    //广告显示完成动画时间
    videoAdconfiguration.showFinishAnimateTime = 0.8;
    //后台返回时,是否显示广告
    videoAdconfiguration.showEnterForeground = NO;
    //设置要添加的子视图(可选)
    videoAdconfiguration.customSkipView = [self customSkipView];
    //videoAdconfiguration.subViews = [self launchAdSubViews];
    //显示开屏广告
    [XHLaunchAd videoAdWithVideoAdConfiguration:videoAdconfiguration delegate:self];
}

- (UIView *)customSkipView {
    UIImageView *skipView = [[UIImageView alloc] init];
    skipView.image = [UIImage imageNamed:@"ad_skip_icon"];
    skipView.frame = CGRectMake(kScreenW-(kSizeScale(17)+kSizeScale(34)), kSizeScale(17), kSizeScale(34), kSizeScale(34));
    skipView.userInteractionEnabled = YES;
    [skipView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipAction:)]];
    return skipView;
}

//跳过按钮点击事件
- (void)skipAction:(UITapGestureRecognizer *)gesture {
    //移除广告
    [kUserDefault setObject:@(YES) forKey:@"isAdAlreadyShow"];
    [XHLaunchAd removeAndAnimated:YES];
}

#pragma mark - XHLaunchAdDelegate
- (void)xhLaunchAd:(XHLaunchAd *)launchAd customSkipView:(UIView *)customSkipView duration:(NSInteger)duration {
    [kUserDefault setObject:@(YES) forKey:@"isAdAlreadyShow"];
}

@end



