//
//  yyImagePickerManagerClass.h
//  YIYanProject
//
//  Created by cjm on 2018/4/7.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TZImagePickerController.h"
#import "YYBaseViewController.h"

typedef void (^imageBlock)(NSArray *array);
typedef void (^uploadImgBlock)(NSMutableArray *array);

@interface yyImagePickerManagerClass : NSObject

+ (void)openImagePickerCtrl:(NSInteger)maxCount
                columnCount:(NSInteger)columnCount
                isAllowCrop:(BOOL)isAllowCrop
                   delegate:(id<TZImagePickerControllerDelegate>)delegate
                   viewCtrl:(YYBaseViewController *)viewCtrl
                    myBlock:(imageBlock)myBlock;
@end
