//
//  yyUserManagerClass.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/10.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "yyUserManagerClass.h"

static NSString *const __kUserInfoFileName = @"B282B8BD1CCD76D2";
static NSString *const __kStoreInfoFileName = @"B282B8BD1CCDDD34";

static NSString *const __kTabbarMenuInfoFileName = @"B282B8BD12343CCDDD34";
static NSString *const __kCommonSourceFileName = @"B282B8BD12344CCDDD34";
static NSString *const __kNewCouponFileName = @"B282B8BD12344CCDDD355";

@interface yyUserManagerClass()

@end

@implementation yyUserManagerClass

WMSingletonM(yyUserManagerClass);

- (BOOL)isLogin {
    self.currentUserInfo = [self loadUserInfo];
    return kValidString(kUserInfo.useToken) ? YES : NO;
}

- (BOOL)isFan {
    return (self.currentUserInfo.level <= 1) ? YES : NO;
}

- (BOOL)isMember {
    return self.currentUserInfo.level == 0 ? YES : NO;
}

- (BOOL)isTest {
    return self.currentUserInfo.isTestAccount;
}
-(BOOL)isTourist{
    return self.currentUserInfo.isTouristAccount;
}

#pragma mark - Public method
- (BOOL)modifyUserInfo:(NSMutableDictionary *)userDict {
    NSString *userPath = [self userFileFullPath];
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:userDict];
    BOOL isSuccess = [NSKeyedArchiver archiveRootObject:tempDict
                                                 toFile:userPath];
    if (isSuccess) {
        self.currentUserInfo = [self loadUserInfo];
    }
    return isSuccess;
}

- (BOOL)modifyStoreInfo:(NSMutableDictionary *)userDict {
    NSString *userPath = [self storeFileFullPath];
    NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithDictionary:userDict];
    BOOL isSuccess = [NSKeyedArchiver archiveRootObject:tempDict
                                                 toFile:userPath];
    if (isSuccess) {
        self.currentStoreInfo = [self loadStoreInfo];
    }
    return isSuccess;
}

- (BOOL)modifyTabbarMenuInfo:(NSMutableArray *)tabbarMenus {
    NSString *tabbarPath = [self tabbarMenuFileFullPath];
    BOOL isSuccess = [NSKeyedArchiver archiveRootObject:tabbarMenus
                                                 toFile:tabbarPath];
    return isSuccess;
}

- (BOOL)modifyCommonSources:(NSMutableArray *)commonSources {
    NSString *commonPath = [self commonSourceFileFullPath];
    BOOL isSuccess = [NSKeyedArchiver archiveRootObject:commonSources
                                                 toFile:commonPath];
    return isSuccess;
}

- (BOOL)modifyNewCouponInfo:(NSMutableDictionary *)couponInfo {
    NSString *couponPath = [self newCouponFileFullPath];
    BOOL isSuccess = [NSKeyedArchiver archiveRootObject:couponInfo
                                                 toFile:couponPath];
    return isSuccess;
}

- (YYUserInfoModel *)loadUserInfo {
    NSString *userPath = [self userFileFullPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:userPath]) {
        NSDictionary *info = [NSKeyedUnarchiver unarchiveObjectWithFile:userPath];
        if ([info.allKeys count] > 0) {
            return [YYUserInfoModel modelWithJSON:info];
        }
    }
    return nil;
}

- (StoreInfoModel *)loadStoreInfo {
    NSString *userPath = [self storeFileFullPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:userPath]) {
        NSDictionary *info = [NSKeyedUnarchiver unarchiveObjectWithFile:userPath];
        if ([info.allKeys count] > 0) {
            return [StoreInfoModel modelWithJSON:info];
        }
    }
    return nil;
}

- (NSMutableDictionary *)loadNewCouponInfo {
    NSString *couponPath = [self newCouponFileFullPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:couponPath]) {
        NSDictionary *info = [NSKeyedUnarchiver unarchiveObjectWithFile:couponPath];
        if ([info.allKeys count] > 0) {
            return [info mutableCopy];
        }
    }
    return nil;
}

- (NSMutableArray *)loadTabbarMenuInfo {
    NSString *tabbarPath = [self tabbarMenuFileFullPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:tabbarPath]) {
        NSMutableArray *info = [NSKeyedUnarchiver unarchiveObjectWithFile:tabbarPath];
        if (info.count > 0) {
            return info;
        }
    }
    return nil;
}

- (NSMutableArray *)loadCommonSources {
    NSString *commonPath = [self commonSourceFileFullPath];
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:commonPath]) {
        NSMutableArray *info = [NSKeyedUnarchiver unarchiveObjectWithFile:commonPath];
        if (info.count > 0) {
            return info;
        }
    }
    return nil;
}

- (void)clearUserData {
    [self modifyUserInfo:nil];
    [self modifyStoreInfo:nil];
    [self modifyCommonSources:nil];
    [self modifyTabbarMenuInfo:nil];
}

#pragma mark - Private method
- (NSString *)documentDirectoryPath {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

- (NSString *)userFileFullPath {
    return [[self documentDirectoryPath] stringByAppendingPathComponent:__kUserInfoFileName];
}

- (NSString *)storeFileFullPath {
    return [[self documentDirectoryPath] stringByAppendingPathComponent:__kStoreInfoFileName];
}

- (NSString *)tabbarMenuFileFullPath {
    return [[self documentDirectoryPath] stringByAppendingPathComponent:__kTabbarMenuInfoFileName];
}

- (NSString *)commonSourceFileFullPath {
    return [[self documentDirectoryPath] stringByAppendingPathComponent:__kCommonSourceFileName];
}

- (NSString *)newCouponFileFullPath {
    return [[self documentDirectoryPath] stringByAppendingPathComponent:__kNewCouponFileName];
}
@end
