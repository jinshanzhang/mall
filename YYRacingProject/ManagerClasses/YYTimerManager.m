//
//  YYTimerManager.m
//  TestSideslip
//
//  Created by 蔡金明 on 2018/12/18.
//  Copyright © 2018 蔡金明. All rights reserved.
//

#import "YYTimerManager.h"

#define yy_signal(sema) dispatch_semaphore_signal(sema);
#define yy_wait(sema) dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
@interface YYTimerManager ()

@property (nonatomic, strong) dispatch_source_t timer; //定时器对象
@property (nonatomic, strong) dispatch_semaphore_t  lock; //信号量
@property (nonatomic, strong) dispatch_queue_t timeQueue; //子线程
@property (nonatomic, assign) NSInteger        totalSecond; //倒计时时间
@property (nonatomic, copy)   YYTimerCallBack  timerCallBack; //倒计时回调

@property (nonatomic, strong) NSDate  *enterBackgroundTime; //进入后台的时间
@end

@implementation YYTimerManager

#pragma mark - Life cycle
- (instancetype)init {
    self = [super init];
    if (self) {
        // 传入的值必须是大于0， 否则返回null
        self.lock = dispatch_semaphore_create(1);
        self.timeQueue = dispatch_queue_create("com.yiyan.timer.queue", DISPATCH_QUEUE_SERIAL);
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecameActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

#pragma mark - Public
+ (instancetype)timerManager {
    static YYTimerManager *timerManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        timerManager = [YYTimerManager new];
    });
    return timerManager;
}

- (void)registerCoutDown:(NSUInteger)seconds timerCallBack:(nonnull YYTimerCallBack)timerCallBack {
    if (seconds < 0 || timerCallBack == nil) { return ; }
    
    self.totalSecond = seconds;
    self.timerCallBack = timerCallBack;
    
    yy_wait(self.lock);
    [self _createTimer];
    yy_signal(self.lock);
}

- (void)modifyCountDownDuration:(NSUInteger)seconds {
    if (seconds < 0) { return; }
    self.totalSecond = seconds;
    if (!self.timer) {
        yy_wait(self.lock);
        [self _createTimer];
        yy_signal(self.lock);
    }
    else {
        dispatch_suspend(self.timer);
        dispatch_resume(self.timer);
    }
}

#pragma mark - Private
- (void)_createTimer {
    if (self.timer != nil) { return; }
    // NSEC_PER_SEC 秒     USEC_PER_SEC 毫秒   NSEC_PER_USEC 每毫秒有多少纳秒
    // 创建定时器
    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, self.timeQueue);
    // 设置定时器倒计时规则
    dispatch_source_set_timer(self.timer, DISPATCH_TIME_NOW + 1.0 * NSEC_PER_SEC, 1.0 *NSEC_PER_SEC, 0);
    // 处理倒计时
    dispatch_source_set_event_handler(self.timer, ^{
        [self _countDownWithInterval:1];
    });
    dispatch_resume(self.timer);
}

- (void)_countDownWithInterval:(unsigned long)interval {
    yy_wait(self.lock);
    BOOL stop = NO;
    if (self.totalSecond <= 0) {
        self.totalSecond = 0;
        dispatch_source_cancel(self.timer);
        self.timer = nil;
        stop = YES;
    }
    else {
        self.totalSecond -= interval;
    }
    if (self.timerCallBack) {
        self.timerCallBack(self.totalSecond, stop);
    }
    yy_signal(self.lock);
}

#pragma mark - Notification

- (void)applicationDidBecameActive:(NSNotification *)notification {
    // 挂起状态恢复
    if (self.enterBackgroundTime && self.timer) {
        long delay = [[NSDate date] timeIntervalSinceDate: self.enterBackgroundTime];
        dispatch_suspend(self.timer);
        [self _countDownWithInterval: delay];
        dispatch_resume(self.timer);
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    // 进入后台
    self.enterBackgroundTime = [NSDate date];
}
@end
