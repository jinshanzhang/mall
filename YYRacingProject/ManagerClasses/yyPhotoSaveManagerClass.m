//
//  yyPhotoSaveManagerClass.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/20.
//  Copyright © 2018 cjm. All rights reserved.
//

#import "yyPhotoSaveManagerClass.h"

#import "AFURLSessionManager.h"
@interface YYPhotoAlbumManager ()

@end

@implementation YYPhotoAlbumManager

- (BOOL)isAccessAuthorityAboutPhotoAlbum {
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    if (status == PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied) {
        if (status == PHAuthorizationStatusRestricted) {
            [YYCommonTools showTipMessage:@"系统原因，不能访问相册"];
        }
        else {
            if (status == PHAuthorizationStatusNotDetermined)
            [YYCommonTools showTipMessage:@"请打开访问相册权限"];
        }
        return NO;
    }
    return YES;
}

- (PHAssetCollection *)createCollection {
    // 是否使用app名字
    //NSString *appName = [NSBundle mainBundle].infoDictionary[(NSString *)kCFBundleNameKey];
    __block NSString *appName = @"蜀黍之家";
    // 先从已存在相册中找到自定义相册对象
    PHFetchResult<PHAssetCollection *> *collectionResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    for (PHAssetCollection *collection in collectionResult) {
        if ([collection.localizedTitle isEqualToString:appName]) {
            return collection;
        }
    }
    // 新建自定义相册
    __block NSString *collectionId = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
        collectionId = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:appName].placeholderForCreatedAssetCollection.localIdentifier;
    } error:nil];
    
    if (!kValidString(collectionId)) {
        [YYCommonTools showTipMessage:@"相册创建失败"];
        return nil;
    }
    //注意返回的是lastObject
    return [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[collectionId] options:nil].firstObject;
}

- (PHObjectPlaceholder *)createdAssetWithImageObject:(id)imageObject {
    NSError *error = nil;
    __block PHObjectPlaceholder *createdAsset = nil;
    [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
        if ([imageObject isKindOfClass:[UIImage class]]) {
            UIImage *image = imageObject;
            createdAsset = [PHAssetChangeRequest creationRequestForAssetFromImage:image].placeholderForCreatedAsset;
        }
        else {
            if ([imageObject isKindOfClass:[NSString class]] || [imageObject isKindOfClass:[NSURL class]]) {
                NSURL *imageUrl = nil;
                if ([imageObject isKindOfClass:[NSString class]]) {
                    imageUrl = [NSURL URLWithString:imageObject];
                }
                else {
                    imageUrl = imageObject;
                }
                // url 图片不能使用下面方法， 需要自己做一下转化。
                //createAssetId = [PHAssetCreationRequest creationRequestForAssetFromImageAtFileURL:newImageUrl].placeholderForCreatedAsset.localIdentifier;
                if (imageUrl) {
                    NSData *data = [NSData dataWithContentsOfURL:imageUrl];
                    UIImage *image = [UIImage imageWithData:data];
                    createdAsset = [PHAssetChangeRequest creationRequestForAssetFromImage:image].placeholderForCreatedAsset;
                }
                else {
                    NSAssert(YES, @"imageObject 参数为空");
                }
            }
            else {
                NSAssert(YES, @"类型不匹配");
            }
        }
    } error:&error];
    
    if (error) {
        NSAssert(YES, @"图片转换存储相册数据失败");
        return nil;
    }
    return createdAsset;
}

- (BOOL)saveSinglePhotoReachAlbum:(id)imageObject {
    BOOL success = NO;
    NSError *error = nil;
    // 创建相册
    PHAssetCollection *createdCollection = [self createCollection];
    // 创建添加资源项
    PHObjectPlaceholder *createdAsset = [self createdAssetWithImageObject:imageObject];
    if (createdAsset == nil || createdCollection == nil) {
        NSAssert(YES, @"数据缺失");
        return success;
    }
    [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
        // 操作指定的相册，插入图片
        [[PHAssetCollectionChangeRequest changeRequestForAssetCollection:createdCollection] insertAssets:@[createdAsset] atIndexes:[NSIndexSet indexSetWithIndex:0]];
    } error:&error];
    
    if (error) {
        NSAssert(error, @"error=%@",error);
    }
    else {
        success = YES;
    }
    return success;
}
@end

@implementation yyPhotoSaveManagerClass

WMSingletonM(yyPhotoSaveManagerClass);
- (void)savePhotoReachAlbum:(NSMutableArray *)photoArrays {
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    if (photoArrays.count <= 0 || !photoArrays) {
        NSAssert(photoArrays.count <= 0, @"保存图片对象不能为空");
        return;
    }
    
    YYPhotoAlbumManager *manager = [YYPhotoAlbumManager new];
    PHAuthorizationStatus oldStatus = [PHPhotoLibrary authorizationStatus];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status)
        {
            case PHAuthorizationStatusAuthorized: // 权限打开
            {
                __block PHAssetCollection *collection = nil;
                // 创建线程组
                dispatch_group_t group =  dispatch_group_create();
                // 创建串行队列
                dispatch_queue_t queue = dispatch_queue_create("com.beike.savePhotoQueue", DISPATCH_QUEUE_SERIAL);
                // 异步任务
                dispatch_group_async(group, queue, ^{
                    // 防止自定义相册首次没有创建
                    collection = [manager createCollection];
                });
                // 保存到自定义相册里的图片顺序为倒序。
                for (int i = 0; i < photoArrays.count; i ++) {
                    dispatch_group_async(group, queue, ^{
                        [manager saveSinglePhotoReachAlbum:[photoArrays objectAtIndex:i]];
                    });
                }
                // 任务执行完毕，任务通知
                dispatch_group_notify(group, dispatch_get_main_queue(), ^{
                    CFAbsoluteTime endTime = (CFAbsoluteTimeGetCurrent() - startTime);
                    YYLog(@"方法耗时: %f ms", endTime * 1000.0);
                    [YYCommonTools showTipMessage:@"保存成功"];
                    [YYCenterLoading hideCenterLoading];
                    [YYCenterLoading hideCenterLoadingInWindow];
                    [YYQRCodeCenterLoading hideCenterLoading:nil
                                                   superView:nil];
                });
                break;
            }
            case PHAuthorizationStatusDenied: // 权限拒绝
            case PHAuthorizationStatusRestricted: // 权限受限
            {
                if (oldStatus == PHAuthorizationStatusNotDetermined) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                     message:@"请在设置>隐私>照片中开启权限"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"知道了"
                                                           otherButtonTitles:nil, nil];
                    [alert show];
                });
                break;
            }
            default:
                break;
        }
    }];
}

- (void)saveVideoReachAlbum:(id)dataSource {
    NSURL *downloadURL = nil;
    if ( !dataSource ) {
        NSAssert(YES, @"视频资源不能为空");
        return;
    }
    if ([dataSource isKindOfClass:[NSString class]]) {
        downloadURL = [NSURL URLWithString:dataSource];
    }
    else {
        downloadURL = dataSource;
    }
    
    PHAuthorizationStatus oldStatus = [PHPhotoLibrary authorizationStatus];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status)
        {
            case PHAuthorizationStatusAuthorized: // 权限打开
            {
                [YYCommonTools showTipMessage:@"开始下载"];
                NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
                AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
                //设置网络请求序列化对象
                AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
                requestSerializer.timeoutInterval = 30;
                //设置返回数据序列化对象
                AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
                manager.responseSerializer = responseSerializer;
               
                NSURLRequest *request = [NSURLRequest requestWithURL:downloadURL];
                NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress *downloadProgress){
                    NSLog(@"下载进度:%lld",downloadProgress.completedUnitCount);
                } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                    NSURL *fileURL = [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
                    NSLog(@"fileURL:%@",[fileURL absoluteString]);
                    return fileURL;
                } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                    if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([filePath path])) {
                        UISaveVideoAtPathToSavedPhotosAlbum([filePath path], self, nil, nil);
                        [YYCommonTools showTipMessage:@"视频下载成功"];
                    }
                    NSLog(@"File downloaded to: %@", filePath);
                }];
                [downloadTask resume];
            }
                break;
            case PHAuthorizationStatusDenied: // 权限拒绝
            case PHAuthorizationStatusRestricted: // 权限受限
            {
                if (oldStatus == PHAuthorizationStatusNotDetermined) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                     message:@"请在设置>隐私>照片中开启权限"
                                                                    delegate:nil
                                                           cancelButtonTitle:@"知道了"
                                                           otherButtonTitles:nil, nil];
                    [alert show];
                });
                break;
            }
            default:
                break;
        }

    }];
}
@end
