//
//  yyPhotoSaveManagerClass.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/12/20.
//  Copyright © 2018 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

NS_ASSUME_NONNULL_BEGIN
#define kSaveManager    [yyPhotoSaveManagerClass sharedyyPhotoSaveManagerClass]

#pragma mark - Manager  (相册管理)
@interface YYPhotoAlbumManager : NSObject

/**
 是否有访问相册的权限
 **/
- (BOOL)isAccessAuthorityAboutPhotoAlbum;

/**
 创建相册
 **/
- (PHAssetCollection *)createCollection;

/**
 图片转换为可存入相册的对象
 imageObject 可NSString,UIImage,NSURL
 **/
- (PHObjectPlaceholder *)createdAssetWithImageObject:(id)imageObject;

/**
 单张图片保存到相册是否成功
 **/
- (BOOL)saveSinglePhotoReachAlbum:(id)imageObject;

@end


@interface yyPhotoSaveManagerClass : NSObject

WMSingletonH(yyPhotoSaveManagerClass);

/**
 将图片保存到相册，支持网络图片和本地图片
 **/
- (void)savePhotoReachAlbum:(NSMutableArray *)photoArrays;

/**
 将视频保存到相册
 **/
- (void)saveVideoReachAlbum:(id)dataSource;
@end

NS_ASSUME_NONNULL_END
