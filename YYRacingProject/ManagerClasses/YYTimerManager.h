//
//  YYTimerManager.h
//  TestSideslip
//
//  Created by 蔡金明 on 2018/12/18.
//  Copyright © 2018 蔡金明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

typedef void (^YYTimerCallBack)(NSUInteger second, BOOL stop);
@interface YYTimerManager : NSObject

/**定时器管理对象**/
+ (instancetype)timerManager;

/**注册倒计时**/
- (void)registerCoutDown:(NSUInteger)seconds timerCallBack:(YYTimerCallBack)timerCallBack;

/**是否修改倒计时时长**/
- (void)modifyCountDownDuration:(NSUInteger)seconds;
@end

NS_ASSUME_NONNULL_END
