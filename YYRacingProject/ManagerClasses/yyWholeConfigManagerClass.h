//
//  yyWholeConfigManagerClass.h
//  YIYanProject
//
//  Created by cjm on 2018/4/21.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PreOrderModel.h"
#import "MyCourceModel.h"

#define kWholeConfig          [yyWholeConfigManagerClass sharedyyWholeConfigManagerClass]

@interface yyWholeConfigManagerClass : NSObject

WMSingletonH(yyWholeConfigManagerClass);

#pragma mark - Request
/**获取购物车数量**/
- (void)gainGoodSkuTotalCountRequest:(void(^)(NSInteger ))skuTotalCount;

/**加sku到购物车**/
- (void)addGoodSkuToShippingCart:(NSDictionary *)params
                         success:(void(^)(BOOL))success;

/**sku结算操作**/
- (void)submitOrderWithSkulist:(NSMutableArray *)skulist
                         souce:(int)source
                         addID:(int)addID
                 previewSwitch:(int)previewSwitch
                      couponId:(NSArray *)couponId
                      payment:(NSString *)payment
                      deliveryType:(NSString *)deliveryType 
                         block:(void (^)(PreOrderModel *))preModelBlock
                     failBlock:(void (^)(PreOrderModel *))failBlock;

/**获取用户信息**/
- (void)gainUserCenterInfo:(void(^)(BOOL))success
                 isLoading:(BOOL)isLoading;

/**新人优惠券信息**/
- (void)newUserCouponRequest;

/**双十一口令弹框**/
- (void)passwordCheckRequest;

/**短信链接通用链跳转**/
- (void)noteLinkUniversalSkip:(NSMutableDictionary *)params;

/**用户绑定请求**/
- (void)bindPromotionRequset:(NSString *)code
                       token:(NSString *)token;
/**获取用户位置信息**/
- (void)locationRecommend:(NSString *)loc;

/**获取底部tabbar资源位**/
- (void)tabbarMenuIconRequest;

/**获取通用资源位**/
- (void)commonResourceRequest;

/**获取推送token**/
- (void)gainUserPushToken;

/**获取店铺信息**/
- (void)storeDetailInfoRequest:(void(^)(BOOL))success
                     isLoading:(BOOL)isLoading;

/**根据商品ID获取课程内容**/
- (void)getLessonWithGoodID:(int)appItemId
                      block:(void (^)(NSMutableArray *))courceModelBlock
                  failBlock:(void (^)(NSMutableArray *))failBlock;


/**订单取消操作**/
- (void)cancelOrderRequest:(NSString *)orderID
                   success:(void(^)(BOOL))success;
/**订单确认收货操作**/
- (void)confirmOrderRequest:(NSString *)orderID
                   success:(void(^)(BOOL))success;
/**删除订单操作**/
- (void)deleteOrderRequest:(NSString *)orderID
                   isStore:(BOOL)isStore
                    success:(void(^)(BOOL))success;

/**生成商品二维码**/
- (void)produceGoodQRCodeRequest:(NSMutableDictionary *)params
                         success:(void(^)(NSDictionary *))success;

#pragma mark - Home
/**获取首页资源位**/
- (void)homeSourcePointRequest:(NSMutableDictionary *)params
                       success:(void(^)(NSDictionary *))success;


#pragma mark - CsdCircle
/**素材圈接口**/
- (void)gainGoodCsdCircleListRequest:(NSMutableDictionary *)params
                             success:(void(^)(NSDictionary *))success;
/**素材圈反馈接口**/
- (void)feedBackAboutCsdCircle:(NSMutableDictionary *)params
                       success:(void(^)(NSDictionary *))success;

#pragma mark - Course
/**获取播放授权**/
- (void)gainCoursePlayAuth:(void(^)(NSDictionary *))success;
/**获取播放视频信息**/
- (void)gainVideoPlayInfo;

#pragma mark - Common Tool

/**计算缓存文件大小**/
- (CGFloat)calculateBufferSize;
/**清除成功**/
- (void)clearBufferSize;

@end
