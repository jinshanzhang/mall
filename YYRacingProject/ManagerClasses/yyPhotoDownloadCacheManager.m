//
//  YYPhotoDownloadCacheManager.m
//  OCTheme
//
//  Created by 蔡金明 on 2019/4/2.
//  Copyright © 2019 蔡金明. All rights reserved.
//

#import "yyPhotoDownloadCacheManager.h"
#import "AFNetworking.h"

@implementation yyPhotoDownloadCacheManager

/**
 *  创建目录文件
 */
+ (NSString *)createDirectoryFileExistsAtPath:(NSString *)path
                                      isClear:(BOOL)isClear {
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", path]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createDirectoryAtPath:filePath
               withIntermediateDirectories:YES attributes:nil error:nil];
    }
    else {
        if (isClear) {
            [fileManager removeItemAtPath:filePath
                                    error:nil];
            [fileManager createDirectoryAtPath:filePath
                   withIntermediateDirectories:YES attributes:nil error:nil];
        }
    }
    return filePath;
}

/**
 *  创建文件
 */
+ (NSString *)createIconPath:(NSString *)iconName iconIndex:(NSInteger)iconIndex status:(NSString *)status isAddSuffix:(BOOL)isAddSuffix {
    NSInteger scale = [UIScreen mainScreen].scale;
    if (isAddSuffix) {
        return [NSString stringWithFormat:@"%@%ld_%@_icon@%ldx.png", iconName,iconIndex,status, scale];
    }
    else {
        return [NSString stringWithFormat:@"%@@%ldx.png", iconName, scale];
    }
}

/**
 *  资源内容下载
 */
+ (void)downloadPhotoSources:(NSMutableArray *)photoSources directory:(NSString *)directory
                    iconName:(NSString *)iconName  isAddSuffix:(BOOL)isAddSuffix
                   callBlock:(void(^)(BOOL isSuccess))callBlock {
    __block BOOL isDownloadSuccess = NO;
    if (photoSources.count <= 0) {
        callBlock(isDownloadSuccess);
    }
    __block NSInteger downloadTimes = 0;
    NSInteger photoCount = photoSources.count;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = 3;
    
    NSString *basePath = [kPhotoCacheConfig createDirectoryFileExistsAtPath:directory
                                                                    isClear:YES];
    
    for (NSInteger i = 0; i < photoCount; i ++) {
        NSString *iconUrl = photoSources[i];
        
        NSInteger value = ceil(i/2);
        NSString *status = (i%2==0 ? @"default" : @"select");
        NSString *iconPath = [kPhotoCacheConfig createIconPath:iconName
                                                               iconIndex:value
                                                                  status:status
                                                             isAddSuffix:isAddSuffix];
        iconPath =  [basePath stringByAppendingPathComponent:iconPath];
        NSOperation *operation = [NSBlockOperation  blockOperationWithBlock:^{
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:iconUrl]];
            NSURLSessionDownloadTask *task = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
                
            } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
                //设置下载路径，通过沙盒获取缓存地址，最后返回NSURL对象
                NSLog(@"[filePath = %@]", iconPath);
                return [NSURL fileURLWithPath:iconPath];
            } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
                downloadTimes ++;
                if (downloadTimes >= photoCount) {
                    isDownloadSuccess = YES;
                    callBlock(isDownloadSuccess);
                }
            }];
            [task resume];
        }];
        
        [queue addOperation:operation];
    }
}

/**
 *  图片资源读取
 */
+ (UIImage *)readImageSource:(NSString *)imageName
                   directory:(NSString *)directory {
    if (!kValidString(imageName)) {
        return nil;
    }
    NSInteger scale = [UIScreen mainScreen].scale;
    NSString *basePath = [kPhotoCacheConfig createDirectoryFileExistsAtPath:directory
                                                                    isClear:NO];
    NSString *iconPath = [NSString stringWithFormat:@"%@@%ldx.png", imageName, scale];
    iconPath = [basePath stringByAppendingPathComponent:iconPath];
    return [UIImage imageWithContentsOfFile:iconPath];
}
@end
