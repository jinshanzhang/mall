//
//  UIColor+Extension.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "UIColor+Extension.h"

@implementation UIColor (Extension)

+ (UIColor *)addGradientColor:(NSArray*)colors gradientType:(GradientType)gradientType imgSize:(CGSize)imgSize {
    UIImage *tempImage = [UIImage gradientColorImageFromColors:colors
                                                  gradientType:gradientType
                                                       imgSize:imgSize];
    return  [UIColor colorWithPatternImage:tempImage];
}


/**金色渐变**/
+ (UIColor *)addGoldGradient:(CGSize)colorScopeSize{
    return [UIColor addGradientColor:@[HexRGB(0xE6B96E), HexRGB(0xE2A848)]
                     gradientType:GradientTypeLeftToRight
                          imgSize:colorScopeSize];
}

/**深蓝色渐变**/
+ (UIColor *)addDeepBlueGradient:(CGSize)colorScopeSize {
    return [UIColor addGradientColor:@[HexRGB(0x323264), HexRGB(0x191946)]
                     gradientType:GradientTypeLeftToRight
                          imgSize:colorScopeSize];
}


@end
