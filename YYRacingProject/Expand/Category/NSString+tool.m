//
//  NSString+tool.m
//  BagerKit
//
//  Created by Bager on 2017/8/8.
//  Copyright © 2017年 Bager. All rights reserved.
//

#import "NSString+tool.h"

@implementation NSString (tool)

+ (NSString *)getWeekDay:(NSTimeInterval)time {
    //创建一个星期数组
    NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    //将时间戳转换成日期
    NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:time];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
    NSString *weekStr = [weekday objectAtIndex:components.weekday];
    return weekStr;
}

+ (NSString *)getDate {
    // 获取代表公历的NSCalendar对象
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    // 获取当前日期
    NSDate *dt = [NSDate date];
    // 定义一个时间字段的旗标，指定将会获取指定年、月、日、时、分、秒的信息
    unsigned unitFlags = NSCalendarUnitYear |
    NSCalendarUnitMonth |  NSCalendarUnitDay |
    NSCalendarUnitHour |  NSCalendarUnitMinute |
    NSCalendarUnitSecond | NSCalendarUnitWeekday;
    // 获取不同时间字段的信息
    NSDateComponents* comp = [gregorian components: unitFlags
                                          fromDate:dt];
    return [NSString stringWithFormat:@"%ld.%ld.%ld",comp.year,comp.month,comp.day];
}

+ (NSString *)formatDateAndTime:(NSTimeInterval)time{
    if (time < 0) {
        return @"";
    }
    NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time/1000];
    return [formatter stringFromDate:date];
}

+ (NSString *)formatDateAndTime:(NSTimeInterval)time
                      formatter:(NSDateFormatter *)formatter {
    if (time < 0 || !formatter) {
        return @"";
    }
    //[formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time/1000];
    return [formatter stringFromDate:date];
}

/**
十进制转换为二进制

@param decimal 十进制数
@return 二进制数
*/
+ (NSString *)getBinaryByDecimal:(NSInteger)decimal {
    NSString *binary = @"";
    while (decimal) {
        binary = [[NSString stringWithFormat:@"%ld", decimal % 2] stringByAppendingString:binary];
        if (decimal / 2 < 1) {
            break;
        }
        decimal = decimal / 2 ;
    }
    if (binary.length % 8 != 0) {
        NSMutableString *mStr = [[NSMutableString alloc]init];;
        for (int i = 0; i < 8 - binary.length % 8; i++) {
            [mStr appendString:@"0"];
        }
        binary = [mStr stringByAppendingString:binary];
    }
    return binary;
}

- (NSMutableAttributedString *)addCenterLine{
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:self attributes:attribtDic];
    return attributeStr;
}

- (NSMutableAttributedString *)addDownLine{
    
     NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:self attributes:attribtDic];
    return attributeStr;
}

- (NSString *)URLEncodedString {
    NSString *result = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)self,
                                                              NULL,
                                                              CFSTR("!*'();:@&=+$,/?%#[] "),
                                                              kCFStringEncodingUTF8));
    return result;
}

- (NSString*)URLDecodedString {
    NSString *result = (NSString *)
    CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
                                                                              (CFStringRef)self,
                                                                              CFSTR(""),
                                                                              kCFStringEncodingUTF8));
    return result;
}

/*邮箱验证*/
- (BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)isValidUrl {
    NSString *regex =@"[a-zA-z]+://[^\\s]*";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    return [urlTest evaluateWithObject:self];
}

/*手机号码验证*/
- (BOOL)isValidateMobile:(NSString *)mobile
{
    if (mobile.length != 11)
    {
        return NO;
    }
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     * 联通号段: 130,131,132,155,156,185,186,145,176,1709
     * 电信号段: 133,153,180,181,189,177,1700
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)\\d{8}$";
    /**
     * 中国移动：China Mobile
     * 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     */
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    /**
     * 中国联通：China Unicom
     * 130,131,132,155,156,185,186,145,176,1709
     */
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    /**
     * 中国电信：China Telecom
     * 133,153,180,181,189,177,1700
     */
    NSString *CT = @"(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";
    
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobile] == YES)
        || ([regextestcm evaluateWithObject:mobile] == YES)
        || ([regextestct evaluateWithObject:mobile] == YES)
        || ([regextestcu evaluateWithObject:mobile] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

/*车牌号验证*/
- (BOOL)isValidateCarNo:(NSString*)carNo
{
    NSString *carRegex = @"^[A-Za-z]{1}[A-Za-z_0-9]{5}$";
    NSPredicate *carTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",carRegex];
    YYLog(@"carTest is %@",carTest);
    return [carTest evaluateWithObject:carNo];
}
/*身份证号验证*/
- (BOOL)isValidateIDCardNo:(NSString *)idCardNo{
    BOOL flag;
    if (idCardNo.length <= 0)
    {
        flag = NO;
        return flag;
    }
    
    NSString *regex2 = @"^(^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$)|(^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[Xx])$)$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    flag = [identityCardPredicate evaluateWithObject:idCardNo];
    
    
    //如果通过该验证，说明身份证格式正确，但准确性还需计算
    if(flag)
    {
        if(idCardNo.length==18)
        {
            //将前17位加权因子保存在数组里
            NSArray * idCardWiArray = @[@"7", @"9", @"10", @"5", @"8", @"4", @"2", @"1", @"6", @"3", @"7", @"9", @"10", @"5", @"8", @"4", @"2"];
            
            //这是除以11后，可能产生的11位余数、验证码，也保存成数组
            NSArray * idCardYArray = @[@"1", @"0", @"10", @"9", @"8", @"7", @"6", @"5", @"4", @"3", @"2"];
            
            //用来保存前17位各自乖以加权因子后的总和
            
            NSInteger idCardWiSum = 0;
            for(int i = 0;i < 17;i++)
            {
                NSInteger subStrIndex = [[idCardNo substringWithRange:NSMakeRange(i, 1)] integerValue];
                NSInteger idCardWiIndex = [[idCardWiArray objectAtIndex:i] integerValue];
                
                idCardWiSum+= subStrIndex * idCardWiIndex;
                
            }
            
            //计算出校验码所在数组的位置
            NSInteger idCardMod=idCardWiSum%11;
            
            //得到最后一位身份证号码
            NSString * idCardLast= [idCardNo substringWithRange:NSMakeRange(17, 1)];
            
            //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
            if(idCardMod==2)
            {
                if([idCardLast isEqualToString:@"X"]||[idCardLast isEqualToString:@"x"])
                {
                    return flag;
                }else
                {
                    flag =  NO;
                    return flag;
                }
            }else
            {
                //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                if([idCardLast isEqualToString: [idCardYArray objectAtIndex:idCardMod]])
                {
                    return flag;
                }
                else
                {
                    flag =  NO;
                    return flag;
                }
            }
        }
        else
        {
            flag =  NO;
            return flag;
        }
    }
    else
    {
        return flag;
    }
}

+ (NSString *)usingEncoding:(NSString *)str {
    if (@available(iOS 9, *)) {
        return  [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }
    else {
        return [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
}

+ (NSString *)dataTOjsonString:(id)object {
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        YYLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    
    if (err) {
        YYLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

- (NSString *)cutStringUseSpecialChar:(NSString *)chars {
    NSRange range = [self rangeOfString:chars];
    if (range.length == 0) {
        return self;
    }
    NSString *tempStr = [self substringFromIndex:range.location+1];
    return tempStr;
}


/**判断是否包含中文**/
- (BOOL)isValidateChineseStr:(NSString *)text
{
    int asciiCode = [text characterAtIndex:0];
    if (asciiCode > 47 && asciiCode < 58) {//数字
        return NO;
    }
    if (asciiCode > 64 && asciiCode < 91) {//大写字母
        return NO;
    }
    if (asciiCode > 96 && asciiCode < 123) {//小写
        return NO;
    }
    //以下为正则表达式
    NSString *phoneRegex = @"[\u4e00-\u9fa5]";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:text];
}

- (NSMutableArray *)intergerValueSeparated:(BOOL)isReverse {
    NSMutableArray *tempValues = [NSMutableArray array];
    if (!kValidString(self)) {
        return nil;
    }
    NSInteger showValue = [self integerValue];
    for (int i = 0; i < self.length; i ++) {
        NSInteger divisorValue = showValue/10;
        NSInteger remainderValue = showValue%10;
        [tempValues addObject:@(remainderValue)];
        showValue = divisorValue;
    }
    if (isReverse) {
        return [[[tempValues reverseObjectEnumerator] allObjects] mutableCopy];
    }
    else {
        return tempValues;
    }
}

- (NSString *)handlerMiriadeOverChar:(NSString *)chars {
    float value = [chars floatValue];
    if (value >= 10000.0) {
        //是否超过一万
        value = value / 10000.0;
        NSString *transString = [NSString stringWithFormat:@"%f",value];//1.4500
        NSRange range = [transString rangeOfString:@"."];
        if (range.location != NSNotFound) {
            NSString *nextChar = [transString substringFromIndex:range.location+1];
            if ([nextChar intValue] == 0) {
               return  [NSString stringWithFormat:@"%@万",[transString substringToIndex:range.location]];
            }
            else {
                return  [NSString stringWithFormat:@"%.1f万",value];
            }
        }
        else {
            return transString;
        }
    }
    return chars;
}


- (NSDate *)getCurrentDate {
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    // 获取当前时间
    NSDate *current = [NSDate date];
    // 修复时差, 获取当前时区时间
    NSInteger currentinterval = [timeZone secondsFromGMTForDate:current];
    NSDate *currentDate = [current dateByAddingTimeInterval:currentinterval];
    return currentDate;
}

- (NSString *)getCurrentTimestamp {
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    // 获取当前时间
    NSDate *current = [NSDate date];
    // 修复时差, 获取当前时区时间
    NSInteger currentinterval = [timeZone secondsFromGMTForDate:current];
    NSDate *currentDate = [current dateByAddingTimeInterval:currentinterval];
    
    NSTimeInterval time = [currentDate timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%.f", time];//转为字符型
    return timeString;
}

+ (NSString *)getPreHotShowTime:(NSTimeInterval)showTime
                    currentTime:(NSTimeInterval)currentTime {
    // 是否后天
    BOOL isTheDayAfterTomorrow = NO;
    NSString *time = [NSString string];
    NSString *prefixTime = [NSString string];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:showTime/1000];
    
    BOOL isToday = [calendar isDateInToday:date];
    BOOL isTomorrow = [calendar isDateInTomorrow:date];
    
    NSCalendarUnit unit = NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
    NSDateComponents *components = [calendar components:unit fromDate:[NSDate dateWithTimeIntervalSinceNow:-60 * 60 * 24 * 2]];
    NSDate *tomorrow = [calendar dateFromComponents:components];
    isTheDayAfterTomorrow = [tomorrow isEqualToDate:date];
    
    if (isToday) {
        //是否今天
        prefixTime = (date.hour >= 18? @"今晚" : @"今天");
    }
    else {
        if (isTomorrow) {
            //是否明天
            prefixTime = (date.hour >= 18? @"明晚" : @"明天");
        }
        else {
            if (isTheDayAfterTomorrow) {
                //是否是后天
                prefixTime = @"后天";
            }
            else {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM月dd日"];
                prefixTime = [formatter stringFromDate:date];
            }
        }
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    time = [NSString stringWithFormat:@"%@%@",prefixTime,[formatter stringFromDate:date]];
    return time;
}

- (NSString *)changeProductNameWithProductName:(NSString *)productName
                                        length:(NSInteger)length
{
    if (productName.length < length/2) return productName;
    
    NSString * name = [NSString string];
    NSString * temp = nil;
    NSInteger count = productName.length > length ? length : productName.length;
    int j = 0;
    for (int i = 0; i < count; i ++) {
        temp = [productName substringWithRange:NSMakeRange(i, 1)];
        j++;
        BOOL isChinese = [self isValidateChineseStr:temp];
        if (isChinese) {
            j ++;
            if (i >= count) {
                break;
            }
        }
        if (j >= length) {
            break;
        }
        name = [NSString stringWithFormat:@"%@%@",name,temp];
    }
    return name;
}

+ (NSString *)removeBlankAndLine:(NSString *)str {
    NSString *countString = str;
    countString = [countString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除首位空格
    //countString = [countString stringByReplacingOccurrencesOfString:@" "withString:@""];  //去除中间空格
    countString = [countString stringByReplacingOccurrencesOfString:@"\n" withString:@""];  //去除换行符
    return countString;
}

+ (NSUInteger)textLength:(NSString *)text {
    
    NSUInteger asciiLength = 0;
    for (NSUInteger i = 0; i < text.length; i++) {
        unichar uc = [text characterAtIndex: i];
        asciiLength += isascii(uc) ? 1 : 2;
    }
    NSUInteger unicodeLength = asciiLength;
    return unicodeLength;
}

// 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
+ (UIColor *)colorWithHexString:(NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    // 判断前缀并剪切掉
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    // 从六位数值中找到RGB对应的位数并转换
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //R、G、B
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

- (NSString *)encode:(NSString *)string
{
    //先将string转换成data
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *base64Data = [data base64EncodedDataWithOptions:0];
    
    NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
    
    return baseString;
}

- (NSString *)dencode:(NSString *)base64String
{
    NSData *data = [[NSData alloc] initWithBase64EncodedString:base64String options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

//转json字符串
+(NSString *)convertToJsonData:(NSDictionary *)dict{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString;
    if (!jsonData) {
        NSLog(@"%@",error);
    }else{
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    NSRange range = {0,jsonString.length};
    //去掉字符串中的空格
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0,mutStr.length};
    //去掉字符串中的换行符
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    return mutStr;
    
}

@end
