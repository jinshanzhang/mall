//
//  NSObject+Swizzling.h
//  XinHuoApp
//
//  Created by born on 2017/2/21.
//  Copyright © 2017年 蔡金明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Swizzling)

+ (void)methodSwizzlingWithOriginalSelector:(SEL)originalSelector swizzling:(SEL)swizzlingSelector;

@end
