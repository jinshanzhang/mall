//
//  UINavigationController+Extension.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/3.
//  Copyright © 2019 cjm. All rights reserved.
//

#import "UINavigationController+Extension.h"
#import "UIViewController+Extension.h"

@implementation UINavigationController (Extension)

- (UIViewController *)childViewControllerForStatusBarStyle
{
    return self.visibleViewController;
}

- (UIViewController *)childViewControllerForStatusBarHidden
{
    return self.visibleViewController;
}


@end
