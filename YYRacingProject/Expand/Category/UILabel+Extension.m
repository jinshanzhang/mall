//
//  UILabel+Extension.m
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)

- (void)changeSpaceLineSpace:(float)lineSpace
                   wordSpace:(float)wordSpace{
    NSString *labelText = self.text;
    if (!kValidString(labelText)) {
        return;
    }
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(wordSpace)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpace];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    self.attributedText = attributedString;
}




// 行间距与字间距和首行缩进
- (void)changeSpaceLineSpace:(float)lineSpace WordSpace:(float)wordSpace headSpace:(float)headSpace
{
    
    NSString *_test  =  self.text;
    NSMutableParagraphStyle *paraStyle01 = [[NSMutableParagraphStyle alloc] init];
    paraStyle01.alignment = NSTextAlignmentLeft;  //对齐
    paraStyle01.headIndent = 0.0f;//行首缩进
    //参数：（字体大小17号字乘以2，34f即首行空出两个字符）
    
    paraStyle01.firstLineHeadIndent = headSpace;//首行缩进
    paraStyle01.tailIndent = 0.0f;//行尾缩进
    paraStyle01.lineSpacing = lineSpace;//行间距
    

    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:_test attributes:@{NSParagraphStyleAttributeName:paraStyle01,NSKernAttributeName:@(wordSpace)}];
    
    self.attributedText = attrText;
}


@end
