//
//  UIView+tool.m
//  BagerKit
//
//  Created by Bager on 2017/8/8.
//  Copyright © 2017年 Bager. All rights reserved.
//

#import "UIView+tool.h"

@implementation UIView (tool)

#pragma mark [frame]
// [GET方法]

- (CGFloat)v_min_x{
    return self.frame.origin.x;
}

- (CGFloat)v_min_y{
    return self.frame.origin.y;
}

- (CGFloat)v_max_x{
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)v_max_y{
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat)v_w{
    return self.frame.size.width;
}

- (CGFloat)v_h{
    return self.frame.size.height;
}
- (CGFloat)v_centerX{
    return self.center.x;
}

- (CGFloat)v_centerY{
    return self.center.y;
}
// [SET方法]

- (void)setV_x:(CGFloat)v_x{
    self.frame = CGRectMake(v_x, self.v_min_y, self.v_w, self.v_h);
}

- (void)setV_y:(CGFloat)v_y{
    self.frame = CGRectMake(self.v_min_x, v_y, self.v_w, self.v_h);
}

- (void)setV_w:(CGFloat)v_w{
    self.frame = CGRectMake(self.v_min_x, self.v_min_y, v_w, self.v_h);
}

- (void)setV_h:(CGFloat)v_h{
    self.frame = CGRectMake(self.v_min_x, self.v_min_y, self.v_w, v_h);
}


- (void)setV_centerX:(CGFloat)v_centerX{
    self.center = CGPointMake(v_centerX, self.v_centerY);
}

- (void)setV_centerY:(CGFloat)v_centerY{
    self.center = CGPointMake(self.v_centerX, v_centerY);
}
#pragma mark [layer]

- (CGFloat)v_cornerRadius{
    return self.layer.cornerRadius;
}

- (void)setV_cornerRadius:(CGFloat)v_cornerRadius{
    self.layer.cornerRadius = v_cornerRadius;
    self.layer.masksToBounds = YES;
}

#pragma mark - 设置部分圆角
/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

- (void)addShadowToView:(UIView *)theView withColor:(UIColor *)theColor {
    
    theView.layer.shadowColor = theColor.CGColor;
    theView.layer.shadowOffset = CGSizeMake(0,10);
    theView.layer.shadowOpacity = 0.5;
    theView.layer.shadowRadius = 20;
    // 单边阴影 顶边
    float shadowPathWidth = theView.layer.shadowRadius;
    CGRect shadowRect = CGRectMake(0, 0-shadowPathWidth/2.0, theView.bounds.size.width, shadowPathWidth);
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:shadowRect];
    theView.layer.shadowPath = path.CGPath;
    
}

- (void)addShadow:(ShadowDirection)direction
      shadowColor:(UIColor *)shadowColor
    shadowOpacity:(CGFloat)shadowOpacity
     shadowRadius:(CGFloat)shadowRadius
  shadowPathWidth:(CGFloat)shadowPathWidth {

    self.layer.masksToBounds = NO;
    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOpacity = shadowOpacity;
    self.layer.shadowRadius =  shadowRadius;
    self.layer.shadowOffset = CGSizeZero;

    CGRect shadowRect;
    CGFloat originX = 0;
    CGFloat originY = 0;
    CGFloat originW = self.bounds.size.width;
    CGFloat originH = self.bounds.size.height;
    switch (direction) {
        case ShadowDirectionLeft:
            shadowRect  = CGRectMake(originX - shadowPathWidth/2, originY, shadowPathWidth, originH);
            break;
        case ShadowDirectionRight:
            shadowRect  = CGRectMake(originW - shadowPathWidth/2, originY, shadowPathWidth, originH);
            break;
        case ShadowDirectionTop:
            shadowRect  = CGRectMake(originX, originY - shadowPathWidth/2, originW,  shadowPathWidth);
            break;
        case ShadowDirectionBottom:
            shadowRect  = CGRectMake(originX, originH -shadowPathWidth/2, originW, shadowPathWidth);
            break;
        case ShadowDirectionAll:
            shadowRect  = CGRectMake(originX - shadowPathWidth/2, originY - shadowPathWidth/2, originW +  shadowPathWidth, originH + shadowPathWidth);
            break;
    }
    
    UIBezierPath *path =[UIBezierPath bezierPathWithRect:shadowRect];
    self.layer.shadowPath = path.CGPath;
}

- (void)yy_multipleViewsAutoLayout:(NSMutableArray *)views
                         superView:(UIView *)superView
                           padding:(CGFloat)padding
                          viewSize:(CGSize)viewSize {
    UIView *tempView = nil;
    for (int i = 0; i < views.count ; i ++) {
        UIView *subView = views[i];
        if (tempView) {
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                if (padding > 0) {
                    make.left.equalTo(tempView.mas_right).offset(padding);
                    make.centerY.equalTo(tempView);
                    make.size.mas_equalTo(tempView);
                }
                else {
                    make.right.equalTo(tempView.mas_left).offset(-kSizeScale(padding));
                    make.centerY.equalTo(tempView);
                    make.size.mas_equalTo(tempView);
                }
            }];
        }
        else {
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                if (padding > 0) {
                    make.left.equalTo(superView).offset(padding);
                    make.centerY.equalTo(superView);
                    make.size.mas_equalTo(viewSize);
                }
                else {
                    make.right.equalTo(superView).offset(-kSizeScale(padding));
                    make.centerY.equalTo(superView);
                    make.size.mas_equalTo(viewSize);
                }
            }];
        }
        tempView = subView;
    }
}

- (void)yy_multipleViewsAutoLayout:(NSMutableArray *)views
                         superView:(UIView *)superView
                           padding:(CGFloat)padding
                          viewSize:(CGSize)viewSize
                    startSildeLeft:(BOOL)startSildeLeft {
    UIView *tempView = nil;
    for (int i = 0; i < views.count ; i ++) {
        UIView *subView = views[i];
        if (tempView) {
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                if (startSildeLeft) {
                    make.left.equalTo(tempView.mas_right).offset(padding);
                }
                else {
                    make.right.equalTo(tempView.mas_left).offset(-kSizeScale(padding));
                }
                make.centerY.equalTo(tempView);
                make.size.mas_equalTo(tempView);
            }];
        }
        else {
            [subView mas_makeConstraints:^(MASConstraintMaker *make) {
                if (startSildeLeft) {
                    make.left.equalTo(superView).offset(padding);
                }
                else {
                    make.right.equalTo(superView).offset(-kSizeScale(padding));
                }
                make.centerY.equalTo(superView);
                make.size.mas_equalTo(viewSize);
            }];
        }
        tempView = subView;
    }
}

@end
