//
//  NSMutableAttributedString+Extension.h
//  YIYanProject
//
//  Created by cjm on 2018/5/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableAttributedString (Extension)


- (void)yy_setAttributes:(NSMutableDictionary *)attributes
                 content:(NSMutableAttributedString *)content
               alignment:(NSTextAlignment)alignment;
    
- (void)yy_setAttributes:(UIColor *)textColor
                    font:(UIFont *)font
       content:(NSMutableAttributedString *)content
               alignment:(NSTextAlignment)alignment;

- (void)yy_setAttributes:(UIColor *)textColor
                    font:(UIFont *)font
               wordSpace:(NSNumber *)wordSpace
                 content:(NSMutableAttributedString *)content
               alignment:(NSTextAlignment)alignment;

- (void)yy_setAttributeWordSpace:(NSNumber *)wordSpace
                         content:(NSMutableAttributedString *)content
                       alignment:(NSTextAlignment)alignment;

// 增加删除线
- (void)yy_setAttributeOfDeleteLine:(NSRange)contentRange;

@end
