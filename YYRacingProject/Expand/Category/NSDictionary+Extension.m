//
//  NSDictionary+Extension.m
//  YIYanProject
//
//  Created by cjm on 2018/4/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "NSDictionary+Extension.h"

@implementation NSDictionary (Extension)

+ (NSDictionary *)readLocalFileWithName:(NSString *)name
                               fileType:(NSString *)fileType {
    // 获取文件路径
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:fileType];
    // 将文件数据化
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    // 对数据进行JSON格式化并返回字典形式
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

@end
