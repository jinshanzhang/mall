//
//  UITableView+Extension.m
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UITableView+Extension.h"

@implementation UITableView (Extension)
/**
 *  是否当前手势需要开始
 */
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([NSStringFromClass([gestureRecognizer.view class]) isEqualToString:@"SDCollectionViewCell"]) {
        CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self];
        if (fabs(translation.x) < fabs(translation.y)) {
            // 上下方向的滑动
            return NO;
        }
    }
    return YES;
}

/**
 *  是否可以识别多个手势。
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    BOOL shouldSimultaneous = NO;
    if (![[[YYCommonTools getCurrentVC] className] isEqualToString:@"YY_HomeViewController"]) {
        return shouldSimultaneous;
    }
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        CGPoint translation = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self];
        if ([[[YYCommonTools getCurrentVC] className] isEqualToString:@"YY_GoodDetailViewController"]) {
            if (translation.x < 0) {
                // 临时方法处理，侧滑table跳动。
                return shouldSimultaneous;
            }
        }
        else {
            if (fabs(translation.x) < fabs(translation.y)) {
                // 上下方向的滑动
                shouldSimultaneous = YES;
            }
        }
    }
    return shouldSimultaneous;
}

@end
