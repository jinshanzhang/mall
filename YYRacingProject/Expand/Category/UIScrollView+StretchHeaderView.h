//
//  UIScrollView+StretchHeaderView.h
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface UIScrollView (StretchHeaderView)
<UIScrollViewDelegate>

@property (nonatomic, weak)   UIView *headerView;
- (void)addStretchHeaderView:(UIView *)headerView;
- (void)addStretchHeaderView:(UIView *)headerView
                     kvoCtrl:(FBKVOController *)kvoCtrl;

@end
