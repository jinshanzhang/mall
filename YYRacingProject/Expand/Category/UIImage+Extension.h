//
//  UIImage+Extension.h
//  YIYanProject
//
//  Created by cjm on 2018/4/14.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ImageIO/ImageIO.h>

typedef NS_ENUM(NSUInteger, GradientType) {
    GradientTypeTopToBottom = 0,//从上到小
    GradientTypeLeftToRight = 1,//从左到右
    GradientTypeUpleftToLowright = 2,//左上到右下
    GradientTypeUprightToLowleft = 3,//右上到左下
};

typedef NS_ENUM(NSUInteger, DiscardImageType) {
    DiscardImageUnknown = 0,
    DiscardImageTopSide = 1,
    DiscardImageRightSide = 2,
    DiscardImageBottomSide = 3,
    DiscardImageLeftSide = 4,
};

@interface UIImage (Extension)

/**渐变颜色**/
+ (UIImage *)gradientColorImageFromColors:(NSArray*)colors gradientType:(GradientType)gradientType imgSize:(CGSize)imgSize;

/**网络尺寸**/
+ (CGSize)getImageSizeWithURL:(id)URL;

/**网络获取UIImage**/
+ (UIImage *)getImageFromURL:(NSString *)fileURL;

+ (UIImage *)image:(UIImage *)image centerInSize:(CGSize)viewsize;

/**压缩图片到指定的大小**/
+ (UIImage *)compressImage:(UIImage *)image toByte:(NSUInteger)maxLength;

/**图片压缩，放大操作**/
+ (UIImage *)scaleImageKeepingRatio:(UIImage *)image
                         targetSize:(CGSize)targetSize;

+ (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize;

/**获取首帧封面图片**/
+ (UIImage *)getFirstFrameCoverImage:(NSString *)videoURL;
@end
