//
//  NSString+tool.h
//  BagerKit
//
//  Created by Bager on 2017/8/8.
//  Copyright © 2017年 Bager. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString  *const XCColorKey = @"color";
static NSString  *const XCFontKey = @"font";
static NSString  *const XCRangeKey = @"range";


@interface NSString (tool)

#pragma mark - 常用工具
/**
 * 时间戳转化为周几
 */
+ (NSString *)getWeekDay:(NSTimeInterval) time;

+ (NSString *)getDate;

+ (NSString *)formatDateAndTime:(NSTimeInterval)time
                      formatter:(NSDateFormatter *)formatter;

+ (NSString *)formatDateAndTime:(NSTimeInterval)time;

/**
 十进制转换为二进制字符串
 */
+ (NSString *)getBinaryByDecimal:(NSInteger)decimal;

#pragma mark - 给字符串添加中划线
/**
 *  添加中划线
 *
 *  @return 富文本
 */
- (NSMutableAttributedString *)addCenterLine;

#pragma mark - 给字符串添加下划线
/**
 *  添加下划线
 *
 *  @return 富文本
 */
- (NSMutableAttributedString *)addDownLine;

- (NSString *)URLEncodedString;

- (NSString *)URLDecodedString;

/**
 *  校验邮箱格式
 *
 *  @param email 需要校验的邮箱
 *
 *  @return 返回结果
 */
- (BOOL)isValidateEmail:(NSString *)email;
/**
 *  校验手机号格式
 *
 *  @param mobile 手机号
 *
 *  @return 结果
 */
- (BOOL)isValidateMobile:(NSString *)mobile;
/**
 *  校验车牌号
 *
 *  @param carNo 车牌号
 *
 *  @return 结果
 */
- (BOOL)isValidateCarNo:(NSString*)carNo;
/**
 *  校验身份证号
 *
 *  @param idCardNo 身份证号
 *
 *  @return 结果
 */
- (BOOL)isValidateIDCardNo:(NSString *)idCardNo;


/**
 *  判断是否是链接
 **/
- (BOOL)isValidUrl;

/**
 * 去除全部空格，和换行符
 */
+ (NSString *)removeBlankAndLine:(NSString *)str;

/**
 *  对字符串进行Encod  的处理
 **/
+ (NSString *)usingEncoding:(NSString *)str;

/**
 * 字典转化为字符串
 **/
+ (NSString *)dataTOjsonString:(id)object;
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

/**判断是否包含中文**/
- (BOOL)isValidateChineseStr:(NSString *)text;

/**按照指定字符剪切到结尾**/
- (NSString *)cutStringUseSpecialChar:(NSString *)chars;

/**将101001变成数组，无分割的符号的时候**/
- (NSMutableArray *)intergerValueSeparated:(BOOL)isReverse;

/**处理满1w的数字，保留一位小数，且四舍五入**/
- (NSString *)handlerMiriadeOverChar:(NSString *)chars;

/**获取当前时间戳**/
- (NSDate *)getCurrentDate;
- (NSString *)getCurrentTimestamp;
+ (NSString *)getPreHotShowTime:(NSTimeInterval)showTime
                    currentTime:(NSTimeInterval)currentTime;
+ (NSUInteger)textLength:(NSString *)text;

// 颜色转换三：iOS中十六进制的颜色（以#开头）转换为UIColor
+ (UIColor *)colorWithHexString:(NSString *)color;

/**限制产品名称字数**/
- (NSString *)changeProductNameWithProductName:(NSString *)productName length:(NSInteger)length;
/**base加密**/
- (NSString *)encode:(NSString *)string;
- (NSString *)dencode:(NSString *)base64String;

//转json字符串
+ (NSString *)convertToJsonData:(NSDictionary *)dict;

@end
