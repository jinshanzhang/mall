//
//  UILabel+Extension.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Extension)
/**行间距，字间距**/
- (void)changeSpaceLineSpace:(float)lineSpace
                   wordSpace:(float)wordSpace;

// 行间距与字间距和首行缩进
- (void)changeSpaceLineSpace:(float)lineSpace WordSpace:(float)wordSpace headSpace:(float)headSpace;
@end
