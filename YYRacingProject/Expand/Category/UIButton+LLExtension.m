//
//  UIButton+LLExtension.m
//  LLButtonDemo
//
//  Created by kevin on 2017/2/17.
//  Copyright © 2017年 Ecommerce. All rights reserved.
//

#import "UIButton+LLExtension.h"
#import <objc/runtime.h>
@interface UIButton ()

/** 记录上一次接收点击事件的时间 */
@property(nonatomic, assign) NSTimeInterval fm_acceptEventTime;
@end
/** 关联关键字 */
static const char *UIControl_multipleClickInterval = "fm_multipleClickInterval";
static const char *UIControl_acceptEventTime = "fm_acceptEventTime";


@implementation UIButton (LLExtension)
/** 动态关联对象 */
- (void)setFm_multipleClickInterval:(NSTimeInterval)fm_multipleClickInterval {
    //四个参数：源对象，关键字，关联的对象和一个关联策略
    objc_setAssociatedObject(self, UIControl_multipleClickInterval, @(fm_multipleClickInterval), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSTimeInterval)fm_multipleClickInterval {
    return [objc_getAssociatedObject(self, UIControl_multipleClickInterval) doubleValue];
}
- (void)setFm_acceptEventTime:(NSTimeInterval)fm_acceptEventTime {
    objc_setAssociatedObject(self, UIControl_acceptEventTime, @(fm_acceptEventTime), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSTimeInterval)fm_acceptEventTime {
    return [objc_getAssociatedObject(self, UIControl_acceptEventTime) doubleValue];
}

+ (void)load {
    //获取着两个方法
    //系统方法
    Method sysMethod = class_getInstanceMethod(self, @selector(sendAction:to:forEvent:));
    SEL sysM = @selector(sendAction:to:forEvent:);
    //自定义方法
    Method myMethod = class_getInstanceMethod(self, @selector(fm_sendAction:to:forEvent:));
    SEL myM = @selector(fm_sendAction:to:forEvent:);
    //添加方法进去（系统方法名执行自己的自定义函数，相当于重写父类方法）
    BOOL overrideSuccess = class_addMethod(self, sysM, method_getImplementation(myMethod), method_getTypeEncoding(myMethod));
    //如果添加成功
    if (overrideSuccess) {
        //自定义函数名执行系统函数
        class_replaceMethod(self, myM, method_getImplementation(sysMethod), method_getTypeEncoding(sysMethod));
    } else {
        method_exchangeImplementations(sysMethod, myMethod);
    }
}
- (void)fm_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event {
    if (NSDate.date.timeIntervalSince1970 - self.fm_acceptEventTime < self.fm_multipleClickInterval) return;
    if (self.fm_multipleClickInterval > 0) {
        self.fm_acceptEventTime = NSDate.date.timeIntervalSince1970;//记录上次点击的时间
    }
    //这里并不是循环调用，由于交换了两个方法，fm_sendAction:to:forEvent:现在就是sendAction:to:forEvent:
    [self fm_sendAction:action to:target forEvent:event];
}



- (void)layoutButtonWithEdgeInsetsStyle:(LLButtonStyle)style
                        imageTitleSpace:(CGFloat)space
{
    //    self.backgroundColor = [UIColor cyanColor];
    /**
     *  前置知识点：titleEdgeInsets是title相对于其上下左右的inset，跟tableView的contentInset是类似的，
     *  如果只有title，那它上下左右都是相对于button的，image也是一样；
     *  如果同时有image和label，那这时候image的上左下是相对于button，右边是相对于label的；title的上右下是相对于button，左边是相对于image的。
     */
    // 1. 得到imageView和titleLabel的宽、高
    CGFloat imageWith = self.imageView.frame.size.width;
    CGFloat imageHeight = self.imageView.frame.size.height;
    
    CGFloat labelWidth = 0.0;
    CGFloat labelHeight = 0.0;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // 由于iOS8中titleLabel的size为0，用下面的这种设置
        labelWidth = self.titleLabel.intrinsicContentSize.width;
        labelHeight = self.titleLabel.intrinsicContentSize.height;
    } else {
        labelWidth = self.titleLabel.frame.size.width;
        labelHeight = self.titleLabel.frame.size.height;
    }
    
    // 2. 声明全局的imageEdgeInsets和labelEdgeInsets
    UIEdgeInsets imageEdgeInsets = UIEdgeInsetsZero;
    UIEdgeInsets labelEdgeInsets = UIEdgeInsetsZero;
    
    // 3. 根据style和space得到imageEdgeInsets和labelEdgeInsets的值
    switch (style) {
        case LLButtonStyleTextBottom:
        {
            imageEdgeInsets = UIEdgeInsetsMake(-labelHeight-space/2.0, 0, 0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight-space/2.0, 0);
        }
            break;
        case LLButtonStyleTextRight:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, -space/2.0, 0, space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, space/2.0, 0, -space/2.0);
        }
            break;
        case LLButtonStyleTextTop:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, 0, -labelHeight-space/2.0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(-imageHeight-space/2.0, -imageWith, 0, 0);
        }
            break;
        case LLButtonStyleTextLeft:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth+space/2.0, 0, -labelWidth-space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith-space/2.0, 0, imageWith+space/2.0);
        }
            break;
            
        default:
            break;
    }
    
    // 4. 赋值
    self.titleEdgeInsets = labelEdgeInsets;
    self.imageEdgeInsets = imageEdgeInsets;
}

@end
