//
//  UIButton+Condition.h
//  XinHuoApp
//
//  Created by born on 2017/2/21.
//  Copyright © 2017年 蔡金明. All rights reserved.
//

#import <UIKit/UIKit.h>

#define defaultInterval 0
@interface UIButton (Condition)
/**点击间隔**/
@property (nonatomic , assign) NSTimeInterval timeInterval;
@property (nonatomic, assign) NSTimeInterval cs_acceptEventTime;
/**是否忽略设置间隔**/
@property (nonatomic , assign) BOOL isIgnore;

@property (nonatomic , copy) void (^actionBlock)(id);

- (void)enlargeTouchAreaWithTop:(CGFloat)top
                          right:(CGFloat)right
                         bottom:(CGFloat)bottom
                           left:(CGFloat)left;

@end
