//
//  UIImageView+Extension.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Extension)

- (void)yy_sdWebImage:(NSString *)imageURL
 placeholderImageType:(YYPlaceholderImageType)placeholderImageType;

@end
