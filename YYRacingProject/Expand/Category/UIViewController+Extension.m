//
//  UIViewController+Extension.m
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UIViewController+Extension.h"

#import "AppDelegate.h"
#import "YYAudioManagerClass.h"

@implementation UIViewController (Extension)

- (void)pushAndPopViewController:(BOOL)isPush
                  viewController:(UIViewController *)ctrl{
    CATransition *animation = [CATransition animation];
    //设置运动轨迹的速度
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //设置动画类型为立方体动画
    animation.type = @"fade";
    //设置动画时长
    animation.duration = 0.5f;
    //控制器间跳转动画
    [[UIApplication sharedApplication].keyWindow.layer addAnimation:animation forKey:nil];
    if (isPush) {
        // 这里animated 一定要为NO
        [self.navigationController pushViewController:ctrl animated:YES];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)popToSelectVc:(YYBaseViewController *)ctrl
{
    if (!ctrl) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[ctrl class]]) {
            [self.navigationController popToViewController:ctrl animated:YES];
        }
    }
}


- (void)pushAndHideTabbartoViewController:(UIViewController *)ctrl{
    [ctrl setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (void)setTabBarHidden:(BOOL)hidden operatorView:(UIView *)operatorView {
    
    UIView *tab = self.tabBarController.view;
    tab.backgroundColor = [UIColor clearColor];
    
    CGRect tabRect = self.tabBarController.tabBar.frame;
    
    if ([tab.subviews count] < 2) {
        return;
    }
    
    UIView *view;

    if ([[tab.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]]) {
        view = [tab.subviews objectAtIndex:1];
    } else {
        view = [tab.subviews objectAtIndex:0];
    }
    
    view.backgroundColor = [UIColor clearColor];
    
    if (hidden) {
        view.frame = tab.bounds;
        tabRect.origin.y = kScreenH + kTabbarH;
        
        operatorView.frame = CGRectMake(0, kNavigationH, kScreenW, kScreenH + kTabbarH);
        
        [tab bringSubviewToFront:view];
        [view sendSubviewToBack:self.tabBarController.tabBar];
        [UITabBar appearance].translucent = NO;
        
    } else {
        view.frame = CGRectMake(tab.bounds.origin.x, tab.bounds.origin.y, tab.bounds.size.width, tab.bounds.size.height);
        
        tabRect.origin.y = kScreenH - kTabbarH;
        
        operatorView.frame = view.frame;
        
        [tab bringSubviewToFront:self.tabBarController.tabBar];
        [self.tabBarController.tabBar sendSubviewToBack:view];
        [UITabBar appearance].translucent = NO;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        self.tabBarController.tabBar.frame = tabRect;
    }completion:^(BOOL finished) {
    }];
}

- (void)pushViewController:(NSString *)name
                    params:(NSDictionary *)params{
    //类名
    NSString *class = [NSString stringWithFormat:@"%@",name];
    const char *className = [class cStringUsingEncoding:NSASCIIStringEncoding];
    //从一个字符返回一个类
    Class newClass = objc_getClass(className);
    if (!newClass) {
        //创建一个类
        Class superClass = [NSObject class];
        newClass = objc_allocateClassPair(superClass, className, 0);
        //注册创建的类
        objc_registerClassPair(newClass);
    }
    // 创建对象
    id instance = [[newClass alloc]init];
    // 对该对象赋值属性
    NSDictionary * propertys = params[@"property"];
    [propertys enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        // 检测这个对象是否存在该属性
        if ([self checkIsExistPropertyWithInstance:instance verifyPropertyName:key]) {
            // 利用kvc赋值
            [instance setValue:obj forKey:key];
        }
    }];
    // 获取导航控制器
    /*YYBaseTabBarViewController *tabVC = [(AppDelegate *)[[UIApplication sharedApplication] delegate] tabbar];
    UINavigationController *pushClassStance = (UINavigationController *)tabVC.viewControllers[tabVC.selectedIndex];
    // 跳转到对应的控制器
    [pushClassStance pushViewController:instance animated:YES];*/
}

- (BOOL)checkIsExistPropertyWithInstance:(id)instance verifyPropertyName:(NSString *)verifyPropertyName
{
    unsigned int outCount, i;
    // 获取对象里的属性列表
    objc_property_t * properties = class_copyPropertyList([instance
                                                           class], &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property =properties[i];
        //  属性名转成字符串
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        // 判断该属性是否存在
        if ([propertyName isEqualToString:verifyPropertyName]) {
            free(properties);
            return YES;
        }
    }
    free(properties);
    return NO;
}

- (void)judgeScrollDirection:(UIScrollView *)scrollView {
    UIPanGestureRecognizer *pan = scrollView.panGestureRecognizer;
    //获取到拖拽的速度 >0 向下拖动 <0 向上拖动
    CGFloat velocity = [pan velocityInView:scrollView].y;
    if (velocity <- 5) {
        [[YYAudioManagerClass shared] handlerAudioHideAndShow:NO];
    }
    else if (velocity > 5) {
        [[YYAudioManagerClass shared] handlerAudioHideAndShow:YES];
    }
    else if(velocity == 0) {
    }
}

@end
