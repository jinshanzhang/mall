//
//  UIViewController+CJParams.h
//  CJ Dropshipping
//
//  Created by cujia_1 on 2020/7/28.
//  Copyright © 2020 CuJia. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (CJParams)

@property (nonatomic, strong) NSDictionary * params;

@end

NS_ASSUME_NONNULL_END
