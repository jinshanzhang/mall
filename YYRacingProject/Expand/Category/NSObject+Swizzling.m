//
//  NSObject+Swizzling.m
//  XinHuoApp
//
//  Created by born on 2017/2/21.
//  Copyright © 2017年 蔡金明. All rights reserved.
//

#import "NSObject+Swizzling.h"
#import <objc/runtime.h>

@implementation NSObject (Swizzling)

+ (void)methodSwizzlingWithOriginalSelector:(SEL)originalSelector swizzling:(SEL)swizzlingSelector{
    Class cls = [self class];
    Method originalMethod = class_getInstanceMethod(cls, originalSelector);
    Method swizzlingMethod = class_getInstanceMethod(cls, swizzlingSelector);
    // 尝试给SEL 添加IMP，为了防止原来的SEL，没有实现IMP 的情况
    BOOL isAddMethod = class_addMethod(cls, originalSelector, method_getImplementation(swizzlingMethod), method_getTypeEncoding(swizzlingMethod));
    if (isAddMethod) {
        // 添加成功，说明原来的SEL没有实现IMP,将原来的SEL的IMP 替换成交换的IMP.
        class_replaceMethod(cls, swizzlingSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod));
    }
    else{
        // 添加失败，说明原来的SEL已经有IMP,直接交换即可。
        method_exchangeImplementations(originalMethod, swizzlingMethod);
    }
}


@end
