//
//  UIImageView+Extension.m
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UIImageView+Extension.h"

@implementation UIImageView (Extension)

- (void)yy_sdWebImage:(NSString *)imageURL placeholderImageType:(YYPlaceholderImageType)placeholderImageType {
    NSString *placeHolderImageName = @"";
    if (placeholderImageType == YYPlaceholderImageHomeBannerType) {
        placeHolderImageName = @"home_banner_placehold_icon";
    }
    else if (placeholderImageType == YYPlaceholderImageListGoodType) {
        placeHolderImageName = @"home_list_good_placehold_icon";
    }
    else if (placeholderImageType == YYPlaceholderImageHotGoodType) {
        placeHolderImageName = @"home_hot_good_placehold_icon";
    }
    else if (placeholderImageType == YYPlaceholderImageGoodDetailType) {
        placeHolderImageName = @"good_detail_placehold_icon";
    }
    [self sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:[UIImage imageNamed:placeHolderImageName] options:SDWebImageRetryFailed completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        if ([error.userInfo objectForKey:@"NSErrorFailingURLKey"]) {
            //上报错误URL
            NSURL *errorURL = [error.userInfo objectForKey:@"NSErrorFailingURLKey"];
            if (errorURL) {
                [dic setObject:errorURL forKey:@"ID"];
            }
        }
    }];
}

@end
