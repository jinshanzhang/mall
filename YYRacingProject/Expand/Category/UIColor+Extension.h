//
//  UIColor+Extension.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2019/4/9.
//  Copyright © 2019 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Extension.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Extension)

/**设置渐变的颜色**/
+ (UIColor *)addGradientColor:(NSArray*)colors gradientType:(GradientType)gradientType imgSize:(CGSize)imgSize;

/**金色渐变**/
+ (UIColor *)addGoldGradient:(CGSize)colorScopeSize;

/**深蓝色渐变**/
+ (UIColor *)addDeepBlueGradient:(CGSize)colorScopeSize;

@end

NS_ASSUME_NONNULL_END
