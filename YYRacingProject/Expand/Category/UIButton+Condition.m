//
//  UIButton+Condition.m
//  XinHuoApp
//
//  Created by born on 2017/2/21.
//  Copyright © 2017年 蔡金明. All rights reserved.
//

#import "UIButton+Condition.h"
#import "NSObject+Swizzling.h"
#import <objc/runtime.h>

static char topKey;
static char rightKey;
static char bottomKey;
static char leftKey;

static const char *ClickEventBlockKey = "BlockKey";
static const char *UIControl_acceptEventTime = "UIControl_acceptEventTime";

@interface  XHCustomeButtonTarget : NSObject
@property (nonatomic , copy) void (^block)(id sender);

- (id)initWithBlock:(void (^)(id sender))block;
- (void)invoke:(id)sender;

@end

@implementation XHCustomeButtonTarget

- (id)initWithBlock:(void (^)(id))block{
    self = [super init];
    if (self) {
        _block = [block copy];
    }
    return self;
}

- (void)invoke:(id)sender{
    if (self.block) {
        self.block(sender);
    }
}

@end

@implementation UIButton (Condition)

+ (void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self methodSwizzlingWithOriginalSelector:@selector(sendAction:to:forEvent:) swizzling:@selector(sure_SendAction:to:forEvent:)];
    });
}
/**
 *timeInterval
 **/
- (NSTimeInterval)timeInterval{
    return [objc_getAssociatedObject(self, _cmd) doubleValue];
}

- (void)setTimeInterval:(NSTimeInterval)timeInterval{
    objc_setAssociatedObject(self, @selector(timeInterval), @(timeInterval), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 *acceptEventTime
 **/
- (NSTimeInterval)cs_acceptEventTime {
    return  [objc_getAssociatedObject(self, UIControl_acceptEventTime) doubleValue];
}

- (void)setCs_acceptEventTime:(NSTimeInterval)cs_acceptEventTime {
    objc_setAssociatedObject(self, UIControl_acceptEventTime, @(cs_acceptEventTime), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/**
 *block
 **/
- (void)setActionBlock:(void (^)(id))actionBlock{
    XHCustomeButtonTarget *target = [[XHCustomeButtonTarget alloc] initWithBlock:actionBlock];
    objc_setAssociatedObject(self, &ClickEventBlockKey, target, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addTarget:target action:@selector(invoke:) forControlEvents:UIControlEventTouchUpInside];
}

- (void (^)(id))actionBlock{
    XHCustomeButtonTarget *target = objc_getAssociatedObject(self, &ClickEventBlockKey);
    return target.block;
}

- (void)sure_SendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event{
    // 是否忽略
    if (self.isIgnore) {
        [self sure_SendAction:action to:target forEvent:event];
        return;
    }
    if ([NSStringFromClass(self.class) isEqualToString:@"UIButton"]) {
        self.timeInterval = self.timeInterval==0?defaultInterval:self.timeInterval;
        if ([NSDate date].timeIntervalSince1970 - self.cs_acceptEventTime < self.timeInterval) {
            return;
        }
        if (self.timeInterval > 0) {
            self.cs_acceptEventTime = [NSDate date].timeIntervalSince1970;
        }
    }
    
    [self sure_SendAction:action to:target forEvent:event];
}
// 注意BOOL 类型需要用OBJC_ASSOCIATION_RETAIN_NONATOMIC
- (BOOL)isIgnore{
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setIsIgnore:(BOOL)isIgnore{
    objc_setAssociatedObject(self, @selector(isIgnore), @(isIgnore), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

// 扩大按钮的可点击区域
- (void)enlargeTouchAreaWithTop:(CGFloat)top
                          right:(CGFloat)right
                         bottom:(CGFloat)bottom
                           left:(CGFloat)left {
    objc_setAssociatedObject(self, &topKey, [NSNumber numberWithFloat:top], OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &rightKey, [NSNumber numberWithFloat:right], OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &bottomKey, [NSNumber numberWithFloat:bottom], OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &leftKey, [NSNumber numberWithFloat:left], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGRect)enlargedRect {
    CGRect rect = CGRectZero;
    NSNumber* topEdge = objc_getAssociatedObject(self, &topKey);
    NSNumber* rightEdge = objc_getAssociatedObject(self, &rightKey);
    NSNumber* bottomEdge = objc_getAssociatedObject(self, &bottomKey);
    NSNumber* leftEdge = objc_getAssociatedObject(self, &leftKey);
    if (topEdge && rightEdge && bottomEdge && leftEdge) {
        rect = CGRectMake(self.bounds.origin.x - leftEdge.floatValue,
                          self.bounds.origin.y - topEdge.floatValue,
                          self.bounds.size.width + leftEdge.floatValue + rightEdge.floatValue,
                          self.bounds.size.height + topEdge.floatValue + bottomEdge.floatValue);
    } else {
        rect = self.bounds;
    }
    return rect;
}



- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent*)event {
    CGRect rect = [self enlargedRect];
    if (CGRectEqualToRect(rect, self.bounds)) {
        return [super hitTest:point withEvent:event];
    }
    return CGRectContainsPoint(rect, point) ? self : nil;
}


/*- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    [super pointInside:point withEvent:event];
    CGFloat width = 0, height  = 0;
    CGRect bounds = self.bounds;
    if (bounds.size.width <= 25.0) {
        width = 44 - bounds.size.width;
    }
    if (bounds.size.height <= 25.0) {
        height = 44 - bounds.size.height;
    }
    bounds = CGRectInset(bounds, -(width*0.8), -(height*0.8));
    return CGRectContainsPoint(bounds, point);
}*/
@end
