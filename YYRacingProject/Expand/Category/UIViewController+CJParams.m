//
//  UIViewController+CJParams.m
//  CJ Dropshipping
//
//  Created by cujia_1 on 2020/7/28.
//  Copyright © 2020 CuJia. All rights reserved.
//

#import "UIViewController+CJParams.h"

static NSString * CJParamtersKey = @"CJParamtersKey";

@implementation UIViewController (CJParams)

- (void)setParams:(NSDictionary *)params {
    objc_setAssociatedObject(self, &CJParamtersKey, params, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDictionary *)params {
    return objc_getAssociatedObject(self, &CJParamtersKey);
}

@end
