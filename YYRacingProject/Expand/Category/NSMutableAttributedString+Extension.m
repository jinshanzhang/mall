//
//  NSMutableAttributedString+Extension.m
//  YIYanProject
//
//  Created by cjm on 2018/5/2.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "NSMutableAttributedString+Extension.h"

@implementation NSMutableAttributedString (Extension)

/**
 *  NSDictionary *dict = @{   NSFontAttributeName : boldFont(11),
                              NSParagraphStyleAttributeName : style,
                              NSKernAttributeName : @(2), //字间隔
                              NSForegroundColorAttributeName : XHRedColor};
 */
- (void)yy_setAttributes:(NSMutableDictionary *)attributes
                 content:(NSMutableAttributedString *)content
               alignment:(NSTextAlignment)alignment {
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.alignment = alignment;
    [attributes setObject:style forKey:NSParagraphStyleAttributeName];
    [content addAttributes:attributes
                       range:NSMakeRange(0, content.length)];
}

- (void)yy_setAttributeWordSpace:(NSNumber *)wordSpace
                         content:(NSMutableAttributedString *)content
                       alignment:(NSTextAlignment)alignment {
    NSMutableDictionary *attributs = [NSMutableDictionary dictionary];
    [attributs setObject:wordSpace forKey:NSKernAttributeName];
    [self yy_setAttributes:attributs content:content alignment:alignment];
}


- (void)yy_setAttributes:(UIColor *)textColor
                    font:(UIFont *)font
                 content:(NSMutableAttributedString *)content
               alignment:(NSTextAlignment)alignment {
    NSMutableDictionary *attributs = [NSMutableDictionary dictionary];
    if (textColor) {
        [attributs setObject:textColor forKey:NSForegroundColorAttributeName];
    }
    [attributs setObject:font forKey:NSFontAttributeName];
    [self yy_setAttributes:attributs content:content alignment:alignment];
}

- (void)yy_setAttributes:(UIColor *)textColor
                    font:(UIFont *)font
               wordSpace:(NSNumber *)wordSpace
                 content:(NSMutableAttributedString *)content
               alignment:(NSTextAlignment)alignment {
    NSMutableDictionary *attributs = [NSMutableDictionary dictionary];
    if (textColor) {
        [attributs setObject:textColor forKey:NSForegroundColorAttributeName];
    }
    [attributs setObject:font forKey:NSFontAttributeName];
    [attributs setObject:wordSpace forKey:NSKernAttributeName];
    [self yy_setAttributes:attributs content:content alignment:alignment];
}

// 增加删除线
- (void)yy_setAttributeOfDeleteLine:(NSRange)contentRange {
    [self addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    [self addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle)range:contentRange];
    if (@available(iOS 10.3, *)) {
        [self addAttributes:@{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),NSBaselineOffsetAttributeName:@(0)}range:contentRange];
    }
}
@end
