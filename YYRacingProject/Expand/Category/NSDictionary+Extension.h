//
//  NSDictionary+Extension.h
//  YIYanProject
//
//  Created by cjm on 2018/4/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extension)

+ (NSDictionary *)readLocalFileWithName:(NSString *)name
                               fileType:(NSString *)fileType;

@end
