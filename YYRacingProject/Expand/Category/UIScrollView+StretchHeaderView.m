//
//  UIScrollView+StretchHeaderView.m
//  YYRacingProject
//
//  Created by cjm on 2018/7/19.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import "UIScrollView+StretchHeaderView.h"
#import <objc/runtime.h>

static char *headerViewKey;
@implementation UIScrollView (StretchHeaderView)

- (void)setHeaderView:(UIView *)headerView {
    [self willChangeValueForKey:@"StretchHeaderView"];
    objc_setAssociatedObject(self, headerViewKey, headerView, OBJC_ASSOCIATION_ASSIGN);
    [self didChangeValueForKey:@"StretchHeaderView"];
}

- (UIView *)headerView {
    return objc_getAssociatedObject(self, headerViewKey);
}

- (void)addStretchHeaderView:(UIView *)headerView {
    self.contentInset = UIEdgeInsetsMake(headerView.bounds.size.height, 0, 0, 0);
    [self addSubview:headerView];
    headerView.frame = CGRectMake(0, -headerView.bounds.size.height, headerView.bounds.size.width, headerView.bounds.size.height);
    self.headerView = headerView;
}

- (void)addStretchHeaderView:(UIView *)headerView
                     kvoCtrl:(FBKVOController *)kvoCtrl {
    [self addStretchHeaderView:headerView];
    //使用kvo监听scrollView的滚动
    [kvoCtrl observe:self keyPath:@"contentOffset" options:NSKeyValueObservingOptionNew block:^(id  _Nullable observer, id  _Nonnull object, NSDictionary<NSKeyValueChangeKey,id> * _Nonnull change) {
        [self scrollViewDidScroll:self];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offSetY = scrollView.contentOffset.y;
    if (offSetY < 0) {
        self.headerView.frame = CGRectMake(0, offSetY, self.headerView.bounds.size.width, -offSetY);
    }
}

/*- (void)dealloc {
    @try {
        [self removeObserver:self forKeyPath:@"contentOffset"];
    }
    @catch (NSException* exception) {
        NSLog(@"Exception=%@\nStack Trace:%@", exception, [exception callStackSymbols]);
    }
}*/
@end
