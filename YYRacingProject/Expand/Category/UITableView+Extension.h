//
//  UITableView+Extension.h
//  YYRacingProject
//
//  Created by 蔡金明 on 2018/11/8.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (Extension)<UIGestureRecognizerDelegate>

@end

NS_ASSUME_NONNULL_END
