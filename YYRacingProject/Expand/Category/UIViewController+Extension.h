//
//  UIViewController+Extension.h
//  YIYanProject
//
//  Created by cjm on 2018/4/3.
//  Copyright © 2018年 cjm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YYBaseViewController.h"

@interface UIViewController (Extension)

- (void)pushAndPopViewController:(BOOL)isPush
                  viewController:(UIViewController *)ctrl;

- (void)pushViewController:(NSString *)name
                    params:(NSDictionary *)params;

- (void)pushAndHideTabbartoViewController:(UIViewController *)ctrl;

- (void)setTabBarHidden:(BOOL)hidden operatorView:(UIView *)operatorView;

- (void)popToSelectVc:(YYBaseViewController *)ctrl;

- (void)judgeScrollDirection:(UIScrollView *)scrollView;

@end
