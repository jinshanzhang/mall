//
//  UIView+tool.h
//  BagerKit
//
//  Created by Bager on 2017/8/8.
//  Copyright © 2017年 Bager. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ShadowDirection) {
    ShadowDirectionLeft = 0,
    ShadowDirectionRight,
    ShadowDirectionTop,
    ShadowDirectionBottom,
    ShadowDirectionAll
};

@interface UIView (tool)

#pragma mark [frame]
/**
 *  view的宽度
 */
@property (nonatomic, assign)CGFloat v_w;
/**
 *  view的高度
 */
@property (nonatomic, assign)CGFloat v_h;
/**
 *  view的中心横坐标
 */
@property (nonatomic, assign)CGFloat v_centerX;
/**
 *  view的中心纵坐标
 */
@property (nonatomic, assign)CGFloat v_centerY;

#pragma mark [layer]

/**
 *  view的圆角半径
 */
@property (nonatomic, assign)CGFloat v_cornerRadius;



/**
 *  设置view四周阴影
 **/
- (void)addShadow:(ShadowDirection)direction
      shadowColor:(UIColor *)shadowColor
    shadowOpacity:(CGFloat)shadowOpacity
     shadowRadius:(CGFloat)shadowRadius
  shadowPathWidth:(CGFloat)shadowPathWidth;

- (void)addShadowToView:(UIView *)theView withColor:(UIColor *)theColor;

/**
 *  数组视图布局
 *  padding 正从左边， 负从右边
 **/
- (void)yy_multipleViewsAutoLayout:(NSMutableArray *)views
                         superView:(UIView *)superView
                           padding:(CGFloat)padding
                          viewSize:(CGSize)viewSize;

- (void)yy_multipleViewsAutoLayout:(NSMutableArray *)views
                         superView:(UIView *)superView
                           padding:(CGFloat)padding
                          viewSize:(CGSize)viewSize
                    startSildeLeft:(BOOL)startSildeLeft;
@end
