//
//  VTMagicController.m
//  VTMagicView
//
//  Created by tianzhuo on 14-11-11.
//  Copyright (c) 2014年 tianzhuo. All rights reserved.
//

#import "VTMagicController.h"
#import <objc/runtime.h>

@interface UINavigationController (TZPopGesturePrivate)
@property (nonatomic, weak, readonly) id tz_naviDelegate;
@property (nonatomic, weak, readonly) id tz_popDelegate;
@end

@implementation UINavigationController (TZPopGesture)

+ (void)load {
    Method originalMethod = class_getInstanceMethod(self, @selector(viewWillAppear:));
    Method swizzledMethod = class_getInstanceMethod(self, @selector(tzPop_viewWillAppear:));
    method_exchangeImplementations(originalMethod, swizzledMethod);
}

- (void)tzPop_viewWillAppear:(BOOL)animated {
    [self tzPop_viewWillAppear:animated];
    // 只是为了触发tz_PopDelegate的get方法，获取到原始的interactivePopGestureRecognizer的delegate
    [self.tz_popDelegate class];
    // 获取导航栏的代理
    [self.tz_naviDelegate class];
    self.delegate = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.delegate = self.tz_naviDelegate;
    });
}

- (id)tz_popDelegate {
    id tz_popDelegate = objc_getAssociatedObject(self, _cmd);
    if (!tz_popDelegate) {
        tz_popDelegate = self.interactivePopGestureRecognizer.delegate;
        objc_setAssociatedObject(self, _cmd, tz_popDelegate, OBJC_ASSOCIATION_ASSIGN);
    }
    return tz_popDelegate;
}

- (id)tz_naviDelegate {
    id tz_naviDelegate = objc_getAssociatedObject(self, _cmd);
    if (!tz_naviDelegate) {
        tz_naviDelegate = self.delegate;
        if (tz_naviDelegate) {
            objc_setAssociatedObject(self, _cmd, tz_naviDelegate, OBJC_ASSOCIATION_ASSIGN);
        }
    }
    return tz_naviDelegate;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    if ([[self valueForKey:@"_isTransitioning"] boolValue]) {
        return NO;
    }
    if ([self.navigationController.transitionCoordinator isAnimated]) {
        return NO;
    }
    if (self.childViewControllers.count <= 1) {
        return NO;
    }
    UIViewController *vc = self.topViewController;
    /*if (vc.tz_interactivePopDisabled) {
     return NO;
     }*/
    // 侧滑手势触发位置
    CGPoint location = [gestureRecognizer locationInView:self.view];
    CGPoint offSet = [gestureRecognizer translationInView:gestureRecognizer.view];
    BOOL ret = (0 < offSet.x && location.x <= 40);
    // NSLog(@"%@ %@",NSStringFromCGPoint(location),NSStringFromCGPoint(offSet));
    return ret;
}

/// 只有当系统侧滑手势失败了，才去触发ScrollView的滑动
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 转发给业务方代理
    if (self.tz_naviDelegate && ![self.tz_naviDelegate isEqual:self]) {
        if ([self.tz_naviDelegate respondsToSelector:@selector(navigationController:didShowViewController:animated:)]) {
            [self.tz_naviDelegate navigationController:navigationController didShowViewController:viewController animated:animated];
        }
    }
    // 让系统的侧滑返回生效
    self.interactivePopGestureRecognizer.enabled = YES;
    if (self.childViewControllers.count > 0) {
        if (viewController == self.childViewControllers[0]) {
            self.interactivePopGestureRecognizer.delegate = self.tz_popDelegate; // 不支持侧滑
        } else {
            self.interactivePopGestureRecognizer.delegate = nil; // 支持侧滑
        }
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 转发给业务方代理
    if (self.tz_naviDelegate && ![self.tz_naviDelegate isEqual:self]) {
        if ([self.tz_naviDelegate respondsToSelector:@selector(navigationController:willShowViewController:animated:)]) {
            [self.tz_naviDelegate navigationController:navigationController willShowViewController:viewController animated:animated];
        }
    }
}

@end



@interface UIViewController (TZPopGesturePrivate)
@property (nonatomic, strong, readonly) UIPanGestureRecognizer *tz_popGestureRecognizer;
@end

@implementation UIViewController (TZPopGesture)

- (void)tz_addPopGestureToView:(UIView *)view {
    if (!view) return;
    if (!self.navigationController) {
        // 在控制器转场的时候，self.navigationController可能是nil,这里用GCD和递归来处理这种情况
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self tz_addPopGestureToView:view];
        });
    } else {
        UIPanGestureRecognizer *pan = self.tz_popGestureRecognizer;
        if (![view.gestureRecognizers containsObject:pan]) {
            [view addGestureRecognizer:pan];
        }
    }
}

- (UIPanGestureRecognizer *)tz_popGestureRecognizer {
    UIPanGestureRecognizer *pan = objc_getAssociatedObject(self, _cmd);
    if (!pan) {
        // 侧滑返回手势 手势触发的时候，让target执行action
        id target = self.navigationController.tz_popDelegate;
        SEL action = NSSelectorFromString(@"handleNavigationTransition:");
        pan = [[UIPanGestureRecognizer alloc] initWithTarget:target action:action];
        pan.maximumNumberOfTouches = 1;
        pan.delegate = self.navigationController;
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        objc_setAssociatedObject(self, _cmd, pan, OBJC_ASSOCIATION_ASSIGN);
    }
    return pan;
}

- (BOOL)tz_interactivePopDisabled {
    return [objc_getAssociatedObject(self, _cmd) boolValue];
}

- (void)setTz_interactivePopDisabled:(BOOL)disabled {
    objc_setAssociatedObject(self, @selector(tz_interactivePopDisabled), @(disabled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@interface VTMagicController ()


@end

@implementation VTMagicController

#pragma mark - Lifecycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        } else if ([self respondsToSelector:@selector(setWantsFullScreenLayout:)]) {
            [self setValue:@YES forKey:@"wantsFullScreenLayout"];
        }
    }
    return self;
}

- (void)loadView {
    [super loadView];
    
    self.view = self.magicView;
    [self tz_addPopGestureToView:self.magicView.contentView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _appearanceState = VTAppearanceStateWillAppear;
    if (!_magicView.isSwitching) {
        [_currentViewController beginAppearanceTransition:YES animated:animated];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _appearanceState = VTAppearanceStateDidAppear;
    if (!_magicView.isSwitching) {
        [_currentViewController endAppearanceTransition];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    _appearanceState = VTAppearanceStateWillDisappear;
    if (!_magicView.isSwitching) {
        [_currentViewController beginAppearanceTransition:NO animated:animated];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    _appearanceState = VTAppearanceStateDidDisappear;
    if (!_magicView.isSwitching) {
        [_currentViewController endAppearanceTransition];
    }
}

#pragma mark - 禁止自动触发appearance methods
- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

#pragma mark - functional methods
- (UIViewController *)viewControllerAtPage:(NSUInteger)pageIndex {
    return [self.magicView viewControllerAtPage:pageIndex];
}

- (void)switchToPage:(NSUInteger)pageIndex animated:(BOOL)animated {
    [self.magicView switchToPage:pageIndex animated:animated];
}

#pragma mark - VTMagicViewDataSource
- (NSArray<NSString *> *)menuTitlesForMagicView:(VTMagicView *)magicView {
    return nil;
}

- (UIButton *)magicView:(VTMagicView *)magicView menuItemAtIndex:(NSUInteger)itemIndex {
    return nil;
}

- (UIViewController *)magicView:(VTMagicView *)magicView viewControllerAtPage:(NSUInteger)pageInde {
    return nil;
}

#pragma mark - VTMagicViewDelegate
- (void)magicView:(VTMagicView *)magicView viewDidAppear:(__kindof UIViewController *)viewController atPage:(NSUInteger)pageIndex {
    VTLog(@"index:%ld viewControllerDidAppear:%@", (long)pageIndex, viewController.view);
}

- (void)magicView:(VTMagicView *)magicView viewDidDisappear:(__kindof UIViewController *)viewController atPage:(NSUInteger)pageIndex {
    VTLog(@"index:%ld viewControllerDidDisappear:%@", (long)pageIndex, viewController.view);
}

- (void)magicView:(VTMagicView *)magicView didSelectItemAtIndex:(NSUInteger)itemIndex {
    VTLog(@"didSelectItemAtIndex:%ld", (long)itemIndex);
}

#pragma mark - accessor methods
- (VTMagicView *)magicView {
    if (!_magicView) {
        _magicView = [[VTMagicView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _magicView.autoresizesSubviews = YES;
        _magicView.magicController = self;
        _magicView.delegate = self;
        _magicView.dataSource = self;
        [self.view setNeedsLayout];
    }
    return _magicView;
}

- (NSArray<UIViewController *> *)viewControllers {
    return self.magicView.viewControllers;
}

@end
