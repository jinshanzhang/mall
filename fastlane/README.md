fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios addDevice
```
fastlane ios addDevice
```

### ios testDebugLane
```
fastlane ios testDebugLane
```

### ios testReleaseLane
```
fastlane ios testReleaseLane
```

### ios releaseLane
```
fastlane ios releaseLane
```

### ios onlinePGYLane
```
fastlane ios onlinePGYLane
```

### ios onlineLane
```
fastlane ios onlineLane
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
